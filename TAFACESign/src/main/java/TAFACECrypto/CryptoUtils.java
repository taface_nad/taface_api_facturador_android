/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package TAFACECrypto;

import TAFACE2ApiEntidad.TAException;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import java.security.cert.X509Certificate;
import javax.xml.crypto.dsig.*;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.*;
import javax.xml.crypto.dsig.spec.*;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.jcp.xml.dsig.internal.dom.XMLDSigRI;
import org.w3c.dom.Document;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.KeySelector;
import java.util.*;
import java.io.*;
import java.nio.charset.Charset;
import java.security.KeyStore;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.KeyStoreException;

/**
 *
 * @author DanielCera
 */
public class CryptoUtils {

    private String ErrMsg;

    /**
     * @return the ErrMsg
     */
    public String getErrMsg() {
        return ErrMsg;
    }

    /**
     * @param ErrMsg the ErrMsg to set
     */
    public void setErrMsg(String ErrMsg) {
        this.ErrMsg = ErrMsg;
    }

    ///////////////////SIGN XML//////////////////////////////
    public String signXML(String fileToBeSignedPath, String password, byte[] CertBytes) throws Exception {

        XMLSignatureFactory fac = getXMLSignatureFactory();
        Reference ref = getSHA1WholeDocumentEnvelopedTransformReference(fac);
        SignedInfo si = getSignedInfo(fac, ref);

        //creo el archivo temporal que contiene el certificado desencriptado        
        Random rn = new Random();
        int answer = rn.nextInt(999999) + 1;
//        String RutaArchivoTemp = "t" + answer;
//        File certFile = File.createTempFile(RutaArchivoTemp, "");
//        FileOutputStream fos = new FileOutputStream(certFile);
//        fos.write(CertBytes);
//        fos.close();

        PrivateKeyEntry keyEntry = loadPKCS12KeyStoreAndGetSigningKeyEntry(CertBytes, password);

        KeyInfo ki = getKeyInfoWithX509Data(keyEntry, fac);
        //creo el archivo temporal para firmar el XML
        answer = rn.nextInt(9999999) + 1;
        String RutaArchivoTempFileSign = "tmpSignFile" + answer;
        File n = File.createTempFile(RutaArchivoTempFileSign, ".tsf");
        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(n));
        PrintWriter writer = new PrintWriter(osw);
        String result = fileToBeSignedPath.replace("<?xml version=\"1.0\" ?>", "");
        result = result.replace("\"", "'");
        result = result.replace(" />", "/>");
        result = result.replace("<?xml version='1.0' encoding='UTF-8'?>", "");
        writer.print(result);
        osw.close();


        Document doc = instantiateDocumentToBeSigned(n.getPath());
        signDocumentAndPlaceSignatureAsLastChildElement(doc, keyEntry, fac, si, ki);
        //eliminar los temporales
//        keyEntry = null;
//        ki = null;
//        fos = null;
//        certFile.delete();
        n.delete();
        return writeResultingDocument(doc);
    }

    private XMLSignatureFactory getXMLSignatureFactory() {
        XMLSignatureFactory factory = XMLSignatureFactory.getInstance("DOM", new
                XMLDSigRI());
        return factory;
//        return XMLSignatureFactory.getInstance("DOM");
    }

    private Reference getSHA1WholeDocumentEnvelopedTransformReference(XMLSignatureFactory fac) throws Exception {
        List<Transform> lista = new ArrayList<Transform>();
        lista.add(fac.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null));
        lista.add(fac.newTransform("http://www.w3.org/TR/2001/REC-xml-c14n-20010315", (TransformParameterSpec) null));

        return fac.newReference(
                "",
                fac.newDigestMethod(DigestMethod.SHA1, null),
                lista,
                null,
                null);
    }

    private SignedInfo getSignedInfo(XMLSignatureFactory fac1, Reference ref) throws Exception {
        return fac1.newSignedInfo(
                fac1.newCanonicalizationMethod(
                CanonicalizationMethod.INCLUSIVE,
                (C14NMethodParameterSpec) null),
                fac1.newSignatureMethod(SignatureMethod.RSA_SHA1, null),
                Collections.singletonList(ref));
    }

    private PrivateKeyEntry loadPKCS12KeyStoreAndGetSigningKeyEntry(byte[] CerfBytes, String password) throws Exception {
        try {
            java.io.InputStream certPKCS12 = new ByteArrayInputStream(CerfBytes);

            KeyStore ks = KeyStore.getInstance("PKCS12");

            ks.load(certPKCS12, password.toCharArray());

            return (PrivateKeyEntry) ks.getEntry(ks.aliases().nextElement(), new KeyStore.PasswordProtection(password.toCharArray()));
        } catch (KeyStoreException ex) {
            throw new TAException("Error en la firma del certificado: " + ex.getMessage());
        }
    }

    private KeyInfo getKeyInfoWithX509Data(PrivateKeyEntry keyEntry, XMLSignatureFactory fac) {
        X509Certificate cert = (X509Certificate) keyEntry.getCertificate();
        KeyInfoFactory kif = fac.getKeyInfoFactory();
        List<Object> x509Content = new ArrayList<Object>();
        X509IssuerSerial issuer = kif.newX509IssuerSerial(cert.getIssuerX500Principal().getName(), cert.getSerialNumber());
        x509Content.add(issuer);
        x509Content.add(cert);
        X509Data xd = kif.newX509Data(x509Content);
        return kif.newKeyInfo(Collections.singletonList(xd));
    }

    private Document instantiateDocumentToBeSigned(String fileToBeSignedPath) throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        return dbf.newDocumentBuilder().parse(new FileInputStream(fileToBeSignedPath));
    }

    //Si quiero firmar el XML antes de los datos
    private void signDocumentAndPlaceSignatureAsFirstChildElement(Document doc, PrivateKeyEntry keyEntry, XMLSignatureFactory fac, SignedInfo si, KeyInfo ki) throws Exception {
        DOMSignContext dsc = new DOMSignContext(keyEntry.getPrivateKey(), doc.getDocumentElement(), doc.getDocumentElement().getFirstChild());
        XMLSignature signature = fac.newXMLSignature(si, ki);
        signature.sign(dsc);
    }

    private void signDocumentAndPlaceSignatureAsLastChildElement(Document doc, PrivateKeyEntry keyEntry, XMLSignatureFactory fac, SignedInfo si, KeyInfo ki) throws Exception {
        DOMSignContext dsc = new DOMSignContext(KeySelector.singletonKeySelector(keyEntry.getPrivateKey()), doc.getDocumentElement()); // para firmar 
        XMLSignature signature = fac.newXMLSignature(si, ki);
        signature.sign(dsc);
    }

    private String writeResultingDocument(Document doc) throws Exception {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer trans = tf.newTransformer();
        Random rn = new Random();
        int answer = rn.nextInt(9999999) + 1;
        String RutaArchivoTemp = "tmpSignedFile" + answer;
        File n = File.createTempFile(RutaArchivoTemp, ".tsgf");
        StreamResult resSigned = new StreamResult(n.getPath());
        trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        trans.transform(new DOMSource(doc), resSigned);
        //leer el XML y quitar la primera etiqueta
        String everything = "";
        BufferedReader br = new BufferedReader(new FileReader(n.getPath()));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            everything = sb.toString().trim();

        } finally {
            br.close();
        }
        String replace = everything.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>", ""); //quitar el primer encabezado del xml
        replace = replace.replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");
        n.delete();
        return replace;

    }
}
