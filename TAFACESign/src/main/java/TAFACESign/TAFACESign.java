/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package TAFACESign;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import TAFACE2ApiEntidad.TAException;
import TAFACECrypto.CryptoUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.security.Key;
import java.security.spec.KeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 *
 * @author DanielCera
 */
public class TAFACESign {

    private char[] HEX = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private Cipher _cipherDecrypEncrypt;
    private CryptoUtils cryptoUtils; //acceso a las funcionalidades de CryptoUtils

    public TAFACESign() {
        cryptoUtils = new CryptoUtils();
    }

    public boolean FirmarXMLConCertParam(String Xml, String Nodo, String CertificadoEncriptado, String CertClaveEncriptada, tangible.RefObject<String> XmlFirmado, tangible.RefObject<String> ErrorMsg) throws TAException {
        boolean Firmo = false;
        File n = new File("");
        byte[] CertBytes = null;
        cryptoUtils.setErrMsg("");
        Firmo = false;
        try {

            CertBytes = DesencriptarCertificado(CertificadoEncriptado, CertClaveEncriptada);
            //Save certif into a temp file to create later an instance of Sign
            XmlFirmado.argValue = cryptoUtils.signXML(Xml, "", CertBytes);

            if (!cryptoUtils.getErrMsg().equals("")) {
                throw new RuntimeException(cryptoUtils.getErrMsg());
            } else {
                if (XmlFirmado.argValue == "") {
                    throw new RuntimeException("No se pudo firmar, XmlFirmado sin datos.");
                }
                Firmo = true;
            }
        } catch (RuntimeException ex) {
            ErrorMsg.argValue = ex.getMessage();
        } catch (IOException ex) {
            throw new TAException("Error durante la firma del XML: " + ex.getMessage());
        } catch (NoSuchAlgorithmException ex) {
            throw new TAException("Error durante la firma del XML: " + ex.getMessage());
        } catch (InvalidKeySpecException ex) {
            throw new TAException("Error durante la firma del XML: " + ex.getMessage());
        } catch (NoSuchPaddingException ex) {
            throw new TAException("Error durante la firma del XML: " + ex.getMessage());
        } catch (InvalidAlgorithmParameterException ex) {
            throw new TAException("Error durante la firma del XML: " + ex.getMessage());
        } catch (NoSuchProviderException ex) {
            throw new TAException("Error durante la firma del XML: " + ex.getMessage());
        } catch (ShortBufferException ex) {
            throw new TAException("Error durante la firma del XML: " + ex.getMessage());
        } catch (InvalidKeyException ex) {
            throw new TAException("Error durante la firma del XML: " + ex.getMessage());
        } catch (IllegalBlockSizeException ex) {
            throw new TAException("Error durante la firma del XML: " + ex.getMessage());
        } catch (BadPaddingException ex) {
            throw new TAException("Error durante la firma del XML: " + ex.getMessage());
        } catch (Exception ex) {
            throw new TAException("Error durante la firma del XML: " + ex.getMessage());
        }
        return Firmo;
    }

    //Desencriptar certificado a partir de una clave con PKCS7-Rijndael
    public byte[] DesencriptarCertificado(String pCertificado, String pClave) throws NoSuchAlgorithmException, InvalidKeySpecException, UnsupportedEncodingException, NoSuchPaddingException, InvalidAlgorithmParameterException, NoSuchProviderException, ShortBufferException, IOException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

        try {
            String CertClaveDesencriptada = DesencriptarString(pClave, "");

            ////////////CONVERTIR CLAVE//////////////
            byte[] bytes = new byte[8];
            byte[] BytesClave = CertClaveDesencriptada.getBytes();
            int length = Math.min(BytesClave.length, bytes.length);

            for (int i = 0; i < length; i++) {
                bytes[i] = BytesClave[i];
            }

            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            KeySpec pbeKeySpec = new PBEKeySpec(CertClaveDesencriptada.toCharArray(), bytes, 1000, 384);
            Key secretKey = factory.generateSecret(pbeKeySpec);

            byte[] key = new byte[32];
            byte[] iv = new byte[16];
            byte[] encoded = secretKey.getEncoded();
            System.arraycopy(encoded, 0, key, 0, 32);
            System.arraycopy(encoded, 32, iv, 0, 16);

            SecretKeySpec secret = new SecretKeySpec(key, "Rijndael");
            AlgorithmParameterSpec ivSpec = new IvParameterSpec(iv);
            _cipherDecrypEncrypt = Cipher.getInstance("AES/CBC/PKCS7Padding", "BC");
            _cipherDecrypEncrypt.init(Cipher.DECRYPT_MODE, secret, ivSpec);
            ////////HASTA AQUI LA FUNCIONALIDAD DE CONVERTIR CLAVE////////

            ///////////////DESENCRIPTAR CERTIFICADO/////////////////////
            byte[] beforeEncrypt = HexToByte(pCertificado);
            byte[] byteCertificadoDescencriptado = _cipherDecrypEncrypt.doFinal(beforeEncrypt);

            return byteCertificadoDescencriptado;

        } catch (InvalidKeyException e) {
            throw new TAFACE2ApiEntidad.TAException(e.getMessage());
        } catch (IllegalBlockSizeException e) {
            System.out.println(e);
            throw new TAFACE2ApiEntidad.TAException(e.getMessage());
        } catch (BadPaddingException e) {
            System.out.println(e);
            throw new TAFACE2ApiEntidad.TAException(e.getMessage());
        }

    }

    //Encriptar certificado a partir de una clave con PKCS7-Rijndael
    public final String EncriptarCertificado(byte[] pCertificado, String pClave) throws IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, ShortBufferException, TAException {

        try {

            ////////////CONVERTIR CLAVE//////////////
            byte[] bytes = new byte[8];
            byte[] BytesClave = pClave.getBytes();
            int length = Math.min(BytesClave.length, bytes.length);

            for (int i = 0; i < length; i++) {
                bytes[i] = BytesClave[i];
            }

            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            KeySpec pbeKeySpec = new PBEKeySpec(pClave.toCharArray(), bytes, 12, 1000);
            Key secretKey = factory.generateSecret(pbeKeySpec);

            byte[] key = new byte[32];
            byte[] iv = new byte[16];
            byte[] encoded = secretKey.getEncoded();
            System.arraycopy(encoded, 0, key, 0, 32);
            System.arraycopy(encoded, 32, iv, 0, 16);

            SecretKeySpec secret = new SecretKeySpec(key, "Rijndael");
            AlgorithmParameterSpec ivSpec = new IvParameterSpec(iv);
            _cipherDecrypEncrypt = Cipher.getInstance("AES/CBC/PKCS7Padding", "BC");
            _cipherDecrypEncrypt.init(Cipher.ENCRYPT_MODE, secret, ivSpec);
            ////////HASTA AQUI LA PARTE DE CONVERTIR CLAVE////////

            ///////////////DESENCRIPTAR CERTIFICADO/////////////////////
            //byte[] beforeEncrypt = HexToByte(pCertificado);
            byte[] byteCertificadoDescencriptado = _cipherDecrypEncrypt.doFinal(pCertificado);

            return ByteToHex(byteCertificadoDescencriptado);
        } catch (InvalidKeyException e) {
            throw new TAFACE2ApiEntidad.TAException(e.getMessage());

        } catch (IllegalBlockSizeException e) {
            throw new TAFACE2ApiEntidad.TAException(e.getMessage());

        } catch (BadPaddingException e) {
            throw new TAFACE2ApiEntidad.TAException(e.getMessage());
        }
    }

    public final String EncriptarString(String pToEncrypt, String pClave) throws NoSuchProviderException, UnsupportedEncodingException, ShortBufferException, TAException {
        //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        //'ATENCIÓN:''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        //'En caso de modificar esta función, aplicar la misma lógica en TAFACECertKey''
        //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        String strEncriptado = "";
        if (pClave.isEmpty()) {
            pClave = GetAlternativePassword();
        }
        Rijndael rijndaelManagedJava = new Rijndael(pClave);
        strEncriptado = rijndaelManagedJava.encrypt(pToEncrypt.getBytes("US-ASCII"));

        return strEncriptado;
    }

    public final String DesencriptarString(String pToDecrypt, String pClave) throws NoSuchProviderException, UnsupportedEncodingException, ShortBufferException, TAException {

        String CadenaDesencriptada = "";
        if (pClave.isEmpty()) {
            pClave = GetAlternativePassword();
        }
        Rijndael rijndaelManagedJava = new Rijndael(pClave);
        CadenaDesencriptada = rijndaelManagedJava.decrypt(pToDecrypt);

        return CadenaDesencriptada;
    }

    private String GetAlternativePassword() {
        //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        //'ATENCIÓN:''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        //'En caso de modificar esta función, aplicar la misma lógica en TAFACECertKey''
        //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        return "bc95b36c1d4d680a";
    }

    public final String ByteToHex(byte[] bytes) {
        //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        //'ATENCIÓN:''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        //'En caso de modificar esta función, aplicar la misma lógica en TAFACECertKey''
        //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        StringBuffer hexBuffer = new StringBuffer(bytes.length * 2);
        for (int i = 0; i < bytes.length; i++) {
            for (int j = 1; j >= 0; j--) {
                hexBuffer.append(HEX[(bytes[i] >> (j * 4)) & 0xF]);
            }
        }
        return hexBuffer.toString();
    }

    public final byte[] HexToByte(String strHex) {

        int len = strHex.length();
        byte[] r = new byte[len / 2];
        for (int i = 0; i < r.length; i++) {
            int digit1 = strHex.charAt(i * 2), digit2 = strHex.charAt(i * 2 + 1);
            if (digit1 >= '0' && digit1 <= '9') {
                digit1 -= '0';
            } else if (digit1 >= 'A' && digit1 <= 'F') {
                digit1 -= 'A' - 10;
            }
            if (digit2 >= '0' && digit2 <= '9') {
                digit2 -= '0';
            } else if (digit2 >= 'A' && digit2 <= 'F') {
                digit2 -= 'A' - 10;
            }

            r[i] = (byte) ((digit1 << 4) + digit2);
        }
        return r;
    }

    //Certificado DGI
    public String ObtenerNombreCertificado(String Subject) {
        String tempObtenerNombreCertificado = null;
        String[] strCol = null;

        tempObtenerNombreCertificado = Subject;
        strCol = Subject.replace(", ", ",").split("[,]", -1);

        for (String str : strCol) {
            if (str.substring(0, 2).equals("CN") || str.substring(0, 2).equals("OU")) {
                tempObtenerNombreCertificado = str.substring(str.length() - (str.length() - 3));
                break;
            }
        }

        return tempObtenerNombreCertificado;
    }
}
