/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package TAFACESign;

import org.bouncycastle.util.encoders.Base64;

import TAFACE2ApiEntidad.TAException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.SecretKeySpec;

import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author DanielCera
 * Class to decode password to decode certificate using PKCS7 encryption
 */
public class Rijndael {

    private static String TRANSFORMATION = "AES/ECB/PKCS7Padding";
    private static String ALGORITHM = "Rijndael";
    private static String DIGEST = "MD5";
    private static Cipher _cipher;
    private static SecretKey _password;
    MessageDigest m;

    public Rijndael(String password) throws NoSuchProviderException, TAException {

        try {
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
            //Encode digest
            MessageDigest digest;
            digest = MessageDigest.getInstance(DIGEST);
            byte[] HASH = new byte[32];
            byte[] diggested = digest.digest(password.getBytes());
            System.arraycopy(diggested, 0, HASH, 0, 16);
            System.arraycopy(diggested, 0, HASH, 15, 16);
            _password = new SecretKeySpec(HASH, ALGORITHM);
            //Initialize objects
            _cipher = Cipher.getInstance(TRANSFORMATION, "BC");

        } catch (NoSuchAlgorithmException e) {
            throw new TAFACE2ApiEntidad.TAException(e.getMessage());
        } catch (NoSuchPaddingException e) {
            throw new TAFACE2ApiEntidad.TAException(e.getMessage());
        }
    }

    /**
     * Encryptor.
     *
     * @text String to be encrypted
     * @return Base64 encrypted text
     *
     */
    public String encrypt(byte[] text) throws UnsupportedEncodingException, ShortBufferException {

        byte[] encryptedData;

        try {

            _cipher.init(Cipher.ENCRYPT_MODE, _password);
            encryptedData = _cipher.doFinal(text);


        } catch (InvalidKeyException e) {
            
            return null;
        } catch (IllegalBlockSizeException e) {

            return null;
        } catch (BadPaddingException e) {

            return null;
        }

        String encodedDuke = "";
        try {
            encodedDuke = Base64.encode(encryptedData).toString();
        } catch (Exception e) {

        }
        return encodedDuke;
    }

    /**
     * Decryptor.
     *
     * @text Base64 string to be decrypted
     * @return decrypted text
     *
     */
    public String decrypt(String text) {

        try {
            _cipher.init(Cipher.DECRYPT_MODE, _password);

            //tengo que pasarle el string decodificado a 64
            byte[] decodedValue = Base64.decode(text);
            byte[] decryptedVal = _cipher.doFinal(decodedValue);
            return new String(decryptedVal);

        } catch (InvalidKeyException e) {

            return null;
        } catch (IllegalBlockSizeException e) {

            return null;
        } catch (BadPaddingException e) {

            return null;
        }

    }
}
