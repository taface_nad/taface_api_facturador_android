package Fachada;

import Negocio.TAParsing;
import TAFACE2ApiEntidad.TAException;
import java.io.UnsupportedEncodingException;

/**
 *
 * @author DanielCera
 */
public class TAParsing_v0206 {

	private TAParsing objTAParsing = new TAParsing();

	public final TAParsing.enumTipoApi getTipoApi() {
		return objTAParsing.getTipoApi();
	}

	public final void setTipoApi(TAParsing.enumTipoApi value) {
		objTAParsing.setTipoApi(value);
	}

	public final String VersionActual() {
		return objTAParsing.VersionActual();
	}

	public final boolean NuevaFactura(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, ITAParsing_v0206.enumTipoDeNegocio TipoDeNegocio, String RutaCarpetaDebug) throws TAException, UnsupportedEncodingException {
		return objTAParsing.NuevaFactura(ErrorMsg, ErrorCod, "0.99", TipoDeNegocio, RutaCarpetaDebug);
	}

	public final boolean FinalizarFactura(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, tangible.RefObject<String> XMLFactura) throws TAException {
		return objTAParsing.FinalizarFactura(ErrorMsg, ErrorCod, XMLFactura);
	}

	public final boolean DATOSADICIONALESsetDatos(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String SOFTWAREFACTURADOR, String VERSIONDESOFTWAREFACTURADOR, String EMPRESARAZONSOCIAL, long EMPRESARUT, int SUCURSALNRO, String SUCURSALNOMBRE, int CAJANRO, long CAJERONRO, String CAJERONOMBRE, String DOCUMENTOTIPO, String DOCUMENTOSERIE, long DOCUMENTONRO, long TRANSACCIONNRO, long CLIENTENRO, String CLIENTEDOC, String CLIENTERAZONSOCIAL, String CLIENTENOMBRE, String CLIENTEDIRECCION, String CLIENTETELEFONO, String CLIENTEEMAIL, String CLIENTEPAISNOM, long VENDEDORNRO, String VENDEDORNOMBRE, double VALORUNIDADINDEXADA) throws TAException {
		return objTAParsing.DATOSADICIONALESsetDatos(ErrorMsg, ErrorCod, SOFTWAREFACTURADOR, VERSIONDESOFTWAREFACTURADOR, EMPRESARAZONSOCIAL, EMPRESARUT, SUCURSALNRO, SUCURSALNOMBRE, CAJANRO, CAJERONRO, CAJERONOMBRE, DOCUMENTOTIPO, DOCUMENTOSERIE, DOCUMENTONRO, TRANSACCIONNRO, CLIENTENRO, CLIENTEDOC, CLIENTERAZONSOCIAL, CLIENTENOMBRE, CLIENTEDIRECCION, CLIENTETELEFONO, CLIENTEEMAIL, CLIENTEPAISNOM, VENDEDORNRO, VENDEDORNOMBRE, VALORUNIDADINDEXADA);
	}

	public final boolean DATOSCONTINGENCIAsetDatos(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, int TIPOCFE, String SERIECOMPROBANTE, long NROCOMPROBANTE, long CAENROAUTORIZACION, long CAENRODESDE, long CAENROHASTA, java.util.Date CAEFECVENC) throws TAException {
		return objTAParsing.DATOSCONTINGENCIAsetDatos(ErrorMsg, ErrorCod, TIPOCFE, SERIECOMPROBANTE, NROCOMPROBANTE, CAENROAUTORIZACION, CAENRODESDE, CAENROHASTA, CAEFECVENC);
	}

	public final boolean DGIA1setEncabezado(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, ITAParsing_v0206.enumTipoDeComprobanteCFE TipoCFE,
			java.util.Date FechaDeComprobante, ITAParsing_v0206.enumIndicadorTipoTraslado IndicadorTipoTraslado, java.util.Date PeriodoDesde, java.util.Date PeriodoHasta,
			ITAParsing_v0206.enumIndicadorMontosBrutos IndicadorMontosBrutos, ITAParsing_v0206.enumFormaDePago FormaDePago, java.util.Date FechaDeVencimiento, String ClausulaDeVenta,
			ITAParsing_v0206.enumModalidadDeVenta ModalidadDeVenta, ITAParsing_v0206.enumViaDeTransporte ViaDeTransporte, String InfoAdicionalDoc,
			ITAParsing_v0206.enumIVAalDia IVAalDia, ITAParsing_v0206.enumSecretoProf SecretoProfesional, ITAParsing_v0206.enumIndicadorPropiedadMercaderiaTransportada IndicadorPropMercaderiaTransp,
			ITAParsing_v0206.enumTipoDocumentoDelReceptor TipoDocumentoMercaderiaTransp, String CodigoPais, String NroDocPropietarioUrugMercadTransp, String NroDocPropietarioExtMercadTransp,
			String RazonSocPropietarioMercadTransp) throws TAException {
		return objTAParsing.DGIA1setEncabezado(ErrorMsg, ErrorCod, TipoCFE, FechaDeComprobante, IndicadorTipoTraslado, PeriodoDesde, PeriodoHasta, IndicadorMontosBrutos, FormaDePago, FechaDeVencimiento, ClausulaDeVenta, ModalidadDeVenta, ViaDeTransporte, InfoAdicionalDoc, IVAalDia, SecretoProfesional, IndicadorPropMercaderiaTransp, TipoDocumentoMercaderiaTransp, CodigoPais, NroDocPropietarioUrugMercadTransp, NroDocPropietarioExtMercadTransp, RazonSocPropietarioMercadTransp);
	}

	public final boolean DGIA2setEmisor(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, long RUCEmisor, String RazonSocialEmisor, String NombreComercialEmisor, String GiroNegocioEmisor, String CorreoEmisor, String NombreCasaPrincipalSucursalEmisor, int CodigoCasaPrincipalSucursalEmisor, String DomicilioFiscalEmisor, String CiudadEmisor, String DepartamentoEmisor, String InfoAdicionalEmisor) throws TAException {
		return objTAParsing.DGIA2setEmisor(ErrorMsg, ErrorCod, RUCEmisor, RazonSocialEmisor, NombreComercialEmisor, GiroNegocioEmisor, CorreoEmisor, NombreCasaPrincipalSucursalEmisor, CodigoCasaPrincipalSucursalEmisor, DomicilioFiscalEmisor, CiudadEmisor, DepartamentoEmisor, InfoAdicionalEmisor);
	}

	public final boolean DGIA3addTelefonoEmisor(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String TelefonoEmisor) throws TAException {
		return objTAParsing.DGIA3addTelefonoEmisor(ErrorMsg, ErrorCod, TelefonoEmisor);
	}

	public final boolean DGIA4setReceptor(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, ITAParsing_v0206.enumTipoDocumentoDelReceptor TipoDocumentoReceptor, String CodigoPaisReceptor, String NroDocumentoReceptorUruguayo, String NroDocumentoReceptorExtranjero, String RazonSocialReceptor, String DireccionReceptor, String CiudadReceptor, String DepartamentoProviciaEstado, String PaisReceptor, int CodigoPostalReceptor, String InfoAdicionalReceptor, String LugarDestinoEntrega, String NumeroOrdenCompra) throws TAException {
		return objTAParsing.DGIA4setReceptor(ErrorMsg, ErrorCod, TipoDocumentoReceptor, CodigoPaisReceptor, NroDocumentoReceptorUruguayo, NroDocumentoReceptorExtranjero, RazonSocialReceptor, DireccionReceptor, CiudadReceptor, DepartamentoProviciaEstado, PaisReceptor, CodigoPostalReceptor, InfoAdicionalReceptor, LugarDestinoEntrega, NumeroOrdenCompra);
	}

	public final boolean DGIA5setTotalesDeEncabezado(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String TipoMonedaTransaccion, double TipoDeCambio, double TotalMontoNoGravado, double TotalMontoExportacionYAsimiladas, double TotalMontoImpuestoPercibido, double TotalMontoIVAEnSuspenso, double TotalMontoNetoIVATasaMinima, double TotalMontoNetoIVATasaBasica, double TotalMontoNetoIVAOtraTasa, double TasaMinimaIVA, double TasaBasicaIVA, double TotalIVATasaMinima, double TotalIVATasaBasica, double TotalIVAOtraTasa, double TotalMontoTotal, double TotalMontoRetenido, int CantLineasDetalle, double MontoNoFacturable, double MontoTotalAPagar, double TotMntTotCredFisc) throws TAException {
		return objTAParsing.DGIA5setTotalesDeEncabezado(ErrorMsg, ErrorCod, TipoMonedaTransaccion, TipoDeCambio, TotalMontoNoGravado, TotalMontoExportacionYAsimiladas, TotalMontoImpuestoPercibido, TotalMontoIVAEnSuspenso, TotalMontoNetoIVATasaMinima, TotalMontoNetoIVATasaBasica, TotalMontoNetoIVAOtraTasa, TasaMinimaIVA, TasaBasicaIVA, TotalIVATasaMinima, TotalIVATasaBasica, TotalIVAOtraTasa, TotalMontoTotal, TotalMontoRetenido, CantLineasDetalle, MontoNoFacturable, MontoTotalAPagar, TotMntTotCredFisc);
	}

	public final boolean DGIJ1setAdenda(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, tangible.RefObject<String> AdendaTexto, byte[] AdendaOtro) throws TAException {
		return objTAParsing.DGIJ1setAdenda(ErrorMsg, ErrorCod, AdendaTexto, AdendaOtro);
	}

	public final boolean DGIA6addTotalRetencionOPercepcion(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String CodigoDeRetencionPercepcion, double ValorRetencionPercepcion) throws TAException {
		return objTAParsing.DGIA6addTotalRetencionOPercepcion(ErrorMsg, ErrorCod, CodigoDeRetencionPercepcion, ValorRetencionPercepcion);
	}

	public final boolean DGIB1addDetalleNuevaLinea(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, ITAParsing_v0206.enumIndicidaorDeFacturacionDetalle IndicadorDeFacturacion, String IndicadorAgenteResponsable, String NombreDelItem, String DescripcionAdicional, double Cantidad, String UnidadDeMedida, double PrecioUnitario, double DescuentoEnPorcentaje, double MontoDescuento, double RecargoEnPorcentaje, double MontoRecargo, double MontoItem) throws TAException {
		return objTAParsing.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, IndicadorDeFacturacion, IndicadorAgenteResponsable, NombreDelItem, DescripcionAdicional, Cantidad, UnidadDeMedida, PrecioUnitario, DescuentoEnPorcentaje, MontoDescuento, RecargoEnPorcentaje, MontoRecargo, MontoItem);
	}

	public final boolean DGIB2addDetalleLineaActualNuevoCodigoItem(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, ITAParsing_v0206.enumTipoDeCodigoItem TipoDeCodigoItem, String CodigoDelItem) throws TAException {
		return objTAParsing.DGIB2addDetalleLineaActualNuevoCodigoItem(ErrorMsg, ErrorCod, TipoDeCodigoItem, CodigoDelItem);
	}

	public final boolean DGIB3addDetalleLineaActualNuevoSubDescuento(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, TAParsing.enumTipoSubDescuento SubDescuentoTipo, double SubDescuentoValor) throws TAException {
		return objTAParsing.DGIB3addDetalleLineaActualNuevoSubDescuento(ErrorMsg, ErrorCod, SubDescuentoTipo, SubDescuentoValor);
	}

	public final boolean DGIB4addDetalleLineaActualNuevoSubRecargo(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, TAParsing.enumTipoSubRecargo SubRecargoTipo, double SubRecargoValor) throws TAException {
		return objTAParsing.DGIB4addDetalleLineaActualNuevoSubRecargo(ErrorMsg, ErrorCod, SubRecargoTipo, SubRecargoValor);
	}

	public final boolean DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String CodigoRetencionPercepcion, double Tasa, double MontoSujetoARetencionPercepcion, double ValorDeRetencionPercepcion, String InformacionAdicional) throws TAException, UnsupportedEncodingException {
		return objTAParsing.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, CodigoRetencionPercepcion, Tasa, MontoSujetoARetencionPercepcion, ValorDeRetencionPercepcion, InformacionAdicional);
	}

	public final boolean DGIC1addSubTotalInformativo(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String Glosa, short Orden, double ValorDelSubTotal) throws TAException {
		return objTAParsing.DGIC1addSubTotalInformativo(ErrorMsg, ErrorCod, Glosa, Orden, ValorDelSubTotal);
	}

	public final boolean DGID1addDescuentoORecargoGlobal(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, ITAParsing_v0206.enumTipoMovimientoDescuentoORecargoGlobal TipoDeMovimiento, ITAParsing_v0206.enumTipoDescuentoORecargoGlobal TipoDescRegGlobal, short CodigoDelDescuentoORecargo, String Glosa, double Valor, ITAParsing_v0206.enumIndicadorDeFacturacionDescuentoGlobal IndicadorDeFacturacion) throws TAException {
		return objTAParsing.DGID1addDescuentoORecargoGlobal(ErrorMsg, ErrorCod, TipoDeMovimiento, TipoDescRegGlobal, CodigoDelDescuentoORecargo, Glosa, Valor, IndicadorDeFacturacion);
	}

	public final boolean DGIE1addMedioDePago(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, short CodigoMedioDePago, String Glosa, short Orden, double ValorDelPago) throws TAException {
		return objTAParsing.DGIE1addMedioDePago(ErrorMsg, ErrorCod, CodigoMedioDePago, Glosa, Orden, ValorDelPago);
	}

	public final boolean DGIF1addInformacionDeReferencia(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, short IndicadorDeReferenciaGlobal, ITAParsing_v0206.enumTipoDeComprobanteCFE TipoCFEReferencia, String SerieCFEReferencia, long NroCFEReferencia, String RazonReferencia, java.util.Date FechaCFEReferencia) throws TAException {
		return objTAParsing.DGIF1addInformacionDeReferencia(ErrorMsg, ErrorCod, IndicadorDeReferenciaGlobal, TipoCFEReferencia, SerieCFEReferencia, NroCFEReferencia, RazonReferencia, FechaCFEReferencia);
	}

	public final boolean DGIK1setComplementoFiscal(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, long RUCEmisor, TAParsing.enumTipoDocumentoDelMandante TipoDocMandante, String CodPaisMandante, String NroDocMandante, String NombreMandante) throws TAException {
		return objTAParsing.DGIK1setComplementoFiscal(ErrorMsg, ErrorCod, RUCEmisor, TipoDocMandante, CodPaisMandante, NroDocMandante, NombreMandante);
	}

	public final boolean UnParsingXMLRespuesta(String XMLRESPUESTA, tangible.RefObject<String> ERRORMSG, tangible.RefObject<Integer> ERRORCOD, tangible.RefObject<String> WARNINGMSG, tangible.RefObject<Boolean> FIRMADOOK, tangible.RefObject<java.util.Date> FIRMADOFCHHORA, tangible.RefObject<Long> CAENA, tangible.RefObject<Long> CAENROINICIAL, tangible.RefObject<Long> CAENROFINAL, tangible.RefObject<java.util.Date> CAEVENCIMIENTO, tangible.RefObject<String> CAESERIE, tangible.RefObject<Long> CAENRO, tangible.RefObject<String> CODSEGURIDAD, tangible.RefObject<String> URLPARAVERIFICARTEXTO, tangible.RefObject<String> URLPARAVERIFICARQR, tangible.RefObject<String> RESOLUCIONIVA) throws TAException {
		return objTAParsing.UnParsingXMLRespuesta(XMLRESPUESTA, ERRORMSG, ERRORCOD, WARNINGMSG, FIRMADOOK, FIRMADOFCHHORA, CAENA, CAENROINICIAL, CAENROFINAL, CAEVENCIMIENTO, CAESERIE, CAENRO, CODSEGURIDAD, URLPARAVERIFICARTEXTO, URLPARAVERIFICARQR, RESOLUCIONIVA);
	}
}
