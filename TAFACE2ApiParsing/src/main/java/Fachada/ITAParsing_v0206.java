/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Fachada;

/**
 *
 * @author DanielCera
 */
public interface ITAParsing_v0206 {

	public enum enumTipoDeComprobanteCFE {

		eTicket(101),
		eTicketNotaCred(102),
		eTicketNotaDeb(103),
		eFactura(111),
		eFacturaNotaCred(112),
		eFacturaNotaDeb(113),
		eRemito(181),
		eResguardo(182),
		eFacturaExportacion(121),
		eFacturaNotaCredExportacion(122),
		eFacturaNotaDebExportacion(123),
		eRemitoExportacion(124),
		eTicketCuentaAjena(131),
		eTicketNotaCredCuentaAjena(132),
		eTicketNotaDebCuentaAjena(133),
		eFacturaCuentaAjena(141),
		eFacturaNotaCredCuentaAjena(142),
		eFacturaNotaDebCuentaAjena(143),
		eBoleta(151),
		eBoletaNotaCred(152),
		eBoletaNotaDeb(153),
		eTicketContingencia(201),
		eTicketNotaCredContingencia(202),
		eTicketNotaDebContingencia(203),
		eFacturaContingencia(211),
		eFacturaNotaCredContingencia(212),
		eFacturaNotaDebContingencia(213),
		eRemitoContingencia(281),
		eResguardoContingencia(282),
		eFacturaExportacionContingencia(221),
		eFacturaNotaCredExportacionContingencia(222),
		eFacturaNotaDebExportacionContingencia(223),
		eRemitoExportacionContingencia(224),
		eTicketCuentaAjenaContingencia(231),
		eTicketNotaCredCuentaAjenaContingencia(232),
		eTicketNotaDebCuentaAjenaContingencia(233),
		eFacturaCuentaAjenaContingencia(241),
		eFacturaNotaCredCuentaAjenaContingencia(242),
		eFacturaNotaDebCuentaAjenaContingencia(243),
		eBoletaContingencia(251),
		eBoletaNotaCredContingencia(252),
		eBoletaNotaDebContingencia(253);
		private int intValue;
		private static java.util.HashMap<Integer, enumTipoDeComprobanteCFE> mappings;

		private static java.util.HashMap<Integer, enumTipoDeComprobanteCFE> getMappings() {
			if (mappings == null) {
					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumTipoDeComprobanteCFE>();
					}
			}
			return mappings;
		}

		private enumTipoDeComprobanteCFE(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumTipoDeComprobanteCFE forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumTipoMovimientoDescuentoORecargoGlobal {

		D,
		R;

		public int getValue() {
			return this.ordinal();
		}

		public static enumTipoMovimientoDescuentoORecargoGlobal forValue(int value) {
			return values()[value];
		}
	}

	public enum enumTipoDescuentoORecargoGlobal {

		Monto(1),
		Porcentaje(2);
		private int intValue;
		private static java.util.HashMap<Integer, enumTipoDescuentoORecargoGlobal> mappings;

		private static java.util.HashMap<Integer, enumTipoDescuentoORecargoGlobal> getMappings() {
			if (mappings == null) {
					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumTipoDescuentoORecargoGlobal>();
					}
			}
			return mappings;
		}

		private enumTipoDescuentoORecargoGlobal(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumTipoDescuentoORecargoGlobal forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumIndicadorDeFacturacionDescuentoGlobal {

		SinDefinir(0),
		ExentoIVA(1),
		TasaMinima(2),
		TasaBasica(3),
		OtraTasa(4),
		NoFacturable(6),
		NoFacturableNegativo(7),
		ExportacionYAsimiladas(10),
		ImpuestoPercibido(11),
		IVAEnSuspenso(12),
		ItemVendidoPorNoContribuyente(13),
		ItemVendidoContribuyenteMonotributo(14),
		ItemVendidoContribuyenteIMEBA(15);
		private int intValue;
		private static java.util.HashMap<Integer, enumIndicadorDeFacturacionDescuentoGlobal> mappings;

		private static java.util.HashMap<Integer, enumIndicadorDeFacturacionDescuentoGlobal> getMappings() {
			if (mappings == null) {
					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumIndicadorDeFacturacionDescuentoGlobal>();
					}
			}
			return mappings;
		}

		private enumIndicadorDeFacturacionDescuentoGlobal(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumIndicadorDeFacturacionDescuentoGlobal forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumTipoSubDescuento {

		Monto(1),
		Porcentaje(2);
		private int intValue;
		private static java.util.HashMap<Integer, enumTipoSubDescuento> mappings;

		private static java.util.HashMap<Integer, enumTipoSubDescuento> getMappings() {
			if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumTipoSubDescuento>();
					}
			return mappings;
		}

		private enumTipoSubDescuento(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumTipoSubDescuento forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumTipoSubRecargo {

		Monto(1),
		Porcentaje(2);
		private int intValue;
		private static java.util.HashMap<Integer, enumTipoSubRecargo> mappings;

		private static java.util.HashMap<Integer, enumTipoSubRecargo> getMappings() {
					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumTipoSubRecargo>();
					}
			return mappings;
		}

		private enumTipoSubRecargo(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumTipoSubRecargo forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumIndicidaorDeFacturacionDetalle {

		ExentoIVA(1),
		TasaMinima(2),
		TasaBasica(3),
		OtraTasa(4),
		EntregaGratuita(5),
		NoFacturable(6),
		NoFacturableNegativo(7),
		ItemARebajarEnRemito(8),
		ItemAAjustarEnResguardo(9),
		ExportacionYAsimiladas(10),
		ImpuestoPercibido(11),
		IVAEnSuspenso(12),
		ItemVendidoPorNoContribuyente(13),
		ItemVendidoContribuyenteMonotributo(14),
		ItemVendidoContribuyenteIMEBA(15);
		private int intValue;
		private static java.util.HashMap<Integer, enumIndicidaorDeFacturacionDetalle> mappings;

		private static java.util.HashMap<Integer, enumIndicidaorDeFacturacionDetalle> getMappings() {
			if (mappings == null) {
					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumIndicidaorDeFacturacionDetalle>();
					}
			}
			return mappings;
		}

		private enumIndicidaorDeFacturacionDetalle(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumIndicidaorDeFacturacionDetalle forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumTipoDeCodigoItem {

		INT1,
		INT2,
		EAN,
		PLU,
		DUN;

		public int getValue() {
			return this.ordinal();
		}

		public static enumTipoDeCodigoItem forValue(int value) {
			return values()[value];
		}
	}

	public enum enumTipoDocumentoDelReceptor {

		SinDefinir(0),
		RUC(2),
		CI(3),
		Otros(4),
		Pasaporte(5),
		DNI(6),
		NIFE(7);
		private int intValue;
		private static java.util.HashMap<Integer, enumTipoDocumentoDelReceptor> mappings;

		private static java.util.HashMap<Integer, enumTipoDocumentoDelReceptor> getMappings() {
					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumTipoDocumentoDelReceptor>();
					}

			return mappings;
		}

		private enumTipoDocumentoDelReceptor(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumTipoDocumentoDelReceptor forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumIndicadorTipoTraslado {

		SinDefinir(0),
		Venta(1),
		TransladoInterno(2);
		private int intValue;
		private static java.util.HashMap<Integer, enumIndicadorTipoTraslado> mappings;

		private static java.util.HashMap<Integer, enumIndicadorTipoTraslado> getMappings() {

					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumIndicadorTipoTraslado>();
					}

			return mappings;
		}

		private enumIndicadorTipoTraslado(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumIndicadorTipoTraslado forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumIndicadorMontosBrutos {

		NoIcluyenIVA(0),
		IVAIncluido(1),
		IMEBAyAdicionalesIncluido(2);
		private int intValue;
		private static java.util.HashMap<Integer, enumIndicadorMontosBrutos> mappings;

		private static java.util.HashMap<Integer, enumIndicadorMontosBrutos> getMappings() {

					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumIndicadorMontosBrutos>();}
			return mappings;
		}

		private enumIndicadorMontosBrutos(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumIndicadorMontosBrutos forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumFormaDePago {

		SinDefinir(0),
		Contado(1),
		Credito(2);
		private int intValue;
		private static java.util.HashMap<Integer, enumFormaDePago> mappings;

		private static java.util.HashMap<Integer, enumFormaDePago> getMappings() {
					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumFormaDePago>();
					}

			return mappings;
		}

		private enumFormaDePago(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumFormaDePago forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumModalidadDeVenta {

		SinDefinir(0),
		RegimenGeneral(1),
		Consignacion(2),
		PrecioReservable(3),
		BienesPropiosAExclavesAduaneros(4),
		RegimenGeneralEsportacionDeServ(90),
		OtrasTransacciones(99);
		private int intValue;
		private static java.util.HashMap<Integer, enumModalidadDeVenta> mappings;

		private static java.util.HashMap<Integer, enumModalidadDeVenta> getMappings() {

					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumModalidadDeVenta>();
					}

			return mappings;
		}

		private enumModalidadDeVenta(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumModalidadDeVenta forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumViaDeTransporte {

		SinDefinir(0),
		Maritimo(1),
		Aereo(2),
		Terrestre(3),
		NA(8),
		Otro(9);
		private int intValue;
		private static java.util.HashMap<Integer, enumViaDeTransporte> mappings;

		private static java.util.HashMap<Integer, enumViaDeTransporte> getMappings() {

					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumViaDeTransporte>();
					}

			return mappings;
		}

		private enumViaDeTransporte(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumViaDeTransporte forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumTipoDeNegocio {

		GenericoPlaza(1),
		TiendaPlaza(2),
		TiendaFreeShop(3),
		MayoristaPlaza(4);
		private int intValue;
		private static java.util.HashMap<Integer, enumTipoDeNegocio> mappings;

		private static java.util.HashMap<Integer, enumTipoDeNegocio> getMappings() {

					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumTipoDeNegocio>();
					}

			return mappings;
		}

		private enumTipoDeNegocio(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumTipoDeNegocio forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumTipoApi {

		TAFE(1),
		TACE(2);
		private int intValue;
		private static java.util.HashMap<Integer, enumTipoApi> mappings;

		private static java.util.HashMap<Integer, enumTipoApi> getMappings() {

					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumTipoApi>();
					}

			return mappings;
		}

		private enumTipoApi(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumTipoApi forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumTipoDocumentoDelMandante {

		SinDefinir(0),
		NIE(1),
		RUC(2),
		CI(3),
		Otros(4),
		Pasaporte(5),
		DNI(6);
		private int intValue;
		private static java.util.HashMap<Integer, enumTipoDocumentoDelMandante> mappings;

		private static java.util.HashMap<Integer, enumTipoDocumentoDelMandante> getMappings() {

					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumTipoDocumentoDelMandante>();
					}

			return mappings;
		}

		private enumTipoDocumentoDelMandante(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumTipoDocumentoDelMandante forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumIVAalDia {

		IVAalDia(0),
		NoIVAalDia(1);
		private int intValue;
		private static java.util.HashMap<Integer, enumIVAalDia> mappings;

		private static java.util.HashMap<Integer, enumIVAalDia> getMappings() {

					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumIVAalDia>();
					}

			return mappings;
		}

		private enumIVAalDia(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumIVAalDia forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumSecretoProf {

		OperacionNoAmparadaSecretoProf(0),
		OperacionAmparadaSecretoProf(1);
		private int intValue;
		private static java.util.HashMap<Integer, enumSecretoProf> mappings;

		private static java.util.HashMap<Integer, enumSecretoProf> getMappings() {

					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumSecretoProf>();
					}

			return mappings;
		}

		private enumSecretoProf(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumSecretoProf forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumIndicadorPropiedadMercaderiaTransportada {

		SinDefinir(0),
		MercaderiaTerceros(1);
		private int intValue;
		private static java.util.HashMap<Integer, enumIndicadorPropiedadMercaderiaTransportada> mappings;

		private static java.util.HashMap<Integer, enumIndicadorPropiedadMercaderiaTransportada> getMappings() {

					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumIndicadorPropiedadMercaderiaTransportada>();
					}

			return mappings;
		}

		private enumIndicadorPropiedadMercaderiaTransportada(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumIndicadorPropiedadMercaderiaTransportada forValue(int value) {
			return getMappings().get(value);
		}
	}

	enumTipoApi getTipoApi();

	void setTipoApi(enumTipoApi value);

	String VersionActual();

	boolean NuevaFactura(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, enumTipoDeNegocio TipoDeNegocio, String RutaCarpetaDebug);

	boolean FinalizarFactura(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, tangible.RefObject<String> XMLFactura);

	boolean DATOSADICIONALESsetDatos(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String SOFTWAREFACTURADOR, String VERSIONDESOFTWAREFACTURADOR,
                                     String EMPRESARAZONSOCIAL, double EMPRESARUT, int SUCURSALNRO, String SUCURSALNOMBRE, int CAJANRO, double CAJERONRO, String CAJERONOMBRE, String DOCUMENTOTIPO,
                                     String DOCUMENTOSERIE, double DOCUMENTONRO, double TRANSACCIONNRO, double CLIENTENRO, String CLIENTEDOC, String CLIENTERAZONSOCIAL, String CLIENTENOMBRE,
                                     String CLIENTEDIRECCION, String CLIENTETELEFONO, String CLIENTEEMAIL, String CLIENTEPAISNOM, double VENDEDORNRO, String VENDEDORNOMBRE, double VALORUNIDADINDEXADA);

	boolean DATOSCONTINGENCIAsetDatos(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, int TIPOCFE, String SERIECOMPROBANTE, double NROCOMPROBANTE,
                                      double CAENROAUTORIZACION, double CAENRODESDE, double CAENROHASTA, java.util.Date CAEFECVENC);

	boolean DGIA1setEncabezado(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, enumTipoDeComprobanteCFE TipoCFE, java.util.Date FechaDeComprobante,
                               enumIndicadorTipoTraslado IndicadorTipoTraslado, java.util.Date PeriodoDesde, java.util.Date PeriodoHasta, enumIndicadorMontosBrutos IndicadorMontosBrutos,
                               enumFormaDePago FormaDePago, java.util.Date FechaDeVencimiento, String ClausulaDeVenta, enumModalidadDeVenta ModalidadDeVenta, enumViaDeTransporte ViaDeTransporte,
                               String InfoAdicionalDoc, enumIVAalDia IVAalDia, enumSecretoProf SecretoProfesional, enumIndicadorPropiedadMercaderiaTransportada IndicadorPropMercaderiaTransp,
                               enumTipoDocumentoDelReceptor TipoDocumentoMercaderiaTransp, String CodigoPais, String NroDocPropietarioUrugMercadTransp, String NroDocPropietarioExtMercadTransp,
                               String RazonSocPropietarioMercadTransp);

	boolean DGIA2setEmisor(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, double RUCEmisor, String RazonSocialEmisor, String NombreComercialEmisor,
                           String GiroNegocioEmisor, String CorreoEmisor, String NombreCasaPrincipalSucursalEmisor, int CodigoCasaPrincipalSucursalEmisor, String DomicilioFiscalEmisor,
                           String CiudadEmisor, String DepartamentoEmisor, String InfoAdicionalEmisor);

	boolean DGIA3addTelefonoEmisor(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String TelefonoEmisor);

	boolean DGIA4setReceptor(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, enumTipoDocumentoDelReceptor TipoDocumentoReceptor,
                             String CodigoPaisReceptor, String NroDocumentoReceptorUruguayo, String NroDocumentoReceptorExtranjero, String RazonSocialReceptor, String DireccionReceptor,
                             String CiudadReceptor, String DepartamentoProviciaEstado, String PaisReceptor, int CodigoPostalReceptor, String InfoAdicionalReceptor, String LugarDestinoEntrega,
                             String NumeroOrdenCompra);

	boolean DGIA5setTotalesDeEncabezado(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String TipoMonedaTransaccion, double TipoDeCambio,
                                        double TotalMontoNoGravado, double TotalMontoExportacionYAsimiladas, double TotalMontoImpuestoPercibido, double TotalMontoIVAEnSuspenso, double TotalMontoNetoIVATasaMinima,
                                        double TotalMontoNetoIVATasaBasica, double TotalMontoNetoIVAOtraTasa, double TasaMinimaIVA, double TasaBasicaIVA, double TotalIVATasaMinima, double TotalIVATasaBasica,
                                        double TotalIVAOtraTasa, double TotalMontoTotal, double TotalMontoRetenido, int CantLineasDetalle, double MontoNoFacturable, double MontoTotalAPagar, double TotMntTotCredFisc);

	boolean DGIJ1setAdenda(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, tangible.RefObject<String> AdendaTexto, tangible.RefObject<byte[]> AdendaOtro);

	boolean DGIA6addTotalRetencionOPercepcion(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String CodigoDeRetencionPercepcion, double ValorRetencionPercepcion);

	boolean DGIB1addDetalleNuevaLinea(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, enumIndicidaorDeFacturacionDetalle IndicadorDeFacturacion,
                                      String IndicadorAgenteResponsable, String NombreDelItem, String DescripcionAdicional, double Cantidad, String UnidadDeMedida, double PrecioUnitario, double DescuentoEnPorcentaje,
                                      double MontoDescuento, double RecargoEnPorcentaje, double MontoRecargo, double MontoItem);

	boolean DGIB2addDetalleLineaActualNuevoCodigoItem(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, enumTipoDeCodigoItem TipoDeCodigoItem, String CodigoDelItem);

	boolean DGIB3addDetalleLineaActualNuevoSubDescuento(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, enumTipoSubDescuento SubDescuentoTipo,
                                                        double SubDescuentoValor);

	boolean DGIB4addDetalleLineaActualNuevoSubRecargo(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, enumTipoSubRecargo SubRecargoTipo, double SubRecargoValor);

	boolean DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String CodigoRetencionPercepcion, double Tasa,
                                                                double MontoSujetoARetencionPercepcion, double ValorDeRetencionPercepcion, String InformacionAdicional);

	boolean DGIC1addSubTotalInformativo(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String Glosa, short Orden, double ValorDelSubTotal);

	boolean DGID1addDescuentoORecargoGlobal(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, enumTipoMovimientoDescuentoORecargoGlobal TipoDeMovimiento,
                                            enumTipoDescuentoORecargoGlobal TipoDescRegGlobal, short CodigoDelDescuentoORecargo, String Glosa, double Valor,
                                            enumIndicadorDeFacturacionDescuentoGlobal IndicadorDeFacturacion);

	boolean DGIE1addMedioDePago(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, short CodigoMedioDePago, String Glosa, short Orden, double ValorDelPago);

	boolean DGIF1addInformacionDeReferencia(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, short IndicadorDeReferenciaGlobal,
                                            enumTipoDeComprobanteCFE TipoCFEReferencia, String SerieCFEReferencia, double NroCFEReferencia, String RazonReferencia, java.util.Date FechaCFEReferencia);

	boolean DGIK1setComplementoFiscal(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, double RUCEmisor, enumTipoDocumentoDelMandante TipoDocMandante,
                                      String CodPaisMandante, String NroDocMandante, String NombreMandante);

	boolean UnParsingXMLRespuesta(String XMLRESPUESTA, tangible.RefObject<String> ERRORMSG, tangible.RefObject<Integer> ERRORCOD, tangible.RefObject<String> WARNINGMSG,
                                  tangible.RefObject<Boolean> FIRMADOOK, tangible.RefObject<java.util.Date> FIRMADOFCHHORA, tangible.RefObject<Double> CAENA, tangible.RefObject<Double> CAENROINICIAL,
                                  tangible.RefObject<Double> CAENROFINAL, tangible.RefObject<java.util.Date> CAEVENCIMIENTO, tangible.RefObject<String> CAESERIE, tangible.RefObject<Double> CAENRO,
                                  tangible.RefObject<String> CODSEGURIDAD, tangible.RefObject<String> URLPARAVERIFICARTEXTO, tangible.RefObject<String> URLPARAVERIFICARQR, tangible.RefObject<String> RESOLUCIONIVA);
}