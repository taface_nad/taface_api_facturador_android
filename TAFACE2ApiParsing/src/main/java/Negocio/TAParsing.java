package Negocio;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import Fachada.ITAParsing_v0206;
import TAFACE2ApiEntidad.*;
import XML.EnvioCFE.*;
import XML.XMLCFC.*;
import XML.XMLCONFIGURACION.*;
import XML.XMLFACTURA;
import XML.XMLFACTURA.clsDGI.Detalle;
import XML.XMLFACTURA.clsDGI.Detalle.DETALLE;
import XML.XMLFACTURA.clsDGI.Detalle.DETCodItems.DETCodItem;
import XML.XMLFACTURA.clsDGI.Detalle.DETSubDescuentos.DETSubDescuento;
import XML.XMLFACTURA.clsDGI.EmiTelefonos;
import XML.XMLFACTURA.clsDGI.EmiTelefonos.EMITelefono;
import XML.XMLFACTURA.clsDGI.TOTRetencPerceps;
import XML.XMLFACTURA.clsDGI.TOTRetencPerceps.TOTRetencPercep;
import XML.XMLLOG.*;
import XML.XMLRESPUESTA;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.xml.bind.JAXBException;

/**
 *
 * @author DanielCera
 */
public class TAParsing {

	private int cantDetalle = 0;
	//Entidades encargadas de conformar la estructura del XML.
	private XML.XMLFACTURA XMLFACTURAField;
	private XML.XMLFACTURA.clsDGI clsDGIXMLFacturaField;
	private XML.XMLFACTURA.clsDATOSADICIONALES clsDATOSADICIONALESXMLFacturaField;
	private XML.XMLFACTURA.clsDATOSCONTINGENCIA clsDATOSCONTINGENCIAXMLFacturaField;
	private XML.XMLFACTURA.clsDGI.EmiTelefonos lstEmiTelefono;
	private XML.XMLFACTURA.clsDGI.TOTRetencPerceps lstTOTRetencPercep;
	private XML.XMLFACTURA.clsDGI.Detalle lstDetalle;
	private XML.XMLFACTURA.clsDGI.SubTotalInfos lstSubTotInfo;
	private XML.XMLFACTURA.clsDGI.DscRcgGlobals lstDscRcgGlobal;
	private XML.XMLFACTURA.clsDGI.Referencias lstReferencia;
	private XML.XMLFACTURA.clsDGI.MedioPagos lstMediosPago;
	private XML.XMLFACTURA.clsDGI.Detalle.DETCodItems lstDetalleCodItem;
	private XML.XMLFACTURA.clsDGI.Detalle.DETSubDescuentos lstDetalleSubDescuento;
	private XML.XMLFACTURA.clsDGI.Detalle.DETSubRecargos lstDetalleSubRecargo;
	private XML.XMLFACTURA.clsDGI.Detalle.DETRetencPerceps lstaDetalleSubRetencPercep;
	//Listas contenedoras de objetos hijos
	private ArrayList<XMLFACTURA.clsDGI.EmiTelefonos.EMITelefono> listaTelefonosArray;
	private ArrayList<XMLFACTURA.clsDGI.Detalle.DETALLE> listaDetallesArray;
	private ArrayList<XML.XMLFACTURA.clsDGI.Detalle.DETCodItems.DETCodItem> listDetalleCodItemArray;
	private ArrayList<XML.XMLFACTURA.clsDGI.Detalle.DETSubDescuentos.DETSubDescuento> listDetalleSubDescuentoArray;
	private ArrayList<XML.XMLFACTURA.clsDGI.Detalle.DETSubRecargos.DETSubRecargo> listDetalleSubRecargoArray;
	private ArrayList<XML.XMLFACTURA.clsDGI.Detalle.DETRetencPerceps.DETRetencPercep> listaDetalleSubRetencPercepArray;
	private ArrayList<XMLFACTURA.clsDGI.SubTotalInfos.SubTotalInfo> listaSubTotInfoArray;
	private ArrayList<XMLFACTURA.clsDGI.DscRcgGlobals.DscRcgGlobal> listaDscRcgGlobalArray;
	private ArrayList<XMLFACTURA.clsDGI.MedioPagos.MedioPago> listaMediosPagoArray;
	private ArrayList<XMLFACTURA.clsDGI.Referencias.Referencia> listaReferenciaArray;
	private ArrayList<XMLFACTURA.clsDGI.TOTRetencPerceps.TOTRetencPercep> listaTOTRetencPercepsArray;
	//Maximos permitidos en las Listas
	private static final int MAXlstEmiTelefono = 2;
	private static final int MAXlstTOTRetencPercep = 20;
	private static final int MAXlstDetalleETICK = 700;
	private static final int MAXlstDetalleOTROS = 200;
	private static final int MAXlstDetalleCodItem = 5;
	private static final int MAXlstDetalleSubDescuento = 5;
	private static final int MAXlstDetalleSubRecargo = 5;
	private static final int MAXlstDetalleSubRetencPercep = 5;
	private static final int MAXlstSubTotInfo = 20;
	private static final int MAXlstDscRcgGlobal = 20;
	private static final int MAXlstMediosPago = 40;
	private static final int MAXlstReferencia = 40;
	private String MAXMargenErrorMontos = "0.10";
	private String EmpresaRUT;
	private TADebug objDebug = new TADebug();
	private enumTipoDeNegocio nTipoDeNegocio = enumTipoDeNegocio.values()[0];
	private enumTipoApi nTipoApi = enumTipoApi.TAFE;
	private static final Date MinFecha = tangible.DotNetToJavaDateHelper.dateForYMD(0100, 1, 1);

	public enum enumTipoDeComprobanteCFE {

		eTicket(101),
		eTicketNotaCred(102),
		eTicketNotaDeb(103),
		eFactura(111),
		eFacturaNotaCred(112),
		eFacturaNotaDeb(113),
		eRemito(181),
		eResguardo(182),
		eFacturaExportacion(121),
		eFacturaNotaCredExportacion(122),
		eFacturaNotaDebExportacion(123),
		eRemitoExportacion(124),
		eTicketCuentaAjena(131),
		eTicketNotaCredCuentaAjena(132),
		eTicketNotaDebCuentaAjena(133),
		eFacturaCuentaAjena(141),
		eFacturaNotaCredCuentaAjena(142),
		eFacturaNotaDebCuentaAjena(143),
		eBoleta(151),
		eBoletaNotaCred(152),
		eBoletaNotaDeb(153),
		eTicketContingencia(201),
		eTicketNotaCredContingencia(202),
		eTicketNotaDebContingencia(203),
		eFacturaContingencia(211),
		eFacturaNotaCredContingencia(212),
		eFacturaNotaDebContingencia(213),
		eRemitoContingencia(281),
		eResguardoContingencia(282),
		eFacturaExportacionContingencia(221),
		eFacturaNotaCredExportacionContingencia(222),
		eFacturaNotaDebExportacionContingencia(223),
		eRemitoExportacionContingencia(224),
		eTicketCuentaAjenaContingencia(231),
		eTicketNotaCredCuentaAjenaContingencia(232),
		eTicketNotaDebCuentaAjenaContingencia(233),
		eFacturaCuentaAjenaContingencia(241),
		eFacturaNotaCredCuentaAjenaContingencia(242),
		eFacturaNotaDebCuentaAjenaContingencia(243),
		eBoletaContingencia(251),
		eBoletaNotaCredContingencia(252),
		eBoletaNotaDebContingencia(253);
		private int intValue;
		private static java.util.HashMap<Integer, enumTipoDeComprobanteCFE> mappings;

		private static java.util.HashMap<Integer, enumTipoDeComprobanteCFE> getMappings() {
				if (mappings == null) {
					mappings = new java.util.HashMap<Integer, enumTipoDeComprobanteCFE>();
				}
			return mappings;
		}

		private enumTipoDeComprobanteCFE(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumTipoDeComprobanteCFE forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumTipoMovimientoDescuentoORecargoGlobal {

		D,
		R;

		public int getValue() {
			return this.ordinal();
		}

		public static enumTipoMovimientoDescuentoORecargoGlobal forValue(int value) {
			return values()[value];
		}
	}

	public enum enumTipoDescuentoORecargoGlobal {

		Monto(1),
		Porcentaje(2);
		private int intValue;
		private static java.util.HashMap<Integer, enumTipoDescuentoORecargoGlobal> mappings;

		private static java.util.HashMap<Integer, enumTipoDescuentoORecargoGlobal> getMappings() {
				if (mappings == null) {
					mappings = new java.util.HashMap<Integer, enumTipoDescuentoORecargoGlobal>();
				}
			return mappings;
		}

		private enumTipoDescuentoORecargoGlobal(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumTipoDescuentoORecargoGlobal forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumIndicadorDeFacturacionDescuentoGlobal {

		SinDefinir(0),
		ExentoIVA(1),
		TasaMinima(2),
		TasaBasica(3),
		OtraTasa(4),
		NoFacturable(6),
		NoFacturableNegativo(7),
		ExportacionYAsimiladas(10),
		ImpuestoPercibido(11),
		IVAEnSuspenso(12),
		temVendidoPorNoContribuyente(13),
		ItemVendidoContribuyenteMonotributo(14),
		ItemVendidoContribuyenteIMEBA(15);
		private int intValue;
		private static java.util.HashMap<Integer, enumIndicadorDeFacturacionDescuentoGlobal> mappings;

		private static java.util.HashMap<Integer, enumIndicadorDeFacturacionDescuentoGlobal> getMappings() {
				if (mappings == null) {
					mappings = new java.util.HashMap<Integer, enumIndicadorDeFacturacionDescuentoGlobal>();
				}
			return mappings;
		}

		private enumIndicadorDeFacturacionDescuentoGlobal(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumIndicadorDeFacturacionDescuentoGlobal forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumTipoSubDescuento {

		Monto(1),
		Porcentaje(2);
		private int intValue;
		private static java.util.HashMap<Integer, enumTipoSubDescuento> mappings;

		private static java.util.HashMap<Integer, enumTipoSubDescuento> getMappings() {
				if (mappings == null) {
					mappings = new java.util.HashMap<Integer, enumTipoSubDescuento>();
				}
			return mappings;
		}

		private enumTipoSubDescuento(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumTipoSubDescuento forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumTipoSubRecargo {

		Monto(1),
		Porcentaje(2);
		private int intValue;
		private static java.util.HashMap<Integer, enumTipoSubRecargo> mappings;

		private static java.util.HashMap<Integer, enumTipoSubRecargo> getMappings() {
				if (mappings == null) {
					mappings = new java.util.HashMap<Integer, enumTipoSubRecargo>();
				}
			return mappings;
		}

		private enumTipoSubRecargo(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumTipoSubRecargo forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumIndicadorDeFacturacionDetalle {

		ExentoIVA(1),
		TasaMinima(2),
		TasaBasica(3),
		OtraTasa(4),
		EntregaGratuita(5),
		NoFacturable(6),
		NoFacturableNegativo(7),
		ItemARebajarEnRemito(8),
		ItemAAjustarEnResguardo(9),
		ExportacionYAsimiladas(10),
		ImpuestoPercibido(11),
		IVAEnSuspenso(12),
		ItemVendidoPorNoContribuyente(13),
		ItemVendidoContribuyenteMonotributo(14),
		ItemVendidoContribuyenteIMEBA(15);
		private int intValue;
		private static java.util.HashMap<Integer, enumIndicadorDeFacturacionDetalle> mappings;

		private static java.util.HashMap<Integer, enumIndicadorDeFacturacionDetalle> getMappings() {
				if (mappings == null) {
					mappings = new java.util.HashMap<Integer, enumIndicadorDeFacturacionDetalle>();
				}
			return mappings;
		}

		private enumIndicadorDeFacturacionDetalle(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumIndicadorDeFacturacionDetalle forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumTipoDeCodigoItem {

		INT1,
		INT2,
		EAN,
		PLU,
		DUN;

		public int getValue() {
			return this.ordinal();
		}

		public static enumTipoDeCodigoItem forValue(int value) {
			return values()[value];
		}
	}

	public enum enumTipoDocumentoDelReceptor {

		SinDefinir(0),
		RUC(2),
		CI(3),
		Otros(4),
		Pasaporte(5),
		DNI(6),
		NIFE(7);
		private int intValue;
		private static java.util.HashMap<Integer, enumTipoDocumentoDelReceptor> mappings;

		private static java.util.HashMap<Integer, enumTipoDocumentoDelReceptor> getMappings() {
			if (mappings == null) {
					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumTipoDocumentoDelReceptor>();
					}
			}
			return mappings;
		}

		private enumTipoDocumentoDelReceptor(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumTipoDocumentoDelReceptor forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumIndicadorTipoTraslado {

		SinDefinir(0),
		Venta(1),
		TransladoInterno(2);
		private int intValue;
		private static java.util.HashMap<Integer, enumIndicadorTipoTraslado> mappings;

		private static java.util.HashMap<Integer, enumIndicadorTipoTraslado> getMappings() {
				if (mappings == null) {
					mappings = new java.util.HashMap<Integer, enumIndicadorTipoTraslado>();
				}
			return mappings;
		}

		private enumIndicadorTipoTraslado(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumIndicadorTipoTraslado forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumIndicadorMontosBrutos {

		NoIcluyenIVA(0),
		IVAIncluido(1),
		IMEBAyAdicionalesIncluido(2);
		private int intValue;
		private static java.util.HashMap<Integer, enumIndicadorMontosBrutos> mappings;

		private static java.util.HashMap<Integer, enumIndicadorMontosBrutos> getMappings() {
				if (mappings == null) {
					mappings = new java.util.HashMap<Integer, enumIndicadorMontosBrutos>();
				}
			return mappings;
		}

		private enumIndicadorMontosBrutos(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumIndicadorMontosBrutos forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumFormaDePago {

		SinDefinir(0),
		Contado(1),
		Credito(2);
		private int intValue;
		private static java.util.HashMap<Integer, enumFormaDePago> mappings;

		private static java.util.HashMap<Integer, enumFormaDePago> getMappings() {
				if (mappings == null) {
					mappings = new java.util.HashMap<Integer, enumFormaDePago>();
				}
			return mappings;
		}

		private enumFormaDePago(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumFormaDePago forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumModalidadDeVenta {

		SinDefinir(0),
		RegimenGeneral(1),
		Consignacion(2),
		PrecioReservable(3),
		BienesPropiosAExclavesAduaneros(4),
		RegimenGeneralEsportacionDeServ(90),
		OtrasTransacciones(99);
		private int intValue;
		private static java.util.HashMap<Integer, enumModalidadDeVenta> mappings;

		private static java.util.HashMap<Integer, enumModalidadDeVenta> getMappings() {
				if (mappings == null) {
					mappings = new java.util.HashMap<Integer, enumModalidadDeVenta>();
				}
			return mappings;
		}

		private enumModalidadDeVenta(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumModalidadDeVenta forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumViaDeTransporte {

		SinDefinir(0),
		Maritimo(1),
		Aereo(2),
		Terrestre(3),
		NA(8),
		Otro(9);
		private int intValue;
		private static java.util.HashMap<Integer, enumViaDeTransporte> mappings;

		private static java.util.HashMap<Integer, enumViaDeTransporte> getMappings() {
				if (mappings == null) {
					mappings = new java.util.HashMap<Integer, enumViaDeTransporte>();
				}
			return mappings;
		}

		private enumViaDeTransporte(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumViaDeTransporte forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumTipoDeNegocio {

		GenericoPlaza(1),
		TiendaPlaza(2),
		TiendaFreeShop(3),
		MayoristaPlaza(4);
		private int intValue;
		private static java.util.HashMap<Integer, enumTipoDeNegocio> mappings;

		private static java.util.HashMap<Integer, enumTipoDeNegocio> getMappings() {
				if (mappings == null) {
					mappings = new java.util.HashMap<Integer, enumTipoDeNegocio>();
				}
			return mappings;
		}

		private enumTipoDeNegocio(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumTipoDeNegocio forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumTipoApi {

		TAFE(1),
		TACE(2);
		private int intValue;
		private static java.util.HashMap<Integer, enumTipoApi> mappings;

		private static java.util.HashMap<Integer, enumTipoApi> getMappings() {
				if (mappings == null) {
					mappings = new java.util.HashMap<Integer, enumTipoApi>();
				}
			return mappings;
		}

		private enumTipoApi(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumTipoApi forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumTipoDocumentoDelMandante {

		SinDefinir(0),
		NIE(1),
		RUC(2),
		CI(3),
		Otros(4),
		Pasaporte(5),
		DNI(6);
		private int intValue;
		private static java.util.HashMap<Integer, enumTipoDocumentoDelMandante> mappings;

		private static java.util.HashMap<Integer, enumTipoDocumentoDelMandante> getMappings() {
				if (mappings == null) {
					mappings = new java.util.HashMap<Integer, enumTipoDocumentoDelMandante>();
				}
			return mappings;
		}

		private enumTipoDocumentoDelMandante(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumTipoDocumentoDelMandante forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumIVAalDia {

		IVAalDia(0),
		NoIVAalDia(1);
		private int intValue;
		private static java.util.HashMap<Integer, enumIVAalDia> mappings;

		private static java.util.HashMap<Integer, enumIVAalDia> getMappings() {
			if (mappings == null) {
					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumIVAalDia>();
					}
			}
			return mappings;
		}

		private enumIVAalDia(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumIVAalDia forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumSecretoProf {

		OperacionNoAmparadaSecretoProf(0),
		OperacionAmparadaSecretoProf(1);
		private int intValue;
		private static java.util.HashMap<Integer, enumSecretoProf> mappings;

		private static java.util.HashMap<Integer, enumSecretoProf> getMappings() {
			if (mappings == null) {
					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumSecretoProf>();
					}

			}
			return mappings;
		}

		private enumSecretoProf(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumSecretoProf forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumIndicadorPropiedadMercaderiaTransportada {

		SinDefinir(0),
		MercaderiaTerceros(1);
		private int intValue;
		private static java.util.HashMap<Integer, enumIndicadorPropiedadMercaderiaTransportada> mappings;

		private static java.util.HashMap<Integer, enumIndicadorPropiedadMercaderiaTransportada> getMappings() {
			if (mappings == null) {
					if (mappings == null) {
						mappings = new java.util.HashMap<Integer, enumIndicadorPropiedadMercaderiaTransportada>();
					}
			}
			return mappings;
		}

		private enumIndicadorPropiedadMercaderiaTransportada(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumIndicadorPropiedadMercaderiaTransportada forValue(int value) {
			return getMappings().get(value);
		}
	}

	public enum enumCodigoPais {

		Afganistan("AF"),
		Aland("AX"),
		Albania("AL"),
		Alemania("DE"),
		Andorra("AD"),
		Angola("AO"),
		Anguila("AI"),
		Antartida("AQ"),
		Antigua_y_Barbuda("AG"),
		Arabia_Saudita("SA"),
		Argelia("DZ"),
		Argentina("AR"),
		Armenia("AM"),
		Aruba("AW"),
		Australia("AU"),
		Austria("AT"),
		Azerbaiyan("AZ"),
		Bahamas("BS"),
		Banglades("BD"),
		Barbados("BB"),
		Barein("BH"),
		Belgica("BE"),
		Belice("BZ"),
		Benin("BJ"),
		Bermudas("BM"),
		Bielorrusia("BY"),
		Bolivia("BO"),
		Bonaire_San_Eustaquio_y_Saba("BQ"),
		Bosnia_y_Herzegovina("BA"),
		Botsuana("BW"),
		Brasil("BR"),
		Brunei("BN"),
		Bulgaria("BG"),
		Burkina_Faso("BF"),
		Burundi("BI"),
		Butan("BT"),
		Cabo_Verde("CV"),
		Camboya("KH"),
		Camerun("CM"),
		Canada("CA"),
		Catar("QA"),
		Chad("TD"),
		Chile("CL"),
		China("CN"),
		Chipre("CY"),
		Colombia("CO"),
		Comoras("KM"),
		Corea_del_Norte("KP"),
		Corea_del_Sur("KR"),
		Costa_de_Marfil("CI"),
		Costa_Rica("CR"),
		Croacia("HR"),
		Cuba("CU"),
		Curazao("CW"),
		Dinamarca("DK"),
		Dominica("DM"),
		Ecuador("EC"),
		Egipto("EG"),
		El_Salvador("SV"),
		Emiratos_Arabes_Unidos("AE"),
		Eritrea("ER"),
		Eslovaquia("SK"),
		Eslovenia("SI"),
		Espana("ES"),
		Estados_Unidos("US"),
		Estonia("EE"),
		Etiopia("ET"),
		Filipinas("PH"),
		Finlandia("FI"),
		Fiyi("FJ"),
		Francia("FR"),
		Gabon("GA"),
		Gambia("GM"),
		Georgia("GE"),
		Ghana("GH"),
		Gibraltar("GI"),
		Granada("GD"),
		Grecia("GR"),
		Groenlandia("GL"),
		Guadalupe("GP"),
		Guam("GU"),
		Guatemala("GT"),
		Guayana_Francesa("GF"),
		Guernsey("GG"),
		Guinea("GN"),
		Guinea_Bisau("GW"),
		Guinea_Ecuatorial("GQ"),
		Guyana("GY"),
		Haiti("HT"),
		Honduras("HN"),
		Hong_Kong("HK"),
		Hungria("HU"),
		India("[IN]"),
		Indonesia("ID"),
		Irak("IQ"),
		Iran("IR"),
		Irlanda("IE"),
		Isla_Bouvet("BV"),
		Isla_de_Man("IM"),
		Isla_de_Navidad("CX"),
		Islandia("[IS]"),
		Islas_Caiman("KY"),
		Islas_Cocos("CC"),
		Islas_Cook("CK"),
		Islas_Feroe("FO"),
		Islas_Georgias_del_Sur_y_Sandwich_del_Sur("GS"),
		Islas_Heard_y_McDonald("HM"),
		Islas_Malvinas("FK"),
		Islas_Marianas_del_Norte("MP"),
		Islas_Marshall("MH"),
		Islas_Pitcairn("PN"),
		Islas_Salomon("SB"),
		Islas_Turcas_y_Caicos("TC"),
		Islas_Ultramarinas_de_Estados_Unidos("UM"),
		Islas_Virgenes_Britanicas("VG"),
		Islas_Virgenes_de_los_Estados_Unidos("VI"),
		Israel("IL"),
		Italia("IT"),
		Jamaica("JM"),
		Japon("JP"),
		Jersey("JE"),
		Jordania("JO"),
		Kazajistan("KZ"),
		Kenia("KE"),
		Kirguistan("KG"),
		Kiribati("KI"),
		Kuwait("KW"),
		Laos("LA"),
		Lesoto("LS"),
		Letonia("LV"),
		Libano("LB"),
		Liberia("LR"),
		Libia("LY"),
		Liechtenstein("LI"),
		Lituania("LT"),
		Luxemburgo("LU"),
		Macao("MO"),
		Macedonia("MK"),
		Madagascar("MG"),
		Malasia("MY"),
		Malaui("MW"),
		Maldivas("MV"),
		Mali("ML"),
		Malta("MT"),
		Marruecos("MA"),
		Martinica("MQ"),
		Mauricio("MU"),
		Mauritania("MR"),
		Mayotte("YT"),
		Mexico("MX"),
		Micronesia("FM"),
		Moldavia("MD"),
		Monaco("MC"),
		Mongolia("MN"),
		Montenegro("[ME]"),
		Montserrat("MS"),
		Mozambique("MZ"),
		Myanmar("MM"),
		Namibia("NA"),
		Nauru("NR"),
		Nepal("NP"),
		Nicaragua("NI"),
		Niger("NE"),
		Nigeria("NG"),
		Niue("NU"),
		Norfolk("NF"),
		Noruega("NO"),
		Nueva_Caledonia("NC"),
		Nueva_Zelanda("NZ"),
		Oman("OM"),
		Paises_Bajos("NL"),
		Pakistan("PK"),
		Palaos("PW"),
		Palestina("PS"),
		Panama("PA"),
		Papua_Nueva_Guinea("PG"),
		Paraguay("PY"),
		Peru("PE"),
		Polinesia_Francesa("PF"),
		Polonia("PL"),
		Portugal("PT"),
		Puerto_Rico("PR"),
		Reino_Unido("GB"),
		Republica_Arabe_Saharaui_Democratica("EH"),
		Republica_Centroafricana("CF"),
		Republica_Checa("CZ"),
		Republica_del_Congo("CG"),
		Republica_Democratica_del_Congo("CD"),
		Republica_Dominicana("[DO]"),
		Reunion("RE"),
		Ruanda("RW"),
		Rumania("RO"),
		Rusia("RU"),
		Samoa("WS"),
		Samoa_Americana("[AS]"),
		San_Bartolome("BL"),
		San_Cristobal_y_Nieves("KN"),
		San_Marino("SM"),
		San_Martin("MF"),
		San_Pedro_y_Miquelon("PM"),
		San_Vicente_y_las_Granadinas("VC"),
		Santa_Elena_Ascension_y_Tristan_de_Acuna("SH"),
		Santa_Lucia("LC"),
		Santo_Tome_y_Principe("ST"),
		Senegal("SN"),
		Serbia("RS"),
		Seychelles("SC"),
		Sierra_Leona("SL"),
		Singapur("SG"),
		Sint_Maarten("SX"),
		Siria("SY"),
		Somalia("SO"),
		Sri_Lanka("LK"),
		Suazilandia("SZ"),
		Sudafrica("ZA"),
		Sudan("SD"),
		Sudan_del_Sur("SS"),
		Suecia("SE"),
		Suiza("CH"),
		Surinam("SR"),
		Svalbard_y_Jan_Mayen("SJ"),
		Tailandia("TH"),
		Taiwan_Republica_de_China("TW"),
		Tanzania("TZ"),
		Tayikistan("TJ"),
		Territorio_Britanico_del_Oceano_Indico("IO"),
		Tierras_Australes_y_Antarticas_Francesas("TF"),
		Timor_Oriental("TL"),
		Togo("TG"),
		Tokelau("TK"),
		Tonga("[TO]"),
		Trinidad_y_Tobago("TT"),
		Tunez("TN"),
		Turkmenistan("TM"),
		Turquia("TR"),
		Tuvalu("TV"),
		Ucrania("UA"),
		Uganda("UG"),
		Uruguay("UY"),
		Uzbekistan("UZ"),
		Vanuatu("VU"),
		Ciudad_del_Vaticano("VA"),
		Venezuela("VE"),
		Vietnam("VN"),
		Wallis_y_Futuna("WF"),
		Yemen("YE"),
		Yibuti("DJ"),
		Zambia("ZM"),
		Zimbabue("ZW"),
		NoExisteCodigo("NoExisteCodigo");
		private final String text;

		private enumCodigoPais(final String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}
	}

	public final enumTipoApi getTipoApi() {
		return nTipoApi;
	}

	public final void setTipoApi(enumTipoApi value) {
		nTipoApi = value;
	}

	public final boolean NuevaFactura(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String MargenErrorMontosPermitido, ITAParsing_v0206.enumTipoDeNegocio TipoDeNegocioaux, String RutaCarpetaDebug) throws TAException, UnsupportedEncodingException {

		boolean nuevaFactura = false;
		try {
			//Inicializo variables de Error
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			enumTipoDeNegocio TipoDeNegocio = enumTipoDeNegocio.forValue(TipoDeNegocioaux.getValue());
			MargenErrorMontosPermitido = CNullStr(MargenErrorMontosPermitido).trim();
			if (RutaCarpetaDebug.trim().equals("")) {
				throw new TAException("No se ha configurado la ruta de la aplicacion");
			}
			if (RutaCarpetaDebug.charAt(RutaCarpetaDebug.length() - 1) != File.separatorChar) {
				RutaCarpetaDebug = RutaCarpetaDebug + File.separator;
			}

			objDebug.setRutaCarpetaDebug(RutaCarpetaDebug);
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {getNombreMetodo()};
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, MargenErrorMontosPermitido, TipoDeNegocio, RutaCarpetaDebug};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

			//Seteo margen para la factura
			MAXMargenErrorMontos = MargenErrorMontosPermitido;
			this.nTipoDeNegocio = TipoDeNegocio;
			//Inicializo el xml
			XMLFACTURAField = new XMLFACTURA();
			XMLFACTURAField.setDATOSADICIONALES(XMLFACTURAField.new clsDATOSADICIONALES());
			XMLFACTURAField.setDGI(XMLFACTURAField.new clsDGI());

			//Inicio Listas
			clsDGIXMLFacturaField = XMLFACTURAField.new clsDGI();
			clsDATOSADICIONALESXMLFacturaField = XMLFACTURAField.new clsDATOSADICIONALES();
			lstEmiTelefono = clsDGIXMLFacturaField.new EmiTelefonos();
			lstTOTRetencPercep = clsDGIXMLFacturaField.new TOTRetencPerceps();
			lstDetalle = clsDGIXMLFacturaField.new Detalle();
			lstSubTotInfo = clsDGIXMLFacturaField.new SubTotalInfos();
			lstDscRcgGlobal = clsDGIXMLFacturaField.new DscRcgGlobals();
			lstMediosPago = clsDGIXMLFacturaField.new MedioPagos();
			lstReferencia = clsDGIXMLFacturaField.new Referencias();

			listaTelefonosArray = new ArrayList<EMITelefono>();
			listaDetallesArray = new ArrayList<DETALLE>();
			listaSubTotInfoArray = new ArrayList<XMLFACTURA.clsDGI.SubTotalInfos.SubTotalInfo>();
			listaDscRcgGlobalArray = new ArrayList<XMLFACTURA.clsDGI.DscRcgGlobals.DscRcgGlobal>();
			listaMediosPagoArray = new ArrayList<XMLFACTURA.clsDGI.MedioPagos.MedioPago>();
			listaReferenciaArray = new ArrayList<XMLFACTURA.clsDGI.Referencias.Referencia>();
			listaTOTRetencPercepsArray = new ArrayList<TOTRetencPercep>();
			listDetalleCodItemArray = new ArrayList<XMLFACTURA.clsDGI.Detalle.DETCodItems.DETCodItem>();
			listDetalleSubDescuentoArray = new ArrayList<XMLFACTURA.clsDGI.Detalle.DETSubDescuentos.DETSubDescuento>();
			listDetalleSubRecargoArray = new ArrayList<XMLFACTURA.clsDGI.Detalle.DETSubRecargos.DETSubRecargo>();
			listaDetalleSubRetencPercepArray = new ArrayList<XMLFACTURA.clsDGI.Detalle.DETRetencPerceps.DETRetencPercep>();

			XMLFACTURAField.getDATOSADICIONALES().setTIPODENEGOCIO((short) TipoDeNegocio.getValue());
			nuevaFactura = true;
		} catch (TAException ex) {
			ErrorCod.argValue = 8;
			ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
			try {
				objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
			nuevaFactura = false;
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
		return nuevaFactura;

	}

	public final boolean FinalizarFactura(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, tangible.RefObject<String> XMLFactura) throws TAException {

		boolean finalizarFactura = false;
		try {
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "tangible.RefObject<String> XMLFactura"};
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, XMLFactura.argValue};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);
			//Valido que haya una factura iniciada
			FacturaIniciada();
			double MargenErrorPermitidoMontos = 0;
			String MargenErrorPermitidoMontosStr = null;
			MargenErrorPermitidoMontosStr = MAXMargenErrorMontos;
			if (isNumeric(MargenErrorPermitidoMontosStr) == true) {
				MargenErrorPermitidoMontos = Double.parseDouble(MargenErrorPermitidoMontosStr);
			}
			double MargenErrorPermitidoSumas = 0;
			//si es contingencia tienen que tener valores los field de contigencia y estar cargado
			if (XMLFACTURAField.EsContingencia() && ((XMLFACTURAField.getDATOSCONTINGENCIA() == null) || XMLFACTURAField.getDATOSCONTINGENCIA().getNRO() < 0)) {
				throw new TAException("Si el comprobante es de contingencia, debe invocar al metodo DATOSCONTIGENCIASetDatos", 4147);
			}
			//*** Validacion de Datos***
			if (XMLFACTURAField.getDATOSADICIONALES().getSOFTWAREFACTURADOR() == null) {
				throw new TAException("Antes de finalizar la factura debe invocar al metodo DATOSADICIONALESSetDatos", 4000);
			}
			//Valido que si es Freeshop se haya seteado el receptor
			if (XMLFACTURAField.getDGI().getRECTipoDocRecep() == enumTipoDocumentoDelReceptor.SinDefinir.getValue() && nTipoDeNegocio == enumTipoDeNegocio.TiendaFreeShop) {
				throw new TAException("Si es freeshop debe setear el receptor", 4001);
			}
			if (XMLFACTURAField.getDATOSADICIONALES().getCOTIZACIONUI() == 0) {
				throw new TAException("Debe ingresar la cotizacion UI en el metodo DATOSADICIONALESSetDatos", 4001);
			}

			//Valido si tope UI supera 1000 se haya seteado el receptor
			//para monedas distintas de pesos por eso multiplico por el tipo de cambio y que se haya seteado el receptor
			if (XMLFACTURAField.getDGI().getTOTTpoMoneda() != null) {
				if (!XMLFACTURAField.getDGI().getTOTTpoMoneda().trim().equals("UYU") && XMLFACTURAField.getDGI().getRECTipoDocRecep() == enumTipoDocumentoDelReceptor.SinDefinir.getValue() && (XMLFACTURAField.getDGI().getTOTMntTotal() * XMLFACTURAField.getDGI().getTOTTpoCambio() / XMLFACTURAField.getDATOSADICIONALES().getCOTIZACIONUI()) > 10000) {
					throw new TAException("El monto ingresado supera las 10000ui debe setear el receptor", 4001);
				} else if (XMLFACTURAField.getDGI().getRECTipoDocRecep() == enumTipoDocumentoDelReceptor.SinDefinir.getValue() && (XMLFACTURAField.getDGI().getTOTMntTotal() / XMLFACTURAField.getDATOSADICIONALES().getCOTIZACIONUI()) > 10000) {
					//Valido si tope UI supera 1000 se haya seteado el receptor
					throw new TAException("El monto ingresado supera las 10000ui debe setear el receptor", 4001);
				}
			}

			//Valido si el eTicket,eTicket Cuenta Ajena y sus Notas de correccion tienen retencion y percepcion se debe de indentificar al cliente
			if (XMLFACTURAField.getDGI().getRECTipoDocRecep() == enumTipoDocumentoDelReceptor.SinDefinir.getValue() && XMLFACTURAField.getDGI().getTOTMntTotRetenido() != 0 && XMLFACTURAField.getDGI().getIDDocSecProf() != 1) {
				throw new TAException("Si el comprobantes tiene retenciones y/o percepciones debe setear el receptor", 4001);
			}

			//Valido que Coincida la Cantidad de Lineas informada en los totales con las enviadas.
			if (XMLFACTURAField.getDGI().getDetalles() == null) {
				throw new TAException("Todos los comprobantes deben contener al menos 1 linea", 4002);
			} else {
				if (XMLFACTURAField.getDGI().getDetalles().getDetalle().size() == 0) {
					throw new TAException("Todos los comprobantes deben contener al menos 1 linea", 4003);
				} else {
					if (nTipoApi == enumTipoApi.TAFE) {
						if (XMLFACTURAField.getDGI().getDetalles().getDetalle().size() != XMLFACTURAField.getDGI().getTOTCantLinDet()) {
							throw new TAException("Cantidad de Lineas Recibidas no coincide con la Informada en los Totales(DGIA5setTotalesDeEncabezado). - Esperadas: " + XMLFACTURAField.getDGI().getTOTCantLinDet() + " Recibidas: " + XMLFACTURAField.getDGI().getDetalles().getDetalle().size(), 4004);
						}
					}
				}
			}

			//Si es eBoleta (NC/ND) y existe Total Monto IVA en suspenso el indicador monto bruto tiene que ser 2 (IMEBA y adicionales incluido)
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoleta.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
				if (XMLFACTURAField.getDGI().getIDDocMntBruto() != enumIndicadorMontosBrutos.IMEBAyAdicionalesIncluido.getValue() && XMLFACTURAField.getDGI().getTOTMntIVaenSusp() != 0) {
					throw new TAException("Si es eBoleta de entrada o sus notas de correcccion el Indicador monto bruto debe ser IMEBA y adicionales incluido");
				}
			}

			if (nTipoApi == enumTipoApi.TAFE) {
				//Si es devolucion obligo Informacion de Referencia
				if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eTicketNotaCred.getValue()
						|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eTicketNotaCredContingencia.getValue()
						|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eTicketNotaDeb.getValue()
						|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eTicketNotaDebContingencia.getValue()
						|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eFacturaNotaCred.getValue()
						|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eFacturaNotaCredContingencia.getValue()
						|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eFacturaNotaDeb.getValue()
						|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eFacturaNotaDebContingencia.getValue()
						|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
						|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
						|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
						|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {

					if (XMLFACTURAField.getDGI().getReferencias() == null) {
						throw new TAException("Las devoluciones deben contener al menos 1 linea de Referencia al comprobante original del que se esta devolviendo", 4005);
					} else {
						if (XMLFACTURAField.getDGI().getReferencias().getReferencia().size() == 0) {
							throw new TAException("Las devoluciones deben contener al menos 1 linea de Referencia al comprobante original del que se esta devolviendo", 4006);
						}
					}
				}
			}

			//Valido Coherencia de los montos totales y las lineas
			double sumaTotalMontoNoGravado = 0;
			double sumaTotalMontoExportacionYAsimiladas = 0;
			double sumaTotalMontoImpuestoPercibido = 0;
			double sumaTotalMontoNetoIvaEnSuspenso = 0;
			double sumaTotalMontoNetoIvaTasaMinima = 0;
			double sumaTotalMontoNetoIvaTasaBasica = 0;
			double sumaTotalMontoNetoIvaOtraTasa = 0;
			double sumaTotalMontoNoFacturable = 0;

			for (int i = 0; i < XMLFACTURAField.getDGI().getDetalles().getDetalle().size(); i++) {
				if ((XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETIndAgenteResp().equals("A") || XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETIndAgenteResp().equals("R"))) {
					if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eRemito.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eRemitoContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eResguardo.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eResguardoContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eFacturaExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue()) {
						throw new TAException("Agente Responsable no valido para tipo CFE", 4108);
					} else {
						if (XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETRetencPerceps() == null) {
							throw new TAException("La linea " + (i + 1) + " no tiene codigos de Retencion/Percepcion/Credito Fiscal pero tiene Indicador de Agente Responsable. - Indicador de Agente Responsable Recibido :" + XMLFACTURAField.getDGI().getDetalles().getDetalle().get(XMLFACTURAField.getDGI().getDetalles().getDetalle().size()).getDETIndAgenteResp(), 4108);
						}
					}
				}

				switch (XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETIndFact()) {
					case 1: //enumIndicidaorDeFacturacionDetalle.ExentoIVA
						sumaTotalMontoNoGravado += XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETMontoItem();
						break;
					case 10: //enumIndicidaorDeFacturacionDetalle.ExportacionYAsimiladas
						sumaTotalMontoExportacionYAsimiladas += XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETMontoItem();
						break;
					case 11: //enumIndicidaorDeFacturacionDetalle.ImpuestoPercibido
						sumaTotalMontoImpuestoPercibido += XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETMontoItem();
						break;
					case 12: //enumIndicidaorDeFacturacionDetalle.IVAEnSuspenso
						sumaTotalMontoNetoIvaEnSuspenso += XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETMontoItem();
						break;
					case 2: //enumIndicidaorDeFacturacionDetalle.TasaMinima
						sumaTotalMontoNetoIvaTasaMinima += XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETMontoItem();
						break;
					case 3: //enumIndicidaorDeFacturacionDetalle.TasaBasica
						sumaTotalMontoNetoIvaTasaBasica += XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETMontoItem();
						break;
					case 4: //enumIndicidaorDeFacturacionDetalle.OtraTasa
						sumaTotalMontoNetoIvaOtraTasa += XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETMontoItem();
						break;
					case 6: //enumIndicidaorDeFacturacionDetalle.NoFacturable
						sumaTotalMontoNoFacturable += XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETMontoItem();
						break;
					case 7: //enumIndicidaorDeFacturacionDetalle.NoFacturableNegativo
						sumaTotalMontoNoFacturable -= XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETMontoItem();
						break;
					case 8: //enumIndicidaorDeFacturacionDetalle.ItemARebajarEnRemito
						sumaTotalMontoExportacionYAsimiladas -= XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETMontoItem();
						break;
					case 15: //enumIndicadorDeFacturacionDetalle.ItemVendidoContribuyenteIMEBA:
						if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoleta.getValue()
								|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
								|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
								|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
								|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
								|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
							if (XMLFACTURAField.getDGI().getRECTipoDocRecep() != 2) {
								throw new TAException("En eBoleta si el indicador de facturacion es Item vendido por un contribuyente IMEBA el tipo de documento del receptor debe ser RUC.");
							} else {
								sumaTotalMontoNoGravado += XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETMontoItem();
							}
						}
						break;
					case 14:  //enumIndicadorDeFacturacionDetalle.ItemVendidoContribuyenteMonotributo:
						if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoleta.getValue()
								|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
								|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
								|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
								|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
								|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
							if (XMLFACTURAField.getDGI().getRECTipoDocRecep() != 2) {
								throw new TAException("En eBoleta si el indicador de facturacion es Item vendido por un contribuyente monotributo el tipo de documento del receptor debe ser RUC.");
							} else {
								sumaTotalMontoNoGravado += XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETMontoItem();
							}
						}
						break;
					case 13: // enumIndicadorDeFacturacionDetalle.ItemVendidoPorNoContribuyente:
						if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoleta.getValue()
								|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
								|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
								|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
								|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
								|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
							if (XMLFACTURAField.getDGI().getRECTipoDocRecep() == 2) {
								throw new TAException("En eBoleta si el indicador de facturacion es Item vendido por un no contribuyente el tipo de documento del receptor no puede ser RUC.");
							} else {
								sumaTotalMontoNoGravado += XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETMontoItem();
							}
						}
					default:
						if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacion.getValue()) {
							sumaTotalMontoExportacionYAsimiladas += XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETMontoItem();
						}
						break;
				}
			}

			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
				for (int i = 0; i < XMLFACTURAField.getDGI().getDetalles().getDetalle().size(); i++) {
					if (XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETIndFact() == enumIndicadorDeFacturacionDetalle.ItemARebajarEnRemito.getValue()) {
						sumaTotalMontoExportacionYAsimiladas -= XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETMontoItem();
					} else {
						sumaTotalMontoExportacionYAsimiladas += XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETMontoItem();
					}
				}
			}

			//Aplico descuentos y Recargos Globales
			if (XMLFACTURAField.getDGI().getDscRcgGlobales() != null) {
				for (int i = 0; i < XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().size(); i++) {
					switch (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGIndFactDR()) {
						case 1: //enumIndicadorDeFacturacionDescuentoGlobal.ExentoIVA:
							if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoMovDR().equals("D")) {
								if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoDR() == enumTipoDescuentoORecargoGlobal.Monto.getValue()) {
									sumaTotalMontoNoGravado = sumaTotalMontoNoGravado - XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
								} else {
									XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).setDRGValorDR(Math.round((sumaTotalMontoNoGravado * XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR() / 100) * Math.pow(10, 2)) / Math.pow(10, 2));
									sumaTotalMontoNoGravado = sumaTotalMontoNoGravado - XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
									//sumaTotalMontoNoGravado = sumaTotalMontoNoGravado - (sumaTotalMontoNoGravado * XMLFACTURAField.getDGI().DscRcgGlobales(i).DRGValorDR / 100)
								}
							} else {
								if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoDR() == enumTipoDescuentoORecargoGlobal.Monto.getValue()) {
									sumaTotalMontoNoGravado = sumaTotalMontoNoGravado + XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
								} else {
									XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).setDRGValorDR(Math.round((sumaTotalMontoNoGravado * XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR() / 100) * Math.pow(10, 2)) / Math.pow(10, 2));
									sumaTotalMontoNoGravado = sumaTotalMontoNoGravado + XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
									//sumaTotalMontoNoGravado = sumaTotalMontoNoGravado + (sumaTotalMontoNoGravado * XMLFACTURAField.getDGI().DscRcgGlobales(i).DRGValorDR / 100)
								}
							}
							break;
						case 6://enumIndicadorDeFacturacionDescuentoGlobal.NoFacturable:
							if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoMovDR().equals("D")) {
								if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoDR() == enumTipoDescuentoORecargoGlobal.Monto.getValue()) {
									sumaTotalMontoNoFacturable = sumaTotalMontoNoFacturable - XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
								} else {
									XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).setDRGValorDR(Math.round((sumaTotalMontoNoFacturable * XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR() / 100) * Math.pow(10, 2)) / Math.pow(10, 2));
									sumaTotalMontoNoFacturable = sumaTotalMontoNoFacturable - XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
									//sumaTotalMontoNoFacturable = sumaTotalMontoNoFacturable - (sumaTotalMontoNoFacturable * XMLFACTURAField.getDGI().DscRcgGlobales(i).DRGValorDR / 100)
								}
							} else {
								if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoDR() == enumTipoDescuentoORecargoGlobal.Monto.getValue()) {
									sumaTotalMontoNoFacturable = sumaTotalMontoNoFacturable + XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
								} else {
									XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).setDRGValorDR(Math.round((sumaTotalMontoNoFacturable * XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR() / 100) * Math.pow(10, 2)) / Math.pow(10, 2));
									sumaTotalMontoNoFacturable = sumaTotalMontoNoFacturable + XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
									//sumaTotalMontoNoFacturable = sumaTotalMontoNoFacturable + (sumaTotalMontoNoFacturable * XMLFACTURAField.getDGI().DscRcgGlobales(i).DRGValorDR / 100)
								}
							}
							break;
						case 7://enumIndicadorDeFacturacionDescuentoGlobal.NoFacturableNegativo:
							if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoMovDR().equals("D")) {
								if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoDR() == enumTipoDescuentoORecargoGlobal.Monto.getValue()) {
									sumaTotalMontoNoFacturable = sumaTotalMontoNoFacturable + XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
								} else {
									XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).setDRGValorDR(Math.round((sumaTotalMontoNoFacturable * XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR() / 100) * Math.pow(10, 2)) / Math.pow(10, 2));
									sumaTotalMontoNoFacturable = sumaTotalMontoNoFacturable + XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
									//sumaTotalMontoNoFacturable = sumaTotalMontoNoFacturable + (sumaTotalMontoNoFacturable * XMLFACTURAField.getDGI().DscRcgGlobales(i).DRGValorDR / 100)
								}
							} else {
								if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoDR() == enumTipoDescuentoORecargoGlobal.Monto.getValue()) {
									sumaTotalMontoNoFacturable = sumaTotalMontoNoFacturable - XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
								} else {
									XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).setDRGValorDR(Math.round((sumaTotalMontoNoFacturable * XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR() / 100) * Math.pow(10, 2)) / Math.pow(10, 2));
									sumaTotalMontoNoFacturable = sumaTotalMontoNoFacturable - XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
									//sumaTotalMontoNoFacturable = sumaTotalMontoNoFacturable - (sumaTotalMontoNoFacturable * XMLFACTURAField.getDGI().DscRcgGlobales(i).DRGValorDR / 100)
								}
							}
							break;
						case 4://enumIndicadorDeFacturacionDescuentoGlobal.OtraTasa:
							if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoMovDR().equals("D")) {
								if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoDR() == enumTipoDescuentoORecargoGlobal.Monto.getValue()) {
									sumaTotalMontoNetoIvaOtraTasa = sumaTotalMontoNetoIvaOtraTasa - XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
								} else {
									XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).setDRGValorDR(Math.round((sumaTotalMontoNetoIvaOtraTasa * XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR() / 100) * Math.pow(10, 2)) / Math.pow(10, 2));
									sumaTotalMontoNetoIvaOtraTasa = sumaTotalMontoNetoIvaOtraTasa - XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
									//sumaTotalMontoNetoIvaOtraTasa = sumaTotalMontoNetoIvaOtraTasa - (sumaTotalMontoNetoIvaOtraTasa * XMLFACTURAField.getDGI().DscRcgGlobales(i).DRGValorDR / 100)
								}
							} else {
								if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoDR() == enumTipoDescuentoORecargoGlobal.Monto.getValue()) {
									sumaTotalMontoNetoIvaOtraTasa = sumaTotalMontoNetoIvaOtraTasa + XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
								} else {
									XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).setDRGValorDR(Math.round((sumaTotalMontoNetoIvaOtraTasa * XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR() / 100) * Math.pow(10, 2)) / Math.pow(10, 2));
									sumaTotalMontoNetoIvaOtraTasa = sumaTotalMontoNetoIvaOtraTasa + XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
									//sumaTotalMontoNetoIvaOtraTasa = sumaTotalMontoNetoIvaOtraTasa + (sumaTotalMontoNetoIvaOtraTasa * XMLFACTURAField.getDGI().DscRcgGlobales(i).DRGValorDR / 100)
								}
							}
							break;
						case 3://enumIndicadorDeFacturacionDescuentoGlobal.TasaBasica:
							if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoMovDR().equals("D")) {
								if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoDR() == enumTipoDescuentoORecargoGlobal.Monto.getValue()) {
									sumaTotalMontoNetoIvaTasaBasica = sumaTotalMontoNetoIvaTasaBasica - XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
								} else {
									XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).setDRGValorDR(Math.round((sumaTotalMontoNetoIvaTasaBasica * XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR() / 100) * Math.pow(10, 2)) / Math.pow(10, 2));
									sumaTotalMontoNetoIvaTasaBasica = sumaTotalMontoNetoIvaTasaBasica - XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
									//sumaTotalMontoNetoIvaTasaBasica = sumaTotalMontoNetoIvaTasaBasica - (sumaTotalMontoNetoIvaTasaBasica * XMLFACTURAField.getDGI().DscRcgGlobales(i).DRGValorDR / 100)
								}
							} else {
								if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoDR() == enumTipoDescuentoORecargoGlobal.Monto.getValue()) {
									sumaTotalMontoNetoIvaTasaBasica = sumaTotalMontoNetoIvaTasaBasica + XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
								} else {
									XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).setDRGValorDR(Math.round((sumaTotalMontoNetoIvaTasaBasica * XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR() / 100) * Math.pow(10, 2)) / Math.pow(10, 2));
									sumaTotalMontoNetoIvaTasaBasica = sumaTotalMontoNetoIvaTasaBasica + XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
									//sumaTotalMontoNetoIvaTasaBasica = sumaTotalMontoNetoIvaTasaBasica + (sumaTotalMontoNetoIvaTasaBasica * XMLFACTURAField.getDGI().DscRcgGlobales(i).DRGValorDR / 100)
								}
							}
							break;
						case 2://enumIndicadorDeFacturacionDescuentoGlobal.TasaMinima:
							if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoMovDR().equals("D")) {
								if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoDR() == enumTipoDescuentoORecargoGlobal.Monto.getValue()) {
									sumaTotalMontoNetoIvaTasaMinima = sumaTotalMontoNetoIvaTasaMinima - XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
								} else {
									XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).setDRGValorDR(Math.round((sumaTotalMontoNetoIvaTasaMinima * XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR() / 100) * Math.pow(10, 2)) / Math.pow(10, 2));
									sumaTotalMontoNetoIvaTasaMinima = sumaTotalMontoNetoIvaTasaMinima - XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
									//sumaTotalMontoNetoIvaTasaMinima = sumaTotalMontoNetoIvaTasaMinima - (sumaTotalMontoNetoIvaTasaMinima * XMLFACTURAField.getDGI().DscRcgGlobales(i).DRGValorDR / 100)
								}
							} else {
								if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoDR() == enumTipoDescuentoORecargoGlobal.Monto.getValue()) {
									sumaTotalMontoNetoIvaTasaMinima = sumaTotalMontoNetoIvaTasaMinima + XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
								} else {
									XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).setDRGValorDR(Math.round((sumaTotalMontoNetoIvaTasaMinima * XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR() / 100) * Math.pow(10, 2)) / Math.pow(10, 2));
									sumaTotalMontoNetoIvaTasaMinima = sumaTotalMontoNetoIvaTasaMinima + XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
									//sumaTotalMontoNetoIvaTasaMinima = sumaTotalMontoNetoIvaTasaMinima + (sumaTotalMontoNetoIvaTasaMinima * XMLFACTURAField.getDGI().DscRcgGlobales(i).DRGValorDR / 100)
								}
							}

							break;
						case 10://enumIndicadorDeFacturacionDescuentoGlobal.ExportacionYAsimiladas:
							if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoMovDR().equals("D")) {
								if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoDR() == enumTipoDescuentoORecargoGlobal.Monto.getValue()) {
									sumaTotalMontoExportacionYAsimiladas = sumaTotalMontoExportacionYAsimiladas - XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
								}
							} else {
								if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoDR() == enumTipoDescuentoORecargoGlobal.Monto.getValue()) {
									sumaTotalMontoExportacionYAsimiladas = sumaTotalMontoExportacionYAsimiladas + XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
								}
							}
							break;
						case 11://enumIndicadorDeFacturacionDescuentoGlobal.ImpuestoPercibido:
							if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoMovDR().equals("D")) {
								if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoDR() == enumTipoDescuentoORecargoGlobal.Monto.getValue()) {
									sumaTotalMontoImpuestoPercibido = sumaTotalMontoImpuestoPercibido - XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
								}
							} else {
								if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoDR() == enumTipoDescuentoORecargoGlobal.Monto.getValue()) {
									sumaTotalMontoImpuestoPercibido = sumaTotalMontoImpuestoPercibido + XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
								}
							}
							break;
						case 12://enumIndicadorDeFacturacionDescuentoGlobal.IVAEnSuspenso:
							if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoMovDR().equals("D")) {
								if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoDR() == enumTipoDescuentoORecargoGlobal.Monto.getValue()) {
									sumaTotalMontoNetoIvaEnSuspenso = sumaTotalMontoNetoIvaEnSuspenso - XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
								}
							} else {
								if (XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGTpoDR() == enumTipoDescuentoORecargoGlobal.Monto.getValue()) {
									sumaTotalMontoNetoIvaEnSuspenso = sumaTotalMontoNetoIvaEnSuspenso + XMLFACTURAField.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(i).getDRGValorDR();
								}
							}
							break;
					}
				}
			}

			//Si tiene lineas con monto bruto(iva incluido saco los impuestos a la linea)
			if (XMLFACTURAField.getDGI().getIDDocMntBruto() == 1) {
				sumaTotalMontoNetoIvaTasaMinima = sumaTotalMontoNetoIvaTasaMinima / ((100 + XMLFACTURAField.getDGI().getTOTIVATasaMin()) / 100);
				sumaTotalMontoNetoIvaTasaBasica = sumaTotalMontoNetoIvaTasaBasica / ((100 + XMLFACTURAField.getDGI().getTOTIVATasaBasica()) / 100);
				sumaTotalMontoNetoIvaOtraTasa = sumaTotalMontoNetoIvaOtraTasa - XMLFACTURAField.getDGI().getTOTMntIVAOtra();
			}
			if (nTipoApi == enumTipoApi.TAFE) {
				//Totalizo y comparo con lo enviado por el Facturador
				double sumaTotMntIVATasaMin = 0;
				sumaTotMntIVATasaMin = Math.round((sumaTotalMontoNetoIvaTasaMinima * XMLFACTURAField.getDGI().getTOTIVATasaMin() / 100) * Math.pow(10, 2)) / Math.pow(10, 2);
				if (Math.abs(Math.round(XMLFACTURAField.getDGI().getTOTMntIVATasaMin() * Math.pow(10, 2)) / Math.pow(10, 2) - Math.round(sumaTotMntIVATasaMin * Math.pow(10, 2)) / Math.pow(10, 2)) > MargenErrorPermitidoMontos) {
					throw new TAException("Total de IVA Tasa Minima Supera el Margen de Error Permitido. - Esperado: " + String.valueOf(Math.round(sumaTotMntIVATasaMin * Math.pow(10, 2)) / Math.pow(10, 2)) + " Recibido: " + XMLFACTURAField.getDGI().getTOTMntIVATasaMin());
				}

				if (Math.abs(Math.round(XMLFACTURAField.getDGI().getTOTMntNetoIvaTasaMin() * Math.pow(10, 2)) / Math.pow(10, 2) - Math.round(sumaTotalMontoNetoIvaTasaMinima * Math.pow(10, 2)) / Math.pow(10, 2)) > MargenErrorPermitidoMontos) {
					throw new TAException("Total Monto Neto IVA Tasa Minima Supera el Margen de Error Permitido. - Esperado: " + Math.round(sumaTotalMontoNetoIvaTasaMinima * Math.pow(10, 2)) / Math.pow(10, 2) + " Recibido: " + XMLFACTURAField.getDGI().getTOTMntNetoIvaTasaMin());
				}

				double sumaTotMntIVATasaBasica = 0;
				sumaTotMntIVATasaBasica = Math.round((sumaTotalMontoNetoIvaTasaBasica * XMLFACTURAField.getDGI().getTOTIVATasaBasica() / 100) * Math.pow(10, 2)) / Math.pow(10, 2);
				if (Math.abs(Math.round(XMLFACTURAField.getDGI().getTOTMntIVATasaBasica() * Math.pow(10, 2)) / Math.pow(10, 2) - Math.round(sumaTotMntIVATasaBasica * Math.pow(10, 2)) / Math.pow(10, 2)) > MargenErrorPermitidoMontos) {
					throw new TAException("Total de IVA Tasa Basica Supera el Margen de Error Permitido. - Esperado: " + Math.round(sumaTotMntIVATasaBasica * Math.pow(10, 2)) / Math.pow(10, 2) + " Recibido: " + XMLFACTURAField.getDGI().getTOTMntIVATasaBasica());
				}

				if (Math.abs(Math.round(XMLFACTURAField.getDGI().getTOTMntNetoIVATasaBasica() * Math.pow(10, 2)) / Math.pow(10, 2) - Math.round(sumaTotalMontoNetoIvaTasaBasica * Math.pow(10, 2)) / Math.pow(10, 2)) > MargenErrorPermitidoMontos) {
					throw new TAException("Total Monto Neto IVA Tasa Basica Supera el Margen de Error Permitido. - Esperado: " + Math.round(sumaTotalMontoNetoIvaTasaBasica * Math.pow(10, 2)) / Math.pow(10, 2) + " Recibido: " + XMLFACTURAField.getDGI().getTOTMntNetoIVATasaBasica());
				}

				if (Math.abs(Math.round(XMLFACTURAField.getDGI().getTOTMntNetoIVAOtra() * Math.pow(10, 2)) / Math.pow(10, 2) - Math.round(sumaTotalMontoNetoIvaOtraTasa * Math.pow(10, 2)) / Math.pow(10, 2)) > MargenErrorPermitidoMontos) {
					throw new TAException("Total Monto Neto IVA Otra Tasa Supera el Margen de Error Permitido. - Esperado: " + Math.round(sumaTotalMontoNetoIvaOtraTasa * Math.pow(10, 2)) / Math.pow(10, 2) + " Recibido: " + XMLFACTURAField.getDGI().getTOTMntNetoIVAOtra());
				}

				if (Math.abs(Math.round(Math.abs(XMLFACTURAField.getDGI().getTOTMontoNF()) * Math.pow(10, 2)) / Math.pow(10, 2) - Math.round(Math.abs(sumaTotalMontoNoFacturable) * Math.pow(10, 2)) / Math.pow(10, 2)) > MargenErrorPermitidoSumas) {
					throw new TAException("Total Monto No Facturable Supera el Margen de Error Permitido. - Esperado: " + Math.round(sumaTotalMontoNoFacturable * Math.pow(10, 2)) / Math.pow(10, 2) + " Recibido: " + XMLFACTURAField.getDGI().getTOTMontoNF());
				}

                                 //TODO: Agregar validacion de MontoTotalExpyAsim, que coincida con la suma de los detalles en el xml
                                if (Math.abs(Math.round(Math.abs(XMLFACTURAField.getDGI().getTOTMntExpoyAsim()) * Math.pow(10, 2)) / Math.pow(10, 2) - Math.round(Math.abs(sumaTotalMontoExportacionYAsimiladas) * Math.pow(10, 2)) / Math.pow(10, 2)) > MargenErrorPermitidoSumas) {
					throw new TAException("Total Monto Exportacion y Asimiladas supera el margen de error permitido. - Esperado: " + Math.round(sumaTotalMontoExportacionYAsimiladas * Math.pow(10, 2)) / Math.pow(10, 2) + " Recibido: " + XMLFACTURAField.getDGI().getTOTMntExpoyAsim());
				}

                                
				double sumaTotMntTotal = 0;
				// Para no arrastrar errores, controlo directamente sobre los datos del facturador.
				sumaTotMntTotal = XMLFACTURAField.getDGI().getTOTMntNoGrvSpecified() ? XMLFACTURAField.getDGI().getTOTMntNoGrv() : 0;
				sumaTotMntTotal += XMLFACTURAField.getDGI().getTOTMntExpoyAsimSpecified() ? XMLFACTURAField.getDGI().getTOTMntExpoyAsim() : 0;
				sumaTotMntTotal += XMLFACTURAField.getDGI().getTOTMntImpuestoPercSpecified() ? XMLFACTURAField.getDGI().getTOTMntImpuestoPerc() : 0;
				sumaTotMntTotal += XMLFACTURAField.getDGI().getTOTMntIVaenSuspSpecified() ? XMLFACTURAField.getDGI().getTOTMntIVaenSusp() : 0;
				sumaTotMntTotal += XMLFACTURAField.getDGI().getTOTMntNetoIvaTasaMinSpecified() ? XMLFACTURAField.getDGI().getTOTMntNetoIvaTasaMin() : 0;
				sumaTotMntTotal += XMLFACTURAField.getDGI().getTOTMntNetoIVATasaBasicaSpecified() ? XMLFACTURAField.getDGI().getTOTMntNetoIVATasaBasica() : 0;
				sumaTotMntTotal += XMLFACTURAField.getDGI().getTOTMntNetoIVAOtraSpecified() ? XMLFACTURAField.getDGI().getTOTMntNetoIVAOtra() : 0;
				sumaTotMntTotal += XMLFACTURAField.getDGI().getTOTMntIVATasaMinSpecified() ? XMLFACTURAField.getDGI().getTOTMntIVATasaMin() : 0;
				sumaTotMntTotal += XMLFACTURAField.getDGI().getTOTMntIVATasaBasicaSpecified() ? XMLFACTURAField.getDGI().getTOTMntIVATasaBasica() : 0;
				sumaTotMntTotal += XMLFACTURAField.getDGI().getTOTMntIVAOtraSpecified() ? XMLFACTURAField.getDGI().getTOTMntIVAOtra() : 0;

				if (Math.abs(Math.round(XMLFACTURAField.getDGI().getTOTMntTotal() * Math.pow(10, 2)) / Math.pow(10, 2) - Math.round(sumaTotMntTotal * Math.pow(10, 2)) / Math.pow(10, 2)) > MargenErrorPermitidoSumas) {
					throw new TAException("Total Monto Total Supera el Margen de Error Permitido. - Esperado: " + Math.round(sumaTotMntTotal * Math.pow(10, 2)) / Math.pow(10, 2) + " Recibido: " + XMLFACTURAField.getDGI().getTOTMntTotal());
				}

				double sumaTotalMontoRetenido = 0;
				double sumaTotalMontoCreditoFiscal = 0;
				if (XMLFACTURAField.getDGI().getTOTRetencPerceps() != null) {
					java.util.HashMap<String, Double> CodigosRetencPercep = new java.util.HashMap<String, Double>();
					String CodigoRetencPercep = "";
					double ValorRetencPercep = 0;

					//Calculo mi propia lista de Codigos de Retencion/Percepcion para comparar contra la que me mando el facturador
					for (int i = 0; i < XMLFACTURAField.getDGI().getDetalles().getDetalle().size(); i++) {
						if (XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETRetencPerceps() != null) {
							for (int k = 0; k < XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETRetencPerceps().getDETRetencPercep().size(); k++) {
								CodigoRetencPercep = XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETRetencPerceps().getDETRetencPercep().get(k).getDETCodRet();
								ValorRetencPercep = Math.round((XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETRetencPerceps().getDETRetencPercep().get(k).getDETValRetPerc()) * Math.pow(10, 2)) / Math.pow(10, 2);

								if (ValorRetencPercep < 0) //El valor de una retencion en la linea nunca puede ser negativa
								{
									//TIRAR EXCEPTION NO PUEDE SER NEGATIVO EL VALOR DE LA RETENCION DE LA LINEA
									throw new TAException("Valor de la Retencion/Percepcion de la linea es negativo y debe ser positivo. - Esperado: " + ValorRetencPercep * -1 + " Recibido: " + ValorRetencPercep, 5011);
								}

								//Si es un item a ajustar en resguardo el valor es negativo
								if (XMLFACTURAField.getDGI().getDetalles().getDetalle().get(i).getDETIndFact() == enumIndicadorDeFacturacionDetalle.ItemAAjustarEnResguardo.getValue()) {
									ValorRetencPercep = ValorRetencPercep * -1;
								}

								if (CodigosRetencPercep.containsKey(CodigoRetencPercep)) {
									CodigosRetencPercep.put(CodigoRetencPercep, CodigosRetencPercep.get(CodigoRetencPercep) + ValorRetencPercep);
								} else {
									CodigosRetencPercep.put(CodigoRetencPercep, ValorRetencPercep);
								}
							}
						}
					}

					boolean EncontreRetencPercep = false;
					for (java.util.Map.Entry<String, Double> entry : CodigosRetencPercep.entrySet()) {
						EncontreRetencPercep = false;
						CodigoRetencPercep = entry.getKey();
						ValorRetencPercep = entry.getValue();

						for (int i = 0; i < XMLFACTURAField.getDGI().getTOTRetencPerceps().getOTRetencPercep().size(); i++) {
							if (CodigoRetencPercep.equals(XMLFACTURAField.getDGI().getTOTRetencPerceps().getOTRetencPercep().get(i).getTOTCodRet())) {
								EncontreRetencPercep = true;
								if (Math.abs(Math.round(ValorRetencPercep * Math.pow(10, 2)) / Math.pow(10, 2) - Math.round((XMLFACTURAField.getDGI().getTOTRetencPerceps().getOTRetencPercep().get(i).getTOTValRetPerc()) * Math.pow(10, 2)) / Math.pow(10, 2)) > MargenErrorPermitidoSumas) {
									//Error los montos son diferentes
									throw new TAException("Valor de la Retencion/Percepcion(Codigo:" + CodigoRetencPercep + ") del encabezado Supera el Margen de Error (" + Math.abs(Math.round(ValorRetencPercep * Math.pow(10, 2)) / Math.pow(10, 2) - Math.round((XMLFACTURAField.getDGI().getTOTRetencPerceps().getOTRetencPercep().get(i).getTOTValRetPerc()) * Math.pow(10, 2)) / Math.pow(10, 2)) + ") Permitido (" + MargenErrorPermitidoSumas + "). - Esperado: " + ValorRetencPercep + " Recibido: " + Math.round((XMLFACTURAField.getDGI().getTOTRetencPerceps().getOTRetencPercep().get(i).getTOTValRetPerc()) * Math.pow(10, 2)) / Math.pow(10, 2));
								}
							}
						}

						if (EncontreRetencPercep == false) {
							//ERROR NO ENCONTRE CODIGO DE RETENCION/PERCEPCION
							throw new TAException("No se encontro el Codigo " + CodigoRetencPercep + " en la tabla de las Retenciones/Percepciones del Encabezado, pero si estaba presente en la tabla del detalle.", 5013);
						}

						if (!CodigoRetencPercep.substring(0, 4).equals("2181")) {
							sumaTotalMontoRetenido += ValorRetencPercep;
						} else {
							sumaTotalMontoCreditoFiscal += ValorRetencPercep;
						}
					}

					if (Math.abs(Math.round(XMLFACTURAField.getDGI().getTOTMntTotRetenido() * Math.pow(10, 2)) / Math.pow(10, 2) - Math.round(sumaTotalMontoRetenido * Math.pow(10, 2)) / Math.pow(10, 2)) > MargenErrorPermitidoSumas) {
						throw new TAException("Total de Retencion o Percepcion Supera el Margen de Error Permitido. - Esperado: " + new BigDecimal(Math.round(sumaTotalMontoRetenido * Math.pow(10, 2)) / Math.pow(10, 2)).setScale(2, RoundingMode.UP).toString() + " Recibido: " + XMLFACTURAField.getDGI().getTOTMntTotRetenido());
					}

					if (XMLFACTURAField.getDGI().getTOTMntTotCredFisc() != 0) {
						if (Math.abs(Math.round(XMLFACTURAField.getDGI().getTOTMntTotCredFisc() * Math.pow(10, 2)) / Math.pow(10, 2) - Math.round(sumaTotalMontoCreditoFiscal * Math.pow(10, 2)) / Math.pow(10, 2)) > MargenErrorPermitidoSumas) {
							throw new TAException("Total de Creditos Fiscales Supera el Margen de Error Permitido. - Esperado: " + new BigDecimal(Math.round(sumaTotalMontoCreditoFiscal * Math.pow(10, 2)) / Math.pow(10, 2)).setScale(2, RoundingMode.UP).toString() + " Recibido: " + XMLFACTURAField.getDGI().getTOTMntTotCredFisc());
						}
					}
				} else if (XMLFACTURAField.getDGI().getTOTRetencPerceps() == null && (XMLFACTURAField.getDGI().getTOTMntTotRetenido() != 0 || XMLFACTURAField.getDGI().getTOTMntTotCredFisc() != 0)) {
					throw new TAException("Si ingresa total de Retencion/Percepcion o Credito Fiscal debe ingresar codigos de Retenciones/Percepciones o Credito Fiscal", 5003);
				}
			}

			//Calculo Total Monto a Pagar en dependencia de si es eBoleta o no
			double sumaTotMntTotalAPagar = 0;
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoleta.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
				sumaTotMntTotalAPagar = XMLFACTURAField.getDGI().getTOTMntTotal() - XMLFACTURAField.getDGI().getTOTMntTotRetenido() + XMLFACTURAField.getDGI().getTOTMontoNF();
			} else {
				sumaTotMntTotalAPagar = XMLFACTURAField.getDGI().getTOTMntTotal() + XMLFACTURAField.getDGI().getTOTMntTotRetenido() + XMLFACTURAField.getDGI().getTOTMontoNF() + XMLFACTURAField.getDGI().getTOTMntTotCredFisc();
			}
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemito.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
				if (nTipoApi == enumTipoApi.TAFE || XMLFACTURAField.getDGI().getEMIRUCEmisor().equals("219999830019")) {
					if (Math.abs(Math.round(XMLFACTURAField.getDGI().getTOTMntPagar() * Math.pow(10, 2)) / Math.pow(10, 2) - Math.round(sumaTotMntTotalAPagar * Math.pow(10, 2)) / Math.pow(10, 2)) > MargenErrorPermitidoSumas) {
						throw new TAException("Total Monto a Pagar Supera el Margen de Error Permitido. - Esperado: " + Math.round(sumaTotMntTotalAPagar * Math.pow(10, 2)) / Math.pow(10, 2) + " Recibido: " + XMLFACTURAField.getDGI().getTOTMntPagar());
					}
				}
			}

			XMLFactura.argValue = XMLFACTURAField.ToXML();
			finalizarFactura = true;
		} catch (TAException ex) {
			try {
				ErrorCod.argValue = 8;
				ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
				objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
				finalizarFactura = false;
				throw ex;
			} catch (UnsupportedEncodingException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (UnsupportedEncodingException ex) {
			throw new TAException(ex.getMessage());
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
		return finalizarFactura;
	}

	public final boolean DATOSADICIONALESsetDatos(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String SOFTWAREFACTURADOR, String VERSIONDESOFTWAREFACTURADOR, String EMPRESARAZONSOCIAL, long EMPRESARUT, int SUCURSALNRO, String SUCURSALNOMBRE, int CAJANRO, long CAJERONRO, String CAJERONOMBRE, String DOCUMENTOTIPO, String DOCUMENTOSERIE, long DOCUMENTONRO, long TRANSACCIONNRO, long CLIENTENRO, String CLIENTEDOC, String CLIENTERAZONSOCIAL, String CLIENTENOMBRE, String CLIENTEDIRECCION, String CLIENTETELEFONO, String CLIENTEEMAIL, String CLIENTEPAISNOM, long VENDEDORNRO, String VENDEDORNOMBRE, double VALORUNIDADINDEXADA) throws TAException {
		try {
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "String SOFTWAREFACTURADOR", "String VERSIONDESOFTWAREFACTURADOR", "String EMPRESARAZONSOCIAL", "long EMPRESARUT", "int SUCURSALNRO", "String SUCURSALNOMBRE", "int CAJANRO", "long CAJERONRO", "String CAJERONOMBRE", "String DOCUMENTOTIPO", "String DOCUMENTOSERIE", "long DOCUMENTONRO", "long TRANSACCIONNRO", "long CLIENTENRO", "String CLIENTEDOC", "String CLIENTERAZONSOCIAL", "String CLIENTENOMBRE", "String CLIENTEDIRECCION", "String CLIENTETELEFONO", "String CLIENTEEMAIL", "String CLIENTEPAISNOM", "long VENDEDORNRO", "String VENDEDORNOMBRE", "double VALORUNIDADINDEXADA"};
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, SOFTWAREFACTURADOR, VERSIONDESOFTWAREFACTURADOR, EMPRESARAZONSOCIAL, EMPRESARUT, SUCURSALNRO, SUCURSALNOMBRE, CAJANRO, CAJERONRO, CAJERONOMBRE, DOCUMENTOTIPO, DOCUMENTOSERIE, DOCUMENTONRO, TRANSACCIONNRO, CLIENTENRO, CLIENTEDOC, CLIENTERAZONSOCIAL, CLIENTENOMBRE, CLIENTEDIRECCION, CLIENTETELEFONO, CLIENTEEMAIL, CLIENTEPAISNOM, VENDEDORNRO, VENDEDORNOMBRE, VALORUNIDADINDEXADA};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

			//Valido que haya una factura iniciada
			FacturaIniciada();

			//UnidadIndexada es obligatoria
			if (VALORUNIDADINDEXADA == 0) {
				throw new TAException("Valor de la Unidad Indexada es Obligatorio.", 4025);
			}
			//Controlo que UI no sea mayor que 10 ya que probablemente se este pasando mal el valor
			if (VALORUNIDADINDEXADA > 10) {
				throw new TAException("Valor de la Unidad Indexada es demasiado grande, Recibido: " + VALORUNIDADINDEXADA, 4085);
			}
			//Controlo que UI no supere los 7 digitos
			if ((new Double(VALORUNIDADINDEXADA)).toString().replace(".", "").replace(",", "").length() > 7) {
				throw new TAException("Valor de la Unidad Indexada supera el largo maximo de 7 caracteres permitidos.", 4175);
			}

			//Seteo Nodo Datos Adicionales
			clsDATOSADICIONALESXMLFacturaField.setSOFTWAREFACTURADOR(CNullStr(SOFTWAREFACTURADOR));
			clsDATOSADICIONALESXMLFacturaField.setVERSIONDESOFTWAREFACTURADOR(CNullStr(VERSIONDESOFTWAREFACTURADOR));
			clsDATOSADICIONALESXMLFacturaField.setEMPRESARUC(String.format("%012d", new Long(EMPRESARUT)));   //COMPLETAR   .PadLeft(12, "0"));
			clsDATOSADICIONALESXMLFacturaField.setSUCURSALNRO(SUCURSALNRO);
			clsDATOSADICIONALESXMLFacturaField.setCAJANRO(CAJANRO);
			clsDATOSADICIONALESXMLFacturaField.setCAJERONRO(CAJERONRO);
			clsDATOSADICIONALESXMLFacturaField.setCAJERONOMBRE(CNullStr(CAJERONOMBRE));
			clsDATOSADICIONALESXMLFacturaField.setDOCUMENTOORIGINALTIPO(CNullStr(DOCUMENTOTIPO));
			clsDATOSADICIONALESXMLFacturaField.setDOCUMENTOORIGINALSERIE(CNullStr(DOCUMENTOSERIE));
			clsDATOSADICIONALESXMLFacturaField.setDOCUMENTOORIGINALNRO(DOCUMENTONRO);
			clsDATOSADICIONALESXMLFacturaField.setTRANSACCIONNRO(TRANSACCIONNRO);
			clsDATOSADICIONALESXMLFacturaField.setCLIENTENRO(CLIENTENRO);
			clsDATOSADICIONALESXMLFacturaField.setCLIENTERAZONSOCIAL(CNullStr(CLIENTERAZONSOCIAL));
			clsDATOSADICIONALESXMLFacturaField.setCLIENTENOMBRE(CNullStr(CLIENTENOMBRE));
			clsDATOSADICIONALESXMLFacturaField.setcLIENTEDIRECCION(CNullStr(CLIENTEDIRECCION));
			clsDATOSADICIONALESXMLFacturaField.setcLIENTETELEFONO(CNullStr(CLIENTETELEFONO));
			clsDATOSADICIONALESXMLFacturaField.setcLIENTEEMAIL(CNullStr(CLIENTEEMAIL));
			clsDATOSADICIONALESXMLFacturaField.setCLIENTEPAISNOM(CNullStr(CLIENTEPAISNOM));
			clsDATOSADICIONALESXMLFacturaField.setVENDEDORNRO(VENDEDORNRO);
			clsDATOSADICIONALESXMLFacturaField.setVENDEDORNOMBRE(CNullStr(VENDEDORNOMBRE));
			clsDATOSADICIONALESXMLFacturaField.setTIPODENEGOCIO((short) nTipoDeNegocio.getValue());
			clsDATOSADICIONALESXMLFacturaField.setCOTIZACIONUI(VALORUNIDADINDEXADA);
			if (isNumeric(CNullStr(CLIENTEDOC))) {
				clsDATOSADICIONALESXMLFacturaField.setCLIENTEDOCUMENTO(CLIENTEDOC.trim());
			} else {
				clsDATOSADICIONALESXMLFacturaField.setCLIENTEDOCUMENTO(CNullStr(CLIENTEDOC));
			}
			XMLFACTURAField.setDATOSADICIONALES(clsDATOSADICIONALESXMLFacturaField);
			return true;
		} catch (TAException ex) {
			try {
				ErrorCod.argValue = 8;
				ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
				objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
				return false;
			} catch (UnsupportedEncodingException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (UnsupportedEncodingException ex) {
			throw new TAException(ex.getMessage());
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
	}

	public final boolean DATOSCONTINGENCIAsetDatos(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, int TIPOCFE, String SERIECOMPROBANTE, long NROCOMPROBANTE, long CAENROAUTORIZACION, long CAENRODESDE, long CAENROHASTA, Date CAEFECVENC) throws TAException {

		boolean datosContingencia = false;
		try {
			XMLFACTURAField.setDATOSCONTINGENCIA(XMLFACTURAField.new clsDATOSCONTINGENCIA());
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "int TIPOCFE", "String SERIECOMPROBANTE", "long NROCOMPROBANTE", "long CAENROAUTORIZACION", "long CAENRODESDE", "long CAENROHASTA", "java.util.Date CAEFECVENC"};
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, TIPOCFE, SERIECOMPROBANTE, NROCOMPROBANTE, CAENROAUTORIZACION, CAENRODESDE, CAENROHASTA, CAEFECVENC};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

			//*** Validacion de Datos***
			//Verifica que comprobante sea de contingencia
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == 0) {
				throw new TAException("Debe invocar primero al metodo DGIA1setEncabezado.", 4283);
			}

			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != TIPOCFE) {
				throw new TAException("El tipo de comprobante debe ser el mismo que se ingreso en DGIA1setEncabezado. DGIA1setEncabezado: " + XMLFACTURAField.getDGI().getIDDocTipoCFE() + " DATOSCONTINGENCIAsetDatos: " + TIPOCFE, 4282);
			}

			if (!XMLFACTURAField.EsContingencia()) {
				throw new TAException("El comprobante debe ser de contingencia.", 4006);
			}

			if (SERIECOMPROBANTE == null) {
				throw new TAException("En los comprobantes de contingencia es obligatorio ingresar Serie y Nro. de Comprobante.", 4007);
			} else {
				if ((SERIECOMPROBANTE.trim() == null || SERIECOMPROBANTE.trim().length() == 0) || NROCOMPROBANTE == 0 || CAENROAUTORIZACION == 0 || CAENRODESDE == 0) {
					throw new TAException("En los comprobantes de contingencia es obligatorio ingresar Serie, Nro. de Comprobante, CAE Nro., CAE Desde, CAE Hasta, CAE Fecha Vencimiento.", 4008);
				}
			}

			if ((new Long(CAENROAUTORIZACION)).toString().length() > 11) {
				throw new TAException("El Numero de Autorizacion supera los 11 digitos permitidos.", 4305);
			}

			clsDATOSCONTINGENCIAXMLFacturaField = XMLFACTURAField.new clsDATOSCONTINGENCIA();
			clsDATOSCONTINGENCIAXMLFacturaField.setTIPOCFE((short) TIPOCFE);
			clsDATOSCONTINGENCIAXMLFacturaField.setSERIE(LargoMax(SERIECOMPROBANTE, 2).toUpperCase().trim());//Solo deberia usarlo cuando es contingencia, sino me lo da el gateway
			clsDATOSCONTINGENCIAXMLFacturaField.setNRO((int) NROCOMPROBANTE); //Solo deberia usarlo cuando es contingencia, sino me lo da el gateway
			clsDATOSCONTINGENCIAXMLFacturaField.setCAENROAUTORIZACION(CAENROAUTORIZACION);
			clsDATOSCONTINGENCIAXMLFacturaField.setCAENRODESDE(CAENRODESDE);
			clsDATOSCONTINGENCIAXMLFacturaField.setCAENROHASTA(CAENROHASTA);
			clsDATOSCONTINGENCIAXMLFacturaField.setCAEVENCIMIENTO(CAEFECVENC);
			clsDATOSCONTINGENCIAXMLFacturaField.setCAEVENCIMIENTOSpecified(true);
			XMLFACTURAField.setDATOSCONTINGENCIA(clsDATOSCONTINGENCIAXMLFacturaField);
			datosContingencia = true;
		} catch (TAException ex) {
			try {
				ErrorCod.argValue = 8;
				ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
				objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
				datosContingencia = false;
				throw ex;
			} catch (UnsupportedEncodingException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (UnsupportedEncodingException ex) {
			throw new TAException(ex.getMessage());
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
		return datosContingencia;
	}

	public final boolean DGIA1setEncabezado(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, ITAParsing_v0206.enumTipoDeComprobanteCFE TipoCFEaux,
			Date FechaDeComprobante, ITAParsing_v0206.enumIndicadorTipoTraslado IndicadorTipoTrasladoaux, Date PeriodoDesde, Date PeriodoHasta,
			ITAParsing_v0206.enumIndicadorMontosBrutos IndicadorMontosBrutosaux, ITAParsing_v0206.enumFormaDePago FormaDePagoaux, Date FechaDeVencimiento, String ClausulaDeVenta,
			ITAParsing_v0206.enumModalidadDeVenta ModalidadDeVentaaux, ITAParsing_v0206.enumViaDeTransporte ViaDeTransporteaux, String InfoAdicionalDoc, ITAParsing_v0206.enumIVAalDia IVAalDiaAux,
			ITAParsing_v0206.enumSecretoProf SecretoProfesionalAux, ITAParsing_v0206.enumIndicadorPropiedadMercaderiaTransportada IndicadorPropMercaderiaTranspAux,
			ITAParsing_v0206.enumTipoDocumentoDelReceptor TipoDocumentoMercaderiaTranspAux, String CodigoPais, String NroDocPropietarioUrugMercadTransp, String NroDocPropietarioExtMercadTransp,
			String RazonSocPropietarioMercadTransp) throws TAException {

		boolean DGIA1 = false;
		try {
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			enumIndicadorTipoTraslado IndicadorTipoTraslado = enumIndicadorTipoTraslado.forValue(IndicadorTipoTrasladoaux.getValue());
			enumTipoDeComprobanteCFE TipoCFE = enumTipoDeComprobanteCFE.forValue(TipoCFEaux.getValue());
			enumModalidadDeVenta ModalidadDeVenta = enumModalidadDeVenta.forValue(ModalidadDeVentaaux.getValue());
			enumViaDeTransporte ViaDeTransporte = enumViaDeTransporte.forValue(ViaDeTransporteaux.getValue());
			enumIndicadorMontosBrutos IndicadorMontosBrutos = enumIndicadorMontosBrutos.forValue(IndicadorMontosBrutosaux.getValue());
			enumFormaDePago FormaDePago = enumFormaDePago.forValue(FormaDePagoaux.getValue());
			enumIVAalDia IVAalDia = enumIVAalDia.forValue(IVAalDiaAux.getValue());
			enumSecretoProf SecretoProfesional = enumSecretoProf.forValue(SecretoProfesionalAux.getValue());
			enumIndicadorPropiedadMercaderiaTransportada IndicadorPropMercaderiaTransp = enumIndicadorPropiedadMercaderiaTransportada.forValue(IndicadorPropMercaderiaTranspAux.getValue());
			enumTipoDocumentoDelReceptor TipoDocumentoMercaderiaTransp = enumTipoDocumentoDelReceptor.forValue(TipoDocumentoMercaderiaTranspAux.getValue());
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "ITAParsing_v0206.enumTipoDeComprobanteCFE TipoCFE",
				"java.util.Date FechaDeComprobante", "ITAParsing_v0206.enumIndicadorTipoTraslado IndicadorTipoTraslado", "java.util.Date PeriodoDesde", "java.util.Date PeriodoHasta",
				"ITAParsing_v0206.enumIndicadorMontosBrutos IndicadorMontosBrutos", "ITAParsing_v0206.enumFormaDePago FormaDePago", "java.util.Date FechaDeVencimiento",
				"String ClausulaDeVenta", "ITAParsing_v0206.enumModalidadDeVenta ModalidadDeVenta", "ITAParsing_v0206.enumViaDeTransporte ViaDeTransporte", "String InfoAdicionalDoc",
				"ITAParsing_v0206.enumIVAalDia IVAalDia", "ITAParsing_v0206.enumSecretoProf SecretoProfesional",
				"ITAParsing_v0206.enumIndicadorPropiedadMercaderiaTransportada IndicadorPropMercaderiaTransp",
				"ITAParsing_v0206.enumTipoDocumentoDelReceptor TipoDocumentoMercaderiaTransp", "String CodigoPais", "String NroDocPropietarioUrugMercadTransp",
				"String NroDocPropietarioExtMercadTransp", " String RazonSocPropietarioMercadTransp"};
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, TipoCFE, FechaDeComprobante, IndicadorTipoTraslado, PeriodoDesde, PeriodoHasta, IndicadorMontosBrutos,
				FormaDePago, FechaDeVencimiento, ClausulaDeVenta, ModalidadDeVenta, ViaDeTransporte, InfoAdicionalDoc, IVAalDia, SecretoProfesional, IndicadorPropMercaderiaTransp, 
				TipoDocumentoMercaderiaTransp, CodigoPais, NroDocPropietarioUrugMercadTransp, NroDocPropietarioExtMercadTransp, RazonSocPropietarioMercadTransp};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

			//Valido que haya una factura iniciada
			FacturaIniciada();

			ClausulaDeVenta = CNullStr(ClausulaDeVenta).trim();

			if (enumTipoDeComprobanteCFE.forValue(TipoCFE.getValue()) != TipoCFE) {
				throw new TAException("Tipo de comprobante invalido - Recibido :" + TipoCFE.toString(), 4161);
			}
			if (enumIndicadorTipoTraslado.forValue(IndicadorTipoTraslado.getValue()) != IndicadorTipoTraslado) {
				throw new TAException("Indicador tipo de translado invalido - Recibido :" + IndicadorTipoTraslado.toString(), 4162);
			}
			if (enumIndicadorMontosBrutos.forValue(IndicadorMontosBrutos.getValue()) != IndicadorMontosBrutos) {
				throw new TAException("Indicador de monto bruto invalido - Recibido :" + IndicadorMontosBrutos.toString(), 4163);
			}
			if (enumFormaDePago.forValue(FormaDePago.getValue()) != FormaDePago) {
				throw new TAException("Forma de pago invalido - Recibido :" + FormaDePago.toString(), 4164);
			}
			if (enumModalidadDeVenta.forValue(ModalidadDeVenta.getValue()) != ModalidadDeVenta) {
				throw new TAException("Modalidad de venta invalido - Recibido :" + ModalidadDeVenta.toString(), 4165);
			}
			if (enumViaDeTransporte.forValue(ViaDeTransporte.getValue()) != ViaDeTransporte) {
				throw new TAException("Via de transporte invalido - Recibido :" + ViaDeTransporte.toString(), 4166);
			}
			if (enumIVAalDia.forValue(IVAalDia.getValue()) != IVAalDia) {
				throw new TAException("IVA al dia recibido invalido - Recibido :" + IVAalDia.toString(), 4166);
			}
			if (enumSecretoProf.forValue(SecretoProfesional.getValue()) != SecretoProfesional) {
				throw new TAException("Secreto profesional recibido invalido - Recibido :" + SecretoProfesional.toString(), 4166);
			}
			if (enumIndicadorPropiedadMercaderiaTransportada.forValue(IndicadorPropMercaderiaTransp.getValue()) != IndicadorPropMercaderiaTransp) {
				throw new TAException("indicador propiedad mercaderia transportada recibido invalido - Recibido :" + IndicadorPropMercaderiaTransp.toString(), 4166);
			}
			if (enumTipoDocumentoDelReceptor.forValue(TipoDocumentoMercaderiaTransp.getValue()) != TipoDocumentoMercaderiaTransp) {
				throw new TAException("Tipo documento receptor mercaderia transportada recibido invalido - Recibido :" + TipoDocumentoMercaderiaTransp.toString(), 4166);
			}

			//Fecha de Comprobante debe ser mayor a 01/10/2011
			if (FechaDeComprobante.compareTo(new Date("2011/10/01")) < 0) {
				throw new TAException("Fecha del Comprobante debe ser mayor a 01/10/2011 - Recibida :" + FechaDeComprobante.toString(), 4009);
			}
			//Fecha de Comprobante debe ser menor a 31/12/2060
			if (FechaDeComprobante.compareTo(new Date("2060/12/31")) > 0) {
				throw new TAException("Fecha del Comprobante debe ser menor a 31/12/2060 - Recibida :" + FechaDeComprobante.toString(), 4009);
			}

			if (nTipoApi == enumTipoApi.TAFE) {
				if (TipoCFE == enumTipoDeComprobanteCFE.eRemito || TipoCFE == enumTipoDeComprobanteCFE.eRemitoContingencia || TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacion || TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia) {
					if (IndicadorTipoTraslado != enumIndicadorTipoTraslado.TransladoInterno && IndicadorTipoTraslado != enumIndicadorTipoTraslado.Venta) {
						throw new TAException("Indicador Tipo Translado es obligatorio para Remitos - Recibida :" + IndicadorTipoTraslado.toString(), 4010);
					}

					if (IndicadorPropMercaderiaTransp == enumIndicadorPropiedadMercaderiaTransportada.MercaderiaTerceros) {
						clsDGIXMLFacturaField.setIDDocIndPropiedadSpecified(true);
						clsDGIXMLFacturaField.setIDDocIndPropiedad((short) IndicadorPropMercaderiaTransp.getValue());

						if (RazonSocPropietarioMercadTransp == null || RazonSocPropietarioMercadTransp.equals("")) {
							throw new TAException("Si se traslada mercaderia de terceros es obligatorio el nombre o denominacion del propietario mercaderia transportada", 4010);
						} else {
							clsDGIXMLFacturaField.setIDDocRznSocPropSpecified(true);
							clsDGIXMLFacturaField.setIDDocRznSocProp(LargoMax(RazonSocPropietarioMercadTransp, 150));
						}

						if (TipoDocumentoMercaderiaTransp == enumTipoDocumentoDelReceptor.SinDefinir) {
							throw new TAException("Si se traslada mercaderia de terceros es obligatorio el codigo tipo documento - Recibido : " + TipoDocumentoMercaderiaTransp.toString(), 4010);
						} else {
							//si el tipo de documento no es sin definir quiere decir que esta correcto y se esta trasladando mercaderia de terceros
							clsDGIXMLFacturaField.setIDDocTipoDocPropSpecified(true);
							clsDGIXMLFacturaField.setIDDocTipoDocProp((short) TipoDocumentoMercaderiaTransp.getValue());

							if (TipoDocumentoMercaderiaTransp == enumTipoDocumentoDelReceptor.CI || TipoDocumentoMercaderiaTransp == enumTipoDocumentoDelReceptor.RUC) {
								if (!enumCodigoPais.Uruguay.toString().equals(CodigoPais)) {
									throw new TAException("Si el tipo documento es RUC o CI el CodigoPais debe ser UY - Recibido : " + CodigoPais.toString(), 4010);
								} else {
									//Si es RUC lo valido
									if ((TipoDocumentoMercaderiaTransp == enumTipoDocumentoDelReceptor.RUC) && !NroDocPropietarioUrugMercadTransp.equals("")) {
										NroDocPropietarioUrugMercadTransp = String.format("%012d", Long.parseLong(NroDocPropietarioUrugMercadTransp)).replace(' ', '0');
										if (!(VerificacionRUT(NroDocPropietarioUrugMercadTransp))) {
											throw new TAException("RUT del propietario uruguayo mercaderia transportada invalido - Recibido: " + NroDocPropietarioUrugMercadTransp.toString(), 1007);
										}
									}

									//Si es CI la valido
									if ((TipoDocumentoMercaderiaTransp == enumTipoDocumentoDelReceptor.CI) && !NroDocPropietarioUrugMercadTransp.equals("")) {
										NroDocPropietarioUrugMercadTransp = NroDocPropietarioUrugMercadTransp.replace("-", "");
										NroDocPropietarioUrugMercadTransp = NroDocPropietarioUrugMercadTransp.replace(".", "");
										NroDocPropietarioUrugMercadTransp = NroDocPropietarioUrugMercadTransp.replace(" ", "");
										if (!(VerificacionCedula(NroDocPropietarioUrugMercadTransp.toString()))) {
											throw new TAException("Cedula del propietario uruguayo mercaderia transportada invalida - Recibido: " + NroDocPropietarioUrugMercadTransp.toString(), 1008);
										}
									}

									clsDGIXMLFacturaField.setIDDocCodPaisPropSpecified(true);
									clsDGIXMLFacturaField.setIDDocDocPropSpecified(true);
									clsDGIXMLFacturaField.setIDDocCodPaisProp(CodigoPais);
									clsDGIXMLFacturaField.setIDDocDocProp(LargoMax(NroDocPropietarioUrugMercadTransp, 12));
								}
							} else if (TipoDocumentoMercaderiaTransp == enumTipoDocumentoDelReceptor.DNI) {
								if (!enumCodigoPais.Brasil.toString().equals(CodigoPais) && !enumCodigoPais.Argentina.toString().equals(CodigoPais) && !enumCodigoPais.Colombia.toString().equals(CodigoPais) && !enumCodigoPais.Paraguay.toString().equals(CodigoPais)) {
									throw new TAException("Si el tipo documento es DNI el CodigoPais debe ser AR, BR, CL o PY - Recibido : " + CodigoPais.toString(), 4010);
								} else {
									clsDGIXMLFacturaField.setIDDocCodPaisPropSpecified(true);
									clsDGIXMLFacturaField.setIDDocDocPropExtSpecified(true);
									clsDGIXMLFacturaField.setIDDocCodPaisProp(CodigoPais);
									clsDGIXMLFacturaField.setIDDocDocPropExt(LargoMax(NroDocPropietarioExtMercadTransp, 20));
								}
							} else {
								//el tipo documento es Otros, Pasaporte o NIFE
								clsDGIXMLFacturaField.setIDDocCodPaisPropSpecified(true);
								clsDGIXMLFacturaField.setIDDocDocPropExtSpecified(true);
								clsDGIXMLFacturaField.setIDDocCodPaisProp(CodigoPais);
								clsDGIXMLFacturaField.setIDDocDocPropExt(LargoMax(NroDocPropietarioExtMercadTransp, 20));
							}
						}
					}
				} else {
					clsDGIXMLFacturaField.setIDDocTipoDocPropSpecified(false);
					clsDGIXMLFacturaField.setIDDocIndPropiedadSpecified(false);
					clsDGIXMLFacturaField.setIDDocRznSocPropSpecified(false);
					clsDGIXMLFacturaField.setIDDocCodPaisPropSpecified(false);
					clsDGIXMLFacturaField.setIDDocCodPaisPropSpecified(false);
					clsDGIXMLFacturaField.setIDDocDocPropSpecified(false);
				}
			}

			//*** Fin Validacion de Datos ***
			clsDGIXMLFacturaField.setIDDocTipoCFE((short) TipoCFE.getValue());
			clsDGIXMLFacturaField.setIDDocFchEmis(FechaDeComprobante);

			//El tipo de translado de bienes es obligatorio en remitos, en el resto no corresponde
			if (TipoCFE == enumTipoDeComprobanteCFE.eRemito || TipoCFE == enumTipoDeComprobanteCFE.eRemitoContingencia || TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacion || TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia) {
				clsDGIXMLFacturaField.setIDDocTipoTrasladoSpecified(true);
			} else {
				clsDGIXMLFacturaField.setIDDocTipoTrasladoSpecified(false);
			}
			clsDGIXMLFacturaField.setIDDocTipoTraslado((short) IndicadorTipoTraslado.getValue());

			//Periodo desde, Periodo hasta y fecha de vencimiento no corresponde en remitos y resguardos
			if (TipoCFE == enumTipoDeComprobanteCFE.eRemito || TipoCFE == enumTipoDeComprobanteCFE.eRemitoContingencia || TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacion || TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia || TipoCFE == enumTipoDeComprobanteCFE.eResguardo || TipoCFE == enumTipoDeComprobanteCFE.eResguardoContingencia) {
				clsDGIXMLFacturaField.setIDDocPeriodoDesdeSpecified(false);
				clsDGIXMLFacturaField.setIDDocPeriodoHastaSpecified(false);
			} else {
				//Si PeriodoDesde es menor que minvalue(.net) o MinFecha(hecho para compatibilidad con vb6 #1/1/0100#) no va en el mensaje xml
				if (PeriodoDesde != null && PeriodoDesde.compareTo(new Date(0)) != 0 && !MinFecha.equals(PeriodoDesde)) {
					if (PeriodoDesde.compareTo(new Date("2000/01/01")) < 0) {
						throw new TAException("Fecha del Periodo Desde debe ser mayor a 01/01/2000 - Recibida :" + PeriodoDesde.toString(), 4009);
					}
					if (PeriodoDesde.compareTo(new Date("2060/12/31")) > 0) {
						throw new TAException("Fecha del Periodo Desde debe ser menor a 31/12/2060 - Recibida :" + PeriodoDesde.toString(), 4009);
					}
					clsDGIXMLFacturaField.setIDDocPeriodoDesde(PeriodoDesde);
					clsDGIXMLFacturaField.setIDDocPeriodoDesdeSpecified(true);
				} else {
					clsDGIXMLFacturaField.setIDDocPeriodoDesdeSpecified(false);
				}

				//Si PeriodoHasta es menor que minvalue() o MinFecha no va en el mensaje xml
				if (PeriodoHasta != null && PeriodoHasta.compareTo(new Date(0)) != 0 && !MinFecha.equals(PeriodoHasta)) {
					//Valida que PeriodoHasta no sea menor que PeriodoDesde
					if (PeriodoHasta.compareTo(new Date("2000/01/01")) < 0) {
						throw new TAException("Fecha del Periodo Hasta debe ser mayor a 01/01/2000 - Recibida :" + PeriodoHasta.toString(), 4009);
					}
					if (PeriodoHasta.compareTo(new Date("2060/12/31")) > 0) {
						throw new TAException("Fecha del Periodo Hasta debe ser menor a 31/12/2060 - Recibida :" + PeriodoHasta.toString(), 4009);
					}
					if (PeriodoHasta.compareTo(PeriodoDesde) < 0) {
						throw new TAException("PeriodoHasta no puede ser menor al PeriodoDesde - PeriodoHasta: " + PeriodoHasta.toString() + " - PeriodoDesde: " + PeriodoDesde.toString(), 4011);
					}
					clsDGIXMLFacturaField.setIDDocPeriodoHasta(PeriodoHasta);
					clsDGIXMLFacturaField.setIDDocPeriodoHastaSpecified(true);
				} else {
					clsDGIXMLFacturaField.setIDDocPeriodoHastaSpecified(false);
				}

				//Si FechaDeVencimiento es menor que minvalue(.net) o MinFecha(hecho para compatibilidad con vb6 #1/1/0100#) no va en el mensaje xml
				if (FechaDeVencimiento != null && FechaDeVencimiento.compareTo(new Date(0)) != 0 && !MinFecha.equals(FechaDeVencimiento)) {
					//Valida que PeriodoHasta no sea menor que PeriodoDesde
					if (FechaDeVencimiento.compareTo(new Date("2000/01/01")) < 0) {
						throw new TAException("Fecha De Vencimiento debe ser mayor a 01/01/2000 - Recibida :" + FechaDeVencimiento.toString(), 4009);
					}
					if (FechaDeVencimiento.compareTo(new Date("2060/12/31")) > 0) {
						throw new TAException("Fecha De Vencimiento Hasta debe ser menor a 31/12/2060 - Recibida :" + FechaDeVencimiento.toString(), 4009);
					}
					clsDGIXMLFacturaField.setIDDocFchVenc(FechaDeVencimiento);
					clsDGIXMLFacturaField.setIDDocFchVencSpecified(true);
				} else {
					clsDGIXMLFacturaField.setIDDocFchVencSpecified(false);
				}
			}

			//Indicador de montos brutos no corresponde en remitos y resguardos
			if (TipoCFE == enumTipoDeComprobanteCFE.eRemito || TipoCFE == enumTipoDeComprobanteCFE.eRemitoContingencia || TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacion || TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia || TipoCFE == enumTipoDeComprobanteCFE.eResguardo || TipoCFE == enumTipoDeComprobanteCFE.eResguardoContingencia) {
				clsDGIXMLFacturaField.setIDDocMntBrutoSpecified(false);
			} else {
				if (IndicadorMontosBrutos == enumIndicadorMontosBrutos.IVAIncluido || IndicadorMontosBrutos == enumIndicadorMontosBrutos.IMEBAyAdicionalesIncluido) {
					clsDGIXMLFacturaField.setIDDocMntBruto((short) IndicadorMontosBrutos.getValue());
					clsDGIXMLFacturaField.setIDDocMntBrutoSpecified(true);
				} else {
					clsDGIXMLFacturaField.setIDDocMntBrutoSpecified(false);
				}
			}

			//Forma de pago no corresponde en remitos y resguardos
			if (TipoCFE == enumTipoDeComprobanteCFE.eRemito || TipoCFE == enumTipoDeComprobanteCFE.eRemitoContingencia || TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacion || TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia || TipoCFE == enumTipoDeComprobanteCFE.eResguardo || TipoCFE == enumTipoDeComprobanteCFE.eResguardoContingencia) {
				clsDGIXMLFacturaField.setIDDocFmaPagoSpecified(false);
			} else {
				if (FormaDePago == enumFormaDePago.SinDefinir) {
					throw new TAException("Es obligatorio especificar forma de pago.", 4060);
				} else {
					clsDGIXMLFacturaField.setIDDocFmaPago((byte) FormaDePago.getValue());
					clsDGIXMLFacturaField.setIDDocFmaPagoSpecified(true);
				}
			}

			//En factura de exportacion y sus notas de credito es obligatorio especificar su clausula de venta, en el resto no corresponde
			if (TipoCFE == enumTipoDeComprobanteCFE.eFacturaExportacion || TipoCFE == enumTipoDeComprobanteCFE.eFacturaExportacionContingencia || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia) {
				if (ClausulaDeVenta.trim().equals("")) {
					throw new TAException("Es obligatorio especificar la clausula de venta para las exportaciones", 4012);
				} else {
					clsDGIXMLFacturaField.setIDDocClausulaVenta(LargoMax(ClausulaDeVenta.trim(), 3));
				}
			}

			//En eRemito de exportacion si especificaron se setea sino no (no es obligatorio)
			if (TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacion || TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia) {
				clsDGIXMLFacturaField.setIDDocClausulaVenta(LargoMax(ClausulaDeVenta.trim(), 3));
			}

			//En exportaciones es obligatoria la modalidad de venta, en el resto de los casos no se toma en cuenta
			if (TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia || TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacion || TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia) {

				if (ModalidadDeVenta == enumModalidadDeVenta.SinDefinir) {
					throw new TAException("Es obligatorio especificar la modalidad de venta para las exportaciones", 4013);
				} else {
					clsDGIXMLFacturaField.setIDDocModalidadVenta((short) ModalidadDeVenta.getValue());
					clsDGIXMLFacturaField.setIDDocModalidadVentaSpecified(true);
				}
			} else {
				clsDGIXMLFacturaField.setIDDocModalidadVentaSpecified(false);
			}

			//En exportaciones es obligatoria la via de transporte, en el resto de los casos no se toma en cuenta
			if (TipoCFE == enumTipoDeComprobanteCFE.eFacturaExportacion || TipoCFE == enumTipoDeComprobanteCFE.eFacturaExportacionContingencia || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia || TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacion || TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia) {
				if (ViaDeTransporte == enumViaDeTransporte.SinDefinir) {
					throw new TAException("Es obligatorio especificar la via de transporte para las exportaciones", 4014);
				} else {
					clsDGIXMLFacturaField.setIDDocViaTransporte((short) ViaDeTransporte.getValue());
					clsDGIXMLFacturaField.setIDDocViaTransporteSpecified(true);
				}
			} else {
				clsDGIXMLFacturaField.setIDDocViaTransporteSpecified(false);
			}

			//En remito, resguardo y boleta no corresponde iva al dia
			if (TipoCFE == enumTipoDeComprobanteCFE.eRemito || TipoCFE == enumTipoDeComprobanteCFE.eResguardo || TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacion || TipoCFE == enumTipoDeComprobanteCFE.eBoleta || TipoCFE == enumTipoDeComprobanteCFE.eBoletaNotaCred || TipoCFE == enumTipoDeComprobanteCFE.eBoletaNotaDeb || TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacion || TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia) {
				clsDGIXMLFacturaField.setIDDocIVAalDiaSpecified(false);
			} else {
				if (IVAalDia == enumIVAalDia.NoIVAalDia) {
					clsDGIXMLFacturaField.setIDDocIVAalDiaSpecified(true);
					clsDGIXMLFacturaField.setIDDocIVAalDia((short) IVAalDia.getValue());
				}
			}

			//Secreto profesional no viaja en eBoleta
			if (TipoCFE == enumTipoDeComprobanteCFE.eBoleta || TipoCFE == enumTipoDeComprobanteCFE.eBoletaNotaCred || TipoCFE == enumTipoDeComprobanteCFE.eBoletaNotaDeb) {
				clsDGIXMLFacturaField.setIDDocSecProfSpecified(false);
			} else {
				if (SecretoProfesional == enumSecretoProf.OperacionAmparadaSecretoProf) {
					clsDGIXMLFacturaField.setIDDocSecProfSpecified(true);
					clsDGIXMLFacturaField.setIDDocSecProf((short) SecretoProfesional.getValue());
				}
			}

			clsDGIXMLFacturaField.setIDDocInfoAdicionalDoc(InfoAdicionalDoc);
			XMLFACTURAField.setDGI(clsDGIXMLFacturaField);
			DGIA1 = true;
		} catch (TAException ex) {
			ErrorCod.argValue = 8;

			ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
			try {
				objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
				DGIA1 = false;
				throw ex;
			} catch (UnsupportedEncodingException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (UnsupportedEncodingException ex) {
			throw new TAException(ex.getMessage());
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
		return DGIA1;
	}

	public final boolean DGIA2setEmisor(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, long RUCEmisor, String RazonSocialEmisor, String NombreComercialEmisor, String GiroNegocioEmisor, String CorreoEmisor, String NombreCasaPrincipalSucursalEmisor, int CodigoCasaPrincipalSucursalEmisor, String DomicilioFiscalEmisor, String CiudadEmisor, String DepartamentoEmisor, String InfoAdicionalEmisor) throws TAException {

		boolean DGIA2 = false;
		try {
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "long RUCEmisor", "String RazonSocialEmisor", "String NombreComercialEmisor", "String GiroNegocioEmisor", "String CorreoEmisor", "String NombreCasaPrincipalSucursalEmisor", "int CodigoCasaPrincipalSucursalEmisor", "String DomicilioFiscalEmisor", "String CiudadEmisor", "String DepartamentoEmisor", "String InfoAdicionalEmisor"};
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, RUCEmisor, RazonSocialEmisor, NombreComercialEmisor, GiroNegocioEmisor, CorreoEmisor, NombreCasaPrincipalSucursalEmisor, CodigoCasaPrincipalSucursalEmisor, DomicilioFiscalEmisor, CiudadEmisor, DepartamentoEmisor, InfoAdicionalEmisor};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

			//Valido que haya una factura iniciada
			FacturaIniciada();
			//Elimino espacios
			RazonSocialEmisor = CNullStr(RazonSocialEmisor).trim();
			NombreComercialEmisor = CNullStr(NombreComercialEmisor).trim();
			GiroNegocioEmisor = CNullStr(GiroNegocioEmisor).trim();
			CorreoEmisor = CNullStr(CorreoEmisor).trim();
			NombreCasaPrincipalSucursalEmisor = CNullStr(NombreCasaPrincipalSucursalEmisor).trim();
			DomicilioFiscalEmisor = CNullStr(DomicilioFiscalEmisor).trim();
			CiudadEmisor = CNullStr(CiudadEmisor).trim();
			DepartamentoEmisor = CNullStr(DepartamentoEmisor).trim();

			//*** Validacion de Datos***
			String StrRucEmisor = String.format("%012d", RUCEmisor);
			if (!(VerificacionRUT(StrRucEmisor.toString()))) {
				throw new TAException("RUT del Emisor Invalido - Recibido: " + StrRucEmisor.toString(), 4015);
			}
			if (CodigoCasaPrincipalSucursalEmisor == 0) {
				throw new TAException("Codigo de Sucursal del Emisor para DGI no puede ser vacio.", 4017);
			}
			if ((new Integer(CodigoCasaPrincipalSucursalEmisor)).toString().length() > 4) {
				throw new TAException("Codigo de Sucursal del Emisor para DGI no puede tener mas de 4 digitos.", 4017);
			}
			if (nTipoApi == enumTipoApi.TAFE) {
				if ((RazonSocialEmisor == null) || (RazonSocialEmisor.trim() == null || RazonSocialEmisor.trim().length() == 0)) {
					throw new TAException("Razon Social del Emisor no puede ser vacia.", 4016);
				}
				if ((DomicilioFiscalEmisor == null) || (DomicilioFiscalEmisor.trim() == null || DomicilioFiscalEmisor.trim().length() == 0)) {
					throw new TAException("Domicilio Fiscal del Emisor no puede ser vacio.", 4018);
				}
				if ((CiudadEmisor == null) || (CiudadEmisor.trim() == null || CiudadEmisor.trim().length() == 0)) {
					throw new TAException("Ciudad del Emisor no puede ser vacia.", 4019);
				}
				if ((DepartamentoEmisor == null) || (DepartamentoEmisor.trim() == null || DepartamentoEmisor.trim().length() == 0)) {
					throw new TAException("Departamento del Emisor no puede ser vacio.", 4020);
				}
			}
			//*** Fin Validacion de Datos ***

			clsDGIXMLFacturaField.setEMIRUCEmisor(StrRucEmisor);
			clsDGIXMLFacturaField.setEMIRznSoc(LargoMax(RazonSocialEmisor, 150));
			clsDGIXMLFacturaField.setEMINomComercial(LargoMax(NombreComercialEmisor, 30));
			clsDGIXMLFacturaField.setEMIGiroEmis(LargoMax(GiroNegocioEmisor, 60));
			clsDGIXMLFacturaField.setEMICorreoEmisor(LargoMax(CorreoEmisor, 80));
			clsDGIXMLFacturaField.setEMIEmiSucursal(LargoMax(NombreCasaPrincipalSucursalEmisor, 20));
			clsDGIXMLFacturaField.setEMICdgDGISucur(CodigoCasaPrincipalSucursalEmisor);
			clsDGIXMLFacturaField.setEMIDomFiscal(LargoMax(DomicilioFiscalEmisor, 70));
			clsDGIXMLFacturaField.setEMICiudad(LargoMax(CiudadEmisor, 30));
			clsDGIXMLFacturaField.setEMIDepartamento(LargoMax(DepartamentoEmisor, 30));
			clsDGIXMLFacturaField.setEMIInfoAdicionalEmisor(InfoAdicionalEmisor);
			XMLFACTURAField.setDGI(clsDGIXMLFacturaField);
			DGIA2 = true;

		} catch (TAException ex) {
			try {
				ErrorCod.argValue = 8;
				ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
				objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
				DGIA2 = false;
				throw ex;
			} catch (UnsupportedEncodingException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (UnsupportedEncodingException ex) {
			throw new TAException(ex.getMessage());
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
		return DGIA2;
	}

	@SuppressWarnings("empty-statement")
	public final boolean DGIA3addTelefonoEmisor(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String TelefonoEmisor) throws TAException {

		boolean DGIA3 = false;
		try {
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "String TelefonoEmisor"};
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, TelefonoEmisor};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);
			//Valido que haya una factura iniciada
			FacturaIniciada();

			//Si ya habia alcanzado la cantidad maxima no permito seguir agregando
			if (listaTelefonosArray != null) {
				if (listaTelefonosArray.size() == MAXlstEmiTelefono) {
					throw new TAException("Cantidad maxima de Telefonos alcanzada (Zona A) Max: " + MAXlstEmiTelefono, 4021);
				}

				//Elimino espacios
				TelefonoEmisor = CNullStr(TelefonoEmisor).trim();
				//Creo Instancia, seteo propiedades y agrego a lista
				EMITelefono telefono = lstEmiTelefono.new EMITelefono();
				telefono.setEMITelefono(TelefonoEmisor);

				if (listaTelefonosArray.size() > 0) {
					for (int i = 0; i < listaTelefonosArray.size(); i++) {
						if (listaTelefonosArray.get(i).equals(telefono.getEMITelefono())) {
							return true;
						}
					}
				}
				listaTelefonosArray.add(telefono);
				EMITelefono[] tempListEMITelef = new EMITelefono[listaTelefonosArray.size()];
				lstEmiTelefono.setEMITelefono(listaTelefonosArray);
				clsDGIXMLFacturaField.setEmiTelefonos(lstEmiTelefono);
				XMLFACTURAField.setDGI(clsDGIXMLFacturaField);

				DGIA3 = true;
			}
		} catch (TAException ex) {
			try {
				ErrorCod.argValue = 8;
				ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
				objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
				DGIA3 = false;
				throw ex;
			} catch (UnsupportedEncodingException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (UnsupportedEncodingException ex) {
			throw new TAException(ex.getMessage());
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
		return DGIA3;
	}

	public final boolean DGIA4setReceptor(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, ITAParsing_v0206.enumTipoDocumentoDelReceptor TipoDocumentoReceptoraux, String CodigoPaisReceptor, String NroDocumentoReceptorUruguayo, String NroDocumentoReceptorExtranjero, String RazonSocialReceptor, String DireccionReceptor, String CiudadReceptor, String DepartamentoProviciaEstado, String PaisReceptor, int CodigoPostalReceptor, String InfoAdicionalReceptor, String LugarDestinoEntrega, String NumeroOrdenCompra) throws TAException {

		boolean DGIA4 = false;
		try {
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			enumTipoDocumentoDelReceptor TipoDocumentoReceptor = enumTipoDocumentoDelReceptor.forValue(TipoDocumentoReceptoraux.getValue());
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, TipoDocumentoReceptor, CodigoPaisReceptor, NroDocumentoReceptorUruguayo, NroDocumentoReceptorExtranjero, RazonSocialReceptor, DireccionReceptor, CiudadReceptor, DepartamentoProviciaEstado, PaisReceptor, CodigoPostalReceptor, InfoAdicionalReceptor, LugarDestinoEntrega, NumeroOrdenCompra};
			Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "ITAParsing_v0206.enumTipoDocumentoDelReceptor TipoDocumentoReceptor", "String CodigoPaisReceptor", "String NroDocumentoReceptorUruguayo", "String NroDocumentoReceptorExtranjero", "String RazonSocialReceptor", "String DireccionReceptor", "String CiudadReceptor", "String DepartamentoProviciaEstado", "String PaisReceptor", "int CodigoPostalReceptor", "String InfoAdicionalReceptor", "String LugarDestinoEntrega", "String NumeroOrdenCompra"};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

			//Valido que haya una factura iniciada
			FacturaIniciada();
			//Elimino espacios
			InfoAdicionalReceptor = CNullStr(InfoAdicionalReceptor).trim();
			LugarDestinoEntrega = CNullStr(LugarDestinoEntrega).trim();
			NumeroOrdenCompra = CNullStr(NumeroOrdenCompra).trim();
			DireccionReceptor = CNullStr(DireccionReceptor).trim();
			NroDocumentoReceptorExtranjero = CNullStr(NroDocumentoReceptorExtranjero).trim();
			NroDocumentoReceptorUruguayo = CNullStr(NroDocumentoReceptorUruguayo).trim();
			CodigoPaisReceptor = CNullStr(CodigoPaisReceptor).trim();
			RazonSocialReceptor = CNullStr(RazonSocialReceptor).trim();
			CiudadReceptor = CNullStr(CiudadReceptor).trim();
			DepartamentoProviciaEstado = CNullStr(DepartamentoProviciaEstado).trim();
			PaisReceptor = CNullStr(PaisReceptor).trim();

			//*** Validacion de Datos***
			if (enumTipoDocumentoDelReceptor.forValue(TipoDocumentoReceptor.getValue()) != TipoDocumentoReceptor) {
				throw new TAException("Tipo de documento receptor invalido - Recibido :" + TipoDocumentoReceptor.toString(), 4167);
			}

			if (TipoDocumentoReceptor != enumTipoDocumentoDelReceptor.SinDefinir && ((CNullStr(CodigoPaisReceptor).trim() == null || CNullStr(CodigoPaisReceptor).trim().length() == 0) || ((CNullStr(NroDocumentoReceptorUruguayo).trim() == null || CNullStr(NroDocumentoReceptorUruguayo).trim().length() == 0) && (CNullStr(NroDocumentoReceptorExtranjero).trim() == null || CNullStr(NroDocumentoReceptorExtranjero).trim().length() == 0)))) {
				throw new TAException("Si especifica tipo de documento de receptor, debe especificar pais y numero de documento", 1015);
			}

			//Si es boleta debe tener TipoDocumentoReceptor y puede ser cualquiera, si es RUC no puede ser el de un emisor electronico, esto lo valido desde TAFEApi
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoleta.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaContingencia.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue() ||
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
				if (TipoDocumentoReceptor == enumTipoDocumentoDelReceptor.SinDefinir) {
					throw new TAException("Si es eBoleta debe especificar el tipo de documento de receptor", 1000);
				}
			}

			//Si es factura debe tener RUT
			if ((XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFactura.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaContingencia.getValue() ||
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCred.getValue() ||
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredContingencia.getValue() ||
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDeb.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebContingencia.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaCuentaAjena.getValue() ||
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaCuentaAjenaContingencia.getValue() ||
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjena.getValue() ||
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjenaContingencia.getValue() ||
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjena.getValue() ||
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjenaContingencia.getValue()) && 
					TipoDocumentoReceptor != enumTipoDocumentoDelReceptor.RUC && XMLFACTURAField.getDGI().getIDDocSecProf() != 1) {
				throw new TAException("RUT del Receptor(cliente) es obligatorio para eFacturas.", 1001);
			}

			if (nTipoDeNegocio == enumTipoDeNegocio.TiendaFreeShop && 
					(XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicket.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketContingencia.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketNotaCred.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketNotaCredContingencia.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketNotaDeb.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketNotaDebContingencia.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemito.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoContingencia.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eResguardo.getValue() ||
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eResguardoContingencia.getValue())) {
				if (TipoDocumentoReceptor == enumTipoDocumentoDelReceptor.SinDefinir) {
					throw new TAException("El receptor(cliente) es obligatorio para Freeshop.", 1002);
				}
			}

			if ((CodigoPaisReceptor != null && CodigoPaisReceptor.length() > 0) && 
					(TipoDocumentoReceptor == enumTipoDocumentoDelReceptor.SinDefinir || 
					((NroDocumentoReceptorUruguayo == null || NroDocumentoReceptorUruguayo.length() == 0) && 
					(NroDocumentoReceptorExtranjero == null || NroDocumentoReceptorExtranjero.length() == 0)))) {
				throw new TAException("Si especifica codigo del pais del documento de receptor, debe especificar tipo y numero de documento", 1003);
			}

			//Valido coherencia entre el Tipo de documento y el pais del cliente
			if ((TipoDocumentoReceptor == enumTipoDocumentoDelReceptor.CI || 
					TipoDocumentoReceptor == enumTipoDocumentoDelReceptor.RUC) && 
					(CodigoPaisReceptor != null && !CodigoPaisReceptor.equals("UY"))) {
				throw new TAException("Si el documento del Receptor(cliente) es C.I o RUT debe ser uruguayo. - Recibido: " + CodigoPaisReceptor, 1004);
			}


			//Valido Coherencia entre el Tipo de documento,el pais del cliente y el documento
			if (TipoDocumentoReceptor == enumTipoDocumentoDelReceptor.CI && NroDocumentoReceptorUruguayo != null && NroDocumentoReceptorUruguayo.replace(".", "").replace("-", "").length() > 8) {
				throw new TAException("Si el documento del Receptor(cliente) es C.I la misma no puede tener mas de 8 numeros. - Recibido: " + NroDocumentoReceptorUruguayo, 1034);
			}


			//Valido Coherencia entre el Tipo de documento y el pais del cliente
			if ((TipoDocumentoReceptor == enumTipoDocumentoDelReceptor.DNI) && (CodigoPaisReceptor != null && !CodigoPaisReceptor.equals("AR") && !CodigoPaisReceptor.equals("BR") && !CodigoPaisReceptor.equals("CL") && !CodigoPaisReceptor.equals("PY"))) {
				throw new TAException("Si el documento del Receptor(cliente) es DNI debe ser Argentino, Brasilero, Chileno o Paraguayo. - Recibido: " + CodigoPaisReceptor, 1035);
			}

			//Si existe documento uruguayo debe existir tipo de documento y codigo de pais
			if (!CNullStr(NroDocumentoReceptorUruguayo).trim().equals("") && (TipoDocumentoReceptor == enumTipoDocumentoDelReceptor.SinDefinir || (CodigoPaisReceptor == null || CodigoPaisReceptor.length() == 0))) {
				throw new TAException("Si especifica el documento uruguayo debe existir el tipo de documento y el codigo de pais", 1005);
			}

			//Si existe documento uruguayo no debe existir documento extranjero
			if ((NroDocumentoReceptorUruguayo != null && NroDocumentoReceptorUruguayo.length() > 0) && (NroDocumentoReceptorExtranjero != null && NroDocumentoReceptorExtranjero.length() > 0)) {
				throw new TAException("Si especifica el documento uruguayo y extranjero al mismo tiempo. Doc Uruguayo: " + NroDocumentoReceptorUruguayo + " - Doc Extranjero: " + NroDocumentoReceptorExtranjero, 4022);
			}

			//si es ci o ruc debe especificar nro de documento
			if ((TipoDocumentoReceptor == enumTipoDocumentoDelReceptor.CI || TipoDocumentoReceptor == enumTipoDocumentoDelReceptor.RUC) && (NroDocumentoReceptorUruguayo == null || NroDocumentoReceptorUruguayo.length() == 0)) {
				throw new TAException("Si el documento del Receptor(cliente) es C.I o RUT debe especificar el numero de documento uruguayo.", 1006);
			}

			//Si es RUC lo valido
			if (TipoDocumentoReceptor == enumTipoDocumentoDelReceptor.RUC) {
				NroDocumentoReceptorUruguayo = String.format("%012d", Long.parseLong(NroDocumentoReceptorUruguayo));
				if (!(VerificacionRUT(NroDocumentoReceptorUruguayo))) {
					throw new TAException("RUT del Receptor(cliente) Invalido - Recibido: " + NroDocumentoReceptorUruguayo.toString(), 1007);
				}
			}

			//Si es CI la valido
			if ((TipoDocumentoReceptor == enumTipoDocumentoDelReceptor.CI) && (NroDocumentoReceptorUruguayo != null && NroDocumentoReceptorUruguayo.length() > 0)) {
				NroDocumentoReceptorUruguayo = NroDocumentoReceptorUruguayo.replace("-", "");
				NroDocumentoReceptorUruguayo = NroDocumentoReceptorUruguayo.replace(".", "");
				NroDocumentoReceptorUruguayo = NroDocumentoReceptorUruguayo.replace(" ", "");
				NroDocumentoReceptorUruguayo = String.valueOf(CTLng(NroDocumentoReceptorUruguayo));
				if (!(VerificacionCedula(NroDocumentoReceptorUruguayo.toString()))) {
					throw new TAException("Cedula del Receptor(cliente) Invalida - Recibido: " + NroDocumentoReceptorUruguayo.toString(), 1008);
				}
			}

			//Si Tipo de doc es Pasaporte, DNI u otros debe especificar el nro doc extranjero
			if ((TipoDocumentoReceptor == enumTipoDocumentoDelReceptor.Otros || TipoDocumentoReceptor == enumTipoDocumentoDelReceptor.Pasaporte || TipoDocumentoReceptor == enumTipoDocumentoDelReceptor.DNI) && (NroDocumentoReceptorExtranjero == null || NroDocumentoReceptorExtranjero.length() == 0)) {
				throw new TAException("Si el documento del Receptor(cliente) es Pasaporte, DNI u Otros debe especificar el numero de documento extranjero.", 1009);
			}

			//Si Tipo Documento es eFactura debe especificar documento uuruguayo
			if ((NroDocumentoReceptorUruguayo == null || NroDocumentoReceptorUruguayo.length() == 0) && 
					(XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFactura.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaContingencia.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCred.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredContingencia.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDeb.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebContingencia.getValue() ||
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaCuentaAjena.getValue() || 
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaCuentaAjenaContingencia.getValue() ||
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjena.getValue() ||
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjenaContingencia.getValue() ||
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjena.getValue() ||
					XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjenaContingencia.getValue()) &&
					XMLFACTURAField.getDGI().getIDDocSecProf() != 1) {
				throw new TAException("Nro. de Documento del Receptor(cliente) es obligatorio.", 4023);
			}

			//Si Tipo Documento es eFactura no debe ser documento extranjero
			if ((NroDocumentoReceptorExtranjero != null && NroDocumentoReceptorExtranjero.length() > 0)
					&& (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFactura.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCred.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDeb.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaCuentaAjena.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaCuentaAjenaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjena.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjenaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjena.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjenaContingencia.getValue())) {
				throw new TAException("No se pueden emitir eFacturas a documentos extranjeros. Doc Extranjero: " + NroDocumentoReceptorExtranjero, 4024);
			}

			//Valido que razon social no este vacia
			if ((RazonSocialReceptor == null || RazonSocialReceptor.length() == 0) && nTipoApi == enumTipoApi.TAFE
					&& (((XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFactura.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCred.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDeb.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemito.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaExportacion.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacion.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaCuentaAjena.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaCuentaAjenaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjena.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjenaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjena.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjenaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eResguardo.getValue()) && XMLFACTURAField.getDGI().getIDDocSecProf() != 1)
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoleta.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue())) {
				throw new TAException("Nombre o Razon Social del Receptor(cliente) es obligatorio.", 1010);
			}

			if ((NroDocumentoReceptorUruguayo == null || NroDocumentoReceptorUruguayo.length() == 0) && 
					(NroDocumentoReceptorExtranjero == null || NroDocumentoReceptorExtranjero.length() == 0) &&
					nTipoApi == enumTipoApi.TAFE && XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eResguardo.getValue() 
					&& XMLFACTURAField.getDGI().getIDDocSecProf() != 1) {
				throw new TAException("Debe Ingresar Nro. de Documento del Receptor(cliente) o Nro. de Documento Extranjero.", 1010);
			}

			if ((DireccionReceptor == null || DireccionReceptor.length() == 0) && nTipoApi == enumTipoApi.TAFE
					&& (((XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFactura.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCred.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDeb.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemito.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaExportacion.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacion.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaCuentaAjena.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaCuentaAjenaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjena.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjenaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjena.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjenaContingencia.getValue())
					&& XMLFACTURAField.getDGI().getIDDocSecProf() != 1)
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoleta.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue())) {
				throw new TAException("Direccion del Receptor(cliente) es obligatorio.", 1011);
			}

			if ((CiudadReceptor == null || CiudadReceptor.length() == 0) && nTipoApi == enumTipoApi.TAFE
					&& (((XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFactura.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCred.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDeb.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemito.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaExportacion.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacion.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaCuentaAjena.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaCuentaAjenaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjena.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjenaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjena.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjenaContingencia.getValue())
					&& XMLFACTURAField.getDGI().getIDDocSecProf() != 1)
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoleta.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue())) {
				throw new TAException("Ciudad del Receptor(cliente) es obligatorio.", 1012);
			}

			if ((DepartamentoProviciaEstado == null || DepartamentoProviciaEstado.length() == 0) && nTipoApi == enumTipoApi.TAFE 
					&& (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaExportacion.getValue() 
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue() 
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue() 
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue() 
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() 
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaCuentaAjena.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaCuentaAjenaContingencia.getValue() 
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjena.getValue() 
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjenaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjena.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjenaContingencia.getValue())
					&& XMLFACTURAField.getDGI().getIDDocSecProf() != 1) {
				throw new TAException("Departamento - Provincia - Estado del receptor es obligatorio para las exportaciones.", 1013);
			}

			if ((CNullStr(PaisReceptor).trim() == null || CNullStr(PaisReceptor).trim().length() == 0) && nTipoApi == enumTipoApi.TAFE
					&& (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaExportacion.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue() 
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue() 
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacion.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaCuentaAjena.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaCuentaAjenaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjena.getValue() 
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjenaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjena.getValue() 
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjenaContingencia.getValue())
					&& XMLFACTURAField.getDGI().getIDDocSecProf() != 1) {
				throw new TAException("Pais del receptor es obligatorio para las exportaciones.", 1014);
			}

			//*** Fin Validacion de Datos ***


			if (TipoDocumentoReceptor == enumTipoDocumentoDelReceptor.SinDefinir) {
				clsDGIXMLFacturaField.setRECTipoDocRecepSpecified(false);
			} else {
				clsDGIXMLFacturaField.setRECTipoDocRecepSpecified(true);
			}
			clsDGIXMLFacturaField.setRECTipoDocRecep((short) TipoDocumentoReceptor.getValue());
			clsDGIXMLFacturaField.setRECCodPaisRecep(LargoMax(CodigoPaisReceptor, 2));

			clsDGIXMLFacturaField.setRECDocRecep(LargoMax(NroDocumentoReceptorUruguayo, 20));
			clsDGIXMLFacturaField.setRECDocRecepExtranjero(LargoMax(NroDocumentoReceptorExtranjero, 20));

			clsDGIXMLFacturaField.setRECRznSocRecep(LargoMax(RazonSocialReceptor, 150));
			clsDGIXMLFacturaField.setRECDirRecep(LargoMax(DireccionReceptor, 70));
			clsDGIXMLFacturaField.setRECCiudadRecep(LargoMax(CiudadReceptor, 30));
			clsDGIXMLFacturaField.setRECDeptoRecep(LargoMax(DepartamentoProviciaEstado, 30));
			clsDGIXMLFacturaField.setRECPaisRecep(LargoMax(PaisReceptor, 30));

			//Limpio Codigo postal para evitar rechazos (codigos postales de extrajeros)
			if (CodigoPostalReceptor > 99999) {
				CodigoPostalReceptor = 0;
			}

			if (CodigoPostalReceptor > 0) {
				clsDGIXMLFacturaField.setRECCP(CodigoPostalReceptor);
				clsDGIXMLFacturaField.setRECCPSpecified(true);
			} else {
				clsDGIXMLFacturaField.setRECCPSpecified(false);
			}
			clsDGIXMLFacturaField.setRECInfoAdicional(LargoMax(InfoAdicionalReceptor, 150));
			//No debe enviarse el lugar destino entrega y numero orden compra en eResguardo
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()) {
				clsDGIXMLFacturaField.setRECLugarDestinoEntrega(LugarDestinoEntrega);
				clsDGIXMLFacturaField.setRECNroOrdenCompra(NumeroOrdenCompra);
			}
			DGIA4 = true;
			XMLFACTURAField.setDGI(clsDGIXMLFacturaField);
		} catch (TAException ex) {
			try {
				ErrorCod.argValue = 8;
				ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
				objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
				DGIA4 = false;
				throw ex;
			} catch (UnsupportedEncodingException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (UnsupportedEncodingException ex) {
			throw new TAException(ex.getMessage());
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
		return DGIA4;
	}

	public final boolean DGIA5setTotalesDeEncabezado(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String TipoMonedaTransaccion, double TipoDeCambio, double TotalMontoNoGravado, double TotalMontoExportacionYAsimiladas, double TotalMontoImpuestoPercibido, double TotalMontoIVAEnSuspenso, double TotalMontoNetoIVATasaMinima, double TotalMontoNetoIVATasaBasica, double TotalMontoNetoIVAOtraTasa, double TasaMinimaIVA, double TasaBasicaIVA, double TotalIVATasaMinima, double TotalIVATasaBasica, double TotalIVAOtraTasa, double TotalMontoTotal, double TotalMontoRetenido, int CantLineasDetalle, double MontoNoFacturable, double MontoTotalAPagar, double TotalMontoCreditoFiscal) throws TAException {

		boolean DGIA5 = false;
		try {
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "String TipoMonedaTransaccion", "double TipoDeCambio", "double TotalMontoNoGravado", "double TotalMontoExportacionYAsimiladas", "double TotalMontoImpuestoPercibido", "double TotalMontoIVAEnSuspenso", "double TotalMontoNetoIVATasaMinima", "double TotalMontoNetoIVATasaBasica", "double TotalMontoNetoIVAOtraTasa", "double TasaMinimaIVA", "double TasaBasicaIVA", "double TotalIVATasaMinima", "double TotalIVATasaBasica", "double TotalIVAOtraTasa", "double TotalMontoTotal", "double TotalMontoRetenido", "int CantLineasDetalle", "double MontoNoFacturable", "double MontoTotalAPagar", "double TotalMontoCreditoFiscal"};
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, TipoMonedaTransaccion, TipoDeCambio, TotalMontoNoGravado, TotalMontoExportacionYAsimiladas, TotalMontoImpuestoPercibido, TotalMontoIVAEnSuspenso, TotalMontoNetoIVATasaMinima, TotalMontoNetoIVATasaBasica, TotalMontoNetoIVAOtraTasa, TasaMinimaIVA, TasaBasicaIVA, TotalIVATasaMinima, TotalIVATasaBasica, TotalIVAOtraTasa, TotalMontoTotal, TotalMontoRetenido, CantLineasDetalle, MontoNoFacturable, MontoTotalAPagar, TotalMontoCreditoFiscal};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);
			//*** Validacion de Datos***    
			//Elimino espacios
			TipoMonedaTransaccion = CNullStr(TipoMonedaTransaccion).trim();

			//Si el monto de Ticket supera las 10.000 UI es obligatorio el RECEPTOR
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicket.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketNotaCred.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketNotaCredContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketNotaDeb.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketNotaDebContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketCuentaAjena.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketCuentaAjenaContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketNotaCredCuentaAjena.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketNotaCredCuentaAjenaContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketNotaDebCuentaAjena.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketNotaDebCuentaAjenaContingencia.getValue()) {

				//Valido que haya una factura iniciada
				FacturaIniciada();

				double totNeto = 0;
				//Sumatoria de Campos 112 a 118
				totNeto = (TotalMontoNoGravado + TotalMontoExportacionYAsimiladas + TotalMontoImpuestoPercibido + TotalMontoIVAEnSuspenso + TotalMontoNetoIVATasaMinima + TotalMontoNetoIVATasaBasica + TotalMontoNetoIVAOtraTasa);
			}

			//Si es eFactura o Boleta es obligatorio el RECEPTOR
			if (((XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFactura.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCred.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDeb.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaCuentaAjena.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaCuentaAjenaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjena.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjenaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjena.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjenaContingencia.getValue()) 
					&& XMLFACTURAField.getDGI().getIDDocSecProf() != 1)
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoleta.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
				if (XMLFACTURAField.getDGI().getRECTipoDocRecep() == 0) {
					throw new TAException("Datos del Receptor(cliente) son obligatorios para eFacturas y eBoletas.", 1019);
				} else {
					if (XMLFACTURAField.getDGI().getRECTipoDocRecep() == enumTipoDocumentoDelReceptor.CI.getValue() || XMLFACTURAField.getDGI().getRECTipoDocRecep() == enumTipoDocumentoDelReceptor.RUC.getValue()) {
						if (XMLFACTURAField.getDGI().getRECTipoDocRecep() == 0 || XMLFACTURAField.getDGI().getRECTipoDocRecep() == 0) {
							throw new TAException("Datos del Receptor(cliente) son obligatorios para eFacturas y eBoletas.", 1020);
						}
					} else {
						if (XMLFACTURAField.getDGI().getRECDocRecepExtranjero() == null || (XMLFACTURAField.getDGI().getRECDocRecepExtranjero().trim() == null || XMLFACTURAField.getDGI().getRECDocRecepExtranjero().trim().length() == 0)) {
							throw new TAException("Datos del Receptor(cliente) son obligatorios para eFacturas y eBoletas.", 1021);
						}
					}

				}
			}

			//Si es eRemito es obligatorio el RECEPTOR, sino esta amparada la operacion por secreto profesional
			if ((XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemito.getValue() 
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() 
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) 
					&& XMLFACTURAField.getDGI().getIDDocSecProf() != 1) {
				if ((XMLFACTURAField.getDGI().getRECRznSocRecep() == null) || (XMLFACTURAField.getDGI().getRECRznSocRecep().trim() == null || XMLFACTURAField.getDGI().getRECRznSocRecep().trim().length() == 0)) {
					throw new TAException("Nombre del Receptor(cliente) es obligatorio para eRemito.", 1037);
				}
				if ((XMLFACTURAField.getDGI().getRECDirRecep() == null) || (XMLFACTURAField.getDGI().getRECDirRecep().trim() == null || XMLFACTURAField.getDGI().getRECDirRecep().trim().length() == 0)) {
					throw new TAException("Direccion del Receptor(cliente) es obligatorio para eRemito.", 1038);
				}
				if ((XMLFACTURAField.getDGI().getRECCiudadRecep() == null) || (XMLFACTURAField.getDGI().getRECCiudadRecep().trim() == null || XMLFACTURAField.getDGI().getRECCiudadRecep().trim().length() == 0)) {
					throw new TAException("Ciudad del Receptor(cliente) es obligatorio para eRemito.", 1039);
				}
			}

			//Si es eBoleta es obligatorio el RECEPTOR
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoleta.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
				if ((XMLFACTURAField.getDGI().getRECRznSocRecep() == null) || (XMLFACTURAField.getDGI().getRECRznSocRecep().trim() == null || XMLFACTURAField.getDGI().getRECRznSocRecep().trim().length() == 0)) {
					throw new TAException("Nombre del receptor(cliente) es obligatorio para eBoleta.", 1037);
				}
				if ((XMLFACTURAField.getDGI().getRECDirRecep() == null) || (XMLFACTURAField.getDGI().getRECDirRecep().trim() == null || XMLFACTURAField.getDGI().getRECDirRecep().trim().length() == 0)) {
					throw new TAException("Direccion del receptor(cliente) es obligatorio para eBoleta.", 1038);
				}
				if ((XMLFACTURAField.getDGI().getRECCiudadRecep() == null) || (XMLFACTURAField.getDGI().getRECCiudadRecep().trim() == null || XMLFACTURAField.getDGI().getRECCiudadRecep().trim().length() == 0)) {
					throw new TAException("Ciudad del receptor(cliente) es obligatorio para eBoleta.", 1039);
				}
			}

			//Si es eResguardo es obligatorio el RECEPTOR
			if ((XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eResguardo.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eResguardoContingencia.getValue())
					&& XMLFACTURAField.getDGI().getIDDocSecProf() != 1) {
				if ((XMLFACTURAField.getDGI().getRECCodPaisRecep() == null) || (XMLFACTURAField.getDGI().getRECCodPaisRecep().trim() == null || XMLFACTURAField.getDGI().getRECCodPaisRecep().trim().length() == 0)) {
					throw new TAException("Codigo de pais del Receptor(cliente) es obligatorio para eResguardo.", 1040);
				}
				if ((XMLFACTURAField.getDGI().getRECTipoDocRecep() == 0) || XMLFACTURAField.getDGI().getRECTipoDocRecep() == 0) {
					throw new TAException("Debe ingresar el tipo de documento del Receptor(cliente) es obligatorio para eResguardo.", 1041);
				}
			}

			//Si es eFacturaExportacion o eRmitoExportacion es obligatorio el RECEPTOR
			if ((XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaExportacion.getValue() 
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue() 
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue() 
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() 
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) && XMLFACTURAField.getDGI().getIDDocSecProf() != 1) {

				if ((XMLFACTURAField.getDGI().getRECTipoDocRecep() == 0) || XMLFACTURAField.getDGI().getRECTipoDocRecep() == 0) {
					throw new TAException("Debe ingresar el tipo de documento del Receptor(cliente) es obligatorio.", 1043);
				}
				if ((XMLFACTURAField.getDGI().getRECRznSocRecep() == null) || (XMLFACTURAField.getDGI().getRECRznSocRecep().trim() == null || XMLFACTURAField.getDGI().getRECRznSocRecep().trim().length() == 0)) {
					throw new TAException("Nombre del Receptor(cliente) es obligatorio.", 1044);
				}
				if ((XMLFACTURAField.getDGI().getRECCodPaisRecep() == null) || (XMLFACTURAField.getDGI().getRECCodPaisRecep().trim() == null || XMLFACTURAField.getDGI().getRECCodPaisRecep().trim().length() == 0)) {
					throw new TAException("Codigo de pais del Receptor(cliente) es obligatorio.", 1042);
				}
				if ((XMLFACTURAField.getDGI().getRECCiudadRecep() == null) || (XMLFACTURAField.getDGI().getRECCiudadRecep().trim() == null || XMLFACTURAField.getDGI().getRECCiudadRecep().trim().length() == 0)) {
					throw new TAException("Ciudad del Receptor(cliente) es obligatorio.", 1046);
				}
				if ((XMLFACTURAField.getDGI().getRECDirRecep() == null) || (XMLFACTURAField.getDGI().getRECDirRecep().trim() == null || XMLFACTURAField.getDGI().getRECDirRecep().trim().length() == 0)) {
					throw new TAException("Direccion del Receptor(cliente) es obligatorio.", 1045);
				}
				if ((XMLFACTURAField.getDGI().getRECDeptoRecep() == null) || (XMLFACTURAField.getDGI().getRECDeptoRecep().trim() == null || XMLFACTURAField.getDGI().getRECDeptoRecep().trim().length() == 0)) {
					throw new TAException("Departamento del Receptor(cliente) es obligatorio.", 1047);
				}
				if ((XMLFACTURAField.getDGI().getRECPaisRecep() == null) || (XMLFACTURAField.getDGI().getRECPaisRecep().trim() == null || XMLFACTURAField.getDGI().getRECPaisRecep().trim().length() == 0)) {
					throw new TAException("Pais del Receptor(cliente) es obligatorio.", 1048);
				}
				if (XMLFACTURAField.getDGI().getRECCodPaisRecep().trim().toUpperCase().equals("UY")) {
					if ((XMLFACTURAField.getDGI().getRECDocRecep() == null) || (XMLFACTURAField.getDGI().getRECDocRecep().trim() == null || XMLFACTURAField.getDGI().getRECDocRecep().trim().length() == 0)) {
						throw new TAException("Documento del Receptor(cliente) es obligatorio.", 1049);
					}
				} else {
					if ((XMLFACTURAField.getDGI().getRECDocRecepExtranjero() == null) || (XMLFACTURAField.getDGI().getRECDocRecepExtranjero().trim() == null || XMLFACTURAField.getDGI().getRECDocRecepExtranjero().trim().length() == 0)) {
						throw new TAException("Documento extranjero del Receptor(cliente) es obligatorio.", 1050);
					}
				}
			}

			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemito.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoContingencia.getValue()) {
				if (TipoMonedaTransaccion.trim().equals("")) {
					throw new TAException("Tipo de Moneda para la transaccion es obligatorio.", 4026);
				}
				if (nTipoApi == enumTipoApi.TAFE) {
					if (!TipoMonedaTransaccion.trim().equals("ARS") && !TipoMonedaTransaccion.trim().equals("BRL") && !TipoMonedaTransaccion.trim().equals("CAD") && !TipoMonedaTransaccion.trim().equals("CLP") && !TipoMonedaTransaccion.trim().equals("CNY") && !TipoMonedaTransaccion.trim().equals("COP") && !TipoMonedaTransaccion.trim().equals("EUR") && !TipoMonedaTransaccion.trim().equals("JPY") && !TipoMonedaTransaccion.trim().equals("MXN") && !TipoMonedaTransaccion.trim().equals("PYG") && !TipoMonedaTransaccion.trim().equals("PEN") && !TipoMonedaTransaccion.trim().equals("USD") && !TipoMonedaTransaccion.trim().equals("UYU") && !TipoMonedaTransaccion.trim().equals("VEF") && !TipoMonedaTransaccion.trim().equals("UYI") && !TipoMonedaTransaccion.trim().equals("UYR")) {
						throw new TAException("Tipo de Moneda no Existe en el Estandar ISO-4217. - Recibida :" + TipoMonedaTransaccion, 4027);
					}
				}

				clsDGIXMLFacturaField.setTOTTpoMoneda(LargoMax(TipoMonedaTransaccion, 3));

				//Valido la cant. de decimales
				if (CantDeDecimales(TipoDeCambio) > 3) {
					throw new TAException("Tipo de Cambio debe tener 3 lugares decimales. - Recibido :" + TipoDeCambio, 4028);
				}

				//valido que en transacciones que no esten en pesos se cargue el tipo de cambio
				if (!TipoMonedaTransaccion.trim().equals("UYU") && TipoDeCambio == 0) {
					throw new TAException("Tipo de Cambio es obligatorio cuando no es Pesos Uruguayos", 4029);
				}

				if (nTipoApi == enumTipoApi.TAFE) {
					if (TipoDeCambio > 99) {
						throw new TAException("Tipo de Cambio no puede ser mayor a 99. - Recibido :" + TipoDeCambio, 4287);
					}
				}

				//Si tipoMoneda no esta en pesos no viaja en el mensaje xml
				if (!TipoMonedaTransaccion.trim().equals("UYU")) {
					clsDGIXMLFacturaField.setTOTTpoCambioSpecified(true);
					clsDGIXMLFacturaField.setTOTTpoCambio(Math.round(TipoDeCambio * Math.pow(10, 3)) / Math.pow(10, 3));
				} else {
					clsDGIXMLFacturaField.setTOTTpoCambioSpecified(false);
				}

				if (TotalMontoNoGravado != 0 && CantDeDecimales(TotalMontoNoGravado) > 2) {
					throw new TAException("Monto Total No Gravado debe tener 2 lugares decimales. - Recibido :" + TotalMontoNoGravado, 4030);
				}

				//Total monto no gravado no viaja en eRemitos, eResguardo y exportaciones
				if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
					clsDGIXMLFacturaField.setTOTMntNoGrv(Math.round(TotalMontoNoGravado * Math.pow(10, 2)) / Math.pow(10, 2));
					clsDGIXMLFacturaField.setTOTMntNoGrvSpecified(true);
				} else {
					clsDGIXMLFacturaField.setTOTMntNoGrvSpecified(false);
				}

				if (TotalMontoExportacionYAsimiladas == 0 && (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue())) {
					throw new TAException("Monto Total de Exportacion y Asimiladas es obligatorio para exportaciones.", 4031);
				}

				if (TotalMontoExportacionYAsimiladas != 0 && CantDeDecimales(TotalMontoExportacionYAsimiladas) > 2) {
					throw new TAException("Monto Total de Exportacion y Asimiladas debe tener 2 lugares decimales. - Recibido :" + TotalMontoExportacionYAsimiladas, 4032);
				}

				//Total Monto Exportacion y asimiladas no viaja en eRemitos, eResguardo y eBoleta
				if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoleta.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
					clsDGIXMLFacturaField.setTOTMntExpoyAsim(Math.round(TotalMontoExportacionYAsimiladas * Math.pow(10, 2)) / Math.pow(10, 2));
					clsDGIXMLFacturaField.setTOTMntExpoyAsimSpecified(true);
				} else {
					clsDGIXMLFacturaField.setTOTMntExpoyAsimSpecified(false);
				}

				if (TotalMontoImpuestoPercibido != 0 && CantDeDecimales(TotalMontoImpuestoPercibido) > 2) {
					throw new TAException("Monto Total de Impuesto Percibido debe tener 2 lugares decimales. - Recibido :" + TotalMontoImpuestoPercibido, 4033);
				}

				//Total monto impuesto percibido no viaja en eRemitos, eResguardo, exportaciones y eBoleta
				if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoleta.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
					clsDGIXMLFacturaField.setTOTMntImpuestoPerc(Math.round(TotalMontoImpuestoPercibido * Math.pow(10, 2)) / Math.pow(10, 2));
					clsDGIXMLFacturaField.setTOTMntImpuestoPercSpecified(true);
				} else {
					clsDGIXMLFacturaField.setTOTMntImpuestoPercSpecified(false);
				}

				if (TotalMontoIVAEnSuspenso != 0 && CantDeDecimales(TotalMontoIVAEnSuspenso) > 2) {
					throw new TAException("Monto Total de IVA en Suspenso debe tener 2 lugares decimales. - Recibido :" + TotalMontoIVAEnSuspenso, 4034);
				}

				//Total monto IVA en suspenso no viaja en eRemitos, eResguardo y exportaciones
				if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
					clsDGIXMLFacturaField.setTOTMntIVaenSusp(Math.round(TotalMontoIVAEnSuspenso * Math.pow(10, 2)) / Math.pow(10, 2));
					clsDGIXMLFacturaField.setTOTMntIVaenSuspSpecified(true);
				} else {
					clsDGIXMLFacturaField.setTOTMntIVaenSuspSpecified(false);
				}

				if (TotalMontoNetoIVATasaMinima != 0 && CantDeDecimales(TotalMontoNetoIVATasaMinima) > 2) {
					throw new TAException("Monto Total de IVA Tasa Minima debe tener 2 lugares decimales. - Recibido :" + TotalMontoNetoIVATasaMinima, 4035);
				}

				//Total monto IVA Tasa minima no viaja en eRemitos, eResguardo, exportaciones y eBoleta
				if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoleta.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
					clsDGIXMLFacturaField.setTOTMntNetoIvaTasaMin(Math.round(TotalMontoNetoIVATasaMinima * Math.pow(10, 2)) / Math.pow(10, 2));
					clsDGIXMLFacturaField.setTOTMntNetoIvaTasaMinSpecified(true);
				} else {
					clsDGIXMLFacturaField.setTOTMntNetoIvaTasaMinSpecified(false);
				}

				if (TotalMontoNetoIVATasaBasica != 0 && CantDeDecimales(TotalMontoNetoIVATasaBasica) > 2) {
					throw new TAException("Monto Total de IVA Tasa Basica debe tener 2 lugares decimales. - Recibido :" + TotalMontoNetoIVATasaBasica, 4036);
				}

				//Total monto IVA tasa basica no viaja en eRemitos, eResguardo, exportaciones y eBoleta
				if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoleta.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
					clsDGIXMLFacturaField.setTOTMntNetoIVATasaBasica(Math.round(TotalMontoNetoIVATasaBasica * Math.pow(10, 2)) / Math.pow(10, 2));
					clsDGIXMLFacturaField.setTOTMntNetoIVATasaBasicaSpecified(true);
				} else {
					clsDGIXMLFacturaField.setTOTMntNetoIVATasaBasicaSpecified(false);
				}

				if (TotalMontoNetoIVAOtraTasa > 0 && TotalIVAOtraTasa <= 0) {
					throw new TAException("Si especifica Monto Neto IVA Otra Tasa debe especificar Monto IVA Otra Tasa . - Recibido :" + TotalIVAOtraTasa, 4306);
				}

				if (TotalMontoNetoIVAOtraTasa != 0 && CantDeDecimales(TotalMontoNetoIVAOtraTasa) > 2) {
					throw new TAException("Monto Total de IVA Otra Tasa debe tener 2 lugares decimales. - Recibido :" + TotalMontoNetoIVAOtraTasa, 4037);
				}

				//Total monto IVA otra tasa no viaja en eRemitos, eResguardo, exportaciones y eBoleta
				if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoleta.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
					clsDGIXMLFacturaField.setTOTMntNetoIVAOtra(Math.round(TotalMontoNetoIVAOtraTasa * Math.pow(10, 2)) / Math.pow(10, 2));
					clsDGIXMLFacturaField.setTOTMntNetoIVAOtraSpecified(true);
				} else {
					clsDGIXMLFacturaField.setTOTMntNetoIVAOtraSpecified(false);
				}

				if (TasaMinimaIVA != 0 && CantDeDecimales(TasaMinimaIVA) > 3) {
					throw new TAException("Tasa Minima de IVA debe tener 3 lugares decimales. - Recibido :" + TasaMinimaIVA, 4038);
				}

				//Total IVA tasa minima no viaja en eRemitos, eResguardo, exportaciones y eBoleta
				if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoleta.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
					clsDGIXMLFacturaField.setTOTIVATasaMin(Math.round(TasaMinimaIVA * Math.pow(10, 3)) / Math.pow(10, 3));
					clsDGIXMLFacturaField.setTOTIVATasaMinSpecified(true);
				} else {
					clsDGIXMLFacturaField.setTOTIVATasaMinSpecified(false);
				}

				if (TasaBasicaIVA != 0 && CantDeDecimales(TasaBasicaIVA) > 3) {
					throw new TAException("Tasa Basica de IVA debe tener 3 lugares decimales. - Recibido :" + TasaBasicaIVA, 4039);
				}

				//Total monto IVA tasa basica no viaja en eRemitos, eResguardo, exportaciones y eBoleta
				if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoleta.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
					clsDGIXMLFacturaField.setTOTIVATasaBasica(Math.round(TasaBasicaIVA * Math.pow(10, 3)) / Math.pow(10, 3));
					clsDGIXMLFacturaField.setTOTIVATasaBasicaSpecified(true);
				} else {
					clsDGIXMLFacturaField.setTOTIVATasaBasicaSpecified(false);
				}

				if (TotalIVATasaMinima != 0 && CantDeDecimales(TotalIVATasaMinima) > 2) {
					throw new TAException("Total de IVA Tasa Minima debe tener 2 lugares decimales. - Recibido :" + TotalIVATasaMinima, 4040);
				}

				//Total monto IVA tasa minima no viaja en eRemitos, eResguardo, exportaciones y eBoleta
				if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoleta.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
					clsDGIXMLFacturaField.setTOTMntIVATasaMin(Math.round(TotalIVATasaMinima * Math.pow(10, 2)) / Math.pow(10, 2));
					clsDGIXMLFacturaField.setTOTMntIVATasaMinSpecified(true);
				} else {
					clsDGIXMLFacturaField.setTOTMntIVATasaMinSpecified(false);
				}

				if (TotalIVATasaBasica != 0 && CantDeDecimales(TotalIVATasaBasica) > 2) {
					throw new TAException("Total de IVA Tasa Basica debe tener 2 lugares decimales. - Recibido :" + TotalIVATasaBasica, 4041);
				}

				//Total IVA tasa basica no viaja en eRemitos, eResguardo, exportaciones y eBoleta
				if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoleta.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
					clsDGIXMLFacturaField.setTOTMntIVATasaBasica(Math.round(TotalIVATasaBasica * Math.pow(10, 2)) / Math.pow(10, 2));
					clsDGIXMLFacturaField.setTOTMntIVATasaBasicaSpecified(true);
				} else {
					clsDGIXMLFacturaField.setTOTMntIVATasaBasicaSpecified(false);
				}

				if (TotalIVAOtraTasa != 0 && CantDeDecimales(TotalIVAOtraTasa) > 2) {
					throw new TAException("Total de IVA Otra Tasa debe tener 2 lugares decimales. - Recibido :" + TotalIVAOtraTasa, 4042);
				}

				//Total IVA otra tasa no viaja en eRemitos, eResguardo, exportaciones y eBoleta
				if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoleta.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
					clsDGIXMLFacturaField.setTOTMntIVAOtra(Math.round(TotalIVAOtraTasa * Math.pow(10, 2)) / Math.pow(10, 2));
					clsDGIXMLFacturaField.setTOTMntIVAOtraSpecified(true);
				} else {
					clsDGIXMLFacturaField.setTOTMntIVAOtraSpecified(false);
				}

				if (TotalMontoTotal != 0 && CantDeDecimales(TotalMontoTotal) > 2) {
					throw new TAException("Monto Total debe tener 2 lugares decimales. - Recibido :" + TotalMontoTotal, 4043);
				}

				if (nTipoApi == enumTipoApi.TAFE) {
					if (TotalMontoTotal < 0 && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
						throw new TAException("Monto Total no puede ser negativo. - Recibido :" + TotalMontoTotal, 4044);
					}
				}

				//Total monto total no viaja en eRemitos y eResguardo
				if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()) {
					clsDGIXMLFacturaField.setTOTMntTotal(Math.round(TotalMontoTotal * Math.pow(10, 2)) / Math.pow(10, 2));
					clsDGIXMLFacturaField.setTOTMntTotalSpecified(true);
				} else {
					clsDGIXMLFacturaField.setTOTMntTotalSpecified(false);
				}

				if (TotalMontoRetenido != 0 && CantDeDecimales(TotalMontoRetenido) > 2) {
					throw new TAException("Monto Total Retenido debe tener 2 lugares decimales. - Recibido :" + TotalMontoRetenido, 4045);
				}

				//Total monto retenido no viaja en eRemitos y exportaciones
				if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
					clsDGIXMLFacturaField.setTOTMntTotRetenido(Math.round(TotalMontoRetenido * Math.pow(10, 2)) / Math.pow(10, 2));
					clsDGIXMLFacturaField.setTOTMntTotRetenidoSpecified(true);
				} else {
					clsDGIXMLFacturaField.setTOTMntTotRetenidoSpecified(false);
				}

				if (TotalMontoCreditoFiscal != 0 && CantDeDecimales(TotalMontoCreditoFiscal) > 2) {
					throw new TAException("Monto Total Credito Fiscal debe tener 2 lugares decimales. - Recibido :" + TotalMontoCreditoFiscal, 4046);
				}

				//Total monto retenido no viaja en eRemitos, exportaciones y eBoleta
				if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoleta.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
					clsDGIXMLFacturaField.setTOTMntTotCredFisc(Math.round(TotalMontoCreditoFiscal * Math.pow(10, 2)) / Math.pow(10, 2));
					clsDGIXMLFacturaField.setTOTMntTotCredFiscSpecified(true);
				} else {
					clsDGIXMLFacturaField.setTOTMntTotCredFiscSpecified(false);
				}
			}

			if (CantLineasDetalle < 1) {
				throw new TAException("El comprobante debe contener al menos 1 linea de detalle. - Recibido :" + CantLineasDetalle, 4047);
			}

			clsDGIXMLFacturaField.setTOTCantLinDet((short) CantLineasDetalle);

			if (MontoNoFacturable != 0 && CantDeDecimales(MontoNoFacturable) > 2) {
				throw new TAException("Monto No Facturable debe tener 2 lugares decimales. - Recibido :" + CantDeDecimales(MontoNoFacturable), 4048);
			}

			//Monto no facturable no viaja en eRemitos y exportaciones
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemito.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
				clsDGIXMLFacturaField.setTOTMontoNF(Math.round(MontoNoFacturable * Math.pow(10, 2)) / Math.pow(10, 2));
				clsDGIXMLFacturaField.setTOTMontoNFSpecified(true);
			} else {
				clsDGIXMLFacturaField.setTOTMontoNFSpecified(false);
			}

			if (MontoTotalAPagar != 0 && CantDeDecimales(MontoTotalAPagar) > 2) {
				throw new TAException("Monto Total a Pagar debe tener 2 lugares decimales. - Recibido :" + MontoTotalAPagar, 4049);
			}

			//Monto total a pagar no viaja en eRemitos
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemito.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()) {
				clsDGIXMLFacturaField.setTOTMntPagar(Math.round(MontoTotalAPagar * Math.pow(10, 2)) / Math.pow(10, 2));
				clsDGIXMLFacturaField.setTOTMntPagarSpecified(true);
			} else {
				clsDGIXMLFacturaField.setTOTMntPagarSpecified(false);
			}

			if (nTipoApi == enumTipoApi.TAFE) {
				//***Valido Montos >= 0 (No se permiten Negativos)***
				if (TipoDeCambio < 0) {
					throw new TAException("Tipo de Cambio no puede ser Negativo. - Recibido: " + TipoDeCambio, 4050);
				}
				if (TotalMontoNoGravado < 0) {
					throw new TAException("Monto Total No Gravado no puede ser Negativo. - Recibido :" + TotalMontoNoGravado, 4051);
				}
				if ((TotalMontoExportacionYAsimiladas < 0) && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
					throw new TAException("Monto Total de Exportacion y Asimiladas no puede ser Negativo. - Recibido :" + TotalMontoExportacionYAsimiladas, 4052);
				}
				if (TotalMontoImpuestoPercibido < 0) {
					throw new TAException("Monto Total de Impuesto Percibido no puede ser Negativo. - Recibido :" + TotalMontoImpuestoPercibido, 4053);
				}
				if (TotalMontoIVAEnSuspenso < 0) {
					throw new TAException("Monto Total de IVA en Suspenso no puede ser Negativo. - Recibido :" + TotalMontoIVAEnSuspenso, 4054);
				}
				if (TotalMontoNetoIVATasaMinima < 0) {
					throw new TAException("Monto Total de IVA Tasa Minima no puede ser Negativo. - Recibido :" + TotalMontoNetoIVATasaMinima, 4055);
				}
				if (TotalMontoNetoIVATasaBasica < 0) {
					throw new TAException("Monto Total de IVA Tasa Basica no puede ser Negativo. - Recibido :" + TotalMontoNetoIVATasaBasica, 4056);
				}
				if (TotalMontoNetoIVAOtraTasa < 0) {
					throw new TAException("Monto Total de IVA Otra Tasa no pude ser Negativo. - Recibido :" + TotalMontoNetoIVAOtraTasa, 4057);
				}
				if (TasaMinimaIVA < 0) {
					throw new TAException("Tasa Minima de IVA no puede ser Negativo. - Recibido :" + TasaMinimaIVA, 4058);
				}
				if (TasaBasicaIVA < 0) {
					throw new TAException("Tasa Basica de IVA no puede ser Negativo. - Recibido :" + TasaBasicaIVA, 4059);
				}
				if (TotalIVATasaMinima < 0) {
					throw new TAException("Total de IVA Tasa Minima no puede ser Negativo. - Recibido :" + TotalIVATasaMinima, 4060);
				}
				if (TotalIVATasaMinima < 0) {
					throw new TAException("Total de IVA Tasa Minima no puede ser Negativo. - Recibido :" + TotalIVATasaMinima, 4061);
				}
				if (TotalIVATasaBasica < 0) {
					throw new TAException("Total de IVA Tasa Basica no puede ser Negativo. - Recibido :" + TotalIVATasaBasica, 4062);
				}
				if (TotalIVAOtraTasa < 0) {
					throw new TAException("Total de IVA Otra Tasa no puede ser Negativo. - Recibido :" + TotalIVAOtraTasa, 4063);
				}
				if ((TotalMontoTotal < 0) && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
					throw new TAException("Monto Total no puede ser Negativo. - Recibido :" + TotalMontoTotal, 4064);
				}
			}

			//*** Fin Validacion de Datos ***

			XMLFACTURAField.setDGI(clsDGIXMLFacturaField);
			DGIA5 = true;

		} catch (TAException ex) {
			ErrorCod.argValue = 8;
			ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
			try {
				objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
				DGIA5 = false;
				throw ex;
			} catch (UnsupportedEncodingException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (UnsupportedEncodingException ex) {
			throw new TAException(ex.getMessage());
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
		return DGIA5;
	}

	public final boolean DGIJ1setAdenda(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, tangible.RefObject<String> AdendaTexto, byte[] AdendaOtro) throws TAException {

		boolean DGIJ1 = false;
		try {
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			if (!((AdendaOtro == null)) && AdendaOtro.length < 5) {
				AdendaOtro = null;
			}
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "tangible.RefObject<String> AdendaTexto", "byte[] AdendaOtro"};
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, AdendaTexto.argValue, AdendaOtro};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

			//Valido que haya una factura iniciada
			FacturaIniciada();
			//Elimino espacios 
			AdendaTexto.argValue = CNullStr(AdendaTexto.argValue).trim();

			clsDGIXMLFacturaField.setADETextoADE(AdendaTexto.argValue);
			clsDGIXMLFacturaField.setADEOtroADE(AdendaOtro);
			XMLFACTURAField.setDGI(clsDGIXMLFacturaField);
			DGIJ1 = true;

		} catch (TAException ex) {
			try {
				ErrorCod.argValue = 8;
				ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
				objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
				DGIJ1 = false;
				throw ex;
			} catch (UnsupportedEncodingException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (UnsupportedEncodingException ex) {
			throw new TAException(ex.getMessage());
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
		return DGIJ1;
	}

	public final boolean DGIA6addTotalRetencionOPercepcion(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String CodigoDeRetencionPercepcion, double ValorRetencionPercepcion) throws TAException {

		boolean DGI6 = false;
		try {
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "String CodigoDeRetencionPercepcion", "double ValorRetencionPercepcion"};
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, CodigoDeRetencionPercepcion, ValorRetencionPercepcion};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

			//Valido que haya una factura iniciada
			FacturaIniciada();
			//Elimino espacios
			CodigoDeRetencionPercepcion = CNullStr(CodigoDeRetencionPercepcion).trim();


			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemito.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
				throw new TAException("No se permiten retenciones o percepciones sobre eRemitos y/o documentos de exportacion. ", 4067);
			}
			//Si ya habia alcanzado la cantidad maxima no permito seguir agregando
			if (listaTOTRetencPercepsArray.size() == MAXlstTOTRetencPercep) {
				throw new TAException("Cantidad Maxima de Totales de Retencion o Percepcion alcanzada (Zona A) Max: " + MAXlstTOTRetencPercep, 4068);
			}
			if (CantDeDecimales(ValorRetencionPercepcion) > 2) {
				throw new TAException("Valor de Retencion o Percepcion debe tener 2 lugares decimales. - Recibido :" + ValorRetencionPercepcion, 4069);
			}
			if (CodigoDeRetencionPercepcion.trim().equals("")) {
				throw new TAException("Codigo de Retencion o Percepcion no puede ser vacio.", 4070);
			}

			if (CodigoRetenPercepValido(CodigoDeRetencionPercepcion) == false) {
				throw new TAException("Codigo de Retencion o Percepcion no cumple el formato esperado XXXX-YYY, XXXX/YYY o XXXXYYYY(De 6 a 8 caracteres), Codigo Recibido " + CodigoDeRetencionPercepcion + ".", 4307);
			}

			//Si ya existia alguna Retencion/Percepcion con el mismo codigo, retorno error y le recomiendo crear otra linea.
			if (listaDetalleSubRetencPercepArray.size() > 0) {
				for (int i = 0; i < listaTOTRetencPercepsArray.size(); i++) {
					if (CodigoDeRetencionPercepcion.equals(listaTOTRetencPercepsArray.get(i).getTOTCodRet())) {
						throw new TAException("El Codigo de la Retencion/Percepcion que esta intentando agregar ya se encuentra en el encabezado. - Recibido :" + CodigoDeRetencionPercepcion, 4108);
					}
				}
			}

			//Creo Instancia, seteo propiedades y agrego a lista
			TOTRetencPercep totRetencP = lstTOTRetencPercep.new TOTRetencPercep();
			totRetencP.setTOTCodRet(LargoMax(CodigoDeRetencionPercepcion, 8));
			totRetencP.setTOTValRetPerc(Math.round(ValorRetencionPercepcion * Math.pow(10, 2)) / Math.pow(10, 2));
			totRetencP.setTOTValRetPercSpecified(true);
			listaTOTRetencPercepsArray.add(totRetencP);
			TOTRetencPercep[] tempListTOTRetencPercep = new TOTRetencPercep[listaTOTRetencPercepsArray.size()];
			lstTOTRetencPercep.setTOTRetencPercep(listaTOTRetencPercepsArray);
			clsDGIXMLFacturaField.setTOTRetencPerceps(lstTOTRetencPercep);
			XMLFACTURAField.setDGI(clsDGIXMLFacturaField);
			DGI6 = true;

		} catch (TAException ex) {
			try {
				ErrorCod.argValue = 8;
				ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
				objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
				DGI6 = false;
				throw ex;
			} catch (UnsupportedEncodingException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (UnsupportedEncodingException ex) {
			throw new TAException(ex.getMessage());
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
		return DGI6;
	}

	public final boolean DGIB1addDetalleNuevaLinea(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, ITAParsing_v0206.enumIndicidaorDeFacturacionDetalle IndicadorDeFacturacionaux, String IndicadorAgenteResponsable, String NombreDelItem, String DescripcionAdicional, double Cantidad, String UnidadDeMedida, double PrecioUnitario, double DescuentoEnPorcentaje, double MontoDescuento, double RecargoEnPorcentaje, double MontoRecargo, double MontoItem) throws TAException {

		boolean DGIB1 = false;
		try {
			int IndicadorDeFacturacion = 0;
			if (IndicadorDeFacturacionaux != null && IndicadorDeFacturacionaux.getValue() != 0) {
				IndicadorDeFacturacion = IndicadorDeFacturacionaux.getValue();
			}
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "ITAParsing_v0206.enumIndicidaorDeFacturacionDetalle IndicadorDeFacturacion", "String IndicadorAgenteResponsable", "String NombreDelItem", "String DescripcionAdicional", "double Cantidad", "String UnidadDeMedida", "double PrecioUnitario", "double DescuentoEnPorcentaje", "double MontoDescuento", "double RecargoEnPorcentaje", "double MontoRecargo", "double MontoItem"};
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, IndicadorDeFacturacion, IndicadorAgenteResponsable, NombreDelItem, DescripcionAdicional, Cantidad, UnidadDeMedida, PrecioUnitario, DescuentoEnPorcentaje, MontoDescuento, RecargoEnPorcentaje, MontoRecargo, MontoItem};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

			//Creo Instancia, seteo propiedades y agrego a lista
			DETALLE objDetalle = lstDetalle.new DETALLE();
			short NroDeLinea = 0;
			//Valido que haya una factura iniciada
			FacturaIniciada();
			//Elimino espacios
			IndicadorAgenteResponsable = CNullStr(IndicadorAgenteResponsable).trim();
			NombreDelItem = CNullStr(NombreDelItem).trim();
			DescripcionAdicional = CNullStr(DescripcionAdicional).trim();
			UnidadDeMedida = CNullStr(UnidadDeMedida).trim();

			//*** Validacion de Datos***
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoleta.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
					|| XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
				if (IndicadorDeFacturacion != enumIndicadorDeFacturacionDetalle.ItemVendidoContribuyenteIMEBA.getValue()
						&& IndicadorDeFacturacion != enumIndicadorDeFacturacionDetalle.ItemVendidoPorNoContribuyente.getValue()
						&& IndicadorDeFacturacion != enumIndicadorDeFacturacionDetalle.ItemVendidoContribuyenteMonotributo.getValue()) {
					throw new TAException("Indicador de facturacion invalido. En CFE eBoleta de entrada y sus notas de correccion solo se acepta ItemVendidoContribuyenteIMEBA, ItemVendidoPorNoContribuyente e ItemVendidoContribuyenteMonotributo. - Recibido :" + IndicadorDeFacturacion, 4169);
				}
			}

			if (IndicadorDeFacturacion == enumIndicadorDeFacturacionDetalle.ItemVendidoContribuyenteIMEBA.getValue()
					|| IndicadorDeFacturacion == enumIndicadorDeFacturacionDetalle.ItemVendidoPorNoContribuyente.getValue()
					|| IndicadorDeFacturacion == enumIndicadorDeFacturacionDetalle.ItemVendidoContribuyenteMonotributo.getValue()) {
				if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoleta.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
						&& XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
					throw new TAException("Indicador de facturacion invalido. Solo se usa en CFE eBoleta de entrada y sus notas de correccion. - Recibido :" + IndicadorDeFacturacion, 4169);
				}
			}


			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemito.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
				if (IndicadorDeFacturacion != 0 && enumIndicadorDeFacturacionDetalle.forValue(IndicadorDeFacturacion).getValue() != IndicadorDeFacturacion) {
					throw new TAException("Indicador de facturacion invalido - Recibido :" + IndicadorDeFacturacion, 4169);
				}
			} else {
				if (nTipoApi == enumTipoApi.TAFE) {
					if (IndicadorDeFacturacion != 0 && IndicadorDeFacturacion == enumIndicadorDeFacturacionDetalle.ItemAAjustarEnResguardo.getValue()) {
						if ((XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue())) {
							throw new TAException("Indicador de facturacion invalido. Solo se usa indicador AjustarEnResguardo en CFE eResguardo o eResguardoContingencia. - Recibido :" + IndicadorDeFacturacion, 4169);
						}
					}
					if (IndicadorDeFacturacion != 0 && IndicadorDeFacturacion == enumIndicadorDeFacturacionDetalle.ItemARebajarEnRemito.getValue()) {
						if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemito.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
							throw new TAException("Indicador de facturacion invalido. Solo se usa indicador RebajarEnRemito en CFE eRemito, eRemitoExportacion, eRemitoContingencia o eRemitoExportacionContingencia. - Recibido :" + IndicadorDeFacturacion, 4169);
						}
					}
				}
			}

			objDetalle.setDETIndFact((byte) IndicadorDeFacturacion);
			if (IndicadorAgenteResponsable.equals("R") || IndicadorAgenteResponsable.equals("A")) {
				if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemito.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
					objDetalle.setDETIndAgenteResp(LargoMax(IndicadorAgenteResponsable, 1));
				} else {
					objDetalle.setDETIndAgenteResp("");
				}
			} else {
				objDetalle.setDETIndAgenteResp("");
			}

			if (nTipoApi == enumTipoApi.TAFE) {
				if ((NombreDelItem.trim() == null || NombreDelItem.trim().length() == 0) && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()) {
					throw new TAException("Nombre del Producto o Servicio es obligatorio.", 4071);
				}
			}

			//Si comprobante es eResguardo no va el nombre del Item
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()) {
				objDetalle.setDETNomItem(LargoMax(NombreDelItem, 80));
			} else {
				objDetalle.setDETNomItem("");
			}

			//Si comprobante es eResguardo no va la descripcion del item
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()) {
				objDetalle.setDETDscItem(LargoMax(DescripcionAdicional, 1000));
			} else {
				objDetalle.setDETDscItem("");
			}

			if (CantDeDecimales(Cantidad) > 3) {
				throw new TAException("Cantidad del Producto o Servicio debe tener 3 lugares decimales. - Recibido :" + Cantidad, 4072);
			}
			if (nTipoApi == enumTipoApi.TAFE) {
				if (Cantidad == 0 && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()) {
					throw new TAException("Cantidad del Producto o Servicio no puede ser 0", 4073);
				}
				if (IndicadorDeFacturacion != 0 && Cantidad < 0 && IndicadorDeFacturacion != enumIndicadorDeFacturacionDetalle.NoFacturableNegativo.getValue()) {
					throw new TAException("Cantidad del Producto o Servicio no puede ser negativa. - Recibida :" + Cantidad, 4074);
				}
			}

			objDetalle.setDETCantidad(Math.round(Cantidad * Math.pow(10, 3)) / Math.pow(10, 3));
			objDetalle.setDETCantidadSpecified(true);

			//Si comprobante es eResguardo no va la unidad de medida
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()) {
				if (UnidadDeMedida.trim().equals("")) {
					objDetalle.setDETUniMed("N/A");
				} else {
					objDetalle.setDETUniMed(LargoMax(UnidadDeMedida, 4));
				}
			} else {
				objDetalle.setDETUniMed("");
			}

			//Si comprobante es eResguardo o eRemito no va el precio unitario
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemito.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoContingencia.getValue()) {
				if (CantDeDecimales(PrecioUnitario) > 6) {
					throw new TAException("Precio Unitario del Producto o Servicio debe tener hasta 6 lugares decimales. - Recibido :" + PrecioUnitario, 4075);
				}
				if (nTipoApi == enumTipoApi.TAFE) {
					if (PrecioUnitario < 0) {
						throw new TAException("Precio Unitario del Producto o Servicio no puede ser Negativo. - Recibido :" + PrecioUnitario, 4076);
					}

					/**
					 * *************NO SE TOMA EN CUENA ESTA
					 * VALIDACION PARA LA VERSION 2.05****************
					 */
//                    if (IndicadorDeFacturacion != 0 && PrecioUnitario == 0 && IndicadorDeFacturacion != enumIndicadorDeFacturacionDetalle.EntregaGratuita.getValue()) {
//                        throw new TAException("Precio Unitario del Producto o Servicio debe ser mayor que 0 si el indicador de facturacion es diferente a Entrega Gratuita. - Precio Recibido :" + PrecioUnitario + " - Indicador Recibido :" + IndicadorDeFacturacion, 4308);
//                    }
				}

				if (CantDeEnteros(Math.round(PrecioUnitario)) > 11) {
					throw new TAException("Precio Unitario del Producto o Servicio debe tener hasta 11 enteros. - Recibido :" + PrecioUnitario, 4294);
				}

				objDetalle.setDETPrecioUnitario(Math.round(PrecioUnitario * Math.pow(10, 6)) / Math.pow(10, 6));
				objDetalle.setDETPrecioUnitarioSpecified(true);
			} else {
				objDetalle.setDETPrecioUnitarioSpecified(false);
			}
			if (CantDeDecimales(DescuentoEnPorcentaje) > 3) {
				throw new TAException("Descuento en % del Producto o Servicio debe tener 3 lugares decimales. - Recibido :" + DescuentoEnPorcentaje, 4077);
			}
			if (DescuentoEnPorcentaje < 0) {
				throw new TAException("Descuento en % del Producto o Servicio no puede ser Negativo. - Recibido :" + DescuentoEnPorcentaje, 4078);
			}

			//Si comprobante es eResguardo , eRemito o eRemito Exportarcion no va el Descuento en porcentaje 
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemito.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
				objDetalle.setDETDescuentoPct(Math.round(DescuentoEnPorcentaje * Math.pow(10, 3)) / Math.pow(10, 3));
				objDetalle.setDETDescuentoPctSpecified(true);
			} else {
				objDetalle.setDETDescuentoPctSpecified(false);
			}

			if (CantDeDecimales(MontoDescuento) > 2) {
				throw new TAException("Monto de Descuento del Producto o Servicio debe tener 2 lugares decimales. - Recibido :" + MontoDescuento, 4079);
			}
			if (nTipoApi == enumTipoApi.TAFE) {
				if (MontoDescuento < 0) {
					throw new TAException("Monto de Descuento del Producto o Servicio no puede ser Negativo. - Recibido :" + MontoDescuento, 4080);
				}
				if (DescuentoEnPorcentaje != 0 && MontoDescuento == 0) {
					throw new TAException("Si existe % Descuento en Linea, debe enviar monto de descuento. - Recibido :" + MontoDescuento, 4081);
				}
			}

			//Si comprobante es eResguardo , eRemito o eRemito Exportarcion no va el Monto Descuento
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemito.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
				objDetalle.setDETDescuentoMonto(Math.round(MontoDescuento * Math.pow(10, 2)) / Math.pow(10, 2));
				objDetalle.setDETDescuentoMontoSpecified(true);
			} else {
				objDetalle.setDETDescuentoMontoSpecified(false);
			}

			if (CantDeDecimales(RecargoEnPorcentaje) > 3) {
				throw new TAException("Recargo en % del Producto o Servicio debe tener 3 lugares decimales. - Recibido :" + RecargoEnPorcentaje, 4082);
			}
			if (RecargoEnPorcentaje < 0) {
				throw new TAException("Recargo en % del Producto o Servicio no puede ser Negativo. - Recibido :" + RecargoEnPorcentaje, 4083);
			}

			//Si comprobante es eResguardo , eRemito o eRemito Exportarcion no va el Recargo porcentaje
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemito.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
				objDetalle.setDETRecargoPct(Math.round(RecargoEnPorcentaje * Math.pow(10, 3)) / Math.pow(10, 3));
				objDetalle.setDETRecargoPctSpecified(true);
			} else {
				objDetalle.setDETRecargoPctSpecified(false);
			}

			if (CantDeDecimales(MontoRecargo) > 2) {
				throw new TAException("Monto de Recargo del Producto o Servicio debe tener 2 lugares decimales. - Recibido :" + MontoRecargo, 4084);
			}
			if (nTipoApi == enumTipoApi.TAFE) {
				if (MontoRecargo < 0) {
					throw new TAException("Monto de Recargo del Producto o Servicio no puede ser Negativo. - Recibido :" + MontoRecargo, 4085);
				}
			}

			if (nTipoApi == enumTipoApi.TAFE) {
				if (RecargoEnPorcentaje != 0 && MontoRecargo == 0) {
					throw new TAException("Si existe % Recargo en Linea, debe enviar monto de recargo. - Recibido :" + MontoRecargo, 4086);
				}
			}

			//Si comprobante es eResguardo , eRemito o eRemito Exportarcion no va el Monto Recargo
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemito.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
				objDetalle.setDETRecargoMnt(Math.round(MontoRecargo * Math.pow(10, 2)) / Math.pow(10, 2));
				objDetalle.setDETRecargoMntSpecified(true);
			} else {
				objDetalle.setDETRecargoMntSpecified(false);
			}

			if (CantDeDecimales(MontoItem) > 2) {
				throw new TAException("Monto del Producto o Servicio debe tener 2 lugares decimales. - Recibido :" + MontoItem, 4087);
			}
			if (nTipoApi == enumTipoApi.TAFE) {
				if (IndicadorDeFacturacion != 0 && MontoItem < 0 && IndicadorDeFacturacion != enumIndicadorDeFacturacionDetalle.NoFacturableNegativo.getValue()) {
					throw new TAException("Monto del Producto o Servicio no puede ser Negativo. - Recibido :" + MontoItem, 4088);
				}
				if (IndicadorDeFacturacion != 0 && MontoItem != 0 && IndicadorDeFacturacion == enumIndicadorDeFacturacionDetalle.EntregaGratuita.getValue()) {
					throw new TAException("Monto del Producto o Servicio debe ser cero si el indicador es 'Entrega Gratuita'. - Recibido :" + MontoItem + " - Indicador: " + IndicadorDeFacturacion, 4089);
				}
			}


			//Si comprobante es eResguardo , eRemito o eRemito no va el Monto del item
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemito.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoContingencia.getValue()) {
				double MargenErrorPermitidoMontos = 0;
				String MargenErrorPermitidoMontosStr = null;
				MargenErrorPermitidoMontosStr = MAXMargenErrorMontos;
				if (isNumeric(MargenErrorPermitidoMontosStr) == true) {
					MargenErrorPermitidoMontos = Val(MargenErrorPermitidoMontosStr);
				}

				double SumaMontoItem = 0;
				SumaMontoItem = (Cantidad * PrecioUnitario) - MontoDescuento + MontoRecargo;
				if (nTipoApi == enumTipoApi.TAFE) {
					if (Math.abs(Math.round(MontoItem * Math.pow(10, 2)) / Math.pow(10, 2) - Math.round(SumaMontoItem * Math.pow(10, 2)) / Math.pow(10, 2)) > MargenErrorPermitidoMontos) {
						throw new TAException("Monto del Producto o Servicio debe ser (Cantidad * Precio Unitario) - Monto de Descuento + Monto de Recargo. - Recibido :" + MontoItem + " - Esperado :" + SumaMontoItem, 5006);
					}
				}
				objDetalle.setDETMontoItem(Math.round(MontoItem * Math.pow(10, 2)) / Math.pow(10, 2));
				objDetalle.setDETMontoItemSpecified(true);
			} else {
				objDetalle.setDETMontoItemSpecified(false);
			}


			//*** Fin Validacion de Datos ***

			NroDeLinea = (short) (listaDetallesArray.size() + 1);
			objDetalle.setDETNroLinDet(NroDeLinea);
			listaDetallesArray.add(objDetalle);
			cantDetalle++;
			DETALLE[] tempListDETALLE = new DETALLE[listaDetallesArray.size()];
			lstDetalle.setDetalle(listaDetallesArray);

			//Si ya habia alcanzado la cantidad maxima no permito seguir agregando
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicket.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketNotaCred.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketNotaDeb.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketNotaCredContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eTicketNotaDebContingencia.getValue()) {
				if (lstDetalle.getDetalle().size() > MAXlstDetalleETICK) {
					throw new TAException("Cantidad Maxima de Lineas para eTicket alcanzada (Zona B) Max: " + MAXlstDetalleETICK, 4090);
				}
			} else {
				if (lstDetalle.getDetalle().size() > MAXlstDetalleOTROS) {
					throw new TAException("Cantidad Maxima de Lineas para comprobante alcanzada (Zona B) Max: " + MAXlstDetalleOTROS, 4091);
				}
			}

			clsDGIXMLFacturaField.setDetalles(lstDetalle);
			XMLFACTURAField.setDGI(clsDGIXMLFacturaField);
			//limpio lista detalle retenc percep
			listaDetalleSubRetencPercepArray = new ArrayList<XMLFACTURA.clsDGI.Detalle.DETRetencPerceps.DETRetencPercep>();
			listDetalleSubDescuentoArray = new ArrayList<DETSubDescuento>();
			listDetalleSubRecargoArray = new ArrayList<Detalle.DETSubRecargos.DETSubRecargo>();
			listDetalleCodItemArray = new ArrayList<XMLFACTURA.clsDGI.Detalle.DETCodItems.DETCodItem>();
			lstaDetalleSubRetencPercep = lstDetalle.new DETRetencPerceps();
			lstDetalleSubDescuento = lstDetalle.new DETSubDescuentos();
			lstDetalleSubRecargo = lstDetalle.new DETSubRecargos();
			lstDetalleCodItem = lstDetalle.new DETCodItems();
			DGIB1 = true;

		} catch (TAException ex) {
			try {
				ErrorCod.argValue = 8;
				ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
				objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
				DGIB1 = false;
				throw ex;
			} catch (UnsupportedEncodingException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (UnsupportedEncodingException ex) {
			throw new TAException(ex.getMessage());
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
		return DGIB1;
	}

	public final boolean DGIB2addDetalleLineaActualNuevoCodigoItem(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, ITAParsing_v0206.enumTipoDeCodigoItem TipoDeCodigoItemaux, String CodigoDelItem) throws TAException {

		boolean DGIB2 = false;
		try {
			enumTipoDeCodigoItem TipoDeCodigoItem = enumTipoDeCodigoItem.forValue(TipoDeCodigoItemaux.getValue());
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "ITAParsing_v0206.enumTipoDeCodigoItem TipoDeCodigoItemaux", "String CodigoDelItem"};
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, TipoDeCodigoItem, CodigoDelItem};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

			//Valido que haya una factura iniciada
			FacturaIniciada();
			//Elimino espacios
			CodigoDelItem = CNullStr(CodigoDelItem).trim();
			//Valido que haya una Linea creada
			LineaDeDetalleCreada();

			if (enumTipoDeCodigoItem.forValue(TipoDeCodigoItem.getValue()) != TipoDeCodigoItem) {
				if (nTipoApi == enumTipoApi.TAFE) {
					throw new TAException("Tipo de codigo item invalido - Recibido :" + TipoDeCodigoItem.toString(), 4170);
				} else {
					TipoDeCodigoItem = enumTipoDeCodigoItem.INT1;
				}
			}

			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eResguardo.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()) {
				throw new TAException("No se puede expecificar codigo de item en los eResguardos", 4092);
			}

			if (CodigoDelItem.trim().equals("")) {
				if (nTipoApi == enumTipoApi.TAFE) {
					throw new TAException("Codigo de item no puede ser vacio.", 4093);
				} else {
					CodigoDelItem = "{Sin Definir}";
				}
			}

			//Si ya habia alcanzado la cantidad maxima no permito seguir agregando
			if (listDetalleCodItemArray.size() == MAXlstDetalleCodItem) {
				throw new TAException("Cantidad Maxima de Codigos de producto en una linea alcanzada (Zona B) Max: " + MAXlstDetalleCodItem, 4094);
			}

			//Creo Instancia, seteo propiedades y agrego a lista
			XMLFACTURA.clsDGI.Detalle.DETCodItems.DETCodItem objDetalleCodItem = lstDetalleCodItem.new DETCodItem();
			objDetalleCodItem.setDETTpoCod(LargoMax(TipoDeCodigoItem.toString(), 10));
			objDetalleCodItem.setDETCod(LargoMax(CodigoDelItem, 35));

			listDetalleCodItemArray.add(objDetalleCodItem);
			XMLFACTURA.clsDGI.Detalle.DETCodItems.DETCodItem[] tempDetalleCodItem = new XMLFACTURA.clsDGI.Detalle.DETCodItems.DETCodItem[listDetalleCodItemArray.size()];
			tempDetalleCodItem = listDetalleCodItemArray.toArray(tempDetalleCodItem);
			lstDetalleCodItem.setDETCodItem(listDetalleCodItemArray);
			lstDetalle.getDetalle().get(XMLFACTURAField.getDGI().getDetalles().getDetalle().size() - 1).setDETCodItems(lstDetalleCodItem);
			clsDGIXMLFacturaField.setDetalles(lstDetalle);
			XMLFACTURAField.setDGI(clsDGIXMLFacturaField);
			DGIB2 = true;

		} catch (TAException ex) {
			try {
				ErrorCod.argValue = 8;
				ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
				objDebug.EscribirDebug(getNombreMetodo(), new String[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
				DGIB2 = false;
				throw ex;
			} catch (UnsupportedEncodingException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (UnsupportedEncodingException ex) {
			throw new TAException(ex.getMessage());
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
		return DGIB2;
	}

	public final boolean DGIB3addDetalleLineaActualNuevoSubDescuento(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, enumTipoSubDescuento SubDescuentoTipo, double SubDescuentoValor) throws TAException {

		boolean DGIB3 = false;
		try {
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "enumTipoSubDescuento SubDescuentoTipo", "double SubDescuentoValor"};
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, SubDescuentoTipo, SubDescuentoValor};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

			//Valido que haya una factura iniciada
			FacturaIniciada();
			//Valido que haya una Linea creada
			LineaDeDetalleCreada();

			//Si no viene valor cargado, no levanto el subdescuento
			if (nTipoApi == enumTipoApi.TACE && SubDescuentoValor == 0) {
				return true;
			}

			//Si ya existia algun descuento del mismo tipo, lo sumo a ese.
			if (nTipoApi == enumTipoApi.TACE && !listDetalleSubDescuentoArray.isEmpty()) {
				for (int i = 0; i < listDetalleSubDescuentoArray.size(); i++) {
					if (listDetalleSubDescuentoArray.get(i).getDETDescTipo() == SubDescuentoTipo.getValue()) {
						listDetalleSubDescuentoArray.get(i).setDETDescVal(listDetalleSubDescuentoArray.get(i).getDETDescVal() + SubDescuentoValor);
						DETSubDescuento[] tempDetalleSubDescuento = new DETSubDescuento[listDetalleSubDescuentoArray.size()];
						tempDetalleSubDescuento = listDetalleSubDescuentoArray.toArray(tempDetalleSubDescuento);
						lstDetalleSubDescuento.setDETSubDescuento(listDetalleSubDescuentoArray);
						lstDetalle.getDetalle().get(XMLFACTURAField.getDGI().getDetalles().getDetalle().size() - 1).setDETSubDescuentos(lstDetalleSubDescuento);
						clsDGIXMLFacturaField.setDetalles(lstDetalle);
						XMLFACTURAField.setDGI(clsDGIXMLFacturaField);
						return true;
					}
				}
			}

			if (enumTipoSubDescuento.forValue(SubDescuentoTipo.getValue()) != SubDescuentoTipo) {
				throw new TAException("Sub descuento tipo invalido - Recibido :" + SubDescuentoTipo.toString(), 4171);
			}

			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemito.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eResguardo.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eResguardoContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
				throw new TAException("No se permiten sub descuentos en linea de eRemitos, eResguardos y eRemitos de exportacion", 4095);
			}

			//Si ya habia alcanzado la cantidad maxima no permito seguir agregando
			if (listDetalleSubDescuentoArray.size() == MAXlstDetalleSubDescuento) {
				throw new TAException("Cantidad Maxima de SubDescuentos en una linea alcanzada (Zona B) Max:" + MAXlstDetalleSubDescuento, 4096);
			}

			//Si el valor de el SubDescuentoValor es menor o igual a 0 retonro error
			if (SubDescuentoValor <= 0) {
				throw new TAException("El valor de SubDescuento no puede ser menor o Igual que 0", 4097);
			}
			if (CantDeDecimales(SubDescuentoValor) > 2) {
				throw new TAException("Valor de SubDescuento debe tener 2 lugares decimales. - Recibido :" + SubDescuentoValor, 4098);
			}
			if (SubDescuentoValor < 0) {
				throw new TAException("Valor de SubDescuento no puede ser Negativo. - Recibido :" + SubDescuentoValor, 4099);
			}


			//Creo Instancia, seteo propiedades y agrego a lista
			XMLFACTURA.clsDGI.Detalle.DETSubDescuentos.DETSubDescuento objDetalleDETSubDescuento = lstDetalleSubDescuento.new DETSubDescuento();
			objDetalleDETSubDescuento.setDETDescTipo((byte) SubDescuentoTipo.getValue());
			objDetalleDETSubDescuento.setDETDescVal(Math.round(SubDescuentoValor * Math.pow(10, 2)) / Math.pow(10, 2));

			listDetalleSubDescuentoArray.add(objDetalleDETSubDescuento);
			DETSubDescuento[] tempDetalleSubDescuento = new DETSubDescuento[listDetalleSubDescuentoArray.size()];
			tempDetalleSubDescuento = listDetalleSubDescuentoArray.toArray(tempDetalleSubDescuento);
			lstDetalleSubDescuento.setDETSubDescuento(listDetalleSubDescuentoArray);
			lstDetalle.getDetalle().get(XMLFACTURAField.getDGI().getDetalles().getDetalle().size() - 1).setDETSubDescuentos(lstDetalleSubDescuento);
			clsDGIXMLFacturaField.setDetalles(lstDetalle);
			XMLFACTURAField.setDGI(clsDGIXMLFacturaField);
			DGIB3 = true;

		} catch (TAException ex) {
			ErrorCod.argValue = 8;
			ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
			try {
				objDebug.EscribirDebug(getNombreMetodo(), new String[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
				DGIB3 = false;
				throw ex;
			} catch (UnsupportedEncodingException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (UnsupportedEncodingException ex) {
			throw new TAException(ex.getMessage());
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
		return DGIB3;
	}

	public final boolean DGIB4addDetalleLineaActualNuevoSubRecargo(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, enumTipoSubRecargo SubRecargoTipo, double SubRecargoValor) throws TAException {

		boolean DGIB4 = false;
		try {
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "enumTipoSubRecargo SubRecargoTipo", "double SubRecargoValor"};
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, SubRecargoTipo, SubRecargoValor};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

			//Valido que haya una factura iniciada
			FacturaIniciada();
			//Valido que haya una Linea creada
			LineaDeDetalleCreada();

			//Si no viene valor cargado, no levanto el subrecargo
			if (nTipoApi == enumTipoApi.TACE && SubRecargoValor == 0) {
				return true;
			}

			//Si ya existia algun recargo del mismo tipo, lo sumo a ese.
			if (nTipoApi == enumTipoApi.TACE && !listDetalleSubRecargoArray.isEmpty()) {
				for (int i = 0; i < listDetalleSubRecargoArray.size(); i++) {
					if (listDetalleSubRecargoArray.get(i).getDETRecargoTipo() == SubRecargoTipo.getValue()) {
						listDetalleSubRecargoArray.get(i).setDETRecargoVal(listDetalleSubRecargoArray.get(i).getDETRecargoVal() + SubRecargoValor);
						XMLFACTURA.clsDGI.Detalle.DETSubRecargos.DETSubRecargo[] tempDetalleSubRecargo = new XMLFACTURA.clsDGI.Detalle.DETSubRecargos.DETSubRecargo[listDetalleSubRecargoArray.size()];
						tempDetalleSubRecargo = listDetalleSubRecargoArray.toArray(tempDetalleSubRecargo);
						lstDetalleSubRecargo.setDETSubRecargo(listDetalleSubRecargoArray);
						lstDetalle.getDetalle().get(XMLFACTURAField.getDGI().getDetalles().getDetalle().size()).setDETSubRecargos(lstDetalleSubRecargo);
						clsDGIXMLFacturaField.setDetalles(lstDetalle);
						XMLFACTURAField.setDGI(clsDGIXMLFacturaField);
						return true;
					}
				}
			}

			if (enumTipoSubRecargo.forValue(SubRecargoTipo.getValue()) != SubRecargoTipo) {
				throw new TAException("Sub recargo tipo invalido - Recibido :" + SubRecargoTipo.toString(), 4172);
			}

			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemito.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eResguardo.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eResguardoContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
				throw new TAException("No se permiten sub recargos en linea de eRemitos, eResguardos y eRemitos de exportacion", 4100);
			}

			//Si ya habia alcanzado la cantidad maxima no permito seguir agregando
			if (listDetalleSubRecargoArray.size() == MAXlstDetalleSubRecargo) {
				throw new TAException("Cantidad Maxima de SubRecargos en una linea alcanzada (Zona B) Max:" + MAXlstDetalleSubRecargo, 4101);
			}

			//Si el valor de el SubRecargo es menor o igual a 0 retonro error
			if (SubRecargoValor <= 0) {
				throw new TAException("El valor de el SubRecargo no puede ser menor o Igual que 0", 4102);
			}
			if (CantDeDecimales(SubRecargoValor) > 2) {
				throw new TAException("Valor de SubRecargo debe tener 2 lugares decimales. - Recibido :" + SubRecargoValor, 4103);
			}
			if (SubRecargoValor < 0) {
				throw new TAException("Valor de SubRecargo no puede ser Negativo. - Recibido :" + SubRecargoValor, 4104);
			}

			//Creo Instancia, seteo propiedades y agrego a lista
			XMLFACTURA.clsDGI.Detalle.DETSubRecargos.DETSubRecargo objDetalleDETSubRecargo = lstDetalleSubRecargo.new DETSubRecargo();
			objDetalleDETSubRecargo.setDETRecargoTipo((byte) SubRecargoTipo.getValue());
			objDetalleDETSubRecargo.setDETRecargoVal(Math.round(SubRecargoValor * Math.pow(10, 2)) / Math.pow(10, 2));

			listDetalleSubRecargoArray.add(objDetalleDETSubRecargo);
			XMLFACTURA.clsDGI.Detalle.DETSubRecargos.DETSubRecargo[] tempDetalleSubRecargo = new XMLFACTURA.clsDGI.Detalle.DETSubRecargos.DETSubRecargo[listDetalleSubRecargoArray.size()];
			tempDetalleSubRecargo = listDetalleSubRecargoArray.toArray(tempDetalleSubRecargo);
			lstDetalleSubRecargo.setDETSubRecargo(listDetalleSubRecargoArray);
			lstDetalle.getDetalle().get(XMLFACTURAField.getDGI().getDetalles().getDetalle().size() - 1).setDETSubRecargos(lstDetalleSubRecargo);
			clsDGIXMLFacturaField.setDetalles(lstDetalle);
			XMLFACTURAField.setDGI(clsDGIXMLFacturaField);
			DGIB4 = true;

		} catch (TAException ex) {
			try {
				ErrorCod.argValue = 8;
				ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
				objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
				DGIB4 = false;
				throw ex;
			} catch (UnsupportedEncodingException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (UnsupportedEncodingException ex) {
			throw new TAException(ex.getMessage());
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
		return DGIB4;
	}

	public final boolean DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String CodigoRetencionPercepcion, double Tasa, double MontoSujetoARetencionPercepcion, double ValorDeRetencionPercepcion, String InformacionAdicional) throws TAException, UnsupportedEncodingException {

		boolean DGIB5 = false;
		try {
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "String CodigoRetencionPercepcion", "double Tasa", "double MontoSujetoARetencionPercepcion", "double ValorDeRetencionPercepcion", "String InformacionAdicional"};
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, CodigoRetencionPercepcion, Tasa, MontoSujetoARetencionPercepcion, ValorDeRetencionPercepcion, InformacionAdicional};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

			//Valido que haya una factura iniciada
			FacturaIniciada();

			//Valido que haya una Linea creada
			LineaDeDetalleCreada();

			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemito.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue()) {
				throw new TAException("No se permiten sub retenciones o percepciones en linea de eRemitos y documentos de exportacion", 4105);
			}

			//Si ya habia alcanzado la cantidad maxima no permito seguir agregando
			if (listaDetalleSubRetencPercepArray.size() == MAXlstDetalleSubRetencPercep) {
				throw new TAException("Cantidad Maxima de SubRetenciones o SubPercepciones en una linea alcanzada (Zona B) Max:" + MAXlstDetalleSubRetencPercep, 4106);
			}

			if (nTipoApi == enumTipoApi.TAFE) {
				//Si el valor de la Retencion o Percepcion es menor o igual a 0 retonro error
				if (ValorDeRetencionPercepcion <= 0) {
					throw new TAException("El valor de la Retencion o Percepcion no puede ser menor o Igual que 0", 4107);
				}

				if (CantDeDecimales(Tasa) > 3) {
					throw new TAException("Tasa de Retencion/Percepcion debe tener 3 lugares decimales. - Recibido :" + Tasa, 4108);
				}
				if (Tasa >= 1000) {
					throw new TAException("Tasa de Retencion/Percepcion debe tener 3 lugares. - Recibido :" + Tasa, 4108);
				}

				if (CantDeDecimales(MontoSujetoARetencionPercepcion) > 2) {
					throw new TAException("Monto sujeto a Retencion/Percepcion debe tener 2 lugares decimales. - Recibido :" + MontoSujetoARetencionPercepcion, 4109);
				}
				if (CantDeDecimales(ValorDeRetencionPercepcion) > 2) {
					throw new TAException("Valor de Retencion/Percepcion debe tener 2 lugares decimales. - Recibido :" + ValorDeRetencionPercepcion, 4110);
				}

			}

			if (CodigoRetenPercepValido(CodigoRetencionPercepcion) == false) {
				throw new TAException("Codigo de Retencion o Percepcion no cumple el formato esperado XXXX-YYY, XXXX/YYY o XXXXYYYY(De 6 a 8 caracteres), Codigo Recibido " + CodigoRetencionPercepcion + ".", 4307);
			}

			//Si ya existia alguna Retencion/Percepcion con el mismo codigo, retorno error y le recomiendo crear otra linea.
			if (!listaDetalleSubRetencPercepArray.isEmpty()) {
				for (int i = 0; i < listaDetalleSubRetencPercepArray.size(); i++) {
					if (CodigoRetencionPercepcion.equals(listaDetalleSubRetencPercepArray.get(i).getDETCodRet())) {
						throw new TAException("El Codigo de la Retencion/Percepcion que esta intentando agregar ya se encuentra en la linea. - Recibido :" + CodigoRetencionPercepcion, 4108);
					}
				}
			}


			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemito.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardo.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eResguardoContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue() && XMLFACTURAField.getDGI().getIDDocTipoCFE() != enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue()) {
				if (!XMLFACTURAField.getDGI().getDetalles().getDetalle().get(XMLFACTURAField.getDGI().getDetalles().getDetalle().size() - 1).getDETIndAgenteResp().equals("A") && !XMLFACTURAField.getDGI().getDetalles().getDetalle().get(XMLFACTURAField.getDGI().getDetalles().getDetalle().size() - 1).getDETIndAgenteResp().equals("R")) {
					throw new TAException("Si Utiliza Retencion/Percepcion/Credito Fiscal debe ingresar el campo 'Indicador Agente Responsable'. - Indicador de Agente Recibido :" + XMLFACTURAField.getDGI().getDetalles().getDetalle().get(XMLFACTURAField.getDGI().getDetalles().getDetalle().size() - 1).getDETIndAgenteResp(), 4108);
				}
			}

			//Creo Instancia, seteo propiedades y agrego a lista
			XMLFACTURA.clsDGI.Detalle.DETRetencPerceps.DETRetencPercep objDetalleDETRetencPercep = lstaDetalleSubRetencPercep.new DETRetencPercep();
			objDetalleDETRetencPercep.setDETCodRet(LargoMax(CodigoRetencionPercepcion, 8));
			objDetalleDETRetencPercep.setDETTasa(Math.round(Tasa * Math.pow(10, 3)) / Math.pow(10, 3));
			objDetalleDETRetencPercep.setDETMntSujetoaRet(Math.round(MontoSujetoARetencionPercepcion * Math.pow(10, 2)) / Math.pow(10, 2));
			objDetalleDETRetencPercep.setDETValRetPerc(Math.round(ValorDeRetencionPercepcion * Math.pow(10, 2)) / Math.pow(10, 2));

			listaDetalleSubRetencPercepArray.add(objDetalleDETRetencPercep);
			XMLFACTURA.clsDGI.Detalle.DETRetencPerceps.DETRetencPercep[] tempListDetalleSubRetencPercep = new XMLFACTURA.clsDGI.Detalle.DETRetencPerceps.DETRetencPercep[listaDetalleSubRetencPercepArray.size()];
			tempListDetalleSubRetencPercep = listaDetalleSubRetencPercepArray.toArray(tempListDetalleSubRetencPercep);
			lstaDetalleSubRetencPercep.setDETRetencPercep(listaDetalleSubRetencPercepArray);
			lstDetalle.getDetalle().get(cantDetalle - 1).setDETRetencPerceps(lstaDetalleSubRetencPercep);
			clsDGIXMLFacturaField.setDetalles(lstDetalle);
			XMLFACTURAField.setDGI(clsDGIXMLFacturaField);
			DGIB5 = true;

		} catch (TAException ex) {
			try {
				ErrorCod.argValue = 8;
				ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
				objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
				DGIB5 = false;
				throw ex;
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
		return DGIB5;
	}

	public final boolean DGIC1addSubTotalInformativo(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String Glosa, short Orden, double ValorDelSubTotal) throws TAException {

		boolean DGIC1 = false;
		try {
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "String Glosa", "short Orden", "double ValorDelSubTotal"};
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, Glosa, Orden, ValorDelSubTotal};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

			short NroSubTotal = 0;
			//Valido que haya una factura iniciada
			FacturaIniciada();

			//Si ya habia alcanzado la cantidad maxima no permito seguir agregando
			if (listaSubTotInfoArray.size() == MAXlstSubTotInfo) {
				throw new TAException("Cantidad Maxima de SubTotales Informativos alcanzada (Zona C) Max:" + MAXlstSubTotInfo, 4111);
			}

			if (nTipoApi == enumTipoApi.TAFE) {
				if (Orden == 0) {
					throw new TAException("El Orden del SubTotal no puede ser 0", 4112);
				}
				if (ValorDelSubTotal < 0) {
					throw new TAException("El valor del SubTotal no puede ser menor a 0", 4112);
				}
				if (Glosa.trim().equals("")) {
					throw new TAException("Glosa en Subtotales no puede ser Vacia", 4113);
				}
				if (CantDeDecimales(ValorDelSubTotal) > 2) {
					throw new TAException("Valor de SubTotales debe tener 2 lugares decimales. - Recibido :" + ValorDelSubTotal, 4114);
				}
			}

			NroSubTotal = (short) (listaSubTotInfoArray.size() + 1);

			//Creo Instancia, seteo propiedades y agrego a lista
			XMLFACTURA.clsDGI.SubTotalInfos.SubTotalInfo objSubTotInfo = lstSubTotInfo.new SubTotalInfo();
			objSubTotInfo.setSUBNroSTI((byte) NroSubTotal);
			objSubTotInfo.setSUBGlosaSTI(LargoMax(Glosa, 40));
			objSubTotInfo.setSUBOrdenSTI((byte) (Orden));
			objSubTotInfo.setSUBValSubtotSTI(Math.round(ValorDelSubTotal * Math.pow(10, 2)) / Math.pow(10, 2));
			listaSubTotInfoArray.add(objSubTotInfo);
			XMLFACTURA.clsDGI.SubTotalInfos.SubTotalInfo[] tempSubTotInfoArray = new XMLFACTURA.clsDGI.SubTotalInfos.SubTotalInfo[listaSubTotInfoArray.size()];
			lstSubTotInfo.setSubTotalInfo(listaSubTotInfoArray);
			clsDGIXMLFacturaField.setSubTotalInfos(lstSubTotInfo);
			XMLFACTURAField.setDGI(clsDGIXMLFacturaField);

			DGIC1 = true;

		} catch (TAException ex) {
			try {
				ErrorCod.argValue = 8;
				ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
				objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
				DGIC1 = false;
				throw ex;
			} catch (UnsupportedEncodingException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (UnsupportedEncodingException ex) {
			throw new TAException(ex.getMessage());
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
		return DGIC1;
	}

	public final boolean DGID1addDescuentoORecargoGlobal(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, ITAParsing_v0206.enumTipoMovimientoDescuentoORecargoGlobal TipoDeMovimientoaux, ITAParsing_v0206.enumTipoDescuentoORecargoGlobal TipoDescuentoORecargoaux, short CodigoDelDescuentoORecargo, String Glosa, double Valor, ITAParsing_v0206.enumIndicadorDeFacturacionDescuentoGlobal IndicadorDeFacturacionaux) throws TAException {

		boolean DGID1 = false;
		try {
			enumTipoMovimientoDescuentoORecargoGlobal TipoDeMovimiento = enumTipoMovimientoDescuentoORecargoGlobal.forValue(TipoDeMovimientoaux.getValue());
			enumTipoDescuentoORecargoGlobal TipoDescuentoORecargo = enumTipoDescuentoORecargoGlobal.forValue(TipoDescuentoORecargoaux.getValue());
			enumIndicadorDeFacturacionDescuentoGlobal IndicadorDeFacturacion = enumIndicadorDeFacturacionDescuentoGlobal.forValue(IndicadorDeFacturacionaux.getValue());
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "ITAParsing_v0206.enumTipoMovimientoDescuentoORecargoGlobal TipoDeMovimiento", "ITAParsing_v0206.enumTipoDescuentoORecargoGlobal TipoDescuentoORecargo", "short CodigoDelDescuentoORecargo", "String Glosa", "double Valor", "ITAParsing_v0206.enumIndicadorDeFacturacionDescuentoGlobal IndicadorDeFacturacion"};
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, TipoDeMovimiento, TipoDescuentoORecargo, CodigoDelDescuentoORecargo, Glosa, Valor, IndicadorDeFacturacion};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

			short NroDeLinea = 0;
			//Valido que haya una factura iniciada
			FacturaIniciada();
			//Elimino espacios
			Glosa = CNullStr(Glosa).trim();

			if (enumTipoMovimientoDescuentoORecargoGlobal.forValue(TipoDeMovimiento.getValue()) != TipoDeMovimiento) {
				throw new TAException("Tipo de movimiento descuento o recargo global invalido - Recibido :" + TipoDeMovimiento.toString(), 4173);
			}

			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemito.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eResguardo.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eResguardoContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
				throw new TAException("No se permiten descuentos o recargos globales en eRemitos, eResguardos y eRemitos de exportacion", 4115);
			}

			//Si ya habia alcanzado la cantidad maxima no permito seguir agregando
			if (listaDscRcgGlobalArray.size() == MAXlstDscRcgGlobal) {
				throw new TAException("Cantidad Maxima de Descuentos o Recargos Globales Informativos alcanzada (Zona D) Max:" + MAXlstDscRcgGlobal, 4116);
			}

			if (nTipoApi == enumTipoApi.TAFE) {
				//Si el valor no es mayor a 0 retorno un error
				if (Valor <= 0) {
					throw new TAException("El valor del Descuento o Recargo Global No puede ser 0 o menor que 0", 4117);
				}
				if (CantDeDecimales(Valor) > 2) {
					throw new TAException("Valor de Descuento o Recargo Global debe tener 2 lugares decimales. - Recibido :" + Valor, 4118);
				}
				if (IndicadorDeFacturacion == enumIndicadorDeFacturacionDescuentoGlobal.SinDefinir) {
					throw new TAException("Debe indicar un Indicador de Facturación para cada Descuento/Recargo global", 4119);
				}
			}

			NroDeLinea = (short) (listaDscRcgGlobalArray.size() + 1);

			//Creo Instancia, seteo propiedades y agrego a lista
			XMLFACTURA.clsDGI.DscRcgGlobals.DscRcgGlobal objDscRcgGlobal = lstDscRcgGlobal.new DscRcgGlobal();
			objDscRcgGlobal.setDRGNroLinDR((byte) NroDeLinea);
			objDscRcgGlobal.setDRGTpoMovDR(LargoMax(TipoDeMovimiento.toString(), 1));

			//valido que si el tipo de descuento o recargo no viene en 1/2 lo seteo a 1 (Monto) para no detener el proceso de firma
			if (TipoDescuentoORecargo.getValue() != 1 && TipoDescuentoORecargo.getValue() != 2) {
				objDscRcgGlobal.setDRGTpoDR((byte) 1);
			} else {
				objDscRcgGlobal.setDRGTpoDR((byte) TipoDescuentoORecargo.getValue());
			}

			objDscRcgGlobal.setDRGCodDR(CodigoDelDescuentoORecargo);

			if (tangible.DotNetToJavaStringHelper.isNullOrEmpty(Glosa)) {
				Glosa = "No Especificado";
			}

			objDscRcgGlobal.setDRGGlosaDR(LargoMax(Glosa, 50));
			objDscRcgGlobal.setDRGValorDR(Math.round(Valor * Math.pow(10, 2)) / Math.pow(10, 2));
			objDscRcgGlobal.setDRGIndFactDR((short) IndicadorDeFacturacion.getValue());
			listaDscRcgGlobalArray.add(objDscRcgGlobal);
			XMLFACTURA.clsDGI.DscRcgGlobals.DscRcgGlobal[] tmpjDscRcgGlobalArray = new XMLFACTURA.clsDGI.DscRcgGlobals.DscRcgGlobal[listaDscRcgGlobalArray.size()];
			lstDscRcgGlobal.setDscRcgGlobal(listaDscRcgGlobalArray);
			clsDGIXMLFacturaField.setDscRcgGlobales(lstDscRcgGlobal);
			XMLFACTURAField.setDGI(clsDGIXMLFacturaField);
			DGID1 = true;

		} catch (TAException ex) {
			try {
				ErrorCod.argValue = 8;
				ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
				objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
				DGID1 = false;
				throw ex;
			} catch (UnsupportedEncodingException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (UnsupportedEncodingException ex) {
			throw new TAException(ex.getMessage());
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
		return DGID1;
	}

	public final boolean DGIE1addMedioDePago(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, short CodigoMedioDePago, String Glosa, short Orden, double ValorDelPago) throws TAException {

		boolean DGIE1 = false;
		try {
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "short CodigoMedioDePago", "String Glosa", "short Orden", "double ValorDelPago"};
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, CodigoMedioDePago, Glosa, Orden, ValorDelPago};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

			short NroDeLinea = 0;
			//Valido que haya una factura iniciada
			FacturaIniciada();
			//Elimino espacios
			Glosa = CNullStr(Glosa).trim();
			if (XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemito.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eResguardo.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eResguardoContingencia.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() || XMLFACTURAField.getDGI().getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()) {
				throw new TAException("No se permiten medios de pago en eRemitos, eResguardos y eRemitos de exportacion", 4119);
			}

			//Si ya habia alcanzado la cantidad maxima no permito seguir agregando
			if (listaMediosPagoArray.size() == MAXlstMediosPago) {
				throw new TAException("Cantidad Maxima de Medios de Pago alcanzada (Zona E) Max: " + MAXlstMediosPago, 4120);
			}
			if (Glosa.trim().equals("")) {
				throw new TAException("Glosa en Medios de Pago no puede ser Vacia", 4121);
			}
			if (CantDeDecimales(ValorDelPago) > 2) {
				throw new TAException("Valor de Medio de Pago debe tener 2 lugares decimales. - Recibido :" + ValorDelPago, 4121);
			}
			if ((new Short(CodigoMedioDePago)).toString().length() > 3) {
				throw new TAException("Codigo de Medio de Pago no puede ser mayor a 3 Caracteres. - Recibido :" + CodigoMedioDePago, 4123);
			}


			NroDeLinea = (short) (listaMediosPagoArray.size() + 1);
			//Creo Instancia, seteo propiedades y agrego a lista
			XMLFACTURA.clsDGI.MedioPagos.MedioPago objMediosPago = lstMediosPago.new MedioPago();
			objMediosPago.setMPNroLinMP((byte) NroDeLinea);
			objMediosPago.setMPCodMP(CodigoMedioDePago);
			objMediosPago.setMPGlosaMP(LargoMax(Glosa, 50));
			objMediosPago.setMPOrdenMP((byte) Orden);
			objMediosPago.setMPValorPago(Math.round(ValorDelPago * Math.pow(10, 2)) / Math.pow(10, 2));
			listaMediosPagoArray.add(objMediosPago);
			XMLFACTURA.clsDGI.MedioPagos.MedioPago[] tmpMediosPagoArray = new XMLFACTURA.clsDGI.MedioPagos.MedioPago[listaMediosPagoArray.size()];
			lstMediosPago.setMedioPago(listaMediosPagoArray);
			clsDGIXMLFacturaField.setMediosPago(lstMediosPago);
			XMLFACTURAField.setDGI(clsDGIXMLFacturaField);
			DGIE1 = true;

		} catch (TAException ex) {
			try {
				ErrorCod.argValue = 8;
				ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
				objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
				DGIE1 = false;
				throw ex;
			} catch (UnsupportedEncodingException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (UnsupportedEncodingException ex) {
			throw new TAException(ex.getMessage());
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
		return DGIE1;
	}

	public final boolean DGIF1addInformacionDeReferencia(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, short IndicadorDeReferenciaGlobal, ITAParsing_v0206.enumTipoDeComprobanteCFE TipoCFEReferenciaaux, String SerieCFEReferencia, long NroCFEReferencia, String RazonReferencia, Date FechaCFEReferencia) throws TAException {

		boolean DGIF1 = false;
		try {
			int TipoCFEReferencia = 0;
			if (TipoCFEReferenciaaux != null && TipoCFEReferenciaaux.getValue() != 0) {
				TipoCFEReferencia = TipoCFEReferenciaaux.getValue();
			}
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "short IndicadorDeReferenciaGlobal", "ITAParsing_v0206.enumTipoDeComprobanteCFE TipoCFEReferencia", "String SerieCFEReferencia", "long NroCFEReferencia", "String RazonReferencia", "java.util.Date FechaCFEReferencia"};
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, IndicadorDeReferenciaGlobal, TipoCFEReferencia, SerieCFEReferencia, NroCFEReferencia, RazonReferencia, FechaCFEReferencia};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

			short NroDeLinea = 0;
			//Valido que haya una factura iniciada
			FacturaIniciada();
			//Elimino espacios
			SerieCFEReferencia = CNullStr(SerieCFEReferencia).trim();
			RazonReferencia = CNullStr(RazonReferencia).trim();
			//Si ya habia alcanzado la cantidad maxima no permito seguir agregando
			if (listaReferenciaArray.size() == MAXlstReferencia) {
				throw new TAException("Cantidad Maxima de Informacion de Referencia alcanzada (Zona F) Max: " + MAXlstReferencia, 4124);
			}

			if (nTipoApi == enumTipoApi.TAFE) {
				if (IndicadorDeReferenciaGlobal == 1 && (RazonReferencia.trim() == null || RazonReferencia.trim().length() == 0)) {
					throw new RuntimeException("Debe especificar la Razon de Referencia si utiliza ReferenciaGlobal=1. - Recibido :" + IndicadorDeReferenciaGlobal);
				}
			}

			if (nTipoApi == enumTipoApi.TACE) {
				//Si me pasan que no es referencia global pero tampoco me pasan el comprobante al que hace referencia
				//lo marco como referencia global
				if (TipoCFEReferencia != 0 && IndicadorDeReferenciaGlobal == 0) {
					IndicadorDeReferenciaGlobal = 1;
				}
			}

			NroDeLinea = (short) (listaReferenciaArray.size() + 1);
			//Creo Instancia, seteo propiedades y agrego a lista
			XMLFACTURA.clsDGI.Referencias.Referencia objReferencia = lstReferencia.new Referencia();
			objReferencia.setREFNroLinRef((byte) NroDeLinea);
			//Indicador de Referencia Global (No ENVIAR EL CAMPO A MENOS QUE SEA UNO)
			if (IndicadorDeReferenciaGlobal == 1) {
				objReferencia.setREFIndGlobal(String.valueOf(IndicadorDeReferenciaGlobal));
				objReferencia.setREFIndGlobalSpecified(true);
				objReferencia.setREFTpoDocRefSpecified(false);
				objReferencia.setREFSerie("");
				objReferencia.setREFNroCFERefSpecified(false);
				objReferencia.setREFFechaCFErefSpecified(false);
			} else {
				if (TipoCFEReferencia != 0 && enumTipoDeComprobanteCFE.forValue(TipoCFEReferencia).getValue() != TipoCFEReferencia) {
					throw new TAException("Tipo CFE de referencia invalido - Recibido :" + TipoCFEReferencia, 4174);
				}
				objReferencia.setREFIndGlobalSpecified(false);
				objReferencia.setREFTpoDocRef((short) TipoCFEReferencia);
				objReferencia.setREFTpoDocRefSpecified(true);
				objReferencia.setREFSerie(LargoMax(SerieCFEReferencia, 2));
				objReferencia.setREFNroCFERef(String.valueOf(NroCFEReferencia));
				objReferencia.setREFNroCFERefSpecified(true);
			}
			objReferencia.setREFRazonRef(LargoMax(RazonReferencia, 90));
			if (!FechaCFEReferencia.equals(new Date(0)) && !MinFecha.equals(FechaCFEReferencia)) {
				if (FechaCFEReferencia.compareTo(new Date("2011/10/01")) < 0) {
					throw new TAException("Fecha de referencia debe ser mayor a 01/10/2011 - Recibida :" + FechaCFEReferencia.toString(), 4286);
				}
				if (FechaCFEReferencia.compareTo(new Date("2060/12/31")) > 0) {
					throw new TAException("Fecha de referencia debe ser menor a 31/12/2060 - Recibida :" + FechaCFEReferencia.toString(), 4287);
				}
				objReferencia.setREFFechaCFEref(FechaCFEReferencia);
				objReferencia.setREFFechaCFErefSpecified(true);
			}

			listaReferenciaArray.add(objReferencia);
			XMLFACTURA.clsDGI.Referencias.Referencia[] tempReferenciaArray = new XMLFACTURA.clsDGI.Referencias.Referencia[listaReferenciaArray.size()];
			lstReferencia.setReferencia(listaReferenciaArray);
			clsDGIXMLFacturaField.setReferencias(lstReferencia);
			XMLFACTURAField.setDGI(clsDGIXMLFacturaField);

			DGIF1 = true;

		} catch (TAException ex) {
			try {
				ErrorCod.argValue = 8;
				ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
				objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
				DGIF1 = false;
				throw ex;
			} catch (UnsupportedEncodingException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (UnsupportedEncodingException ex) {
			throw new TAException(ex.getMessage());
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
		return DGIF1;
	}

	public final boolean DGIK1setComplementoFiscal(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, long RUCEmisor, enumTipoDocumentoDelMandante TipoDocMandante, String CodPaisMandante, String NroDocMandante, String NombreMandante) throws TAException {

		boolean DGIK1 = false;
		try {
			ErrorMsg.argValue = "";
			ErrorCod.argValue = 0;
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "long RUCEmisor", "enumTipoDocumentoDelMandante TipoDocMandante", "String CodPaisMandante", "String NroDocMandante", "String NombreMandante"};
			Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, RUCEmisor, TipoDocMandante, CodPaisMandante, NroDocMandante, NombreMandante};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

			//Valido que haya una factura iniciada
			FacturaIniciada();
			//Elimino espacios
			NombreMandante = CNullStr(NombreMandante).trim();

			//*** Validacion de Datos***
			String StrRucEmisor = String.format("%012d", RUCEmisor);
			if (!(VerificacionRUT(StrRucEmisor.toString()))) {
				throw new TAException("RUT de Emisor en el Complemento Fiscal Invalido - Recibido: " + StrRucEmisor.toString(), 4288);
			}

			if (enumTipoDocumentoDelMandante.forValue(TipoDocMandante.getValue()) != TipoDocMandante) {
				throw new TAException("Tipo de documento del Mandante invalido - Recibido :" + TipoDocMandante.toString(), 4289);
			}

			//Valido Coherencia entre el Tipo de documento y el pais del cliente
			if ((TipoDocMandante == enumTipoDocumentoDelMandante.CI || TipoDocMandante == enumTipoDocumentoDelMandante.RUC) && (!CodPaisMandante.equals("UY"))) {
				throw new TAException("Si el documento del Mandante es C.I o RUT debe ser uruguayo. - Recibido: " + CodPaisMandante, 1050);
			}


			//Valido Coherencia entre el Tipo de documento,el pais del cliente y el documento
			if (TipoDocMandante == enumTipoDocumentoDelMandante.CI && NroDocMandante.replace(".", "").replace("-", "").length() > 8) {
				throw new TAException("Si el documento del Mandante es C.I la misma no puede tener mas de 8 numeros. - Recibido: " + NroDocMandante, 1051);
			}


			//Valido Coherencia entre el Tipo de documento y el pais del cliente
			if ((TipoDocMandante == enumTipoDocumentoDelMandante.DNI) && (!CodPaisMandante.equals("AR") && !CodPaisMandante.equals("BR") && !CodPaisMandante.equals("CL") && !CodPaisMandante.equals("PY"))) {
				throw new TAException("Si el documento del Mandante es DNI debe ser Argentino, Brasilero, Chileno o Paraguayo. - Recibido: " + CodPaisMandante, 1052);
			}

			//Si es RUC lo valido
			if (TipoDocMandante == enumTipoDocumentoDelMandante.RUC) {
				NroDocMandante = String.format("%012d", Long.parseLong(NroDocMandante));
				if (!(VerificacionRUT(NroDocMandante))) {
					throw new TAException("RUT del Mandante Invalido - Recibido: " + NroDocMandante.toString(), 1053);
				}
			}

			//Si es CI la valido
			if ((TipoDocMandante == enumTipoDocumentoDelMandante.CI) && (NroDocMandante != null && NroDocMandante.length() > 0)) {
				NroDocMandante = NroDocMandante.replace("-", "");
				NroDocMandante = NroDocMandante.replace(".", "");
				NroDocMandante = NroDocMandante.replace(" ", "");
				if (!(VerificacionCedula(NroDocMandante.toString()))) {
					throw new TAException("Cedula del Mandante Invalida - Recibido: " + NroDocMandante.toString(), 1054);
				}
			}

			//*** Fin Validacion de Datos ***

			XMLFACTURAField.getDGI().setCFRUCEmisor(StrRucEmisor);
			XMLFACTURAField.getDGI().setCFRUCEmisorSpecified(true);
			XMLFACTURAField.getDGI().setCFTipoDocMdte((short) TipoDocMandante.getValue());
			XMLFACTURAField.getDGI().setCFTipoDocMdteSpecified(true);
			XMLFACTURAField.getDGI().setCFPais(CodPaisMandante);
			XMLFACTURAField.getDGI().setCFPaisSpecified(true);
			XMLFACTURAField.getDGI().setCFDocMdte(LargoMax(NroDocMandante, 20));
			XMLFACTURAField.getDGI().setCFDocMdteSpecified(true);
			XMLFACTURAField.getDGI().setCFNombreMdte(LargoMax(NombreMandante, 150));
			XMLFACTURAField.getDGI().setCFNombreMdteSpecified(true);
			DGIK1 = true;

		} catch (TAException ex) {
			ErrorCod.argValue = 8;
			ErrorMsg.argValue = ex.getMessage() + " - " + getNombreMetodo();
			try {
				objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"ErrorCod", "ErrorMsg"}, new Object[]{ErrorCod.argValue, ex.toString()});
				DGIK1 = false;
				throw ex;
			} catch (UnsupportedEncodingException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (UnsupportedEncodingException ex) {
			throw new TAException(ex.getMessage());
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		}
		return DGIK1;
	}

	private void FacturaIniciada() throws TAException {

		//Verifico que se haya iniciado una factura
		if ((XMLFACTURAField == null)) {
			try {
				throw new TAException("Luego de crear la Instancia debe invocar al metodo NuevaFactura", 4125);
			} catch (TAException ex) {
				throw new TAException(ex.getMessage());
			}
		}
	}

	private void LineaDeDetalleCreada() throws TAException {

		//Verifico que se haya creado al menos una linea
		if ((XMLFACTURAField.getDGI().getDetalles() == null) || (XMLFACTURAField.getDGI().getDetalles().getDetalle().size() == 0)) {
			try {
				throw new TAException("Debe crear una linea de detalle primero", 4126);
			} catch (TAException ex) {
				throw new TAException(ex.getMessage());
			}
		}

	}

	private boolean VerificacionRUT(String RUT) {
		short DigRut = 0;
		short Digito = 0;
		short D1 = 0;
		short D2 = 0;
		short D3 = 0;
		short D4 = 0;
		short D5 = 0;
		short D6 = 0;
		short D7 = 0;
		short D8 = 0;
		short D9 = 0;
		short D10 = 0;
		short D11 = 0;
		short Total = 0;
		short Resto = 0;
		short Aux = 0;
		boolean Sirve = true;


		if ((RUT == null || RUT.length() == 0) || RUT.length() != 12) {
			Sirve = false;
		} else {
			D1 = (short) (Short.parseShort(RUT.substring(0, 1)) * 4);
			D2 = (short) (Short.parseShort(RUT.substring(1, 2)) * 3);
			D3 = (short) (Short.parseShort(RUT.substring(2, 3)) * 2);
			D4 = (short) (Short.parseShort(RUT.substring(3, 4)) * 9);
			D5 = (short) (Short.parseShort(RUT.substring(4, 5)) * 8);
			D6 = (short) (Short.parseShort(RUT.substring(5, 6)) * 7);
			D7 = (short) (Short.parseShort(RUT.substring(6, 7)) * 6);
			D8 = (short) (Short.parseShort(RUT.substring(7, 8)) * 5);
			D9 = (short) (Short.parseShort(RUT.substring(8, 9)) * 4);
			D10 = (short) (Short.parseShort(RUT.substring(9, 10)) * 3);
			D11 = (short) (Short.parseShort(RUT.substring(10, 11)) * 2);

			Total = (short) (D1 + D2 + D3 + D4 + D5 + D6 + D7 + D8 + D9 + D10 + D11);

			Aux = (short) Math.floor(Total / 11.0);

			Resto = (short) (Total - (Aux * 11));

			if (Resto == 1) {
				Sirve = false;
			} else {
				if (Resto == 0) {
					Digito = 0;
				} else {
					Digito = (short) (11 - Resto);
				}

				DigRut = Short.parseShort(RUT.substring(11, 12));

				if (Digito != DigRut) {
					Sirve = false;
				}
			}
		}

		if (!Sirve) {
			return false;
		}

		return Sirve;

	}

	private boolean CodigoRetenPercepValido(String CodigoRetenPercep) {
		boolean tempCodigoRetenPercepValido = false;
		//Formato XXXX-YYY, XXXX/YYY o XXXXYYYY OR Códigos 9999001 al 9999999
		tempCodigoRetenPercepValido = false;
		if (isNumeric(CodigoRetenPercep)) {
			if (CodigoRetenPercep.compareTo(String.valueOf(9999001)) >= 0 && CodigoRetenPercep.compareTo(String.valueOf(9999999)) <= 0) //9999001 al 9999999
			{
				tempCodigoRetenPercepValido = true;
			} else if (CodigoRetenPercep.length() >= 6 && CodigoRetenPercep.length() <= 8) //XXXXYYYY
			{
				tempCodigoRetenPercepValido = true;
			}
		} else {
			if (CodigoRetenPercep.length() >= 6 && CodigoRetenPercep.length() <= 8 && (CodigoRetenPercep.contains("-") || CodigoRetenPercep.contains("/"))) //XXXX-YYY, XXXX/YYY
			{
				tempCodigoRetenPercepValido = true;
			}
		}
		return tempCodigoRetenPercepValido;
	}

	private boolean VerificacionCedula(String Cedula) {
//        String Clave = null;
//        short I = 0;
//        short Res = 0;
//        short Dig = 0;
//        boolean CedulaContDigVerif = false;
//        String DigControl = null;
//        if (Cedula.length() < 7) {
//            return false;
//        }
//        DigControl = Cedula.substring(Cedula.length() - 1);
//        Cedula = Cedula.substring(0, Cedula.length() - 1);
//        CedulaContDigVerif = false;
//        if (DigControl != null && DigControl.length() > 0) {
//            if (!Cedula.trim().equals("") && Integer.parseInt(Cedula) != 0) {
//                Clave = "2987634";
//                Cedula = Integer.parseInt(Cedula).toString("0000000");
//                for (I = 0; I <= 6; I++) {
//                    Res = (short) (Short.parseShort(Cedula.substring(I, I + 1)) * Short.parseShort(Clave.substring(I, I + 1)));
//                    if (Res > 9) {
//                        Dig = (short) (Dig + Short.parseShort((new Short(Res)).toString("00").substring(1, 2)));
//                    } else {
//                        Dig = (short) (Dig + Res);
//                    }
//                }
//                Dig = (short) (10 - (Dig % 10));
//                if (Dig > 9) {
//                    Dig = 0;
//                }
//                if (Dig == Short.parseShort(DigControl)) {
//                    CedulaContDigVerif = true;
//                }
//            }
//        } else {
//            CedulaContDigVerif = true;
//        }
//
//        return CedulaContDigVerif;
		return true;
	}

	private int CantDeDecimales(double Num) {
		int v = Math.abs((int) Num);
		return Math.max(0, String.valueOf(Math.abs(Num)).length() - String.valueOf(v).length() - 1);
	}

	private int CantDeEnteros(long Num) {
		return (new Long(Num)).toString().length();
	}

	private String LargoMax(String str, int LargoMaximo) {
		if (str == null) {
			return "";
		}
		if ((str == null ? 0 : str.length()) > LargoMaximo) {
			return str.substring(0, LargoMaximo);
		} else {
			return str;
		}
	}

	private String CNullStr(Object Value) {
		String tempCNullStr = null;
		tempCNullStr = "";
		if (!((Value == null))) {
			tempCNullStr = String.valueOf(Value);
		}
		return tempCNullStr;
	}

	private long CTLng(Object Texto) {
		long tempCTLng = 0;
		if (isNumeric(Texto.toString())) {
			tempCTLng = Long.parseLong(String.valueOf(Texto));
		} else {
			tempCTLng = 0;
		}
		return tempCTLng;
	}

	public final String VersionActual() {
		return "02.05";
	}

	public final boolean UnParsingXMLRespuesta(String XMLRESPUESTA, tangible.RefObject<String> ERRORMSG, tangible.RefObject<Integer> ERRORCOD, tangible.RefObject<String> WARNINGMSG, tangible.RefObject<Boolean> FIRMADOOK, tangible.RefObject<Date> FIRMADOFCHHORA, tangible.RefObject<Long> CAENA, tangible.RefObject<Long> CAENROINICIAL, tangible.RefObject<Long> CAENROFINAL, tangible.RefObject<Date> CAEVENCIMIENTO, tangible.RefObject<String> CAESERIE, tangible.RefObject<Long> CAENRO, tangible.RefObject<String> CODSEGURIDAD, tangible.RefObject<String> URLPARAVERIFICARTEXTO, tangible.RefObject<String> URLPARAVERIFICARQR, tangible.RefObject<String> RESOLUCIONIVA) throws TAException {

		boolean unparsingXML = false;
		try {
			ERRORMSG.argValue = "";
			ERRORCOD.argValue = 0;
			if ((XMLFACTURAField == null)) {
				throw new TAException("Para reailzar el UnParsing de la respuesta debe haber invocado al metodo NuevaFactura.", 4159);
			}
			//***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
			Object[] DebugNamesArray = {"String XMLRESPUESTA", "tangible.RefObject<String> ERRORMSG", "tangible.RefObject<Integer> ERRORCOD", "tangible.RefObject<String> WARNINGMSG", "tangible.RefObject<Boolean> FIRMADOOK", "tangible.RefObject<java.util.Date> FIRMADOFCHHORA", "tangible.RefObject<Long> CAENA", "tangible.RefObject<Long> CAENROINICIAL", "tangible.RefObject<Long> CAENROFINAL", "tangible.RefObject<java.util.Date> CAEVENCIMIENTO", "tangible.RefObject<String> CAESERIE", "tangible.RefObject<Long> CAENRO", "tangible.RefObject<String> CODSEGURIDAD", "tangible.RefObject<String> URLPARAVERIFICARTEXTO", "tangible.RefObject<String> URLPARAVERIFICARQR", "tangible.RefObject<String> RESOLUCIONIVA"};
			Object[] DebugValuesArray = {XMLRESPUESTA, ERRORMSG.argValue, ERRORCOD.argValue, WARNINGMSG.argValue, FIRMADOOK.argValue, FIRMADOFCHHORA.argValue, CAENA.argValue, CAENROINICIAL.argValue, CAENROFINAL.argValue, CAEVENCIMIENTO.argValue, CAESERIE.argValue, CAENRO.argValue, CODSEGURIDAD.argValue, URLPARAVERIFICARTEXTO.argValue, URLPARAVERIFICARQR.argValue, RESOLUCIONIVA.argValue};
			objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

			//Transformo el XML en tipos de datos primitivos
			XMLRESPUESTA XMLResp = new XMLRESPUESTA();
			XMLResp = XMLResp.LoadXML(XMLRESPUESTA);

			ERRORMSG.argValue = XMLResp.getERRORMSG();
			WARNINGMSG.argValue = XMLResp.getWARNINGMSG();
			FIRMADOOK.argValue = XMLResp.getFIRMADOOK() != 0;
			//2017-05-05T15:51:00.0000358-03:00
			String[] splitted = XMLResp.getFIRMADOFCHHORA().split("T");
			SimpleDateFormat formatter = new SimpleDateFormat("hh:MM:ss yyyy-MM-dd");
			Date date = formatter.parse(splitted[1].substring(0, splitted[1].lastIndexOf(".")) + " " + splitted[0]);
			FIRMADOFCHHORA.argValue = date;
			CAENA.argValue = XMLResp.getCAENA();
			CAENROINICIAL.argValue = XMLResp.getCAENROINICIAL();
			CAENROFINAL.argValue = XMLResp.getCAENROFINAL();
			CAEVENCIMIENTO.argValue = XMLResp.getCAEVENCIMIENTO();
			CAESERIE.argValue = XMLResp.getCAESERIE();
			CAENRO.argValue = XMLResp.getCAENRO();
			CODSEGURIDAD.argValue = XMLResp.getCODSEGURIDAD();
			URLPARAVERIFICARTEXTO.argValue = XMLResp.getURLPARAVERIFICARTEXTO();
			URLPARAVERIFICARQR.argValue = XMLResp.getURLPARAVERIFICARQR();
			RESOLUCIONIVA.argValue = XMLResp.getRESOLUCIONIVA();

			//Cambio nothing por cadena vacia ""
			if (ERRORMSG.argValue == null) {
				ERRORMSG.argValue = "";
			}
			if (WARNINGMSG.argValue == null) {
				WARNINGMSG.argValue = "";
			}
			if (CAESERIE.argValue == null) {
				CAESERIE.argValue = "";
			}
			if (CODSEGURIDAD.argValue == null) {
				CODSEGURIDAD.argValue = "";
			}
			if (URLPARAVERIFICARTEXTO.argValue == null) {
				URLPARAVERIFICARTEXTO.argValue = "";
			}
			if (URLPARAVERIFICARQR.argValue == null) {
				URLPARAVERIFICARQR.argValue = "";
			}
			if (RESOLUCIONIVA.argValue == null) {
				RESOLUCIONIVA.argValue = "";
			}

			unparsingXML = true;

		} catch (TAException ex) {
			ERRORCOD.argValue = 8;
			ERRORMSG.argValue = "Error Unparsing XMLRespuesta: " + ex.getMessage() + " - " + XMLRESPUESTA + " - " + getNombreMetodo();
			try {
				objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"ErrorCod", "ErrorMsg"}, new Object[]{ERRORCOD.argValue, ex.toString()});
				unparsingXML = false;
				throw ex;
			} catch (UnsupportedEncodingException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (FileNotFoundException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (IOException ex1) {
				throw new TAException(ex1.getMessage());
			} catch (JAXBException ex1) {
				throw new TAException(ex1.getMessage());
			}
		} catch (JAXBException ex) {
			throw new TAException(ex.getMessage());
		} catch (ParseException ex) {
			throw new TAException(ex.getMessage());
		} catch (UnsupportedEncodingException ex) {
			throw new TAException(ex.getMessage());
		} catch (FileNotFoundException ex) {
			throw new TAException(ex.getMessage());
		} catch (IOException ex) {
			throw new TAException(ex.getMessage());
		}
		return unparsingXML;
	}

	/////////////////ADDED FUNCTIONS
	public String getNombreMetodo() {
		//Retorna el nombre del metodo desde el cual se hace el llamado
		return new Exception().getStackTrace()[1].getMethodName();
	}

	private boolean isNumeric(String s) {
		return s.matches("[-+]?\\d*\\.?\\d+");
	}

	public static double Val(String str) {
		StringBuilder validStr = new StringBuilder();
		boolean seenDot = false;
		boolean seenDigit = false;
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (c == '.' && !seenDot) {
				seenDot = true;
				validStr.append(c);
			} else if ((c == '-' || c == '+') && !seenDigit) {
				validStr.append(c);
			} else if (Character.isDigit(c)) {
				seenDigit = true;
				validStr.append(c);
			} else if (Character.isWhitespace(c)) {
				// just skip over whitespace
				continue;
			} else {
				// invalid character
				break;
			}
		}
		return Double.parseDouble(validStr.toString());
	}

	public void limpiarMemoria() {

		objDebug = null;
		lstDetalle = null;
		lstDetalleCodItem = null;
		lstDetalleSubDescuento = null;
		lstDetalleSubRecargo = null;
		lstDscRcgGlobal = null;
		lstEmiTelefono = null;
		lstMediosPago = null;
		lstMediosPago = null;
		lstReferencia = null;
		lstSubTotInfo = null;
		lstTOTRetencPercep = null;
		lstaDetalleSubRetencPercep = null;
		listDetalleCodItemArray = null;
		listDetalleSubDescuentoArray = null;
		listDetalleSubDescuentoArray = null;
		listDetalleSubRecargoArray = null;
		listaDetalleSubRetencPercepArray = null;
		listaDetallesArray = null;
		listaDscRcgGlobalArray = null;
		listaReferenciaArray = null;
		listaSubTotInfoArray = null;
		listaReferenciaArray = null;
		listaTOTRetencPercepsArray = null;
		listaTelefonosArray = null;
		listaMediosPagoArray = null;
	}
}
