package com.taface.tafaceapi.facturadorandroid;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import java.io.File;

import Fachada.TAFEApi_v0206;
import Negocio.ExecutorWS;
import TAFACE2ApiEntidad.TADebug;
import TAFACE2ApiEntidad.TAException;
import XML.XMLFACTURA;
import tangible.RefObject;
import utils.Constraints;



public class FacturadorAndroid extends AppCompatActivity {

    //REGION DECLARACIONES UI
    ImageView buttonAceptar, buttonSync, buttonClose, imageButtonEOD;
    EditText editTextCampoEmpRUT, editTextSucId,editTextCajaId, editTextCarpetaOperacion, editTextCampoURLServer;

    //REGION DECLARACIONES UTILS
    public String CarpetaOperacion = "";
    public String EmpRUT;
    public int SucId;
    public int CajaId;
    public String URLServerTAFACE;

    //REGION DECLARACIONES AMBITO
    public RefObject<String> ErrMsg;
    public RefObject<Integer> ErrCod;

    ExecutorWS objWS;
    TAFEApi_v0206 objTAFEApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facturador_android);

        if(!checkWritingPermission())
            requestWritingPermissions();

        boolean existeSDConectada = false;
        if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))   {
            Log.i("SDCARDINFO","SD Card se encuentra presente.");
            existeSDConectada = true;
        } else {
            Log.i("SDCARDINFO","No se encuentra SD Card.");
            existeSDConectada = false;
        }


        //INICIALIZAR CARPETA OPERACION
        File directory;
        if(existeSDConectada)
            directory = new File("/sdcard/", "TAFACE");
        else
            directory = new File(getFilesDir(),"TAFACE");

        if(!directory.exists()){
            if(directory.mkdirs())
                Log.d("Carpeta operacion","Creada");
            else
                Log.d("Carpeta operacion","Fallo al crear");
        }


        //REGION INICIALIZACION COMPONENTES UI
        buttonAceptar = findViewById(R.id.imageButtonStart);
        buttonClose = findViewById(R.id.imageButtonClose);
        buttonSync = findViewById(R.id.imageButtonSync);
        imageButtonEOD = findViewById(R.id.imageButtonEOD);
        editTextCampoEmpRUT = findViewById(R.id.editTextCampoEmpRUT);
        editTextSucId = findViewById(R.id.editTextSucId);
        editTextCajaId = findViewById(R.id.editTextCajaId);
        editTextCarpetaOperacion = findViewById(R.id.editTextCarpetaOperacion);
        editTextCampoURLServer = findViewById(R.id.editTextCampoURLServer);

        objWS = new ExecutorWS();
        objTAFEApi = new TAFEApi_v0206();

        //REGION EVENTOS COMPONENTES UI
        buttonAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objWS.setURLServer(editTextCampoURLServer.getText().toString());
                Firmar();
            }
        });

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        buttonSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Sincronizar();
            }
        });

        imageButtonEOD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             CerrarCaja();
            }
        });

        //REGION INICIALIZACIONES AMBITO Y UTILES
        ErrMsg = new RefObject<>("");
        ErrCod = new RefObject<>(0);
        EmpRUT = "";
        SucId =  0;
        CajaId = 0;

        TADebug debug = new TADebug();
        debug.setRutaCarpetaDebug(directory.getAbsolutePath());
        CarpetaOperacion = debug.getRutaCarpetaDebug();
        editTextCarpetaOperacion.setText(CarpetaOperacion);

    }



    public void Firmar()
    {

        EmpRUT = editTextCampoEmpRUT.getText().toString();
        SucId = Integer.parseInt(editTextSucId.getText().toString());
        CajaId = Integer.parseInt(editTextCajaId.getText().toString());
        CarpetaOperacion = editTextCarpetaOperacion.getText().toString();
        URLServerTAFACE = editTextCampoURLServer.getText().toString();
        ErrCod.argValue = 0;;
        ErrMsg.argValue = "";

        RefObject<String> xmlrespuesta = new RefObject<>("");

        try {
            if(objTAFEApi.Inicializar(ErrMsg,ErrCod,URLServerTAFACE,10, Long.parseLong(EmpRUT),SucId,CajaId,CarpetaOperacion)) {
                XMLFACTURA objFactura = new XMLFACTURA();
                objFactura = objFactura.LoadXML(Constraints.XML_REFIRMA);
                String reversedXML = objFactura.ToXML();

                if(objTAFEApi.FirmarFactura(ErrMsg, ErrCod, reversedXML,xmlrespuesta)) {
                    Log.d("Firmado","OK");
                    Toast.makeText(getApplicationContext(), "Firmado", Toast.LENGTH_SHORT).show();
                }
                else {
                    Log.d("Error firmando ",ErrMsg.argValue);
                    Toast.makeText(getApplicationContext(), "Error firmando " + ErrMsg.argValue, Toast.LENGTH_SHORT).show();
                }
            }
            else {
                Log.d("Error inicializando ",ErrMsg.argValue);
                Toast.makeText(getApplicationContext(), "Error inicializando " + ErrMsg.argValue, Toast.LENGTH_SHORT).show();
            }

        } catch (TAException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Error firmando " +e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void Sincronizar(){
        try {
            EmpRUT = editTextCampoEmpRUT.getText().toString();
            SucId = Integer.parseInt(editTextSucId.getText().toString());
            CajaId = Integer.parseInt(editTextCajaId.getText().toString());
            CarpetaOperacion = editTextCarpetaOperacion.getText().toString();
            URLServerTAFACE = editTextCampoURLServer.getText().toString();
            ErrCod.argValue = 0;;
            ErrMsg.argValue = "";

            if(objTAFEApi.Inicializar(ErrMsg,ErrCod,URLServerTAFACE,10, Long.parseLong(EmpRUT),SucId,CajaId,CarpetaOperacion)) {
                if(objTAFEApi.Sincronizar(ErrMsg, ErrCod))
                    Toast.makeText(getApplicationContext(), "Sincronizacion ok ", Toast.LENGTH_SHORT).show();
            }
        } catch (TAException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Error sincronizando " +e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void CerrarCaja(){
        try {
            EmpRUT = editTextCampoEmpRUT.getText().toString();
            SucId = Integer.parseInt(editTextSucId.getText().toString());
            CajaId = Integer.parseInt(editTextCajaId.getText().toString());
            CarpetaOperacion = editTextCarpetaOperacion.getText().toString();
            URLServerTAFACE = editTextCampoURLServer.getText().toString();
            ErrCod.argValue = 0;;
            ErrMsg.argValue = "";

            if(objTAFEApi.Inicializar(ErrMsg,ErrCod,URLServerTAFACE,10, Long.parseLong(EmpRUT),SucId,CajaId,CarpetaOperacion)) {
                if(objTAFEApi.CerrarCaja(ErrMsg, ErrCod,URLServerTAFACE,Long.parseLong(EmpRUT),SucId,CajaId,CarpetaOperacion))
                    Toast.makeText(getApplicationContext(), "Caja cerrada ok ", Toast.LENGTH_SHORT).show();
            }
        } catch (TAException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Error cerrando caja " +e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //UTILS
    public boolean checkWritingPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public void requestWritingPermissions(){
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                1);
    }
}


//    public void Start(){
//        try {
//            XMLFACTURA objFactura = new XMLFACTURA();
//            objFactura = objFactura.LoadXML(Constraints.XML_REFIRMA);
//            String reversedXML = objFactura.ToXML();
//            Log.d("XML=",reversedXML);
//
//            Toast.makeText(getApplicationContext(), "Data: "  + objFactura.getDGI().getEMIRUCEmisor(),Toast.LENGTH_LONG).show();
//        }catch(Exception e)
//        {
//            e.printStackTrace();
//        }
//    }
//    public void WS_SincronizacionCAEParte(){
//        try {
//            EmpRUT = editTextCampoEmpRUT.getText().toString();
//            SucId =  Integer.valueOf(editTextSucId.getText().toString());
//            CajaId = Integer.valueOf(editTextCajaId.getText().toString());
//            List<Map<String, Object>> setCAEParteenBD = new ArrayList<Map<String, Object>>();
//
//            WSSincronizacionCAEParte0206ExecuteResponse script = objWS.executeCAEParte(1, EmpRUT, SucId, (short)CajaId, setCAEParteenBD);
//
//            if(script != null) {
//                if(!script.getPerrorreturn()) {
//                    Log.d("CAE SYNC: ", "OK");
//                    for(int i =0; i<script.getPsdtcaeparteresponse().getSDTCAEParteResponse206SDTCAEParteResponse206Item().size();i++)
//                    {
//                        Log.d("CAE TIPO: ", String.valueOf(script.getPsdtcaeparteresponse().getSDTCAEParteResponse206SDTCAEParteResponse206Item().get(i).getCAE().getCAETipoCFE()));
//                    }
//
//                }
//                else
//                    Log.d("CAE SYNC ERROR" , script.getPerrormessage());
//            }
//        }
//        catch(Exception e)
//        {
//            e.printStackTrace();
//        }
//    }
//
//    public void WS_SincronizacionCertificadoDGI(){
//        try {
//            EmpRUT = editTextCampoEmpRUT.getText().toString();
//            SucId =  Integer.valueOf(editTextSucId.getText().toString());
//            CajaId = Integer.valueOf(editTextCajaId.getText().toString());
//            SDTCertificadoDGIRequest setCAEParteenBD = new SDTCertificadoDGIRequest();
//
//            WSSincronizacionCertificadosDGI0202ExecuteResponse script = objWS.executeCertificadoDGI((short)CajaId,1, EmpRUT, SucId,  setCAEParteenBD);
//
//            if(script != null) {
//                if(!script.getPerrorreturn()) {
//                    Log.d("CERT DGI SYNC: ", "OK");
//                    for(int i =0; i<script.getPsdtcertificadodgiresponse().getSDTCertificadoDGIResponseSDTCertificadoDGIResponseItem().size();i++)
//                    {
//                        Log.d("CERT DGI GUID: ", String.valueOf(script.getPsdtcertificadodgiresponse().getSDTCertificadoDGIResponseSDTCertificadoDGIResponseItem().get(i).getCertDGIRowGUID()));
//                    }
//
//                }
//                else
//                    Log.d("CERT DGI SYNC ERROR" , script.getPerrormessage());
//            }
//        }
//        catch(Exception e)
//        {
//            e.printStackTrace();
//        }
//    }
//
//    public void WS_SincronizacionCertificado(){
//        try {
//            EmpRUT = editTextCampoEmpRUT.getText().toString();
//            SucId =  Integer.valueOf(editTextSucId.getText().toString());
//            CajaId = Integer.valueOf(editTextCajaId.getText().toString());
//            XMLCERTIFICADOREQUEST setCAEParteenBD = new XMLCERTIFICADOREQUEST();
//
//            WSSincronizacionCertificados0200ExecuteResponse script = objWS.executeCertificado((short)CajaId,1, EmpRUT, SucId,  setCAEParteenBD);
//
//            if(script != null) {
//                if(!script.isPerrorreturn()) {
//                    Log.d("CERTIFICADO SYNC: ", "OK");
//                    for(int i =0; i<script.getPsdtcertificadoresponse().getSDTCertificadoResponseSDTCertificadoResponseItem().size();i++)
//                    {
//                        Log.d("CERTIFICADO ROWGUID: ", String.valueOf(script.getPsdtcertificadoresponse().getSDTCertificadoResponseSDTCertificadoResponseItem().get(i).getCertRowGUID()));
//                    }
//
//                }
//                else
//                    Log.d("CERTIFICADO SYNC ERROR" , script.getPerrormessage());
//            }
//        }
//        catch(Exception e)
//        {
//            e.printStackTrace();
//        }
//    }
//
//    public void WS_SincronizacionEmpresas(){
//        try {
//            EmpRUT = editTextCampoEmpRUT.getText().toString();
//            SucId =  Integer.valueOf(editTextSucId.getText().toString());
//            CajaId = Integer.valueOf(editTextCajaId.getText().toString());
//
//            WSSincronizacionEmpresas0202ExecuteResponse script = objWS.executeEmpresa((short)CajaId,"",EmpRUT, SucId);
//
//            if(script != null) {
//                if(!script.isPerrorreturn()) {
//                    Log.d("EMPRESA SYNC: ", "OK");
//                    for(int i =0; i<script.getPsdtempresaresponse().getSDTEmpresaResponse202SDTEmpresaResponse202Item().size();i++)
//                    {
//                        Log.d("EMPRESA NOMBRE: ", String.valueOf(script.getPsdtempresaresponse().getSDTEmpresaResponse202SDTEmpresaResponse202Item().get(i).getEmpNom()));
//                    }
//
//                }
//                else
//                    Log.d("EMPRESA SYNC ERROR" , script.getPerrormessage());
//            }
//        }
//        catch(Exception e)
//        {
//            e.printStackTrace();
//        }
//    }
//
//    public void WS_SincronizacionSucursalCaja(){
//        try {
//            EmpRUT = editTextCampoEmpRUT.getText().toString();
//            SucId =  Integer.valueOf(editTextSucId.getText().toString());
//            CajaId = Integer.valueOf(editTextCajaId.getText().toString());
//
//            WSSincronizacionSucursales0205ExecuteResponse script = objWS.executeSucursal(1,EmpRUT,SucId,"");
//
//            if(script != null) {
//                if(!script.isPerrorreturn()) {
//                    Log.d("SUCCAJA SYNC: ", "OK");
//                    for(int i =0; i<script.getPsdtsucursalresponse().getSDTSucursalResponse205SDTSucursalResponse205Item().size();i++)
//                    {
//                        Log.d("SUC ROWGUID: ", String.valueOf(script.getPsdtsucursalresponse().getSDTSucursalResponse205SDTSucursalResponse205Item().get(i).getSucRowGUID()));
//                        Log.d("CAJA ID: ", String.valueOf(script.getPsdtsucursalresponse().getSDTSucursalResponse205SDTSucursalResponse205Item().get(i).getCajas().getCajasItem().get(0).getCajaUniqueId()));
//                    }
//
//                }
//                else
//                    Log.d("SUCCAJA  SYNC ERROR" , script.getPerrormessage());
//            }
//        }
//        catch(Exception e)
//        {
//            e.printStackTrace();
//        }
//    }
//
//    public void MostrarMensaje(String mensaje){
//
//        Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_LONG).show();
//    }
//
//    public void LimpiarCarpetaOperacion(){
//        File directory = new File(getFilesDir(), "TAFACE");
//        boolean result = directory.delete();
//        if(!result) {
//            File[] files = directory.listFiles();
//            for (int i = 0; i < files.length; i++) {
//                result = files[i].delete();
//                if (result)
//                    result = false;
//            }
//        }
//    }
//
//    public void ListFilesDirectory(){
//        if(!checkWritingPermission())
//            requestWritingPermissions();
//
//        File directory = new File(getFilesDir(), "TAFACE");
//        File[] files = directory.listFiles();
//        Log.d("Files", "Cantidad: "+ files.length);
//        for (int i = 0; i < files.length; i++)
//        {
//            Log.d("FileName", files[i].getName() + " File Size: " + Integer.parseInt(String.valueOf(files[i].length()/1024)));
//        }
//        Log.d("END OF FILES", "\n:" );
//    }
//
//    public void SignTest(){
//
//        String CertClave = "AYU2V4uVCBTVCjK6e42Y+w==";
//        String CertArchivo = "6FD9FFCD4FD6FE6C544A28A81D25FAF13D721C6C137A936A6BA9E5DACA7034017D9B47DBF6F769CDD8CA46FC4C86092E9C477933A4DC0175B9D930526C0E8BE1596A1F63889BCEF4474BE35BC1B481CD9A87A6047AD9A104969DF7535235364F95148C29FD075153CE1F489600E79DCB5FE9C0EE80C6865560F7CE9B7E6C5107F624938C624449A459BEAABA6D0EBE9C715382FDA2FBA14C92C96320E31E25F445287E0FFE1FDABFD4DC87C0BDB100DBE037A7D9447F6EDF0FC2D0798DBC28859FDE2CE5212D57AF77DB080D8D26E751DDB09ABB4E2ACC5F1BFE3C27C3D698D185D13497D1C4A863D7D6F3FD3021FB09E73D27757B663156A0119644FAF519DC3D29C6472B12F68BD7E9F676CA12C5083AD343828FFEB7EFE21ECB684359EA3FFFAE610A4FF14A1CC0CC9C03D6E46F93565E92FAA44E0EC57C68834EE8877CA603CC0EFE6929DBC655BE56BA58E42EEBAEDB290F6E046E3524A6717C002C69E186BE0E1B1CB2BDDE8AF408A376F5B23A7A0272953FF1BF482504F11C5FFE11A4972D7499718D91A052C004FBFD413979E1CA90A1CBE5CE57E0D3512C57BF1BF8664E2EE4D6093E493A773CC03D32C2FB94EF14AA35138D49094807863E737BB322715FA0BA61AF134F6C13E72EBAF887A08908ABA20FB86EFEEF1D4567FA37962AC1DCDF49A2128F6E4D5C09147C5EBB5DB0BFEFF254930AB71E4552D04FA99635A47ED3E036AF29698B5B85CF43137F59BD747C41739BA640E1106FA19BC58DCF0F7CE613B89C3EDEA6A8952C44E27597D75CA7E9F9D79DDDED863893AB85596AB32B8E122D0C13DDCE69B859477A2401B86EFD24B92421912FA67AA8D60E6298D483AC5E284B3E4CFF135447687AE98E95F021FDD94C2A7727F08A7FED5ABE30ED96E58D7F32915FA23EBE6AB13A2FCCBE450F2749D8BF366B1142CC08903638E1EB66547D5BEDA43E29E5E545DFFC76134F5CCCE0D54B49FD6FE9C41785BFBDDCE64A40D47554FC41212088F984858A13F8B96BD8C435A66ACFE42C1B5A404B72FBB480C7702365E27DB9FAEB78EBEB4F927421ADD1A8E6F4DA7E33E2C4F83C80B83B117C282AA7BA5BEC5A0B5177D6B00DB26F84A53A94F8C99D310AD72D1A17A5EC0AD43847E78982304DFD35A6AC222FD7B0482AE4F61156F3EFD9E7F348D8C56C5AF3BCE8C8DEB66E250716B69C468B74C2E4067C0D13C46D70DABD770B83D701663D6EB191C28A582A9E69EC92DD9E4695DD8A1E05CE15190D1DA8124426FB31BFB9FEB5B95F19EF280CEB2096FFFFC7983818D366C8E06CFD1563BB3B193D08DFBDA1E4204F39B9F668A17B607825973F99DF3105C6439B3E3273BC79E285D13A16523C59EFA090C99C8A3A01F1C8A6C92ED08C56CD8CB9FB64FD5C9BA1C55882FD6DCA1A3E55C9D44BC83106B6EFB9F73A2ED5B00DC42B3BEEB84921A654FF657E234B8AF8E6AF9152D6BBBC8572F93932E0D2A3174A623E5EF9772007E8986E75E276DE698B904C51F7BF0F0BC5C744F1B12ABB3005255F8DAD568398AD1B19B64E0EBAF65893128F19FB248AAB69D9523701A6D7B1A39A00B2B80A02B08110C8C3E8F58FB1AC5B98783CE9857E4EF38DCCCFAEEB158A5D88731EB5C8C6DDD399313DEBFF9C45433D86104661C79B5AA48375D73F33491BDE870B3B9FE4432D74742023DED5C81940DBE45E893C632FCE1DD4266D7CECDC274010C2E5C728E7D56F35D21A43A3475CF574DE6E2611FFC573F749E7BA9D84C400DA0BD6BCA8E631025A7E058F7B448FF9C997B066F51F13A420D3D45E199EA86484F14F0B8EDA2D9C43FA62F2799EB66E9CA6C62308A98BE891ECFE09A068144B88CDFEF5FCBD45598E6D6EA7114A4FBF05F38F0D9FE03FF1C3501FC563634D791D3E3AAA1DA7D8E5EEEC7F550D25CE9634B119F8E95D8A50F7AA727EDBCE31BB64BCC18F445C58986BD715E1676165088F63F7CCF76077AB2B097DA378DA04E4BD7679CF2F73B43011ECA14684C5ED6E369BE928E574F75C30071E6B38B92BE18B7C645E42BAB67D444247F3B6DA41F69BF26467FB79C14D574E4FD6DBF84EE1A24CC4B1EF370247EDBDED2BCCD614F8328FABDC0D84435F0E3F646CC47A5280E8463832EAAE6F09016C9FA7ED13AAA319D9D59D9BC2D442D559034808DB128F236675BD920CC61E68CA81F1BA08373AF2EF0DE82788191134400D7845426F3AA07D5436091B15B6C05BFE67DF23D7819CC51439EA5C8B85907040D4E47CFCF7608FFF94AF3E2D9BF2C3485210C4FBB8BB0754EB04B603C4673CBD44D4865CD933887F0B3C8709FE187A31E510543675AF744B7606D988A6ADF57309F5F83D70418BFD1EA8441225C5FE758C2286DB362E065C64F622D413663B4DC152EE2E4AE5C03B465986E4450D45413762748EBB053831294A6DE95369008ABFBE957B3177ECBBB462CDC353D178A048B73B7E9BA96534AB426B24D9CF34447E2D047D0B1189E83F723D56AD389FFEC86EE7D6E57D665AB970D7140F39BA21A3209B9F5CC542DC63C751425DBBBE7930FAB02A5149ED10164464058C2BBE0814AEB034B37E3A49569E82F04F3C97BA16B40764CCA3CA01049770E62F505EE148AD70F594C639BCBD4A9D179A98FD2D03CBA12CEC378CB83F7D1494C976EBFD4176DDE68C37416CA6585A1AD2B749A42C2954A4C4D2A9ADC05D21D10FAB6D74D51149FECDED34DE33E4DC5968F2156B7D551D713504A20F256581C0425808DF45C39A06AE3DEBE11F2B23A1EC6C70F5AA2924F4B27841561F0272D2CBE3C810C4630FC645117911E8B3AD120B359AD5DF265C7D63D09DBE139983E24EB40A3D2D7B827473ACA92DC7E2ED02457E610886D7FBDC7066EF43064F7301860522D4CECD128768255B9810EF8B94E605160D77A5874E6F2040048236DC0AB44B509E805999B3EFC4EE739648E090ECD45F2DEC4D51CFA5DDE5D893227CEB6B2FD3F16ECBA715BCFEFE43D62B91244F322F6E936DC2E68A453F17C9F496E2FC3A4003D0B8D6FA1CE45572AF3FA97C89CC9FA9CB630A227AFDAC698E2655BC7CD9F9B6A512C664886E6D3F4FE3C2E987B75D85D5A8585A0E65224BB89E3BA2058C7215C48E5E7330F67D667D28A7A8D4AEB0056B215624E161AA78D0A70A8515B0D0AEE33DBDFA5CC7A3E6143DCEFAAAD36402D19BE27549217CE2C75BBFBE27F6579969EF7B66C267D61A43A55942D14931821C5DA7E808688481E83300F2E61D01A518E5215E34ED0C17CEBE65568A41136BDEF7C2101D1DF99B506F16FAB83DE6EF57995D7DB12C9FBA7D6585399DF4A26AF715AECDDB2287423D04A90ABD5400E76F75FAAA46F808D427C4E02A1A6868C12FB26FA902CA5C2B4009B5F67256A0053C3E34C00AECE570665872BEA700F0E0A533F9CC6A43A8E9BEA3629B6F9C774785FB536270039E525055EC5C7661B2E22423056BC89AE974B3FAB38851CB4332DFD368AF15977AE064EACFAF2399DB4C779E4AADCFB762F63CA6CA5F1C583E245165F1A16268AC07E4CDF6A9C882AA88523B4AB39B6CA0B17D7C287C1611B6530C272CE1548463D1E45D2F427E8471992DCCA32B336CA5F3F270388D4DF00B43062BF438AFB82114CD7337F191E553C6FB9871581DB75A0EB4AC5A414AF7E3500CBA3154CC8223CDDDFE1C40CFFF75F84D5D899F246F04A4A42D6B78503F5173CA605486FEF7F890D99861FF74678821D839F973097D42FF2DE430A9D0DEBA9A76147ADF36FFB51D7377478B3C0ECC885950D61B160A35D6DE30D8FC0528714E1D6E64DAEF02BE10CB7C31367D07834F17A2641E30D34E47E7DCE8032B0FF077BC5C3F0F8CE189C7662DF9BF7DA33815471A73E984E02494A2CCE28100961A71A0D74DF1CEB908054C0E06E5BC715924B7C7C9191D1F80A6E1164B537ABCDBF63DF454937C4DDD720ADD1D41EC08BB75401F136BC26F918B80CF55C47CD01730BE7A85691D77A11D9C3D849F255792DB11A59E14F3474C1EE73D84307F10B729E05DDC1BA22C5CAF0BB345F89F55AFD7F6AFE8FDFC25C1365B0852DFC49583726470F3292C34134B4B4D1AD843F795BE4E5BF46D0AB5EFE8FAAAFDE7A4FF67F80981E356C68DFF9CFA0566F2845D60BA934B9BB18A0FE8F974ADE4CB3B5BEF639B94EECF2AD163AB72DB06E2B8957FBEBE335DC886DE8C6F633655400DD398023CED77D32A8A1F51AB82947E63E8056B779A712C236C32F6E137FA6C4B4B9F7E4117D3F01B81E6FB33063E4F3F24334B0FA4906B7B3B528685CBF33A6CD6730BF40B18ECA430CF84F20E5F7C6579838F99FA5B3163E76CF2B823158E7D371218430D8D3E52E25A098AECCBBBD20A605F85ADDD9024142DD77157A935BF31BA23B8D30A0FB2F621B7BCFE5256FE88E7F8FDDE40479A24D2186DBD0CCFB9ABF776808AB94DBC4BB593F7D43A9C40C14CFA56C8FD925DD2DD88F1C964C73B8292A03434DA121AA262E779039AEFEC33920EEFCB2DD019E9F01A3EA8621364AA7B73766C4ECBDB69D25C8AD9F28C1691EACABD2CDE8015AA7BB7E2EEE7A6EB9F83368AC9566763BA435038B2F58D13D02BFAEAC5282E79570A84A226E198504A280ECF3DC88C36CCB39AAB89E3181126BC722656A4E3A38DE95DE3E9BA107C1F3CC5CA5B892B869DEFBEFFFD2D220D568796111083252D54009ABFFC88FCAAAA74BA0230C93AB713E7D4499DE333259CA8C122249072216CDEFB2FA776F2C8131D9C828C605EFEAEAE20CD7379B13EF0B5DE7595118CE32B7CB17D599B034D4185C3BE7947A481BC4858E8F9BA3370D8B9B79875AF45BFAEDE582A865350416CCFDBBECFBB8DC10B65F4B86C0916FD293396294A93110F09B9278EE4BB22433AC407BD25DCA41A46D3EBCD9FBA471887A0175020CA972259";
//        String Xml = "<CFE version=\"1.0\"><eTck><TmstFirma>2018-11-08T12:03:18.153000-03:00</TmstFirma><Encabezado><IdDoc><TipoCFE>101</TipoCFE><Serie>A</Serie><Nro>466</Nro><FchEmis>2018-10-18</FchEmis><MntBruto>1</MntBruto><FmaPago>1</FmaPago></IdDoc><Emisor><RUCEmisor>212661610019</RUCEmisor><RznSoc>Phoenix S.A.</RznSoc><NomComercial>New Age Data</NomComercial><GiroEmis>Papel y sobres</GiroEmis><CorreoEmisor>giro@gandulia.com</CorreoEmisor><EmiSucursal>Principal</EmiSucursal><CdgDGISucur>1</CdgDGISucur><DomFiscal>Buenos Aires 574</DomFiscal><Ciudad>Montevideo</Ciudad><Departamento>Montevideo</Departamento></Emisor><Totales><TpoMoneda>UYU</TpoMoneda><MntNoGrv>1.00</MntNoGrv><MntExpoyAsim>0.00</MntExpoyAsim><MntImpuestoPerc>0.00</MntImpuestoPerc><MntIVaenSusp>0.00</MntIVaenSusp><MntNetoIvaTasaMin>0.00</MntNetoIvaTasaMin><MntNetoIVATasaBasica>0.00</MntNetoIVATasaBasica><MntNetoIVAOtra>0.00</MntNetoIVAOtra><IVATasaMin>10.00</IVATasaMin><IVATasaBasica>22.00</IVATasaBasica><MntIVATasaMin>0.00</MntIVATasaMin><MntIVATasaBasica>0.00</MntIVATasaBasica><MntIVAOtra>0.00</MntIVAOtra><MntTotal>1.00</MntTotal><MntTotRetenido>0.00</MntTotRetenido><MntTotCredFisc>0.00</MntTotCredFisc><CantLinDet>1</CantLinDet><MontoNF>0.00</MontoNF><MntPagar>1.00</MntPagar></Totales></Encabezado><Detalle><Item><XML.EnvioCFE_-Item__Det__Fact_-Item><NroLinDet>1</NroLinDet><IndFact>1</IndFact><NomItem>ITEM1</NomItem><Cantidad>1.00</Cantidad><UniMed>N/A</UniMed><PrecioUnitario>1.00</PrecioUnitario><DescuentoPct>0.00</DescuentoPct><DescuentoMonto>0.00</DescuentoMonto><RecargoPct>0.00</RecargoPct><RecargoMnt>0.00</RecargoMnt><MontoItem>1.00</MontoItem><this_-1 reference=\"../../..\"/></XML.EnvioCFE_-Item__Det__Fact_-Item></Item></Detalle><CAEData><CAE_ID>963046914</CAE_ID><DNro>1</DNro><HNro>500000</HNro><FecVenc>2018-12-31</FecVenc></CAEData></eTck></CFE>";
//        String Nodo = "CFE";
//        String pClave = "";
//        tangible.RefString XMLFirmado = new RefObject<>("");
//        tangible.RefString ErrorMessage = new RefObject<>("");
//
//        TAFACESign objSign = new TAFACESign();
//        try {
////            String claveDesencriptada = objSign.DesencriptarString(claveEncriptada, pClave);
//            if(objSign.FirmarXMLConCertParam(Xml, Nodo, CertArchivo, CertClave, XMLFirmado, ErrorMessage))
//                Toast.makeText(getApplicationContext(),"Firmado ok", Toast.LENGTH_SHORT).show();
//            else
//                Toast.makeText(getApplicationContext(),"Firma con errores", Toast.LENGTH_SHORT).show();
//        } catch (TAException e) {
//            e.printStackTrace();
//        }
//    }