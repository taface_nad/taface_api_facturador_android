package TAFE2ApiFirmar;

//import TAFACE2ApiEntidad.*;
import TAFACE2ApiEntidad.TAException;
import XML.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TAComprobante {

    public static class Comprobante {

        /**
         * @return the EmpId
         */
        public static String getEmpId() {
            return EmpId;
        }

        /**
         * @param aEmpId the EmpId to set
         */
        public static void setEmpId(String aEmpId) {
            EmpId = aEmpId;
        }

        /**
         * @return the SucId
         */
        public static String getSucId() {
            return SucId;
        }

        /**
         * @param aSucId the SucId to set
         */
        public static void setSucId(String aSucId) {
            SucId = aSucId;
        }

        /**
         * @return the CajaId
         */
        public static String getCajaId() {
            return CajaId;
        }

        /**
         * @param aCajaId the CajaId to set
         */
        public static void setCajaId(String aCajaId) {
            CajaId = aCajaId;
        }

        /**
         * @return the CteId
         */
        public static String getCteId() {
            return CteId;
        }

        /**
         * @param aCteId the CteId to set
         */
        public static void setCteId(String aCteId) {
            CteId = aCteId;
        }

        /**
         * @return the CAEId
         */
        public static String getCAEId() {
            return CAEId;
        }

        /**
         * @param aCAEId the CAEId to set
         */
        public static void setCAEId(String aCAEId) {
            CAEId = aCAEId;
        }

        /**
         * @return the CAEParteId
         */
        public static String getCAEParteId() {
            return CAEParteId;
        }

        /**
         * @param aCAEParteId the CAEParteId to set
         */
        public static void setCAEParteId(String aCAEParteId) {
            CAEParteId = aCAEParteId;
        }

        /**
         * @return the CteCAESerie
         */
        public static String getCteCAESerie() {
            return CteCAESerie;
        }

        /**
         * @param aCteCAESerie the CteCAESerie to set
         */
        public static void setCteCAESerie(String aCteCAESerie) {
            CteCAESerie = aCteCAESerie;
        }

        /**
         * @return the CteCAENro
         */
        public static String getCteCAENro() {
            return CteCAENro;
        }

        /**
         * @param aCteCAENro the CteCAENro to set
         */
        public static void setCteCAENro(String aCteCAENro) {
            CteCAENro = aCteCAENro;
        }

        /**
         * @return the CteCAENroAutorizacion
         */
        public static String getCteCAENroAutorizacion() {
            return CteCAENroAutorizacion;
        }

        /**
         * @param aCteCAENroAutorizacion the CteCAENroAutorizacion to set
         */
        public static void setCteCAENroAutorizacion(String aCteCAENroAutorizacion) {
            CteCAENroAutorizacion = aCteCAENroAutorizacion;
        }

        /**
         * @return the CteCAETipoCFE
         */
        public static String getCteCAETipoCFE() {
            return CteCAETipoCFE;
        }

        /**
         * @param aCteCAETipoCFE the CteCAETipoCFE to set
         */
        public static void setCteCAETipoCFE(String aCteCAETipoCFE) {
            CteCAETipoCFE = aCteCAETipoCFE;
        }

        /**
         * @return the CteCAENroDesde
         */
        public static String getCteCAENroDesde() {
            return CteCAENroDesde;
        }

        /**
         * @param aCteCAENroDesde the CteCAENroDesde to set
         */
        public static void setCteCAENroDesde(String aCteCAENroDesde) {
            CteCAENroDesde = aCteCAENroDesde;
        }

        /**
         * @return the CteCAENroHasta
         */
        public static String getCteCAENroHasta() {
            return CteCAENroHasta;
        }

        /**
         * @param aCteCAENroHasta the CteCAENroHasta to set
         */
        public static void setCteCAENroHasta(String aCteCAENroHasta) {
            CteCAENroHasta = aCteCAENroHasta;
        }

        /**
         * @return the CteCAEVencimiento
         */
        public static String getCteCAEVencimiento() {
            return CteCAEVencimiento;
        }

        /**
         * @param aCteCAEVencimiento the CteCAEVencimiento to set
         */
        public static void setCteCAEVencimiento(String aCteCAEVencimiento) {
            CteCAEVencimiento = aCteCAEVencimiento;
        }

        /**
         * @return the CteFchHoraFirma
         */
        public static String getCteFchHoraFirma() {
            return CteFchHoraFirma;
        }

        /**
         * @param aCteFchHoraFirma the CteFchHoraFirma to set
         */
        public static void setCteFchHoraFirma(String aCteFchHoraFirma) {
            CteFchHoraFirma = aCteFchHoraFirma;
        }

        /**
         * @return the CertId
         */
        public static String getCertId() {
            return CertId;
        }

        /**
         * @param aCertId the CertId to set
         */
        public static void setCertId(String aCertId) {
            CertId = aCertId;
        }

        /**
         * @return the CteXMLEntrada
         */
        public static String getCteXMLEntrada() {
            return CteXMLEntrada;
        }

        /**
         * @param aCteXMLEntrada the CteXMLEntrada to set
         */
        public static void setCteXMLEntrada(String aCteXMLEntrada) {
            CteXMLEntrada = aCteXMLEntrada;
        }

        /**
         * @return the CteXMLRespuesta
         */
        public static String getCteXMLRespuesta() {
            return CteXMLRespuesta;
        }

        /**
         * @param aCteXMLRespuesta the CteXMLRespuesta to set
         */
        public static void setCteXMLRespuesta(String aCteXMLRespuesta) {
            CteXMLRespuesta = aCteXMLRespuesta;
        }

        /**
         * @return the CteXMLFirmado
         */
        public static String getCteXMLFirmado() {
            return CteXMLFirmado;
        }

        /**
         * @param aCteXMLFirmado the CteXMLFirmado to set
         */
        public static void setCteXMLFirmado(String aCteXMLFirmado) {
            CteXMLFirmado = aCteXMLFirmado;
        }

        /**
         * @return the CteAdendaTexto
         */
        public static String getCteAdendaTexto() {
            return CteAdendaTexto;
        }

        /**
         * @param aCteAdendaTexto the CteAdendaTexto to set
         */
        public static void setCteAdendaTexto(String aCteAdendaTexto) {
            CteAdendaTexto = aCteAdendaTexto;
        }

        /**
         * @return the CteFchEmision
         */
        public static String getCteFchEmision() {
            return CteFchEmision;
        }

        /**
         * @param aCteFchEmision the CteFchEmision to set
         */
        public static void setCteFchEmision(String aCteFchEmision) {
            CteFchEmision = aCteFchEmision;
        }

        /**
         * @return the CteDocOriginalTipo
         */
        public static String getCteDocOriginalTipo() {
            return CteDocOriginalTipo;
        }

        /**
         * @param aCteDocOriginalTipo the CteDocOriginalTipo to set
         */
        public static void setCteDocOriginalTipo(String aCteDocOriginalTipo) {
            CteDocOriginalTipo = aCteDocOriginalTipo;
        }

        /**
         * @return the CteDocOriginalSerie
         */
        public static String getCteDocOriginalSerie() {
            return CteDocOriginalSerie;
        }

        /**
         * @param aCteDocOriginalSerie the CteDocOriginalSerie to set
         */
        public static void setCteDocOriginalSerie(String aCteDocOriginalSerie) {
            CteDocOriginalSerie = aCteDocOriginalSerie;
        }

        /**
         * @return the CteDocOriginalNro
         */
        public static String getCteDocOriginalNro() {
            return CteDocOriginalNro;
        }

        /**
         * @param aCteDocOriginalNro the CteDocOriginalNro to set
         */
        public static void setCteDocOriginalNro(String aCteDocOriginalNro) {
            CteDocOriginalNro = aCteDocOriginalNro;
        }

        /**
         * @return the CteCajaUniqueId
         */
        public static String getCteCajaUniqueId() {
            return CteCajaUniqueId;
        }

        /**
         * @param aCteCajaUniqueId the CteCajaUniqueId to set
         */
        public static void setCteCajaUniqueId(String aCteCajaUniqueId) {
            CteCajaUniqueId = aCteCajaUniqueId;
        }

        /**
         * @return the CteVersionAPI
         */
        public static String getCteVersionAPI() {
            return CteVersionAPI;
        }

        /**
         * @param aCteVersionAPI the CteVersionAPI to set
         */
        public static void setCteVersionAPI(String aCteVersionAPI) {
            CteVersionAPI = aCteVersionAPI;
        }

        /**
         * @return the CteSincronizado
         */
        public static String getCteSincronizado() {
            return CteSincronizado;
        }

        /**
         * @param aCteSincronizado the CteSincronizado to set
         */
        public static void setCteSincronizado(String aCteSincronizado) {
            CteSincronizado = aCteSincronizado;
        }

        /**
         * @return the CteSyncId
         */
        public static String getCteSyncId() {
            return CteSyncId;
        }

        /**
         * @param aCteSyncId the CteSyncId to set
         */
        public static void setCteSyncId(String aCteSyncId) {
            CteSyncId = aCteSyncId;
        }

        /**
         * @return the CteFchHoraSync
         */
        public static String getCteFchHoraSync() {
            return CteFchHoraSync;
        }

        /**
         * @param aCteFchHoraSync the CteFchHoraSync to set
         */
        public static void setCteFchHoraSync(String aCteFchHoraSync) {
            CteFchHoraSync = aCteFchHoraSync;
        }
        private Object x;
        private static String EmpId = "EmpId";
        private static String SucId = "SucId";
        private static String CajaId = "CajaId";
        private static String CteId = "CteId";
        private static String CAEId = "CAEId";
        private static String CAEParteId = "CAEParteId";
        private static String CteCAESerie = "CteCAESerie";
        private static String CteCAENro = "CteCAENro";
        private static String CteCAENroAutorizacion = "CteCAENroAutorizacion";
        private static String CteCAETipoCFE = "CteCAETipoCFE";
        private static String CteCAENroDesde = "CteCAENroDesde";
        private static String CteCAENroHasta = "CteCAENroHasta";
        private static String CteCAEVencimiento = "CteCAEVencimiento";
        private static String CteFchHoraFirma = "CteFchHoraFirma";
        private static String CertId = "CertId";
        private static String CteXMLEntrada = "CteXMLEntrada";
        private static String CteXMLRespuesta = "CteXMLRespuesta";
        private static String CteXMLFirmado = "CteXMLFirmado";
        private static String CteAdendaTexto = "CteAdendaTexto";
        private static String CteFchEmision = "CteFchEmision";
        private static String CteDocOriginalTipo = "CteDocOriginalTipo";
        private static String CteDocOriginalSerie = "CteDocOriginalSerie";
        private static String CteDocOriginalNro = "CteDocOriginalNro";
        private static String CteCajaUniqueId = "CteCajaUniqueId";
        private static String CteVersionAPI = "CteVersionAPI";
        private static String CteSincronizado = "CteSincronizado";
        private static String CteSyncId = "CteSyncId";
        private static String CteFchHoraSync = "CteFchHoraSync";
    }
    private String sSQL;
    private NadMADato NadDato;
    private NadFormato Fto = new NadFormato();
    private AssemblyQuery assembly = new AssemblyQuery();

    public final ResultSet ConsComprobantes(int EmpId, int SucId, int CajaId, int DocOrigNro, String DocOrigSerie, String DocOrigTipo, java.util.Date FechaHoraFirma, tangible.RefObject<String> XMLRespuesta) throws SQLException, TAException, Exception {
        ResultSet dt = null;
        sSQL = "Select * From Comprobante Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajaId + " And CteDocOriginalNro = " + DocOrigNro + " And CteDocOriginalSerie = '" + DocOrigSerie + "' And CteDocOriginalTipo = '" + DocOrigTipo + "' And CteFchEmision = " + Fto.FechaSQL(FechaHoraFirma);
        try {
            dt = NadDato.GetTable(sSQL);
            //dt.TableName = this.Tb.Comprobante.Nombre;
            if (NadDato.getRowCount(dt) > 0) {
                dt = NadDato.GetTable(sSQL);
                XMLRespuesta.argValue = (dt.getString("CteXMLRespuesta"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(TAComprobante.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dt;
    }

    public final boolean CargarDatosComprobante(long EmpId, short SucId, short CajId, int TipoDeComprobante, String SerieComprobante, int NroComprobante, tangible.RefObject<String> XMLEntrada, tangible.RefObject<String> XMLRespuesta) throws SQLException, Exception {

        boolean cargarDatosComprobantes = false;
        try {
            ResultSet dt = null;
            sSQL = "Select * From Comprobante Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajId + " And CteCAENro = " + NroComprobante + " And CteCAESerie = '" + SerieComprobante + "' And CteCAETipoCFE = " + TipoDeComprobante;
            dt = NadDato.GetTable(sSQL);
            if (NadDato.getRowCount(dt) > 0) {
                dt = NadDato.GetTable(sSQL);
                XMLRespuesta.argValue = (dt.getString("CteXMLRespuesta"));
                XMLEntrada.argValue = (dt.getString("CteXMLEntrada"));
                cargarDatosComprobantes = true;
            } else {
                cargarDatosComprobantes = false;
            }

        } catch (Exception ex) {
            Logger.getLogger(TAComprobante.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return cargarDatosComprobantes;
    }

    public final ResultSet ConsComprobantesCFC(int EmpId, int SucId, int CajaId, int NroCFC, String SerieCFC, String TipoCFC, tangible.RefObject<String> XMLRespuesta) throws SQLException {
        ResultSet dt = null;
        sSQL = "Select * From Comprobante Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajaId + " And CteCAENro = " + NroCFC + " And CteCAESerie = '" + SerieCFC + "' And CteCAETipoCFE = '" + TipoCFC + "'";
        dt = NadDato.GetTable(sSQL);
        //dt.TableName = this.Tb.Comprobante.Nombre;
        if (NadDato.getRowCount(dt) > 0) {
            dt = NadDato.GetTable(sSQL);
            XMLRespuesta.argValue = (dt.getString("CteXMLRespuesta"));
        }
        return dt;
    }

    public final ResultSet ConsComprobantes(int EmpId, int SucId, short CajaId, int CteId) throws SQLException {
        ResultSet dt = null;
        sSQL = "Select * From Comprobante Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajaId + " And CteId = " + CteId;
        dt = NadDato.GetTable(sSQL);
        return dt;
    }

    public final boolean EliminarComprobantesSincronizado(int EmpId, int SucId, short CajaId) throws Exception {
        boolean tempEliminarComprobantesSincronizado = false;
        sSQL = "Delete From Comprobante Where CteSincronizado = '1' And EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajaId;
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

        try {
            sttment.execute();
            tempEliminarComprobantesSincronizado = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al eliminar comprobantes sincronizados.");
        } finally {
            NadDato.FinTrans();
            NadDato.schrinkBD();
        }

        NadDato.Conectar();
        NadDato.IniTrans();
        sSQL = "Delete from LogErrores where empid = 0";
        sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);
        try {
            sttment.execute();
            tempEliminarComprobantesSincronizado = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al eliminar log errores.");
        } finally {
            NadDato.FinTrans();
            NadDato.schrinkBD();
        }
        return tempEliminarComprobantesSincronizado;
    }

    //eliminar los comprobantes sincronizados en BD con una cantidad de dias menor a "dias"
    public final boolean EliminarComprobantesSincronizadoDias(int EmpId, int SucId, short CajaId, int dias) throws Exception {
        boolean tempEliminarComprobantesSincronizado = false;
        Date testDate = new Date();
        testDate.setDate(testDate.getDate() - dias);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String date = formatter.format(testDate);
        sSQL = "Delete From Comprobante Where CteSincronizado = '1' And EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajaId + " And CteFchHoraSync < " + date;
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

        try {
            sttment.execute();
            tempEliminarComprobantesSincronizado = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al eliminar comprobantes sincronizados.");
        } finally {
            NadDato.FinTrans();
            NadDato.schrinkBD();
        }
        NadDato.Conectar();
        NadDato.IniTrans();
        sSQL = "Delete from LogErrores where empid = 0";
        sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);
        try {
            sttment.execute();
            tempEliminarComprobantesSincronizado = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al eliminar log errores.");
        } finally {
            NadDato.FinTrans();
            NadDato.schrinkBD();
        }
        return tempEliminarComprobantesSincronizado;
    }

    public final ResultSet ConsComprobantesPorSincronizar(int EmpId, int SucId, short CajaId, int Cantidad) throws SQLException {
        ResultSet dt = null;
        String sTope = "";
        if (NadDato.getProveedorConexion() == NadMADato.TIPO_PROVEEDOR.tpSQLiteClient) {
            if (Cantidad != 0) {
                sTope = "LIMIT " + Cantidad;
            }
            sSQL = "Select * From Comprobante Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajaId + " And CteSincronizado = '0' " + sTope;
        } else {
            if (Cantidad != 0) {
                sTope = "Top(" + Cantidad + ") ";
            }
            sSQL = "Select " + sTope + "* From Comprobante Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajaId + " And CteSincronizado = '0'";
        }
        dt = NadDato.GetTable(sSQL);
        return dt;
    }

    public final boolean VerificarExisteCompPorSincAlerta(int EmpId, int SucId, short CajaId, int MinSinSincronizar, tangible.RefObject<String> WarningMsg) throws SQLException, Exception {

        boolean verificarExisteCompPorSincAlerta = false;
        try {
            ResultSet dt = null;
            Date da = new java.util.Date();
            da.setMinutes(da.getMinutes() - MinSinSincronizar);
            sSQL = "Select * From Comprobante Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajaId + " And CteSincronizado = '0' And CteFchHoraFirma < " + Fto.FechaHoraSQL(da);
            dt = NadDato.GetTable(sSQL);
            if (NadDato.getRowCount(dt) != 0) {
                if (!WarningMsg.argValue.trim().equals("")) {
                    WarningMsg.argValue += "\r\n";
                }
                dt = NadDato.GetTable(sSQL);
                int cantComp = NadDato.getRowCount(dt);
                if (cantComp > 0) {
                    verificarExisteCompPorSincAlerta = true;
                }
                WarningMsg.argValue += "***ATENCIÓN***: Hay " + cantComp + " Facturas Electrónicas pendientes de envío, verifique la conexion con el servidor.";
            }
        } catch (Exception ex) {
            Logger.getLogger(TAComprobante.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return verificarExisteCompPorSincAlerta;
    }

    public final int CantCompPorSincAlerta(int EmpId, int SucId, short CajaId, int MinSinSincronizar) throws SQLException {
        ResultSet dt = null;
        Date da = new java.util.Date();
        da.setMinutes(da.getMinutes() - MinSinSincronizar);
        sSQL = "Select * From Comprobante Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajaId + " And CteSincronizado = '0' And CteFchHoraFirma < " + Fto.FechaHoraSQL(da);
        dt = NadDato.GetTable(sSQL);
        return NadDato.getRowCount(dt);
    }

    public final int CantCompPorSinc() throws SQLException {
        ResultSet dt = null;
        sSQL = "Select * From Comprobante Where CteSincronizado = '0' ";
        dt = NadDato.GetTable(sSQL);
        return NadDato.getRowCount(dt);
    }

    public final boolean ActualizarComprobanteEnviado(int CteSyncId, int EmpId, int SucId, int CajaId, int CteId) throws Exception {
        boolean tempActualizarComprobanteEnviado = false;
        sSQL = "Update Comprobante Set CteSyncId = " + CteSyncId + ", CteSincronizado = '1' Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajaId + " And CteId = " + CteId;
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

        try {
            sttment.execute();
            tempActualizarComprobanteEnviado = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al actualizar comprobante enviado.");
        } finally {
            NadDato.FinTrans();
        }
        return tempActualizarComprobanteEnviado;
    }

    public final int ConsSucursalCajaCteId(int EmpId, int SucId, int CajaId) throws Exception {
        ResultSet dt = null;
        int CteUltId = 0;
        CteUltId = 0;

        sSQL = "Select * From Numerador Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajaId + " And NumTipo = 'CTENUM'";
        dt = NadDato.GetTable(sSQL);
        if (NadDato.getRowCount(dt) == 0) {
            CteUltId = 1;
            sSQL = "Insert Into Numerador (EmpId,SucId,CajaId,NumTipo,NumUltNum,NumFchHora) Values (" + EmpId + "," + SucId + "," + CajaId + ",'CTENUM',1," + Fto.FechaHoraSQL(new java.util.Date()) + ")";
        } else {
            dt = NadDato.GetTable(sSQL);
            //Actualizar numerador
            int cont = 0;
            while (dt.next() && cont == 0) {
                CteUltId = Integer.valueOf((Fto.CTDec(dt.getInt("NumUltNum")).intValueExact() + new BigDecimal(1).intValueExact()));
            }
            sSQL = "UpDate Numerador Set NumUltNum = " + CteUltId + ", NumFchHora = " + Fto.FechaHoraSQL(new java.util.Date()) + " Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajaId + " And NumTipo = 'CTENUM'";
            cont++;
        }
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

        try {
            sttment.execute();
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al actualizar numerador.");
        } finally {
            NadDato.FinTrans();
        }
        return CteUltId;
    }

    public final boolean GrabarComprobante(int EmpresaID, String CteRUT, int SucursalID, int CajaId, int CAEId, int CAEParteId, java.util.Date CteFecha, java.util.Date FechaHoraActual, int CertId, String CajaLicenseId, String XMLFirmado, XMLFACTURA XMLFactura, String VersionAPI, java.math.BigDecimal TopeUI, tangible.RefObject<XMLRESPUESTA> XMLRespuesta) throws Exception {
        boolean tempGrabarComprobante = false;
        int CteId = 0;
        tempGrabarComprobante = false;

        CteId = ConsSucursalCajaCteId(EmpresaID, SucursalID, CajaId);

        //Si Supera Tope UI configurada va en sobre
        //***ESTA VALIDACION DE SI CORRESPONDE SOBRE O NO HACERLA ANTES DE FIRMAR, JUNTO CON LA DE RECEPTOR OBLIGATORIO CUANDO ES FREESHOP Y SUPERA TOPE DE PARAM PARA FS***
        if (ComprobanteDebeEnsobrarse(XMLFactura, TopeUI) == true) {
            XMLRespuesta.argValue.setCORRESPONDESOBRE((short) 1);
        } else {
            XMLRespuesta.argValue.setCORRESPONDESOBRE((short) 0);
        }

        Map<String, Object> dr = new HashMap<String, Object>();

        dr.put(Comprobante.EmpId, EmpresaID);
        dr.put(Comprobante.SucId, SucursalID);
        dr.put(Comprobante.CajaId, CajaId);
        dr.put(Comprobante.CteId, CteId);
        dr.put(Comprobante.CAEId, CAEId);
        dr.put(Comprobante.CAEParteId, CAEParteId);
        dr.put(Comprobante.CteCAENro, XMLRespuesta.argValue.getCAENRO());
        dr.put(Comprobante.CteCAESerie, XMLRespuesta.argValue.getCAESERIE());
        dr.put(Comprobante.CteCAENroAutorizacion, XMLRespuesta.argValue.getCAENA());
        dr.put(Comprobante.CteCAENroDesde, XMLRespuesta.argValue.getCAENROINICIAL());
        dr.put(Comprobante.CteCAENroHasta, XMLRespuesta.argValue.getCAENROFINAL());
        dr.put(Comprobante.CteCAETipoCFE, XMLFactura.getDGI().getIDDocTipoCFE());
        dr.put(Comprobante.CteCAEVencimiento, Fto.FechaHoraSQL(XMLRespuesta.argValue.getCAEVENCIMIENTO()).replace("'", ""));
        dr.put(Comprobante.CteFchHoraFirma, Fto.FechaHoraSQL(FechaHoraActual).replace("'", ""));
        dr.put(Comprobante.CertId, CertId);
        dr.put(Comprobante.CteXMLEntrada, XMLFactura.ToXML());
        dr.put(Comprobante.CteXMLRespuesta, XMLRespuesta.argValue.ToXML());
        dr.put(Comprobante.CteXMLFirmado, XMLFirmado); //SI XMLFirmado es vacio, es una factura de contingencia.
        dr.put(Comprobante.CteAdendaTexto, Fto.CNullStr(XMLFactura.getDGI().getADETextoADE()));
        dr.put(Comprobante.CteFchEmision, Fto.FechaSQL(XMLFactura.getDGI().getIDDocFchEmis()).replace("'", ""));
        dr.put(Comprobante.CteDocOriginalTipo, XMLFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALTIPO());
        dr.put(Comprobante.CteDocOriginalSerie, XMLFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALSERIE());
        dr.put(Comprobante.CteDocOriginalNro, XMLFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALNRO());
        dr.put(Comprobante.CteCajaUniqueId, CajaLicenseId);
        dr.put(Comprobante.CteVersionAPI, VersionAPI);
        dr.put(Comprobante.CteSincronizado, 0);
        dr.put(Comprobante.CteSyncId, 0);
        dr.put(Comprobante.CteFchHoraSync, Fto.FechaHoraSQL(new java.util.Date()).replace("'", ""));

//        sSQL = assembly.AssemblyComprobanteInsert(dr);
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = assembly.AssemblyComprobanteInsert(dr, NadDato.GetConnectionLite());

        try {
            sttment.execute();
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al grabar comprobante.");
        } finally {
            NadDato.FinTrans();
        }
        return tempGrabarComprobante;
    }

    public final Map<String, Object> CrearFacturasGeneradas() {
        Map<String, Object> dt = new HashMap<String, Object>();
        dt.put(Comprobante.EmpId, Integer.class);
        dt.put(Comprobante.SucId, Integer.class);
        dt.put(Comprobante.CajaId, Short.class);
        dt.put(Comprobante.CteId, Integer.class);
        dt.put(Comprobante.CAEId, Integer.class);
        dt.put(Comprobante.CAEParteId, Integer.class);
        dt.put(Comprobante.CteCAENro, Long.class);
        dt.put(Comprobante.CteCAESerie, String.class);
        dt.put(Comprobante.CteCAENroAutorizacion, Long.class);
        dt.put(Comprobante.CteCAENroDesde, Long.class);
        dt.put(Comprobante.CteCAENroHasta, Long.class);
        dt.put(Comprobante.CteCAETipoCFE, Integer.class);
        dt.put(Comprobante.CteCAEVencimiento, String.class);
        dt.put(Comprobante.CteFchHoraFirma, String.class);
        dt.put(Comprobante.CertId, Integer.class);
        dt.put(Comprobante.CteXMLEntrada, String.class);
        dt.put(Comprobante.CteXMLRespuesta, String.class);
        dt.put(Comprobante.CteXMLFirmado, String.class);
        dt.put(Comprobante.CteAdendaTexto, String.class);
        dt.put(Comprobante.CteFchEmision, String.class);
        dt.put(Comprobante.CteDocOriginalTipo, String.class);
        dt.put(Comprobante.CteDocOriginalSerie, String.class);
        dt.put(Comprobante.CteDocOriginalNro, String.class);
        dt.put(Comprobante.CteCajaUniqueId, String.class);
        dt.put(Comprobante.CteVersionAPI, String.class);
        dt.put(Comprobante.CteSincronizado, Integer.class);
        dt.put(Comprobante.CteSyncId, Integer.class);
        dt.put(Comprobante.CteFchHoraSync, String.class);
        return dt;
    }

    public final short SuperoTopeUI(java.math.BigDecimal MontoEnPesos, java.math.BigDecimal CotizacionUI, java.math.BigDecimal TopeUI) {
        short tempSuperoTopeUI = 0;
        java.math.BigDecimal TotalUI = new java.math.BigDecimal(0);
        if (CotizacionUI.compareTo(BigDecimal.ZERO) != 0) {
            TotalUI = MontoEnPesos.divide(CotizacionUI, 2, RoundingMode.HALF_UP);
        }
        if (TotalUI.compareTo(TopeUI) >= 0) {
            tempSuperoTopeUI = 1;
        } else {
            tempSuperoTopeUI = 0;
        }
        return tempSuperoTopeUI;
    }

    public final boolean ComprobanteDebeEnsobrarse(XMLFACTURA XMLFactura, java.math.BigDecimal TopeUI) throws ParseException {
        boolean EnsobrarComprobante = false;
        java.math.BigDecimal TotalEnPesosSinIva;
        TotalEnPesosSinIva = RetornarMontoEnPesos(new BigDecimal(XMLFactura.getDGI().getTOTMntExpoyAsim() + XMLFactura.getDGI().getTOTMntNoGrv() + XMLFactura.getDGI().getTOTMntNetoIvaTasaMin() + XMLFactura.getDGI().getTOTMntNetoIVATasaBasica() + XMLFactura.getDGI().getTOTMntNetoIVAOtra()), XMLFactura.getDGI().getTOTTpoMoneda(), new BigDecimal(XMLFactura.getDGI().getTOTTpoCambio()));

        if ((XMLFactura.getDGI().getIDDocTipoCFE() >= 101 && XMLFactura.getDGI().getIDDocTipoCFE() <= 103) || (XMLFactura.getDGI().getIDDocTipoCFE() >= 201 && XMLFactura.getDGI().getIDDocTipoCFE() <= 203)) {
            if (XMLFactura.getDGI().getTOTMntTotRetenido() != 0) {
                return true;
            }

            if (SuperoTopeUI(TotalEnPesosSinIva, Fto.CTDec(XMLFactura.getDATOSADICIONALES().getCOTIZACIONUI()), TopeUI) == 1) {
                return true;
            }
        } else {
            return true;
        }

        return EnsobrarComprobante;
    }

    private java.math.BigDecimal RetornarMontoEnPesos(java.math.BigDecimal MontoOriginal, String TipoMoneda, java.math.BigDecimal FactorConversion) {
        java.math.BigDecimal tempRetornarMontoEnPesos = new java.math.BigDecimal(0);
        tempRetornarMontoEnPesos = new BigDecimal(0);
        if (TipoMoneda.equals("UYU")) {
            tempRetornarMontoEnPesos = MontoOriginal;
        } else {
            tempRetornarMontoEnPesos = MontoOriginal.multiply(FactorConversion);
        }
        return tempRetornarMontoEnPesos;
    }

    public TAComprobante(NadMADato Dato) {
        NadDato = Dato;
        Fto.setProveedorConexion(Dato.getProveedorConexion().getValue());
    }

    private Map<String, Object> resultSetToArrayList(ResultSet rs) throws SQLException {
        Map<String, Object> myMap = new HashMap<String, Object>();

        ResultSetMetaData meta = rs.getMetaData();
        int row = 0;
        while (rs.next()) {
            if (row == 0) {
                for (int i = 1; i <= meta.getColumnCount(); i++) {
                    String key = meta.getColumnName(i);
                    Object value = rs.getObject(key);
                    myMap.put(key, value);
                }
                row++;
            }
        }
        return myMap;
    }
    
    public boolean CargarDatosComprobanteComparar(int EmpId, int SucId, int CajId, int TipoDeComprobante, long DocOrigNro, String DocOrigSerie, String DocOrigTipo, tangible.RefObject<String> XMLEntrada, tangible.RefObject<String> XMLRespuesta) throws SQLException {
        ResultSet dt = null;
        sSQL = "Select * From Comprobante Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajId + " And CteDocOriginalNro = " + DocOrigNro + " And CteDocOriginalSerie = '" + DocOrigSerie + "' And CteDocOriginalTipo = '" + DocOrigTipo + "' And CteCAETipoCFE = " + TipoDeComprobante;
        dt = NadDato.GetTable(sSQL);
        if (NadDato.getRowCount(dt) > 0) {
            dt = NadDato.GetTable(sSQL);
            dt.next();
            XMLRespuesta.argValue = (dt.getString("CteXMLRespuesta"));
            XMLEntrada.argValue = (dt.getString("CteXMLEntrada"));
            return true;
        }
        return false;
    }

    public final boolean CargarDatosCteDoc(int EmpId, int SucId, int CajId, int TipoDeComprobante, long DocOrigNro, String DocOrigSerie, String DocOrigTipo) throws SQLException {
        ResultSet dt = null;
        sSQL = "Select * From Comprobante Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajId + " And CteDocOriginalNro = " + DocOrigNro + " And CteDocOriginalSerie = '" + DocOrigSerie + "' And CteDocOriginalTipo = '" + DocOrigTipo + "' And CteCAETipoCFE = " + TipoDeComprobante;
        dt = NadDato.GetTable(sSQL);
        if (NadDato.getRowCount(dt) > 0) {
            return true;
        }
        return false;
    }
    
    public final boolean ExisteComprobantes(int EmpresaId, int SucursalId, int CajaId, long DocOrigNro, String DocOrigSerie, String DocOrigTipo, long CAENroAutorizacionConsulta, String CAESerieConsulta, long CAENroConsulta) throws SQLException {
        boolean tempExisteComprobantes = false;
        sSQL = "Select * From Comprobante Where EmpId = " + EmpresaId + " And SucId = " + SucursalId + " And CajaId = " + CajaId + " And CteDocOriginalNro = " + DocOrigNro + " And CteDocOriginalSerie = '" + DocOrigSerie + "' And CteDocOriginalTipo = '" + DocOrigTipo + "' And CteCAENroAutorizacion = '" + CAENroAutorizacionConsulta + "' And CteCAESerie= '" + CAESerieConsulta + "' And CteCAENro = '" + CAENroConsulta + "' ";
         ResultSet dt = null;
        dt = NadDato.GetTable(sSQL);
        String respuesta = null;
         if (NadDato.getRowCount(dt) > 0) {
             dt = NadDato.GetTable(sSQL);
             dt.next();
            respuesta = (dt.getString("CteXMLRespuesta"));
        }
        if (!respuesta.trim().equals("") && respuesta.length() > 0) {
            tempExisteComprobantes = true;
        }
        return tempExisteComprobantes;
    }
}