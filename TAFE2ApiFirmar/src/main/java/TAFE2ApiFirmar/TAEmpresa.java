package TAFE2ApiFirmar;

import TAFACE2ApiEntidad.TAException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TAEmpresa {

    public static class Empresa {

        /**
         * @return the EmpId
         */
        public static String getEmpId() {
            return EmpId;
        }

        /**
         * @param aEmpId the EmpId to set
         */
        public static void setEmpId(String aEmpId) {
            EmpId = aEmpId;
        }

        /**
         * @return the EmpRUC
         */
        public static String getEmpRUC() {
            return EmpRUC;
        }

        /**
         * @param aEmpRUC the EmpRUC to set
         */
        public static void setEmpRUC(String aEmpRUC) {
            EmpRUC = aEmpRUC;
        }

        /**
         * @return the EmpNom
         */
        public static String getEmpNom() {
            return EmpNom;
        }

        /**
         * @param aEmpNom the EmpNom to set
         */
        public static void setEmpNom(String aEmpNom) {
            EmpNom = aEmpNom;
        }

        /**
         * @return the EmpEmiteTickets
         */
        public static String getEmpEmiteTickets() {
            return EmpEmiteTickets;
        }

        /**
         * @param aEmpEmiteTickets the EmpEmiteTickets to set
         */
        public static void setEmpEmiteTickets(String aEmpEmiteTickets) {
            EmpEmiteTickets = aEmpEmiteTickets;
        }

        /**
         * @return the EmpEmiteFacturas
         */
        public static String getEmpEmiteFacturas() {
            return EmpEmiteFacturas;
        }

        /**
         * @param aEmpEmiteFacturas the EmpEmiteFacturas to set
         */
        public static void setEmpEmiteFacturas(String aEmpEmiteFacturas) {
            EmpEmiteFacturas = aEmpEmiteFacturas;
        }

        /**
         * @return the EmpEmiteRemitos
         */
        public static String getEmpEmiteRemitos() {
            return EmpEmiteRemitos;
        }

        /**
         * @param aEmpEmiteRemitos the EmpEmiteRemitos to set
         */
        public static void setEmpEmiteRemitos(String aEmpEmiteRemitos) {
            EmpEmiteRemitos = aEmpEmiteRemitos;
        }

        /**
         * @return the EmpEmiteResguardos
         */
        public static String getEmpEmiteResguardos() {
            return EmpEmiteResguardos;
        }

        /**
         * @param aEmpEmiteResguardos the EmpEmiteResguardos to set
         */
        public static void setEmpEmiteResguardos(String aEmpEmiteResguardos) {
            EmpEmiteResguardos = aEmpEmiteResguardos;
        }

        /**
         * @return the EmpEmiteFacturasExportacion
         */
        public static String getEmpEmiteFacturasExportacion() {
            return EmpEmiteFacturasExportacion;
        }

        /**
         * @param aEmpEmiteFacturasExportacion the EmpEmiteFacturasExportacion
         * to set
         */
        public static void setEmpEmiteFacturasExportacion(String aEmpEmiteFacturasExportacion) {
            EmpEmiteFacturasExportacion = aEmpEmiteFacturasExportacion;
        }

        /**
         * @return the EmpEmiteRemitosExportacion
         */
        public static String getEmpEmiteRemitosExportacion() {
            return EmpEmiteRemitosExportacion;
        }

        /**
         * @param aEmpEmiteRemitosExportacion the EmpEmiteRemitosExportacion to
         * set
         */
        public static void setEmpEmiteRemitosExportacion(String aEmpEmiteRemitosExportacion) {
            EmpEmiteRemitosExportacion = aEmpEmiteRemitosExportacion;
        }

        /**
         * @return the EmpEmiteCuentaAjena
         */
        public static String getEmpEmiteCuentaAjena() {
            return EmpEmiteCuentaAjena;
        }

        /**
         * @param aEmpEmiteCuentaAjena the EmpEmiteCuentaAjena to set
         */
        public static void setEmpEmiteCuentaAjena(String aEmpEmiteCuentaAjena) {
            EmpEmiteCuentaAjena = aEmpEmiteCuentaAjena;
        }

        /**
         * @return the EmpResolucionIVA
         */
        public static String getEmpResolucionIVA() {
            return EmpResolucionIVA;
        }

        /**
         * @param aEmpResolucionIVA the EmpResolucionIVA to set
         */
        public static void setEmpResolucionIVA(String aEmpResolucionIVA) {
            EmpResolucionIVA = aEmpResolucionIVA;
        }

        /**
         * @return the EmpLogoImpresion
         */
        public static String getEmpLogoImpresion() {
            return EmpLogoImpresion;
        }

        /**
         * @param aEmpLogoImpresion the EmpLogoImpresion to set
         */
        public static void setEmpLogoImpresion(String aEmpLogoImpresion) {
            EmpLogoImpresion = aEmpLogoImpresion;
        }

        /**
         * @return the EmpLogoMiniatura
         */
        public static String getEmpLogoMiniatura() {
            return EmpLogoMiniatura;
        }

        /**
         * @param aEmpLogoMiniatura the EmpLogoMiniatura to set
         */
        public static void setEmpLogoMiniatura(String aEmpLogoMiniatura) {
            EmpLogoMiniatura = aEmpLogoMiniatura;
        }

        /**
         * @return the EmpUrlConsultaCFE
         */
        public static String getEmpUrlConsultaCFE() {
            return EmpUrlConsultaCFE;
        }

        /**
         * @param aEmpUrlConsultaCFE the EmpUrlConsultaCFE to set
         */
        public static void setEmpUrlConsultaCFE(String aEmpUrlConsultaCFE) {
            EmpUrlConsultaCFE = aEmpUrlConsultaCFE;
        }

        /**
         * @return the EmpFchHoraSync
         */
        public static String getEmpFchHoraSync() {
            return EmpFchHoraSync;
        }

        /**
         * @param aEmpFchHoraSync the EmpFchHoraSync to set
         */
        public static void setEmpFchHoraSync(String aEmpFchHoraSync) {
            EmpFchHoraSync = aEmpFchHoraSync;
        }

        /**
         * @return the EmpRowGUID
         */
        public static String getEmpRowGUID() {
            return EmpRowGUID;
        }

        /**
         * @param aEmpRowGUID the EmpRowGUID to set
         */
        public static void setEmpRowGUID(String aEmpRowGUID) {
            EmpRowGUID = aEmpRowGUID;
        }
        private static String EmpId = "EmpId";
        private static String EmpRUC = "EmpRUC";
        private static String EmpNom = "EmpNom";
        private static String EmpEmiteTickets = "EmpEmiteTickets";
        private static String EmpEmiteFacturas = "EmpEmiteFacturas";
        private static String EmpEmiteRemitos = "EmpEmiteRemitos";
        private static String EmpEmiteResguardos = "EmpEmiteResguardos";
        private static String EmpEmiteFacturasExportacion = "EmpEmiteFacturasExportacion";
        private static String EmpEmiteRemitosExportacion = "EmpEmiteRemitosExportacion";
        private static String EmpEmiteCuentaAjena = "EmpEmiteCuentaAjena";
        private static String EmpResolucionIVA = "EmpResolucionIVA";
        private static String EmpLogoImpresion = "EmpLogoImpresion";
        private static String EmpLogoMiniatura = "EmpLogoMiniatura";
        private static String EmpUrlConsultaCFE = "EmpUrlConsultaCFE";
        private static String EmpFchHoraSync = "EmpFchHoraSync";
        private static String EmpRowGUID = "EmpRowGUID";
    }

    public static class Sucursal {

        /**
         * @return the EmpId
         */
        public static String getEmpId() {
            return EmpId;
        }

        /**
         * @param aEmpId the EmpId to set
         */
        public static void setEmpId(String aEmpId) {
            EmpId = aEmpId;
        }

        /**
         * @return the SucId
         */
        public static String getSucId() {
            return SucId;
        }

        /**
         * @param aSucId the SucId to set
         */
        public static void setSucId(String aSucId) {
            SucId = aSucId;
        }

        /**
         * @return the SucNom
         */
        public static String getSucNom() {
            return SucNom;
        }

        /**
         * @param aSucNom the SucNom to set
         */
        public static void setSucNom(String aSucNom) {
            SucNom = aSucNom;
        }

        /**
         * @return the SucCodEnDGI
         */
        public static String getSucCodEnDGI() {
            return SucCodEnDGI;
        }

        /**
         * @param aSucCodEnDGI the SucCodEnDGI to set
         */
        public static void setSucCodEnDGI(String aSucCodEnDGI) {
            SucCodEnDGI = aSucCodEnDGI;
        }

        /**
         * @return the SucFchHoraUltSync
         */
        public static String getSucFchHoraUltSync() {
            return SucFchHoraUltSync;
        }

        /**
         * @param aSucFchHoraUltSync the SucFchHoraUltSync to set
         */
        public static void setSucFchHoraUltSync(String aSucFchHoraUltSync) {
            SucFchHoraUltSync = aSucFchHoraUltSync;
        }

        /**
         * @return the SucRowGUID
         */
        public static String getSucRowGUID() {
            return SucRowGUID;
        }

        /**
         * @param aSucRowGUID the SucRowGUID to set
         */
        public static void setSucRowGUID(String aSucRowGUID) {
            SucRowGUID = aSucRowGUID;
        }
        private Object x;
        private static String EmpId = "EmpId";
        private static String SucId = "SucId";
        private static String SucNom = "SucNom";
        private static String SucCodEnDGI = "SucCodEnDGI";
        private static String SucFchHoraUltSync = "SucFchHoraUltSync";
        private static String SucRowGUID = "SucRowGUID";
    }

    public static class SucursalCaja {

        /**
         * @return the EmpId
         */
        public static String getEmpId() {
            return EmpId;
        }

        /**
         * @param aEmpId the EmpId to set
         */
        public static void setEmpId(String aEmpId) {
            EmpId = aEmpId;
        }

        /**
         * @return the SucId
         */
        public static String getSucId() {
            return SucId;
        }

        /**
         * @param aSucId the SucId to set
         */
        public static void setSucId(String aSucId) {
            SucId = aSucId;
        }

        /**
         * @return the CajaId
         */
        public static String getCajaId() {
            return CajaId;
        }

        /**
         * @param aCajaId the CajaId to set
         */
        public static void setCajaId(String aCajaId) {
            CajaId = aCajaId;
        }

        /**
         * @return the CajaUniqueId
         */
        public static String getCajaUniqueId() {
            return CajaUniqueId;
        }

        /**
         * @param aCajaUniqueId the CajaUniqueId to set
         */
        public static void setCajaUniqueId(String aCajaUniqueId) {
            CajaUniqueId = aCajaUniqueId;
        }
        private static String EmpId = "EmpId";
        private static String SucId = "SucId";
        private static String CajaId = "CajaId";
        private static String CajaUniqueId = "CajaUniqueId";
    }
    private String sSQL;
    private NadMADato NadDato;
    private NadFormato Fto = new NadFormato();
    private AssemblyQuery assembly = new AssemblyQuery();

    public final String DameResolucionIVAEmp(String EmpId) throws SQLException, Exception {
        String tempDameResolucionIVAEmp = null;
        try {
            ResultSet dt = null;
            tempDameResolucionIVAEmp = "";
            sSQL = "Select * From Empresa Where EmpId = " + EmpId;
            dt = NadDato.GetTable(sSQL);
            if (dt != null) {
                while (dt.next()) {
                    tempDameResolucionIVAEmp = dt.getString("EmpResolucionIVA");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TAEmpresa.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tempDameResolucionIVAEmp;
    }

    public final ResultSet ConsSucursal(int EmpId, int SucId) throws SQLException {
        ResultSet dt = null;
        sSQL = "Select * From Sucursal Where EmpId = " + EmpId + " And SucId = " + SucId;
        dt = NadDato.GetTable(sSQL);
        //dt.TableName = Tb.Sucursal.Nombre;
        return dt;
    }

    public final int ConsSucursalCodEnDGI(int EmpId, int SucId) throws SQLException, Exception {
        int tempConsSucursalCodEnDGI = 0;
        try {
            ResultSet dt = null;
            tempConsSucursalCodEnDGI = 0;
            sSQL = "Select * From Sucursal Where EmpId = " + EmpId + " And SucId = " + SucId;
            dt = NadDato.GetTable(sSQL);
            if (dt != null) {
                while (dt.next()) {
                    tempConsSucursalCodEnDGI = dt.getInt("SucCodEnDGI");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TAEmpresa.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tempConsSucursalCodEnDGI;
    }

    public final ResultSet ConsEmpresa(int EmpId) throws SQLException {
        ResultSet dt = null;
        sSQL = "Select * From Empresa Where EmpId = " + EmpId;
        dt = NadDato.GetTable(sSQL);
        //dt.TableName = Tb.Sucursal.Nombre;
        return dt;
    }

    public final ResultSet ConsEmpresaRut(String EmpRut) throws SQLException {
        ResultSet dt = null;
        sSQL = "Select * From Empresa Where EmpRUC = '" + EmpRut + "'";
        dt = NadDato.GetTable(sSQL);
        //dt.TableName = Tb.Sucursal.Nombre;
        return dt;
    }

    public final boolean EliminarEmpresa(int EmpId) throws Exception {
        boolean tempEliminarEmpresa = false;
        sSQL = "Delete From Empresa Where EmpId = " + EmpId;
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

        try {
            sttment.execute();
            tempEliminarEmpresa = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al eliminar empresa.");
        } finally {
            NadDato.FinTrans();
        }
        return tempEliminarEmpresa;
    }

    public final boolean EliminarSucursal(int EmpId, int SucId) throws Exception {
        boolean tempEliminarSucursal = false;
        sSQL = "Delete From Sucursal Where EmpId = " + EmpId + " And SucId = " + SucId;
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

        try {
            sttment.execute();
            tempEliminarSucursal = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al eliminar sucursal.");
        } finally {
            NadDato.FinTrans();
        }
        return tempEliminarSucursal;
    }

    public final boolean EliminarCajasQueNoEstanEnLista(int EmpId, int SucId, String ListaCajasId) throws Exception {
        boolean tempEliminarCajasQueNoEstanEnLista = false;
        sSQL = "Select * From SucursalCaja Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId Not In (" + ListaCajasId + ")";

        ResultSet set = NadDato.GetTable(sSQL);
        if (NadDato.getRowCount(set) > 0) {
            sSQL = "Delete From SucursalCaja Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId Not In (" + ListaCajasId + ")";
            NadDato.Conectar();
            NadDato.IniTrans();
            PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

            try {
                sttment.execute();
                tempEliminarCajasQueNoEstanEnLista = true;
            } catch (SQLException ex) {
                throw new TAFACE2ApiEntidad.TAException("Error al eliminar cajas que no estan en lista.");
            } finally {
                NadDato.FinTrans();
            }
        }
        return tempEliminarCajasQueNoEstanEnLista;
    }

    public final boolean GrabarSucursal(Map<String, Object> drSucursal) throws SQLException, Exception {
        boolean tempGrabarSucursal = false;
        String EmpId = null;
        String SucId = null;
        EmpId = drSucursal.get(Sucursal.EmpId).toString();
        SucId = drSucursal.get(Sucursal.SucId).toString();

        //se asume que solamente exista registrada una vez la empresa
        sSQL = "Select * from Sucursal Where EmpId = " + EmpId + " And SucId = " + SucId;
        ResultSet set = NadDato.GetTable(sSQL);
        if (NadDato.getRowCount(set) == 0) {
            sSQL = assembly.AssemblySucursalInsert(drSucursal);//NadDato.GetInsertSQL(drSucursal); // ensamblar la consulta de insertar
        } else {
            sSQL = assembly.AssemblySucursalUpdate(drSucursal) + " Where EmpId = " + EmpId + " And SucId = " + SucId;//NadDato.GetUpdateSQL(drSucursal) ;
        }
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

        try {
            sttment.execute();
            tempGrabarSucursal = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al grabar sucursal.");
        } finally {
            NadDato.FinTrans();
        }
        return tempGrabarSucursal;
    }

    public final boolean GrabarCaja(Map<String, Object> drCaja) throws SQLException, Exception {
        boolean tempGrabarCaja = false;
        String EmpId = null;
        String SucId = null;
        String CajaId = null;
        EmpId = drCaja.get(Sucursal.EmpId).toString();
        SucId = drCaja.get(Sucursal.SucId).toString();
        CajaId = drCaja.get(SucursalCaja.CajaId).toString();
        sSQL = "Select * from SucursalCaja Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajaId;
        ResultSet set = NadDato.GetTable(sSQL);
        ResultSet aux = set;
        if (NadDato.getRowCount(aux) == 0) {
            sSQL = assembly.AssemblyCajaInsert(drCaja);
        } else {
            sSQL = assembly.AssemblyCajaUpdate(drCaja) + " Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajaId;
        }
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

        try {
            sttment.execute();
            tempGrabarCaja = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al grabar caja.");
        } finally {
            NadDato.FinTrans();
        }
        return tempGrabarCaja;
    }

    public final boolean GrabarSucursal(int EmpId, int SucId, String SucNom, String SucDir, String SubTel, String SucEncargadoNom, boolean SucPrincipal, String SucCiudad, short SucCodEnDgi, String SucRowGUID) throws Exception {
        boolean tempGrabarSucursal = false;
        tempGrabarSucursal = false;
        sSQL = "Insert Into Sucursal Where (EmpId,SucId,SucNom,SucDir,SubTel,SucEncargadoNom,SucPrincipal,SucCiudad) Set (" + EmpId + ", " + SucId + ",'" + SucNom + "','" + SucDir + "','" + SubTel + "'," + Fto.LogSQL(SucPrincipal) + ",'" + SucCiudad + "'," + SucCodEnDgi + ",'" + SucRowGUID + "')";
        NadDato.Ejecutar(sSQL);
        return tempGrabarSucursal;
    }

    public final boolean GrabarEmpresa(Map<String, Object> drEmpresa) throws SQLException, TAException, Exception {
        boolean tempGrabarEmpresa = false;
        try {
            String EmpId = null;
            String EmpLogoImpresion = null;
            String EmpLogoMiniatura = null;
            EmpId = drEmpresa.get(Sucursal.EmpId).toString();
            EmpLogoImpresion = drEmpresa.get(Empresa.EmpLogoImpresion).toString();
            EmpLogoMiniatura = drEmpresa.get(Empresa.EmpLogoMiniatura).toString();

            sSQL = "Select * from Empresa where EmpId = " + EmpId;

            ResultSet set = NadDato.GetTable(sSQL);
            if (NadDato.getRowCount(set) == 0) {
                sSQL = assembly.AssemblyEmpresaInsert(drEmpresa);
            } else {
                sSQL = assembly.AssemblyEmpresaUpdate(drEmpresa) + " where EmpId = " + EmpId;
            }
            if (NadDato.getProveedorConexion() == NadMADato.TIPO_PROVEEDOR.tpSQLiteClient) {
                NadDato.Conectar();
                NadDato.IniTrans();
                PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);
                sttment.setBytes(1, null);
                sttment.setBytes(2, null);
                try {
                    sttment.execute();
                    tempGrabarEmpresa = true;
                } catch (SQLException ex) {
                    throw new TAFACE2ApiEntidad.TAException("Error al grabar empresa.");
                } finally {
                    NadDato.FinTrans();
                }

            }
        } catch (RuntimeException ex) {
            throw ex;
        } catch (Exception ex) {
            Logger.getLogger(TAEmpresa.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return tempGrabarEmpresa;
    }

    public final boolean EmpresaDameTipoEmisor(String EmpRUC, tangible.RefObject<Integer> EmpId, tangible.RefObject<Boolean> EmpEmiteTickets, tangible.RefObject<Boolean> EmpEmiteFacturas, tangible.RefObject<Boolean> EmpEmiteRemitos, tangible.RefObject<Boolean> EmpEmiteResguardos, tangible.RefObject<Boolean> EmpEmiteFacturasExp, tangible.RefObject<Boolean> EmpEmiteRemitosExp, tangible.RefObject<Boolean> EmpEmiteCuentaAjena) throws SQLException, Exception {
        boolean tempEmpresaDameTipoEmisor = false;
        try {

            ResultSet dt = null;
            tempEmpresaDameTipoEmisor = false;
            sSQL = "Select * From Empresa Where EmpRUC = '" + EmpRUC + "'";
            dt = NadDato.GetTable(sSQL);
            if (NadDato.getRowCount(dt) > 0) {
                dt = NadDato.GetTable(sSQL);
                while (dt.next()) {
                    EmpId.argValue = dt.getInt(Empresa.EmpId);
                    EmpEmiteTickets.argValue = Integer.parseInt(dt.getString(Empresa.EmpEmiteTickets)) == 1;
                    EmpEmiteFacturas.argValue = Integer.parseInt(dt.getString(Empresa.EmpEmiteFacturas)) == 1;
                    EmpEmiteRemitos.argValue = Integer.parseInt(dt.getString(Empresa.EmpEmiteRemitos)) == 1;
                    EmpEmiteResguardos.argValue = Integer.parseInt(dt.getString(Empresa.EmpEmiteResguardos)) == 1;
                    EmpEmiteFacturasExp.argValue = Integer.parseInt(dt.getString(Empresa.EmpEmiteFacturasExportacion)) == 1;
                    EmpEmiteRemitosExp.argValue = Integer.parseInt(dt.getString(Empresa.EmpEmiteRemitosExportacion)) == 1;
                    EmpEmiteCuentaAjena.argValue = Integer.parseInt(dt.getString(Empresa.EmpEmiteCuentaAjena)) == 1;
                    tempEmpresaDameTipoEmisor = true;

                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TAEmpresa.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return tempEmpresaDameTipoEmisor;
    }

    public final boolean ObtenerIdEmpresa(String EmpRUC, tangible.RefObject<Integer> EmpId) throws SQLException, Exception {

        boolean tempObtenerIdEmpresa = false;
        try {
            ResultSet dt = null;
            sSQL = "Select * From Empresa Where EmpRUC = '" + EmpRUC + "'";
            dt = NadDato.GetTable(sSQL);

            if (NadDato.getRowCount(dt) > 0) {
                dt = NadDato.GetTable(sSQL);
                while (dt.next()) {
                    EmpId.argValue = dt.getInt(Empresa.EmpId);
                }
                tempObtenerIdEmpresa = true;
            }
        } catch (Exception ex) {
            Logger.getLogger(TAEmpresa.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tempObtenerIdEmpresa;
    }

    public final boolean ObtenerLogoEmpresa(String EmpRUC, tangible.RefObject<byte[]> EmpLogo, boolean UtilizarLogoMiniatura) throws SQLException, Exception {

        boolean tempObtenerLogoEmpresa = false;
        try {
            ResultSet dt = null;
            sSQL = "Select * From Empresa Where EmpRUC = '" + EmpRUC + "'";
            dt = NadDato.GetTable(sSQL);
            if (NadDato.getRowCount(dt) > 0) {
                dt = NadDato.GetTable(sSQL);
                while (dt.next()) {
                    if (dt.getBlob(Empresa.EmpLogoImpresion).length() > 0) {
                        if (UtilizarLogoMiniatura) {
                            int blobLength = (int) dt.getBlob(Empresa.EmpLogoMiniatura).length();
                            EmpLogo.argValue = dt.getBlob(Empresa.EmpLogoMiniatura).getBytes(0, blobLength);
                        } else {
                            int blobLength = (int) dt.getBlob(Empresa.EmpLogoImpresion).length();
                            EmpLogo.argValue = dt.getBlob(Empresa.EmpLogoImpresion).getBytes(0, blobLength);
                        }
                    }
                    tempObtenerLogoEmpresa = true;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TAEmpresa.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tempObtenerLogoEmpresa;
    }

    public final String ObtenerUrlConsultaCFE(int EmpId) throws SQLException, Exception {

        String tempObtenerUrlConsultaCFE = "";
        try {
            ResultSet dt = null;
            sSQL = "Select * From Empresa Where EmpId = " + EmpId;
            dt = NadDato.GetTable(sSQL);
            if (NadDato.getRowCount(dt) > 0) {
                dt = NadDato.GetTable(sSQL);
                while (dt.next()) {
                    tempObtenerUrlConsultaCFE = dt.getString(Empresa.EmpUrlConsultaCFE);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TAEmpresa.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tempObtenerUrlConsultaCFE;
    }

    public final int DameEmpresaGateway() throws SQLException, Exception {

        int tempDameEmpresaGateway = 0;
        try {
            ResultSet dt = null;
            sSQL = "Select EmpId From Empresa Where EmpId <> 0";
            dt = NadDato.GetTable(sSQL);
            if (NadDato.getRowCount(dt) > 0) {
                dt = NadDato.GetTable(sSQL);
                while (dt.next()) {
                    tempDameEmpresaGateway = dt.getInt(Empresa.EmpId);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TAEmpresa.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return tempDameEmpresaGateway;
    }

    public final boolean ExisteEmpresa(String EmpRUT, tangible.RefObject<Integer> EmpId) throws SQLException, Exception {

        boolean existeEmpresa = false;
        try {
            ResultSet dt = null;
            sSQL = "Select * from Empresa Where EmpRUC = '" + EmpRUT + "'";
            dt = NadDato.GetTable(sSQL);
            if (NadDato.getRowCount(dt) > 0) {
                dt = NadDato.GetTable(sSQL);
                while (dt.next()) {
                    EmpId.argValue = dt.getInt(Empresa.EmpId);
                    existeEmpresa = true;
                }
            } else {
                existeEmpresa = false;
            }
        } catch (Exception ex) {
            Logger.getLogger(TAEmpresa.class.getName()).log(Level.SEVERE, null, ex);
        }
        return existeEmpresa;
    }

    public final boolean ExisteCaja(int EmpId, int SucId, int CajId) throws SQLException, Exception {

        boolean existeCaja = false;
        try {
            ResultSet dt = null;
            sSQL = "Select * from SucursalCaja Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajId;
            dt = NadDato.GetTable(sSQL);
            if (NadDato.getRowCount(dt) > 0) {
                existeCaja = true;
            } else {
                existeCaja = false;
            }
        } catch (Exception ex) {
            Logger.getLogger(TAEmpresa.class.getName()).log(Level.SEVERE, null, ex);
        }
        return existeCaja;
    }

    public final boolean ExisteSucursal(int EmpId, int SucId) throws SQLException, Exception {

        boolean existeSucursal = false;
        try {
            ResultSet dt = null;
            sSQL = "Select * from Sucursal Where EmpId = " + EmpId + " And SucId = " + SucId;
            dt = NadDato.GetTable(sSQL);
            if (NadDato.getRowCount(dt) > 0) {
                existeSucursal = true;
            } else {
                existeSucursal = false;
            }
        } catch (Exception ex) {
            Logger.getLogger(TAEmpresa.class.getName()).log(Level.SEVERE, null, ex);
        }
        return existeSucursal;
    }

    public final String GetUniqueIdCaja(int EmpId, int SucId, int CajId) throws SQLException, Exception {

        String tempGetUniqueIdCaja = "";
        try {
            ResultSet dt = null;
            sSQL = "Select * from SucursalCaja Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajId;
            dt = NadDato.GetTable(sSQL);
            if (NadDato.getRowCount(dt) > 0) {
                dt = NadDato.GetTable(sSQL);
                while (dt.next()) {
                    tempGetUniqueIdCaja = dt.getString(SucursalCaja.CajaUniqueId);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TAEmpresa.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tempGetUniqueIdCaja;
    }

    public final boolean GrabarCajaUniqueId(int EmpId, int SucId, int CajId, String UniqueId) throws SQLException, Exception {
        boolean tempGrabarCajaUniqueId = false;
        ResultSet dt = null;
        sSQL = "Select * from SucursalCaja Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajId;
        try {
            dt = NadDato.GetTable(sSQL);
            if (NadDato.getRowCount(dt) > 0) {
                dt = NadDato.GetTable(sSQL);
                Map<String, Object> dtMap = resultSetToArrayList(dt);
                dt = NadDato.GetTable(sSQL);
                while (dt.next()) {
                    dtMap.put(SucursalCaja.CajaUniqueId, UniqueId);
                }
                sSQL = assembly.AssemblyCajaUpdate(dtMap) + " Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajId;
                NadDato.Ejecutar(sSQL);
                tempGrabarCajaUniqueId = true;
            }
        } catch (Exception ex) {
            Logger.getLogger(TAEmpresa.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return tempGrabarCajaUniqueId;
    }

    public final Map<String, Object> CrearEmpresa() {
        Map<String, Object> records = new HashMap<String, Object>();
        records.put(Empresa.EmpId, Integer.class);
        records.put(Empresa.EmpRUC, String.class);
        records.put(Empresa.EmpNom, String.class);
        records.put(Empresa.EmpEmiteTickets, Integer.class);
        records.put(Empresa.EmpEmiteFacturas, Integer.class);
        records.put(Empresa.EmpEmiteRemitos, Integer.class);
        records.put(Empresa.EmpEmiteResguardos, Integer.class);
        records.put(Empresa.EmpEmiteFacturasExportacion, Integer.class);
        records.put(Empresa.EmpEmiteRemitosExportacion, Integer.class);
        records.put(Empresa.EmpEmiteCuentaAjena, Integer.class);
        records.put(Empresa.EmpResolucionIVA, String.class);
        records.put(Empresa.EmpLogoImpresion, byte[].class);
        records.put(Empresa.EmpLogoMiniatura, byte[].class);
        records.put(Empresa.EmpUrlConsultaCFE, String.class);
        records.put(Empresa.EmpFchHoraSync, String.class);
        records.put(Empresa.EmpRowGUID, String.class);
        return records;
    }

    public final Map<String, Object> CrearSucursal() {
        Map<String, Object> records = new HashMap<String, Object>();
        records.put(Sucursal.EmpId, Integer.class);
        records.put(Sucursal.SucId, Integer.class);
        records.put(Sucursal.SucNom, String.class);
        records.put(Sucursal.SucCodEnDGI, Short.class);
        records.put(Sucursal.SucFchHoraUltSync, String.class);
        records.put(Sucursal.SucRowGUID, String.class);
        return records;
    }

    public final Map<String, Object> CrearSucursalCaja() {
        Map<String, Object> records = new HashMap<String, Object>();
        records.put(SucursalCaja.EmpId, Integer.class);
        records.put(SucursalCaja.SucId, Integer.class);
        records.put(SucursalCaja.CajaId, Short.class);
        records.put(SucursalCaja.CajaUniqueId, String.class);
        return records;
    }

    public TAEmpresa(NadMADato Dato) {
        NadDato = Dato;
        Fto.setProveedorConexion(Dato.getProveedorConexion().getValue());
    }

    private Map<String, Object> resultSetToArrayList(ResultSet rs) throws SQLException {
        Map<String, Object> myMap = new HashMap<String, Object>();

        ResultSetMetaData meta = rs.getMetaData();
        int row = 0;
        while (rs.next()) {
            if (row == 0) {
                for (int i = 1; i <= meta.getColumnCount(); i++) {
                    String key = meta.getColumnName(i);
                    Object value = rs.getObject(key);
                    myMap.put(key, value);
                }
                row++;
            }
        }
        return myMap;
    }
}