package TAFE2ApiFirmar;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TACertificado {

    public static class Certificado {

        /**
         * @return the EmpId
         */
        public static String getEmpId() {
            return EmpId;
        }

        /**
         * @return the CertId
         */
        public static String getCertId() {
            return CertId;
        }

        /**
         * @return the CertClaveEncriptacion
         */
        public static String getCertClaveEncriptacion() {
            return CertClaveEncriptacion;
        }

        /**
         * @return the CertArchivo
         */
        public static String getCertArchivo() {
            return CertArchivo;
        }

        /**
         * @return the CertVigencia
         */
        public static String getCertVigencia() {
            return CertVigencia;
        }

        /**
         * @return the CertVencimiento
         */
        public static String getCertVencimiento() {
            return CertVencimiento;
        }

        /**
         * @return the CertFchHoraSync
         */
        public static String getCertFchHoraSync() {
            return CertFchHoraSync;
        }

        /**
         * @return the CertRowGUID
         */
        public static String getCertRowGUID() {
            return CertRowGUID;
        }
        private Object x;
        private static final String EmpId = "EmpId";
        private static final String CertId = "CertId";
        private static final String CertClaveEncriptacion = "CertClaveEncriptacion";
        private static final String CertArchivo = "CertArchivo";
        private static final String CertVigencia = "CertVigencia";
        private static final String CertVencimiento = "CertVencimiento";
        private static final String CertFchHoraSync = "CertFchHoraSync";
        private static final String CertRowGUID = "CertRowGUID";
    }
    private String sSQL;
    private NadMADato NadDato;
    private NadFormato Fto = new NadFormato();
    private AssemblyQuery assembly = new AssemblyQuery();

    public final boolean CargarCertificado(int EmpId, java.util.Date FechaGenerado, short DiasAntelacionCertVenc, tangible.RefObject<String> CertArchivo, tangible.RefObject<String> CertClave, tangible.RefObject<Integer> CertId, tangible.RefObject<String> WarningMsg) throws SQLException, Exception {
        boolean tempCargarCertificado = false;
        ResultSet dt = null;
        sSQL = "Select * From Certificado Where EmpId = " + EmpId + " And CertVigencia <= " + Fto.FechaSQL(FechaGenerado) + " And CertVencimiento > " + Fto.FechaSQL(FechaGenerado) + " Order by CertVigencia Desc";
        try {
            dt = NadDato.GetTable(sSQL);
            if (NadDato.getRowCount(dt) > 0) {
                int cont = 0;//para quedarme solo con el primer registro devuelto que debe ser el que mas demora en vencer certificado
                dt = NadDato.GetTable(sSQL);
                while (dt.next() && cont == 0) {
                    CertArchivo.argValue = dt.getString(Certificado.getCertArchivo());
                    CertClave.argValue = dt.getString(Certificado.getCertClaveEncriptacion());
                    CertId.argValue = dt.getInt(Certificado.getCertId());

                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    Date CertVencimiento = null;
                    try {
                        CertVencimiento = formatter.parse(dt.getString(Certificado.getCertVencimiento()));
                    } catch (ParseException ex) {
                        Logger.getLogger(TACertificado.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    Date fechaAux = new Date(FechaGenerado.getDate());
                    fechaAux.setDate(fechaAux.getDate() + DiasAntelacionCertVenc);
                    if (CertVencimiento.before(fechaAux)) {
                        if (!(HayCertDisponible(dt, FechaGenerado, DiasAntelacionCertVenc))) {
                            if (!WarningMsg.argValue.trim().equals("")) {
                                WarningMsg.argValue += "\r\n";
                            }
                            WarningMsg.argValue += "***ATENCIÓN***: Por favor solicite un nuevo Certificado Digital para Factura Eletrónica. Verifique el actual porque está próximo a caducar.";
                        }
                    }
                    tempCargarCertificado = true;
                    cont++;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TACertificado.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return tempCargarCertificado;
    }

    private boolean HayCertDisponible(ResultSet dt, java.util.Date FechaGenerado, short DiasAntelacionCertVenc) throws SQLException {

        Date auxDate = new Date(FechaGenerado.getDate());
        auxDate.setDate(auxDate.getDate() + DiasAntelacionCertVenc);
        while (dt.next()) {

            if (dt.getDate(Certificado.getCertVencimiento()).getDate() > auxDate.getDate()) {
                return true;
            }
        }
        return false;
    }

    public final ResultSet ConsCertificado(int EmpId) throws SQLException {
        ResultSet dt = null;
        sSQL = "Select * From Certificado Where EmpId = " + EmpId;
        dt = NadDato.GetTable(sSQL);
        //dt.TableName = this.Tb.Certificado.Nombre;
        return dt;
    }

    public final boolean Eliminar(int EmpId, int CertId) throws Exception {
        boolean tempEliminar = false;
        sSQL = "Delete From Certificado Where EmpId = " + EmpId + " And CertId = " + CertId;
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);
        try {
            sttment.execute();
            tempEliminar = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al grabar elimnar certificado.");
        } finally {
            NadDato.FinTrans();
        }
        return tempEliminar;
    }

    public final boolean Guardar(Map<String, Object> drCert) throws SQLException, Exception {
        boolean tempGuardar = false;
        sSQL = "Select * From Certificado Where EmpId = " + Integer.parseInt(drCert.get(Certificado.getEmpId()).toString()) + " And CertId = " + Integer.parseInt(drCert.get(Certificado.getCertId()).toString());
        if (NadDato.getRowCount(NadDato.GetTable(sSQL)) == 0) {
            sSQL = assembly.AssemblyCertificadoInsert(drCert);
        } else {
            sSQL = assembly.AssemblyCertficadoUpdate(drCert) + " Where EmpId = " + Integer.parseInt(drCert.get(Certificado.getEmpId()).toString()) + " And CertId = " + Integer.parseInt(drCert.get(Certificado.getCertId()).toString());
        }
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);
        try {
            sttment.execute();
            tempGuardar = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al guardar certificado.");
        } finally {
            NadDato.FinTrans();
        }
        return tempGuardar;
    }

    public final Map<String, Object> CrearCertificado() {
        //DataTable dt = new DataTable(Tb.Certificado.Nombre);
        Map<String, Object> dt = new HashMap<String, Object>();
        //dt.TableName = Tb.Certificado.Nombre;
        dt.put(Certificado.getEmpId(), Integer.class);
        dt.put(Certificado.getCertId(), Integer.class);
        dt.put(Certificado.getCertClaveEncriptacion(), String.class);
        dt.put(Certificado.getCertArchivo(), String.class);
        dt.put(Certificado.getCertVigencia(), java.util.Date.class);
        dt.put(Certificado.getCertVencimiento(), java.util.Date.class);
        dt.put(Certificado.getCertFchHoraSync(), String.class);
        dt.put(Certificado.getCertRowGUID(), String.class);
        return dt;
    }

    public TACertificado(NadMADato Dato) {
        NadDato = Dato;
        Fto.setProveedorConexion(Dato.getProveedorConexion().getValue());
    }
}