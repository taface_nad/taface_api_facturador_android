/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package TAFE2ApiFirmar;

import org.sqldroid.SQLDroidConnection;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author danielcera
 */
public class AssemblyQuery {

	//ENSAMBLAR UNA QUERY INSERT DE LA TABLA SUCURSAL
	public String AssemblySucursalInsert(Map<String, Object> records) throws SQLException {
		String sql = null;
		if (records.size() > 0) {

			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int SucId = Integer.parseInt(records.get("SucId").toString());
			String SucNom = records.get("SucNom")== null ? "" : records.get("SucNom").toString();
			int SucCodEnDGI = Integer.parseInt(records.get("SucCodEnDGI").toString());
			String SucFchHoraUltSync = records.get("SucFchHoraUltSync").toString();
			String SucRowGUID = records.get("SucRowGUID").toString();
			sql = "insert into Sucursal (EmpId, SucId, SucNom, SucCodEnDGI, SucFchHoraUltSync , SucRowGUID) "
					+ "values ('" + EmpId + "', '" + SucId + "', '" + SucNom + "', '" + SucCodEnDGI + "', '" + SucFchHoraUltSync + "' , '" + SucRowGUID + "')";
		}
		return sql;
	}

	public String AssemblySucursalInsert(ResultSet set) throws SQLException {
		Map<String, Object> records = resultSetToArrayList(set);

		String sql = null;
		if (records.size() > 0) {

			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int SucId = Integer.parseInt(records.get("SucId").toString());
			String SucNom = records.get("SucNom")== null ? "" : records.get("SucNom").toString();
			int SucCodEnDGI = Integer.parseInt(records.get("SucCodEnDGI").toString());
			String SucFchHoraUltSync = records.get("SucFchHoraUltSync").toString();
			String SucRowGUID = records.get("SucRowGUID")== null ? "" : records.get("SucRowGUID").toString();
			sql = "insert into Sucursal (EmpId, SucId, SucNom, SucCodEnDGI, SucFchHoraUltSync , SucRowGUID) "
					+ "values ('" + EmpId + "', '" + SucId + "', '" + SucNom + "', '" + SucCodEnDGI + "', '" + SucFchHoraUltSync + "' , '" + SucRowGUID + "')";
		}
		return sql;
	}

	//ENSAMBLAR UNA QUERY UPDATE DE LA TABLA SUCURSAL
	public String AssemblySucursalUpdate(Map<String, Object> records) throws SQLException {
		String sql = null;
		if (records.size() > 0) {
//                int EmpId = set.getInt("EmpId");
//                int SucId = set.getInt("SucId");
			String SucNom = records.get("SucNom")== null ? "" : records.get("SucNom").toString();
			int SucCodEnDGI = Integer.parseInt(records.get("SucCodEnDGI").toString());
			String SucFchHoraUltSync = records.get("SucFchHoraUltSync").toString();
			String SucRowGUID = records.get("SucRowGUID")== null ? "" : records.get("SucRowGUID").toString();
			sql = "UPDATE Sucursal SET SucNom = '" + SucNom + "',"
					+ "SucCodEnDGI = '" + SucCodEnDGI + "', SucFchHoraUltSync = '" + SucFchHoraUltSync + "', SucRowGUID = '" + SucRowGUID + "'";
		}
		return sql;
	}

	public String AssemblySucursalUpdate(ResultSet set) throws SQLException {
		Map<String, Object> records = resultSetToArrayList(set);

		String sql = null;
		if (records.size() > 0) {
//                int EmpId = set.getInt("EmpId");
//                int SucId = set.getInt("SucId");
			String SucNom = records.get("SucNom")== null ? "" : records.get("SucNom").toString();
			int SucCodEnDGI = Integer.parseInt(records.get("SucCodEnDGI").toString());
			String SucFchHoraUltSync = records.get("SucFchHoraUltSync").toString();
			String SucRowGUID = records.get("SucRowGUID")== null ? "" : records.get("SucRowGUID").toString();
			sql = "UPDATE Sucursal SET SucNom = '" + SucNom + "',"
					+ "SucCodEnDGI = '" + SucCodEnDGI + "', SucFchHoraUltSync = '" + SucFchHoraUltSync + "', SucRowGUID = '" + SucRowGUID + "'";
		}
		return sql;
	}

	//ENSAMBLAR UNA QUERY INSERT DE LA TABLA CAJA
	public String AssemblyCajaInsert(Map<String, Object> records) throws SQLException {
		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int SucId = Integer.parseInt(records.get("SucId").toString());
			int CajaId = Integer.parseInt(records.get("CajaId").toString());
			sql = "insert into SucursalCaja (EmpId, SucId, CajaId) values ('" + EmpId + "', '" + SucId + "', '" + CajaId + "')";
		}
		return sql;
	}

	public String AssemblyCajaInsert(ResultSet set) throws SQLException {
		Map<String, Object> records = resultSetToArrayList(set);

		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int SucId = Integer.parseInt(records.get("SucId").toString());
			int CajaId = Integer.parseInt(records.get("CajaId").toString());
			sql = "insert into SucursalCaja (EmpId, SucId, CajaId) values ('" + EmpId + "', '" + SucId + "', '" + CajaId + "')";
		}
		return sql;
	}

	//ENSAMBLAR UNA QUERY UPDATE DE LA TABLA SUCURSAL
	public String AssemblyCajaUpdate(Map<String, Object> records) throws SQLException {
		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int SucId = Integer.parseInt(records.get("SucId").toString());
			int CajaId = Integer.parseInt(records.get("CajaId").toString());
			String CajaUniqueId = records.get("CajaUniqueId").toString();
			sql = "update SucursalCaja set EmpId = '" + EmpId + "', SucId = '" + SucId + "', CajaId = '" + CajaId + "', CajaUniqueId = '" + CajaUniqueId + "'";
		}
		return sql;
	}

	public String AssemblyCajaUpdate(ResultSet set) throws SQLException {
		Map<String, Object> records = resultSetToArrayList(set);

		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int SucId = Integer.parseInt(records.get("SucId").toString());
			int CajaId = Integer.parseInt(records.get("CajaId").toString());
			String CajaUniqueId = records.get("CajaUniqueId")== null ? "" : records.get("CajaUniqueId").toString();
			sql = "update SucursalCaja set EmpId = '" + EmpId + "', SucId = '" + SucId + "', CajaId = '" + CajaId + "', CajaUniqueId = '" + CajaUniqueId + "'";
		}
		return sql;
	}

	//ENSAMBLAR UNA QUERY INSERT DE LA TABLA EMPRESA
	public String AssemblyEmpresaInsert(Map<String, Object> records) throws SQLException {
		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			String EmpRUC = records.get("EmpRUC").toString();
			String EmpNom = records.get("EmpNom").toString();
			int EmpEmiteTickets = Integer.parseInt(records.get("EmpEmiteTickets").toString());
			int EmpEmiteFacturas = Integer.parseInt(records.get("EmpEmiteFacturas").toString());
			int EmpEmiteRemitos = Integer.parseInt(records.get("EmpEmiteRemitos").toString());
			int EmpEmiteResguardos = Integer.parseInt(records.get("EmpEmiteResguardos").toString());
			int EmpEmiteFacturasExportacion = Integer.parseInt(records.get("EmpEmiteFacturasExportacion").toString());
			int EmpEmiteRemitosExportacion = Integer.parseInt(records.get("EmpEmiteRemitosExportacion").toString());
			int EmpEmiteCuentaAjena = Integer.parseInt(records.get("EmpEmiteCuentaAjena").toString());
			String EmpResolucionIVA = records.get("EmpResolucionIVA") == null ? "" : records.get("EmpResolucionIVA").toString();
//            String EmpLogoImpresion = records.get("EmpLogoImpresion").toString();
//            String EmpLogoMiniatura = records.get("EmpLogoMiniatura").toString();
			String EmpUrlConsultaCFE = records.get("EmpUrlConsultaCFE") == null ? "" : records.get("EmpUrlConsultaCFE").toString();
			String EmpFchHoraSync = records.get("EmpFchHoraSync").toString();
			String EmpRowGUID = records.get("EmpRowGUID").toString();
			sql = "insert into Empresa (EmpId, EmpRUC, EmpNom, EmpEmiteTickets, EmpEmiteFacturas, EmpEmiteRemitos,"
					+ "EmpEmiteResguardos, EmpEmiteFacturasExportacion, EmpEmiteRemitosExportacion, EmpEmiteCuentaAjena,"
					+ "EmpResolucionIVA, EmpLogoImpresion, EmpLogoMiniatura, EmpUrlConsultaCFE, EmpFchHoraSync, EmpRowGUID)"
					+ "values ('" + EmpId + "', '" + EmpRUC + "', '" + EmpNom + "', '" + EmpEmiteTickets + "', "
					+ "'" + EmpEmiteFacturas + "' , '" + EmpEmiteRemitos + "',  '" + EmpEmiteResguardos + "', "
					+ " '" + EmpEmiteFacturasExportacion + "',  '" + EmpEmiteRemitosExportacion + "',  '" + EmpEmiteCuentaAjena + "', "
					+ " '" + EmpResolucionIVA + "', ?, ?,  '" + EmpUrlConsultaCFE + "', "
					+ "'" + EmpFchHoraSync + "',  '" + EmpRowGUID + "')";

		}
		return sql;
	}

	public String AssemblyEmpresaInsert(ResultSet set) throws SQLException {
		Map<String, Object> records = resultSetToArrayList(set);

		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			String EmpRUC = records.get("EmpRUC").toString();
			String EmpNom = records.get("EmpNom").toString();
			int EmpEmiteTickets = Integer.parseInt(records.get("EmpEmiteTickets").toString());
			int EmpEmiteFacturas = Integer.parseInt(records.get("EmpEmiteFacturas").toString());
			int EmpEmiteRemitos = Integer.parseInt(records.get("EmpEmiteRemitos").toString());
			int EmpEmiteResguardos = Integer.parseInt(records.get("EmpEmiteResguardos").toString());
			int EmpEmiteFacturasExportacion = Integer.parseInt(records.get("EmpEmiteFacturasExportacion").toString());
			int EmpEmiteRemitosExportacion = Integer.parseInt(records.get("EmpEmiteRemitosExportacion").toString());
			int EmpEmiteCuentaAjena = Integer.parseInt(records.get("EmpEmiteCuentaAjena").toString());
			String EmpResolucionIVA = records.get("EmpResolucionIVA")== null ? "" : records.get("EmpResolucionIVA").toString();
			//Blob EmpLogoImpresion = (Blob)records.get("EmpLogoImpresion");
			//Blob EmpLogoMiniatura = (Blob)records.get("EmpLogoMiniatura");
			String EmpUrlConsultaCFE = records.get("EmpUrlConsultaCFE")== null ? "" : records.get("EmpUrlConsultaCFE").toString();
			String EmpFchHoraSync = records.get("EmpFchHoraSync").toString();
			String EmpRowGUID = records.get("EmpRowGUID").toString();
			sql = "insert into Empresa (EmpId, EmpRUC, EmpNom, EmpEmiteTickets, EmpEmiteFacturas, EmpEmiteRemitos,"
					+ "EmpEmiteResguardos, EmpEmiteFacturasExportacion, EmpEmiteRemitosExportacion, EmpEmiteCuentaAjena,"
					+ "EmpResolucionIVA, EmpLogoImpresion, EmpLogoMiniatura, EmpUrlConsultaCFE, EmpFchHoraSync, EmpRowGUID)"
					+ "values ('" + EmpId + "', '" + EmpRUC + "', '" + EmpNom + "', '" + EmpEmiteTickets + "', "
					+ "'" + EmpEmiteFacturas + "' , '" + EmpEmiteRemitos + "',  '" + EmpEmiteResguardos + "',"
					+ " '" + EmpEmiteFacturasExportacion + "',  '" + EmpEmiteRemitosExportacion + "',  '" + EmpEmiteCuentaAjena + "',"
					+ " '" + EmpResolucionIVA + "', ?, ?,  '" + EmpUrlConsultaCFE + "'"
					+ "'" + EmpFchHoraSync + "',  '" + EmpRowGUID + "')";

		}
		return sql;
	}

	//ENSAMBLAR UNA QUERY INSERT DE LA TABLA EMPRESA
	public String AssemblyEmpresaUpdate(Map<String, Object> records) throws SQLException {
		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			String EmpRUC = records.get("EmpRUC").toString();
			String EmpNom = records.get("EmpNom").toString();
			int EmpEmiteTickets = Integer.parseInt(records.get("EmpEmiteTickets").toString());
			int EmpEmiteFacturas = Integer.parseInt(records.get("EmpEmiteFacturas").toString());
			int EmpEmiteRemitos = Integer.parseInt(records.get("EmpEmiteRemitos").toString());
			int EmpEmiteResguardos = Integer.parseInt(records.get("EmpEmiteResguardos").toString());
			int EmpEmiteFacturasExportacion = Integer.parseInt(records.get("EmpEmiteFacturasExportacion").toString());
			int EmpEmiteRemitosExportacion = Integer.parseInt(records.get("EmpEmiteRemitosExportacion").toString());
			int EmpEmiteCuentaAjena = Integer.parseInt(records.get("EmpEmiteCuentaAjena").toString());
			String EmpResolucionIVA = records.get("EmpResolucionIVA")== null ? "" : records.get("EmpResolucionIVA").toString();
			//Blob EmpLogoImpresion = (Blob)records.get("EmpLogoImpresion");
			//Blob EmpLogoMiniatura = (Blob)records.get("EmpLogoMiniatura");
			String EmpUrlConsultaCFE = records.get("EmpUrlConsultaCFE")== null ? "" : records.get("EmpUrlConsultaCFE").toString();
			String EmpFchHoraSync = records.get("EmpFchHoraSync").toString();
			String EmpRowGUID = records.get("EmpRowGUID").toString();
			sql = "update Empresa set EmpRUC = '" + EmpRUC + "', EmpNom = '" + EmpNom + "', EmpEmiteTickets = '" + EmpEmiteTickets + "',"
					+ "EmpEmiteFacturas = '" + EmpEmiteFacturas + "', EmpEmiteRemitos = '" + EmpEmiteRemitos + "',"
					+ "EmpEmiteResguardos = '" + EmpEmiteResguardos + "', EmpEmiteFacturasExportacion = '" + EmpEmiteFacturasExportacion + "', "
					+ "EmpEmiteRemitosExportacion = '" + EmpEmiteRemitosExportacion + "', EmpEmiteCuentaAjena = '" + EmpEmiteCuentaAjena + "',"
					+ "EmpResolucionIVA = '" + EmpResolucionIVA + "', EmpLogoImpresion = ?, EmpLogoMiniatura = ?,"
					+ "EmpUrlConsultaCFE = '" + EmpUrlConsultaCFE + "', EmpFchHoraSync = '" + EmpFchHoraSync + "', EmpRowGUID = '" + EmpRowGUID + "'";
		}
		return sql;
	}

	public String AssemblyEmpresaUpdate(ResultSet set) throws SQLException {
		Map<String, Object> records = resultSetToArrayList(set);

		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			String EmpRUC = records.get("EmpRUC").toString();
			String EmpNom = records.get("EmpNom").toString();
			int EmpEmiteTickets = Integer.parseInt(records.get("EmpEmiteTickets").toString());
			int EmpEmiteFacturas = Integer.parseInt(records.get("EmpEmiteFacturas").toString());
			int EmpEmiteRemitos = Integer.parseInt(records.get("EmpEmiteRemitos").toString());
			int EmpEmiteResguardos = Integer.parseInt(records.get("EmpEmiteResguardos").toString());
			int EmpEmiteFacturasExportacion = Integer.parseInt(records.get("EmpEmiteFacturasExportacion").toString());
			int EmpEmiteRemitosExportacion = Integer.parseInt(records.get("EmpEmiteRemitosExportacion").toString());
			int EmpEmiteCuentaAjena = Integer.parseInt(records.get("EmpEmiteCuentaAjena").toString());
			String EmpResolucionIVA = records.get("EmpResolucionIVA")== null ? "" : records.get("EmpResolucionIVA").toString();
			//Blob EmpLogoImpresion = (Blob)records.get("EmpLogoImpresion");
			//Blob EmpLogoMiniatura = (Blob)records.get("EmpLogoMiniatura");
			String EmpUrlConsultaCFE = records.get("EmpUrlConsultaCFE")== null ? "" : records.get("EmpUrlConsultaCFE").toString();
			String EmpFchHoraSync = records.get("EmpFchHoraSync").toString();
			String EmpRowGUID = records.get("EmpRowGUID").toString();
			sql = "update Empresa set EmpRUC = '" + EmpRUC + "', EmpNom = '" + EmpNom + "', EmpEmiteTickets = '" + EmpEmiteTickets + "',"
					+ "EmpEmiteFacturas = '" + EmpEmiteFacturas + "', EmpEmiteRemitos = '" + EmpEmiteRemitos + "',"
					+ "EmpEmiteResguardos = '" + EmpEmiteResguardos + "', EmpEmiteFacturasExportacion = '" + EmpEmiteFacturasExportacion + "', "
					+ "EmpEmiteRemitosExportacion = '" + EmpEmiteRemitosExportacion + "', EmpEmiteCuentaAjena = '" + EmpEmiteCuentaAjena + "',"
					+ "EmpResolucionIVA = '" + EmpResolucionIVA + "', EmpLogoImpresion = ?, EmpLogoMiniatura = ?,"
					+ "EmpUrlConsultaCFE = '" + EmpUrlConsultaCFE + "', EmpFchHoraSync = '" + EmpFchHoraSync + "', EmpRowGUID = '" + EmpRowGUID + "'";
		}
		return sql;
	}

	//ENSAMBLAR UNA QUERY INSERT DE COMPROBANTE
	public String AssemblyComprobanteInsert(Map<String, Object> records) throws SQLException {

		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int SucId = Integer.parseInt(records.get("SucId").toString());
			int CajaId = Integer.parseInt(records.get("CajaId").toString());
			int CteId = Integer.parseInt(records.get("CteId").toString());
			int CAEId = Integer.parseInt(records.get("CAEId").toString());
			int CAEParteId = Integer.parseInt(records.get("CAEParteId").toString());
			String CteCAESerie = records.get("CteCAESerie").toString();
			int CteCAENro = Integer.parseInt(records.get("CteCAENro").toString());
			long CteCAENroAutorizacion = Long.parseLong(records.get("CteCAENroAutorizacion").toString());
			int CteCAETipoCFE = Integer.parseInt(records.get("CteCAETipoCFE").toString());
			int CteCAENroDesde = Integer.parseInt(records.get("CteCAENroDesde").toString());
			int CteCAENroHasta = Integer.parseInt(records.get("CteCAENroHasta").toString());
			String CteCAEVencimiento = records.get("CteCAEVencimiento").toString();
			String CteFchHoraFirma = records.get("CteFchHoraFirma").toString();
			int CertId = Integer.parseInt(records.get("CertId").toString());
			String CteXMLEntrada = records.get("CteXMLEntrada").toString();
			String CteXMLRespuesta = records.get("CteXMLRespuesta").toString();
			String CteXMLFirmado = records.get("CteXMLFirmado").toString();
			String CteAdendaTexto = records.get("CteAdendaTexto").toString();
			String CteFchEmision = records.get("CteFchEmision").toString();
			String CteDocOriginalTipo = records.get("CteDocOriginalTipo").toString();
			String CteDocOriginalSerie = records.get("CteDocOriginalSerie").toString();
			String CteDocOriginalNro = records.get("CteDocOriginalNro").toString();
			String CteCajaUniqueId = records.get("CteCajaUniqueId").toString();
			String CteVersionAPI = records.get("CteVersionAPI").toString();
			int CteSincronizado = Integer.parseInt(records.get("CteSincronizado").toString());
			int CteSyncId = Integer.parseInt(records.get("CteSyncId").toString());
			String CteFchHoraSync = records.get("CteFchHoraSync").toString();
			sql = "insert into Comprobante (EmpId,SucId,CajaId,CteId,CAEId,CAEParteId,CteCAESerie,CteCAENro,"
					+ "CteCAENroAutorizacion,CteCAETipoCFE,CteCAENroDesde,CteCAENroHasta,CteCAEVencimiento,"
					+ "CteFchHoraFirma,CertId,CteXMLEntrada,CteXMLRespuesta,CteXMLFirmado,CteAdendaTexto,CteFchEmision,"
					+ "CteDocOriginalTipo, CteDocOriginalSerie, CteDocOriginalNro,CteCajaUniqueId, CteVersionAPI,"
					+ "CteSincronizado,CteSyncId , CteFchHoraSync)"
					+ "values ( '" + EmpId + "', '" + SucId + "', '" + CajaId + "', '" + CteId + "', "
					+ "'" + CAEId + "', '" + CAEParteId + "', '" + CteCAESerie + "', '" + CteCAENro + "', "
					+ "'" + CteCAENroAutorizacion + "', '" + CteCAETipoCFE + "', '" + CteCAENroDesde + "',"
					+ "'" + CteCAENroHasta + "', '" + CteCAEVencimiento + "', '" + CteFchHoraFirma + "', '" + CertId + "',"
					+ "'" + CteXMLEntrada + "', '" + CteXMLRespuesta + "', '" + CteXMLFirmado + "', '" + CteAdendaTexto + "',"
					+ "'" + CteFchEmision + "', '" + CteDocOriginalTipo + "', '" + CteDocOriginalSerie + "', "
					+ "'" + CteDocOriginalNro + "', '" + CteCajaUniqueId + "', '" + CteVersionAPI + "', '" + CteSincronizado + "',"
					+ "'" + CteSyncId + "', '" + CteFchHoraSync + "')";
		}
		return sql;
	}

	public String AssemblyComprobanteInsert(ResultSet set) throws SQLException {
		Map<String, Object> records = resultSetToArrayList(set);

		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int SucId = Integer.parseInt(records.get("SucId").toString());
			int CajaId = Integer.parseInt(records.get("CajaId").toString());
			int CteId = Integer.parseInt(records.get("CteId").toString());
			int CAEId = Integer.parseInt(records.get("CAEId").toString());
			int CAEParteId = Integer.parseInt(records.get("CAEParteId").toString());
			String CteCAESerie = records.get("CteCAESerie").toString();
			int CteCAENro = Integer.parseInt(records.get("CteCAENro").toString());
			int CteCAENroAutorizacion = Integer.parseInt(records.get("CteCAENroAutorizacion").toString());
			int CteCAETipoCFE = Integer.parseInt(records.get("CteCAETipoCFE").toString());
			int CteCAENroDesde = Integer.parseInt(records.get("CteCAENroDesde").toString());
			int CteCAENroHasta = Integer.parseInt(records.get("CteCAENroHasta").toString());
			String CteCAEVencimiento = records.get("CteCAEVencimiento").toString();
			String CteFchHoraFirma = records.get("CteFchHoraFirma").toString();
			int CertId = Integer.parseInt(records.get("CertId").toString());
			String CteXMLEntrada = records.get("CteXMLEntrada").toString();
			String CteXMLRespuesta = records.get("CteXMLRespuesta").toString();
			String CteXMLFirmado = records.get("CteXMLFirmado").toString();
			String CteAdendaTexto = records.get("CteAdendaTexto").toString();
			String CteFchEmision = records.get("CteFchEmision").toString();
			String CteDocOriginalTipo = records.get("CteDocOriginalTipo").toString();
			String CteDocOriginalSerie = records.get("CteDocOriginalSerie").toString();
			String CteDocOriginalNro = records.get("CteDocOriginalNro").toString();
			String CteCajaUniqueId = records.get("CteCajaUniqueId").toString();
			String CteVersionAPI = records.get("CteVersionAPI").toString();
			int CteSincronizado = Integer.parseInt(records.get("CteSincronizado").toString());
			int CteSyncId = Integer.parseInt(records.get("CteSyncId").toString());
			String CteFchHoraSync = records.get("CteFchHoraSync").toString();
			sql = "insert into Comprobante (EmpId,SucId,CajaId,CteId,CAEId,CAEParteId,CteCAESerie,CteCAENro,"
					+ "CteCAENroAutorizacion,CteCAETipoCFE,CteCAENroDesde,CteCAENroHasta,CteCAEVencimiento,"
					+ "CteFchHoraFirma,CertId,CteXMLEntrada,CteXMLRespuesta,CteXMLFirmado,CteAdendaTexto,CteFchEmision,"
					+ "CteDocOriginalTipo, CteDocOriginalSerie, CteDocOriginalNro,CteCajaUniqueId, CteVersionAPI,"
					+ "CteSincronizado,CteSyncId , CteFchHoraSync)"
					+ "values ( '" + EmpId + "', '" + SucId + "', '" + CajaId + "', '" + CteId + "', "
					+ "'" + CAEId + "', '" + CAEParteId + "', '" + CteCAESerie + "', '" + CteCAENro + "', "
					+ "'" + CteCAENroAutorizacion + "', '" + CteCAETipoCFE + "', '" + CteCAENroDesde + "',"
					+ "'" + CteCAENroHasta + "', '" + CteCAEVencimiento + "', '" + CteFchHoraFirma + "', '" + CertId + "',"
					+ "'" + CteXMLEntrada + "', '" + CteXMLRespuesta + "', '" + CteXMLFirmado + "', '" + CteAdendaTexto + "',"
					+ "'" + CteFchEmision + "', '" + CteDocOriginalTipo + "', '" + CteDocOriginalSerie + "', "
					+ "'" + CteDocOriginalNro + "', '" + CteCajaUniqueId + "', '" + CteVersionAPI + "', '" + CteSincronizado + "',"
					+ "'" + CteSyncId + "', '" + CteFchHoraSync + "')";

		}
		return sql;
	}

	public PreparedStatement AssemblyComprobanteInsert(Map<String, Object> records, SQLDroidConnection connection) throws SQLException {

		PreparedStatement statement = null;
		String sql = "insert into Comprobante (EmpId,SucId,CajaId,CteId,CAEId,CAEParteId,CteCAESerie,CteCAENro,"
				+ "CteCAENroAutorizacion,CteCAETipoCFE,CteCAENroDesde,CteCAENroHasta,CteCAEVencimiento,"
				+ "CteFchHoraFirma,CertId,CteXMLEntrada,CteXMLRespuesta,CteXMLFirmado,CteAdendaTexto,CteFchEmision,"
				+ "CteDocOriginalTipo, CteDocOriginalSerie, CteDocOriginalNro,CteCajaUniqueId, CteVersionAPI,"
				+ "CteSincronizado,CteSyncId , CteFchHoraSync)"
				+ "values ( ?, ?, ?, ?, "
				+ "?, ?, ?, ?, "
				+ "?, ?, ?,"
				+ "?, ?, ?, ?,"
				+ "?, ?, ?, ?,"
				+ "?, ?, ?, "
				+ "?, ?, ?, ?,"
				+ "?, ?)";
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int SucId = Integer.parseInt(records.get("SucId").toString());
			int CajaId = Integer.parseInt(records.get("CajaId").toString());
			int CteId = Integer.parseInt(records.get("CteId").toString());
			int CAEId = Integer.parseInt(records.get("CAEId").toString());
			int CAEParteId = Integer.parseInt(records.get("CAEParteId").toString());
			String CteCAESerie = records.get("CteCAESerie").toString();
			int CteCAENro = Integer.parseInt(records.get("CteCAENro").toString());
			long CteCAENroAutorizacion = Long.parseLong(records.get("CteCAENroAutorizacion").toString());
			int CteCAETipoCFE = Integer.parseInt(records.get("CteCAETipoCFE").toString());
			int CteCAENroDesde = Integer.parseInt(records.get("CteCAENroDesde").toString());
			int CteCAENroHasta = Integer.parseInt(records.get("CteCAENroHasta").toString());
			String CteCAEVencimiento = records.get("CteCAEVencimiento").toString();
			String CteFchHoraFirma = records.get("CteFchHoraFirma").toString();
			int CertId = Integer.parseInt(records.get("CertId").toString());
			String CteXMLEntrada = records.get("CteXMLEntrada").toString();
			String CteXMLRespuesta = records.get("CteXMLRespuesta").toString();
			String CteXMLFirmado = records.get("CteXMLFirmado").toString();
			String CteAdendaTexto = records.get("CteAdendaTexto").toString();
			String CteFchEmision = records.get("CteFchEmision").toString();
			String CteDocOriginalTipo = records.get("CteDocOriginalTipo").toString();
			String CteDocOriginalSerie = records.get("CteDocOriginalSerie").toString();
			String CteDocOriginalNro = records.get("CteDocOriginalNro").toString();
			String CteCajaUniqueId = records.get("CteCajaUniqueId").toString();
			String CteVersionAPI = records.get("CteVersionAPI").toString();
			int CteSincronizado = Integer.parseInt(records.get("CteSincronizado").toString());
			int CteSyncId = Integer.parseInt(records.get("CteSyncId").toString());
			String CteFchHoraSync = records.get("CteFchHoraSync").toString();


			statement = connection.prepareStatement(sql);
			statement.setInt(1, EmpId);
			statement.setInt(2, SucId);
			statement.setInt(3, CajaId);
			statement.setInt(4, CteId);
			statement.setInt(5, CAEId);
			statement.setInt(6, CAEParteId);
			statement.setString(7, CteCAESerie);
			statement.setInt(8, CteCAENro);
			statement.setLong(9, CteCAENroAutorizacion);
			statement.setInt(10, CteCAETipoCFE);
			statement.setInt(11, CteCAENroDesde);
			statement.setInt(12, CteCAENroHasta);
			statement.setString(13, CteCAEVencimiento);
			statement.setString(14, CteFchHoraFirma);
			statement.setInt(15, CertId);
			statement.setString(16, CteXMLEntrada);
			statement.setString(17, CteXMLRespuesta);
			statement.setString(18, CteXMLFirmado);
			statement.setString(19, CteAdendaTexto);
			statement.setString(20, CteFchEmision);
			statement.setString(21, CteDocOriginalTipo);
			statement.setString(22, CteDocOriginalSerie);
			statement.setString(23, CteDocOriginalNro);
			statement.setString(24, CteCajaUniqueId);
			statement.setString(25, CteVersionAPI);
			statement.setInt(26, CteSincronizado);
			statement.setInt(27, CteSyncId);
			statement.setString(28, CteFchHoraSync);
		}
		return statement;
	}

	//ENSAMBLAR UNA QUERY INSERT DE LA TABLA CERTIFICADO
	public String AssemblyCertificadoInsert(Map<String, Object> records) throws SQLException {

		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int CertId = Integer.parseInt(records.get("CertId").toString());
			String CertClaveEncriptacion = records.get("CertClaveEncriptacion").toString();
			String CertArchivo = records.get("CertArchivo").toString().toString();
			String CertVigencia = records.get("CertVigencia").toString();
			String CertVencimiento = records.get("CertVencimiento").toString();
			String CertFchHoraSync = records.get("CertFchHoraSync").toString();
			String CertRowGUID = records.get("CertRowGUID").toString();
			sql = "insert into Certificado (EmpId, CertId, CertClaveEncriptacion,CertArchivo,CertVigencia,"
					+ "CertVencimiento, CertFchHoraSync, CertRowGUID)"
					+ "values ('" + EmpId + "', '" + CertId + "',"
					+ "'" + CertClaveEncriptacion + "', '" + CertArchivo + "','" + CertVigencia + "',"
					+ "'" + CertVencimiento + "','" + CertFchHoraSync + "', '" + CertRowGUID + "')";
		}
		return sql;
	}

	public String AssemblyCertificadoInsert(ResultSet set) throws SQLException {
		Map<String, Object> records = resultSetToArrayList(set);

		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int CertId = Integer.parseInt(records.get("CertId").toString());
			String CertClaveEncriptacion = records.get("CertClaveEncriptacion").toString();
			String CertArchivo = records.get("CertArchivo").toString().toString();
			String CertVigencia = records.get("CertVigencia").toString();
			String CertVencimiento = records.get("CertVencimiento").toString();
			String CertFchHoraSync = records.get("CertFchHoraSync").toString();
			String CertRowGUID = records.get("CertRowGUID").toString();
			sql = "insert into Certificado (EmpId, CertId, CertClaveEncriptacion,CertArchivo,CertVigencia,"
					+ "CertVencimiento, CertFchHoraSync, CertRowGUID)"
					+ "values ('" + EmpId + "', '" + CertId + "',"
					+ "'" + CertClaveEncriptacion + "', '" + CertArchivo + "','" + CertVigencia + "',"
					+ "'" + CertVencimiento + "','" + CertFchHoraSync + "', '" + CertRowGUID + "')";
		}
		return sql;
	}

//ENSAMBLAR UNA QUERY UPDATE DE LA TABLA CERTIFICADO
	public String AssemblyCertficadoUpdate(Map<String, Object> records) throws SQLException {
		String sql = null;
		if (records.size() > 0) {
			//int EmpId = Integer.parseInt(records.get("EmpId").toString());
			//int CertId = Integer.parseInt(records.get("CertId").toString());
			String CertClaveEncriptacion = records.get("CertClaveEncriptacion").toString();
			String CertArchivo = records.get("CertArchivo").toString().toString();
			String CertVigencia = records.get("CertVigencia").toString();
			String CertVencimiento = records.get("CertVencimiento").toString();
			String CertFchHoraSync = records.get("CertFchHoraSync").toString();
			String CertRowGUID = records.get("CertRowGUID").toString();
			sql = "update Certificado set CertClaveEncriptacion = '" + CertClaveEncriptacion + "', "
					+ "CertArchivo = '" + CertArchivo + "', CertVigencia = '" + CertVigencia + "', "
					+ "CertVencimiento = '" + CertVencimiento + "',CertFchHoraSync = '" + CertFchHoraSync + "',"
					+ "CertRowGUID = '" + CertRowGUID + "'";
		}
		return sql;
	}

	public String AssemblyCertficadoUpdate(ResultSet set) throws SQLException {
		Map<String, Object> records = resultSetToArrayList(set);

		String sql = null;
		if (records.size() > 0) {
			//int EmpId = Integer.parseInt(records.get("EmpId").toString());
			//int CertId = Integer.parseInt(records.get("CertId").toString());
			String CertClaveEncriptacion = records.get("CertClaveEncriptacion").toString();
			String CertArchivo = records.get("CertArchivo").toString().toString();
			String CertVigencia = records.get("CertVigencia").toString();
			String CertVencimiento = records.get("CertVencimiento").toString();
			String CertFchHoraSync = records.get("CertFchHoraSync").toString();
			String CertRowGUID = records.get("CertRowGUID").toString();
			sql = "update Certificado set CertClaveEncriptacion = '" + CertClaveEncriptacion + "', "
					+ "CertArchivo = '" + CertArchivo + "', CertVigencia = '" + CertVigencia + "', "
					+ "CertVencimiento = '" + CertVencimiento + "',CertFchHoraSync = '" + CertFchHoraSync + "',"
					+ "CertRowGUID = '" + CertRowGUID + "'";
		}
		return sql;
	}

	//ENSAMBLAR UNA QUERY INSERT DE LA TABLA CERTIFICADODGI
	public String AssemblyCertificadoDGIInsert(Map<String, Object> records) throws SQLException {

		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int CertDGIId = Integer.parseInt(records.get("CertDGIId").toString());
			String CertDGIVigencia = records.get("CertDGIVigencia").toString();
			String CertDGIVencimiento = records.get("CertDGIVencimiento").toString();
			String CertDGIArchivo = records.get("CertDGIArchivo").toString();
			String CertDGICN = records.get("CertDGICN").toString();
			String CertDGIFchHoraSync = records.get("CertDGIFchHoraSync").toString();
			String CertDGIRowGUID = records.get("CertDGIRowGUID").toString();
			sql = "insert into CertificadoDGI (EmpId, CertDGIId, CertDGIVigencia,CertDGIVencimiento,CertDGIArchivo,"
					+ "CertDGICN, CertDGIFchHoraSync, CertDGIRowGUID)"
					+ "values ('" + EmpId + "', '" + CertDGIId + "',"
					+ "'" + CertDGIVigencia + "', '" + CertDGIVencimiento + "','" + CertDGIArchivo + "',"
					+ "'" + CertDGICN + "','" + CertDGIFchHoraSync + "', '" + CertDGIRowGUID + "')";
		}
		return sql;
	}

	public String AssemblyCertificadoDGIInsert(ResultSet set) throws SQLException {
		Map<String, Object> records = resultSetToArrayList(set);

		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int CertDGIId = Integer.parseInt(records.get("CertDGIId").toString());
			String CertDGIVigencia = records.get("CertDGIVigencia").toString();
			String CertDGIVencimiento = records.get("CertDGIVencimiento").toString();
			String CertDGIArchivo = records.get("CertDGIArchivo").toString();
			String CertDGICN = records.get("CertDGICN").toString();
			String CertDGIFchHoraSync = records.get("CertDGIFchHoraSync").toString();
			String CertDGIRowGUID = records.get("CertDGIRowGUID").toString();
			sql = "insert into CertificadoDGI (EmpId, CertDGIId, CertDGIVigencia,CertDGIVencimiento,CertDGIArchivo,"
					+ "CertDGICN, CertDGIFchHoraSync, CertDGIRowGUID)"
					+ "values ('" + EmpId + "', '" + CertDGIId + "',"
					+ "'" + CertDGIVigencia + "', '" + CertDGIVencimiento + "','" + CertDGIArchivo + "',"
					+ "'" + CertDGICN + "','" + CertDGIFchHoraSync + "', '" + CertDGIRowGUID + "')";
		}
		return sql;
	}

//ENSAMBLAR UNA QUERY UPDATE DE LA TABLA CERTIFICADO
	public String AssemblyCertficadoDGIUpdate(Map<String, Object> records) throws SQLException {
		String sql = null;
		if (records.size() > 0) {
			//int EmpId = Integer.parseInt(records.get("EmpId").toString());
			//int CertDGIId = Integer.parseInt(records.get("CertDGIId").toString());
			String CertDGIVigencia = records.get("CertDGIVigencia").toString();
			String CertDGIVencimiento = records.get("CertDGIVencimiento").toString();
			String CertDGIArchivo = records.get("CertDGIArchivo").toString();
			String CertDGICN = records.get("CertDGICN").toString();
			String CertDGIFchHoraSync = records.get("CertDGIFchHoraSync").toString();
			String CertDGIRowGUID = records.get("CertDGIRowGUID").toString();
			sql = "update CertificadoDGI set CertDGIVigencia = '" + CertDGIVigencia + "', "
					+ "CertDGIVencimiento = '" + CertDGIVencimiento + "', CertDGIArchivo = '" + CertDGIArchivo + "', "
					+ "CertDGICN = '" + CertDGICN + "',CertDGIFchHoraSync = '" + CertDGIFchHoraSync + "',"
					+ "CertDGIRowGUID = '" + CertDGIRowGUID + "'";
		}
		return sql;
	}

	public String AssemblyCertficadoDGIUpdate(ResultSet set) throws SQLException {
		Map<String, Object> records = resultSetToArrayList(set);

		String sql = null;
		if (records.size() > 0) {
			//int EmpId = Integer.parseInt(records.get("EmpId").toString());
			//int CertDGIId = Integer.parseInt(records.get("CertDGIId").toString());
			String CertDGIVigencia = records.get("CertDGIVigencia").toString();
			String CertDGIVencimiento = records.get("CertDGIVencimiento").toString();
			String CertDGIArchivo = records.get("CertDGIArchivo").toString();
			String CertDGICN = records.get("CertDGICN").toString();
			String CertDGIFchHoraSync = records.get("CertDGIFchHoraSync").toString();
			String CertDGIRowGUID = records.get("CertDGIRowGUID").toString();
			sql = "update CertificadoDGI set CertDGIVigencia = '" + CertDGIVigencia + "', "
					+ "CertDGIVencimiento = '" + CertDGIVencimiento + "', CertDGIArchivo = '" + CertDGIArchivo + "', "
					+ "CertDGICN = '" + CertDGICN + "',CertDGIFchHoraSync = '" + CertDGIFchHoraSync + "',"
					+ "CertDGIRowGUID = '" + CertDGIRowGUID + "'";
		}
		return sql;
	}

	//ENSAMBLAR UNA QUERY INSERT DE LA TABLA CAEParteUtilizado
	public String AssemblyCAEParteUtilizadoInsert(Map<String, Object> records) throws SQLException {
		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int CAEId = Integer.parseInt(records.get("CAEId").toString());
			String CAEParteId = records.get("CAEParteId").toString();
			String CAEParteFchUtilizado = records.get("CAEParteFchUtilizado").toString().replace("'", "");
			String CAEParteUtilNroDesde = records.get("CAEParteUtilNroDesde").toString();
			String CAEParteUtilNroHasta = records.get("CAEParteUtilNroHasta").toString();
			String CAEParteUtilSincronizado = records.get("CAEParteUtilSincronizado").toString();
			sql = "insert into CAEParteUtilizado (EmpId, CAEId, CAEParteId,CAEParteFchUtilizado,CAEParteUtilNroDesde,"
					+ "CAEParteUtilNroHasta, CAEParteUtilSincronizado)"
					+ "values ('" + EmpId + "', '" + CAEId + "',"
					+ "'" + CAEParteId + "', '" + CAEParteFchUtilizado + "','" + CAEParteUtilNroDesde + "',"
					+ "'" + CAEParteUtilNroHasta + "','" + CAEParteUtilSincronizado + "')";
		}
		return sql;
	}

	public String AssemblyCAEParteUtilizadoInsert(ResultSet set) throws SQLException {
		Map<String, Object> records = resultSetToArrayList(set);
		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int CAEId = Integer.parseInt(records.get("CAEId").toString());
			String CAEParteId = records.get("CAEParteId").toString();
			String CAEParteFchUtilizado = records.get("CAEParteFchUtilizado").toString().replace("'", "");
			String CAEParteUtilNroDesde = records.get("CAEParteUtilNroDesde").toString();
			String CAEParteUtilNroHasta = records.get("CAEParteUtilNroHasta").toString();
			String CAEParteUtilSincronizado = records.get("CAEParteUtilSincronizado").toString();
			sql = "insert into CAEParteUtilizado (EmpId, CAEId, CAEParteId,CAEParteFchUtilizado,CAEParteUtilNroDesde,"
					+ "CAEParteUtilNroHasta, CAEParteUtilSincronizado)"
					+ "values ('" + EmpId + "', '" + CAEId + "',"
					+ "'" + CAEParteId + "', '" + CAEParteFchUtilizado + "','" + CAEParteUtilNroDesde + "',"
					+ "'" + CAEParteUtilNroHasta + "','" + CAEParteUtilSincronizado + "')";
		}
		return sql;
	}

	//ENSAMBLAR UNA QUERY INSERT DE LA TABLA CAEParteUtilizadoAnulado
	public String AssemblyCAEParteUtilizadoAnuladoInsert(Map<String, Object> records) throws SQLException {
		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int CAEId = Integer.parseInt(records.get("CAEId").toString());
			int CAEParteId = Integer.parseInt(records.get("CAEParteId").toString());
			String CAEParteFchUtilizado = records.get("CAEParteFchUtilizado").toString().replace("'", "");
			int CAEParteAnulNro = Integer.parseInt(records.get("CAEParteAnulNro").toString());
			int CAEParteAnulSincronizado = Integer.parseInt(records.get("CAEParteAnulSincronizado").toString());
			int CAEParteAnulRestaurar = Integer.parseInt(records.get("CAEParteAnulRestaurar").toString());
			sql = "insert into CAEParteUtilizadoAnulado (EmpId, CAEId, CAEParteId,CAEParteFchUtilizado,CAEParteAnulNro,"
					+ "CAEParteAnulSincronizado, CAEParteAnulRestaurar)"
					+ "values ('" + EmpId + "', '" + CAEId + "',"
					+ "'" + CAEParteId + "', '" + CAEParteFchUtilizado + "','" + CAEParteAnulNro + "',"
					+ "'" + CAEParteAnulSincronizado + "','" + CAEParteAnulRestaurar + "')";
		}
		return sql;
	}

	public String AssemblyCAEParteUtilizadoAnuladoInsert(ResultSet set) throws SQLException {
		Map<String, Object> records = resultSetToArrayList(set);
		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int CAEId = Integer.parseInt(records.get("CAEId").toString());
			int CAEParteId = Integer.parseInt(records.get("CAEParteId").toString());
			String CAEParteFchUtilizado = records.get("CAEParteFchUtilizado").toString().replace("'", "");
			int CAEParteAnulNro = Integer.parseInt(records.get("CAEParteAnulNro").toString());
			int CAEParteAnulSincronizado = Integer.parseInt(records.get("CAEParteAnulSincronizado").toString());
			int CAEParteAnulRestaurar = Integer.parseInt(records.get("CAEParteAnulRestaurar").toString());
			sql = "insert into CAEParteUtilizadoAnulado (EmpId, CAEId, CAEParteId,CAEParteFchUtilizado,CAEParteAnulNro,"
					+ "CAEParteAnulSincronizado, CAEParteAnulRestaurar)"
					+ "values ('" + EmpId + "', '" + CAEId + "',"
					+ "'" + CAEParteId + "', '" + CAEParteFchUtilizado + "','" + CAEParteAnulNro + "',"
					+ "'" + CAEParteAnulSincronizado + "','" + CAEParteAnulRestaurar + "')";
		}
		return sql;
	}

	//ENSAMBLAR UNA QUERY INSERT DE LA TABLA CAE
	public String AssemblyCAEInsert(Map<String, Object> records) throws SQLException {
		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int CAEId = Integer.parseInt(records.get("CAEId").toString());
			BigDecimal CAENroAutorizacion = new BigDecimal(records.get("CAENroAutorizacion").toString());
			short CAETipoCFE = Short.parseShort(records.get("CAETipoCFE").toString());
			String CAESerie = records.get("CAESerie").toString();
			int CAENroDesde = Integer.parseInt(records.get("CAENroDesde").toString());
			int CAENroHasta = Integer.parseInt(records.get("CAENroHasta").toString());
			String CAEVigencia = records.get("CAEVigencia").toString();
			String CAEVencimiento = records.get("CAEVencimiento").toString();
			int CAEAnulado = Integer.parseInt(records.get("CAEAnulado").toString());
			String CAEFchHoraSync = records.get("CAEFchHoraSync").toString();
			int CAEEspecial = Integer.parseInt(records.get("CAEEspecial").toString());
			int CausalTipoCAEEspecial = Integer.parseInt(records.get("CAECAusalTipo").toString());
			sql = "insert into CAE (EmpId, CAEId, CAENroAutorizacion,CAETipoCFE,CAESerie,"
					+ "CAENroDesde, CAENroHasta,CAEVigencia,CAEVencimiento, CAEAnulado, CAEFchHoraSync, CAEEspecial, CAECAusalTipo)"
					+ "values ('" + EmpId + "', '" + CAEId + "', '" + CAENroAutorizacion + "', '" + CAETipoCFE + "','" + CAESerie + "', '" + CAENroDesde + "','" + CAENroHasta + "','" + CAEVigencia + "','" + CAEVencimiento + "', '" + CAEAnulado + "', '" + CAEFchHoraSync + "','" + CAEEspecial + "', '" + CausalTipoCAEEspecial + "' )";
		}
		return sql;
	}

	public String AssemblyCAEInsert(ResultSet set) throws SQLException {
		Map<String, Object> records = resultSetToArrayList(set);
		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int CAEId = Integer.parseInt(records.get("CAEId").toString());
			BigDecimal CAENroAutorizacion = new BigDecimal(records.get("CAENroAutorizacion").toString());
			short CAETipoCFE = Short.parseShort(records.get("CAETipoCFE").toString());
			String CAESerie = records.get("CAESerie").toString();
			int CAENroDesde = Integer.parseInt(records.get("CAENroDesde").toString());
			int CAENroHasta = Integer.parseInt(records.get("CAENroHasta").toString());
			String CAEVigencia = records.get("CAEVigencia").toString();
			String CAEVencimiento = records.get("CAEVencimiento").toString();
			int CAEAnulado = Integer.parseInt(records.get("CAEAnulado").toString());
			String CAEFchHoraSync = records.get("CAEFchHoraSync").toString();
			int CAEEspecial = Integer.parseInt(records.get("CAEEspecial").toString());
			int CausalTipoCAEEspecial = Integer.parseInt(records.get("CAECAusalTipo").toString());
			sql = "insert into CAE (EmpId, CAEId, CAENroAutorizacion,CAETipoCFE,CAESerie,"
					+ "CAENroDesde, CAENroHasta,CAEVigencia,CAEVencimiento, CAEAnulado, CAEFchHoraSync, CAEEspecial, CAECAusalTipo)"
					+ "values ('" + EmpId + "', '" + CAEId + "', '" + CAENroAutorizacion + "', '" + CAETipoCFE + "','" + CAESerie + "', '" + CAENroDesde + "','" + CAENroHasta + "','" + CAEVigencia + "','" + CAEVencimiento + "', '" + CAEAnulado + "', '" + CAEFchHoraSync + "','" + CAEEspecial + "', '" + CausalTipoCAEEspecial + "' )";
		}
		return sql;
	}

	//ENSAMBLAR UNA QUERY INSERT DE LA TABLA CAE
	public String AssemblyCAEUpdate(Map<String, Object> records) throws SQLException {
		String sql = null;
		if (records.size() > 0) {
			//int EmpId = Integer.parseInt(records.get("EmpId").toString());
			//int CAEId = Integer.parseInt(records.get("CAEId").toString());
			BigDecimal CAENroAutorizacion = new BigDecimal(records.get("CAENroAutorizacion").toString());
			short CAETipoCFE = Short.parseShort(records.get("CAETipoCFE").toString());
			String CAESerie = records.get("CAESerie").toString();
			int CAENroDesde = Integer.parseInt(records.get("CAENroDesde").toString());
			int CAENroHasta = Integer.parseInt(records.get("CAENroHasta").toString());
			String CAEVigencia = records.get("CAEVigencia").toString();
			String CAEVencimiento = records.get("CAEVencimiento").toString();
			int CAEAnulado = Integer.parseInt(records.get("CAEAnulado").toString());
			String CAEFchHoraSync = records.get("CAEFchHoraSync").toString();
			int CAEEspecial = Integer.parseInt(records.get("CAEEspecial").toString());
			int CausalTipoCAEEspecial = Integer.parseInt(records.get("CAECAusalTipo").toString());
			sql = "Update CAE set CAENroAutorizacion = '" + CAENroAutorizacion + "', CAETipoCFE = '" + CAETipoCFE + "', "
					+ "CAESerie =  '" + CAESerie + "', CAENroDesde = '" + CAENroDesde + "', CAENroHasta = '" + CAENroHasta + "',"
					+ "CAEVigencia =  '" + CAEVigencia + "', CAEVencimiento = '" + CAEVencimiento + "', CAEAnulado =  '" + CAEAnulado + "',"
					+ "CAEFchHoraSync = '" + CAEFchHoraSync + "', CAEEspecial = '" + CAEEspecial + "', CAECAusalTipo = '" + CausalTipoCAEEspecial + "'";
		}
		return sql;
	}

	public String AssemblyCAEUpdate(ResultSet set) throws SQLException {
		Map<String, Object> records = resultSetToArrayList(set);
		String sql = null;
		if (records.size() > 0) {
			//int EmpId = Integer.parseInt(records.get("EmpId").toString());
			//int CAEId = Integer.parseInt(records.get("CAEId").toString());
			BigDecimal CAENroAutorizacion = new BigDecimal(records.get("CAENroAutorizacion").toString());
			short CAETipoCFE = Short.parseShort(records.get("CAETipoCFE").toString());
			String CAESerie = records.get("CAESerie").toString();
			int CAENroDesde = Integer.parseInt(records.get("CAENroDesde").toString());
			int CAENroHasta = Integer.parseInt(records.get("CAENroHasta").toString());
			String CAEVigencia = records.get("CAEVigencia").toString();
			String CAEVencimiento = records.get("CAEVencimiento").toString();
			int CAEAnulado = Integer.parseInt(records.get("CAEAnulado").toString());
			String CAEFchHoraSync = records.get("CAEFchHoraSync").toString();
			int CAEEspecial = Integer.parseInt(records.get("CAEEspecial").toString());
			int CausalTipoCAEEspecial = Integer.parseInt(records.get("CAECAusalTipo").toString());
			sql = "Update CAE set CAENroAutorizacion = '" + CAENroAutorizacion + "', CAETipoCFE = '" + CAETipoCFE + "', "
					+ "CAESerie =  '" + CAESerie + "', CAENroDesde = '" + CAENroDesde + "', CAENroHasta = '" + CAENroHasta + "',"
					+ "CAEVigencia =  '" + CAEVigencia + "', CAEVencimiento = '" + CAEVencimiento + "', CAEAnulado =  '" + CAEAnulado + "',"
					+ "CAEFchHoraSync = '" + CAEFchHoraSync + "', CAEEspecial = '" + CAEEspecial + "', CAECAusalTipo = '" + CausalTipoCAEEspecial + "'";
		}
		return sql;
	}

	//ENSAMBLAR UNA QUERY INSERT DE LA TABLA CAEParte
	public String AssemblyCAEParteInsert(Map<String, Object> records) throws SQLException {
		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int CAEId = Integer.parseInt(records.get("CAEId").toString());
			int CAEParteId = Integer.parseInt(records.get("CAEParteId").toString());
			int SucId = Integer.parseInt(records.get("SucId").toString());
			int CajaId = Integer.parseInt(records.get("CajaId").toString());
			int CAEParteNroDesde = Integer.parseInt(records.get("CAEParteNroDesde").toString());
			int CAEParteNroHasta = Integer.parseInt(records.get("CAEParteNroHasta").toString());
			String CAEParteFchHoraSync = records.get("CAEParteFchHoraSync").toString();
			int CAEParteUltAsignado = Integer.parseInt(records.get("CAEParteUltAsignado").toString());
			String CAEParteFchHoraUltSync = records.get("CAEParteFchHoraUltSync").toString();
			String CAEParteRowGUID = records.get("CAEParteRowGUID").toString();
			int CAEParteAnulado = Integer.parseInt(records.get("CAEParteAnulado").toString());
			int CAEParteAnuladoSync = Integer.parseInt(records.get("CAEParteAnuladoSync").toString());
			sql = "insert into CAEParte (EmpId, CAEId, CAEParteId,SucId,CajaId,"
					+ "CAEParteNroDesde, CAEParteNroHasta,CAEParteFchHoraSync,CAEParteUltAsignado, CAEParteFchHoraUltSync,"
					+ "CAEParteRowGUID,CAEParteAnulado ,CAEParteAnuladoSync)"
					+ "values ('" + EmpId + "', '" + CAEId + "',"
					+ "'" + CAEParteId + "', '" + SucId + "','" + CajaId + "',"
					+ "'" + CAEParteNroDesde + "','" + CAEParteNroHasta + "','" + CAEParteFchHoraSync + "',"
					+ "'" + CAEParteUltAsignado + "', '" + CAEParteFchHoraUltSync + "', '" + CAEParteRowGUID + "',"
					+ "'" + CAEParteAnulado + "', '" + CAEParteAnuladoSync + "')";
		}
		return sql;
	}

	public String AssemblyCAEParteInsert(ResultSet set) throws SQLException {
		Map<String, Object> records = resultSetToArrayList(set);
		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int CAEId = Integer.parseInt(records.get("CAEId").toString());
			int CAEParteId = Integer.parseInt(records.get("CAEParteId").toString());
			int SucId = Integer.parseInt(records.get("SucId").toString());
			int CajaId = Integer.parseInt(records.get("CajaId").toString());
			int CAEParteNroDesde = Integer.parseInt(records.get("CAEParteNroDesde").toString());
			int CAEParteNroHasta = Integer.parseInt(records.get("CAEParteNroHasta").toString());
			String CAEParteFchHoraSync = records.get("CAEParteFchHoraSync").toString();
			int CAEParteUltAsignado = Integer.parseInt(records.get("CAEParteUltAsignado").toString());
			String CAEParteFchHoraUltSync = records.get("CAEParteFchHoraUltSync").toString();
			String CAEParteRowGUID = records.get("CAEParteRowGUID").toString();
			int CAEParteAnulado = Integer.parseInt(records.get("CAEParteAnulado").toString());
			int CAEParteAnuladoSync = Integer.parseInt(records.get("CAEParteAnuladoSync").toString());
			sql = "insert into CAEParte (EmpId, CAEId, CAEParteId,SucId,CajaId,"
					+ "CAEParteNroDesde, CAEParteNroHasta,CAEParteFchHoraSync,CAEParteUltAsignado, CAEParteFchHoraUltSync,"
					+ "CAEParteRowGUID,CAEParteAnulado ,CAEParteAnuladoSync)"
					+ "values ('" + EmpId + "', '" + CAEId + "',"
					+ "'" + CAEParteId + "', '" + SucId + "','" + CajaId + "',"
					+ "'" + CAEParteNroDesde + "','" + CAEParteNroHasta + "','" + CAEParteFchHoraSync + "',"
					+ "'" + CAEParteUltAsignado + "', '" + CAEParteFchHoraUltSync + "', '" + CAEParteRowGUID + "',"
					+ "'" + CAEParteAnulado + "', '" + CAEParteAnuladoSync + "')";
		}
		return sql;
	}

	//ENSAMBLAR UNA QUERY UPDATE DE LA TABLA CAEParte
	public String AssemblyCAEParteUpdate(Map<String, Object> records) throws SQLException {
		String sql = null;
		if (records.size() > 0) {
			//int EmpId = Integer.parseInt(records.get("EmpId").toString());
			//int CAEId = Integer.parseInt(records.get("CAEId").toString());
			int CAEParteId = Integer.parseInt(records.get("CAEParteId").toString());
			int SucId = Integer.parseInt(records.get("SucId").toString());
			int CajaId = Integer.parseInt(records.get("CajaId").toString());
			int CAEParteNroDesde = Integer.parseInt(records.get("CAEParteNroDesde").toString());
			int CAEParteNroHasta = Integer.parseInt(records.get("CAEParteNroHasta").toString());
			String CAEParteFchHoraSync = records.get("CAEParteFchHoraSync").toString();
			int CAEParteUltAsignado = Integer.parseInt(records.get("CAEParteUltAsignado").toString());
			String CAEParteFchHoraUltSync = records.get("CAEParteFchHoraUltSync").toString();
			String CAEParteRowGUID = records.get("CAEParteRowGUID").toString();
			int CAEParteAnulado = Integer.parseInt(records.get("CAEParteAnulado").toString());
			int CAEParteAnuladoSync = Integer.parseInt(records.get("CAEParteAnuladoSync").toString());
			sql = "update CAEParte set CAEParteId = '" + CAEParteId + "', SucId = '" + SucId + "',CajaId = '" + CajaId + "',"
					+ "CAEParteNroDesde = '" + CAEParteNroDesde + "', CAEParteNroHasta = '" + CAEParteNroHasta + "',"
					+ "CAEParteFchHoraSync = '" + CAEParteFchHoraSync + "', CAEParteUltAsignado = '" + CAEParteUltAsignado + "',"
					+ "CAEParteFchHoraUltSync = '" + CAEParteFchHoraUltSync + "',CAEParteRowGUID = '" + CAEParteRowGUID + "',"
					+ "CAEParteAnulado = '" + CAEParteAnulado + "', CAEParteAnuladoSync = '" + CAEParteAnuladoSync + "'";
		}
		return sql;
	}

	public String AssemblyCAEParteUpdate(ResultSet set) throws SQLException {
		Map<String, Object> records = resultSetToArrayList(set);
		String sql = null;
		if (records.size() > 0) {
			//int EmpId = Integer.parseInt(records.get("EmpId").toString());
			//int CAEId = Integer.parseInt(records.get("CAEId").toString());
			int CAEParteId = Integer.parseInt(records.get("CAEParteId").toString());
			int SucId = Integer.parseInt(records.get("SucId").toString());
			int CajaId = Integer.parseInt(records.get("CajaId").toString());
			int CAEParteNroDesde = Integer.parseInt(records.get("CAEParteNroDesde").toString());
			int CAEParteNroHasta = Integer.parseInt(records.get("CAEParteNroHasta").toString());
			String CAEParteFchHoraSync = records.get("CAEParteFchHoraSync").toString();
			int CAEParteUltAsignado = Integer.parseInt(records.get("CAEParteUltAsignado").toString());
			String CAEParteFchHoraUltSync = records.get("CAEParteFchHoraUltSync").toString();
			String CAEParteRowGUID = records.get("CAEParteRowGUID").toString();
			int CAEParteAnulado = Integer.parseInt(records.get("CAEParteAnulado").toString());
			int CAEParteAnuladoSync = Integer.parseInt(records.get("CAEParteAnuladoSync").toString());
			sql = "update CAEParte set CAEParteId = '" + CAEParteId + "', SucId = '" + SucId + "',CajaId = '" + CajaId + "',"
					+ "CAEParteNroDesde = '" + CAEParteNroDesde + "', CAEParteNroHasta = '" + CAEParteNroHasta + "',"
					+ "CAEParteFchHoraSync = '" + CAEParteFchHoraSync + "', CAEParteUltAsignado = '" + CAEParteUltAsignado + "',"
					+ "CAEParteFchHoraUltSync = '" + CAEParteFchHoraUltSync + "',CAEParteRowGUID = '" + CAEParteRowGUID + "',"
					+ "CAEParteAnulado = '" + CAEParteAnulado + "', CAEParteAnuladoSync = '" + CAEParteAnuladoSync + "'";
		}
		return sql;
	}

	//ENSAMBLAR UNA QUERY INSERT DE LA TABLA CAEParteReservado
	public String AssemblyCAEParteReservadoInsert(Map<String, Object> records) throws SQLException {
		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int CAEId = Integer.parseInt(records.get("CAEId").toString());
			int CAEParteId = Integer.parseInt(records.get("CAEParteId").toString());
			int CAEParteResNroCAE = Integer.parseInt(records.get("CAEParteResNroCAE").toString());
			String CAEParteResFchHora = records.get("CAEParteResFchHora").toString();
			int CAEParteResCaduco = Integer.parseInt(records.get("CAEParteResCaduco").toString());
			String CAEParteResEstado = records.get("CAEParteResEstado").toString();
			sql = "insert into CAEParteReservado (EmpId, CAEId, CAEParteId,CAEParteResNroCAE,CAEParteResFchHora,"
					+ "CAEParteResCaduco, CAEParteResEstado)"
					+ "values ('" + EmpId + "', '" + CAEId + "',"
					+ "'" + CAEParteId + "', '" + CAEParteResNroCAE + "','" + CAEParteResFchHora + "',"
					+ "'" + CAEParteResCaduco + "','" + CAEParteResEstado + "')";
		}
		return sql;
	}

	public String AssemblyCAEParteReservadoInsert(ResultSet set) throws SQLException {
		Map<String, Object> records = resultSetToArrayList(set);
		String sql = null;
		if (records.size() > 0) {
			int EmpId = Integer.parseInt(records.get("EmpId").toString());
			int CAEId = Integer.parseInt(records.get("CAEId").toString());
			int CAEParteId = Integer.parseInt(records.get("CAEParteId").toString());
			int CAEParteResNroCAE = Integer.parseInt(records.get("CAEParteResNroCAE").toString());
			String CAEParteResFchHora = records.get("CAEParteResFchHora").toString();
			int CAEParteResCaduco = Integer.parseInt(records.get("CAEParteResCaduco").toString());
			String CAEParteResEstado = records.get("CAEParteResEstado").toString();
			sql = "insert into CAEParteReservado (EmpId, CAEId, CAEParteId,CAEParteResNroCAE,CAEParteResFchHora,"
					+ "CAEParteResCaduco, CAEParteResEstado)"
					+ "values ('" + EmpId + "', '" + CAEId + "',"
					+ "'" + CAEParteId + "', '" + CAEParteResNroCAE + "','" + CAEParteResFchHora + "',"
					+ "'" + CAEParteResCaduco + "','" + CAEParteResEstado + "')";
		}
		return sql;
	}

	//ENSAMBLAR UNA QUERY UPDATE DE LA TABLA CAEParteReservado
	public String AssemblyCAEParteReservadoUpdate(ResultSet set) throws SQLException {
		Map<String, Object> records = resultSetToArrayList(set);
		String sql = null;
		if (records.size() > 0) {
			//int EmpId = Integer.parseInt(records.get("EmpId").toString());
			//int CAEId = Integer.parseInt(records.get("CAEId").toString());
			//int CAEParteId = Integer.parseInt(records.get("CAEParteId").toString());
			//int CAEParteResNroCAE = Integer.parseInt(records.get("CAEParteResNroCAE").toString());
			String CAEParteResFchHora = records.get("CAEParteResFchHora").toString();
			int CAEParteResCaduco = Integer.parseInt(records.get("CAEParteResCaduco").toString());
			String CAEParteResEstado = records.get("CAEParteResEstado").toString();
			sql = "update CAEParteReservado set CAEParteResFchHora = '" + CAEParteResFchHora + "',"
					+ "CAEParteResCaduco = '" + CAEParteResCaduco + "', CAEParteResEstado = '" + CAEParteResEstado + "'";
		}
		return sql;
	}

	public String AssemblyCAEParteReservadoUpdate(Map<String, Object> records) throws SQLException {
		String sql = null;
		if (records.size() > 0) {
			//int EmpId = Integer.parseInt(records.get("EmpId").toString());
			//int CAEId = Integer.parseInt(records.get("CAEId").toString());
			//int CAEParteId = Integer.parseInt(records.get("CAEParteId").toString());
			//int CAEParteResNroCAE = Integer.parseInt(records.get("CAEParteResNroCAE").toString());
			String CAEParteResFchHora = records.get("CAEParteResFchHora").toString();
			int CAEParteResCaduco = Integer.parseInt(records.get("CAEParteResCaduco").toString());
			String CAEParteResEstado = records.get("CAEParteResEstado").toString();
			sql = "update CAEParteReservado set CAEParteResFchHora = '" + CAEParteResFchHora + "',"
					+ "CAEParteResCaduco = '" + CAEParteResCaduco + "', CAEParteResEstado = '" + CAEParteResEstado + "'";
		}
		return sql;
	}

	/////////UTILS////////////////////
	public byte[] readFileToBlob(String file) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			File f = new File(file);
			FileInputStream fis = new FileInputStream(f);
			byte[] buffer = new byte[1024];
			bos = new ByteArrayOutputStream();
			for (int len; (len = fis.read(buffer)) != -1;) {
				bos.write(buffer, 0, len);
			}
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		} catch (IOException e2) {
			System.err.println(e2.getMessage());
		}
		return bos != null ? bos.toByteArray() : null;
	}

	public Map<String, Object> resultSetToArrayList(ResultSet rs) throws SQLException {
		Map<String, Object> myMap = new HashMap<String, Object>();

		ResultSetMetaData meta = rs.getMetaData();
		while (rs.next()) {
			for (int i = 1; i <= meta.getColumnCount(); i++) {
				String key = meta.getColumnName(i);
				Object value = rs.getObject(key);
				myMap.put(key, value);
			}
		}
		return myMap;
	}

	public List<Map<String, Object>> resultSetToListMapArray(ResultSet rs) throws SQLException {
		List<Map<String, Object>> myList = new ArrayList<Map<String, Object>>();
		ResultSetMetaData meta = rs.getMetaData();
		while (rs.next()) {
			Map<String, Object> myMap = new HashMap<String, Object>();
			for (int i = 1; i <= meta.getColumnCount(); i++) {
				String key = meta.getColumnName(i);
				Object value = rs.getObject(key);
				myMap.put(key, value);
			}
			myList.add(myMap);
		}
		return myList;
	}

	public Date ConvertDate(String date) {
		Date dateResult = null;
		if (date.length() > 0) {

			String[] l = date.split(" ");
			String[] fecha = l[0].split("-");
			String[] time = l[1].split(":");
			int year, month, day, hour, minute, second = 0;
			year = Integer.parseInt(fecha[0]);
			month = Integer.parseInt(fecha[1]);
			day = Integer.parseInt(fecha[2]);
			hour = Integer.parseInt(time[0]);
			minute = Integer.parseInt(time[1]);
			second = Integer.parseInt(time[2]);
			dateResult = new Date(year, month, day, hour, minute, second);
		}
		return dateResult;
	}
}
