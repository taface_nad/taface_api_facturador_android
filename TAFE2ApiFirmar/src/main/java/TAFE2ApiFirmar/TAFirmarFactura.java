package TAFE2ApiFirmar;

import Negocio.ExecutorWS;
import Negocio.WSInterface.CAEParte.SDTCAEParteResponse206SDTCAEParteResponse206Item;
import Negocio.WSInterface.CAEParte.WSSincronizacionCAEParte0206ExecuteResponse;
import TAFACE2ApiEntidad.*;
import XML.*;
import XML.XMLFACTURA.enumTipoDeComprobanteCFE;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import TAFACESign.TAFACESign;
import XML.EnvioCFE.ReferenciaReferenciaIndGlobal;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import tangible.RefObject;

public class TAFirmarFactura {

	private TACAE objCAE;
	private TAEmpresa objEmpresa;
	private TAComprobante objComprobante;
	private TALogErrores objLogErrores;
	private TACertificado objCertificado;
	private TAConexion objConexion;
	private TAActualiza objActualiza;
	private TACertificadoDGI objCertificadoDGI;
	private String StringConexion = "";
	private NadMADato.TIPO_PROVEEDOR nProveedorConexion = NadMADato.TIPO_PROVEEDOR.tpSQLiteClient;
	private ExecutorWS executorWS;

	public final void setRutaConexion(String StrConex) {
		StringConexion = StrConex;
	}

	public final NadMADato.TIPO_PROVEEDOR getProveedorConexion() {
		return nProveedorConexion;
	}

	public final void setProveedorConexion(NadMADato.TIPO_PROVEEDOR value) {
		nProveedorConexion = value;
	}

	public final void setProveedorConexion(int value) {
		nProveedorConexion = NadMADato.TIPO_PROVEEDOR.forValue(value);
	}

	public final String FirmarFactura(String Factura, String CajaLicenseID, tangible.RefObject<String> Respuesta, tangible.RefObject<Boolean> Firmado, int SucId, int CajId, String VersionApi, String UrlServidor, short TimeOutSincronizacion, java.math.BigDecimal TopeUIFreeShopp, java.math.BigDecimal TopeUIPlaza, short DiasAntelacionCAEVenc, short DiasAntelacionCertVenc, byte PorcUscoAntelacionCAE, short MinDatosCajaSinSinc, short EncriptarComplFiscal, long CAENroAutorizacion, String CAESerie, long CAENroReservado, tangible.RefObject<Integer> CodError, java.util.Date FechaSistema, short NoValidarFechaFirma, int DiasdeFirmaRetroactivo, boolean ComprobarDiffFechaSistema) throws TAException, SQLException, Exception {

		String tempFirmarFactura = null;
		int EmpresaId = 0;
		int SucursalId = 0;
		int CajaId = 0;
		int CodErrorInt = 0;
		XMLFACTURA XMLFactura = new XMLFACTURA();
		XMLRESPUESTA XMLRespuesta = new XMLRESPUESTA();
		String XMLFirmado = "";
		EnvioCFE Doc = new EnvioCFE();
		executorWS = new ExecutorWS();
		executorWS.setURLServer(UrlServidor);
		String ErrorMsg = "";
		String WARNINGMSG = "";
		boolean YaExiste = false;
		boolean LecturaOk = false;
		boolean CAEOK = false;
		String EmpresaRUT = "";
		String Parametros = "";
		boolean FacturaOK = false;
		java.util.Date FechaHoraActual = new java.util.Date();
		Map<String, Object> drCAERecords = new HashMap<String, Object>();
		Map<String, Object> drCAEParteRecords = new HashMap<String, Object>();
		Map<String, Object> drCAEParteReservadoRecords = new HashMap<String, Object>();
		int IDDocTipoCFE = 0;
		java.math.BigDecimal TopeUI = new java.math.BigDecimal(0);
		boolean EmpresaEmiTickets = false;
		boolean EmpresaEmiFacturas = false;
		boolean EmpresaEmiRemitos = false;
		boolean EmpresaEmiResguardos = false;
		boolean EmpresaEmiFacturasExportacion = false;
		boolean EmpresaEmiRemitosExportacion = false;
		boolean EmpresaEmiCuentaAjena = false;
		String CteXMLRespuesta = "";
		int cdgDGISucur = 0;
		int CertId = 0;
		String CertDGIArchivo = "";
		int CertDGIid = 0;
		int NroCAEUtilizar = 0;
		java.util.Date FchHoraFirma = new java.util.Date();
		java.util.Date FechaSistemaServer = new java.util.Date(0);
		String XMLEntradaComprobar = "";

		try {
			Firmado.argValue = false;
			tempFirmarFactura = "";
			WARNINGMSG = "";
			XMLFactura = XMLFactura.LoadXML(Factura);
			EmpresaRUT = XMLFactura.getDGI().getEMIRUCEmisor();
			SucursalId = XMLFactura.getDATOSADICIONALES().getSUCURSALNRO();
			CajaId = XMLFactura.getDATOSADICIONALES().getCAJANRO();

			//Verificar sucursal y caja pasada por parametro con la que viene en el mensaje
			if (SucursalId != SucId) {
				throw new TAException("Nro de sucursal recibido en Datos Adicionales no coincide con nro de sucursal que se inicializo la Api - Datos Adicionales: " + SucursalId + " - Inicializa: " + SucId, 4155);
			}
			if (CajaId != CajId) {
				throw new TAException("Nro de caja recibido en Datos Adicionales no coincide con nro de caja que se inicializo la Api - Datos Adicionales: " + CajaId + " - Inicializa: " + CajId, 4156);
			}

			tangible.RefObject<Integer> tempRef_EmpresaId = new tangible.RefObject<Integer>(EmpresaId);
			ConectarVerificarBD(EmpresaRUT, SucursalId, CajaId, UrlServidor, TimeOutSincronizacion, VersionApi, tempRef_EmpresaId, SucId, CajId, false);
			EmpresaId = tempRef_EmpresaId.argValue;

			if (NoValidarFechaFirma == 0) //Parametro si valido o no fecha de Firma
			{
				//1)Si la fecha del Equipo es Igual a la fecha de sistema que me llega por parametro sigo de largo - caso normal
				Date fechaPC = new Date();
				fechaPC = new Date(fechaPC.getYear(), fechaPC.getMonth(), fechaPC.getDate());
				if (FechaSistema.compareTo(fechaPC) != 0 && ComprobarDiffFechaSistema) {
					//2)Si la fecha del equipo es distinta a la fecha del sistema voy al GW/SRV a buscar la fecha y la comparo con la del equipo local

					//3)Si me falla traer la fecha desde el GW/SRV continuo con la firma
					TASincronizarDatos sincFecha = new TASincronizarDatos(objConexion.NadDato());
					FechaSistemaServer = sincFecha.DescargarFechaSistema(UrlServidor, 5);
					if (FechaSistemaServer != null && (FechaSistemaServer != new Date(0))) {
						if (new Date(FechaSistemaServer.getYear(), FechaSistemaServer.getMonth(), FechaSistemaServer.getDate()).compareTo(FechaSistema) != 0) {
							//"Error la Fecha del Equipo local es distinta a la fecha de sistema"
							//throw new TAException("Error verifique la Fecha del equipo local, Fecha del equipo local: " + new GregorianCalendar().getTime() + ", Fecha de Sistema: " + FechaSistemaServer.toGMTString(), 4293);
							FechaHoraActual = new Date(FechaHoraActual.getYear(), FechaHoraActual.getMonth(), FechaHoraActual.getDate(), new Date().getHours(), new Date().getMinutes(), new Date().getSeconds());
							FechaSistema = new Date(FechaSistemaServer.getYear(), FechaSistemaServer.getMonth(), FechaSistemaServer.getDate(), new Date().getHours(), new Date().getMinutes(), new Date().getSeconds());
						}
					} else {
						//Si no obtengo la fecha del server no valido, ya que no se que dia es y ademas puede que el gateway
						//este offline hace dias justamente porque no llega al server entonces es imposible validar la fecha
						NoValidarFechaFirma = 1;
					}
				}

				if (NoValidarFechaFirma == 0) {
					//Valido la Fecha del Equipo Local contra la del "sistema"(la que me dio el GW)0
					java.util.Date FchComprobante = new Date(XMLFactura.getDGI().getIDDocFchEmis().getYear(), XMLFactura.getDGI().getIDDocFchEmis().getMonth(), XMLFactura.getDGI().getIDDocFchEmis().getDate(), FechaHoraActual.getHours(), FechaHoraActual.getMinutes(), FechaHoraActual.getSeconds());
					fechaPC = new Date();
					fechaPC.setDate(fechaPC.getDate() + 1);
					if (FchComprobante.compareTo(fechaPC) >= 0) //La fecha del comprobante debe ser menor igual a la fecha del sistema + 1
					{
						throw new TAException("Error, la Fecha de Emision del comprobante es mayor a la fecha actual mas un dia, debe ser menor o igual. Fecha del equipo: " + new Date().toGMTString() + " fecha del comprobante: " + new java.util.Date(tangible.DotNetToJavaDateHelper.datePart(java.util.Calendar.YEAR, XMLFactura.getDGI().getIDDocFchEmis()), tangible.DotNetToJavaDateHelper.datePart(java.util.Calendar.MONTH, XMLFactura.getDGI().getIDDocFchEmis()), tangible.DotNetToJavaDateHelper.datePart(java.util.Calendar.DAY_OF_MONTH, XMLFactura.getDGI().getIDDocFchEmis()), tangible.DotNetToJavaDateHelper.datePart(java.util.Calendar.HOUR_OF_DAY, XMLFactura.getDGI().getIDDocFchEmis()), tangible.DotNetToJavaDateHelper.datePart(java.util.Calendar.MINUTE, XMLFactura.getDGI().getIDDocFchEmis()), tangible.DotNetToJavaDateHelper.datePart(java.util.Calendar.SECOND, XMLFactura.getDGI().getIDDocFchEmis())), 4293);
					}
				}
			}

			//Si DiasdeFirmaRetroactivo distinto de cero tengo restriccion de fecha de firma hacia atras
			if (!(DiasdeFirmaRetroactivo == 0)) {
				//Si la fecha de emision del comprobante es menor a la fecha de hoy menos DiasdeFirmaRetroactivo muestro error
				if (XMLFactura.getDGI().getIDDocFchEmis().compareTo(new Date(FechaSistema.getYear(), FechaSistema.getMonth(), FechaSistema.getDate() - DiasdeFirmaRetroactivo)) < 0) {
					throw new TAException("Error, la Fecha de Emision del comprobante es menor a la fecha actual menos " + DiasdeFirmaRetroactivo + ", debe ser mayor o igual. Fecha de emision CFE: " + XMLFactura.getDGI().getIDDocFchEmis().toGMTString(), 4293);
				}
			}

			if (!objConexion.NadDato().VerificarIntegridadSQLite()) {
				if (VerificarCAEParteSincronizado(EmpresaId, SucId, CajaId, EmpresaRUT, UrlServidor, TimeOutSincronizacion)) {
					tangible.RefObject<Integer> tempRef_EmpresaId2 = new tangible.RefObject<Integer>(EmpresaId);
					RecontruccionBDCaliente(UrlServidor, TimeOutSincronizacion, EmpresaRUT, tempRef_EmpresaId2, SucId, CajaId, VersionApi);
					EmpresaId = tempRef_EmpresaId2.argValue;
				} else {
					TASincronizarDatos sinc = new TASincronizarDatos(objConexion.NadDato());
					try {
						sinc.EnviarLogErrorWS(EmpresaId, EmpresaRUT, SucursalId, CajaId, "La base de datos está corrupta y no logra reconstruirse ya que existen comprobantes sin sincronizar.", UrlServidor, TimeOutSincronizacion);
					} catch (RuntimeException ex) {
						objLogErrores.GrabarError(EmpresaId, SucursalId, CajaId, "La base de datos está corrupta y no logra reconstruirse ya que existen comprobantes sin sincronizar.", "LOG", 0, "INS", (short) 1, getNombreMetodo());
					}
					throw new TAException("Error en base de datos, existen comprobantes sin sincronizar y no se logró reconstruir.");
				}
			}

			//Valido que el UniqueId del Equipo(PC fisico) sea el mismo que el de la caja id en SucursalCaja
			LicenciaCajaValida(EmpresaRUT, SucId, CajId, CajaLicenseID);

			IDDocTipoCFE = XMLFactura.getDGI().getIDDocTipoCFE();
			tangible.RefObject<Integer> tempRef_EmpresaId3 = new tangible.RefObject<Integer>(EmpresaId);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiTickets = new tangible.RefObject<Boolean>(EmpresaEmiTickets);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiFacturas = new tangible.RefObject<Boolean>(EmpresaEmiFacturas);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiRemitos = new tangible.RefObject<Boolean>(EmpresaEmiRemitos);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiResguardos = new tangible.RefObject<Boolean>(EmpresaEmiResguardos);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiFacturasExportacion = new tangible.RefObject<Boolean>(EmpresaEmiFacturasExportacion);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiRemitosExportacion = new tangible.RefObject<Boolean>(EmpresaEmiRemitosExportacion);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiCuentaAjena = new tangible.RefObject<Boolean>(EmpresaEmiCuentaAjena);
			boolean tempVar = !(objEmpresa.EmpresaDameTipoEmisor(EmpresaRUT, tempRef_EmpresaId3, tempRef_EmpresaEmiTickets, tempRef_EmpresaEmiFacturas, tempRef_EmpresaEmiRemitos, tempRef_EmpresaEmiResguardos, tempRef_EmpresaEmiFacturasExportacion, tempRef_EmpresaEmiRemitosExportacion, tempRef_EmpresaEmiCuentaAjena));
			EmpresaId = tempRef_EmpresaId3.argValue;
			EmpresaEmiTickets = tempRef_EmpresaEmiTickets.argValue;
			EmpresaEmiFacturas = tempRef_EmpresaEmiFacturas.argValue;
			EmpresaEmiRemitos = tempRef_EmpresaEmiRemitos.argValue;
			EmpresaEmiResguardos = tempRef_EmpresaEmiResguardos.argValue;
			EmpresaEmiFacturasExportacion = tempRef_EmpresaEmiFacturasExportacion.argValue;
			EmpresaEmiRemitosExportacion = tempRef_EmpresaEmiRemitosExportacion.argValue;
			EmpresaEmiCuentaAjena = tempRef_EmpresaEmiCuentaAjena.argValue;
			if (tempVar) {
				throw new TAException("No se encontro la empresa con el RUT: " + EmpresaRUT + " - SucId: " + SucursalId + " - CajaId: " + CajaId, 3401);
			}
			if (!EmpresaEmiTickets && (IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicket.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaCred.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaCredContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaDeb.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaDebContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir eTicket o sus contingencias: " + EmpresaId + " - EmpRUT: " + EmpresaRUT + " - SucId: " + SucursalId + " - CajaId: " + CajaId, 4132);
			}
			if (!EmpresaEmiFacturas && (IDDocTipoCFE == enumTipoDeComprobanteCFE.eFactura.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCred.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDeb.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir eFacturas o sus contingencias: " + EmpresaId + " - EmpRUT: " + EmpresaRUT + " - SucId: " + SucursalId + " - CajaId: " + CajaId, 4133);
			}
			if (!EmpresaEmiResguardos && (IDDocTipoCFE == enumTipoDeComprobanteCFE.eResguardo.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eResguardoContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir eResguardo o eResguardoContingencia: " + EmpresaId + " - EmpRUT: " + EmpresaRUT + " - SucId: " + SucursalId + " - CajaId: " + CajaId, 4134);
			}
			if (!EmpresaEmiRemitos && (IDDocTipoCFE == enumTipoDeComprobanteCFE.eRemito.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eRemitoContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir eRemito o eRemitoContingencia: " + EmpresaId + " - EmpRUT: " + EmpresaRUT + " - SucId: " + SucursalId + " - CajaId: " + CajaId, 4135);
			}
			if (!EmpresaEmiFacturasExportacion && (IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaExportacion.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir eFacturasExportacion o sus contingencias: " + EmpresaId + " - EmpRUT: " + EmpresaRUT + " - SucId: " + SucursalId + " - CajaId: " + CajaId, 4136);
			}
			if (!EmpresaEmiRemitosExportacion && (IDDocTipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir eRemitoExportacion o eRemitoExportacionContingencia: " + EmpresaId + " - EmpRUT: " + EmpresaRUT + " - SucId: " + SucursalId + " - CajaId: " + CajaId, 4137);
			}
			if (!EmpresaEmiCuentaAjena && (IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaCredCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaCredCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaDebCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaDebCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjenaContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir comprobantes del tipo Cuenta Ajena: " + EmpresaId + " - EmpRUT: " + EmpresaRUT + " - SucId: " + SucursalId + " - CajaId: " + CajaId, 4290);
			}

			if (!(objEmpresa.ExisteSucursal(EmpresaId, SucId))) {
				throw new TAException("Sucursal no encontrada: EmpId:" + EmpresaId + " - EmpRUT: " + EmpresaRUT + " - SucId: " + SucursalId, 4176);
			}

			if (!(objEmpresa.ExisteCaja(EmpresaId, SucId, CajId))) {
				throw new TAException("Caja no encontrada: EmpId:" + EmpresaId + " - EmpRUT: " + EmpresaRUT + " - SucId: " + SucursalId + " - CajaId: " + CajaId, 4177);
			}

			tangible.RefObject<String> tempRef_CteXMLRespuesta = new tangible.RefObject<String>(CteXMLRespuesta);
			tangible.RefObject<String> tempRef_CteXMLEntrada = new tangible.RefObject<String>(XMLEntradaComprobar);
			//boolean tempVar2 = objConexion.NadDato().getRowCount(objComprobante.ConsComprobantes(EmpresaId, SucursalId, CajaId, (int) XMLFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALNRO(), XMLFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALSERIE(), XMLFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALTIPO(), XMLFactura.getDGI().getIDDocFchEmis(), tempRef_CteXMLRespuesta)) > 0;
			boolean tempVar2 = objComprobante.CargarDatosComprobanteComparar(EmpresaId, SucursalId, CajaId, IDDocTipoCFE, XMLFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALNRO(), XMLFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALSERIE(), XMLFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALTIPO(), tempRef_CteXMLEntrada, tempRef_CteXMLRespuesta);
			CteXMLRespuesta = tempRef_CteXMLRespuesta.argValue;
			XMLEntradaComprobar = tempRef_CteXMLEntrada.argValue;
			if (tempVar2) {
				if (VerificarExisteComprobanteAFirmar(XMLFactura, XMLEntradaComprobar)) {
					FacturaOK = true;
					Firmado.argValue = true;
					ErrorMsg = "";
					XMLRespuesta = XMLRespuesta.LoadXML(CteXMLRespuesta);
					YaExiste = true;
				} else if (objComprobante.CargarDatosCteDoc(EmpresaId, SucursalId, CajaId, IDDocTipoCFE, XMLFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALNRO(), XMLFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALSERIE(), XMLFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALTIPO())) {
					throw new TAException("Esta intentando firmar un CFE con datos de documento del que ya existen registros en la base de datos. DocOriginalNro: " + XMLFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALNRO());
				} else {
					YaExiste = false;
				}
			}

			if (!YaExiste) {
				//Transformo Factura
				tangible.RefObject<EnvioCFE> tempRef_Doc = new tangible.RefObject<EnvioCFE>(Doc);
				tangible.RefObject<Integer> tempRef_IDDocTipoCFE = new tangible.RefObject<Integer>(IDDocTipoCFE);
				tangible.RefObject<Boolean> tempRef_LecturaOk = new tangible.RefObject<Boolean>(LecturaOk);
				tangible.RefObject<String> tempRef_ErrorMsg = new tangible.RefObject<String>(ErrorMsg);

				FacturaSDTaFacturaElectronica(XMLFactura, tempRef_Doc, tempRef_IDDocTipoCFE, tempRef_LecturaOk, tempRef_ErrorMsg);

				Doc = tempRef_Doc.argValue;
				IDDocTipoCFE = tempRef_IDDocTipoCFE.argValue;
				LecturaOk = tempRef_LecturaOk.argValue;
				ErrorMsg = tempRef_ErrorMsg.argValue;
				if (LecturaOk) {
					if (!EmpresaRUT.equals(XMLFactura.getDGI().getEMIRUCEmisor()) && !(!String.valueOf(XMLFactura.getDGI().getEMIRUCEmisor()).trim().equals(""))) {
						LecturaOk = false;
						throw new TAException("RUT del Emisor no coincide con RUT de la empresa TAFACE o esta vacio - TAFACE: " + EmpresaRUT.toString() + " RECIBIDO: " + XMLFactura.getDGI().getEMIRUCEmisor());
					}
					if (!EmpresaRUT.equals(XMLFactura.getDATOSADICIONALES().getEMPRESARUC())) {
						LecturaOk = false;
						throw new TAException("RUT de Datos Adicionales no coincide con RUT de la empresa TAFACE - TAFACE: " + EmpresaRUT.toString() + " RECIBIDO: " + XMLFactura.getDATOSADICIONALES().getEMPRESARUC());
					}
					cdgDGISucur = objEmpresa.ConsSucursalCodEnDGI(EmpresaId, SucursalId);
					if (XMLFactura.getDGI().getEMICdgDGISucur() != cdgDGISucur) {
						LecturaOk = false;
						throw new TAException("Codigo DGI sucursal no coincide con la sucursal TAFACE - TAFACE: " + cdgDGISucur + " RECIBIDO: " + XMLFactura.getDGI().getEMICdgDGISucur());
					}
				}
				if (LecturaOk) {

					//Aca hago el switch si tiene o no reserva de CAE
					if (CAENroReservado == 0) {
						//Asigno CAE
						tangible.RefObject<Map<String, Object>> tempRef_drCAE = new RefObject<Map<String, Object>>(drCAERecords);
						tangible.RefObject<Map<String, Object>> tempRef_drCAEParte = new RefObject<Map<String, Object>>(drCAEParteRecords);
						tangible.RefObject<String> tempRef_WARNINGMSG = new RefObject<String>(WARNINGMSG);
						CAEOK = objCAE.TomarCAE(EmpresaId, SucursalId, CajaId, (short) IDDocTipoCFE, FechaHoraActual, DiasAntelacionCAEVenc, PorcUscoAntelacionCAE, tempRef_drCAE, tempRef_drCAEParte, tempRef_WARNINGMSG);
						drCAERecords = tempRef_drCAE.argValue;
						drCAEParteRecords = tempRef_drCAEParte.argValue;


						WARNINGMSG = tempRef_WARNINGMSG.argValue;

						if (CAEOK) {
							NroCAEUtilizar = Integer.parseInt(drCAEParteRecords.get(TACAE.CAEParte.getCAEParteUltAsignado()).toString());
							FchHoraFirma = FechaHoraActual;
						}
					} else {
						//Tomo el CAE que tenia reservado
						tangible.RefObject<Map<String, Object>> tempRef_drCAE2 = new RefObject<Map<String, Object>>(drCAERecords);
						tangible.RefObject<Map<String, Object>> tempRef_drCAEParte2 = new RefObject<Map<String, Object>>(drCAEParteRecords);
						tangible.RefObject<Map<String, Object>> tempRef_drCAEParteReservado = new tangible.RefObject<Map<String, Object>>(drCAEParteReservadoRecords);
						CAEOK = objCAE.TomarCAEReservado(EmpresaId, IDDocTipoCFE, CAENroAutorizacion, CAESerie, CAENroReservado, tempRef_drCAE2, tempRef_drCAEParte2, tempRef_drCAEParteReservado);
						drCAERecords = tempRef_drCAE2.argValue;
						drCAEParteRecords = tempRef_drCAEParte2.argValue;
						drCAEParteReservadoRecords = tempRef_drCAEParteReservado.argValue;


						if (CAEOK) {
							NroCAEUtilizar = (int) CAENroReservado;
							FchHoraFirma = new java.util.Date(FechaHoraActual.getYear(), FechaHoraActual.getMonth(), FechaHoraActual.getDate(), FechaHoraActual.getHours(), FechaHoraActual.getMinutes(), FechaHoraActual.getSeconds());
						}
					}

					if (CAEOK) {

						//Asigno Versión
						Doc.setversion(VersionDeSobre());
						//Completo datos del cae
						Doc.getCFE().get(0).getItem().getEncabezado().getIdDoc().setNro(String.valueOf(NroCAEUtilizar));
						Doc.getCFE().get(0).getItem().getEncabezado().getIdDoc().setSerie(drCAERecords.get(TACAE.CAE.getCAESerie()).toString());
						Doc.getCFE().get(0).getItem().getEncabezado().getIdDoc().setTipoCFE(EnvioCFE.IdDoc_TckTipoCFE.forValue(IDDocTipoCFE));
						Doc.getCFE().get(0).getItem().setTmstFirma(convertirFechaXMLSincronizar(FchHoraFirma));
						Doc.getCFE().get(0).getItem().getCAEData().setCAE_ID(drCAERecords.get(TACAE.CAE.getCAENroAutorizacion()).toString());
						Doc.getCFE().get(0).getItem().getCAEData().setDNro(drCAERecords.get(TACAE.CAE.getCAENroDesde()).toString());
						Doc.getCFE().get(0).getItem().getCAEData().setHNro(drCAERecords.get(TACAE.CAE.getCAENroHasta()).toString());
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
						Date CAEVencimiento = formatter.parse(drCAERecords.get(TACAE.CAE.getCAEVencimiento()).toString());
						Doc.getCFE().get(0).getItem().getCAEData().setFecVenc(CAEVencimiento);
						//si el tipo de CAE es especial añado la informacion a la Zona del CAE
						if (Integer.parseInt(drCAERecords.get(TACAE.CAE.getCAEEspecial()).toString()) != 0) {
							Doc.getCFE().get(0).getItem().getCAEData().setCAEEspecial(drCAERecords.get(TACAE.CAE.getCAEEspecial()).toString());
							Doc.getCFE().get(0).getItem().getCAEData().setCausalCAEEsp(drCAERecords.get(TACAE.CAE.getCAECAusalTipo()).toString());
						}

						//XML es la factura en la estructura correcta justo antes de firmar
						String DocXml = null;
						DocXml = Doc.getCFE().get(0).ToXML();

						//Si es del Tipo Cuenta Ajena y el parametro de encriptar complemento fiscal esta activado encripto antes de firmar el mismo
						if (EncriptarComplFiscal == 1 && (IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaCredCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaCredCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaDebCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaDebCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjenaContingencia.getValue())) {
							tangible.RefObject<String> tempRef_CertDGIArchivo = new tangible.RefObject<String>(CertDGIArchivo);
							tangible.RefObject<Integer> tempRef_CertDGIid = new tangible.RefObject<Integer>(CertDGIid);
							tangible.RefObject<String> tempRef_WARNINGMSG2 = new tangible.RefObject<String>(WARNINGMSG);
							boolean tempVar3 = !(objCertificadoDGI.CargarCertificadoDGI(EmpresaId, FechaHoraActual, DiasAntelacionCertVenc, tempRef_CertDGIArchivo, tempRef_CertDGIid, tempRef_WARNINGMSG2));
							CertDGIArchivo = tempRef_CertDGIArchivo.argValue;
							CertDGIid = tempRef_CertDGIid.argValue;
							WARNINGMSG = tempRef_WARNINGMSG2.argValue;
							if (tempVar3) {
								throw new TAException(getNombreMetodo() + " - No se pudo cargar certificado de DGI EmpId: " + EmpresaId, 3104);
							}
							/*
							 if (!(CryptoCF.EncriptarComplFiscal(CertDGIArchivo, "CERT_DGI_EFACTURA", DocXml, DocXml, ErrorMsg))) {
							 throw new TAException("No se pudo encriptar el complemento fiscal: EmpId:" + EmpresaId + " - EmpRUT: " + EmpresaRUT + " - SucId: " + SucursalId + " - CajaId: " + CajaId, 4293);
							 }*/
						}


						tangible.RefObject<Integer> tempRef_CertId = new tangible.RefObject<Integer>(CertId);
						tangible.RefObject<String> tempRef_XMLFirmado = new tangible.RefObject<String>(XMLFirmado);
						tangible.RefObject<String> tempRef_ErrorMsg2 = new tangible.RefObject<String>(ErrorMsg);
						tangible.RefObject<String> tempRef_WARNINGMSG3 = new tangible.RefObject<String>(WARNINGMSG);

						FirmarXML(EmpresaId, DocXml, "CFE", tempRef_CertId, FechaHoraActual, DiasAntelacionCertVenc, tempRef_XMLFirmado, tempRef_ErrorMsg2, tempRef_WARNINGMSG3);
						CertId = tempRef_CertId.argValue;
						XMLFirmado = tempRef_XMLFirmado.argValue;
						ErrorMsg = tempRef_ErrorMsg2.argValue;
						WARNINGMSG = tempRef_WARNINGMSG3.argValue;
					} else {
						ErrorMsg = "No existe CAE disponible";
					}
				}

				if (LecturaOk && CAEOK && !XMLFirmado.trim().equals("")) {

					Firmado.argValue = true;
					ErrorMsg = "";
					tangible.RefObject<String> tempRef_WARNINGMSG4 = new tangible.RefObject<String>(WARNINGMSG);
					objComprobante.VerificarExisteCompPorSincAlerta(EmpresaId, SucursalId, (short) CajaId, MinDatosCajaSinSinc, tempRef_WARNINGMSG4);
					WARNINGMSG = tempRef_WARNINGMSG4.argValue;
					String HashQR = "";

					XMLRespuesta.setWARNINGMSG(WARNINGMSG);
					XMLRespuesta.setFIRMADOOK((short) 1);
					//2017-05-02T12:47:52.3558396-03:00
					XMLRespuesta.setFIRMADOFCHHORA(convertirFechaXMLSincronizar(FchHoraFirma));
					XMLRespuesta.setCAENA(Long.parseLong(drCAERecords.get(TACAE.CAE.getCAENroAutorizacion()).toString()));
					XMLRespuesta.setCAENROINICIAL(Long.parseLong(drCAERecords.get(TACAE.CAE.getCAENroDesde()).toString()));
					XMLRespuesta.setCAENROFINAL(Long.parseLong(drCAERecords.get(TACAE.CAE.getCAENroHasta()).toString()));
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
					Date CAEVencimiento = formatter.parse(drCAERecords.get(TACAE.CAE.getCAEVencimiento()).toString());
					XMLRespuesta.setCAEVENCIMIENTO(CAEVencimiento);
					XMLRespuesta.setCAESERIE(drCAERecords.get(TACAE.CAE.getCAESerie()).toString());
					XMLRespuesta.setCAENRO(NroCAEUtilizar);
					XMLRespuesta.setCODSEGURIDAD(Obtener6DigitosHash(XMLFirmado));
					XMLRespuesta.setURLPARAVERIFICARTEXTO(objEmpresa.ObtenerUrlConsultaCFE(EmpresaId));
					XMLRespuesta.setRESOLUCIONIVA(objEmpresa.DameResolucionIVAEmp(String.valueOf(EmpresaId)));
					//Link a la página de DGI, con los siguientes parámetros:
					//		- Nº de RUC emisor.
					//		- Tipo de CFE.
					//		- Nº de comprobante (serie y Nº).
					//		- Monto : (Remito "0", Resguardo "A-C125", e-Remito de Exportación "A-C124" y resto "A-C130");
					//		- Fecha de firma del CFE (dd/mm/yyyy);
					//		- Código de seguridad del CFE (hash SHA-1)).
					HashQR = ObtenerHash(XMLFirmado);

					//https://www.efactura.dgi.gub.uy/consultaQR/cfe?ruc,tipoCFE,serie,nroCFE,monto,fecha,hash
					SimpleDateFormat formatterParam = new SimpleDateFormat("dd/MM/yyyy");
					String dateParam = formatterParam.format(FechaHoraActual);
					Parametros = EmpresaRUT.toString() + "," + (new Integer(IDDocTipoCFE)).toString().trim() + "," + drCAERecords.get(TACAE.CAE.getCAESerie()).toString() + "," + drCAEParteRecords.get(TACAE.CAEParte.getCAEParteUltAsignado().toString()) + "," + String.valueOf(XMLFactura.getDGI().getTOTMntTotal()).trim().replace(",", ".") + "," + dateParam + "," + HashQR;
					XMLRespuesta.setURLPARAVERIFICARQR("https://www.efactura.dgi.gub.uy/consultaQR/cfe?" + Parametros);

					//Asigno TopeUI Segun tipo de negocio freeshop = 3
					if (XMLFactura.getDATOSADICIONALES().getTIPODENEGOCIO() == 3) {
						TopeUI = TopeUIFreeShopp;
					} else {
						TopeUI = TopeUIPlaza;
					}

					//Firmado correctamente.
					tangible.RefObject<XMLRESPUESTA> tempRef_XMLRespuesta = new tangible.RefObject<XMLRESPUESTA>(XMLRespuesta);
					objComprobante.GrabarComprobante(EmpresaId, EmpresaRUT, SucursalId, CajaId, Integer.parseInt(drCAERecords.get(TACAE.CAE.getCAEId()).toString()), Integer.parseInt(drCAEParteRecords.get(TACAE.CAEParte.getCAEParteId()).toString()), FchHoraFirma, FchHoraFirma, CertId, CajaLicenseID, XMLFirmado, XMLFactura, VersionApi, TopeUI, tempRef_XMLRespuesta);
					XMLRespuesta = tempRef_XMLRespuesta.argValue;
					objEmpresa.GrabarCajaUniqueId(EmpresaId, SucursalId, CajaId, CajaLicenseID);
				} else {
					//Si el XML es vacio entonces no se pudo firmar
					Firmado.argValue = false;
					XMLRespuesta.setERRORMSG(ErrorMsg);
					XMLRespuesta.setFIRMADOOK((short) 0);
				}
			}



			if (Firmado.argValue) {
				//Obtengo los valores del CAE para la consulta de si existe el CFE en la BD
				long CAENroAutorizacionConsulta = XMLRespuesta.getCAENA();
				String CAESerieConsulta = XMLRespuesta.getCAESERIE();
				long CAENroConsulta = XMLRespuesta.getCAENRO();
				//Verifico que el CFE este grabado en la BD antes de emitir la respuesta
				if (objComprobante.ExisteComprobantes(EmpresaId, SucursalId, CajaId, XMLFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALNRO(), XMLFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALSERIE(), XMLFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALTIPO(), CAENroAutorizacionConsulta, CAESerieConsulta, CAENroConsulta)) {
					Respuesta.argValue = XMLRespuesta.ToXML();
				}
			} else {
				try {
					objLogErrores.GrabarError(EmpresaId, SucursalId, CajaId, ErrorMsg, "LOG", 0, "INS", (short) 1, getNombreMetodo());
				} catch (RuntimeException ex) {
					throw new TAException(ex.getMessage());
				}
			}

		} catch (TAException e) {
			Firmado.argValue = false;
			ErrorMsg = e.getMessage();
			try {
				objConexion.NadDato().FinTrans(false);
			} catch (RuntimeException ex) {
			}
			if (e.getClass() == TAException.class) {
				CodError.argValue = ((TAException) e).CodError;
				CodErrorInt = ((TAException) e).CodErrorInterno;
			} else {
				CodError.argValue = 8;
				CodErrorInt = 8;
			}
			XMLFirmado = null;
			XMLRespuesta.setERRORMSG(ErrorMsg);
			XMLRespuesta.setFIRMADOOK((short) 0);
			tempFirmarFactura = ErrorMsg;
			if (!((objLogErrores == null))) {
				try {
					objLogErrores.GrabarError(EmpresaId, SucursalId, CajaId, getNombreMetodo() + " - " + e.toString(), "LOG", CodErrorInt, "INS", (short) NumeroLineaDeError(), e.getMessage());
				} catch (RuntimeException ex) {
					throw new TAException(ex.getMessage());
				}
			}
		} finally {
			try {
				objConexion.Liberar(true);
				objConexion = null;
			} catch (RuntimeException ex) {
			}
		}
		return tempFirmarFactura;
	}

	public final String EnviarContingencia(String Factura, String CajaLicenseID, tangible.RefObject<Boolean> Firmado, int SucId, int CajId, String VersionApi, String UrlServidor, short TimeOutSincronizacion, java.math.BigDecimal TopeUIFreeShopp, java.math.BigDecimal TopeUIPlaza, short DiasAntelacionCertVenc, tangible.RefObject<Integer> CodError) throws TAException, SQLException, UnsupportedEncodingException, IOException, Exception {
		String tempEnviarContingencia = null;
		int EmpresaId = 0;
		int SucursalId = 0;
		int CajaId = 0;

		int CodErrorInt = 0;
		XMLFACTURA XMLFactura = new XMLFACTURA();
		XMLRESPUESTA XMLRespuesta = new XMLRESPUESTA();
		String XMLFirmado = null;
		EnvioCFE Doc = new EnvioCFE();
		ResultSet dtComprobante = null;
		String ErrorMsg = null;
		String WARNINGMSG = null;
		boolean YaExiste = false;
		boolean LecturaOk = false;
		String EmpresaRUT = null;
		String Parametros = "";
		boolean FacturaOK = false;
		java.util.Date FechaHoraActual = new java.util.Date();
		int IDDocTipoCFE = 0;
		java.math.BigDecimal TopeUI = new java.math.BigDecimal(0);
		boolean EmpresaEmiTickets = false;
		boolean EmpresaEmiFacturas = false;
		boolean EmpresaEmiRemitos = false;
		boolean EmpresaEmiResguardos = false;
		boolean EmpresaEmiFacturasExportacion = false;
		boolean EmpresaEmiRemitosExportacion = false;
		boolean EmpresaEmiCuentaAjena = false;
		String CteXMLRespuesta = "";
		int cdgDGISucur = 0;
		int CertId = 0;
		try {
			Firmado.argValue = false;
			tempEnviarContingencia = "";
			WARNINGMSG = "";
			XMLFactura = XMLFactura.LoadXML(Factura);
			EmpresaRUT = XMLFactura.getDGI().getEMIRUCEmisor();
			SucursalId = XMLFactura.getDATOSADICIONALES().getSUCURSALNRO();
			CajaId = XMLFactura.getDATOSADICIONALES().getCAJANRO();
			//Verificar sucursal y caja pasada por parametro con la que viene en el mensaje
			if (SucursalId != SucId) {
				throw new TAException("Nro de suscursal recibido en Datos Adicionales no coincide con nro de sucursal que se inicializo la Api - Datos Adicionales: " + SucursalId + " - Inicializa: " + SucId, 4157);
			}
			if (CajaId != CajId) {
				throw new TAException("Nro de caja recibido en Datos Adicionales no coincide con nro de caja que se inicializo la Api - Datos Adicionales: " + CajaId + " - Inicializa: " + CajId, 4158);
			}

			tangible.RefObject<Integer> tempRef_EmpresaId = new tangible.RefObject<Integer>(EmpresaId);
			ConectarVerificarBD(EmpresaRUT, SucursalId, CajaId, UrlServidor, TimeOutSincronizacion, VersionApi, tempRef_EmpresaId, SucId, CajId, false);
			EmpresaId = tempRef_EmpresaId.argValue;

			IDDocTipoCFE = XMLFactura.getDGI().getIDDocTipoCFE();
			tangible.RefObject<Integer> tempRef_EmpresaId2 = new tangible.RefObject<Integer>(EmpresaId);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiTickets = new tangible.RefObject<Boolean>(EmpresaEmiTickets);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiFacturas = new tangible.RefObject<Boolean>(EmpresaEmiFacturas);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiRemitos = new tangible.RefObject<Boolean>(EmpresaEmiRemitos);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiResguardos = new tangible.RefObject<Boolean>(EmpresaEmiResguardos);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiFacturasExportacion = new tangible.RefObject<Boolean>(EmpresaEmiFacturasExportacion);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiRemitosExportacion = new tangible.RefObject<Boolean>(EmpresaEmiRemitosExportacion);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiCuentaAjena = new tangible.RefObject<Boolean>(EmpresaEmiCuentaAjena);
			boolean tempVar = !(objEmpresa.EmpresaDameTipoEmisor(EmpresaRUT, tempRef_EmpresaId2, tempRef_EmpresaEmiTickets, tempRef_EmpresaEmiFacturas, tempRef_EmpresaEmiRemitos, tempRef_EmpresaEmiResguardos, tempRef_EmpresaEmiFacturasExportacion, tempRef_EmpresaEmiRemitosExportacion, tempRef_EmpresaEmiCuentaAjena));
			EmpresaId = tempRef_EmpresaId2.argValue;
			EmpresaEmiTickets = tempRef_EmpresaEmiTickets.argValue;
			EmpresaEmiFacturas = tempRef_EmpresaEmiFacturas.argValue;
			EmpresaEmiRemitos = tempRef_EmpresaEmiRemitos.argValue;
			EmpresaEmiResguardos = tempRef_EmpresaEmiResguardos.argValue;
			EmpresaEmiFacturasExportacion = tempRef_EmpresaEmiFacturasExportacion.argValue;
			EmpresaEmiRemitosExportacion = tempRef_EmpresaEmiRemitosExportacion.argValue;
			EmpresaEmiCuentaAjena = tempRef_EmpresaEmiCuentaAjena.argValue;
			if (tempVar) {
				throw new TAException("No se encontro la empresa con el RUT: " + EmpresaRUT + " - SucId: " + SucursalId + " - CajaId: " + CajaId, 3401);
			}
			if (!EmpresaEmiTickets && (IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicket.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaCred.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaCredContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaDeb.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaDebContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir eTicket o sus contingencias: " + EmpresaId + " - EmpRUT: " + EmpresaRUT + " - SucId: " + SucursalId + " - CajaId: " + CajaId, 4132);
			}
			if (!EmpresaEmiFacturas && (IDDocTipoCFE == enumTipoDeComprobanteCFE.eFactura.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCred.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDeb.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir eFacturas o sus contingencias: " + EmpresaId + " - EmpRUT: " + EmpresaRUT + " - SucId: " + SucursalId + " - CajaId: " + CajaId, 4133);
			}
			if (!EmpresaEmiResguardos && (IDDocTipoCFE == enumTipoDeComprobanteCFE.eResguardo.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eResguardoContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir eResguardo o eResguardoContingencia: " + EmpresaId + " - EmpRUT: " + EmpresaRUT + " - SucId: " + SucursalId + " - CajaId: " + CajaId, 4134);
			}
			if (!EmpresaEmiRemitos && (IDDocTipoCFE == enumTipoDeComprobanteCFE.eRemito.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eRemitoContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir eRemito o eRemitoContingencia: " + EmpresaId + " - EmpRUT: " + EmpresaRUT + " - SucId: " + SucursalId + " - CajaId: " + CajaId, 4135);
			}
			if (!EmpresaEmiFacturasExportacion && (IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaExportacion.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir eFacturasExportacion o sus contingencias: " + EmpresaId + " - EmpRUT: " + EmpresaRUT + " - SucId: " + SucursalId + " - CajaId: " + CajaId, 4136);
			}
			if (!EmpresaEmiRemitosExportacion && (IDDocTipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir eRemitoExportacion o eRemitoExportacionContingencia: " + EmpresaId + " - EmpRUT: " + EmpresaRUT + " - SucId: " + SucursalId + " - CajaId: " + CajaId, 4137);
			}
			if (!EmpresaEmiCuentaAjena && (IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaCredCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaCredCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaDebCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaDebCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjenaContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir comprobantes del tipo Cuenta Ajena: " + EmpresaId + " - EmpRUT: " + EmpresaRUT + " - SucId: " + SucursalId + " - CajaId: " + CajaId, 4291);
			}

			if (!(objEmpresa.ExisteSucursal(EmpresaId, SucId))) {
				throw new TAException("Sucursal no encontrada: EmpId:" + EmpresaId + " - EmpRUT: " + EmpresaRUT + " - SucId: " + SucursalId, 4176);
			}

			if (!(objEmpresa.ExisteCaja(EmpresaId, SucId, CajId))) {
				throw new TAException("Caja no encontrada: EmpId:" + EmpresaId + " - EmpRUT: " + EmpresaRUT + " - SucId: " + SucursalId + " - CajaId: " + CajaId, 4177);
			}

			tangible.RefObject<String> tempRef_CteXMLRespuesta = new tangible.RefObject<String>(CteXMLRespuesta);
			dtComprobante = objComprobante.ConsComprobantes(EmpresaId, SucursalId, CajaId, (int) XMLFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALNRO(), XMLFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALSERIE(), XMLFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALTIPO(), FechaHoraActual, tempRef_CteXMLRespuesta);
			CteXMLRespuesta = tempRef_CteXMLRespuesta.argValue;
			if (objConexion.NadDato().getRowCount(dtComprobante) > 0) {
				int i = 0;
				while (dtComprobante.next() && i < 1) {
					if (dtComprobante.getInt(TAComprobante.Comprobante.getCteCAENro()) == XMLFactura.getDATOSCONTINGENCIA().getNRO() && dtComprobante.getString(TAComprobante.Comprobante.getCteCAESerie()).equals(XMLFactura.getDATOSCONTINGENCIA().getSERIE()) && dtComprobante.getBigDecimal(TAComprobante.Comprobante.getCteCAENroAutorizacion()).longValueExact() == XMLFactura.getDATOSCONTINGENCIA().getCAENROAUTORIZACION() && dtComprobante.getShort(TAComprobante.Comprobante.getCteCAETipoCFE()) == XMLFactura.getDATOSCONTINGENCIA().getTIPOCFE()) {
						i++;
						FacturaOK = true;
						Firmado.argValue = true;
						ErrorMsg = "";
						YaExiste = true;
						return "";
					} else {
						throw new TAException("Error se intentó firmar un Comprobante con un Nro. de CFC ya utilizado anteriormente con otro Id de Comprobante: | TipoCFE=" + XMLFactura.getDATOSCONTINGENCIA().getTIPOCFE() + " | CAENA= " + XMLFactura.getDATOSCONTINGENCIA().getCAENROAUTORIZACION() + " | Serie= " + XMLFactura.getDATOSCONTINGENCIA().getSERIE() + " | Nro.=" + XMLFactura.getDATOSCONTINGENCIA().getNRO() + " | EmpId=" + EmpresaId + " | SucId=" + SucId + " | CajaId=" + CajId);
					}

				}
			} else {
				tangible.RefObject<String> tempRef_CteXMLRespuesta2 = new tangible.RefObject<String>(CteXMLRespuesta);
				boolean tempVar2 = objConexion.NadDato().getRowCount(objComprobante.ConsComprobantesCFC(EmpresaId, SucursalId, CajaId, XMLFactura.getDATOSCONTINGENCIA().getNRO(), XMLFactura.getDATOSCONTINGENCIA().getSERIE(), String.valueOf(XMLFactura.getDATOSCONTINGENCIA().getTIPOCFE()), tempRef_CteXMLRespuesta2)) > 0;
				CteXMLRespuesta = tempRef_CteXMLRespuesta2.argValue;
				if (tempVar2) {
					throw new TAException("Error se intentó firmar un Comprobante con un Nro. de CFC ya utilizado anteriormente con otro Id de Comprobante: | TipoCFE=" + XMLFactura.getDATOSCONTINGENCIA().getTIPOCFE() + " | CAENA= " + XMLFactura.getDATOSCONTINGENCIA().getCAENROAUTORIZACION() + " | Serie= " + XMLFactura.getDATOSCONTINGENCIA().getSERIE() + " | Nro.=" + XMLFactura.getDATOSCONTINGENCIA().getNRO() + " | EmpId=" + EmpresaId + " | SucId=" + SucId + " | CajaId=" + CajId);
				}
				YaExiste = false;
			}

			if (!YaExiste) {
				//Transformo Factura
				tangible.RefObject<EnvioCFE> tempRef_Doc = new tangible.RefObject<EnvioCFE>(Doc);
				tangible.RefObject<Integer> tempRef_IDDocTipoCFE = new tangible.RefObject<Integer>(IDDocTipoCFE);
				tangible.RefObject<Boolean> tempRef_LecturaOk = new tangible.RefObject<Boolean>(LecturaOk);
				tangible.RefObject<String> tempRef_ErrorMsg = new tangible.RefObject<String>(ErrorMsg);
				FacturaSDTaFacturaElectronica(XMLFactura, tempRef_Doc, tempRef_IDDocTipoCFE, tempRef_LecturaOk, tempRef_ErrorMsg);
				Doc = tempRef_Doc.argValue;
				IDDocTipoCFE = tempRef_IDDocTipoCFE.argValue;
				LecturaOk = tempRef_LecturaOk.argValue;
				ErrorMsg = tempRef_ErrorMsg.argValue;

				if (LecturaOk) {
					if (!(EmpresaRUT.equals(XMLFactura.getDGI().getEMIRUCEmisor())) && !(String.valueOf(XMLFactura.getDGI().getEMIRUCEmisor()).trim() == null || String.valueOf(XMLFactura.getDGI().getEMIRUCEmisor()).trim().length() == 0)) {
						LecturaOk = false;
						throw new TAException("RUT del Emisor no coincide con RUT de la empresa TAFACE o esta vacio - TAFACE: " + EmpresaRUT.toString() + " RECIBIDO: " + XMLFactura.getDGI().getEMIRUCEmisor());
					}
					if (!(EmpresaRUT.equals(XMLFactura.getDATOSADICIONALES().getEMPRESARUC()))) {
						LecturaOk = false;
						throw new TAException("RUT de Datos Adicionales no coincide con RUT de la empresa TAFACE - TAFACE: " + EmpresaRUT.toString() + " RECIBIDO: " + XMLFactura.getDATOSADICIONALES().getEMPRESARUC());
					}
					cdgDGISucur = objEmpresa.ConsSucursalCodEnDGI(EmpresaId, SucursalId);
					if (!(XMLFactura.getDGI().getEMICdgDGISucur() == cdgDGISucur)) {
						LecturaOk = false;
						throw new TAException("Codigo DGI sucursal no coincide con la sucursal TAFACE - TAFACE: " + cdgDGISucur + " RECIBIDO: " + XMLFactura.getDGI().getEMICdgDGISucur());
					}
				}
				if (LecturaOk) {

					//Asigno Versión
					Doc.setversion(VersionDeSobre());
					//Completo datos del cae
					Doc.getCFE().get(0).getItem().getEncabezado().getIdDoc().setNro(String.valueOf(XMLFactura.getDATOSCONTINGENCIA().getNRO()));
					Doc.getCFE().get(0).getItem().getEncabezado().getIdDoc().setSerie(XMLFactura.getDATOSCONTINGENCIA().getSERIE());
					Doc.getCFE().get(0).getItem().getEncabezado().getIdDoc().setTipoCFE(IDDocTipoCFE);
					Doc.getCFE().get(0).getItem().setTmstFirma(convertirFechaXMLSincronizar(FechaHoraActual));
					Doc.getCFE().get(0).getItem().getCAEData().setCAE_ID(String.valueOf(XMLFactura.getDATOSCONTINGENCIA().getCAENROAUTORIZACION()));
					Doc.getCFE().get(0).getItem().getCAEData().setDNro(String.valueOf(XMLFactura.getDATOSCONTINGENCIA().getCAENRODESDE()));
					Doc.getCFE().get(0).getItem().getCAEData().setHNro(String.valueOf(XMLFactura.getDATOSCONTINGENCIA().getCAENROHASTA()));
					Doc.getCFE().get(0).getItem().getCAEData().setFecVenc(XMLFactura.getDATOSCONTINGENCIA().getCAEVENCIMIENTO());

					Doc.setversion(VersionDeSobre());
					//Completo datos del cae

					//si el tipo de CAE es especial añado la informacion a la Zona del CAE
//					if (Integer.parseInt(drCAERecords.get(TACAE.CAE.getCAEEspecial()).toString()) != 0) {
//						Doc.getCFE()[0].getItem().getCAEData().setCAEEspecial(drCAERecords.get(TACAE.CAE.getCAEEspecial()).toString());
//						Doc.getCFE()[0].getItem().getCAEData().setCausalCAEEsp(drCAERecords.get(TACAE.CAE.getCAECAusalTipo()).toString());
//					}

					//XML es la factura en la estructura correcta justo antes de firmar
					String DocXml = null;
					DocXml = Doc.getCFE().get(0).ToXML();

					tangible.RefObject<Integer> tempRef_CertId = new tangible.RefObject<Integer>(CertId);
					tangible.RefObject<String> tempRef_XMLFirmado = new tangible.RefObject<String>(XMLFirmado);
					tangible.RefObject<String> tempRef_ErrorMsg2 = new tangible.RefObject<String>(ErrorMsg);
					tangible.RefObject<String> tempRef_WARNINGMSG = new tangible.RefObject<String>(WARNINGMSG);
					FirmarXML(EmpresaId, DocXml, "CFE", tempRef_CertId, FechaHoraActual, DiasAntelacionCertVenc, tempRef_XMLFirmado, tempRef_ErrorMsg2, tempRef_WARNINGMSG);
					CertId = tempRef_CertId.argValue;
					XMLFirmado = tempRef_XMLFirmado.argValue;
					ErrorMsg = tempRef_ErrorMsg2.argValue;
					WARNINGMSG = tempRef_WARNINGMSG.argValue;

				}

				if (LecturaOk && !XMLFirmado.trim().equals("")) {
					Firmado.argValue = true;
					ErrorMsg = "";

					XMLRespuesta.setWARNINGMSG(WARNINGMSG);
					XMLRespuesta.setFIRMADOOK((short) 1);
					XMLRespuesta.setFIRMADOFCHHORA(convertirFechaXMLSincronizar(FechaHoraActual));
					XMLRespuesta.setCAENA(XMLFactura.getDATOSCONTINGENCIA().getCAENROAUTORIZACION());
					XMLRespuesta.setCAENROINICIAL(XMLFactura.getDATOSCONTINGENCIA().getCAENRODESDE());
					XMLRespuesta.setCAENROFINAL(XMLFactura.getDATOSCONTINGENCIA().getCAENROHASTA());
					XMLRespuesta.setCAEVENCIMIENTO(XMLFactura.getDATOSCONTINGENCIA().getCAEVENCIMIENTO());
					XMLRespuesta.setCAESERIE(XMLFactura.getDATOSCONTINGENCIA().getSERIE());
					XMLRespuesta.setCAENRO(XMLFactura.getDATOSCONTINGENCIA().getNRO());
					XMLRespuesta.setCODSEGURIDAD(Obtener6DigitosHash(XMLFirmado));
					XMLRespuesta.setURLPARAVERIFICARTEXTO(objEmpresa.ObtenerUrlConsultaCFE(EmpresaId));
					XMLRespuesta.setRESOLUCIONIVA(objEmpresa.DameResolucionIVAEmp(String.valueOf(EmpresaId)));

					//Link a la página de DGI, con los siguientes parámetros:
					//		- Nº de RUC emisor.
					//		- Tipo de CFE.
					//		- Nº de comprobante (serie y Nº).
					//		- Monto total
					//		- Fecha de la firma
					//		- Código de seguridad del CFE.
					String HashQR = null;
					HashQR = ObtenerHash(XMLFirmado);


					SimpleDateFormat formatterParam = new SimpleDateFormat("dd/MM/yyyy");
					String dateParam = formatterParam.format(FechaHoraActual);
					//https://www.efactura.dgi.gub.uy/consultaQR/cfe?ruc,tipoCFE,serie,nroCFE,monto,fecha,hash
					Parametros = EmpresaRUT.toString() + "," + (new Integer(IDDocTipoCFE)).toString().trim() + "," + XMLFactura.getDATOSCONTINGENCIA().getSERIE() + "," + XMLFactura.getDATOSCONTINGENCIA().getNRO() + "," + String.valueOf(XMLFactura.getDGI().getTOTMntTotal()).trim().replace(",", ".") + "," + dateParam + "," + HashQR;
					XMLRespuesta.setURLPARAVERIFICARQR("https://www.efactura.dgi.gub.uy/consultaQR/cfe?" + Parametros);
					//Asigno TopeUI Segun tipo de negocio
					if (XMLFactura.getDATOSADICIONALES().getTIPODENEGOCIO() == 3) {
						TopeUI = TopeUIFreeShopp;
					} else {
						TopeUI = TopeUIPlaza;
					}
					//Firmado correctamente.
					tangible.RefObject<XMLRESPUESTA> tempRef_XMLRespuesta = new tangible.RefObject<XMLRESPUESTA>(XMLRespuesta);
					objComprobante.GrabarComprobante(EmpresaId, EmpresaRUT, SucursalId, CajaId, 0, 0, FechaHoraActual, FechaHoraActual, CertId, CajaLicenseID, XMLFirmado, XMLFactura, VersionApi, TopeUI, tempRef_XMLRespuesta);
					XMLRespuesta = tempRef_XMLRespuesta.argValue;
				}
			} else {
				//Si el XML es vacio entonces no se pudo firmar
				Firmado.argValue = false;
			}

			if (!Firmado.argValue) {
				try {
					objLogErrores.GrabarError(EmpresaId, SucursalId, CajaId, ErrorMsg, "LOG", 0, "INS", (short) 1, getNombreMetodo());
				} catch (RuntimeException ex) {
					throw new TAException(ex.getMessage());
				}
			}

		} catch (TAException e) {
			Firmado.argValue = false;
			ErrorMsg = e.getMessage();
			if (e.getClass() == TAException.class) {
				CodError.argValue = ((TAException) e).CodError;
				CodErrorInt = ((TAException) e).CodErrorInterno;
			} else {
				CodError.argValue = 8;
				CodErrorInt = 8;
			}
			XMLFirmado = null;
			tempEnviarContingencia = ErrorMsg;
			if (!((objLogErrores == null))) {
				try {
					objLogErrores.GrabarError(EmpresaId, SucursalId, CajaId, getNombreMetodo() + " - " + e.toString(), "LOG", CodErrorInt, "INS", (short) NumeroLineaDeError(), e.getStackTrace().toString());
				} catch (RuntimeException ex) {
					throw new TAException(ex.getMessage());
				}
			}
		} finally {
			try {
				objConexion.Liberar(true);
				objConexion = null;
			} catch (RuntimeException ex) {
			}
		}
		return tempEnviarContingencia;
	}

	private void FacturaSDTaFacturaElectronica(XMLFACTURA FactElec, tangible.RefObject<EnvioCFE> Doc, tangible.RefObject<Integer> IDDocTipoCFE, tangible.RefObject<Boolean> LecturaOk, tangible.RefObject<String> ErrorMsg) throws TAException {

		IDDocTipoCFE.argValue = (int) FactElec.getDGI().getIDDocTipoCFE();
		if ((IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eTicket.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eTicketNotaCred.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eTicketNotaDeb.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eTicketContingencia.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eTicketNotaCredContingencia.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eTicketNotaDebContingencia.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eTicketCuentaAjena.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eTicketNotaCredCuentaAjena.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eTicketNotaDebCuentaAjena.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eTicketCuentaAjenaContingencia.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eTicketNotaCredCuentaAjenaContingencia.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eTicketNotaDebCuentaAjenaContingencia.getValue())) {
			//e_tck
			XMLEntrada_a_DGI_E_Tck(FactElec, Doc, LecturaOk, ErrorMsg);
		} else if ((IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eFactura.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eFacturaNotaCred.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eFacturaNotaDeb.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eFacturaContingencia.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eFacturaNotaCredContingencia.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eFacturaNotaDebContingencia.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eFacturaCuentaAjena.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjena.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjena.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eFacturaCuentaAjenaContingencia.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjenaContingencia.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjenaContingencia.getValue())) {
			//e_Factura
			XMLEntrada_a_DGI_E_Fact(FactElec, Doc, LecturaOk, ErrorMsg);
		} else if ((IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eRemito.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eRemitoContingencia.getValue())) {
			//e_Remito
			XMLEntrada_a_DGI_E_Rem(FactElec, Doc, LecturaOk, ErrorMsg);
		} else if ((IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eResguardo.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eResguardoContingencia.getValue())) {
			//e_Resguardo
			XMLEntrada_a_DGI_E_Res(FactElec, Doc, LecturaOk, ErrorMsg);
		} else if ((IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eFacturaExportacion.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue())) {
			//e_Factura_Exportacion
			XMLEntrada_a_DGI_E_Fact_Exp(FactElec, Doc, LecturaOk, ErrorMsg);
		} else if ((IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eRemitoExportacion.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue())) {
			//e_Remito_Exportacion
			XMLEntrada_a_DGI_E_Rem_Exp(FactElec, Doc, LecturaOk, ErrorMsg);
		} else if ((IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eBoleta.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()) || (IDDocTipoCFE.argValue == enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue())) {
			//e_Boleta
			XMLEntrada_a_DGI_E_Boleta(FactElec, Doc, LecturaOk, ErrorMsg);
		}

		if (!LecturaOk.argValue) {
			throw new TAException(ErrorMsg.argValue, 5008);
		}
	}

	private void XMLEntrada_a_DGI_E_Tck(XMLFACTURA FactElec, tangible.RefObject<EnvioCFE> EnvCFE, tangible.RefObject<Boolean> LecturaOk, tangible.RefObject<String> ErrorMsg) {

		EnvioCFE.CFEDefTypeETck ItemCFE = EnvCFE.argValue.new CFEDefTypeETck();
		EnvioCFE.CFEDefType CFE = EnvCFE.argValue.new CFEDefType();
		EnvioCFE.Emisor Emisor = EnvCFE.argValue.new Emisor();
		EnvioCFE.Receptor_Tck Receptor = EnvCFE.argValue.new Receptor_Tck();
		EnvioCFE.CFEDefTypeETckEncabezado Encabezado = EnvCFE.argValue.new CFEDefTypeETckEncabezado();
		EnvioCFE.Totales Totales = EnvCFE.argValue.new Totales();
		EnvioCFE.Item_Det_Fact Detalle = EnvCFE.argValue.new Item_Det_Fact();
		EnvioCFE.CAEDataType CAEData = EnvCFE.argValue.new CAEDataType();
		EnvioCFE.IdDoc_Tck IdDoc = EnvCFE.argValue.new IdDoc_Tck();
		EnvioCFE.Compl_Fiscal_DataType ComplementoFiscalData = EnvCFE.argValue.new Compl_Fiscal_DataType();
		EnvioCFE.Compl_FiscalType ComplementoFiscal = EnvCFE.argValue.new Compl_FiscalType();

		try {
			LecturaOk.argValue = false;

			//A - Encabezado
			ItemCFE.setEncabezado(Encabezado);

			//Identificacion del comprobante
			ItemCFE.getEncabezado().setIdDoc(IdDoc);
			ItemCFE.getEncabezado().getIdDoc().setTipoCFE(EnvioCFE.IdDoc_TckTipoCFE.forValue(FactElec.getDGI().getIDDocTipoCFE()));
			ItemCFE.getEncabezado().getIdDoc().setFchEmis(FactElec.getDGI().getIDDocFchEmis());
			if (FactElec.getDGI().getIDDocPeriodoDesde() != null && FactElec.getDGI().getIDDocPeriodoDesde() != new Date(0)) {
				ItemCFE.getEncabezado().getIdDoc().setPeriodoDesde(FactElec.getDGI().getIDDocPeriodoDesde());
				ItemCFE.getEncabezado().getIdDoc().setPeriodoDesdeSpecified(true);
			}
			if (FactElec.getDGI().getIDDocPeriodoHasta() != null && FactElec.getDGI().getIDDocPeriodoHasta() != new Date(0)) {
				ItemCFE.getEncabezado().getIdDoc().setPeriodoHasta(FactElec.getDGI().getIDDocPeriodoHasta());
				ItemCFE.getEncabezado().getIdDoc().setPeriodoHastaSpecified(true);
			}
			if (FactElec.getDGI().getIDDocMntBruto() == 1) {
				ItemCFE.getEncabezado().getIdDoc().setMntBruto(EnvioCFE.IdDoc_TckMntBruto.forValue(FactElec.getDGI().getIDDocMntBruto()));
				ItemCFE.getEncabezado().getIdDoc().setMntBrutoSpecified(true);
			}
			ItemCFE.getEncabezado().getIdDoc().setFmaPago(EnvioCFE.IdDoc_TckFmaPago.forValue(FactElec.getDGI().getIDDocFmaPago()));
			if (FactElec.getDGI().getIDDocFchVenc() != null && FactElec.getDGI().getIDDocFchVenc().compareTo(new Date(0)) != 0) {
				ItemCFE.getEncabezado().getIdDoc().setFchaVenc(FactElec.getDGI().getIDDocFchVenc());
				ItemCFE.getEncabezado().getIdDoc().setFchVencSpecified(true);
			}
			ItemCFE.getEncabezado().getIdDoc().setInfoAdicionalDoc(FactElec.getDGI().getIDDocInfoAdicionalDoc());

			if (FactElec.getDGI().getIDDocIVAalDia() == 1) {
				ItemCFE.getEncabezado().getIdDoc().setIVAalDia(String.valueOf(FactElec.getDGI().getIDDocIVAalDia()));
			}
			if (FactElec.getDGI().getIDDocSecProf() == 1) {
				ItemCFE.getEncabezado().getIdDoc().setSecProf(String.valueOf(FactElec.getDGI().getIDDocSecProf()));
			}

			//Emisor
			ItemCFE.getEncabezado().setEmisor(Emisor);
			ItemCFE.getEncabezado().getEmisor().setRUCEmisor(FactElec.getDGI().getEMIRUCEmisor());
			ItemCFE.getEncabezado().getEmisor().setRznSoc(FactElec.getDGI().getEMIRznSoc());
			ItemCFE.getEncabezado().getEmisor().setNomComercial(FactElec.getDGI().getEMINomComercial());
			ItemCFE.getEncabezado().getEmisor().setGiroEmis(FactElec.getDGI().getEMIGiroEmis());

			if (!((FactElec.getDGI().getEmiTelefonos() == null))) {
				List<XMLFACTURA.clsDGI.EmiTelefonos.EMITelefono> telef = FactElec.getDGI().getEmiTelefonos().getEMITelefono();
				List<String> ListTelefonos = new ArrayList<String>();
				for (int i = 0; i < telef.size(); i++) {
					ListTelefonos.add(telef.get(i).getEMITelefono());
				}
				ItemCFE.getEncabezado().getEmisor().setTelefono(ListTelefonos);
			}
			ItemCFE.getEncabezado().getEmisor().setCorreoEmisor(FactElec.getDGI().getEMICorreoEmisor());
			ItemCFE.getEncabezado().getEmisor().setEmiSucursal(FactElec.getDGI().getEMIEmiSucursal());
			ItemCFE.getEncabezado().getEmisor().setCdgDGISucur(String.valueOf(FactElec.getDGI().getEMICdgDGISucur()));
			ItemCFE.getEncabezado().getEmisor().setDomFiscal(FactElec.getDGI().getEMIDomFiscal());
			ItemCFE.getEncabezado().getEmisor().setCiudad(FactElec.getDGI().getEMICiudad());
			ItemCFE.getEncabezado().getEmisor().setDepartamento(FactElec.getDGI().getEMIDepartamento());
			ItemCFE.getEncabezado().getEmisor().setInfoAdicionalEmisor(FactElec.getDGI().getEMIInfoAdicionalEmisor());

			//Receptor
			if (FactElec.getDGI().getRECTipoDocRecep() != 0) {
				//Si existe un receptor, tiene que existir TipoDocRecep, Se deja dentro del IF, ya que no siempre existe el receptor
				ItemCFE.getEncabezado().setReceptor(Receptor);
				ItemCFE.getEncabezado().getReceptor().setTipoDocRecep(EnvioCFE.DocType.forValue(FactElec.getDGI().getRECTipoDocRecep()));
				ItemCFE.getEncabezado().getReceptor().setTipoDocRecepSpecified(true);
				if (!FactElec.getDGI().getRECCodPaisRecep().equals("")) {
					ItemCFE.getEncabezado().getReceptor().setCodPaisRecep(FactElec.getDGI().getRECCodPaisRecep());
					ItemCFE.getEncabezado().getReceptor().setCodPaisRecepSpecified(true);
				}
				if (FactElec.getDGI().getRECTipoDocRecep() == 4 || FactElec.getDGI().getRECTipoDocRecep() == 5 || FactElec.getDGI().getRECTipoDocRecep() == 6 || FactElec.getDGI().getRECTipoDocRecep() == 7) {
					ItemCFE.getEncabezado().getReceptor().setDocRecepExt(FactElec.getDGI().getRECDocRecepExtranjero());
				} else {
					ItemCFE.getEncabezado().getReceptor().setDocRecep(FactElec.getDGI().getRECDocRecep());
				}
				ItemCFE.getEncabezado().getReceptor().setRznSocRecep(FactElec.getDGI().getRECRznSocRecep());
				ItemCFE.getEncabezado().getReceptor().setDirRecep(FactElec.getDGI().getRECDirRecep());
				ItemCFE.getEncabezado().getReceptor().setCiudadRecep(FactElec.getDGI().getRECCiudadRecep());
				ItemCFE.getEncabezado().getReceptor().setDeptoRecep(FactElec.getDGI().getRECDeptoRecep());
				ItemCFE.getEncabezado().getReceptor().setPaisRecep(FactElec.getDGI().getRECPaisRecep());
				if (FactElec.getDGI().getRECCP() != 0) {
					ItemCFE.getEncabezado().getReceptor().setCP(String.valueOf(FactElec.getDGI().getRECCP()));
				}
				ItemCFE.getEncabezado().getReceptor().setInfoAdicional(FactElec.getDGI().getRECInfoAdicional());
				ItemCFE.getEncabezado().getReceptor().setLugarDestEnt(FactElec.getDGI().getRECLugarDestinoEntrega());
				ItemCFE.getEncabezado().getReceptor().setCompraID(FactElec.getDGI().getRECNroOrdenCompra());
			}

			//Totales
			ItemCFE.getEncabezado().setTotales(Totales);
			ItemCFE.getEncabezado().getTotales().setTpoMoneda(FactElec.getDGI().getTOTTpoMoneda());
			if (FactElec.getDGI().getTOTTpoCambio() != 0) {
				ItemCFE.getEncabezado().getTotales().setTpoCambio(new BigDecimal(FactElec.getDGI().getTOTTpoCambio()));
				ItemCFE.getEncabezado().getTotales().setTpoCambioSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntNoGrv() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntNoGrv(new BigDecimal(FactElec.getDGI().getTOTMntNoGrv()));
				ItemCFE.getEncabezado().getTotales().setMntNoGrvSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntExpoyAsim() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntExpoyAsim(new BigDecimal(FactElec.getDGI().getTOTMntExpoyAsim()));
				ItemCFE.getEncabezado().getTotales().setMntExpoyAsimSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntImpuestoPerc() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntImpuestoPerc(new BigDecimal(FactElec.getDGI().getTOTMntImpuestoPerc()));
				ItemCFE.getEncabezado().getTotales().setMntImpuestoPercSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntIVaenSusp() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntIVaenSusp(new BigDecimal(FactElec.getDGI().getTOTMntIVaenSusp()));
				ItemCFE.getEncabezado().getTotales().setMntIVaenSuspSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntNetoIvaTasaMin() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntNetoIvaTasaMin(new BigDecimal(FactElec.getDGI().getTOTMntNetoIvaTasaMin()));
				ItemCFE.getEncabezado().getTotales().setMntNetoIvaTasaMinSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntNetoIVATasaBasica() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntNetoIVATasaBasica(new BigDecimal(FactElec.getDGI().getTOTMntNetoIVATasaBasica()));
				ItemCFE.getEncabezado().getTotales().setMntNetoIVATasaBasicaSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntNetoIVAOtra() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntNetoIVAOtra(new BigDecimal(FactElec.getDGI().getTOTMntNetoIVAOtra()));
				ItemCFE.getEncabezado().getTotales().setMntNetoIVAOtraSpecified(true);
			}
			if (FactElec.getDGI().getTOTIVATasaMin() != 0) {
				ItemCFE.getEncabezado().getTotales().setIVATasaMin(new BigDecimal(FactElec.getDGI().getTOTIVATasaMin()));
				ItemCFE.getEncabezado().getTotales().setIVATasaMinSpecified(true);
			}
			if (FactElec.getDGI().getTOTIVATasaBasica() != 0) {
				ItemCFE.getEncabezado().getTotales().setIVATasaBasica(new BigDecimal(FactElec.getDGI().getTOTIVATasaBasica()));
				ItemCFE.getEncabezado().getTotales().setIVATasaBasicaSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntIVATasaMin() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntIVATasaMin(new BigDecimal(FactElec.getDGI().getTOTMntIVATasaMin()));
				ItemCFE.getEncabezado().getTotales().setMntIVATasaMinSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntIVATasaBasica() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntIVATasaBasica(new BigDecimal(FactElec.getDGI().getTOTMntIVATasaBasica()));
				ItemCFE.getEncabezado().getTotales().setMntIVATasaBasicaSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntIVAOtra() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntIVAOtra(new BigDecimal(FactElec.getDGI().getTOTMntIVAOtra()));
				ItemCFE.getEncabezado().getTotales().setMntIVAOtraSpecified(true);
			}
			ItemCFE.getEncabezado().getTotales().setMntTotal(new BigDecimal(FactElec.getDGI().getTOTMntTotal()));
			if (FactElec.getDGI().getTOTMntTotRetenido() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntTotRetenido(new BigDecimal(FactElec.getDGI().getTOTMntTotRetenido()));
				ItemCFE.getEncabezado().getTotales().setMntTotRetenidoSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntTotCredFisc() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntTotCredFisc(new BigDecimal(FactElec.getDGI().getTOTMntTotCredFisc()));
				ItemCFE.getEncabezado().getTotales().setMntTotCredFiscSpecified(true);
			}
			 ItemCFE.getEncabezado().getTotales().setCantLinDet(String.valueOf(FactElec.getDGI().getTOTCantLinDet()));
            if (!((FactElec.getDGI().getTOTRetencPerceps() == null))) {
                List<EnvioCFE.TotalesRetencPercep> ListRetencPerceps = new ArrayList<EnvioCFE.TotalesRetencPercep>();
                for (int i = 0; i < FactElec.getDGI().getTOTRetencPerceps().getOTRetencPercep().size(); i++) {
                    EnvioCFE.TotalesRetencPercep NewRetPrec = EnvCFE.argValue.new TotalesRetencPercep();
                    NewRetPrec.setCodRet(FactElec.getDGI().getTOTRetencPerceps().getOTRetencPercep().get(i).getTOTCodRet());
                    NewRetPrec.setValRetPerc(new BigDecimal(FactElec.getDGI().getTOTRetencPerceps().getOTRetencPercep().get(i).getTOTValRetPerc()));
                    ListRetencPerceps.add(NewRetPrec);
                }
                ItemCFE.getEncabezado().getTotales().setRetencPercep(ListRetencPerceps);
            }
            if (FactElec.getDGI().getTOTMontoNF() != 0) {
                ItemCFE.getEncabezado().getTotales().setMontoNF(new BigDecimal(FactElec.getDGI().getTOTMontoNF()));
                ItemCFE.getEncabezado().getTotales().setMontoNFSpecified(true);
            }
            ItemCFE.getEncabezado().getTotales().setMntPagar(new BigDecimal(FactElec.getDGI().getTOTMntPagar()));

			//B - Detalle de productos o servicios
            List<EnvioCFE.Item_Det_Fact.Item> DetalleItemsList = new ArrayList<EnvioCFE.Item_Det_Fact.Item>();
            int cont = 0;
            for (XMLFACTURA.clsDGI.Detalle.DETALLE DetFact : FactElec.getDGI().getDetalles().getDetalle()) {
                List<EnvioCFE.Item_Det_FactSubDescuento> ListSubDesc;
                List<EnvioCFE.Item_Det_FactSubRecargo> ListSubRec;
                List<EnvioCFE.RetPerc> ListRetPerc;
                List<EnvioCFE.Item_Det_FactCodItem> ListCodItem;

                EnvioCFE.Item_Det_Fact.Item DetalleItem = Detalle.new Item();
                DetalleItem.setNroLinDet(String.valueOf(DetFact.getDETNroLinDet()));
                if (!((DetFact.getDETCodItems() == null))) {
                    ListCodItem = new ArrayList<EnvioCFE.Item_Det_FactCodItem>();
                    for (int j = 0; j < DetFact.getDETCodItems().getDETCodItem().size(); j++) {
                        EnvioCFE.Item_Det_FactCodItem NewCodItem = EnvCFE.argValue.new Item_Det_FactCodItem();
                        NewCodItem.setCod(DetFact.getDETCodItems().getDETCodItem().get(j).getDETCod());
                        NewCodItem.setTpoCod(DetFact.getDETCodItems().getDETCodItem().get(j).getDETTpoCod());
                        ListCodItem.add(NewCodItem);
                    }
                    DetalleItem.setCodItem(ListCodItem);
                }
                if (DetFact.getDETIndFact() != 0) {
                    DetalleItem.setIndFact(EnvioCFE.Item_Det_FactIndFact.forValue(DetFact.getDETIndFact()));
                }
                if (!((DetFact.getDETIndAgenteResp() == null)) && !DetFact.getDETIndAgenteResp().trim().equals("")) {
                    DetalleItem.setIndAgenteResp(DetFact.getDETIndAgenteResp());
                    DetalleItem.setIndAgenteRespSpecified(true);
                }
				DetalleItem.setNomItem(DetFact.getDETNomItem());
				if (!((DetFact.getDETDscItem() == null)) && !DetFact.getDETDscItem().trim().equals("")) {
					DetalleItem.setDscItem(DetFact.getDETDscItem());
				}
				DetalleItem.setCantidad(new BigDecimal(DetFact.getDETCantidad()));
				DetalleItem.setUniMed(String.valueOf(DetFact.getDETUniMed()));
				DetalleItem.setPrecioUnitario(new BigDecimal(DetFact.getDETPrecioUnitario()).setScale(6, RoundingMode.HALF_EVEN));
				if (DetFact.getDETDescuentoPct() != 0) {
					DetalleItem.setDescuentoPct(new BigDecimal(DetFact.getDETDescuentoPct()));
					DetalleItem.setDescuentoPctSpecified(true);
				}
				if (DetFact.getDETDescuentoMonto() != 0) {
					DetalleItem.setDescuentoMonto(new BigDecimal(DetFact.getDETDescuentoMonto()));
					DetalleItem.setDescuentoMontoSpecified(true);
				}
				if (!((DetFact.getDETSubDescuentos() == null))) {
                    ListSubDesc = new ArrayList<EnvioCFE.Item_Det_FactSubDescuento>();
                    for (int k = 0; k < DetFact.getDETSubDescuentos().getDETSubDescuento().size(); k++) {
                        EnvioCFE.Item_Det_FactSubDescuento NewSubDesc = EnvCFE.argValue.new Item_Det_FactSubDescuento();
                        NewSubDesc.setDescTipo(String.valueOf(DetFact.getDETSubDescuentos().getDETSubDescuento().get(k).getDETDescTipo()));
                        NewSubDesc.setDescVal(new BigDecimal(DetFact.getDETSubDescuentos().getDETSubDescuento().get(k).getDETDescVal()));
                        ListSubDesc.add(NewSubDesc);
                    }
                    DetalleItem.setSubDescuento(ListSubDesc);
                }
				if (DetFact.getDETRecargoPct() != 0) {
                    DetalleItem.setRecargoPct(new BigDecimal(DetFact.getDETRecargoPct()));
                    DetalleItem.setRecargoPctSpecified(true);
                }
                if (DetFact.getDETRecargoMnt() != 0) {
                    DetalleItem.setRecargoMnt(new BigDecimal(DetFact.getDETRecargoMnt()));
                    DetalleItem.setRecargoMntSpecified(true);
                }
				if (!((DetFact.getDETSubRecargos() == null))) {
                    ListSubRec = new ArrayList<EnvioCFE.Item_Det_FactSubRecargo>();
                    for (int m = 0; m < DetFact.getDETSubRecargos().getDETSubRecargo().size(); m++) {
                        EnvioCFE.Item_Det_FactSubRecargo NewSubRec = EnvCFE.argValue.new Item_Det_FactSubRecargo();
                        NewSubRec.setRecargoTipo(EnvioCFE.Item_Det_FactSubRecargoRecargoTipo.forValue(DetFact.getDETSubRecargos().getDETSubRecargo().get(m).getDETRecargoTipo()));
                        NewSubRec.setRecargoVal(new BigDecimal(DetFact.getDETSubRecargos().getDETSubRecargo().get(m).getDETRecargoVal()));
                        ListSubRec.add(NewSubRec);
                    }
                    DetalleItem.setSubRecargo(ListSubRec);
                }
				if (!((DetFact.getDETRetencPerceps() == null))) {
                    ListRetPerc = new ArrayList<EnvioCFE.RetPerc>();
                    for (int n = 0; n < DetFact.getDETRetencPerceps().getDETRetencPercep().size(); n++) {
                        EnvioCFE.RetPerc NewRetPerc = EnvCFE.argValue.new RetPerc();
                        NewRetPerc.setCodRet(DetFact.getDETRetencPerceps().getDETRetencPercep().get(n).getDETCodRet());
                        if (DetFact.getDETRetencPerceps().getDETRetencPercep().get(n).getDETTasa() > 0) {
                            NewRetPerc.setTasa(new BigDecimal(DetFact.getDETRetencPerceps().getDETRetencPercep().get(n).getDETTasa()));
                            NewRetPerc.setTasaSpecified(true);
                        }
                        if (DetFact.getDETRetencPerceps().getDETRetencPercep().get(n).getDETMntSujetoaRet() > 0) {
                            NewRetPerc.setMntSujetoaRet(new BigDecimal(DetFact.getDETRetencPerceps().getDETRetencPercep().get(n).getDETMntSujetoaRet()));
                            NewRetPerc.setMntSujetoaRetSpecified(true);
                        }
                        NewRetPerc.setValRetPerc(new BigDecimal(DetFact.getDETRetencPerceps().getDETRetencPercep().get(n).getDETValRetPerc()));
                        ListRetPerc.add(NewRetPerc);
                    }
                    DetalleItem.setRetencPercep(ListRetPerc);
                }
				 DetalleItem.setMontoItem(new BigDecimal(DetFact.getDETMontoItem()));
                DetalleItemsList.add(DetalleItem);
            }
            Detalle.setItem(DetalleItemsList);
            ItemCFE.setDetalle(Detalle);

			//C - SubTotales Informativos
            if (!((FactElec.getDGI().getSubTotalInfos() == null))) {
                EnvioCFE.SubTotInfoSTI_Item SubTotInf = EnvCFE.argValue.new SubTotInfoSTI_Item();
                List<EnvioCFE.SubTotInfoSTI_Item.STI_Item> ListSubTot = new ArrayList<EnvioCFE.SubTotInfoSTI_Item.STI_Item>();
                for (int SubTot = 0; SubTot < FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().size(); SubTot++) {
                    EnvioCFE.SubTotInfoSTI_Item.STI_Item NewSubTot = SubTotInf.new STI_Item();
                    NewSubTot.setNroSTI(String.valueOf(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBNroSTI()));
                    NewSubTot.setGlosaSTI(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBGlosaSTI());
                    NewSubTot.setOrdenSTI(String.valueOf(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBOrdenSTI()));
                    NewSubTot.setValSubtotSTI(new BigDecimal(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBValSubtotSTI()));
                    ListSubTot.add(NewSubTot);
                }
                SubTotInf.setItem(ListSubTot);
                ItemCFE.setSubTotInfo(SubTotInf);
            }
			
			 //D - Descuentos y Recargos
            if (!((FactElec.getDGI().getDscRcgGlobales() == null))) {
                EnvioCFE.DscRcgGlobalDRG_Item DscRcgGlob = EnvCFE.argValue.new DscRcgGlobalDRG_Item();
                List<EnvioCFE.DscRcgGlobalDRG_Item.DRG_Item> ListDscRcgGlob = new ArrayList<EnvioCFE.DscRcgGlobalDRG_Item.DRG_Item>();
                for (int DscRcgGlobCount = 0; DscRcgGlobCount < FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().size(); DscRcgGlobCount++) {
                    EnvioCFE.DscRcgGlobalDRG_Item.DRG_Item NewDscRcgGlob = DscRcgGlob.new DRG_Item();
                    NewDscRcgGlob.setNroLinDR(String.valueOf(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGNroLinDR()));
                    NewDscRcgGlob.setTpoMovDR(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGTpoMovDR());
                    if (FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGTpoDR() > 0) {
                        NewDscRcgGlob.setTpoDR(EnvioCFE.TipoDRType.forValue(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGTpoDR()));
                        NewDscRcgGlob.setTpoDRSpecified(true);
                    }
                    NewDscRcgGlob.setCodDR(String.valueOf(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGCodDR()));
                    NewDscRcgGlob.setGlosaDR(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGGlosaDR());
                    NewDscRcgGlob.setValorDR(new BigDecimal(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGValorDR()));
                    if (FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGIndFactDR() > 0) {
                        NewDscRcgGlob.setIndFactDR(EnvioCFE.DscRcgGlobalDRG_ItemIndFactDR.forValue(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGIndFactDR()));
                        NewDscRcgGlob.setIndFactDRSpecified(true);
                    }
                    ListDscRcgGlob.add(NewDscRcgGlob);
                }
                DscRcgGlob.setItem(ListDscRcgGlob);
                ItemCFE.setDscRcgGlobal(DscRcgGlob);
            }

			//E - Medios de Pago
            if (!((FactElec.getDGI().getMediosPago() == null))) {
                EnvioCFE.MediosPagoMedioPago MedioPago = EnvCFE.argValue.new MediosPagoMedioPago();
                List<EnvioCFE.MediosPagoMedioPago.MedioPago_Item> ListMedioPago = new ArrayList<EnvioCFE.MediosPagoMedioPago.MedioPago_Item>();
                for (int MedioPagoCount = 0; MedioPagoCount < FactElec.getDGI().getMediosPago().getMedioPago().size(); MedioPagoCount++) {
                    EnvioCFE.MediosPagoMedioPago.MedioPago_Item NewMedioPago = MedioPago.new MedioPago_Item();
                    NewMedioPago.setNroLinMP(String.valueOf(FactElec.getDGI().getMediosPago().getMedioPago().get(MedioPagoCount).getMPNroLinMP()));
                    NewMedioPago.setCodMP(String.valueOf(FactElec.getDGI().getMediosPago().getMedioPago().get(MedioPagoCount).getMPCodMP()));
                    NewMedioPago.setGlosaMP(FactElec.getDGI().getMediosPago().getMedioPago().get(MedioPagoCount).getMPGlosaMP());
                    NewMedioPago.setOrdenMP(String.valueOf(FactElec.getDGI().getMediosPago().getMedioPago().get(MedioPagoCount).getMPOrdenMP()));
                    NewMedioPago.setValorPago(new BigDecimal(FactElec.getDGI().getMediosPago().getMedioPago().get(MedioPagoCount).getMPValorPago()));
                    ListMedioPago.add(NewMedioPago);
                }
                MedioPago.setItem(ListMedioPago);
                ItemCFE.setMediosPago(MedioPago);
            }

			//F - Informacion de referencia
            if (!((FactElec.getDGI().getReferencias() == null))) {
                EnvioCFE.ReferenciaReferencia Referencias = EnvCFE.argValue.new ReferenciaReferencia();
                List<EnvioCFE.ReferenciaReferencia.Referencia_Item> ListReferencias = new ArrayList<EnvioCFE.ReferenciaReferencia.Referencia_Item>();
                for (int Referencia = 0; Referencia < FactElec.getDGI().getReferencias().getReferencia().size(); Referencia++) {
                    EnvioCFE.ReferenciaReferencia.Referencia_Item NewReferencia = Referencias.new Referencia_Item();
                    NewReferencia.setNroLinRef(String.valueOf(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroLinRef()));
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal() != null) {
                        if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal() != null && !FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal().equals("")) {
                            int referGlobal = Integer.parseInt(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal());
                            NewReferencia.setIndGlobal(referGlobal);
                            NewReferencia.setIndGlobalSpecified(true);
                        }
                    }
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFTpoDocRef() != 0) {
                        NewReferencia.setTpoDocRef(EnvioCFE.CFEType.forValue(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFTpoDocRef()));
                        NewReferencia.setTpoDocRefSpecified(true);
                    }
                    NewReferencia.setSerie(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFSerie());
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroCFERef() != null) {
                        NewReferencia.setNroCFERef(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroCFERef());
                    }
                    NewReferencia.setRazonRef(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFRazonRef());
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFFechaCFEref() != null) {
                        NewReferencia.setFechaCFEref(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFFechaCFEref());
                    }
                    ListReferencias.add(NewReferencia);
                }
                Referencias.setItem(ListReferencias);
                ItemCFE.setReferencia(Referencias);
            }

			//G - CAE
			ItemCFE.setCAEData(CAEData);

			//K - Complemento Fiscal(Solo si es cuenta ajena)
			int IDDocTipoCFE = FactElec.getDGI().getIDDocTipoCFE();
			if (IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaCredCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaDebCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaCredCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaDebCuentaAjenaContingencia.getValue()) {
				ComplementoFiscalData.setRUCEmisor(FactElec.getDGI().getCFRUCEmisor());
				ComplementoFiscalData.setPais(FactElec.getDGI().getCFPais());
				ComplementoFiscalData.setTipoDocMdte(EnvioCFE.DocTypemasNIE.forValue(FactElec.getDGI().getCFTipoDocMdte()));
				ComplementoFiscalData.setDocMdte(FactElec.getDGI().getCFDocMdte());
				ComplementoFiscalData.setNombreMdte(FactElec.getDGI().getCFNombreMdte());
				ComplementoFiscal.setItem(ComplementoFiscalData);
				ItemCFE.setCompl_Fiscal(ComplementoFiscal);
			}

			CFE.setItem(ItemCFE);
            List<EnvioCFE.CFEDefType> listCFE = new ArrayList<EnvioCFE.CFEDefType>();
            listCFE.add(CFE);
            EnvCFE.argValue.setCFE(listCFE);

            LecturaOk.argValue = true;
			
		} catch (RuntimeException ex) {
			ErrorMsg.argValue = ((ex.getMessage().equals("")) ? ex.getMessage() : ex.getMessage()).toString();
		}
	}

	private void XMLEntrada_a_DGI_E_Fact(XMLFACTURA FactElec, tangible.RefObject<EnvioCFE> EnvCFE, tangible.RefObject<Boolean> LecturaOk, tangible.RefObject<String> ErrorMsg) {
		EnvioCFE.CFEDefTypeEFact ItemCFE = EnvCFE.argValue.new CFEDefTypeEFact();
		EnvioCFE.CFEDefType CFE = EnvCFE.argValue.new CFEDefType();
		EnvioCFE.Emisor Emisor = EnvCFE.argValue.new Emisor();
		EnvioCFE.Receptor_Fact Receptor = EnvCFE.argValue.new Receptor_Fact();
		EnvioCFE.CFEDefTypeEFactEncabezado Encabezado = EnvCFE.argValue.new CFEDefTypeEFactEncabezado();
		EnvioCFE.Totales Totales = EnvCFE.argValue.new Totales();
		EnvioCFE.Item_Det_Fact Detalle = EnvCFE.argValue.new Item_Det_Fact();
		EnvioCFE.CAEDataType CAEData = EnvCFE.argValue.new CAEDataType();
		EnvioCFE.IdDoc_Fact IdDoc = EnvCFE.argValue.new IdDoc_Fact();
		EnvioCFE.Compl_Fiscal_DataType ComplementoFiscalData = EnvCFE.argValue.new Compl_Fiscal_DataType();
		EnvioCFE.Compl_FiscalType ComplementoFiscal = EnvCFE.argValue.new Compl_FiscalType();

		try {
			LecturaOk.argValue = false;

			//A - Encabezado
			ItemCFE.setEncabezado(Encabezado);

			//Identificacion del comprobante
			ItemCFE.getEncabezado().setIdDoc(IdDoc);
			ItemCFE.getEncabezado().getIdDoc().setTipoCFE(EnvioCFE.IdDoc_FactTipoCFE.forValue(FactElec.getDGI().getIDDocTipoCFE()));
			ItemCFE.getEncabezado().getIdDoc().setFchEmis(FactElec.getDGI().getIDDocFchEmis());
			if (FactElec.getDGI().getIDDocPeriodoDesde() != null && FactElec.getDGI().getIDDocPeriodoDesde() != new Date(0)) {
				ItemCFE.getEncabezado().getIdDoc().setPeriodoDesde(FactElec.getDGI().getIDDocPeriodoDesde());
				ItemCFE.getEncabezado().getIdDoc().setPeriodoDesdeSpecified(true);
			}
			if (ItemCFE.getEncabezado().getIdDoc().getPeriodoHasta() != null && ItemCFE.getEncabezado().getIdDoc().getPeriodoHasta() != new Date(0)) {
				ItemCFE.getEncabezado().getIdDoc().setPeriodoHasta(FactElec.getDGI().getIDDocPeriodoHasta());
				ItemCFE.getEncabezado().getIdDoc().setPeriodoHastaSpecified(true);
			}

			if (FactElec.getDGI().getIDDocMntBruto() == 1) {
				ItemCFE.getEncabezado().getIdDoc().setMntBruto(EnvioCFE.IdDoc_FactMntBruto.forValue(FactElec.getDGI().getIDDocMntBruto()));
				ItemCFE.getEncabezado().getIdDoc().setMntBrutoSpecified(true);
			}
			ItemCFE.getEncabezado().getIdDoc().setFmaPago(EnvioCFE.IdDoc_FactFmaPago.forValue(FactElec.getDGI().getIDDocFmaPago()));
			if (FactElec.getDGI().getIDDocFchVenc() != null && FactElec.getDGI().getIDDocFchVenc().compareTo(new Date(0)) != 0) {
				ItemCFE.getEncabezado().getIdDoc().setFchaVenc(FactElec.getDGI().getIDDocFchVenc());
				ItemCFE.getEncabezado().getIdDoc().setFchVencSpecified(true);
			}
			ItemCFE.getEncabezado().getIdDoc().setInfoAdicionalDoc(FactElec.getDGI().getIDDocInfoAdicionalDoc());

			if (FactElec.getDGI().getIDDocIVAalDia() == 1) {
				ItemCFE.getEncabezado().getIdDoc().setIVAalDia(String.valueOf(FactElec.getDGI().getIDDocIVAalDia()));
			}
			if (FactElec.getDGI().getIDDocSecProf() == 1) {
				ItemCFE.getEncabezado().getIdDoc().setSecProf(String.valueOf(FactElec.getDGI().getIDDocSecProf()));
			}

			//Emisor
            ItemCFE.getEncabezado().setEmisor(Emisor);
            ItemCFE.getEncabezado().getEmisor().setRUCEmisor(FactElec.getDGI().getEMIRUCEmisor());
            ItemCFE.getEncabezado().getEmisor().setRznSoc(FactElec.getDGI().getEMIRznSoc());
            ItemCFE.getEncabezado().getEmisor().setNomComercial(FactElec.getDGI().getEMINomComercial());
            ItemCFE.getEncabezado().getEmisor().setGiroEmis(FactElec.getDGI().getEMIGiroEmis());
            if (!((FactElec.getDGI().getEmiTelefonos() == null))) {
                java.util.ArrayList<String> ListTelefonos = new java.util.ArrayList<String>();
                for (XMLFACTURA.clsDGI.EmiTelefonos.EMITelefono Tel : FactElec.getDGI().getEmiTelefonos().getEMITelefono()) {
                    ListTelefonos.add(Tel.getEMITelefono());
                }
                String[] tempListTelefonos = new String[ListTelefonos.size()];
                tempListTelefonos = ListTelefonos.toArray(tempListTelefonos);
                ItemCFE.getEncabezado().getEmisor().setTelefono(ListTelefonos);
            }
			ItemCFE.getEncabezado().getEmisor().setCorreoEmisor(FactElec.getDGI().getEMICorreoEmisor());
			ItemCFE.getEncabezado().getEmisor().setEmiSucursal(FactElec.getDGI().getEMIEmiSucursal());
			ItemCFE.getEncabezado().getEmisor().setCdgDGISucur(String.valueOf(FactElec.getDGI().getEMICdgDGISucur()));
			ItemCFE.getEncabezado().getEmisor().setDomFiscal(FactElec.getDGI().getEMIDomFiscal());
			ItemCFE.getEncabezado().getEmisor().setCiudad(FactElec.getDGI().getEMICiudad());
			ItemCFE.getEncabezado().getEmisor().setDepartamento(FactElec.getDGI().getEMIDepartamento());
			ItemCFE.getEncabezado().getEmisor().setInfoAdicionalEmisor(FactElec.getDGI().getEMIInfoAdicionalEmisor());

			//Receptor
			if (FactElec.getDGI().getRECTipoDocRecep() != 0) {
				ItemCFE.getEncabezado().setReceptor(Receptor);
				ItemCFE.getEncabezado().getReceptor().setTipoDocRecep(EnvioCFE.DocType.forValue(FactElec.getDGI().getRECTipoDocRecep()));
				ItemCFE.getEncabezado().getReceptor().setCodPaisRecep(FactElec.getDGI().getRECCodPaisRecep());
				ItemCFE.getEncabezado().getReceptor().setDocRecep(FactElec.getDGI().getRECDocRecep());
				ItemCFE.getEncabezado().getReceptor().setRznSocRecep(FactElec.getDGI().getRECRznSocRecep());
				ItemCFE.getEncabezado().getReceptor().setDirRecep(FactElec.getDGI().getRECDirRecep());
				ItemCFE.getEncabezado().getReceptor().setCiudadRecep(FactElec.getDGI().getRECCiudadRecep());
				ItemCFE.getEncabezado().getReceptor().setDeptoRecep(FactElec.getDGI().getRECDeptoRecep());
				ItemCFE.getEncabezado().getReceptor().setPaisRecep(FactElec.getDGI().getRECPaisRecep());
				if (FactElec.getDGI().getRECCP() != 0) {
					ItemCFE.getEncabezado().getReceptor().setCP(String.valueOf(FactElec.getDGI().getRECCP()));
				}
				ItemCFE.getEncabezado().getReceptor().setInfoAdicional(FactElec.getDGI().getRECInfoAdicional());
				ItemCFE.getEncabezado().getReceptor().setLugarDestEnt(FactElec.getDGI().getRECLugarDestinoEntrega());
				ItemCFE.getEncabezado().getReceptor().setCompraID(FactElec.getDGI().getRECNroOrdenCompra());
			}

			//Totales			
			ItemCFE.getEncabezado().setTotales(Totales);
			ItemCFE.getEncabezado().getTotales().setTpoMoneda(FactElec.getDGI().getTOTTpoMoneda());
			if (FactElec.getDGI().getTOTTpoCambio() != 0) {
				ItemCFE.getEncabezado().getTotales().setTpoCambio(new BigDecimal(FactElec.getDGI().getTOTTpoCambio()));
				ItemCFE.getEncabezado().getTotales().setTpoCambioSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntNoGrv() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntNoGrv(new BigDecimal(FactElec.getDGI().getTOTMntNoGrv()));
				ItemCFE.getEncabezado().getTotales().setMntNoGrvSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntExpoyAsim() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntExpoyAsim(new BigDecimal(FactElec.getDGI().getTOTMntExpoyAsim()));
				ItemCFE.getEncabezado().getTotales().setMntExpoyAsimSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntImpuestoPerc() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntImpuestoPerc(new BigDecimal(FactElec.getDGI().getTOTMntImpuestoPerc()));
				ItemCFE.getEncabezado().getTotales().setMntImpuestoPercSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntIVaenSusp() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntIVaenSusp(new BigDecimal(FactElec.getDGI().getTOTMntIVaenSusp()));
				ItemCFE.getEncabezado().getTotales().setMntIVaenSuspSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntNetoIvaTasaMin() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntNetoIvaTasaMin(new BigDecimal(FactElec.getDGI().getTOTMntNetoIvaTasaMin()));
				ItemCFE.getEncabezado().getTotales().setMntNetoIvaTasaMinSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntNetoIVATasaBasica() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntNetoIVATasaBasica(new BigDecimal(FactElec.getDGI().getTOTMntNetoIVATasaBasica()));
				ItemCFE.getEncabezado().getTotales().setMntNetoIVATasaBasicaSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntNetoIVAOtra() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntNetoIVAOtra(new BigDecimal(FactElec.getDGI().getTOTMntNetoIVAOtra()));
				ItemCFE.getEncabezado().getTotales().setMntNetoIVAOtraSpecified(true);
			}
			if (FactElec.getDGI().getTOTIVATasaMin() != 0) {
				ItemCFE.getEncabezado().getTotales().setIVATasaMin(new BigDecimal(FactElec.getDGI().getTOTIVATasaMin()));
				ItemCFE.getEncabezado().getTotales().setIVATasaMinSpecified(true);
			}
			if (FactElec.getDGI().getTOTIVATasaBasica() != 0) {
				ItemCFE.getEncabezado().getTotales().setIVATasaBasica(new BigDecimal(FactElec.getDGI().getTOTIVATasaBasica()));
				ItemCFE.getEncabezado().getTotales().setIVATasaBasicaSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntIVATasaMin() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntIVATasaMin(new BigDecimal(FactElec.getDGI().getTOTMntIVATasaMin()));
				ItemCFE.getEncabezado().getTotales().setMntIVATasaMinSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntIVATasaBasica() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntIVATasaBasica(new BigDecimal(FactElec.getDGI().getTOTMntIVATasaBasica()));
				ItemCFE.getEncabezado().getTotales().setMntIVATasaBasicaSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntIVAOtra() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntIVAOtra(new BigDecimal(FactElec.getDGI().getTOTMntIVAOtra()));
				ItemCFE.getEncabezado().getTotales().setMntIVAOtraSpecified(true);
			}
			ItemCFE.getEncabezado().getTotales().setMntTotal(new BigDecimal(FactElec.getDGI().getTOTMntTotal()));
			if (FactElec.getDGI().getTOTMntTotRetenido() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntTotRetenido(new BigDecimal(FactElec.getDGI().getTOTMntTotRetenido()));
				ItemCFE.getEncabezado().getTotales().setMntTotRetenidoSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntTotCredFisc() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntTotCredFisc(new BigDecimal(FactElec.getDGI().getTOTMntTotCredFisc()));
				ItemCFE.getEncabezado().getTotales().setMntTotCredFiscSpecified(true);
			}
			ItemCFE.getEncabezado().getTotales().setCantLinDet(String.valueOf(FactElec.getDGI().getTOTCantLinDet()));
            if (!((FactElec.getDGI().getTOTRetencPerceps() == null))) {
                java.util.ArrayList<EnvioCFE.TotalesRetencPercep> ListRetencPerceps = new java.util.ArrayList<EnvioCFE.TotalesRetencPercep>();
                for (XMLFACTURA.clsDGI.TOTRetencPerceps.TOTRetencPercep RetPrec : FactElec.getDGI().getTOTRetencPerceps().getOTRetencPercep()) {
                    EnvioCFE.TotalesRetencPercep NewRetPrec = EnvCFE.argValue.new TotalesRetencPercep();
                    NewRetPrec.setCodRet(RetPrec.getTOTCodRet());
                    NewRetPrec.setValRetPerc(new BigDecimal(RetPrec.getTOTValRetPerc()));
                    ListRetencPerceps.add(NewRetPrec);
                }
                EnvioCFE.TotalesRetencPercep[] tempListRetencPerceps = new EnvioCFE.TotalesRetencPercep[ListRetencPerceps.size()];
                tempListRetencPerceps = ListRetencPerceps.toArray(tempListRetencPerceps);
                ItemCFE.getEncabezado().getTotales().setRetencPercep(ListRetencPerceps);
            }
			if (FactElec.getDGI().getTOTMontoNF() != 0) {
				ItemCFE.getEncabezado().getTotales().setMontoNF(new BigDecimal(FactElec.getDGI().getTOTMontoNF()));
				ItemCFE.getEncabezado().getTotales().setMontoNFSpecified(true);
			}
			ItemCFE.getEncabezado().getTotales().setMntPagar(new BigDecimal(FactElec.getDGI().getTOTMntPagar()));


			//B - Detalle de productos o servicios
			List<EnvioCFE.Item_Det_Fact.Item> DetalleItemsList = new ArrayList<EnvioCFE.Item_Det_Fact.Item>();
			for (XMLFACTURA.clsDGI.Detalle.DETALLE DetFact : FactElec.getDGI().getDetalles().getDetalle()) {
				List<EnvioCFE.Item_Det_FactSubDescuento> ListSubDesc;
                List<EnvioCFE.Item_Det_FactSubRecargo> ListSubRec;
                List<EnvioCFE.RetPerc> ListRetPerc;
                List<EnvioCFE.Item_Det_FactCodItem> ListCodItem;

                EnvioCFE.Item_Det_Fact.Item DetalleItem = Detalle.new Item();
				DetalleItem.setNroLinDet(String.valueOf(DetFact.getDETNroLinDet()));
				           if (!((DetFact.getDETCodItems() == null))) {
                    ListCodItem = new ArrayList<EnvioCFE.Item_Det_FactCodItem>();
                    for (int CodItem = 0; CodItem < DetFact.getDETCodItems().getDETCodItem().size(); CodItem++) {
                        EnvioCFE.Item_Det_FactCodItem NewCodItem = EnvCFE.argValue.new Item_Det_FactCodItem();
                        NewCodItem.setCod(DetFact.getDETCodItems().getDETCodItem().get(CodItem).getDETCod());
                        NewCodItem.setTpoCod(DetFact.getDETCodItems().getDETCodItem().get(CodItem).getDETTpoCod());
                        ListCodItem.add(NewCodItem);
                    }
                    DetalleItem.setCodItem(ListCodItem);
                }
				if (DetFact.getDETIndFact() != 0) {
                    DetalleItem.setIndFact(EnvioCFE.Item_Det_FactIndFact.forValue(DetFact.getDETIndFact()));
                }
                if (!((DetFact.getDETIndAgenteResp() == null)) && !DetFact.getDETIndAgenteResp().trim().equals("")) {
                    DetalleItem.setIndAgenteResp(DetFact.getDETIndAgenteResp());
                    DetalleItem.setIndAgenteRespSpecified(true);
                }
				DetalleItem.setNomItem(DetFact.getDETNomItem());
                if (!((DetFact.getDETDscItem() == null)) && !DetFact.getDETDscItem().trim().equals("")) {
                    DetalleItem.setDscItem(DetFact.getDETDscItem());
                }
				DetalleItem.setCantidad(new BigDecimal(DetFact.getDETCantidad()));
				DetalleItem.setUniMed(DetFact.getDETUniMed());
				DetalleItem.setPrecioUnitario(new BigDecimal(DetFact.getDETPrecioUnitario()));
				if (DetFact.getDETDescuentoPct() != 0) {
					DetalleItem.setDescuentoPct(new BigDecimal(DetFact.getDETDescuentoPct()));
					DetalleItem.setDescuentoPctSpecified(true);
				}
				if (DetFact.getDETDescuentoMonto() != 0) {
					DetalleItem.setDescuentoMonto(new BigDecimal(DetFact.getDETDescuentoMonto()));
					DetalleItem.setDescuentoMontoSpecified(true);
				}
				if (!((DetFact.getDETSubDescuentos() == null))) {
                    ListSubDesc = new ArrayList<EnvioCFE.Item_Det_FactSubDescuento>();
                    for (int SubDesc = 0; SubDesc < DetFact.getDETSubDescuentos().getDETSubDescuento().size(); SubDesc++) {
                        EnvioCFE.Item_Det_FactSubDescuento NewSubDesc = EnvCFE.argValue.new Item_Det_FactSubDescuento();
                        NewSubDesc.setDescTipo(String.valueOf(DetFact.getDETSubDescuentos().getDETSubDescuento().get(SubDesc).getDETDescTipo()));
                        NewSubDesc.setDescVal(new BigDecimal(DetFact.getDETSubDescuentos().getDETSubDescuento().get(SubDesc).getDETDescVal()));
                        ListSubDesc.add(NewSubDesc);
                    }
                    DetalleItem.setSubDescuento(ListSubDesc);
                }
				if (DetFact.getDETRecargoPct() != 0) {
                    DetalleItem.setRecargoPct(new BigDecimal(DetFact.getDETRecargoPct()));
                    DetalleItem.setRecargoPctSpecified(true);
                }
                if (DetFact.getDETRecargoMnt() != 0) {
                    DetalleItem.setRecargoMnt(new BigDecimal(DetFact.getDETRecargoMnt()));
                    DetalleItem.setRecargoMntSpecified(true);
                }
				if (!((DetFact.getDETSubRecargos() == null))) {
                    ListSubRec = new ArrayList<EnvioCFE.Item_Det_FactSubRecargo>();
                    for (int SubRec = 0; SubRec < DetFact.getDETSubRecargos().getDETSubRecargo().size(); SubRec++) {
                        EnvioCFE.Item_Det_FactSubRecargo NewSubRec = EnvCFE.argValue.new Item_Det_FactSubRecargo();
                        NewSubRec.setRecargoTipo(EnvioCFE.Item_Det_FactSubRecargoRecargoTipo.forValue(DetFact.getDETSubRecargos().getDETSubRecargo().get(SubRec).getDETRecargoTipo()));
                        NewSubRec.setRecargoVal(new BigDecimal(DetFact.getDETSubRecargos().getDETSubRecargo().get(SubRec).getDETRecargoVal()));
                        ListSubRec.add(NewSubRec);
                    }
             DetalleItem.setSubRecargo(ListSubRec);
				}
				if (!((DetFact.getDETRetencPerceps() == null))) {
					ListRetPerc = new ArrayList<EnvioCFE.RetPerc>();
					int cont = 0;
					for (XMLFACTURA.clsDGI.Detalle.DETRetencPerceps.DETRetencPercep RetPerc : DetFact.getDETRetencPerceps().getDETRetencPercep()) {
						EnvioCFE.RetPerc NewRetPerc = EnvCFE.argValue.new RetPerc();
						NewRetPerc.setCodRet(RetPerc.getDETCodRet());
						if (RetPerc.getDETTasa() > 0) {
							NewRetPerc.setTasa(new BigDecimal(RetPerc.getDETTasa()));
							NewRetPerc.setTasaSpecified(true);
						}
						if (RetPerc.getDETMntSujetoaRet() > 0) {
							NewRetPerc.setMntSujetoaRet(new BigDecimal(RetPerc.getDETMntSujetoaRet()));
							NewRetPerc.setMntSujetoaRetSpecified(true);
						}
						NewRetPerc.setValRetPerc(new BigDecimal(RetPerc.getDETValRetPerc()));
						ListRetPerc.add(NewRetPerc);
					}
					DetalleItem.setRetencPercep(ListRetPerc);
				}
				DetalleItem.setMontoItem(new BigDecimal(DetFact.getDETMontoItem()));
				DetalleItemsList.add(DetalleItem);
			}

			Detalle.setItem(DetalleItemsList);
			ItemCFE.setDetalle(Detalle);

			//C - SubTotales Informativos
            if (!((FactElec.getDGI().getSubTotalInfos() == null))) {
                EnvioCFE.SubTotInfoSTI_Item SubTotInf = EnvCFE.argValue.new SubTotInfoSTI_Item();
                List<EnvioCFE.SubTotInfoSTI_Item.STI_Item> ListSubTot = new ArrayList<EnvioCFE.SubTotInfoSTI_Item.STI_Item>();
                for (int SubTot = 0; SubTot < FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().size(); SubTot++) {
                    EnvioCFE.SubTotInfoSTI_Item.STI_Item NewSubTot = SubTotInf.new STI_Item();
                    NewSubTot.setNroSTI(String.valueOf(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBNroSTI()));
                    NewSubTot.setGlosaSTI(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBGlosaSTI());
                    NewSubTot.setOrdenSTI(String.valueOf(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBOrdenSTI()));
                    NewSubTot.setValSubtotSTI(new BigDecimal(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBValSubtotSTI()));
                    ListSubTot.add(NewSubTot);
                }
                SubTotInf.setItem(ListSubTot);
                ItemCFE.setSubTotInfo(SubTotInf);
            }

			 //D - Descuentos y Recargos
            if (!((FactElec.getDGI().getDscRcgGlobales() == null))) {
                EnvioCFE.DscRcgGlobalDRG_Item DscRcgGlob = EnvCFE.argValue.new DscRcgGlobalDRG_Item();
                List<EnvioCFE.DscRcgGlobalDRG_Item.DRG_Item> ListDscRcgGlob = new ArrayList<EnvioCFE.DscRcgGlobalDRG_Item.DRG_Item>();
                for (int DscRcgGlobCount = 0; DscRcgGlobCount < FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().size(); DscRcgGlobCount++) {
                    EnvioCFE.DscRcgGlobalDRG_Item.DRG_Item NewDscRcgGlob = DscRcgGlob.new DRG_Item();
                    NewDscRcgGlob.setNroLinDR(String.valueOf(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGNroLinDR()));
                    NewDscRcgGlob.setTpoMovDR(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGTpoMovDR());
                    if (FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGTpoDR() > 0) {
                        NewDscRcgGlob.setTpoDR(EnvioCFE.TipoDRType.forValue(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGTpoDR()));
                        NewDscRcgGlob.setTpoDRSpecified(true);
                    }
                    NewDscRcgGlob.setCodDR(String.valueOf(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGCodDR()));
                    NewDscRcgGlob.setGlosaDR(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGGlosaDR());
                    NewDscRcgGlob.setValorDR(new BigDecimal(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGValorDR()));
                    if (FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGIndFactDR() > 0) {
                        NewDscRcgGlob.setIndFactDR(EnvioCFE.DscRcgGlobalDRG_ItemIndFactDR.forValue(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGIndFactDR()));
                        NewDscRcgGlob.setIndFactDRSpecified(true);
                    }
                    ListDscRcgGlob.add(NewDscRcgGlob);
                }
                DscRcgGlob.setItem(ListDscRcgGlob);
                ItemCFE.setDscRcgGlobal(DscRcgGlob);
            }

			//E - Medios de Pago
            if (!((FactElec.getDGI().getMediosPago() == null))) {
                EnvioCFE.MediosPagoMedioPago MedioPago = EnvCFE.argValue.new MediosPagoMedioPago();
                List<EnvioCFE.MediosPagoMedioPago.MedioPago_Item> ListMedioPago = new ArrayList<EnvioCFE.MediosPagoMedioPago.MedioPago_Item>();
                for (int MedioPagoCount = 0; MedioPagoCount < FactElec.getDGI().getMediosPago().getMedioPago().size(); MedioPagoCount++) {
                    EnvioCFE.MediosPagoMedioPago.MedioPago_Item NewMedioPago = MedioPago.new MedioPago_Item();
                    NewMedioPago.setNroLinMP(String.valueOf(FactElec.getDGI().getMediosPago().getMedioPago().get(MedioPagoCount).getMPNroLinMP()));
                    NewMedioPago.setCodMP(String.valueOf(FactElec.getDGI().getMediosPago().getMedioPago().get(MedioPagoCount).getMPCodMP()));
                    NewMedioPago.setGlosaMP(FactElec.getDGI().getMediosPago().getMedioPago().get(MedioPagoCount).getMPGlosaMP());
                    NewMedioPago.setOrdenMP(String.valueOf(FactElec.getDGI().getMediosPago().getMedioPago().get(MedioPagoCount).getMPOrdenMP()));
                    NewMedioPago.setValorPago(new BigDecimal(FactElec.getDGI().getMediosPago().getMedioPago().get(MedioPagoCount).getMPValorPago()));
                    ListMedioPago.add(NewMedioPago);
                }
                MedioPago.setItem(ListMedioPago);
                ItemCFE.setMediosPago(MedioPago);
            }

			//F - Informacion de referencia
            if (!((FactElec.getDGI().getReferencias() == null))) {
                EnvioCFE.ReferenciaReferencia Referencias = EnvCFE.argValue.new ReferenciaReferencia();
                List<EnvioCFE.ReferenciaReferencia.Referencia_Item> ListReferencias = new ArrayList<EnvioCFE.ReferenciaReferencia.Referencia_Item>();
                for (int Referencia = 0; Referencia < FactElec.getDGI().getReferencias().getReferencia().size(); Referencia++) {
                    EnvioCFE.ReferenciaReferencia.Referencia_Item NewReferencia = Referencias.new Referencia_Item();
                    NewReferencia.setNroLinRef(String.valueOf(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroLinRef()));
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal() != null) {
                        if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal() != null && !FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal().equals("")) {
                            int referGlobal = Integer.parseInt(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal());
                            NewReferencia.setIndGlobal(referGlobal);
                            NewReferencia.setIndGlobalSpecified(true);
                        }
                    }
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFTpoDocRef() != 0) {
                        NewReferencia.setTpoDocRef(EnvioCFE.CFEType.forValue(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFTpoDocRef()));
                        NewReferencia.setTpoDocRefSpecified(true);
                    }
                    NewReferencia.setSerie(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFSerie());
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroCFERef() != null) {
                        NewReferencia.setNroCFERef(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroCFERef());
                    }
                    NewReferencia.setRazonRef(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFRazonRef());
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFFechaCFEref() != null) {
                        NewReferencia.setFechaCFEref(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFFechaCFEref());
                    }
                    ListReferencias.add(NewReferencia);
                }
                Referencias.setItem(ListReferencias);
                ItemCFE.setReferencia(Referencias);
            }

			//G - CAE
			ItemCFE.setCAEData(CAEData);

			//K - Complemento Fiscal(Solo si es cuenta ajena)
			int IDDocTipoCFE = FactElec.getDGI().getIDDocTipoCFE();
			if (IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjenaContingencia.getValue()) {
				ComplementoFiscalData.setRUCEmisor(FactElec.getDGI().getCFRUCEmisor());
				ComplementoFiscalData.setPais(FactElec.getDGI().getCFPais());
				ComplementoFiscalData.setTipoDocMdte(EnvioCFE.DocTypemasNIE.forValue(FactElec.getDGI().getCFTipoDocMdte()));
				ComplementoFiscalData.setDocMdte(FactElec.getDGI().getCFDocMdte());
				ComplementoFiscalData.setNombreMdte(FactElec.getDGI().getCFNombreMdte());
				ComplementoFiscal.setItem(ComplementoFiscalData);
				ItemCFE.setCompl_Fiscal(ComplementoFiscal);
			}

			CFE.setItem(ItemCFE);
            List<EnvioCFE.CFEDefType> listCFE = new ArrayList<EnvioCFE.CFEDefType>();
            listCFE.add(CFE);
            EnvCFE.argValue.setCFE(listCFE);

            LecturaOk.argValue = true;

		} catch (RuntimeException ex) {
			ErrorMsg.argValue = ((ex.getMessage().equals("")) ? ex.getMessage() : ex.getMessage()).toString();
		}
	}

	private void XMLEntrada_a_DGI_E_Rem(XMLFACTURA FactElec, tangible.RefObject<EnvioCFE> EnvCFE, tangible.RefObject<Boolean> LecturaOk, tangible.RefObject<String> ErrorMsg) {
		EnvioCFE.CFEDefTypeERem ItemCFE = EnvCFE.argValue.new CFEDefTypeERem();
		EnvioCFE.CFEDefType CFE = EnvCFE.argValue.new CFEDefType();
		EnvioCFE.Emisor Emisor = EnvCFE.argValue.new Emisor();
		EnvioCFE.Receptor_Rem Receptor = EnvCFE.argValue.new Receptor_Rem();
		EnvioCFE.CFEDefTypeERemEncabezado Encabezado = EnvCFE.argValue.new CFEDefTypeERemEncabezado();
		EnvioCFE.CFEDefTypeERemEncabezadoTotales Totales = EnvCFE.argValue.new CFEDefTypeERemEncabezadoTotales();
		EnvioCFE.Item_Rem Detalle = EnvCFE.argValue.new Item_Rem();
		EnvioCFE.CAEDataType CAEData = EnvCFE.argValue.new CAEDataType();
		EnvioCFE.IdDoc_Rem IdDoc = EnvCFE.argValue.new IdDoc_Rem();

		try {
			LecturaOk.argValue = false;

			//A - Encabezado
			ItemCFE.setEncabezado(Encabezado);

			//Identificacion del comprobante
			ItemCFE.getEncabezado().setIdDoc(IdDoc);
			ItemCFE.getEncabezado().getIdDoc().setTipoCFE(EnvioCFE.IdDoc_RemTipoCFE.forValue(FactElec.getDGI().getIDDocTipoCFE()));
			ItemCFE.getEncabezado().getIdDoc().setFchEmis(FactElec.getDGI().getIDDocFchEmis());
			ItemCFE.getEncabezado().getIdDoc().setTipoTraslado(EnvioCFE.IdDoc_RemTipoTraslado.forValue(FactElec.getDGI().getIDDocTipoTraslado()));

			if (FactElec.getDGI().getIDDocIndPropiedad() == 1) //mercaderia de terceros
			{
				ItemCFE.getEncabezado().getIdDoc().setIndPropiedad(EnvioCFE.IdDoc_IndPropiedadMercadTransp.forValue(FactElec.getDGI().getIDDocIndPropiedad()));
				if (FactElec.getDGI().getIDDocTipoDocProp() != 0) {
					ItemCFE.getEncabezado().getIdDoc().setTipoDocProp(EnvioCFE.DocType.forValue(FactElec.getDGI().getIDDocTipoDocProp()));
					if (FactElec.getDGI().getIDDocTipoDocProp() == 4 || FactElec.getDGI().getIDDocTipoDocProp() == 5 || FactElec.getDGI().getIDDocTipoDocProp() == 6 || FactElec.getDGI().getIDDocTipoDocProp() == 7) {
						ItemCFE.getEncabezado().getIdDoc().setDocPropExt(FactElec.getDGI().getIDDocDocPropExt());
					} else {
						ItemCFE.getEncabezado().getIdDoc().setDocProp(FactElec.getDGI().getIDDocDocProp());
					}
				}

				if (FactElec.getDGI().getIDDocRznSocProp() != null && !FactElec.getDGI().getIDDocRznSocProp().equals("")) {
					ItemCFE.getEncabezado().getIdDoc().setRznSocProp(FactElec.getDGI().getIDDocRznSocProp());

				}
				if (FactElec.getDGI().getIDDocCodPaisProp() != null && !FactElec.getDGI().getIDDocCodPaisProp().equals("")) {
					ItemCFE.getEncabezado().getIdDoc().setCodPaisProp(FactElec.getDGI().getIDDocCodPaisProp());
				}
			}

			ItemCFE.getEncabezado().getIdDoc().setInfoAdicionalDoc(FactElec.getDGI().getIDDocInfoAdicionalDoc());

			if (FactElec.getDGI().getIDDocSecProf() == 1) {
				ItemCFE.getEncabezado().getIdDoc().setSecProf(String.valueOf(FactElec.getDGI().getIDDocSecProf()));
			}
			//Emisor
			ItemCFE.getEncabezado().setEmisor(Emisor);
			ItemCFE.getEncabezado().getEmisor().setRUCEmisor(FactElec.getDGI().getEMIRUCEmisor());
			ItemCFE.getEncabezado().getEmisor().setRznSoc(FactElec.getDGI().getEMIRznSoc());
			ItemCFE.getEncabezado().getEmisor().setNomComercial(FactElec.getDGI().getEMINomComercial());
			ItemCFE.getEncabezado().getEmisor().setGiroEmis(FactElec.getDGI().getEMIGiroEmis());
			if (!((FactElec.getDGI().getEmiTelefonos() == null))) {
                java.util.ArrayList<String> ListTelefonos = new java.util.ArrayList<String>();
                for (XMLFACTURA.clsDGI.EmiTelefonos.EMITelefono Tel : FactElec.getDGI().getEmiTelefonos().getEMITelefono()) {
                    ListTelefonos.add(Tel.getEMITelefono());
                }
                String[] temp = new String[ListTelefonos.size()];
                temp = ListTelefonos.toArray(temp);
                ItemCFE.getEncabezado().getEmisor().setTelefono(ListTelefonos);
            }
			ItemCFE.getEncabezado().getEmisor().setCorreoEmisor(FactElec.getDGI().getEMICorreoEmisor());
			ItemCFE.getEncabezado().getEmisor().setEmiSucursal(FactElec.getDGI().getEMIEmiSucursal());
			ItemCFE.getEncabezado().getEmisor().setCdgDGISucur(String.valueOf(FactElec.getDGI().getEMICdgDGISucur()));
			ItemCFE.getEncabezado().getEmisor().setDomFiscal(FactElec.getDGI().getEMIDomFiscal());
			ItemCFE.getEncabezado().getEmisor().setCiudad(FactElec.getDGI().getEMICiudad());
			ItemCFE.getEncabezado().getEmisor().setDepartamento(FactElec.getDGI().getEMIDepartamento());
			ItemCFE.getEncabezado().getEmisor().setInfoAdicionalEmisor(FactElec.getDGI().getEMIInfoAdicionalEmisor());

			if (FactElec.getDGI().getIDDocSecProf() == 1) {
				ItemCFE.getEncabezado().getIdDoc().setSecProf(String.valueOf(FactElec.getDGI().getIDDocSecProf()));
			}

			//Receptor
			if (FactElec.getDGI().getRECTipoDocRecep() != 0) {
				ItemCFE.getEncabezado().setReceptor(Receptor);
				if (FactElec.getDGI().getRECTipoDocRecep() != 0) {
					ItemCFE.getEncabezado().getReceptor().setTipoDocRecep(EnvioCFE.DocType.forValue(FactElec.getDGI().getRECTipoDocRecep()));
					ItemCFE.getEncabezado().getReceptor().setTipoDocRecepSpecified(true);
					if (FactElec.getDGI().getRECTipoDocRecep() != 0) {
						if (FactElec.getDGI().getRECTipoDocRecep() == 4 || FactElec.getDGI().getRECTipoDocRecep() == 5 || FactElec.getDGI().getRECTipoDocRecep() == 6 || FactElec.getDGI().getRECTipoDocRecep() == 7) {
							ItemCFE.getEncabezado().getReceptor().setDocRecepExt(FactElec.getDGI().getRECDocRecepExtranjero());
						} else {
							ItemCFE.getEncabezado().getReceptor().setDocRecep(FactElec.getDGI().getRECDocRecep());
						}
					}
				}
				if (!FactElec.getDGI().getRECCodPaisRecep().equals("")) {
					ItemCFE.getEncabezado().getReceptor().setCodPaisRecep(FactElec.getDGI().getRECCodPaisRecep());
					ItemCFE.getEncabezado().getReceptor().setCodPaisRecepSpecified(true);
				}

				ItemCFE.getEncabezado().getReceptor().setRznSocRecep(FactElec.getDGI().getRECRznSocRecep());
				ItemCFE.getEncabezado().getReceptor().setDirRecep(FactElec.getDGI().getRECDirRecep());
				ItemCFE.getEncabezado().getReceptor().setCiudadRecep(FactElec.getDGI().getRECCiudadRecep());
				ItemCFE.getEncabezado().getReceptor().setDeptoRecep(FactElec.getDGI().getRECDeptoRecep());
				ItemCFE.getEncabezado().getReceptor().setPaisRecep(FactElec.getDGI().getRECPaisRecep());
				if (FactElec.getDGI().getRECCP() != 0) {
					ItemCFE.getEncabezado().getReceptor().setCP(String.valueOf(FactElec.getDGI().getRECCP()));
				}
				ItemCFE.getEncabezado().getReceptor().setInfoAdicional(FactElec.getDGI().getRECInfoAdicional());
				ItemCFE.getEncabezado().getReceptor().setLugarDestEnt(FactElec.getDGI().getRECLugarDestinoEntrega());
				ItemCFE.getEncabezado().getReceptor().setCompraID(FactElec.getDGI().getRECNroOrdenCompra());
			}

			//Totales
			ItemCFE.getEncabezado().setTotales(Totales);
			ItemCFE.getEncabezado().getTotales().setCantLinDet(String.valueOf(FactElec.getDGI().getTOTCantLinDet()));

			//B - Detalle de productos o servicios
			List<EnvioCFE.Item_Rem.Item> DetalleItemsList = new ArrayList<EnvioCFE.Item_Rem.Item>();
			for (XMLFACTURA.clsDGI.Detalle.DETALLE DetFact : FactElec.getDGI().getDetalles().getDetalle()) {
				 java.util.ArrayList<EnvioCFE.Item_Det_FactSubDescuento> ListSubDesc = new java.util.ArrayList<EnvioCFE.Item_Det_FactSubDescuento>();
                java.util.ArrayList<EnvioCFE.Item_Det_FactSubRecargo> ListSubRec = new java.util.ArrayList<EnvioCFE.Item_Det_FactSubRecargo>();
                java.util.ArrayList<EnvioCFE.RetPerc> ListRetPerc = new java.util.ArrayList<EnvioCFE.RetPerc>();
                java.util.ArrayList<EnvioCFE.Item_RemCodItem> ListCodItem = new java.util.ArrayList<EnvioCFE.Item_RemCodItem>();

                EnvioCFE.Item_Rem.Item DetalleItem = Detalle.new Item();

				DetalleItem.setNroLinDet(String.valueOf(DetFact.getDETNroLinDet()));
                if (!((DetFact.getDETCodItems() == null))) {
                    for (XMLFACTURA.clsDGI.Detalle.DETCodItems.DETCodItem CodItem : DetFact.getDETCodItems().getDETCodItem()) {
                        EnvioCFE.Item_RemCodItem NewCodItem = EnvCFE.argValue.new Item_RemCodItem();
                        NewCodItem.setCod(CodItem.getDETCod());
                        NewCodItem.setTpoCod(CodItem.getDETTpoCod());
                        ListCodItem.add(NewCodItem);
                    }

                    EnvioCFE.Item_RemCodItem[] temp = new EnvioCFE.Item_RemCodItem[ListCodItem.size()];
                    temp = ListCodItem.toArray(temp);
                    DetalleItem.setCodItem(ListCodItem);
                }
				if (DetFact.getDETIndFact() == 0) {
					DetalleItem.setIndFactSpecified(false);
				} else {
					DetalleItem.setIndFactSpecified(true);
					DetalleItem.setIndFact(EnvioCFE.Item_RemIndFact.forValue(DetFact.getDETIndFact()));
				}
				DetalleItem.setNomItem(DetFact.getDETNomItem());
				if (!((DetFact.getDETDscItem() == null)) && !DetFact.getDETDscItem().trim().equals("")) {
					DetalleItem.setDscItem(DetFact.getDETDscItem());
				}
				DetalleItem.setCantidad(new BigDecimal(DetFact.getDETCantidad()));
                DetalleItem.setUniMed(DetFact.getDETUniMed());
                DetalleItemsList.add(DetalleItem);
			}

			Detalle.setItem(DetalleItemsList);
            ItemCFE.setDetalle(Detalle);

			//C - SubTotales Informativos
            if (!((FactElec.getDGI().getSubTotalInfos() == null))) {
                EnvioCFE.SubTotInfoSTI_Item SubTotInf = EnvCFE.argValue.new SubTotInfoSTI_Item();
                List<EnvioCFE.SubTotInfoSTI_Item.STI_Item> ListSubTot = new ArrayList<EnvioCFE.SubTotInfoSTI_Item.STI_Item>();
                for (int SubTot = 0; SubTot < FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().size(); SubTot++) {
                    EnvioCFE.SubTotInfoSTI_Item.STI_Item NewSubTot = SubTotInf.new STI_Item();
                    NewSubTot.setNroSTI(String.valueOf(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBNroSTI()));
                    NewSubTot.setGlosaSTI(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBGlosaSTI());
                    NewSubTot.setOrdenSTI(String.valueOf(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBOrdenSTI()));
                    NewSubTot.setValSubtotSTI(new BigDecimal(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBValSubtotSTI()));
                    ListSubTot.add(NewSubTot);
                }
                SubTotInf.setItem(ListSubTot);
                ItemCFE.setSubTotInfo(SubTotInf);
            }

			 //F - Informacion de referencia
            if (!((FactElec.getDGI().getReferencias() == null))) {
                EnvioCFE.ReferenciaReferencia Referencias = EnvCFE.argValue.new ReferenciaReferencia();
                List<EnvioCFE.ReferenciaReferencia.Referencia_Item> ListReferencias = new ArrayList<EnvioCFE.ReferenciaReferencia.Referencia_Item>();
                for (int Referencia = 0; Referencia < FactElec.getDGI().getReferencias().getReferencia().size(); Referencia++) {
                    EnvioCFE.ReferenciaReferencia.Referencia_Item NewReferencia = Referencias.new Referencia_Item();
                    NewReferencia.setNroLinRef(String.valueOf(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroLinRef()));
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal() != null) {
                        if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal() != null && !FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal().equals("")) {
                            int referGlobal = Integer.parseInt(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal());
                            NewReferencia.setIndGlobal(referGlobal);
                            NewReferencia.setIndGlobalSpecified(true);
                        }
                    }
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFTpoDocRef() != 0) {
                        NewReferencia.setTpoDocRef(EnvioCFE.CFEType.forValue(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFTpoDocRef()));
                        NewReferencia.setTpoDocRefSpecified(true);
                    }
                    NewReferencia.setSerie(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFSerie());
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroCFERef() != null) {
                        NewReferencia.setNroCFERef(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroCFERef());
                    }
                    NewReferencia.setRazonRef(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFRazonRef());
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFFechaCFEref() != null) {
                        NewReferencia.setFechaCFEref(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFFechaCFEref());
                    }
                    ListReferencias.add(NewReferencia);
                }
                Referencias.setItem(ListReferencias);
                ItemCFE.setReferencia(Referencias);
            }

			//G - CAE
			ItemCFE.setCAEData(CAEData);

			CFE.setItem(ItemCFE);
            List<EnvioCFE.CFEDefType> listCFE = new ArrayList<EnvioCFE.CFEDefType>();
            listCFE.add(CFE);
            EnvCFE.argValue.setCFE(listCFE);

            LecturaOk.argValue = true;

		} catch (RuntimeException ex) {
			ErrorMsg.argValue = ((ex.getMessage().equals("")) ? ex.getMessage() : ex.getMessage()).toString();
		}
	}

	private void XMLEntrada_a_DGI_E_Res(XMLFACTURA FactElec, tangible.RefObject<EnvioCFE> EnvCFE, tangible.RefObject<Boolean> LecturaOk, tangible.RefObject<String> ErrorMsg) {
		EnvioCFE.CFEDefTypeEResg ItemCFE = EnvCFE.argValue.new CFEDefTypeEResg();
		EnvioCFE.CFEDefType CFE = EnvCFE.argValue.new CFEDefType();
		EnvioCFE.Emisor Emisor = EnvCFE.argValue.new Emisor();
		EnvioCFE.Receptor_Resg Receptor = EnvCFE.argValue.new Receptor_Resg();
		EnvioCFE.CFEDefTypeEResgEncabezado Encabezado = EnvCFE.argValue.new CFEDefTypeEResgEncabezado();
		EnvioCFE.Totales_Resg Totales = EnvCFE.argValue.new Totales_Resg();
		EnvioCFE.Item_Resg Detalle = EnvCFE.argValue.new Item_Resg();
		EnvioCFE.CAEDataType CAEData = EnvCFE.argValue.new CAEDataType();
		EnvioCFE.IdDoc_Resg IdDoc = EnvCFE.argValue.new IdDoc_Resg();

		try {
			LecturaOk.argValue = false;

			//A - Encabezado
			ItemCFE.setEncabezado(Encabezado);

			//Identificacion del comprobante
			ItemCFE.getEncabezado().setIdDoc(IdDoc);
			ItemCFE.getEncabezado().getIdDoc().setTipoCFE(EnvioCFE.IdDoc_ResgTipoCFE.forValue(FactElec.getDGI().getIDDocTipoCFE()));
			ItemCFE.getEncabezado().getIdDoc().setFchEmis(FactElec.getDGI().getIDDocFchEmis());
			ItemCFE.getEncabezado().getIdDoc().setInfoAdicionalDoc(FactElec.getDGI().getIDDocInfoAdicionalDoc());
			if (FactElec.getDGI().getIDDocSecProf() == 1) {
				ItemCFE.getEncabezado().getIdDoc().setSecProf(String.valueOf(FactElec.getDGI().getIDDocSecProf()));
			}

			//Emisor
			ItemCFE.getEncabezado().setEmisor(Emisor);
			ItemCFE.getEncabezado().getEmisor().setRUCEmisor(FactElec.getDGI().getEMIRUCEmisor());
			ItemCFE.getEncabezado().getEmisor().setRznSoc(FactElec.getDGI().getEMIRznSoc());
			ItemCFE.getEncabezado().getEmisor().setNomComercial(FactElec.getDGI().getEMINomComercial());
			ItemCFE.getEncabezado().getEmisor().setGiroEmis(FactElec.getDGI().getEMIGiroEmis());
			if (!((FactElec.getDGI().getEmiTelefonos() == null))) {
                java.util.ArrayList<String> ListTelefonos = new java.util.ArrayList<String>();
                for (XMLFACTURA.clsDGI.EmiTelefonos.EMITelefono Tel : FactElec.getDGI().getEmiTelefonos().getEMITelefono()) {
                    ListTelefonos.add(Tel.getEMITelefono());
                }
                String[] tempListTelefonos = new String[ListTelefonos.size()];
                tempListTelefonos = ListTelefonos.toArray(tempListTelefonos);
                ItemCFE.getEncabezado().getEmisor().setTelefono(ListTelefonos);
            }
			ItemCFE.getEncabezado().getEmisor().setCorreoEmisor(FactElec.getDGI().getEMICorreoEmisor());
			ItemCFE.getEncabezado().getEmisor().setEmiSucursal(FactElec.getDGI().getEMIEmiSucursal());
			ItemCFE.getEncabezado().getEmisor().setCdgDGISucur(String.valueOf(FactElec.getDGI().getEMICdgDGISucur()));
			ItemCFE.getEncabezado().getEmisor().setDomFiscal(FactElec.getDGI().getEMIDomFiscal());
			ItemCFE.getEncabezado().getEmisor().setCiudad(FactElec.getDGI().getEMICiudad());
			ItemCFE.getEncabezado().getEmisor().setDepartamento(FactElec.getDGI().getEMIDepartamento());
			ItemCFE.getEncabezado().getEmisor().setInfoAdicionalEmisor(FactElec.getDGI().getEMIInfoAdicionalEmisor());

			//Receptor
						if (FactElec.getDGI().getRECTipoDocRecep() != 0) {
				ItemCFE.getEncabezado().setReceptor(Receptor);
				if (FactElec.getDGI().getRECTipoDocRecep() != 0) {
					ItemCFE.getEncabezado().getReceptor().setTipoDocRecep(EnvioCFE.DocType.forValue(FactElec.getDGI().getRECTipoDocRecep()));
					ItemCFE.getEncabezado().getReceptor().setTipoDocRecepSpecified(true);
					if (FactElec.getDGI().getRECTipoDocRecep() == 4 || FactElec.getDGI().getRECTipoDocRecep() == 5 || FactElec.getDGI().getRECTipoDocRecep() == 6 || FactElec.getDGI().getRECTipoDocRecep() == 7) {
						ItemCFE.getEncabezado().getReceptor().setDocRecepExt(FactElec.getDGI().getRECDocRecepExtranjero());
					} else {
						ItemCFE.getEncabezado().getReceptor().setDocRecep(FactElec.getDGI().getRECDocRecep());
					}
				}

				if (!FactElec.getDGI().getRECCodPaisRecep().equals("")) {
					ItemCFE.getEncabezado().getReceptor().setCodPaisRecep(FactElec.getDGI().getRECCodPaisRecep());
					ItemCFE.getEncabezado().getReceptor().setCodPaisRecepSpecified(true);
				}

				ItemCFE.getEncabezado().getReceptor().setRznSocRecep(FactElec.getDGI().getRECRznSocRecep());
				ItemCFE.getEncabezado().getReceptor().setDirRecep(FactElec.getDGI().getRECDirRecep());
				ItemCFE.getEncabezado().getReceptor().setCiudadRecep(FactElec.getDGI().getRECCiudadRecep());
				ItemCFE.getEncabezado().getReceptor().setDeptoRecep(FactElec.getDGI().getRECDeptoRecep());
				ItemCFE.getEncabezado().getReceptor().setPaisRecep(FactElec.getDGI().getRECPaisRecep());
				if (FactElec.getDGI().getRECCP() != 0) {
					ItemCFE.getEncabezado().getReceptor().setCP(String.valueOf(FactElec.getDGI().getRECCP()));
				}
				ItemCFE.getEncabezado().getReceptor().setInfoAdicional(FactElec.getDGI().getRECInfoAdicional());
			}

			//Totales
			ItemCFE.getEncabezado().setTotales(Totales);
			ItemCFE.getEncabezado().getTotales().setTpoMoneda(FactElec.getDGI().getTOTTpoMoneda());
			if (FactElec.getDGI().getTOTTpoCambio() != 0) {
				ItemCFE.getEncabezado().getTotales().setTpoCambio(new BigDecimal(FactElec.getDGI().getTOTTpoCambio()));
				ItemCFE.getEncabezado().getTotales().setTpoCambioSpecified(true);
			}
			ItemCFE.getEncabezado().getTotales().setMntTotRetenido(new BigDecimal(FactElec.getDGI().getTOTMntTotRetenido()));
			if (FactElec.getDGI().getTOTMntTotCredFisc() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntTotCredFisc(new BigDecimal(FactElec.getDGI().getTOTMntTotCredFisc()));
				ItemCFE.getEncabezado().getTotales().setMntTotCredFiscSpecified(true);
			}
			ItemCFE.getEncabezado().getTotales().setCantLinDet(String.valueOf(FactElec.getDGI().getTOTCantLinDet()));
            if (!((FactElec.getDGI().getTOTRetencPerceps() == null))) {
                java.util.ArrayList<EnvioCFE.Totales_ResgRetencPercep> ListRetencPerceps = new java.util.ArrayList<EnvioCFE.Totales_ResgRetencPercep>();
                for (XMLFACTURA.clsDGI.TOTRetencPerceps.TOTRetencPercep RetPrec : FactElec.getDGI().getTOTRetencPerceps().getOTRetencPercep()) {
                    EnvioCFE.Totales_ResgRetencPercep NewRetPrec = EnvCFE.argValue.new Totales_ResgRetencPercep();
                    NewRetPrec.setCodRet(RetPrec.getTOTCodRet());
                    if (RetPrec.getTOTValRetPerc() != 0) {
                        NewRetPrec.setValRetPerc(new BigDecimal(RetPrec.getTOTValRetPerc()));
                    }
                    ListRetencPerceps.add(NewRetPrec);
                }
                EnvioCFE.Totales_ResgRetencPercep[] tempListRetencPerceps = new EnvioCFE.Totales_ResgRetencPercep[ListRetencPerceps.size()];
                tempListRetencPerceps = ListRetencPerceps.toArray(tempListRetencPerceps);
                ItemCFE.getEncabezado().getTotales().setRetencPercep(ListRetencPerceps);
            }

			//B - Detalle de productos o servicios
			List<EnvioCFE.Item_Resg.Item> DetalleItemsList = new ArrayList<EnvioCFE.Item_Resg.Item>();
			for (XMLFACTURA.clsDGI.Detalle.DETALLE DetFact : FactElec.getDGI().getDetalles().getDetalle()) {
				java.util.ArrayList<EnvioCFE.Item_Det_FactSubDescuento> ListSubDesc = new java.util.ArrayList<EnvioCFE.Item_Det_FactSubDescuento>();
                java.util.ArrayList<EnvioCFE.Item_Det_FactSubRecargo> ListSubRec = new java.util.ArrayList<EnvioCFE.Item_Det_FactSubRecargo>();
                java.util.ArrayList<EnvioCFE.RetPerc_Resg> ListRetPerc = new java.util.ArrayList<EnvioCFE.RetPerc_Resg>();
                java.util.ArrayList<EnvioCFE.Item_Det_FactCodItem> ListCodItem = new java.util.ArrayList<EnvioCFE.Item_Det_FactCodItem>();

                EnvioCFE.Item_Resg.Item DetalleItem = Detalle.new Item();
			
				if (DetFact.getDETIndFact() == 0) {
					DetalleItem.setIndFactSpecified(false);
				} else {
					DetalleItem.setIndFactSpecified(true);
					DetalleItem.setIndFact(EnvioCFE.Item_ResgIndFact.forValue(DetFact.getDETIndFact()));
				}
				DetalleItem.setNroLinDet(String.valueOf(DetFact.getDETNroLinDet()));
				if (!((DetFact.getDETRetencPerceps() == null))) {
					for (XMLFACTURA.clsDGI.Detalle.DETRetencPerceps.DETRetencPercep RetPerc : DetFact.getDETRetencPerceps().getDETRetencPercep()) {
						EnvioCFE.RetPerc_Resg NewRetPerc = EnvCFE.argValue.new RetPerc_Resg();
						NewRetPerc.setCodRet(RetPerc.getDETCodRet());
						if (RetPerc.getDETTasa() > 0) {
							NewRetPerc.setTasa(new BigDecimal(RetPerc.getDETTasa()));
							NewRetPerc.setTasaSpecified(true);
						}
						NewRetPerc.setMntSujetoaRet(new BigDecimal(RetPerc.getDETMntSujetoaRet()));
						NewRetPerc.setValRetPerc(new BigDecimal(RetPerc.getDETValRetPerc()));
						ListRetPerc.add(NewRetPerc);
					}
					EnvioCFE.RetPerc_Resg[] tempListRetPerc = new EnvioCFE.RetPerc_Resg[ListRetPerc.size()];
					tempListRetPerc = ListRetPerc.toArray(tempListRetPerc);
					DetalleItem.setRetencPercep(ListRetPerc);
					DetalleItemsList.add(DetalleItem);
				}
			}
			Detalle.setItem(DetalleItemsList);
			ItemCFE.setDetalle(Detalle);

			//C - SubTotales Informativos
            if (!((FactElec.getDGI().getSubTotalInfos() == null))) {
                EnvioCFE.SubTotInfoSTI_Item SubTotInf = EnvCFE.argValue.new SubTotInfoSTI_Item();
                List<EnvioCFE.SubTotInfoSTI_Item.STI_Item> ListSubTot = new ArrayList<EnvioCFE.SubTotInfoSTI_Item.STI_Item>();
                for (int SubTot = 0; SubTot < FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().size(); SubTot++) {
                    EnvioCFE.SubTotInfoSTI_Item.STI_Item NewSubTot = SubTotInf.new STI_Item();
                    NewSubTot.setNroSTI(String.valueOf(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBNroSTI()));
                    NewSubTot.setGlosaSTI(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBGlosaSTI());
                    NewSubTot.setOrdenSTI(String.valueOf(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBOrdenSTI()));
                    NewSubTot.setValSubtotSTI(new BigDecimal(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBValSubtotSTI()));
                    ListSubTot.add(NewSubTot);
                }
                SubTotInf.setItem(ListSubTot);
                ItemCFE.setSubTotInfo(SubTotInf);
            }

			//F - Informacion de referencia
            if (!((FactElec.getDGI().getReferencias() == null))) {
                EnvioCFE.ReferenciaReferencia Referencias = EnvCFE.argValue.new ReferenciaReferencia();
                List<EnvioCFE.ReferenciaReferencia.Referencia_Item> ListReferencias = new ArrayList<EnvioCFE.ReferenciaReferencia.Referencia_Item>();
                for (int Referencia = 0; Referencia < FactElec.getDGI().getReferencias().getReferencia().size(); Referencia++) {
                    EnvioCFE.ReferenciaReferencia.Referencia_Item NewReferencia = Referencias.new Referencia_Item();
                    NewReferencia.setNroLinRef(String.valueOf(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroLinRef()));
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal() != null) {
                        if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal() != null && !FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal().equals("")) {
                            int referGlobal = Integer.parseInt(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal());
                            NewReferencia.setIndGlobal(referGlobal);
                            NewReferencia.setIndGlobalSpecified(true);
                        }
                    }
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFTpoDocRef() != 0) {
                        NewReferencia.setTpoDocRef(EnvioCFE.CFEType.forValue(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFTpoDocRef()));
                        NewReferencia.setTpoDocRefSpecified(true);
                    }
                    NewReferencia.setSerie(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFSerie());
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroCFERef() != null) {
                        NewReferencia.setNroCFERef(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroCFERef());
                    }
                    NewReferencia.setRazonRef(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFRazonRef());
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFFechaCFEref() != null) {
                        NewReferencia.setFechaCFEref(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFFechaCFEref());
                    }
                    ListReferencias.add(NewReferencia);
                }
                Referencias.setItem(ListReferencias);
                ItemCFE.setReferencia(Referencias);
            }

			//G - CAE
			ItemCFE.setCAEData(CAEData);

			CFE.setItem(ItemCFE);
            List<EnvioCFE.CFEDefType> listCFE = new ArrayList<EnvioCFE.CFEDefType>();
            listCFE.add(CFE);
            EnvCFE.argValue.setCFE(listCFE);


			LecturaOk.argValue = true;

		} catch (RuntimeException ex) {
			ErrorMsg.argValue = ((ex.getMessage().equals("")) ? ex.getMessage() : ex.getMessage()).toString();
		}
	}

	private void XMLEntrada_a_DGI_E_Fact_Exp(XMLFACTURA FactElec, tangible.RefObject<EnvioCFE> EnvCFE, tangible.RefObject<Boolean> LecturaOk, tangible.RefObject<String> ErrorMsg) {
		EnvioCFE.CFEDefTypeEFact_Exp ItemCFE = EnvCFE.argValue.new CFEDefTypeEFact_Exp();
		EnvioCFE.CFEDefType CFE = EnvCFE.argValue.new CFEDefType();
		EnvioCFE.Emisor Emisor = EnvCFE.argValue.new Emisor();
		EnvioCFE.Receptor_Fact_Exp Receptor = EnvCFE.argValue.new Receptor_Fact_Exp();
		EnvioCFE.CFEDefTypeEFact_ExpEncabezado Encabezado = EnvCFE.argValue.new CFEDefTypeEFact_ExpEncabezado();
		EnvioCFE.Totales_Fact_Exp Totales = EnvCFE.argValue.new Totales_Fact_Exp();
		EnvioCFE.Item_Det_Fact_Exp Detalle = EnvCFE.argValue.new Item_Det_Fact_Exp();
		EnvioCFE.CAEDataType CAEData = EnvCFE.argValue.new CAEDataType();
		EnvioCFE.IdDoc_Fact_Exp IdDoc = EnvCFE.argValue.new IdDoc_Fact_Exp();

		try {
			LecturaOk.argValue = false;

			//A - Encabezado
			ItemCFE.setEncabezado(Encabezado);

			//Identificacion del comprobante
			ItemCFE.getEncabezado().setIdDoc(IdDoc);
			ItemCFE.getEncabezado().getIdDoc().setTipoCFE(EnvioCFE.IdDoc_Fact_ExpTipoCFE.forValue(FactElec.getDGI().getIDDocTipoCFE()));
			ItemCFE.getEncabezado().getIdDoc().setFchEmis(FactElec.getDGI().getIDDocFchEmis());
			if (FactElec.getDGI().getIDDocPeriodoDesde() != null && FactElec.getDGI().getIDDocPeriodoDesde() != new Date(0)) {
				ItemCFE.getEncabezado().getIdDoc().setPeriodoDesde(FactElec.getDGI().getIDDocPeriodoDesde());
				ItemCFE.getEncabezado().getIdDoc().setPeriodoDesdeSpecified(true);
			}
			if (FactElec.getDGI().getIDDocPeriodoHasta() != null && FactElec.getDGI().getIDDocPeriodoHasta() != new Date(0)) {
				ItemCFE.getEncabezado().getIdDoc().setPeriodoHasta(FactElec.getDGI().getIDDocPeriodoHasta());
				ItemCFE.getEncabezado().getIdDoc().setPeriodoHastaSpecified(true);
			}
			if (FactElec.getDGI().getIDDocMntBruto() == 1) {
				ItemCFE.getEncabezado().getIdDoc().setMntBruto(EnvioCFE.IdDoc_Fact_ExpMntBruto.forValue(FactElec.getDGI().getIDDocMntBruto()));
				ItemCFE.getEncabezado().getIdDoc().setMntBrutoSpecified(true);
			}
			ItemCFE.getEncabezado().getIdDoc().setFmaPago(EnvioCFE.IdDoc_Fact_ExpFmaPago.forValue(FactElec.getDGI().getIDDocFmaPago()));
			if (FactElec.getDGI().getIDDocFchVenc() != null && FactElec.getDGI().getIDDocFchVenc().compareTo(new Date(0)) != 0) {
				ItemCFE.getEncabezado().getIdDoc().setFchaVenc(FactElec.getDGI().getIDDocFchVenc());
				ItemCFE.getEncabezado().getIdDoc().setFchVencSpecified(true);
			}
			ItemCFE.getEncabezado().getIdDoc().setClauVenta(FactElec.getDGI().getIDDocClausulaVenta());

			// En Las NC/ND se puede omitir la modalidad de venta
			if (FactElec.getDGI().getIDDocModalidadVenta() != 0) {
				ItemCFE.getEncabezado().getIdDoc().setModVenta(FactElec.getDGI().getIDDocModalidadVenta());
				ItemCFE.getEncabezado().getIdDoc().setModVentaSpecified(true);
			}

			ItemCFE.getEncabezado().getIdDoc().setViaTransp(FactElec.getDGI().getIDDocViaTransporte());
			ItemCFE.getEncabezado().getIdDoc().setViaTranspSpecified(true);
			ItemCFE.getEncabezado().getIdDoc().setInfoAdicionalDoc(FactElec.getDGI().getIDDocInfoAdicionalDoc());

			if (FactElec.getDGI().getIDDocIVAalDia() == 1) {
				ItemCFE.getEncabezado().getIdDoc().setIVAalDia(String.valueOf(FactElec.getDGI().getIDDocIVAalDia()));
			}
			if (FactElec.getDGI().getIDDocSecProf() == 1) {
				ItemCFE.getEncabezado().getIdDoc().setSecProf(String.valueOf(FactElec.getDGI().getIDDocSecProf()));
			}

			//Emisor
			ItemCFE.getEncabezado().setEmisor(Emisor);
			ItemCFE.getEncabezado().getEmisor().setRUCEmisor(FactElec.getDGI().getEMIRUCEmisor());
			ItemCFE.getEncabezado().getEmisor().setRznSoc(FactElec.getDGI().getEMIRznSoc());
			ItemCFE.getEncabezado().getEmisor().setNomComercial(FactElec.getDGI().getEMINomComercial());
			ItemCFE.getEncabezado().getEmisor().setGiroEmis(FactElec.getDGI().getEMIGiroEmis());
			if (!((FactElec.getDGI().getEmiTelefonos() == null))) {
                java.util.ArrayList<String> ListTelefonos = new java.util.ArrayList<String>();
                for (XMLFACTURA.clsDGI.EmiTelefonos.EMITelefono Tel : FactElec.getDGI().getEmiTelefonos().getEMITelefono()) {
                    ListTelefonos.add(Tel.getEMITelefono());
                }
                String[] tempListTelefonos = new String[ListTelefonos.size()];
                tempListTelefonos = ListTelefonos.toArray(tempListTelefonos);
                ItemCFE.getEncabezado().getEmisor().setTelefono(ListTelefonos);
            }
			ItemCFE.getEncabezado().getEmisor().setCorreoEmisor(FactElec.getDGI().getEMICorreoEmisor());
			ItemCFE.getEncabezado().getEmisor().setEmiSucursal(FactElec.getDGI().getEMIEmiSucursal());
			ItemCFE.getEncabezado().getEmisor().setCdgDGISucur(String.valueOf(FactElec.getDGI().getEMICdgDGISucur()));
			ItemCFE.getEncabezado().getEmisor().setDomFiscal(FactElec.getDGI().getEMIDomFiscal());
			ItemCFE.getEncabezado().getEmisor().setCiudad(FactElec.getDGI().getEMICiudad());
			ItemCFE.getEncabezado().getEmisor().setDepartamento(FactElec.getDGI().getEMIDepartamento());
			ItemCFE.getEncabezado().getEmisor().setInfoAdicionalEmisor(FactElec.getDGI().getEMIInfoAdicionalEmisor());

			//Receptor
			if (FactElec.getDGI().getRECTipoDocRecep() != 0) {
				ItemCFE.getEncabezado().setReceptor(Receptor);
				if (FactElec.getDGI().getRECTipoDocRecep() != 0) {
					ItemCFE.getEncabezado().getReceptor().setTipoDocRecep(EnvioCFE.DocType.forValue(FactElec.getDGI().getRECTipoDocRecep()));
					ItemCFE.getEncabezado().getReceptor().setTipoDocRecepSpecified(true);
					if (FactElec.getDGI().getRECTipoDocRecep() == 4 || FactElec.getDGI().getRECTipoDocRecep() == 5 || FactElec.getDGI().getRECTipoDocRecep() == 6 || FactElec.getDGI().getRECTipoDocRecep() == 7) {
						ItemCFE.getEncabezado().getReceptor().setDocRecepExt(FactElec.getDGI().getRECDocRecepExtranjero());
					} else {
						ItemCFE.getEncabezado().getReceptor().setDocRecep(FactElec.getDGI().getRECDocRecep());

					}
				}
				if (!FactElec.getDGI().getRECCodPaisRecep().equals("")) {
					ItemCFE.getEncabezado().getReceptor().setCodPaisRecep(FactElec.getDGI().getRECCodPaisRecep());
					ItemCFE.getEncabezado().getReceptor().setCodPaisRecepSpecified(true);
				}

				ItemCFE.getEncabezado().getReceptor().setRznSocRecep(FactElec.getDGI().getRECRznSocRecep());
				ItemCFE.getEncabezado().getReceptor().setDirRecep(FactElec.getDGI().getRECDirRecep());
				ItemCFE.getEncabezado().getReceptor().setCiudadRecep(FactElec.getDGI().getRECCiudadRecep());
				ItemCFE.getEncabezado().getReceptor().setDeptoRecep(FactElec.getDGI().getRECDeptoRecep());
				ItemCFE.getEncabezado().getReceptor().setPaisRecep(FactElec.getDGI().getRECPaisRecep());
				if (FactElec.getDGI().getRECCP() != 0) {
					ItemCFE.getEncabezado().getReceptor().setCP(String.valueOf(FactElec.getDGI().getRECCP()));
				}
				ItemCFE.getEncabezado().getReceptor().setInfoAdicional(FactElec.getDGI().getRECInfoAdicional());
				ItemCFE.getEncabezado().getReceptor().setLugarDestEnt(FactElec.getDGI().getRECLugarDestinoEntrega());
				ItemCFE.getEncabezado().getReceptor().setCompraID(FactElec.getDGI().getRECNroOrdenCompra());
			}

			//Totales
			ItemCFE.getEncabezado().setTotales(Totales);
			ItemCFE.getEncabezado().getTotales().setTpoMoneda(FactElec.getDGI().getTOTTpoMoneda());
			if (FactElec.getDGI().getTOTTpoCambio() != 0) {
				ItemCFE.getEncabezado().getTotales().setTpoCambio(new BigDecimal(FactElec.getDGI().getTOTTpoCambio()));
				ItemCFE.getEncabezado().getTotales().setTpoCambioSpecified(true);
			}
			ItemCFE.getEncabezado().getTotales().setMntExpoyAsim(new BigDecimal(FactElec.getDGI().getTOTMntExpoyAsim()));
			ItemCFE.getEncabezado().getTotales().setMntTotal(new BigDecimal(FactElec.getDGI().getTOTMntTotal()));
			ItemCFE.getEncabezado().getTotales().setCantLinDet(String.valueOf(FactElec.getDGI().getTOTCantLinDet()));
			if (FactElec.getDGI().getTOTMontoNF() != 0) {
				ItemCFE.getEncabezado().getTotales().setMontoNF(new BigDecimal(FactElec.getDGI().getTOTMontoNF()));
				ItemCFE.getEncabezado().getTotales().setMontoNFSpecified(true);
			}
			ItemCFE.getEncabezado().getTotales().setMntPagar(new BigDecimal(FactElec.getDGI().getTOTMntPagar()));

			//B - Detalle de productos o servicios
			List<EnvioCFE.Item_Det_Fact_Exp.Item> DetalleItemsList = new ArrayList<EnvioCFE.Item_Det_Fact_Exp.Item>();
			for (XMLFACTURA.clsDGI.Detalle.DETALLE DetFact : FactElec.getDGI().getDetalles().getDetalle()) {
				java.util.ArrayList<EnvioCFE.Item_Det_Fact_ExpSubDescuento> ListSubDesc = new java.util.ArrayList<EnvioCFE.Item_Det_Fact_ExpSubDescuento>();
                java.util.ArrayList<EnvioCFE.Item_Det_Fact_ExpSubRecargo> ListSubRec = new java.util.ArrayList<EnvioCFE.Item_Det_Fact_ExpSubRecargo>();
                java.util.ArrayList<EnvioCFE.Item_Det_Fact_ExpCodItem> ListCodItem = new java.util.ArrayList<EnvioCFE.Item_Det_Fact_ExpCodItem>();
                EnvioCFE.Item_Det_Fact_Exp.Item DetalleItem = Detalle.new Item();
				
				DetalleItem.setNroLinDet(String.valueOf(DetFact.getDETNroLinDet()));
				if (!((DetFact.getDETCodItems() == null))) {
                    for (XMLFACTURA.clsDGI.Detalle.DETCodItems.DETCodItem CodItem : DetFact.getDETCodItems().getDETCodItem()) {
                        EnvioCFE.Item_Det_Fact_ExpCodItem NewCodItem = EnvCFE.argValue.new Item_Det_Fact_ExpCodItem();
                        NewCodItem.setCod(CodItem.getDETCod());
                        NewCodItem.setTpoCod(CodItem.getDETTpoCod());
                        ListCodItem.add(NewCodItem);
                    }
                    EnvioCFE.Item_Det_Fact_ExpCodItem[] tempListCodItem = new EnvioCFE.Item_Det_Fact_ExpCodItem[ListCodItem.size()];
                    tempListCodItem = ListCodItem.toArray(tempListCodItem);
                    DetalleItem.setCodItem(ListCodItem);
                }
				if (DetFact.getDETIndFact() != 0) {
					DetalleItem.setIndFact(EnvioCFE.Item_Det_Fact_ExpIndFact.forValue(DetFact.getDETIndFact()));
				}

				DetalleItem.setNomItem(DetFact.getDETNomItem());
				if (!((DetFact.getDETDscItem() == null)) && !DetFact.getDETDscItem().trim().equals("")) {
					DetalleItem.setDscItem(DetFact.getDETDscItem());
				}
				DetalleItem.setCantidad(new BigDecimal(DetFact.getDETCantidad()));
				DetalleItem.setUniMed(DetFact.getDETUniMed());
				DetalleItem.setPrecioUnitario(new BigDecimal(DetFact.getDETPrecioUnitario()));
				if (DetFact.getDETDescuentoPct() != 0) {
					DetalleItem.setDescuentoPct(new BigDecimal(DetFact.getDETDescuentoPct()));
					DetalleItem.setDescuentoPctSpecified(true);
				}
				if (DetFact.getDETDescuentoMonto() != 0) {
					DetalleItem.setDescuentoMonto(new BigDecimal(DetFact.getDETDescuentoMonto()));
					DetalleItem.setDescuentoMontoSpecified(true);
				}
				if (!((DetFact.getDETSubDescuentos() == null))) {
					for (XMLFACTURA.clsDGI.Detalle.DETSubDescuentos.DETSubDescuento SubDesc : DetFact.getDETSubDescuentos().getDETSubDescuento()) {
						EnvioCFE.Item_Det_Fact_ExpSubDescuento NewSubDesc = EnvCFE.argValue.new Item_Det_Fact_ExpSubDescuento();
						NewSubDesc.setDescTipo(String.valueOf(SubDesc.getDETDescTipo()));
						NewSubDesc.setDescVal(new BigDecimal(SubDesc.getDETDescVal()));
						ListSubDesc.add(NewSubDesc);
					}
					EnvioCFE.Item_Det_Fact_ExpSubDescuento[] tempListSubDesc = new EnvioCFE.Item_Det_Fact_ExpSubDescuento[ListSubDesc.size()];
					tempListSubDesc = ListSubDesc.toArray(tempListSubDesc);
					DetalleItem.setSubDescuento(ListSubDesc);
				}
				if (DetFact.getDETRecargoPct() != 0) {
                    DetalleItem.setRecargoPct(new BigDecimal(DetFact.getDETRecargoPct()));
                    DetalleItem.setRecargoPctSpecified(true);
                }
                if (DetFact.getDETRecargoMnt() != 0) {
                    DetalleItem.setRecargoMnt(new BigDecimal(DetFact.getDETRecargoMnt()));
                    DetalleItem.setRecargoMntSpecified(true);
                }
				if (!((DetFact.getDETSubRecargos() == null))) {
                    for (XMLFACTURA.clsDGI.Detalle.DETSubRecargos.DETSubRecargo SubRec : DetFact.getDETSubRecargos().getDETSubRecargo()) {
                        EnvioCFE.Item_Det_Fact_ExpSubRecargo NewSubRec = EnvCFE.argValue.new Item_Det_Fact_ExpSubRecargo();
                        NewSubRec.setRecargoTipo(EnvioCFE.Item_Det_Fact_ExpSubRecargoRecargoTipo.forValue(SubRec.getDETRecargoTipo()));
                        NewSubRec.setRecargoVal(new BigDecimal(SubRec.getDETRecargoVal()));
                        ListSubRec.add(NewSubRec);
                    }
                    EnvioCFE.Item_Det_Fact_ExpSubRecargo[] tempListSubRec = new EnvioCFE.Item_Det_Fact_ExpSubRecargo[ListSubRec.size()];
                    tempListSubRec = ListSubRec.toArray(tempListSubRec);
                    DetalleItem.setSubRecargo(ListSubRec);
                }
                DetalleItem.setMontoItem(new BigDecimal(DetFact.getDETMontoItem()));

                DetalleItemsList.add(DetalleItem);
            }

			Detalle.setItem(DetalleItemsList);
			ItemCFE.setDetalle(Detalle);

			//C - SubTotales Informativos
            if (!((FactElec.getDGI().getSubTotalInfos() == null))) {
                EnvioCFE.SubTotInfoSTI_Item SubTotInf = EnvCFE.argValue.new SubTotInfoSTI_Item();
                List<EnvioCFE.SubTotInfoSTI_Item.STI_Item> ListSubTot = new ArrayList<EnvioCFE.SubTotInfoSTI_Item.STI_Item>();
                for (int SubTot = 0; SubTot < FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().size(); SubTot++) {
                    EnvioCFE.SubTotInfoSTI_Item.STI_Item NewSubTot = SubTotInf.new STI_Item();
                    NewSubTot.setNroSTI(String.valueOf(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBNroSTI()));
                    NewSubTot.setGlosaSTI(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBGlosaSTI());
                    NewSubTot.setOrdenSTI(String.valueOf(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBOrdenSTI()));
                    NewSubTot.setValSubtotSTI(new BigDecimal(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBValSubtotSTI()));
                    ListSubTot.add(NewSubTot);
                }
                SubTotInf.setItem(ListSubTot);
                ItemCFE.setSubTotInfo(SubTotInf);
            }

			//D - Descuentos y Recargos
            if (!((FactElec.getDGI().getDscRcgGlobales() == null))) {
                EnvioCFE.DscRcgGlobalDRG_Item DscRcgGlob = EnvCFE.argValue.new DscRcgGlobalDRG_Item();
                List<EnvioCFE.DscRcgGlobalDRG_Item.DRG_Item> ListDscRcgGlob = new ArrayList<EnvioCFE.DscRcgGlobalDRG_Item.DRG_Item>();
                for (int DscRcgGlobCount = 0; DscRcgGlobCount < FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().size(); DscRcgGlobCount++) {
                    EnvioCFE.DscRcgGlobalDRG_Item.DRG_Item NewDscRcgGlob = DscRcgGlob.new DRG_Item();
                    NewDscRcgGlob.setNroLinDR(String.valueOf(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGNroLinDR()));
                    NewDscRcgGlob.setTpoMovDR(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGTpoMovDR());
                    if (FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGTpoDR() > 0) {
                        NewDscRcgGlob.setTpoDR(EnvioCFE.TipoDRType.forValue(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGTpoDR()));
                        NewDscRcgGlob.setTpoDRSpecified(true);
                    }
                    NewDscRcgGlob.setCodDR(String.valueOf(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGCodDR()));
                    NewDscRcgGlob.setGlosaDR(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGGlosaDR());
                    NewDscRcgGlob.setValorDR(new BigDecimal(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGValorDR()));
                    if (FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGIndFactDR() > 0) {
                        NewDscRcgGlob.setIndFactDR(EnvioCFE.DscRcgGlobalDRG_ItemIndFactDR.forValue(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGIndFactDR()));
                        NewDscRcgGlob.setIndFactDRSpecified(true);
                    }
                    ListDscRcgGlob.add(NewDscRcgGlob);
                }
                DscRcgGlob.setItem(ListDscRcgGlob);
                ItemCFE.setDscRcgGlobal(DscRcgGlob);
            }

			//E - Medios de Pago
            if (!((FactElec.getDGI().getMediosPago() == null))) {
                EnvioCFE.MediosPagoMedioPago MedioPago = EnvCFE.argValue.new MediosPagoMedioPago();
                List<EnvioCFE.MediosPagoMedioPago.MedioPago_Item> ListMedioPago = new ArrayList<EnvioCFE.MediosPagoMedioPago.MedioPago_Item>();
                for (int MedioPagoCount = 0; MedioPagoCount < FactElec.getDGI().getMediosPago().getMedioPago().size(); MedioPagoCount++) {
                    EnvioCFE.MediosPagoMedioPago.MedioPago_Item NewMedioPago = MedioPago.new MedioPago_Item();
                    NewMedioPago.setNroLinMP(String.valueOf(FactElec.getDGI().getMediosPago().getMedioPago().get(MedioPagoCount).getMPNroLinMP()));
                    NewMedioPago.setCodMP(String.valueOf(FactElec.getDGI().getMediosPago().getMedioPago().get(MedioPagoCount).getMPCodMP()));
                    NewMedioPago.setGlosaMP(FactElec.getDGI().getMediosPago().getMedioPago().get(MedioPagoCount).getMPGlosaMP());
                    NewMedioPago.setOrdenMP(String.valueOf(FactElec.getDGI().getMediosPago().getMedioPago().get(MedioPagoCount).getMPOrdenMP()));
                    NewMedioPago.setValorPago(new BigDecimal(FactElec.getDGI().getMediosPago().getMedioPago().get(MedioPagoCount).getMPValorPago()));
                    ListMedioPago.add(NewMedioPago);
                }
                MedioPago.setItem(ListMedioPago);
                ItemCFE.setMediosPago(MedioPago);
            }

			//F - Informacion de referencia
            if (!((FactElec.getDGI().getReferencias() == null))) {
                EnvioCFE.ReferenciaReferencia Referencias = EnvCFE.argValue.new ReferenciaReferencia();
                List<EnvioCFE.ReferenciaReferencia.Referencia_Item> ListReferencias = new ArrayList<EnvioCFE.ReferenciaReferencia.Referencia_Item>();
                for (int Referencia = 0; Referencia < FactElec.getDGI().getReferencias().getReferencia().size(); Referencia++) {
                    EnvioCFE.ReferenciaReferencia.Referencia_Item NewReferencia = Referencias.new Referencia_Item();
                    NewReferencia.setNroLinRef(String.valueOf(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroLinRef()));
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal() != null) {
                        if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal() != null && !FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal().equals("")) {
                            int referGlobal = Integer.parseInt(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal());
                            NewReferencia.setIndGlobal(referGlobal);
                            NewReferencia.setIndGlobalSpecified(true);
                        }
                    }
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFTpoDocRef() != 0) {
                        NewReferencia.setTpoDocRef(EnvioCFE.CFEType.forValue(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFTpoDocRef()));
                        NewReferencia.setTpoDocRefSpecified(true);
                    }
                    NewReferencia.setSerie(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFSerie());
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroCFERef() != null) {
                        NewReferencia.setNroCFERef(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroCFERef());
                    }
                    NewReferencia.setRazonRef(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFRazonRef());
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFFechaCFEref() != null) {
                        NewReferencia.setFechaCFEref(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFFechaCFEref());
                    }
                    ListReferencias.add(NewReferencia);
                }
                Referencias.setItem(ListReferencias);
                ItemCFE.setReferencia(Referencias);
            }

			//G - CAE
			ItemCFE.setCAEData(CAEData);

			CFE.setItem(ItemCFE);
            List<EnvioCFE.CFEDefType> listCFE = new ArrayList<EnvioCFE.CFEDefType>();
            listCFE.add(CFE);
            EnvCFE.argValue.setCFE(listCFE);

            LecturaOk.argValue = true;

		} catch (RuntimeException ex) {
			ErrorMsg.argValue = ((ex.getMessage().equals("")) ? ex.getMessage() : ex.getMessage()).toString();
		}
	}

	private void XMLEntrada_a_DGI_E_Rem_Exp(XMLFACTURA FactElec, tangible.RefObject<EnvioCFE> EnvCFE, tangible.RefObject<Boolean> LecturaOk, tangible.RefObject<String> ErrorMsg) {
		EnvioCFE.CFEDefTypeERem_Exp ItemCFE = EnvCFE.argValue.new CFEDefTypeERem_Exp();
		EnvioCFE.CFEDefType CFE = EnvCFE.argValue.new CFEDefType();
		EnvioCFE.Emisor Emisor = EnvCFE.argValue.new Emisor();
		EnvioCFE.Receptor_Rem_Exp Receptor = EnvCFE.argValue.new Receptor_Rem_Exp();
		EnvioCFE.CFEDefTypeERem_ExpEncabezado Encabezado = EnvCFE.argValue.new CFEDefTypeERem_ExpEncabezado();
		EnvioCFE.Totales_Rem_Exp Totales = EnvCFE.argValue.new Totales_Rem_Exp();
		EnvioCFE.Item_Rem_Exp Detalle = EnvCFE.argValue.new Item_Rem_Exp();
		EnvioCFE.CAEDataType CAEData = EnvCFE.argValue.new CAEDataType();
		EnvioCFE.IdDoc_Rem_Exp IdDoc = EnvCFE.argValue.new IdDoc_Rem_Exp();

		try {
			LecturaOk.argValue = false;

			//A - Encabezado
			ItemCFE.setEncabezado(Encabezado);

			//Identificacion del comprobante
			ItemCFE.getEncabezado().setIdDoc(IdDoc);
			ItemCFE.getEncabezado().getIdDoc().setTipoCFE(EnvioCFE.IdDoc_Rem_ExpTipoCFE.forValue(FactElec.getDGI().getIDDocTipoCFE()));
			ItemCFE.getEncabezado().getIdDoc().setFchEmis(FactElec.getDGI().getIDDocFchEmis());
			ItemCFE.getEncabezado().getIdDoc().setTipoTraslado(EnvioCFE.IdDoc_Rem_ExpTipoTraslado.forValue(FactElec.getDGI().getIDDocTipoTraslado()));
			ItemCFE.getEncabezado().getIdDoc().setClauVenta(FactElec.getDGI().getIDDocClausulaVenta());
			if (FactElec.getDGI().getIDDocModalidadVenta() != 0) {
				ItemCFE.getEncabezado().getIdDoc().setModVenta(FactElec.getDGI().getIDDocModalidadVenta());
			}
			ItemCFE.getEncabezado().getIdDoc().setViaTransp(FactElec.getDGI().getIDDocViaTransporte());
			ItemCFE.getEncabezado().getIdDoc().setInfoAdicionalDoc(FactElec.getDGI().getIDDocInfoAdicionalDoc());

			if (FactElec.getDGI().getIDDocIndPropiedad() == 1) //mercaderia de terceros
			{
				ItemCFE.getEncabezado().getIdDoc().setIndPropiedad(EnvioCFE.IdDoc_IndPropiedadMercadTransp.forValue(FactElec.getDGI().getIDDocIndPropiedad()));
				if (FactElec.getDGI().getIDDocTipoDocPropSpecified()) {
					ItemCFE.getEncabezado().getIdDoc().setTipoDocProp(EnvioCFE.DocType.forValue(FactElec.getDGI().getIDDocTipoDocProp()));
					if (FactElec.getDGI().getIDDocTipoDocProp() == 4 || FactElec.getDGI().getIDDocTipoDocProp() == 5 || FactElec.getDGI().getIDDocTipoDocProp() == 6 || FactElec.getDGI().getIDDocTipoDocProp() == 7) {
						ItemCFE.getEncabezado().getIdDoc().setDocPropExt(FactElec.getDGI().getIDDocDocPropExt());
					} else {
						ItemCFE.getEncabezado().getIdDoc().setDocProp(FactElec.getDGI().getIDDocDocProp());
					}
				}

				if (FactElec.getDGI().getIDDocRznSocPropSpecified()) {
					ItemCFE.getEncabezado().getIdDoc().setRznSocProp(FactElec.getDGI().getIDDocRznSocProp());

				}
				if (FactElec.getDGI().getIDDocCodPaisPropSpecified()) {
					ItemCFE.getEncabezado().getIdDoc().setCodPaisProp(FactElec.getDGI().getIDDocCodPaisProp());
				}
			}

			ItemCFE.getEncabezado().getIdDoc().setInfoAdicionalDoc(FactElec.getDGI().getIDDocInfoAdicionalDoc());

			if (FactElec.getDGI().getIDDocSecProf() == 1) {
				ItemCFE.getEncabezado().getIdDoc().setSecProf(String.valueOf(FactElec.getDGI().getIDDocSecProf()));
			}

			//Emisor
			ItemCFE.getEncabezado().setEmisor(Emisor);
			ItemCFE.getEncabezado().getEmisor().setRUCEmisor(FactElec.getDGI().getEMIRUCEmisor());
			ItemCFE.getEncabezado().getEmisor().setRznSoc(FactElec.getDGI().getEMIRznSoc());
			ItemCFE.getEncabezado().getEmisor().setNomComercial(FactElec.getDGI().getEMINomComercial());
			ItemCFE.getEncabezado().getEmisor().setGiroEmis(FactElec.getDGI().getEMIGiroEmis());
			if (!((FactElec.getDGI().getEmiTelefonos() == null))) {
                java.util.ArrayList<String> ListTelefonos = new java.util.ArrayList<String>();
                for (XMLFACTURA.clsDGI.EmiTelefonos.EMITelefono Tel : FactElec.getDGI().getEmiTelefonos().getEMITelefono()) {
                    ListTelefonos.add(Tel.getEMITelefono());
                }

                String[] ListReferencias = new String[ListTelefonos.size()];
                ListReferencias = ListTelefonos.toArray(ListReferencias);
                ItemCFE.getEncabezado().getEmisor().setTelefono(ListTelefonos);
            }
			ItemCFE.getEncabezado().getEmisor().setCorreoEmisor(FactElec.getDGI().getEMICorreoEmisor());
			ItemCFE.getEncabezado().getEmisor().setEmiSucursal(FactElec.getDGI().getEMIEmiSucursal());
			ItemCFE.getEncabezado().getEmisor().setCdgDGISucur(String.valueOf(FactElec.getDGI().getEMICdgDGISucur()));
			ItemCFE.getEncabezado().getEmisor().setDomFiscal(FactElec.getDGI().getEMIDomFiscal());
			ItemCFE.getEncabezado().getEmisor().setCiudad(FactElec.getDGI().getEMICiudad());
			ItemCFE.getEncabezado().getEmisor().setDepartamento(FactElec.getDGI().getEMIDepartamento());
			ItemCFE.getEncabezado().getEmisor().setInfoAdicionalEmisor(FactElec.getDGI().getEMIInfoAdicionalEmisor());

			//Receptor
						if (FactElec.getDGI().getRECTipoDocRecep() != 0) {
				ItemCFE.getEncabezado().setReceptor(Receptor);
				if (FactElec.getDGI().getRECTipoDocRecep() != 0) {
					ItemCFE.getEncabezado().getReceptor().setTipoDocRecep(EnvioCFE.DocType.forValue(FactElec.getDGI().getRECTipoDocRecep()));
					ItemCFE.getEncabezado().getReceptor().setTipoDocRecepSpecified(true);
					if (FactElec.getDGI().getRECTipoDocRecep() == 4 || FactElec.getDGI().getRECTipoDocRecep() == 5 || FactElec.getDGI().getRECTipoDocRecep() == 6) {
						ItemCFE.getEncabezado().getReceptor().setDocRecepExt(FactElec.getDGI().getRECDocRecepExtranjero());
					} else {
						ItemCFE.getEncabezado().getReceptor().setDocRecep(FactElec.getDGI().getRECDocRecep());
					}
				}
				if (!FactElec.getDGI().getRECCodPaisRecep().equals("")) {
					ItemCFE.getEncabezado().getReceptor().setCodPaisRecep(FactElec.getDGI().getRECCodPaisRecep());
					ItemCFE.getEncabezado().getReceptor().setCodPaisRecepSpecified(true);
				}

				ItemCFE.getEncabezado().getReceptor().setRznSocRecep(FactElec.getDGI().getRECRznSocRecep());
				ItemCFE.getEncabezado().getReceptor().setDirRecep(FactElec.getDGI().getRECDirRecep());
				ItemCFE.getEncabezado().getReceptor().setCiudadRecep(FactElec.getDGI().getRECCiudadRecep());
				ItemCFE.getEncabezado().getReceptor().setDeptoRecep(FactElec.getDGI().getRECDeptoRecep());
				ItemCFE.getEncabezado().getReceptor().setPaisRecep(FactElec.getDGI().getRECPaisRecep());
				if (FactElec.getDGI().getRECCP() != 0) {
					ItemCFE.getEncabezado().getReceptor().setCP(String.valueOf(FactElec.getDGI().getRECCP()));
				}
				ItemCFE.getEncabezado().getReceptor().setInfoAdicional(FactElec.getDGI().getRECInfoAdicional());
				ItemCFE.getEncabezado().getReceptor().setLugarDestEnt(FactElec.getDGI().getRECLugarDestinoEntrega());
				ItemCFE.getEncabezado().getReceptor().setCompraID(FactElec.getDGI().getRECNroOrdenCompra());
			}

			//Totales
			ItemCFE.getEncabezado().setTotales(Totales);
			ItemCFE.getEncabezado().getTotales().setTpoMoneda(FactElec.getDGI().getTOTTpoMoneda());
			if (FactElec.getDGI().getTOTTpoCambio() != 0) {
				ItemCFE.getEncabezado().getTotales().setTpoCambio(new BigDecimal(FactElec.getDGI().getTOTTpoCambio()));
				ItemCFE.getEncabezado().getTotales().setTpoCambioSpecified(true);
			}
			ItemCFE.getEncabezado().getTotales().setMntExpoyAsim(new BigDecimal(FactElec.getDGI().getTOTMntExpoyAsim()));
			ItemCFE.getEncabezado().getTotales().setMntTotal(new BigDecimal(FactElec.getDGI().getTOTMntTotal()));
			ItemCFE.getEncabezado().getTotales().setMntPagar(new BigDecimal(FactElec.getDGI().getTOTMntPagar()));
			ItemCFE.getEncabezado().getTotales().setCantLinDet(String.valueOf(FactElec.getDGI().getTOTCantLinDet()));

			//B - Detalle de productos o servicios
			List<EnvioCFE.Item_Rem_Exp.Item> DetalleItemsList = new ArrayList<EnvioCFE.Item_Rem_Exp.Item>();
            for (XMLFACTURA.clsDGI.Detalle.DETALLE DetFact : FactElec.getDGI().getDetalles().getDetalle()) {
                java.util.ArrayList<EnvioCFE.Item_Det_Fact_ExpSubDescuento> ListSubDesc = new java.util.ArrayList<EnvioCFE.Item_Det_Fact_ExpSubDescuento>();
                java.util.ArrayList<EnvioCFE.Item_Det_Fact_ExpSubRecargo> ListSubRec = new java.util.ArrayList<EnvioCFE.Item_Det_Fact_ExpSubRecargo>();
                java.util.ArrayList<EnvioCFE.RetPerc> ListRetPerc = new java.util.ArrayList<EnvioCFE.RetPerc>();
                java.util.ArrayList<EnvioCFE.Item_Rem_ExpCodItem> ListCodItem = new java.util.ArrayList<EnvioCFE.Item_Rem_ExpCodItem>();

                EnvioCFE.Item_Rem_Exp.Item DetalleItem = Detalle.new Item();

				DetalleItem.setNroLinDet(String.valueOf(DetFact.getDETNroLinDet()));
				if (!((DetFact.getDETCodItems() == null))) {
                    for (XMLFACTURA.clsDGI.Detalle.DETCodItems.DETCodItem CodItem : DetFact.getDETCodItems().getDETCodItem()) {
                        EnvioCFE.Item_Rem_ExpCodItem NewCodItem = EnvCFE.argValue.new Item_Rem_ExpCodItem();
                        NewCodItem.setCod(CodItem.getDETCod());
                        NewCodItem.setTpoCod(CodItem.getDETTpoCod());
                        ListCodItem.add(NewCodItem);
                    }
                    EnvioCFE.Item_Rem_ExpCodItem[] ListListCodItem = new EnvioCFE.Item_Rem_ExpCodItem[ListCodItem.size()];
                    ListListCodItem = ListCodItem.toArray(ListListCodItem);
                    DetalleItem.setCodItem(ListCodItem);
                }
				if (DetFact.getDETIndFact() == 0) {
					DetalleItem.setIndFactSpecified(false);
				} else {
					DetalleItem.setIndFactSpecified(true);
					DetalleItem.setIndFact(EnvioCFE.Item_Rem_ExpIndFact.forValue(DetFact.getDETIndFact()));
				}
				DetalleItem.setNomItem(DetFact.getDETNomItem());
				if (!((DetFact.getDETDscItem() == null)) && !DetFact.getDETDscItem().trim().equals("")) {
					DetalleItem.setDscItem(DetFact.getDETDscItem());
				}
				DetalleItem.setCantidad(new BigDecimal(DetFact.getDETCantidad()));
				DetalleItem.setUniMed(DetFact.getDETUniMed());
				DetalleItem.setPrecioUnitario(new BigDecimal(DetFact.getDETPrecioUnitario()));
				DetalleItem.setMontoItem(new BigDecimal(DetFact.getDETMontoItem()));
				DetalleItemsList.add(DetalleItem);
			}
			
			Detalle.setItem(DetalleItemsList);
			ItemCFE.setDetalle(Detalle);

			 //C - SubTotales Informativos
            if (!((FactElec.getDGI().getSubTotalInfos() == null))) {
                EnvioCFE.SubTotInfoSTI_Item SubTotInf = EnvCFE.argValue.new SubTotInfoSTI_Item();
                List<EnvioCFE.SubTotInfoSTI_Item.STI_Item> ListSubTot = new ArrayList<EnvioCFE.SubTotInfoSTI_Item.STI_Item>();
                for (int SubTot = 0; SubTot < FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().size(); SubTot++) {
                    EnvioCFE.SubTotInfoSTI_Item.STI_Item NewSubTot = SubTotInf.new STI_Item();
                    NewSubTot.setNroSTI(String.valueOf(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBNroSTI()));
                    NewSubTot.setGlosaSTI(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBGlosaSTI());
                    NewSubTot.setOrdenSTI(String.valueOf(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBOrdenSTI()));
                    NewSubTot.setValSubtotSTI(new BigDecimal(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBValSubtotSTI()));
                    ListSubTot.add(NewSubTot);
                }
                SubTotInf.setItem(ListSubTot);
                ItemCFE.setSubTotInfo(SubTotInf);
            }

			//F - Informacion de referencia
            if (!((FactElec.getDGI().getReferencias() == null))) {
                EnvioCFE.ReferenciaReferencia Referencias = EnvCFE.argValue.new ReferenciaReferencia();
                List<EnvioCFE.ReferenciaReferencia.Referencia_Item> ListReferencias = new ArrayList<EnvioCFE.ReferenciaReferencia.Referencia_Item>();
                for (int Referencia = 0; Referencia < FactElec.getDGI().getReferencias().getReferencia().size(); Referencia++) {
                    EnvioCFE.ReferenciaReferencia.Referencia_Item NewReferencia = Referencias.new Referencia_Item();
                    NewReferencia.setNroLinRef(String.valueOf(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroLinRef()));
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal() != null) {
                        if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal() != null && !FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal().equals("")) {
                            int referGlobal = Integer.parseInt(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal());
                            NewReferencia.setIndGlobal(referGlobal);
                            NewReferencia.setIndGlobalSpecified(true);
                        }
                    }
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFTpoDocRef() != 0) {
                        NewReferencia.setTpoDocRef(EnvioCFE.CFEType.forValue(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFTpoDocRef()));
                        NewReferencia.setTpoDocRefSpecified(true);
                    }
                    NewReferencia.setSerie(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFSerie());
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroCFERef() != null) {
                        NewReferencia.setNroCFERef(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroCFERef());
                    }
                    NewReferencia.setRazonRef(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFRazonRef());
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFFechaCFEref() != null) {
                        NewReferencia.setFechaCFEref(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFFechaCFEref());
                    }
                    ListReferencias.add(NewReferencia);
                }
                Referencias.setItem(ListReferencias);
                ItemCFE.setReferencia(Referencias);
            }

			//G - CAE
			ItemCFE.setCAEData(CAEData);

			CFE.setItem(ItemCFE);
            List<EnvioCFE.CFEDefType> listCFE = new ArrayList<EnvioCFE.CFEDefType>();
            listCFE.add(CFE);
            EnvCFE.argValue.setCFE(listCFE);

            LecturaOk.argValue = true;

		} catch (RuntimeException ex) {
			ErrorMsg.argValue = ((ex.getMessage().equals("")) ? ex.getMessage() : ex.getMessage()).toString();
		}
	}

	private void XMLEntrada_a_DGI_E_Boleta(XMLFACTURA FactElec, tangible.RefObject<EnvioCFE> EnvCFE, tangible.RefObject<Boolean> LecturaOk, tangible.RefObject<String> ErrorMsg) {

		EnvioCFE.CFEDefTypeEBoleta ItemCFE = EnvCFE.argValue.new CFEDefTypeEBoleta();
		EnvioCFE.CFEDefType CFE = EnvCFE.argValue.new CFEDefType();
		EnvioCFE.Emisor Emisor = EnvCFE.argValue.new Emisor();
		EnvioCFE.Receptor_Tck Receptor = EnvCFE.argValue.new Receptor_Tck();
		EnvioCFE.CFEDefTypeEBoletaEncabezado Encabezado = EnvCFE.argValue.new CFEDefTypeEBoletaEncabezado();
		EnvioCFE.Totales_Boleta Totales = EnvCFE.argValue.new Totales_Boleta();
		EnvioCFE.Item_Det_Boleta Detalle = EnvCFE.argValue.new Item_Det_Boleta();
		EnvioCFE.CAEDataType CAEData = EnvCFE.argValue.new CAEDataType();
		EnvioCFE.IdDoc_Boleta IdDoc = EnvCFE.argValue.new IdDoc_Boleta();
		EnvioCFE.Compl_Fiscal_DataType ComplementoFiscalData = EnvCFE.argValue.new Compl_Fiscal_DataType();
		EnvioCFE.Compl_FiscalType ComplementoFiscal = EnvCFE.argValue.new Compl_FiscalType();

		try {
			LecturaOk.argValue = false;

			//A - Encabezado
			ItemCFE.setEncabezado(Encabezado);

			//Identificacion del comprobante
			ItemCFE.getEncabezado().setIdDoc(IdDoc);
			ItemCFE.getEncabezado().getIdDoc().setTipoCFE(EnvioCFE.IdDoc_BoletaTipoCFE.forValue(FactElec.getDGI().getIDDocTipoCFE()));
			ItemCFE.getEncabezado().getIdDoc().setFchEmis(FactElec.getDGI().getIDDocFchEmis());
			if (FactElec.getDGI().getIDDocPeriodoDesde() != null && FactElec.getDGI().getIDDocPeriodoDesde() != new Date(0)) {
				ItemCFE.getEncabezado().getIdDoc().setPeriodoDesde(FactElec.getDGI().getIDDocPeriodoDesde());
				ItemCFE.getEncabezado().getIdDoc().setPeriodoDesdeSpecified(true);
			}
			if (FactElec.getDGI().getIDDocPeriodoDesde() != null && FactElec.getDGI().getIDDocPeriodoHasta() != new Date(0)) {
				ItemCFE.getEncabezado().getIdDoc().setPeriodoHasta(FactElec.getDGI().getIDDocPeriodoHasta());
				ItemCFE.getEncabezado().getIdDoc().setPeriodoHastaSpecified(true);
			}
			if (FactElec.getDGI().getIDDocMntBruto() == 2) {
				ItemCFE.getEncabezado().getIdDoc().setMntBruto(EnvioCFE.IdDoc_BoletaMntBruto.forValue(FactElec.getDGI().getIDDocMntBruto()));
				ItemCFE.getEncabezado().getIdDoc().setMntBrutoSpecified(true);
			}
			ItemCFE.getEncabezado().getIdDoc().setFmaPago(EnvioCFE.IdDoc_TckFmaPago.forValue(FactElec.getDGI().getIDDocFmaPago()));
			if (FactElec.getDGI().getIDDocPeriodoDesde() != null && FactElec.getDGI().getIDDocFchVenc().compareTo(new Date(0)) != 0) {
				ItemCFE.getEncabezado().getIdDoc().setFchaVenc(FactElec.getDGI().getIDDocFchVenc());
				ItemCFE.getEncabezado().getIdDoc().setFchVencSpecified(true);
			}
			ItemCFE.getEncabezado().getIdDoc().setInfoAdicionalDoc(FactElec.getDGI().getIDDocInfoAdicionalDoc());

			//Emisor
			ItemCFE.getEncabezado().setEmisor(Emisor);
			ItemCFE.getEncabezado().getEmisor().setRUCEmisor(FactElec.getDGI().getEMIRUCEmisor());
			ItemCFE.getEncabezado().getEmisor().setRznSoc(FactElec.getDGI().getEMIRznSoc());
			ItemCFE.getEncabezado().getEmisor().setNomComercial(FactElec.getDGI().getEMINomComercial());
			ItemCFE.getEncabezado().getEmisor().setGiroEmis(FactElec.getDGI().getEMIGiroEmis());

			if (!((FactElec.getDGI().getEmiTelefonos() == null))) {
                List<XML.XMLFACTURA.clsDGI.EmiTelefonos.EMITelefono> telef = FactElec.getDGI().getEmiTelefonos().getEMITelefono();
                List<String> ListTelefonos = new ArrayList<String>();
                for (int i = 0; i < telef.size(); i++) {
                    ListTelefonos.add(telef.get(i).getEMITelefono());
                }
                ItemCFE.getEncabezado().getEmisor().setTelefono(ListTelefonos);
            }
			ItemCFE.getEncabezado().getEmisor().setCorreoEmisor(FactElec.getDGI().getEMICorreoEmisor());
			ItemCFE.getEncabezado().getEmisor().setEmiSucursal(FactElec.getDGI().getEMIEmiSucursal());
			ItemCFE.getEncabezado().getEmisor().setCdgDGISucur(String.valueOf(FactElec.getDGI().getEMICdgDGISucur()));
			ItemCFE.getEncabezado().getEmisor().setDomFiscal(FactElec.getDGI().getEMIDomFiscal());
			ItemCFE.getEncabezado().getEmisor().setCiudad(FactElec.getDGI().getEMICiudad());
			ItemCFE.getEncabezado().getEmisor().setDepartamento(FactElec.getDGI().getEMIDepartamento());
			ItemCFE.getEncabezado().getEmisor().setInfoAdicionalEmisor(FactElec.getDGI().getEMIInfoAdicionalEmisor());

			//Receptor
			if (FactElec.getDGI().getRECTipoDocRecep() != 0) {
				//Si existe un receptor, tiene que existir TipoDocRecep, Se deja dentro del IF, ya que no siempre existe el receptor
				ItemCFE.getEncabezado().setReceptor(Receptor);
				ItemCFE.getEncabezado().getReceptor().setTipoDocRecep(EnvioCFE.DocType.forValue(FactElec.getDGI().getRECTipoDocRecep()));
				ItemCFE.getEncabezado().getReceptor().setTipoDocRecepSpecified(true);
				if (!FactElec.getDGI().getRECCodPaisRecep().equals("")) {
					ItemCFE.getEncabezado().getReceptor().setCodPaisRecep(FactElec.getDGI().getRECCodPaisRecep());
					ItemCFE.getEncabezado().getReceptor().setCodPaisRecepSpecified(true);
				}
				if (FactElec.getDGI().getRECTipoDocRecep() == 4 || FactElec.getDGI().getRECTipoDocRecep() == 5 || FactElec.getDGI().getRECTipoDocRecep() == 6 || FactElec.getDGI().getRECTipoDocRecep() == 7) {
					ItemCFE.getEncabezado().getReceptor().setDocRecepExt(FactElec.getDGI().getRECDocRecepExtranjero());
				} else {
					ItemCFE.getEncabezado().getReceptor().setDocRecep(FactElec.getDGI().getRECDocRecep());
				}
				ItemCFE.getEncabezado().getReceptor().setRznSocRecep(FactElec.getDGI().getRECRznSocRecep());
				ItemCFE.getEncabezado().getReceptor().setDirRecep(FactElec.getDGI().getRECDirRecep());
				ItemCFE.getEncabezado().getReceptor().setCiudadRecep(FactElec.getDGI().getRECCiudadRecep());
				ItemCFE.getEncabezado().getReceptor().setDeptoRecep(FactElec.getDGI().getRECDeptoRecep());
				ItemCFE.getEncabezado().getReceptor().setPaisRecep(FactElec.getDGI().getRECPaisRecep());
				if (FactElec.getDGI().getRECCP() != 0) {
					ItemCFE.getEncabezado().getReceptor().setCP(String.valueOf(FactElec.getDGI().getRECCP()));
				}
				ItemCFE.getEncabezado().getReceptor().setInfoAdicional(FactElec.getDGI().getRECInfoAdicional());
				ItemCFE.getEncabezado().getReceptor().setLugarDestEnt(FactElec.getDGI().getRECLugarDestinoEntrega());
				ItemCFE.getEncabezado().getReceptor().setCompraID(FactElec.getDGI().getRECNroOrdenCompra());
			}

			//Totales
			ItemCFE.getEncabezado().setTotales(Totales);
			ItemCFE.getEncabezado().getTotales().setTpoMoneda(FactElec.getDGI().getTOTTpoMoneda());
			if (FactElec.getDGI().getTOTTpoCambio() != 0) {
				ItemCFE.getEncabezado().getTotales().setTpoCambio(new BigDecimal(FactElec.getDGI().getTOTTpoCambio()));
				ItemCFE.getEncabezado().getTotales().setTpoCambioSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntNoGrv() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntNoGrv(new BigDecimal(FactElec.getDGI().getTOTMntNoGrv()));
				ItemCFE.getEncabezado().getTotales().setMntNoGrvSpecified(true);
			}
			if (FactElec.getDGI().getTOTMntIVaenSusp() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntIVaenSusp(new BigDecimal(FactElec.getDGI().getTOTMntIVaenSusp()));
				ItemCFE.getEncabezado().getTotales().setMntIVaenSuspSpecified(true);
			}
			ItemCFE.getEncabezado().getTotales().setMntTotal(new BigDecimal(FactElec.getDGI().getTOTMntTotal()));
			if (FactElec.getDGI().getTOTMntTotRetenido() != 0) {
				ItemCFE.getEncabezado().getTotales().setMntTotRetenido(new BigDecimal(FactElec.getDGI().getTOTMntTotRetenido()));
				ItemCFE.getEncabezado().getTotales().setMntTotRetenidoSpecified(true);
			}
			ItemCFE.getEncabezado().getTotales().setCantLinDet(String.valueOf(FactElec.getDGI().getTOTCantLinDet()));
			if (!((FactElec.getDGI().getTOTRetencPerceps() == null))) {
                List<EnvioCFE.TotalesRetencPercep> ListRetencPerceps = new ArrayList<EnvioCFE.TotalesRetencPercep>();
                for (int i = 0; i < FactElec.getDGI().getTOTRetencPerceps().getOTRetencPercep().size(); i++) {
                    EnvioCFE.TotalesRetencPercep NewRetPrec = EnvCFE.argValue.new TotalesRetencPercep();
                    NewRetPrec.setCodRet(FactElec.getDGI().getTOTRetencPerceps().getOTRetencPercep().get(i).getTOTCodRet());
                    NewRetPrec.setValRetPerc(new BigDecimal(FactElec.getDGI().getTOTRetencPerceps().getOTRetencPercep().get(i).getTOTValRetPerc()));
                    ListRetencPerceps.add(NewRetPrec);
                }
                ItemCFE.getEncabezado().getTotales().setRetencPercep(ListRetencPerceps);
            }
			if (FactElec.getDGI().getTOTMontoNF() != 0) {
				ItemCFE.getEncabezado().getTotales().setMontoNF(new BigDecimal(FactElec.getDGI().getTOTMontoNF()));
				ItemCFE.getEncabezado().getTotales().setMontoNFSpecified(true);
			}
			ItemCFE.getEncabezado().getTotales().setMntPagar(new BigDecimal(FactElec.getDGI().getTOTMntPagar()));

			//B - Detalle de productos o servicios
			List<EnvioCFE.Item_Det_Boleta.Item> DetalleItemsList = new ArrayList<EnvioCFE.Item_Det_Boleta.Item>();
			int cont = 0;
			for (XMLFACTURA.clsDGI.Detalle.DETALLE DetFact : FactElec.getDGI().getDetalles().getDetalle()) {
				List<EnvioCFE.Item_Det_FactSubDescuento> ListSubDesc;
                List<EnvioCFE.Item_Det_FactSubRecargo> ListSubRec;
                List<EnvioCFE.RetPerc> ListRetPerc;
                List<EnvioCFE.Item_Det_FactCodItem> ListCodItem;

				EnvioCFE.Item_Det_Boleta.Item DetalleItem = Detalle.new Item();

				DetalleItem.setNroLinDet(String.valueOf(DetFact.getDETNroLinDet()));
				 if (!((DetFact.getDETCodItems() == null))) {
                    ListCodItem = new ArrayList<EnvioCFE.Item_Det_FactCodItem>();
                    for (int j = 0; j < DetFact.getDETCodItems().getDETCodItem().size(); j++) {
                        EnvioCFE.Item_Det_FactCodItem NewCodItem = EnvCFE.argValue.new Item_Det_FactCodItem();
                        NewCodItem.setCod(DetFact.getDETCodItems().getDETCodItem().get(j).getDETCod());
                        NewCodItem.setTpoCod(DetFact.getDETCodItems().getDETCodItem().get(j).getDETTpoCod());
                        ListCodItem.add(NewCodItem);
                    }
                    DetalleItem.setCodItem(ListCodItem);
                }
				if (DetFact.getDETIndFact() != 0) {
					DetalleItem.setIndFact(EnvioCFE.Item_Det_BoletaIndFact.forValue(DetFact.getDETIndFact()));
				}
				if (!((DetFact.getDETIndAgenteResp() == null)) && !DetFact.getDETIndAgenteResp().trim().equals("")) {
					DetalleItem.setIndAgenteResp(DetFact.getDETIndAgenteResp());
					DetalleItem.setIndAgenteRespSpecified(true);
				}
				DetalleItem.setNomItem(DetFact.getDETNomItem());
				if (!((DetFact.getDETDscItem() == null)) && !DetFact.getDETDscItem().trim().equals("")) {
					DetalleItem.setDscItem(DetFact.getDETDscItem());
				}
				DetalleItem.setCantidad(new BigDecimal(DetFact.getDETCantidad()));
				DetalleItem.setUniMed(String.valueOf(DetFact.getDETUniMed()));
				DetalleItem.setPrecioUnitario(new BigDecimal(DetFact.getDETPrecioUnitario()).setScale(6, RoundingMode.HALF_EVEN));
				if (DetFact.getDETDescuentoPct() != 0) {
					DetalleItem.setDescuentoPct(new BigDecimal(DetFact.getDETDescuentoPct()));
					DetalleItem.setDescuentoPctSpecified(true);
				}
				if (DetFact.getDETDescuentoMonto() != 0) {
					DetalleItem.setDescuentoMonto(new BigDecimal(DetFact.getDETDescuentoMonto()));
					DetalleItem.setDescuentoMontoSpecified(true);
				}
				if (!((DetFact.getDETSubDescuentos() == null))) {
                    ListSubDesc = new ArrayList<EnvioCFE.Item_Det_FactSubDescuento>();
                    for (int k = 0; k < DetFact.getDETSubDescuentos().getDETSubDescuento().size(); k++) {
                        EnvioCFE.Item_Det_FactSubDescuento NewSubDesc = EnvCFE.argValue.new Item_Det_FactSubDescuento();
                        NewSubDesc.setDescTipo(String.valueOf(DetFact.getDETSubDescuentos().getDETSubDescuento().get(k).getDETDescTipo()));
                        NewSubDesc.setDescVal(new BigDecimal(DetFact.getDETSubDescuentos().getDETSubDescuento().get(k).getDETDescVal()));
                        ListSubDesc.add(NewSubDesc);
                    }
                    DetalleItem.setSubDescuento(ListSubDesc);
                }
				if (DetFact.getDETRecargoPct() != 0) {
					DetalleItem.setRecargoPct(new BigDecimal(DetFact.getDETRecargoPct()));
					DetalleItem.setRecargoPctSpecified(true);
				}
				if (DetFact.getDETRecargoMnt() != 0) {
					DetalleItem.setRecargoMnt(new BigDecimal(DetFact.getDETRecargoMnt()));
					DetalleItem.setRecargoMntSpecified(true);
				}
				if (!((DetFact.getDETSubRecargos() == null))) {
                    ListSubRec = new ArrayList<EnvioCFE.Item_Det_FactSubRecargo>();
                    for (int m = 0; m < DetFact.getDETSubRecargos().getDETSubRecargo().size(); m++) {
                        EnvioCFE.Item_Det_FactSubRecargo NewSubRec = EnvCFE.argValue.new Item_Det_FactSubRecargo();
                        NewSubRec.setRecargoTipo(EnvioCFE.Item_Det_FactSubRecargoRecargoTipo.forValue(DetFact.getDETSubRecargos().getDETSubRecargo().get(m).getDETRecargoTipo()));
                        NewSubRec.setRecargoVal(new BigDecimal(DetFact.getDETSubRecargos().getDETSubRecargo().get(m).getDETRecargoVal()));
                        ListSubRec.add(NewSubRec);
                    }
                    DetalleItem.setSubRecargo(ListSubRec);
                }
				if (!((DetFact.getDETRetencPerceps() == null))) {
                    ListRetPerc = new ArrayList<EnvioCFE.RetPerc>();
                    for (int n = 0; n < DetFact.getDETRetencPerceps().getDETRetencPercep().size(); n++) {
                        EnvioCFE.RetPerc NewRetPerc = EnvCFE.argValue.new RetPerc();
                        NewRetPerc.setCodRet(DetFact.getDETRetencPerceps().getDETRetencPercep().get(n).getDETCodRet());
                        if (DetFact.getDETRetencPerceps().getDETRetencPercep().get(n).getDETTasa() > 0) {
                            NewRetPerc.setTasa(new BigDecimal(DetFact.getDETRetencPerceps().getDETRetencPercep().get(n).getDETTasa()));
                            NewRetPerc.setTasaSpecified(true);
                        }
                        if (DetFact.getDETRetencPerceps().getDETRetencPercep().get(n).getDETMntSujetoaRet() > 0) {
                            NewRetPerc.setMntSujetoaRet(new BigDecimal(DetFact.getDETRetencPerceps().getDETRetencPercep().get(n).getDETMntSujetoaRet()));
                            NewRetPerc.setMntSujetoaRetSpecified(true);
                        }
                        NewRetPerc.setValRetPerc(new BigDecimal(DetFact.getDETRetencPerceps().getDETRetencPercep().get(n).getDETValRetPerc()));
                        ListRetPerc.add(NewRetPerc);
                    }
                    DetalleItem.setRetencPercep(ListRetPerc);
                }
				 DetalleItem.setMontoItem(new BigDecimal(DetFact.getDETMontoItem()));
                DetalleItemsList.add(DetalleItem);
			}
			
			Detalle.setItem(DetalleItemsList);
            ItemCFE.setDetalle(Detalle);


			 //C - SubTotales Informativos
            if (!((FactElec.getDGI().getSubTotalInfos() == null))) {
                EnvioCFE.SubTotInfoSTI_Item SubTotInf = EnvCFE.argValue.new SubTotInfoSTI_Item();
                List<EnvioCFE.SubTotInfoSTI_Item.STI_Item> ListSubTot = new ArrayList<EnvioCFE.SubTotInfoSTI_Item.STI_Item>();
                for (int SubTot = 0; SubTot < FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().size(); SubTot++) {
                    EnvioCFE.SubTotInfoSTI_Item.STI_Item NewSubTot = SubTotInf.new STI_Item();
                    NewSubTot.setNroSTI(String.valueOf(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBNroSTI()));
                    NewSubTot.setGlosaSTI(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBGlosaSTI());
                    NewSubTot.setOrdenSTI(String.valueOf(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBOrdenSTI()));
                    NewSubTot.setValSubtotSTI(new BigDecimal(FactElec.getDGI().getSubTotalInfos().getSubTotalInfo().get(SubTot).getSUBValSubtotSTI()));
                    ListSubTot.add(NewSubTot);
                }
                SubTotInf.setItem(ListSubTot);
                ItemCFE.setSubTotInfo(SubTotInf);
            }

			//D - Descuentos y Recargos
            if (!((FactElec.getDGI().getDscRcgGlobales() == null))) {
                EnvioCFE.DscRcgGlobalDRG_Item DscRcgGlob = EnvCFE.argValue.new DscRcgGlobalDRG_Item();
                List<EnvioCFE.DscRcgGlobalDRG_Item.DRG_Item> ListDscRcgGlob = new ArrayList<EnvioCFE.DscRcgGlobalDRG_Item.DRG_Item>();
                for (int DscRcgGlobCount = 0; DscRcgGlobCount < FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().size(); DscRcgGlobCount++) {
                    EnvioCFE.DscRcgGlobalDRG_Item.DRG_Item NewDscRcgGlob = DscRcgGlob.new DRG_Item();
                    NewDscRcgGlob.setNroLinDR(String.valueOf(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGNroLinDR()));
                    NewDscRcgGlob.setTpoMovDR(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGTpoMovDR());
                    if (FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGTpoDR() > 0) {
                        NewDscRcgGlob.setTpoDR(EnvioCFE.TipoDRType.forValue(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGTpoDR()));
                        NewDscRcgGlob.setTpoDRSpecified(true);
                    }
                    NewDscRcgGlob.setCodDR(String.valueOf(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGCodDR()));
                    NewDscRcgGlob.setGlosaDR(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGGlosaDR());
                    NewDscRcgGlob.setValorDR(new BigDecimal(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGValorDR()));
                    if (FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGIndFactDR() > 0) {
                        NewDscRcgGlob.setIndFactDR(EnvioCFE.DscRcgGlobalDRG_ItemIndFactDR.forValue(FactElec.getDGI().getDscRcgGlobales().getDscRcgGlobal().get(DscRcgGlobCount).getDRGIndFactDR()));
                        NewDscRcgGlob.setIndFactDRSpecified(true);
                    }
                    ListDscRcgGlob.add(NewDscRcgGlob);
                }
                DscRcgGlob.setItem(ListDscRcgGlob);
                ItemCFE.setDscRcgGlobal(DscRcgGlob);
            }

			//E - Medios de Pago
            if (!((FactElec.getDGI().getMediosPago() == null))) {
                EnvioCFE.MediosPagoMedioPago MedioPago = EnvCFE.argValue.new MediosPagoMedioPago();
                List<EnvioCFE.MediosPagoMedioPago.MedioPago_Item> ListMedioPago = new ArrayList<EnvioCFE.MediosPagoMedioPago.MedioPago_Item>();
                for (int MedioPagoCount = 0; MedioPagoCount < FactElec.getDGI().getMediosPago().getMedioPago().size(); MedioPagoCount++) {
                    EnvioCFE.MediosPagoMedioPago.MedioPago_Item NewMedioPago = MedioPago.new MedioPago_Item();
                    NewMedioPago.setNroLinMP(String.valueOf(FactElec.getDGI().getMediosPago().getMedioPago().get(MedioPagoCount).getMPNroLinMP()));
                    NewMedioPago.setCodMP(String.valueOf(FactElec.getDGI().getMediosPago().getMedioPago().get(MedioPagoCount).getMPCodMP()));
                    NewMedioPago.setGlosaMP(FactElec.getDGI().getMediosPago().getMedioPago().get(MedioPagoCount).getMPGlosaMP());
                    NewMedioPago.setOrdenMP(String.valueOf(FactElec.getDGI().getMediosPago().getMedioPago().get(MedioPagoCount).getMPOrdenMP()));
                    NewMedioPago.setValorPago(new BigDecimal(FactElec.getDGI().getMediosPago().getMedioPago().get(MedioPagoCount).getMPValorPago()));
                    ListMedioPago.add(NewMedioPago);
                }
                MedioPago.setItem(ListMedioPago);
                ItemCFE.setMediosPago(MedioPago);
            }

			//F - Informacion de referencia
            if (!((FactElec.getDGI().getReferencias() == null))) {
                EnvioCFE.ReferenciaReferencia Referencias = EnvCFE.argValue.new ReferenciaReferencia();
                List<EnvioCFE.ReferenciaReferencia.Referencia_Item> ListReferencias = new ArrayList<EnvioCFE.ReferenciaReferencia.Referencia_Item>();
                for (int Referencia = 0; Referencia < FactElec.getDGI().getReferencias().getReferencia().size(); Referencia++) {
                    EnvioCFE.ReferenciaReferencia.Referencia_Item NewReferencia = Referencias.new Referencia_Item();
                    NewReferencia.setNroLinRef(String.valueOf(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroLinRef()));
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal() != null) {
                        if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal() != null && !FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal().equals("")) {
                            int referGlobal = Integer.parseInt(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFIndGlobal());
                            NewReferencia.setIndGlobal(referGlobal);
                            NewReferencia.setIndGlobalSpecified(true);
                        }
                    }
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFTpoDocRef() != 0) {
                        NewReferencia.setTpoDocRef(EnvioCFE.CFEType.forValue(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFTpoDocRef()));
                        NewReferencia.setTpoDocRefSpecified(true);
                    }
                    NewReferencia.setSerie(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFSerie());
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroCFERef() != null) {
                        NewReferencia.setNroCFERef(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFNroCFERef());
                    }
                    NewReferencia.setRazonRef(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFRazonRef());
                    if (FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFFechaCFEref() != null) {
                        NewReferencia.setFechaCFEref(FactElec.getDGI().getReferencias().getReferencia().get(Referencia).getREFFechaCFEref());
                    }
                    ListReferencias.add(NewReferencia);
                }
                Referencias.setItem(ListReferencias);
                ItemCFE.setReferencia(Referencias);
            }


			//G - CAE
			ItemCFE.setCAEData(CAEData);

			//K - Complemento Fiscal(Solo si es cuenta ajena)
			int IDDocTipoCFE = FactElec.getDGI().getIDDocTipoCFE();
			if (IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaCredCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaDebCuentaAjena.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaCredCuentaAjenaContingencia.getValue() || IDDocTipoCFE == enumTipoDeComprobanteCFE.eTicketNotaDebCuentaAjenaContingencia.getValue()) {
				ComplementoFiscalData.setRUCEmisor(FactElec.getDGI().getCFRUCEmisor());
				ComplementoFiscalData.setPais(FactElec.getDGI().getCFPais());
				ComplementoFiscalData.setTipoDocMdte(EnvioCFE.DocTypemasNIE.forValue(FactElec.getDGI().getCFTipoDocMdte()));
				ComplementoFiscalData.setDocMdte(FactElec.getDGI().getCFDocMdte());
				ComplementoFiscalData.setNombreMdte(FactElec.getDGI().getCFNombreMdte());
				ComplementoFiscal.setItem(ComplementoFiscalData);
				ItemCFE.setCompl_Fiscal(ComplementoFiscal);
			}

			CFE.setItem(ItemCFE);
            List<EnvioCFE.CFEDefType> listCFE = new ArrayList<EnvioCFE.CFEDefType>();
            listCFE.add(CFE);
            EnvCFE.argValue.setCFE(listCFE);


			LecturaOk.argValue = true;

		} catch (RuntimeException ex) {
			ErrorMsg.argValue = ((ex.getMessage().equals("")) ? ex.getMessage() : ex.getMessage()).toString();
		}
	}

	private void FirmarXML(int EmpId, String Xml, String Nodo, tangible.RefObject<Integer> CertId, java.util.Date FechaGenerado, short DiasAntelacionCertVenc, tangible.RefObject<String> XMLFirmado, tangible.RefObject<String> ErrorMessage, tangible.RefObject<String> WarningMsg) throws SQLException, TAException, FileNotFoundException {

		TAFACESign TASign = new TAFACESign();
		String CertArchivo = null;
		String CertClave = null;

		ErrorMessage.argValue = "";
		XMLFirmado.argValue = "";
		try {
			tangible.RefObject<String> tempRef_CertArchivo = new tangible.RefObject<String>(CertArchivo);
			tangible.RefObject<String> tempRef_CertClave = new tangible.RefObject<String>(CertClave);
			boolean tempVar = !(objCertificado.CargarCertificado(EmpId, FechaGenerado, DiasAntelacionCertVenc, tempRef_CertArchivo, tempRef_CertClave, CertId, WarningMsg));
			CertArchivo = tempRef_CertArchivo.argValue;
			CertClave = tempRef_CertClave.argValue;
			if (tempVar) {
				throw new TAException(getNombreMetodo() + " - No se pudo cargar certificado EmpId: " + EmpId, 3101);
			}

			if (!(TASign.FirmarXMLConCertParam(Xml, Nodo, CertArchivo, CertClave, XMLFirmado, ErrorMessage))) {
				ErrorMessage.argValue = getNombreMetodo() + " - No se pudo Firmar el Documento EmpId: " + EmpId + " - CFE DE Entrada : " + Xml + " - Error: " + ErrorMessage.argValue;
				throw new TAException(getNombreMetodo() + " - " + ErrorMessage.argValue, 3102);
			}

		} catch (TAException TaExCert) {
			throw TaExCert;
		} catch (RuntimeException ex) {
			throw new TAException(ex.getMessage(), 3103);
		} catch (Exception ex) {
			throw new TAException(ex.getMessage(), 3103);
		}
	}

	public final String Obtener6DigitosHash(String oXMLFirmado) throws TAException {
		String tempObtener6DigitosHash = null;

		int Pos = oXMLFirmado.indexOf("<DigestValue>");
		if (Pos < 0) {
			tempObtener6DigitosHash = "";
			throw new TAException("No se encontro DigestValue", 5007);
		}
		return tangible.DotNetToJavaStringHelper.substring(oXMLFirmado, (Pos + 14) - 1, 6);
	}

	public final String ObtenerHash(String oXMLFirmado) throws TAException {
		String tempObtenerHash = null;

		int Pos = oXMLFirmado.indexOf("<DigestValue>");
		if (Pos < 0) {
			tempObtenerHash = "";
			throw new TAException("No se encontro DigestValue", 5007);
		}
		return tangible.DotNetToJavaStringHelper.substring(oXMLFirmado, (Pos + 14) - 1, 28);
	}

	private Object ConectarBD(String EmpRUT, short SucId, short CajId, String UrlServidor, short TimeOutFirma, boolean ComprobarVersionBD) throws TAException, UnsupportedEncodingException, IOException, SQLException, Exception {
		return ConectarBD(EmpRUT, SucId, CajId, UrlServidor, TimeOutFirma, "", ComprobarVersionBD);
	}

	private boolean ConectarBD(String EmpRUT, short SucId, short CajId, String UrlServidor, short TimeOutFirma, String VersionApi, boolean ComprobarVersionBD) throws TAException, UnsupportedEncodingException, IOException, SQLException, Exception {
		// boolean tempConectarBD = false;
		boolean BDNueva = false;
		boolean GrabarLogRotura = false;
		int EmpId = 0;
		String ErrorMsg = "";
		RefObject<Boolean> FlagMismatchUniqueId = new RefObject<Boolean>(false);
		if ((objConexion == null)) {
			if (StringConexion.trim().equals("")) {
				throw new TAException("Debe configurar string de conexion a base de datos", 3005);
			}

			objConexion = new TAConexion();
			objConexion.setmVarStringConexion(StringConexion);
			objConexion.setProveedorConexion(getProveedorConexion());
			tangible.RefObject<Boolean> tempRef_BDNueva = new tangible.RefObject<Boolean>(BDNueva);
			tangible.RefObject<Boolean> tempRef_GrabarLogRotura = new tangible.RefObject<Boolean>(GrabarLogRotura);
			boolean tempVar = objConexion.Conectar(StringConexion, UrlServidor, TimeOutFirma, EmpRUT, SucId, CajId, tempRef_BDNueva, tempRef_GrabarLogRotura);
			BDNueva = tempRef_BDNueva.argValue;
			GrabarLogRotura = tempRef_GrabarLogRotura.argValue;
			if (!tempVar) {
				throw new TAException("No se logro conectar a la base de datos, RutaConexion: " + StringConexion, 3005);
			}
			tangible.RefObject<NadMADato> tempRef_NadDato = new RefObject<NadMADato>(objConexion.NadDato());

			objCAE = new TACAE(objConexion.NadDato());
			objEmpresa = new TAEmpresa(objConexion.NadDato());
			objComprobante = new TAComprobante(objConexion.NadDato());
			objLogErrores = new TALogErrores(objConexion.NadDato());
			objCertificado = new TACertificado(objConexion.NadDato());
			objCertificadoDGI = new TACertificadoDGI(objConexion.NadDato());
			objActualiza = new TAActualiza(objConexion.NadDato());

			//Verifca version y actualiza si es necesario
			if (ComprobarVersionBD && (VersionApi != null && VersionApi.length() > 0)) {
				tangible.RefObject<String> tempRef_ErrorMsg = new tangible.RefObject<String>(ErrorMsg);
				boolean tempVar2 = (objActualiza.VerificarVersion(VersionApi, tempRef_ErrorMsg));
				ErrorMsg = tempRef_ErrorMsg.argValue;
				if (!tempVar2) {
					//Error al acualizar base de datos!
					objLogErrores.GrabarError(EmpId, (int) SucId, (int) CajId, "Base de datos esta desactualizada y no logra actualizarse automaticamente." + ErrorMsg, "LOG", 0, "INS", (short) 1, getNombreMetodo());
				}
			}
			if (BDNueva) {
				//Si BD es nueva, sincronizo datos
				TASincronizarDatos objSinc = new TASincronizarDatos(objConexion.NadDato());
				tangible.RefObject<Integer> tempRef_EmpId = new tangible.RefObject<Integer>(EmpId);
				objSinc.SincronizarEmpresas(EmpRUT, SucId, CajId, UrlServidor, TimeOutFirma, tempRef_EmpId);
				EmpId = tempRef_EmpId.argValue;
				objSinc.SincronizarSucursales(EmpId, SucId, CajId, EmpRUT, UrlServidor, TimeOutFirma, true, CajaLicenseId(), FlagMismatchUniqueId);
				if (FlagMismatchUniqueId.argValue) {
					CerrarFirmaMismatchUniqueId(EmpRUT, SucId, CajId);
					throw new TAException("Detenido el proceso de facturación, el UniqueId recibido desde el servidor no coincide con el de la Base de Datos: ", 3011);
				}
				objSinc.SincronizarCertificados(EmpId, SucId, CajId, EmpRUT, UrlServidor, TimeOutFirma);
				objSinc.SincronizarCertificadosDGI(EmpId, SucId, CajId, EmpRUT, UrlServidor, TimeOutFirma);
				objSinc.SincronizarCAEParte(EmpId, SucId, CajId, EmpRUT, UrlServidor, TimeOutFirma);
			}

			if (GrabarLogRotura) {
				objLogErrores.GrabarError(EmpId, (int) SucId, (int) CajId, "Error al abrir base de datos, posible rotura. La base de datos fue renombrada.", "LOG", 0, "INS", (short) 1, getNombreMetodo());
			}
		}
		return true;
	}

	public final boolean SincronizarDatos(String UrlServidor, short SegundosTimeOut, short LoteComprobantes, String EmpRUT, int SucId, int CajId, String VersionApi, tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, int MinutosCaducidadReservaCAE, String CajaLicenceId) throws Exception {

		boolean tempSincronizarDatos = false;
		TASincronizarDatos sinc = null;
		RefObject<Boolean> FlagMismatchUniqueId = new RefObject<Boolean>(false);
		try {
			int EmpId = 0;
			ConectarBD(EmpRUT, (short) SucId, (short) CajId, UrlServidor, SegundosTimeOut, VersionApi, true);
			tempSincronizarDatos = true;

			//Verifica version y actualiza si es necesario
			if (!(objActualiza.VerificarVersion(VersionApi, ErrorMsg))) {
				//Error al acualizar base de datos!
				throw new TAException("Base de datos esta desactualizada y no logra actualizarse automaticamente. " + ErrorMsg.argValue, 3009);
			}

			try {
				if (!(CancelarCAEReservadosCaducados(ErrorCod, ErrorMsg, UrlServidor, SegundosTimeOut, EmpRUT, SucId, CajId, MinutosCaducidadReservaCAE))) {
					tempSincronizarDatos = false;
				}
			} catch (RuntimeException ex) {
				ErrorMsg.argValue += " - " + ex.getMessage() + "\r\n";
				tempSincronizarDatos = false;
			}

			sinc = new TASincronizarDatos(objConexion.NadDato());
			tangible.RefObject<Integer> tempRef_EmpId = new tangible.RefObject<Integer>(EmpId);
			sinc.SincronizarEmpresas(EmpRUT, SucId, CajId, UrlServidor, SegundosTimeOut, tempRef_EmpId);
			EmpId = tempRef_EmpId.argValue;
			try {
				sinc.SincronizarSucursales(EmpId, SucId, CajId, EmpRUT, UrlServidor, SegundosTimeOut, false, CajaLicenceId, FlagMismatchUniqueId);
				if (FlagMismatchUniqueId.argValue) {
					CerrarFirmaMismatchUniqueId(EmpRUT, SucId, CajId);
					throw new TAException("Detenido el proceso de facturación, el UniqueId recibido desde el servidor no coincide con el de la Base de Datos: ", 3011);
				}
			} catch (RuntimeException ex) {
				ErrorMsg.argValue += " - " + ex.getMessage() + "\r\n";
				tempSincronizarDatos = false;
			}
			try {
				sinc.SincronizarCAEParte(EmpId, SucId, CajId, EmpRUT, UrlServidor, SegundosTimeOut);
			} catch (RuntimeException ex) {
				ErrorMsg.argValue += " - " + ex.getMessage() + "\r\n";
				tempSincronizarDatos = false;
			}
			try {
				sinc.SincronizarCertificados(EmpId, SucId, CajId, EmpRUT, UrlServidor, SegundosTimeOut);
			} catch (RuntimeException ex) {
				ErrorMsg.argValue += " - " + ex.getMessage() + "\r\n";
				tempSincronizarDatos = false;
			}
			try {
				sinc.SincronizarCertificadosDGI(EmpId, SucId, CajId, EmpRUT, UrlServidor, SegundosTimeOut);
			} catch (RuntimeException ex) {
				ErrorMsg.argValue += " - " + ex.getMessage() + "\r\n";
				tempSincronizarDatos = false;
			}
			try {
				sinc.SincronizarLogErrores(EmpId, EmpRUT, SucId, CajId, LoteComprobantes, UrlServidor, SegundosTimeOut);
			} catch (RuntimeException ex) {
				ErrorMsg.argValue += " - " + ex.getMessage() + "\r\n";
				tempSincronizarDatos = false;
			}
			try {
				sinc.SincronizarCAEUtilizado(EmpId, UrlServidor, SegundosTimeOut);
				sinc.SincronizarCAEUtilAnulado(EmpId, UrlServidor, SegundosTimeOut);
				sinc.SincronizarCAEParteAnulado(EmpId, SucId, CajId, UrlServidor, SegundosTimeOut);
				sinc.SincronizarComprobantes(EmpId, SucId, CajId, LoteComprobantes, UrlServidor, SegundosTimeOut);
			} catch (RuntimeException ex) {
				ErrorMsg.argValue += " - " + ex.getMessage() + "\r\n";
				tempSincronizarDatos = false;
			}
		} catch (TAException ex) {
			if (ex.getClass() == TAException.class) {
				ErrorCod.argValue = ((TAException) ex).CodError;
				throw ex;
			} else {
				ErrorCod.argValue = 8;
			}
			ErrorMsg.argValue += " - " + ex.getMessage() + "\r\n";
			tempSincronizarDatos = false;
		} finally {
			try {
				if (!((objConexion == null))) {
					objConexion.Liberar(true);
					objConexion = null;
				}
			} catch (RuntimeException ex) {
				throw new TAException(ex.getMessage());
			}
		}
		return tempSincronizarDatos;
	}

	public final boolean CancelarCAEReservadosCaducados(tangible.RefObject<Integer> ErrorCod, tangible.RefObject<String> ErrorMsg, String UrlServidor, short SegundosTimeOut, String EmpRUT, int SucId, int CajId, int MinutosCaducidadReservaCAE) throws SQLException, Exception {
		boolean tempCancelarCAEReservadosCaducados = false;
		int EmpId = 0;

		tangible.RefObject<Integer> tempRef_EmpId = new tangible.RefObject<Integer>(EmpId);
		objEmpresa.ObtenerIdEmpresa(EmpRUT, tempRef_EmpId);
		EmpId = tempRef_EmpId.argValue;

		tempCancelarCAEReservadosCaducados = objCAE.CancelarCAEReservadosCaducados(EmpId, MinutosCaducidadReservaCAE);
		return tempCancelarCAEReservadosCaducados;
	}

	public final boolean LimpiarComprobantesSincronizados(String UrlServidor, short SegundosTimeOut, String EmpRUT, int SucId, int CajId, tangible.RefObject<String> ErrorMsg) throws TAException, UnsupportedEncodingException, IOException, SQLException, Exception {
		try {
			int EmpId = 0;
			ConectarBD(EmpRUT, (short) SucId, (short) CajId, UrlServidor, SegundosTimeOut, false);
			tangible.RefObject<Integer> tempRef_EmpId = new tangible.RefObject<Integer>(EmpId);
			objEmpresa.ObtenerIdEmpresa(EmpRUT, tempRef_EmpId);
			EmpId = tempRef_EmpId.argValue;
			objComprobante.EliminarComprobantesSincronizado(EmpId, SucId, (short) CajId);
			return true;
		} catch (RuntimeException ex) {
			return false;
		}
	}

	public final boolean LimpiarComprobantesSincronizadosDias(String UrlServidor, short SegundosTimeOut, String EmpRUT, int SucId, int CajId, int CantDias, tangible.RefObject<String> ErrorMsg) throws TAException {
		try {
			int EmpId = 0;
			ConectarBD(EmpRUT, (short) SucId, (short) CajId, UrlServidor, SegundosTimeOut, false);
			tangible.RefObject<Integer> tempRef_EmpId = new tangible.RefObject<Integer>(EmpId);
			objEmpresa.ObtenerIdEmpresa(EmpRUT, tempRef_EmpId);
			EmpId = tempRef_EmpId.argValue;
			objComprobante.EliminarComprobantesSincronizadoDias(EmpId, SucId, (short) CajId, CantDias);
			return true;
		} catch (RuntimeException ex) {
			return false;
		} catch (UnsupportedEncodingException ex) {
			throw new TAException("Error al limpiar comprobantes sincronizados. " + ex.getMessage());
		} catch (IOException ex) {
			throw new TAException("Error al limpiar comprobantes sincronizados. " + ex.getMessage());
		} catch (SQLException ex) {
			throw new TAException("Error al limpiar comprobantes sincronizados. " + ex.getMessage());
		} catch (Exception ex) {
			throw new TAException("Error al limpiar comprobantes sincronizados. " + ex.getMessage());
		} finally {
			try {
				objConexion.Liberar(true);
				objConexion = null;
			} catch (RuntimeException ex) {
			} catch (Exception ex) {
			}
		}
	}

	public final boolean LimpiarLogsSincronizados(String UrlServidor, short SegundosTimeOut, String EmpRUT, int SucId, int CajId, tangible.RefObject<String> ErrorMsg) throws TAException, UnsupportedEncodingException, IOException, SQLException, Exception {
		try {
			int EmpId = 0;
			ConectarBD(EmpRUT, (short) SucId, (short) CajId, UrlServidor, SegundosTimeOut, false);
			tangible.RefObject<Integer> tempRef_EmpId = new tangible.RefObject<Integer>(EmpId);
			objEmpresa.ObtenerIdEmpresa(EmpRUT, tempRef_EmpId);
			EmpId = tempRef_EmpId.argValue;
			objLogErrores.EliminarLogsSincronizado(EmpId, SucId, (short) CajId);
			return true;
		} catch (RuntimeException ex) {
			return false;
		} finally {
			try {
				objConexion.Liberar(true);
				objConexion = null;
			} catch (RuntimeException ex) {
			}
		}
	}

	public final boolean ExistenFacturasPendiendesDeSincronizar(String UrlServidor, short SegundosTimeOut, String EmpRUT, int SucId, int CajId, tangible.RefObject<String> ErrorMsg) throws TAException, UnsupportedEncodingException, IOException, SQLException, Exception {
		try {
			int EmpId = 0;
			ConectarBD(EmpRUT, (short) SucId, (short) CajId, UrlServidor, SegundosTimeOut,false);
			tangible.RefObject<Integer> tempRef_EmpId = new tangible.RefObject<Integer>(EmpId);
			objEmpresa.ObtenerIdEmpresa(EmpRUT, tempRef_EmpId);
			EmpId = tempRef_EmpId.argValue;
			if (objConexion.NadDato().getRowCount(objComprobante.ConsComprobantesPorSincronizar(EmpId, SucId, (short) CajId, 0)) == 0) {
				return false;
			} else {
				return true;
			}
		} catch (RuntimeException ex) {
			throw new TAException("Error al consultar cantidad de facturas pendientes de sincronizar");
		} finally {
			try {
				objConexion.Liberar(true);
				objConexion = null;
			} catch (RuntimeException ex) {
			}
		}
	}

	public final int CantidadCompPendSincronizar(String UrlServidor, short SegundosTimeOut, String EmpRUT, int SucId, int CajId, int MinSinSincronizar) throws TAException, UnsupportedEncodingException, IOException, SQLException, Exception {
		try {
			int EmpId = 0;
			ConectarBD(EmpRUT, (short) SucId, (short) CajId, UrlServidor, SegundosTimeOut, false);
			tangible.RefObject<Integer> tempRef_EmpId = new tangible.RefObject<Integer>(EmpId);
			objEmpresa.ObtenerIdEmpresa(EmpRUT, tempRef_EmpId);
			EmpId = tempRef_EmpId.argValue;
			return objComprobante.CantCompPorSincAlerta(EmpId, SucId, (short) CajId, MinSinSincronizar);
		} catch (RuntimeException ex) {
			throw new TAException("Error al consultar cantidad de comprobantes pendientes de sincronizar");
		} finally {
			try {
				objConexion.Liberar(true);
				objConexion = null;
			} catch (RuntimeException ex) {
			}
		}
	}

	public final int CantidadLogsPendSincronizar(String UrlServidor, short SegundosTimeOut, String EmpRUT, int SucId, int CajId, int MinSinSincronizar) throws TAException, UnsupportedEncodingException, IOException, SQLException, Exception {
		try {
			int EmpId = 0;
			ConectarBD(EmpRUT, (short) SucId, (short) CajId, UrlServidor, SegundosTimeOut,false);
			tangible.RefObject<Integer> tempRef_EmpId = new tangible.RefObject<Integer>(EmpId);
			objEmpresa.ObtenerIdEmpresa(EmpRUT, tempRef_EmpId);
			EmpId = tempRef_EmpId.argValue;
			return objLogErrores.CantLogsPorSincAlerta(EmpId, SucId, (short) CajId, MinSinSincronizar);
		} catch (RuntimeException ex) {
			throw new TAException("Error al consultar cantidad de logs pendientes de sincronizar");
		} finally {
			try {
				objConexion.Liberar(true);
				objConexion = null;
			} catch (RuntimeException ex) {
			}
		}
	}

	public final boolean CargarFactura(String UrlServidor, short SegundosTimeOut, long EmpresaRUC, short SucursalId, short Cajaid, int TipoDeComprobante, String SerieComprobante, int NroComprobante, tangible.RefObject<String> XMLEntrada, tangible.RefObject<String> XMLRespuesta) throws TAException, UnsupportedEncodingException, IOException, SQLException, Exception {
		try {
			String StrRucEmisor = String.format("%012d", EmpresaRUC);
			int EmpId = 0;
			ConectarBD(StrRucEmisor, SucursalId, Cajaid, UrlServidor, SegundosTimeOut,false);
			tangible.RefObject<Integer> tempRef_EmpId = new tangible.RefObject<Integer>(EmpId);
			objEmpresa.ObtenerIdEmpresa(StrRucEmisor, tempRef_EmpId);
			EmpId = tempRef_EmpId.argValue;
			return objComprobante.CargarDatosComprobante(EmpId, SucursalId, Cajaid, TipoDeComprobante, SerieComprobante, NroComprobante, XMLEntrada, XMLRespuesta);
		} catch (RuntimeException ex) {
			throw new TAException("Error al intentar obtener datos del comprobante");
		} finally {
			try {
				objConexion.Liberar(true);
				objConexion = null;
			} catch (RuntimeException ex) {
			}
		}
	}

	private int NumeroLineaDeError() {
		return Thread.currentThread().getStackTrace()[2].getLineNumber();
	}

	private void RecontruccionBDCaliente(String UrlServidor, short SegundosTimeOut, String EmpRUT, tangible.RefObject<Integer> EmpId, int SucId, int CajId, String VersionApi) throws SQLException, TAException, IOException, Exception {
		//No agrego try ya que capturo la excepcion en un nivel superior
		if (nProveedorConexion == NadMADato.TIPO_PROVEEDOR.tpSQLiteClient) {

			Format formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
			String string = formatter.format(new java.util.Date());
			String strRuta = StringConexion + string;

			//Verifico que todos los comprobantes esten sincronizados
			if (objComprobante.CantCompPorSinc() > 0) {
				throw new TAException("Error al leer tabla en Base de datos, debe ser reconstrudida pero quedan comprobantes por sinronizar.");
			}
			objConexion.Liberar(true);
			objConexion = null;
			try {
				File afile = new File(StringConexion);
				afile.renameTo(new File(strRuta));
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (ConectarBD(EmpRUT, (short) SucId, (short) CajId, UrlServidor, SegundosTimeOut, VersionApi,true)) {
				objEmpresa.ObtenerIdEmpresa(EmpRUT, EmpId);
				objLogErrores.GrabarError(EmpId.argValue, SucId, CajId, "La base de datos fue reconstruida debido a errores en la integridad de tablas, la base original fue renombrada y se volvio a generar una nueva.", "LOG", 0, "INS", (short) 1, getNombreMetodo());
			} else {
				throw new TAException("Error al conectar con base de datos luego de reconstruccion.");
			}
		}
	}

	private boolean VerificarCAEParteSincronizado(int EmpId, int SucId, int CajId, String EmpRUT, String URLWS, int TimeOut) throws SQLException, MalformedURLException {
		TASincronizarDatos sinc = new TASincronizarDatos(objConexion.NadDato());
		ResultSet dtCAE = null;
		WSSincronizacionCAEParte0206ExecuteResponse resp;
		List<SDTCAEParteResponse206SDTCAEParteResponse206Item> CAEServ = null;
		dtCAE = objCAE.ConsCAEParte(EmpId, SucId, CajId);
		ResultSet set = null;
                List<Map<String, Object>> setCAEParteenBD = new ArrayList<Map<String, Object>>();
                setCAEParteenBD = sinc.resultSetToListMapArray(set);
		executorWS.setURLServer(URLWS);
		resp = executorWS.executeCAEParte(EmpId, EmpRUT, SucId, (short) CajId, setCAEParteenBD);
		CAEServ = resp.getPsdtcaeparteresponse().getSDTCAEParteResponse206SDTCAEParteResponse206Item();
		while (dtCAE.next()) {
			int CAEId = dtCAE.getInt(TACAE.CAEParte.getCAEId());
			int CAEParteId = dtCAE.getInt(TACAE.CAEParte.getCAEParteId());
			int CAEParteUltAsignado = dtCAE.getInt(TACAE.CAEParte.getCAEParteUltAsignado());
			if (!(CAEParteSincOk(CAEId, CAEParteId, CAEParteUltAsignado, CAEServ))) {
				return false;
			}
		}
		return true;
	}

	private boolean CAEParteSincOk(int CAEId, int CAEParteId, int CAEParteUltAsignado, List<SDTCAEParteResponse206SDTCAEParteResponse206Item> CAEServ) {
		boolean tempCAEParteSincOk = false;
		for (int i = 0; i < CAEServ.size(); i++) {
			if (CAEId == CAEServ.get(i).getCAEId() && CAEParteId == CAEServ.get(i).getCAEParteId()) {
				if (CAEServ.get(i).getCAE().isCAEAnulado()) {
					return true;
				}
				if (CAEParteUltAsignado == CAEServ.get(i).getCAEParteUltAsignado()) {
					return true;
				} else {
					return false;
				}
			}
		}
		return tempCAEParteSincOk;
	}

	private boolean DescargarDatos(String UrlServidor, short SegundosTimeOut, String EmpRUT, int SucId, int CajId) throws SQLException, TAException, Exception {
		boolean tempDescargarDatos = false;
		TASincronizarDatos sinc = null;
		int EmpId = 0;
		RefObject<Boolean> FlagMismatchUniqueId = new RefObject<Boolean>(false);
		sinc = new TASincronizarDatos(objConexion.NadDato());
		tangible.RefObject<Integer> tempRef_EmpId = new tangible.RefObject<Integer>(EmpId);
		sinc.SincronizarEmpresas(EmpRUT, SucId, CajId, UrlServidor, SegundosTimeOut, tempRef_EmpId);
		EmpId = tempRef_EmpId.argValue;
		sinc.SincronizarSucursales(EmpId, SucId, CajId, EmpRUT, UrlServidor, SegundosTimeOut, false, CajaLicenseId(), FlagMismatchUniqueId);
		sinc.SincronizarCAEParte(EmpId, SucId, CajId, EmpRUT, UrlServidor, SegundosTimeOut);
		sinc.SincronizarCertificados(EmpId, SucId, CajId, EmpRUT, UrlServidor, SegundosTimeOut);
		sinc.SincronizarCertificadosDGI(EmpId, SucId, CajId, EmpRUT, UrlServidor, SegundosTimeOut);
		return true;
	}

	public final String VersionDeSobre() {
		return "1.0";
	}

	public final String ReservarNroCAE(String EmpRUC, int SucId, int CajaId, int TipoCFE, short DiasAntelacionCAEVenc, byte PorcUscoAntelacionCAE, tangible.RefObject<Boolean> Reservado, tangible.RefObject<Long> CAENroAutorizacion, tangible.RefObject<String> CAESerie, tangible.RefObject<Long> CAENroReservado, tangible.RefObject<Integer> ErrorCod, String VersionApi, String UrlServidor, short TimeOutSincronizacion) throws IOException, SQLException, Exception {
		boolean tempReservarNroCAE = false;
		TACAE CAE = null;
		java.util.Date FechaHoraActual = new java.util.Date();
		int EmpId = 0;
		int CodErrorInt = 0;
		java.util.Date FechaGenerado = new java.util.Date();
		String ErrorMsg = "";
		String WarningMsg = "";
		Map<String, Object> drCAE = new HashMap<String, Object>();
		Map<String, Object> drCAEParte = new HashMap<String, Object>();
		Map<String, Object> drCAEParteReservado = new HashMap<String, Object>();
		NadFormato Fto = new NadFormato();
		boolean EmpresaEmiTickets = false;
		boolean EmpresaEmiFacturas = false;
		boolean EmpresaEmiRemitos = false;
		boolean EmpresaEmiResguardos = false;
		boolean EmpresaEmiFacturasExportacion = false;
		boolean EmpresaEmiRemitosExportacion = false;
		boolean EmpresaEmiCuentaAjena = false;

		Reservado.argValue = false;

		try {
			Fto.setProveedorConexion(NadFormato.TIPO_PROVEEDOR.tpSQLiteClient);

			tangible.RefObject<Integer> tempRef_EmpId = new tangible.RefObject<Integer>(EmpId);
			ConectarVerificarBD(EmpRUC, SucId, CajaId, UrlServidor, TimeOutSincronizacion, VersionApi, tempRef_EmpId, SucId, CajaId, false);
			EmpId = tempRef_EmpId.argValue;

			tangible.RefObject<Integer> tempRef_EmpId2 = new tangible.RefObject<Integer>(EmpId);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiTickets = new tangible.RefObject<Boolean>(EmpresaEmiTickets);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiFacturas = new tangible.RefObject<Boolean>(EmpresaEmiFacturas);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiRemitos = new tangible.RefObject<Boolean>(EmpresaEmiRemitos);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiResguardos = new tangible.RefObject<Boolean>(EmpresaEmiResguardos);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiFacturasExportacion = new tangible.RefObject<Boolean>(EmpresaEmiFacturasExportacion);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiRemitosExportacion = new tangible.RefObject<Boolean>(EmpresaEmiRemitosExportacion);
			tangible.RefObject<Boolean> tempRef_EmpresaEmiCuentaAjena = new tangible.RefObject<Boolean>(EmpresaEmiCuentaAjena);
			boolean tempVar = !(objEmpresa.EmpresaDameTipoEmisor(EmpRUC, tempRef_EmpId2, tempRef_EmpresaEmiTickets, tempRef_EmpresaEmiFacturas, tempRef_EmpresaEmiRemitos, tempRef_EmpresaEmiResguardos, tempRef_EmpresaEmiFacturasExportacion, tempRef_EmpresaEmiRemitosExportacion, tempRef_EmpresaEmiCuentaAjena));
			EmpId = tempRef_EmpId2.argValue;
			EmpresaEmiTickets = tempRef_EmpresaEmiTickets.argValue;
			EmpresaEmiFacturas = tempRef_EmpresaEmiFacturas.argValue;
			EmpresaEmiRemitos = tempRef_EmpresaEmiRemitos.argValue;
			EmpresaEmiResguardos = tempRef_EmpresaEmiResguardos.argValue;
			EmpresaEmiFacturasExportacion = tempRef_EmpresaEmiFacturasExportacion.argValue;
			EmpresaEmiRemitosExportacion = tempRef_EmpresaEmiRemitosExportacion.argValue;
			EmpresaEmiCuentaAjena = tempRef_EmpresaEmiCuentaAjena.argValue;
			if (tempVar) {
				throw new TAException("No se encontro la empresa con el RUT: " + EmpRUC + " - SucId: " + SucId + " - CajaId: " + CajaId, 3401);
			}
			if (!EmpresaEmiTickets && (TipoCFE == enumTipoDeComprobanteCFE.eTicket.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eTicketContingencia.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eTicketNotaCred.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eTicketNotaCredContingencia.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eTicketNotaDeb.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eTicketNotaDebContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir eTicket o sus contingencias. Empresa: " + EmpId + " - EmpRUT: " + EmpRUC + " - SucId: " + SucId + " - CajaId: " + CajaId, 4132);
			}
			if (!EmpresaEmiFacturas && (TipoCFE == enumTipoDeComprobanteCFE.eFactura.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eFacturaContingencia.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCred.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredContingencia.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDeb.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir eFacturas o sus contingencias. Empresa: " + EmpId + " - EmpRUT: " + EmpRUC + " - SucId: " + SucId + " - CajaId: " + CajaId, 4133);
			}
			if (!EmpresaEmiResguardos && (TipoCFE == enumTipoDeComprobanteCFE.eResguardo.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eResguardoContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir eResguardo o eResguardoContingencia. Empresa: " + EmpId + " - EmpRUT: " + EmpRUC + " - SucId: " + SucId + " - CajaId: " + CajaId, 4134);
			}
			if (!EmpresaEmiRemitos && (TipoCFE == enumTipoDeComprobanteCFE.eRemito.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eRemitoContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir eRemito o eRemitoContingencia. Empresa: " + EmpId + " - EmpRUT: " + EmpRUC + " - SucId: " + SucId + " - CajaId: " + CajaId, 4135);
			}
			if (!EmpresaEmiFacturasExportacion && (TipoCFE == enumTipoDeComprobanteCFE.eFacturaExportacion.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir eFacturasExportacion o sus contingencias. Empresa: " + EmpId + " - EmpRUT: " + EmpRUC + " - SucId: " + SucId + " - CajaId: " + CajaId, 4136);
			}
			if (!EmpresaEmiRemitosExportacion && (TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacion.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir eRemitoExportacion o eRemitoExportacionContingencia. Empresa: " + EmpId + " - EmpRUT: " + EmpRUC + " - SucId: " + SucId + " - CajaId: " + CajaId, 4137);
			}
			if (!EmpresaEmiCuentaAjena && (TipoCFE == enumTipoDeComprobanteCFE.eTicketCuentaAjena.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eTicketCuentaAjenaContingencia.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eTicketNotaCredCuentaAjena.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eTicketNotaCredCuentaAjenaContingencia.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eTicketNotaDebCuentaAjena.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eTicketNotaDebCuentaAjenaContingencia.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eFacturaCuentaAjena.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eFacturaCuentaAjenaContingencia.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjena.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjenaContingencia.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjena.getValue() || TipoCFE == enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjenaContingencia.getValue())) {
				throw new TAException("Empresa configurada para no permitir comprobantes del tipo Cuenta Ajena. Empresa: " + EmpId + " - EmpRUT: " + EmpRUC + " - SucId: " + SucId + " - CajaId: " + CajaId, 4290);
			}

			if (!(objEmpresa.ExisteSucursal(EmpId, SucId))) {
				throw new TAException("Sucursal no encontrada: EmpId:" + EmpId + " - EmpRUT: " + EmpRUC + " - SucId: " + SucId, 4176);
			}

			if (!(objEmpresa.ExisteCaja(EmpId, SucId, CajaId))) {
				throw new TAException("Caja no encontrada: EmpId:" + EmpId + " - EmpRUT: " + EmpRUC + " - SucId: " + SucId + " - CajaId: " + CajaId, 4177);
			}

			tangible.RefObject<Map<String, Object>> tempRef_drCAE = new RefObject<Map<String, Object>>(drCAE);
			tangible.RefObject<Map<String, Object>> tempRef_drCAEParte = new RefObject<Map<String, Object>>(drCAEParte);
			tangible.RefObject<String> tempRef_WarningMsg = new tangible.RefObject<String>(WarningMsg);
			tempReservarNroCAE = objCAE.TomarCAE(EmpId, SucId, CajaId, (short) TipoCFE, FechaGenerado, DiasAntelacionCAEVenc, PorcUscoAntelacionCAE, tempRef_drCAE, tempRef_drCAEParte, tempRef_WarningMsg);
			drCAE = tempRef_drCAE.argValue;
			drCAEParte = tempRef_drCAEParte.argValue;
			WarningMsg = tempRef_WarningMsg.argValue;
			if (tempReservarNroCAE == false) {
				throw new TAException("No se pudo tomar CAE para reserva.", 3010);
			}

			CAE = new TACAE(objConexion.NadDato());

			drCAEParteReservado = CAE.CrearCAEParteReservado();
			drCAEParteReservado.put(TACAE.CAEParteReservado.getEmpId(), EmpId);
			drCAEParteReservado.put(TACAE.CAEParteReservado.getCAEId(), Integer.parseInt(drCAE.get(TACAE.CAEParte.getCAEId()).toString()));
			drCAEParteReservado.put(TACAE.CAEParteReservado.getCAEParteId(), Integer.parseInt(drCAEParte.get(TACAE.CAEParte.getCAEParteId()).toString()));
			drCAEParteReservado.put(TACAE.CAEParteReservado.getCAEParteResNroCAE(), Integer.parseInt(drCAEParte.get(TACAE.CAEParte.getCAEParteUltAsignado()).toString()));
			drCAEParteReservado.put(TACAE.CAEParteReservado.getCAEParteResFchHora(), Fto.FechaHoraSQL(FechaHoraActual).replace("'", ""));
			drCAEParteReservado.put(TACAE.CAEParteReservado.getCAEParteResCaduco(), 0);
			drCAEParteReservado.put(TACAE.CAEParteReservado.getCAEParteResEstado(), "PEN");
			objCAE.GrabarCAEParteReservado(drCAEParteReservado);


			objCAE.AnularCAE(EmpId, Integer.parseInt(drCAE.get(TACAE.CAEParte.getCAEId()).toString()), Integer.parseInt(drCAEParte.get(TACAE.CAEParte.getCAEParteId()).toString()), FechaGenerado, Long.parseLong(drCAEParte.get(TACAE.CAEParte.getCAEParteUltAsignado()).toString()));


			CAENroAutorizacion.argValue = new BigDecimal(drCAE.get(TACAE.CAE.getCAENroAutorizacion()).toString()).longValue();
			CAESerie.argValue = drCAE.get(TACAE.CAE.getCAESerie()).toString();
			CAENroReservado.argValue = Long.parseLong(drCAEParte.get(TACAE.CAEParte.getCAEParteUltAsignado()).toString());
			Reservado.argValue = true;
		} catch (TAException e) {
			Reservado.argValue = false;
			ErrorMsg = e.getMessage();
			if (e.getClass() == TAException.class) {
				ErrorCod.argValue = ((TAException) e).CodError;
				CodErrorInt = ((TAException) e).CodErrorInterno;
			} //ORIGINAL LINE: Case Else
			else {
				ErrorCod.argValue = 8;
				CodErrorInt = 8;
			}
			CAENroAutorizacion.argValue = (long) 0;
			CAESerie.argValue = "";
			CAENroReservado.argValue = (long) 0;
			if (!((objLogErrores == null))) {
				try {
					objLogErrores.GrabarError(EmpId, SucId, CajaId, getNombreMetodo() + " - " + e.toString(), "LOG", CodErrorInt, "INS", (short) NumeroLineaDeError(), e.getStackTrace().toString());
				} catch (RuntimeException ex) {
				}
			}
		} finally {
			try {
				objConexion.Liberar(true);
				objConexion = null;
			} catch (RuntimeException ex) {
			}
		}
		return ErrorMsg;
	}

	public final boolean ReservaNroCAEEstaVigente(tangible.RefObject<Integer> ErrorCod, tangible.RefObject<String> ErrorMsg, String EmpRUC, int SucId, int CajaId, long CAENroAutorizacion, String CAESerie, long CAEParteResNroCAE, tangible.RefObject<Boolean> ReservaVigente, String UrlServidor, short SegundosTimeOut, short TimeOutSincronizacion, String VersionApi, int MinutosCaducidadReservaCAE) throws IOException, Exception {
		boolean tempReservaNroCAEEstaVigente = false;
		int EmpId = 0;
		int CodErrorInt = 0;
		ReservaVigente.argValue = false;
		tempReservaNroCAEEstaVigente = false;

		try {
			tangible.RefObject<Integer> tempRef_EmpId = new tangible.RefObject<Integer>(EmpId);
			ConectarVerificarBD(EmpRUC, SucId, CajaId, UrlServidor, TimeOutSincronizacion, VersionApi, tempRef_EmpId, SucId, CajaId, false);
			EmpId = tempRef_EmpId.argValue;
			ReservaVigente.argValue = objCAE.ReservaNroCAEEstaVigente(EmpId, CAENroAutorizacion, CAESerie, CAEParteResNroCAE, MinutosCaducidadReservaCAE);
			tempReservaNroCAEEstaVigente = ReservaVigente.argValue;
			ErrorMsg.argValue = "La reserva de CAE no esta vigente o no se encontraron datos";
		} catch (TAException e) {
			ReservaVigente.argValue = false;
			ErrorMsg.argValue = e.getMessage();
			try {
				objConexion.NadDato().FinTrans(false);
			} catch (RuntimeException ex) {
			}
			if (e.getClass() == TAException.class) {
				ErrorCod.argValue = ((TAException) e).CodError;
				CodErrorInt = ((TAException) e).CodErrorInterno;
			} else {
				ErrorCod.argValue = 8;
				CodErrorInt = 8;
			}
			if (!((objLogErrores == null))) {
				try {
					objLogErrores.GrabarError(EmpId, SucId, CajaId, getNombreMetodo() + " - " + e.toString(), "LOG", CodErrorInt, "INS", (short) NumeroLineaDeError(), e.getStackTrace().toString());
				} catch (RuntimeException ex) {
				}
			}
		} finally {
			try {
				objConexion.Liberar(true);
				objConexion = null;
			} catch (RuntimeException ex) {
			}
		}
		return tempReservaNroCAEEstaVigente;
	}

	public final String CancelarNroCAEReservado(String EmpRUC, int SucId, int CajaId, long CAENroAutorizacion, String CAESerie, long CAEParteResNroCAE, boolean ReservaCaducada, String VersionApi, String UrlServidor, short TimeOutSincronizacion, tangible.RefObject<Boolean> Cancelado, tangible.RefObject<Integer> ErrorCod) throws IOException, SQLException, Exception {
		boolean tempCancelarNroCAEReservado = false;
		ResultSet dtCAEParteReservado = null;
		ResultSet drCAEParteReservado = null;
		int EmpId = 0;
		java.util.Date FechaReserva = new java.util.Date(0);
		String EstadoReserva = "";
		int CodErrorInt = 0;
		String ErrorMsg = null;
		int CAEId = 0;
		int CAEparteId = 0;
		Cancelado.argValue = false;

		try {
			tangible.RefObject<Integer> tempRef_EmpId = new tangible.RefObject<Integer>(EmpId);
			ConectarVerificarBD(EmpRUC, SucId, CajaId, UrlServidor, TimeOutSincronizacion, VersionApi, tempRef_EmpId, SucId, CajaId, false);
			EmpId = tempRef_EmpId.argValue;

			tangible.RefObject<Integer> tempRef_CAEId = new tangible.RefObject<Integer>(CAEId);
			tangible.RefObject<Integer> tempRef_CAEparteId = new tangible.RefObject<Integer>(CAEparteId);
			boolean tempVar = !(objCAE.CargarDatosCAEParteReserva(EmpId, CAENroAutorizacion, CAESerie, CAEParteResNroCAE, tempRef_CAEId, tempRef_CAEparteId));
			CAEId = tempRef_CAEId.argValue;
			CAEparteId = tempRef_CAEparteId.argValue;
			if (tempVar) {
				throw new TAException("No se encontro el Nro. de CAE reservado que se intenta cancelar." + " RUC Empresa:" + EmpRUC + " CAE Nro. Autorizacion:" + CAENroAutorizacion + " CAE Serie:" + CAESerie + " Nro. de CAE Reservado:" + CAEParteResNroCAE);
			}

			dtCAEParteReservado = objCAE.ConsCAEParteReservado(EmpId, CAEId, CAEparteId, CAEParteResNroCAE);

			if (objConexion.NadDato().getRowCount(dtCAEParteReservado) > 0) {
				dtCAEParteReservado = objCAE.ConsCAEParteReservado(EmpId, CAEId, CAEparteId, CAEParteResNroCAE);
				int i = 0;
				while (dtCAEParteReservado.next() && i < 1) {
					SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
					try {
						FechaReserva = formato.parse(dtCAEParteReservado.getString(TACAE.CAEParteReservado.getCAEParteResFchHora()));
					} catch (ParseException ex) {
						Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
					}
					EstadoReserva = dtCAEParteReservado.getString(TACAE.CAEParteReservado.getCAEParteResEstado()).toString();
					i++;
				}
				objConexion.NadDato().FinTrans();
				if (EstadoReserva.equals("PEN")) {
					tempCancelarNroCAEReservado = objCAE.SetEstadoCAEReservadoAnulado(EmpId, CAEId, CAEparteId, CAEParteResNroCAE, ReservaCaducada);
					if (tempCancelarNroCAEReservado == false) {
						return ErrorMsg + tempCancelarNroCAEReservado;
					}
					Cancelado.argValue = true;
				} else {
					//Si intentan cancelar una reserva que ya no se encunetra en estado pendiente retorno true
					Cancelado.argValue = true;
					objConexion.NadDato().FinTrans(true);
				}
			} else {
				//No se encontro la reserva
				throw new TAException("No se encontro el Nro. de CAE reservado que se intenta cancelar." + " RUC Empresa:" + EmpRUC + " CAEId:" + CAEId + " CAEParteId:" + CAEparteId + " Nro. de CAE Reservado:" + CAEParteResNroCAE);
			}
		} catch (TAException e) {
			Cancelado.argValue = false;
			ErrorMsg = e.getMessage();
			try {
				objConexion.NadDato().FinTrans(false);
			} catch (RuntimeException ex) {
				if (e.getClass() == TAException.class) {
					ErrorCod.argValue = ((TAException) e).CodError;
					CodErrorInt = ((TAException) e).CodErrorInterno;
				} //ORIGINAL LINE: Case Else
				else {
					ErrorCod.argValue = 8;
					CodErrorInt = 8;
				}
				if (!((objLogErrores == null))) {
					try {
						objLogErrores.GrabarError(EmpId, SucId, CajaId, getNombreMetodo() + " - " + e.toString(), "LOG", CodErrorInt, "INS", (short) NumeroLineaDeError(), e.getStackTrace().toString());
					} catch (RuntimeException ex1) {
						throw new TAException("Error al grabar log, " + ex1.getMessage());
					}
				}
			}
		} finally {
			try {
				objConexion.Liberar(true);
				objConexion = null;
			} catch (RuntimeException ex) {
			}
		}
		return ErrorMsg;
	}

	private void ConectarVerificarBD(String EmpresaRUT, int SucursalId, int CajaId, String UrlServidor, int TimeOutSincronizacion, String VersionApi, tangible.RefObject<Integer> EmpresaId, int SucId, int CajId, boolean ComprobarVersionBD) throws SQLException, TAException, UnsupportedEncodingException, IOException, Exception {

		//Conectar con base de datos
		if (!(ConectarBD(EmpresaRUT, (short) SucursalId, (short) CajaId, UrlServidor, (short) TimeOutSincronizacion, VersionApi, ComprobarVersionBD))) {
			throw new TAException("Error al conectar con Base de Datos", 3001);
		}

		//Verifica que exista la empresa,sucursal,caja y si no encuentra intenta descargar
		if (!(objEmpresa.ExisteEmpresa(EmpresaRUT, EmpresaId)) || !(objEmpresa.ExisteSucursal(EmpresaId.argValue, SucId)) || !(objEmpresa.ExisteCaja(EmpresaId.argValue, SucId, CajId))) {
			if (objConexion.NadDato().VerificarIntegridadSQLite()) {
				DescargarDatos(UrlServidor, (short) TimeOutSincronizacion, EmpresaRUT, SucId, CajaId);
			} else {
				RecontruccionBDCaliente(UrlServidor, (short) TimeOutSincronizacion, EmpresaRUT, EmpresaId, SucId, CajaId, VersionApi);
			}
		}
	}

	private void LicenciaCajaValida(String EmpRUT, int SucId, int CajaId, String LicenciaEquipo) throws Exception {
		String LicenciaCaja = "";
		int EmpId = 0;

		tangible.RefObject<Integer> tempRef_EmpId = new tangible.RefObject<Integer>(EmpId);
		objEmpresa.ObtenerIdEmpresa(EmpRUT, tempRef_EmpId);
		EmpId = tempRef_EmpId.argValue;

		LicenciaCaja = objEmpresa.GetUniqueIdCaja(EmpId, SucId, CajaId);
		if ((!LicenciaEquipo.equals(LicenciaCaja)) && (LicenciaCaja != null && LicenciaCaja.length() > 0)) {
			throw new TAException("El ID del equipo no es el mismo que el ID asociado a la caja, puede que la caja que intenta usar este asociada a otro equipo, si no es asi debe ir al Server de TAFACE y habilitar la caja para que sea usada por otro equipo. ID del equipo local(Fisico): " + LicenciaEquipo + " - ID asociado a la caja: " + LicenciaCaja + " . ID de caja que se intenta usar: " + (new Integer(CajaId)).toString().trim(), 4306);
		}
	}

	public String getNombreMetodo() {
		//Retorna el nombre del metodo desde el cual se hace el llamado
		return new Exception().getStackTrace()[1].getMethodName();
	}

	public String convertirFechaXMLSincronizar(Date date) {
		//2017-05-02T12:47:52.3558396-03:00
		long timeUS = System.currentTimeMillis() * 1000;
		SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		DecimalFormat DF = new DecimalFormat("000");
		return SDF.format(timeUS / 1000) + DF.format(timeUS % 1000) + "-03:00";
	}

	public void schrinkBDSQlite() {
		//llamo al vacuum de la BD
		objConexion = new TAConexion();
		objConexion.schrinkBDSQlite();
	}

	private boolean VerificarExisteComprobanteAFirmar(XMLFACTURA xmlOriginal, String XMLEntradaAComprobar) {
		boolean tempVerificarExisteComprobanteAFirmar = false;
		tempVerificarExisteComprobanteAFirmar = false;
		XMLFACTURA xmlComprobar = new XMLFACTURA();
		xmlComprobar = xmlComprobar.LoadXML(XMLEntradaAComprobar);
		//realizar comparacion entre los XML para detectar la mayor cantidad de similitudes antes de retornar si existe el XML y evitar el eco transaccional
		if (xmlOriginal.getDGI().getEMIRUCEmisor().equals(xmlComprobar.getDGI().getEMIRUCEmisor())) {
			if (xmlOriginal.getDGI().getIDDocFchEmis().compareTo(xmlComprobar.getDGI().getIDDocFchEmis()) == 0) {
				if (xmlOriginal.getDGI().getTOTMntNoGrv() == xmlComprobar.getDGI().getTOTMntNoGrv() && xmlOriginal.getDGI().getTOTMntTotal() == xmlComprobar.getDGI().getTOTMntTotal()) {
					if (xmlOriginal.getDGI().getTOTCantLinDet() == xmlComprobar.getDGI().getTOTCantLinDet() && xmlOriginal.getDGI().getTOTMntPagar() == xmlComprobar.getDGI().getTOTMntPagar()) {
						if (xmlOriginal.getDGI().getDetalles().getDetalle().get(0).getDETNomItem().equals(xmlComprobar.getDGI().getDetalles().getDetalle().get(0).getDETNomItem()) && xmlOriginal.getDGI().getDetalles().getDetalle().get(0).getDETCantidad() == xmlComprobar.getDGI().getDetalles().getDetalle().get(0).getDETCantidad()) {
							if (xmlOriginal.getDGI().getDetalles().getDetalle().get(0).getDETPrecioUnitario() == xmlComprobar.getDGI().getDetalles().getDetalle().get(0).getDETPrecioUnitario() && xmlOriginal.getDGI().getDetalles().getDetalle().get(0).getDETMontoItem() == xmlComprobar.getDGI().getDetalles().getDetalle().get(0).getDETMontoItem()) {
								if (xmlOriginal.getDATOSADICIONALES().getTRANSACCIONNRO() == xmlComprobar.getDATOSADICIONALES().getTRANSACCIONNRO()) {
									tempVerificarExisteComprobanteAFirmar = true;
								}
							}
						}
					}
				}
			}
		}
		return tempVerificarExisteComprobanteAFirmar;
	}

	private String CajaLicenseId() throws TAException {
		try {
			List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
			for (NetworkInterface nif : all) {
				if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

				byte[] macBytes = nif.getHardwareAddress();
				if (macBytes == null) {
					return "";
				}

				StringBuilder res1 = new StringBuilder();
				for (byte b : macBytes) {
					res1.append(String.format("%02X:",b));
				}

				if (res1.length() > 0) {
					res1.deleteCharAt(res1.length() - 1);
				}
				return res1.toString();
			}
		} catch (Exception ex) {
		}
		return "02:00:00:00:00:00";
	}

	private boolean CerrarFirmaMismatchUniqueId(String EmpRUC, int SucId, int CajaId) {
		boolean tempCerrarFirmaMismatchUniqueId = false;
		int PosCarp = StringConexion.lastIndexOf(File.separator);
		String rootCarpetaOperacion = StringConexion.substring(0, PosCarp + 1);
		try {
			File dir = new File(rootCarpetaOperacion);
			File[] filesList = dir.listFiles();
			for (File f : filesList) {
				if (f.getName() == null ? EmpRUC + "_" + SucId + "_TAFEApi.config" == null : f.getName().equals(EmpRUC + "_" + SucId + "_TAFEApi.config")) {
					try {
						FileOutputStream writer = new FileOutputStream(f);
						writer.write(("").getBytes());
						writer.close();
						tempCerrarFirmaMismatchUniqueId = true;
					} catch (RuntimeException Ex) {
					} catch (FileNotFoundException ex) {
						Logger.getLogger(TAFirmarFactura.class.getName()).log(Level.SEVERE, null, ex);
					} catch (IOException ex) {
						Logger.getLogger(TAFirmarFactura.class.getName()).log(Level.SEVERE, null, ex);
					}
				} else if (f.getName().contains("TAFACEApi.db")) {
					try {
						if (objCAE.EliminarCAECompleto()) {
							tempCerrarFirmaMismatchUniqueId = true;
						}
						objConexion.Liberar(true);
						File b = new File(f.getParent() + File.separator + f.getName());
						f.renameTo(b);
					} catch (RuntimeException Ex) {
					} catch (Exception ex) {
						Logger.getLogger(TAFirmarFactura.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			}
		} catch (RuntimeException ex) {
			tempCerrarFirmaMismatchUniqueId = false;
		}
		return tempCerrarFirmaMismatchUniqueId;
	}
}