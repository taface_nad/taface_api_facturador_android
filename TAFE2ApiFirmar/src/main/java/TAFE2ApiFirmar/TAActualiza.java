package TAFE2ApiFirmar;

import TAFACE2ApiEntidad.TAException;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TAActualiza {

    private String sSQL;
    private NadMADato NadDato;
    private NadFormato Fto = new NadFormato();

    public final int VersionBDActual() throws Exception {
        int version = -1;
        try {
            if (!(NadDato.ExisteTabla("Parametro"))) {
                //Si no existe la tabla retorno la version mas vieja posible
                return 100;
            }
            ResultSet dt = null;
            dt = NadDato.GetTable("Select * from Parametro Where ParId = 1000");
            if (NadDato.getRowCount(dt) == 0) {
                //Si existe la tabla pero no el parametro retorno la version mas vieja posible 
                return 100;
            } else {
                //Si existe el parametro retorno la version
                dt = NadDato.GetTable("Select * from Parametro Where ParId = 1000");
                while (dt.next()) {

                    version = Integer.parseInt(dt.getString("ParValor"));
                }
            }
            return version;
        } catch (RuntimeException ex) {
            throw new TAException("Error al obtener la version actual de la API.");
        }
    }

    public final boolean ActualizarBDV0200() throws Exception {

        boolean tempActualizarBDV0200 = false;
        try {
            //Inicio transaccion, si da error hago RollBack
            NadDato.IniTrans();
            //Consulto si existe la tabla parametro
            if (!(NadDato.ExisteTabla("Parametro"))) {
                //Si no existe la tabla la creo
                sSQL = "CREATE TABLE Parametro (ParId INTEGER, ParNom TEXT,ParValor TEXT);";
                NadDato.Ejecutar(sSQL);
            }
            //Agrego el parametro versión
            if (NadDato.getRowCount(NadDato.GetTable("Select * from Parametro Where ParId=1000")) == 0) {
                //Si no existia el parametro lo creo
                sSQL = "Insert into Parametro (ParId, ParNom, ParValor) values(1000,'VersionBD','200')";
            } else {
                //Si existia el parametro lo actualizo
                sSQL = "Update Parametro set ParValor='200' where ParId=1000";
            }

            NadDato.Ejecutar(sSQL);
            NadDato.FinTrans(true);
            tempActualizarBDV0200 = true;
        } catch (RuntimeException ex) {
            //RollBack
            NadDato.FinTrans(false);
            throw ex;
        }
        return tempActualizarBDV0200;
    }

    public final boolean ActualizarBDV0202() throws Exception {

        boolean tempActualizarBDV0202 = false;
        try {
            //Inicio transaccion, si da error hago RollBack
            NadDato.IniTrans();
            //Consulto si existe la tabla parametro
            if (!(NadDato.ExisteTabla("Parametro"))) {
                //Si no existe la tabla la creo
                sSQL = "CREATE TABLE Parametro (ParId INTEGER, ParNom TEXT,ParValor TEXT);";
                NadDato.Ejecutar(sSQL);
            }
            //Consulto si existe el campo
            if (!(NadDato.ExisteCampo("Empresa", "EmpEmiteCuentaAjena"))) {
                //Si no existe lo creo
                sSQL = "ALTER TABLE Empresa ADD COLUMN [EmpEmiteCuentaAjena] Boolean Default 0;";
                NadDato.Ejecutar(sSQL);
            }
            //Agrego el parametro versión
            if (NadDato.getRowCount(NadDato.GetTable("Select * from Parametro Where ParId=1000")) == 0) {
                //Si no existia el parametro lo creo
                sSQL = "Insert into Parametro (ParId, ParNom, ParValor) values(1000,'VersionBD','202')";
                NadDato.Ejecutar(sSQL);
            } else {
                //Si existia el parametro lo actualizo
                sSQL = "Update Parametro set ParValor='202' where ParId=1000";
                NadDato.Ejecutar(sSQL);
            }
            //Consulto si existe la tabla CertificadoDGI
            if (!(NadDato.ExisteTabla("CertificadoDGI"))) {
                //Si no existe la tabla la creo
                sSQL = "CREATE TABLE [CertificadoDGI] (";
                sSQL += "[EmpId]	integer NOT NULL,";
                sSQL += "[CertDGIId]	integer NOT NULL,";
                sSQL += "[CertDGIVigencia]	datetime NOT NULL,";
                sSQL += "[CertDGIVencimiento]	datetime NOT NULL,";
                sSQL += "[CertDGIArchivo]	varchar NOT NULL COLLATE NOCASE,";
                sSQL += "[CertDGICN]	varchar(200) NOT NULL COLLATE NOCASE,";
                sSQL += "[CertDGIFchHoraSync]	datetime NOT NULL,";
                sSQL += "[CertDGIRowGUID]	varchar(200) NOT NULL COLLATE NOCASE,";
                sSQL += "PRIMARY KEY ([EmpId], [CertDGIId]))";

            }
            NadDato.Ejecutar(sSQL);
            NadDato.FinTrans(true);
            tempActualizarBDV0202 = true;
        } catch (RuntimeException ex) {
            NadDato.FinTrans(false);
            throw new TAException("Error al actualizar la version de la API.");
        }
        return tempActualizarBDV0202;
    }

    public final boolean ActualizarBDV0203() throws Exception {

        boolean tempActualizarBDV0203 = false;
        try {
            //Inicio transaccion, si da error hago RollBack
            NadDato.IniTrans();
            //Consulto si existe la tabla parametro
            if (!(NadDato.ExisteTabla("Parametro"))) {
                //Si no existe la tabla la creo
                sSQL = "CREATE TABLE Parametro (ParId INTEGER, ParNom TEXT,ParValor TEXT);";

                NadDato.Ejecutar(sSQL);
            }
            if (!(NadDato.ExisteCampo("CAEParteUtilizadoAnulado", "CAEParteAnulRestaurar"))) {
                //Si no existe lo creo
                sSQL = "ALTER TABLE CAEParteUtilizadoAnulado ADD COLUMN [CAEParteAnulRestaurar] Boolean Default 0;";
                NadDato.Ejecutar(sSQL);
            }

            //Consulto si existe la tabla CAEParteReservado
            if (!(NadDato.ExisteTabla("CAEParteReservado"))) {
                //Si no existe la tabla la creo
                sSQL = "CREATE TABLE [CAEParteReservado] (";
                sSQL += "[EmpId]	integer NOT NULL,";
                sSQL += "[CAEId]	integer NOT NULL,";
                sSQL += "[CAEParteId]	integer NOT NULL,";
                sSQL += "[CAEParteResNroCAE]	integer NOT NULL,";
                sSQL += "[CAEParteResFchHora]	datetime NOT NULL,";
                sSQL += "[CAEParteResCaduco]	bit NOT NULL,";
                sSQL += "[CAEParteResEstado]	char(3) NOT NULL,";
                sSQL += "PRIMARY KEY ([EmpId], [CAEId], [CAEParteId], [CAEParteResNroCAE]))";
                NadDato.Ejecutar(sSQL);
            }

            //Agrego el parametro versión
            if (NadDato.getRowCount(NadDato.GetTable("Select * from Parametro Where ParId=1000")) == 0) {
                //Si no existia el parametro lo creo
                sSQL = "Insert into Parametro (ParId, ParNom, ParValor) values(1000,'VersionBD','203')";
            } else {
                //Si existia el parametro lo actualizo
                sSQL = "Update Parametro set ParValor='203' where ParId=1000";

            }
            NadDato.Ejecutar(sSQL);
            NadDato.FinTrans(true);
            tempActualizarBDV0203 = true;
        } catch (RuntimeException ex) {
            NadDato.FinTrans(false);
            throw new TAException("Error al actualizar la version de la API.");
        }
        return tempActualizarBDV0203;
    }

    public final boolean ActualizarBDV0204() throws Exception {
        boolean tempActualizarBDV0204 = false;
        try {
            //Inicio transaccion, si da error hago RollBack
            NadDato.IniTrans();
            //Consulto si existe la tabla parametro
            if (!(NadDato.ExisteTabla("Parametro"))) {
                //Si no existe la tabla la creo
                sSQL = "CREATE TABLE Parametro (ParId INTEGER, ParNom TEXT,ParValor TEXT);";
                NadDato.Ejecutar(sSQL);
            }
            //Agrego el parametro versión
            if (NadDato.getRowCount(NadDato.GetTable("Select * from Parametro Where ParId=1000")) == 0) {
                //Si no existia el parametro lo creo
                sSQL = "Insert into Parametro (ParId, ParNom, ParValor) values(1000,'VersionBD','204')";
            } else {
                //Si existia el parametro lo actualizo
                sSQL = "Update Parametro set ParValor='204' where ParId=1000";
            }
            NadDato.Ejecutar(sSQL);
            NadDato.FinTrans(true);
            tempActualizarBDV0204 = true;
        } catch (RuntimeException ex) {
            NadDato.FinTrans(false);
            throw new TAException("Error al actualizar la version de la API.");
        }
        return tempActualizarBDV0204;
    }

    public final boolean ActualizarBDV0205() throws Exception {
        boolean tempActualizarBDV0205 = false;
        try {
            //Inicio transaccion, si da error hago RollBack
            NadDato.IniTrans();
            //Consulto si existe la tabla parametro
            if (!(NadDato.ExisteTabla("Parametro"))) {
                //Si no existe la tabla la creo
                sSQL = "CREATE TABLE Parametro (ParId INTEGER, ParNom TEXT,ParValor TEXT);";
                NadDato.Ejecutar(sSQL);
            }
            if (!(NadDato.ExisteCampo("SucursalCaja", "CajaUniqueId"))) {
                //Si no existe lo creo
                sSQL = "ALTER TABLE SucursalCaja ADD COLUMN [CajaUniqueId] varchar(200) Default '';";
                NadDato.Ejecutar(sSQL);
            }
            //Agrego el parametro versión
            if (NadDato.getRowCount(NadDato.GetTable("Select * from Parametro Where ParId=1000")) == 0) {
                //Si no existia el parametro lo creo
                sSQL = "Insert into Parametro (ParId, ParNom, ParValor) values(1000,'VersionBD','205')";
            } else {
                //Si existia el parametro lo actualizo
                sSQL = "Update Parametro set ParValor='205' where ParId=1000";

            }
            NadDato.Ejecutar(sSQL);
            NadDato.FinTrans(true);
            tempActualizarBDV0205 = true;
        } catch (RuntimeException ex) {
            NadDato.FinTrans(false);
            throw new TAException("Error al actualizar la version de la API.");
        }
        return tempActualizarBDV0205;
    }

	public final boolean ActualizarBDV0206() throws Exception {
        boolean tempActualizarBDV0206 = false;
        try {
      //Inicio transaccion, si da error hago RollBack
            NadDato.IniTrans();
            //Consulto si existe la tabla parametro
            if (!(NadDato.ExisteTabla("Parametro"))) {
                //Si no existe la tabla la creo
                sSQL = "CREATE TABLE Parametro (ParId INTEGER, ParNom TEXT,ParValor TEXT);";
                NadDato.Ejecutar(sSQL);
            }
			if (!(NadDato.ExisteCampo("CAE", "CAEEspecial"))) {
				//Si no existe lo creo
				sSQL = "ALTER TABLE CAE ADD COLUMN [CAEEspecial] integer Default 0;";
                NadDato.Ejecutar(sSQL);
			}
			if (!(NadDato.ExisteCampo("CAE", "CAECAusalTipo"))) {
				//Si no existe lo creo
				sSQL = "ALTER TABLE CAE ADD COLUMN [CAECAusalTipo] integer Default 0;";
                NadDato.Ejecutar(sSQL);
			}
			//Agrego el parametro versión
            if (NadDato.getRowCount(NadDato.GetTable("Select * from Parametro Where ParId=1000")) == 0) {
                //Si no existia el parametro lo creo
                sSQL = "Insert into Parametro (ParId, ParNom, ParValor) values(1000,'VersionBD','206')";
            } else {
                //Si existia el parametro lo actualizo
                sSQL = "Update Parametro set ParValor='206' where ParId=1000";
            }

            NadDato.Ejecutar(sSQL);
            NadDato.FinTrans(true);
            tempActualizarBDV0206 = true;
        } catch (RuntimeException ex) {
            NadDato.FinTrans(false);
            throw new TAException("Error al actualizar la version de la API.");
        }
        return tempActualizarBDV0206;
    }
	
    public final boolean VerificarVersion(String VersionApiActual, tangible.RefObject<String> ErrorMsg) throws Exception {
        try {

            int VerApiActual = 0;
            int VerDBActual = 0;
            //Convierto en integer la version actual.
            VerApiActual = Fto.CTInt(Fto.CNullStr(VersionApiActual).replace(".", ""));
            VerDBActual = Fto.CTInt(VersionBDActual());
            //Si la versin actual es igual a la version de la base retorno true

            if (VerDBActual == VerApiActual) {
                return true;
            } else {
                //Actualiza según la version de la api
                if (VerApiActual >= 200) {
                    ActualizarBDV0200();
                }

                if (VerApiActual >= 202) {
                    ActualizarBDV0202();
                }

                if (VerApiActual >= 203) {
                    ActualizarBDV0203();
                }

                if (VerApiActual >= 204) {
                    ActualizarBDV0204();
                }

                if (VerApiActual >= 205) {
                    ActualizarBDV0205();
                }
				
				if (VerApiActual >= 206) {
                    ActualizarBDV0206();
                }

                return true;
            }
        } catch (RuntimeException ex) {
            ErrorMsg.argValue = ex.getMessage();
            return false;
        }
    }

    public TAActualiza(NadMADato Dato) {
        NadDato = Dato;
        Fto.setProveedorConexion(Dato.getProveedorConexion().getValue());
    }
}