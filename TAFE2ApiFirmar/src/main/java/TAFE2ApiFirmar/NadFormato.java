package TAFE2ApiFirmar;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NadFormato {

    private String[] UNIDADES = {"", "un", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve"};
    private String[] DECENAS = {"", "diez", "veinte", "treinta", "cuarenta", "cincuenta", "sesenta", "setenta", "ochenta", "noventa"};
    private String[] DECEN_1 = {"", "once", "doce", "trece", "catorce", "quince", "dieciseis", "diecisiete", "dieciocho", "diecinueve"};
    private String[] DECEN_2 = {"", "veintiun", "veintidos", "veintitres", "veinticuatro", "veinticinco", "veintiseis", "veintisiete", "veintiocho", "veintinueve"};
    private String[] CENTENAS = {"", "cien", "doscientos", "trescientos", "cuatrocientos", "quinientos", "seiscientos", "setecientos", "ochocientos", "novecientos"};
    private static final String STRING_UNDEF = "";
    private static final int INT_UNDEF = -1;
    private static final String DATE_UNDEF = "1950-1-1";
    private static final boolean BOOL_UNDEF = false;
    public static final String StrNumEntero = "0123456789";

    public enum TIPO_LIKE {

        tlIzquierda(1),
        tlDerecha(2),
        tlAmbos(3);
        private int intValue;
        private static java.util.HashMap<Integer, TIPO_LIKE> mappings;

        private static java.util.HashMap<Integer, TIPO_LIKE> getMappings() {
                if (mappings == null) {
                    mappings = new java.util.HashMap<Integer, TIPO_LIKE>();
                }
            return mappings;
        }

        private TIPO_LIKE(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static TIPO_LIKE forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum TIPO_REDONDEO {

        RedNoAplica(0),
        Red0_05(1),
        Red0_10(2),
        Red0_50(3),
        Red1_00(4);
        private int intValue;
        private static java.util.HashMap<Integer, TIPO_REDONDEO> mappings;

        private static java.util.HashMap<Integer, TIPO_REDONDEO> getMappings() {
            if (mappings == null) {
                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, TIPO_REDONDEO>();
                    }
            }
            return mappings;
        }

        private TIPO_REDONDEO(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static TIPO_REDONDEO forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum TIPO_ALINTEXTO {

        cDerecha(1),
        cIzquierda(2),
        cCentrado(3),
        cNoAlinear(4);
        private int intValue;
        private static java.util.HashMap<Integer, TIPO_ALINTEXTO> mappings;

        private static java.util.HashMap<Integer, TIPO_ALINTEXTO> getMappings() {
            if (mappings == null) {
                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, TIPO_ALINTEXTO>();
                    }
            }
            return mappings;
        }

        private TIPO_ALINTEXTO(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static TIPO_ALINTEXTO forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum TIPO_PROVEEDOR {

        tpSQLClient(1),
        tpSQLOLEDB(2),
        tpSQLiteClient(3);
        private int intValue;
        private static java.util.HashMap<Integer, TIPO_PROVEEDOR> mappings;

        private static java.util.HashMap<Integer, TIPO_PROVEEDOR> getMappings() {
            if (mappings == null) {
                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, TIPO_PROVEEDOR>();
                    }
            }
            return mappings;
        }

        private TIPO_PROVEEDOR(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static TIPO_PROVEEDOR forValue(int value) {
            return getMappings().get(value);
        }
    }
    private TIPO_PROVEEDOR mvarCxProveedor = TIPO_PROVEEDOR.tpSQLClient;

    public final TIPO_PROVEEDOR getProveedorConexion() {
        return mvarCxProveedor;
    }

    public final void setProveedorConexion(TIPO_PROVEEDOR Value) {
        mvarCxProveedor = Value;
    }

    public final void setProveedorConexion(int Value) {
        mvarCxProveedor = TIPO_PROVEEDOR.forValue(Value);
    }

    public final boolean CTBool(Object Texto) {
        if ((Texto == null) || (String.valueOf(Texto).trim() == null || String.valueOf(Texto).trim().length() == 0)) {
            return false;
        } else {
            return (Boolean) Texto;
        }
    }

    public final int CTInt(Object Texto) {
        int tempCTInt = 0;
        if (isNumeric(Texto.toString())) {
            tempCTInt = Integer.parseInt(Texto.toString());
        } else {
            tempCTInt = 0;
        }
        return tempCTInt;
    }

    public final long CTLng(Object Texto) {
        long tempCTLng = 0;
        if (isNumeric(Texto.toString())) {
            tempCTLng = Long.parseLong(String.valueOf(Texto));
        } else {
            tempCTLng = 0;
        }
        return tempCTLng;
    }

    public final java.math.BigDecimal CTDec(Object Texto) throws ParseException {
        java.math.BigDecimal tempCTDec = new java.math.BigDecimal(0);
        if (isNumeric(Texto.toString())) {
            tempCTDec = new BigDecimal(Texto.toString());
            if (tempCTDec == BigDecimal.valueOf(Double.MIN_VALUE)) {
                tempCTDec = new BigDecimal(0);
            }
        } else {
            tempCTDec = new BigDecimal(0);
        }
        return tempCTDec;
    }

    public final short CTShort(Object Texto) {
        short tempCTShort = 0;
        if (isNumeric(Texto.toString())) {
            tempCTShort = Short.parseShort(String.valueOf(Texto));
        } else {
            tempCTShort = 0;
        }
        return tempCTShort;
    }

    public final String CNumFto(Object Numero) {
        return CNumFto(Numero, 21);
    }

    public String FechaSQL(Date Fecha) {
        String sFecha;
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            sFecha = "'" + formato.format(Fecha) + "'";
        } else {
            SimpleDateFormat formato = new SimpleDateFormat("yyyyMMdd");
            sFecha = "'" + formato.format(Fecha) + "'";
        }
        return sFecha;
    }

    public final String FechaHoraSQL(java.util.Date Fecha) {
        String tempFechaHoraSQL = "";
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            tempFechaHoraSQL = "'" + formato.format(Fecha) + "'";
        } else {
            SimpleDateFormat formato = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            tempFechaHoraSQL = "'" + formato.format(Fecha) + "'";
        }
        return tempFechaHoraSQL;
    }

    public final String NumSQL(Object Numero) {
        String tempNumSQL = null;
        if (isNumeric(Numero.toString())) {
            tempNumSQL = String.valueOf(Numero).replace(",", ".");
        } else {
            tempNumSQL = "0";
        }
        return tempNumSQL;
    }

    public final String TextoSQL(String Texto) {
        return "'" + Texto.replace("'", "''") + "'";
    }

    public final String TextoLikeSQL(String Texto, TIPO_LIKE TipoLike) {
        @SuppressWarnings("UnusedAssignment")
        String tempTextoLikeSQL = null;
        tempTextoLikeSQL = "";
        switch (TipoLike) {
            case tlIzquierda:
                tempTextoLikeSQL = "'%" + Texto.replace("'", "''") + "'";
                break;
            case tlDerecha:
                tempTextoLikeSQL = "'" + Texto.replace("'", "''") + "%'";
                break;
            case tlAmbos:
                tempTextoLikeSQL = "'%" + Texto.replace("'", "''") + "%'";
                break;
        }
        return tempTextoLikeSQL;
    }

    public final String LogSQL(boolean Logico) {
        String tempLogSQL = null;
        if (Logico) {
            tempLogSQL = "1";
        } else {
            tempLogSQL = "0";
        }
        return tempLogSQL;
    }

    ///IMPLEMENTAR
    public final java.math.BigDecimal Redondeo(java.math.BigDecimal Numero) {
        /*return Math.round(Numero, 2);*/
        //return Redondeo(Numero, 2);
        return Numero;
    }

    ///IMPLEMENTAR
    public final java.math.BigDecimal Redondeo(java.math.BigDecimal Numero, byte Decimales) {
        /*String Dec = "0.";
         return (java.math.BigDecimal) Microsoft.VisualBasic.Strings.Format(Numero, Dec.PadRight(Decimales + 2, "0"));*/
        return Numero;
    }

    //IMPLEMENTAR
    public final String AjustoNumero(java.math.BigDecimal Numero, short Largo, short DecNum) {
        /*  String Nro = String.valueOf(0);
         int Por = 0;
         switch (DecNum) {
         case 1:
         Por = 10;
         break;
         case 2:
         Por = 100;
         break;
         case 3:
         Por = 1000;
         break;
         default:
         Por = 1;
         break;
         }
         Numero = Redondeo(Numero, (byte) DecNum).multiply(new BigDecimal(Por));
         return Microsoft.VisualBasic.Strings.Format(Numero, Nro.PadLeft(Largo, "0"));*/
        return "";
    }

    //IMPLEMENTAR
    public final String CNumFto(Object Numero, int Decimales) {
        String tempCNumFto = null;
        /*String NumFto = "#,##0";
         if (Decimales > 0) {
         NumFto = NumFto + ".";
         Decimales += (short) NumFto.length();
         NumFto = String.foNumFto.PadRight(Decimales, "0");
         }
         if (Microsoft.VisualBasic.Information.IsNumeric(Numero)) {
         tempCNumFto = Microsoft.VisualBasic.Strings.Format((Double) Numero, NumFto);
         } else {
         tempCNumFto = Microsoft.VisualBasic.Strings.Format(0, NumFto);
         }*/
        return tempCNumFto;
    }

    ///IMPLEMENTAR
    public final String FtoNumero(short Decimales, boolean SepMil) {
        String NumFto = "##0";
        /* if (Decimales > 0) {
         NumFto += ".";
         NumFto = NumFto.PadRight(Decimales + NumFto.length(), "0");
         }
         if (SepMil) {
         NumFto = "#," + NumFto;
         }*/
        return NumFto;
    }

    // 0 = No, 1 = 0.05, 2 = 0.10, 3 = 0.50, 4 = 1.00
    public final Object RedImporte(java.math.BigDecimal nImp, TIPO_REDONDEO nRed) throws ParseException {
        java.math.BigDecimal nRet = new java.math.BigDecimal(0);
        java.math.BigDecimal nPri = new java.math.BigDecimal(0);
        java.math.BigDecimal nUlt = new java.math.BigDecimal(0);
        String sImp = null;

        switch (nRed) {
            case RedNoAplica:
                nRet = nImp.setScale(2, BigDecimal.ROUND_HALF_UP);
                break;
            case Red0_05:
                nRet = nImp.setScale(2, BigDecimal.ROUND_HALF_UP);
                // sImp = nRet.toString("0000000000.000");
                sImp = nRet.toString();
                nPri = CTDec(sImp.substring(0, 12));
                nUlt = CTDec(sImp.substring(12, 14));
                BigDecimal val26 = new BigDecimal(26);
                BigDecimal val75 = new BigDecimal(75);

                if (nUlt.compareTo(val26) < 0) {
                    nUlt = new java.math.BigDecimal(0);
                } else if (nUlt.compareTo(val26) >= 0 && nUlt.compareTo(val75) <= 0) {
                    nUlt = new java.math.BigDecimal(0.05);
                } else if (nUlt.compareTo(val75) > 0) {
                    nUlt = new java.math.BigDecimal(0.1);
                }
                nRet = nPri.add(nUlt);
                break;
            case Red0_10:
                nRet = nImp.setScale(1, BigDecimal.ROUND_HALF_UP);
                break;
            case Red0_50:
                nRet = nImp.setScale(0, BigDecimal.ROUND_HALF_UP);
                BigDecimal val026 = new BigDecimal(0.26);
                BigDecimal val075 = new BigDecimal(0.75);
                if (nUlt.compareTo(val026) < 0) {
                    nUlt = new java.math.BigDecimal(0);
                } else if (nUlt.compareTo(val026) >= 0 && nUlt.compareTo(val075) <= 0) {
                    nUlt = new java.math.BigDecimal(0.5);
                } else if (nUlt.compareTo(val075) > 0) {
                    nUlt = new java.math.BigDecimal(1);
                }
                nRet = nPri.add(nUlt);
                break;
            case Red1_00:
                nRet = nImp.setScale(0, BigDecimal.ROUND_HALF_UP);
                break;
        }

        return nRet;

    }

    public final String CNullStr(Object Value) {
        String tempCNullStr = STRING_UNDEF;
        if (!((Value == null))) {
            tempCNullStr = String.valueOf(Value);
        }
        return tempCNullStr;
    }

    /**
     * *****************************************************************************************************
     */
    public java.util.Date CNullDate(Object Value) throws ParseException {
        java.util.Date tempCNullDate = new java.util.Date(0);
        if (Value != null) {
            tempCNullDate = (Date) Value;
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String dateInString = DATE_UNDEF;
            tempCNullDate = formatter.parse(dateInString);
        }
        return tempCNullDate;
    }

    // public String LTrim(String Cadena, int Largo) { original declaration
    public String LTrim(String Cadena, Integer Largo, char PadCad) {
        String Pad;
        int i;
        Pad = "";
        if (Largo > Cadena.length()) {
            for (i = 0; i < (Largo - Cadena.length()); i++) {
                Pad = Pad + PadCad;
            }
            Cadena = Pad + Cadena;
        } else {
            Cadena = Cadena.substring(0, Largo);
        }
        return Cadena;
    }

    public String RTrim(String Cadena, Integer Largo, char PadCad) {
        String Pad;
        int i;
        Pad = "";
        if (Largo > Cadena.length()) {
            for (i = 0; i < (Largo - Cadena.length()); i++) {
                Pad = Pad + PadCad;
            }
            Cadena = Cadena + Pad;
        } else {
            Cadena = Cadena.substring(0, Largo);
        }
        return Cadena;
    }

    public final String CTrim(String Cadena, int Largo) {
        int i = 0;
        String CadTxt;
        try {
            CadTxt = Cadena;
            for (i = 0; i < ((Largo - CadTxt.length()) / 2); i++) {
                Cadena = " " + Cadena;
            }
            for (i = 0; i < ((Largo - CadTxt.length()) / 2); i++) {
                Cadena = Cadena + " ";
            }
            if (Cadena.length() < Largo) {
                return Cadena + " ";
            } else if (Cadena.length() < Largo) {
                Cadena = Cadena.substring(0, Largo).trim();
            }
            return Cadena;
        } catch (Exception e) {
            return "";
        }
    }

    public final String AjustoTexto(String Cadena, TIPO_ALINTEXTO Alinear, int Desde, int Hasta) {
        String tempAjustoTexto = null;
        tempAjustoTexto = Cadena;
        if (Desde == 0 && Hasta == 0) {
            switch (Alinear) {
                case cIzquierda:
                    tempAjustoTexto = trimStringFromPosition(Cadena, 0, 0, 0, Cadena.length());
                    break;
                case cDerecha:
                    tempAjustoTexto = trimStringFromPosition(Cadena, 1, 1, 0, Cadena.length());
                    break;
                case cCentrado:
                    tempAjustoTexto = Cadena.trim();
                    break;
                case cNoAlinear:
                    tempAjustoTexto = Cadena;
                    break;
            }
        } else {
            switch (Alinear) {
                case cIzquierda:
                    tempAjustoTexto = RTrim(Cadena.substring(Desde - 1, Hasta), Hasta - Desde + 1, ' ');
                    break;
                case cDerecha:
                    tempAjustoTexto = LTrim(Cadena.substring(Desde - 1, Hasta), Hasta - Desde + 1, ' ');
                    break;
                case cCentrado:
                    tempAjustoTexto = CTrim(tangible.DotNetToJavaStringHelper.substring(Cadena, Desde - 1, Hasta), Hasta - Desde + 1);
                    break;
                case cNoAlinear:
                    tempAjustoTexto = tangible.DotNetToJavaStringHelper.substring(Cadena, Desde - 1, Hasta);
                    break;
            }
        }
        return tempAjustoTexto;
    }

    public final String EscribirNumero(java.math.BigDecimal Nro) {
        String NRO_S = null;
        String LINEA = null;
        short XDIGI = 0;
        short DIGITO = 0;

        // Primer Cero del Formato se Descarta en Transformacion
        NRO_S = Nro.toString();
        LINEA = "";
        XDIGI = 1;

        if (XDIGI == 1) {
            DIGITO = Short.parseShort(NRO_S.substring(1, 2));
            XDIGI = 2;
            if (DIGITO != 0) {
                switch (DIGITO) {
                    case 1:
                        LINEA = "mil ";
                        break;
                    default:
                        LINEA = UNIDADES[DIGITO] + " mil ";
                        break;
                }

                if (NRO_S.substring(2, 3).equals("0") && NRO_S.substring(3, 4).equals("0") && NRO_S.substring(4, 5).equals("0")) {
                    LINEA = LINEA + "millones ";
                    XDIGI = 5;
                }
            }
        }

        if (XDIGI == 2) {
            DIGITO = Short.parseShort(NRO_S.substring(2, 3));
            XDIGI = 3;
            switch (DIGITO) {
                case 0:
                    break;
                case 1:
                    if (NRO_S.substring(3, 4).equals("0") && NRO_S.substring(4, 5).equals("0")) {
                        LINEA = LINEA + "cien millones ";
                        XDIGI = 5;
                    } else {
                        LINEA = LINEA + "ciento ";
                    }
                    break;
                default:
                    LINEA = LINEA + CENTENAS[DIGITO] + " ";
                    break;
            }
        }

        if (XDIGI == 3) {
            DIGITO = Short.parseShort(NRO_S.substring(3, 4));
            XDIGI = 4;
            switch (DIGITO) {
                case 0:
                    break;
                case 1:
                    if (NRO_S.substring(4, 5).equals("0")) {
                        LINEA = LINEA + "diez millones ";
                    } else {
                        LINEA = LINEA + DECEN_1[Short.parseShort(NRO_S.substring(4, 5))] + " millones ";
                    }
                    XDIGI = 5;
                    break;
                case 2:
                    if (NRO_S.substring(4, 5).equals("0")) {
                        LINEA = LINEA + "veinte millones ";
                    } else {
                        LINEA = LINEA + DECEN_2[Short.parseShort(NRO_S.substring(4, 5))] + " millones ";
                    }
                    XDIGI = 5;
                    break;
                default:
                    LINEA = LINEA + DECENAS[DIGITO];
                    if (!NRO_S.substring(4, 5).equals("0")) {
                        LINEA = LINEA + " y ";
                        XDIGI = 4;
                    } else {
                        LINEA = LINEA + " millones ";
                        XDIGI = 5;
                    }
                    break;
            }
        }

        if (XDIGI == 4) {
            DIGITO = Short.parseShort(NRO_S.substring(4, 5));
            XDIGI = 5;
            if (DIGITO != 0) {
                LINEA = LINEA + UNIDADES[DIGITO] + " ";
                if (DIGITO == 1) {
                    LINEA = LINEA + "millon ";
                } else {
                    LINEA = LINEA + "millones ";
                }
            }
        }

        if (XDIGI == 5) {
            DIGITO = Short.parseShort(NRO_S.substring(5, 6));
            XDIGI = 6;
            switch (DIGITO) {
                case 0:
                    break;
                case 1:
                    if (NRO_S.substring(6, 7).equals("0") && NRO_S.substring(7, 8).equals("0")) {
                        LINEA = LINEA + "cien mil ";
                        XDIGI = 8;
                    } else {
                        LINEA = LINEA + "ciento ";
                    }
                    break;
                default:
                    LINEA = LINEA + CENTENAS[DIGITO] + " ";
                    break;
            }
        }

        if (XDIGI == 6) {
            DIGITO = Short.parseShort(NRO_S.substring(6, 7));
            XDIGI = 7;
            switch (DIGITO) {
                case 0:
                    break;
                case 1:
                    if (NRO_S.substring(7, 8).equals("0")) {
                        LINEA = LINEA + "diez mil ";
                    } else {
                        LINEA = LINEA + DECEN_1[Short.parseShort(NRO_S.substring(7, 8))] + " mil ";
                    }
                    XDIGI = 8;
                    break;
                case 2:
                    if (NRO_S.substring(7, 8).equals("0")) {
                        LINEA = LINEA + "veinte mil ";
                    } else {
                        LINEA = LINEA + DECEN_2[Short.parseShort(NRO_S.substring(7, 8))] + " mil ";
                    }
                    XDIGI = 8;
                    break;
                default:
                    LINEA = LINEA + DECENAS[DIGITO] + " ";
                    if (!NRO_S.substring(7, 8).equals("0")) {
                        LINEA = LINEA + "y ";
                        XDIGI = 7;
                    } else {
                        LINEA = LINEA + "mil ";
                        XDIGI = 8;
                    }
                    break;
            }
        }

        if (XDIGI == 7) {
            DIGITO = Short.parseShort(NRO_S.substring(7, 8));
            XDIGI = 8;
            if (DIGITO != 0) {
                if (DIGITO == 1 && LINEA.length() == 0) {
                    LINEA = LINEA + "mil ";
                } else {
                    LINEA = LINEA + UNIDADES[DIGITO] + " mil ";
                }
            }
        }

        if (XDIGI == 8) {
            DIGITO = Short.parseShort(NRO_S.substring(8, 9));
            XDIGI = 9;
            switch (DIGITO) {
                case 0:
                    break;
                case 1:
                    if (NRO_S.substring(9, 10).equals("0") && NRO_S.substring(10, 11).equals("0")) {
                        LINEA = LINEA + "cien ";
                        XDIGI = 11;
                    } else {
                        LINEA = LINEA + "ciento ";
                    }
                    break;
                default:
                    LINEA = LINEA + CENTENAS[DIGITO] + " ";
                    break;
            }
        }

        if (XDIGI == 9) {
            DIGITO = Short.parseShort(NRO_S.substring(9, 10));
            XDIGI = 10;
            switch (DIGITO) {
                case 0:
                    break;
                case 1:
                    if (NRO_S.substring(10, 11).equals("0")) {
                        LINEA = LINEA + "diez ";
                    } else {
                        LINEA = (LINEA + DECEN_1[Short.parseShort(NRO_S.substring(10, 11))]) + " ";
                    }
                    XDIGI = 11;
                    break;
                case 2:
                    XDIGI = 11;
                    if ("0".equals(NRO_S.substring(10, 11))) {

                        LINEA = LINEA + "veinte ";
                    } else if ("1".equals(NRO_S.substring(10, 11))) {
                        LINEA = LINEA + "veinti ";
                        XDIGI = 10;
                    } else {
                        LINEA = LINEA + DECEN_2[Short.parseShort(NRO_S.substring(10, 11))] + " ";
                    }
                    break;
                default:
                    LINEA = LINEA + DECENAS[DIGITO] + " ";
                    XDIGI = 11;
                    if (!NRO_S.substring(10, 11).equals("0")) {
                        LINEA = LINEA + "y ";
                        XDIGI = 10;
                    }
                    break;
            }
        }

        if (XDIGI == 10) {
            DIGITO = Short.parseShort(NRO_S.substring(10, 11));
            XDIGI = 11;
            if (DIGITO != 0) {
                if (DIGITO == 1) {
                    LINEA = LINEA + "uno ";
                } else {
                    LINEA = LINEA + UNIDADES[DIGITO] + " ";
                }
            }
        }

        if (XDIGI == 11) {
            XDIGI = 12;
            if (!NRO_S.substring(12, 13).equals("0") || !NRO_S.substring(13, 14).equals("0")) {
                LINEA = LINEA + "con " + NRO_S.substring(12, 14) + "/100";
            } else {
                XDIGI = 14;
            }
        }

        return LINEA;
    }

    // REIMPLEMENTADO
    public final java.math.BigDecimal Calcular(final String Expresion) {
        Object asd = new Object() {
            int pos = -1, ch;

            void nextChar() {
                ch = (++pos < Expresion.length()) ? Expresion.charAt(pos) : -1;
            }

            boolean eat(int charToEat) {
                while (ch == ' ') {
                    nextChar();
                }
                if (ch == charToEat) {
                    nextChar();
                    return true;
                }
                return false;
            }

            double parse() {
                nextChar();
                double x = parseExpression();
                if (pos < Expresion.length()) {
                    throw new RuntimeException("Unexpected: " + (char) ch);
                }
                return x;
            }

            // Grammar:
            // expression = term | expression `+` term | expression `-` term
            // term = factor | term `*` factor | term `/` factor
            // factor = `+` factor | `-` factor | `(` expression `)`
            //        | number | functionName factor | factor `^` factor
            double parseExpression() {
                double x = parseTerm();
                for (;;) {
                    if (eat('+')) {
                        x += parseTerm(); // addition
                    } else if (eat('-')) {
                        x -= parseTerm(); // subtraction
                    } else {
                        return x;
                    }
                }
            }

            double parseTerm() {
                double x = parseFactor();
                for (;;) {
                    if (eat('*')) {
                        x *= parseFactor(); // multiplication
                    } else if (eat('/')) {
                        x /= parseFactor(); // division
                    } else {
                        return x;
                    }
                }
            }

            double parseFactor() {
                if (eat('+')) {
                    return parseFactor(); // unary plus
                }
                if (eat('-')) {
                    return -parseFactor(); // unary minus
                }
                double x;
                int startPos = this.pos;
                if (eat('(')) { // parentheses
                    x = parseExpression();
                    eat(')');
                } else if ((ch >= '0' && ch <= '9') || ch == '.') { // numbers
                    while ((ch >= '0' && ch <= '9') || ch == '.') {
                        nextChar();
                    }
                    x = Double.parseDouble(Expresion.substring(startPos, this.pos));
                } else if (ch >= 'a' && ch <= 'z') { // functions
                    while (ch >= 'a' && ch <= 'z') {
                        nextChar();
                    }
                    String func = Expresion.substring(startPos, this.pos);
                    x = parseFactor();
                    if (func.equals("sqrt")) {
                        x = Math.sqrt(x);
                    } else if (func.equals("sin")) {
                        x = Math.sin(Math.toRadians(x));
                    } else if (func.equals("cos")) {
                        x = Math.cos(Math.toRadians(x));
                    } else if (func.equals("tan")) {
                        x = Math.tan(Math.toRadians(x));
                    } else {
                        throw new RuntimeException("Unknown function: " + func);
                    }
                } else {
                    throw new RuntimeException("Unexpected: " + (char) ch);
                }

                if (eat('^')) {
                    x = Math.pow(x, parseFactor()); // exponentiation
                }
                return x;
            }
        }.parse();

        return new BigDecimal((String) asd);

    }

    private String GetNumFrom(int Pos, String expr) {
        String tempGetNumFrom = null;
        int i = 0;
        String temp = null;
        String a = null;
        tempGetNumFrom = "";
        if (Pos <= (expr == null ? 0 : expr.length())) {
            for (i = Pos; i <= (expr == null ? 0 : expr.length()); i++) {
                //{
                a = expr.substring(i - 1, 1);

                //a = "."

                if ((int) a.toCharArray()[0] >= 48 && (int) a.toCharArray()[0] <= 58 || a.equals(" ") || a.equals('.') || ((a.equals("-") || a.equals("+")) && (temp.trim() == null || temp.trim().length() == 0))) {
                    temp = temp + a;
                } else {
                    if (a.toLowerCase().equals("e")) {
                        temp = temp + "E" + GetNumFrom(i + 1, expr); //Recursion
                        i = expr == null ? 0 : expr.length();
                    } else {
                        //wrong syntax, u can handle error as you like
                    }
                    i = expr == null ? 0 : expr.length();
                }
            }
            //}
            tempGetNumFrom = temp;
        }
        return tempGetNumFrom;
    }

    private String solveFor(String sign, String expr) throws ParseException {
        String tempsolveFor = null;
        int X = 0;
        int start = 0;
        int endat = 0;
        String temp = null;
        String a = null;
        int i = 0;
        tempsolveFor = "";
        start = 1;
        X = expr.indexOf(sign) + 1;
        if (!sign.equals("!")) {
            if (sign.equals("+") || sign.equals("-")) {
                a = GetNumFrom(1, expr);
                if ((a == null ? 0 : a.length()) == (expr == null ? 0 : expr.length())) {
                    return "";
                }
                temp = GetNumFrom((a == null ? 0 : a.length()) + 1, expr);
                if (Integer.parseInt(temp) != 0) {
                    if (Integer.parseInt(temp) < 0) {
                        sign = "-";
                    } else {
                        sign = "+";
                    }
                }
                X = expr.indexOf(sign, (a == null ? 0 : a.length()) - 1) + 1;
                endat = (a == null ? 0 : a.length()) + (temp == null ? 0 : temp.length());
                temp = Eval(GetNumFrom(X + 1, expr), sign, a);
                expr = expr.substring(0, start - 1) + temp + expr.substring(expr.length() - ((expr == null ? 0 : expr.length()) - endat));
                return expr;

            }
        }

        for (i = X - 1; i >= 1; i--) //going back
        {
            a = expr.substring(i - 1, i);

            // a = "."
            if ((int) a.toCharArray()[0] >= 48 && (int) a.toCharArray()[0] <= 58 || a.equals(" ") || a.equals('.') || a.toLowerCase().equals("e") || (a.equals("-") && i == 1)) {
                temp = a + temp;
            } else {
                if ((a.equals("-") || a.equals("+")) && i - 1 > 0) {
                    if (expr.substring(i - 2, 1).equals("e")) {
                        temp = a + temp;
                    } else {
                        start = i + 1;
                        i = 1;
                    }
                } else {
                    start = i + 1;
                    i = 1;
                }
            }
        }
        /*if (!temp.trim().equals("")) {
         //solving for factorial
         if (sign.equals("!")) {
         if (!CTDec(temp).equals((java.math.BigDecimal) Math.floor((double) CTDec(temp)))) {
         //wrong syntax, handle it in whatever way u awnt
         } else {
         expr = expr.substring(0, start - 1) + CNullStr(fact((int) CTDec(temp))) + expr.substring(expr.length() - ((expr == null ? 0 : expr.length()) - X));
         tempsolveFor = expr;
         }
         } else {
         //its not a factorial calculations
         endat = X + ((String) (GetNumFrom(X + 1, expr))).length();
         temp = Eval(GetNumFrom(X + 1, expr), sign, temp);
         expr = expr.substring(0, start - 1) + temp + expr.substring(expr.length() - ((expr == null ? 0 : expr.length()) - endat));
         //Job done, go back
         tempsolveFor = expr;
         }
         } else {
         tempsolveFor = "";
         }*/
        return tempsolveFor;
    }

    private String Eval(String temp, String sign, String prevExpr) throws ParseException {
        String tempEval = null;
        if (sign == "+") {
            tempEval = CNullStr(CTDec(prevExpr).add(CTDec(temp)));
        } else if (sign == "-") {
            tempEval = CNullStr(CTDec(prevExpr).subtract(CTDec(temp)));
        } else if (sign == "*") {
            tempEval = CNullStr(CTDec(prevExpr).multiply(CTDec(temp)));
        } else if (sign == "/") {
            tempEval = CNullStr(CTDec(prevExpr).divide(CTDec(temp)));
        } else if (sign == "^") {
            tempEval = CNullStr(CTDec(prevExpr).pow(2));
        } else {
            tempEval = "";
        }
        return tempEval;
    }

    private java.math.BigDecimal fact(int num) {
        double b = 0;
        b = 1;
        for (num = 1; num <= num; num++) {
            b = b * num; //I wish I could write it as b * = num :(
        }
        return new BigDecimal(b);
    }

    //------------------------------------------------------------------
    // Función que pasa de ASCII a BCD
    // con "parche" de la "D" = "=" para tarjetas de crédito
    //------------------------------------------------------------------
    public final String ASCIItoBCD(String sString, int nBytes) {
        return ASCIItoBCD(sString, nBytes, false);
    }

    public final String ASCIItoBCD(String sString) {
        return ASCIItoBCD(sString, 0, false);
    }

    public final String ASCIItoBCD(String sString, int nBytes, boolean bDerecha) {
        int L = 0;
        int I = 0;
        String S = null;
        String S1 = null;
        String Car = null;
        S = "";
        L = sString == null ? 0 : sString.length();
        if (!((L / 2.0) == (L / 2))) {
            if (bDerecha) {
                sString = sString + "0";
            } else {
                sString = "0" + sString;
            }
        }
        L = sString == null ? 0 : sString.length();
        for (I = 1; I <= L; I++) {
            Car = tangible.DotNetToJavaStringHelper.substring(sString, I - 1, 1);
            S = S + PasaHEXABinario(Car);
        }
        L = (S == null ? 0 : S.length()) / 8;
        S1 = "";
        for (I = 1; I <= L; I++) {
            S1 = S1 + (char) (PasaBINADecimal(tangible.DotNetToJavaStringHelper.substring(S, ((I * 8) - 7) - 1, 8)));

        }
        if (nBytes > 0) {
            for (I = (S1 == null ? 0 : S1.length()); I <= nBytes - 1; I++) {
                if (bDerecha) {
                    S1 = S1 + "\0";
                } else {
                    S1 = "\0" + S1;
                }
            }
        }
        return S1;
    }

    //------------------------------------------------------------------
    // Función que pasa de BCD a ASCII
    //------------------------------------------------------------------
    public final String BCDtoASCII(String sString) {
        int I = 0;
        int sCar = 0;
        String sResp = null;
        String sBinario = null;
        String sBin1 = null;
        String sBin2 = null;
        sResp = "";
        for (I = 1; I <= (sString == null ? 0 : sString.length()); I++) {
            sCar = (int) (sString.substring(I - 1, 1)).charAt(0); ///OJO
            sBinario = PasaDECABinario(sCar, 8);
            sBin1 = PasaBINASCII(sBinario.substring(0, 4));
            sBin2 = PasaBINASCII(sBinario.substring(4, 8));
            sResp = (sResp + sBin1 + sBin2).trim();
        }
        sResp = sResp.trim();
        return sResp;
    }

    //------------------------------------------------------------------
    // Función que pasa de BINARIO a HEX
    //------------------------------------------------------------------
    public final String BINtoHEX(String sBinario) {
        long I = 0;
        String sHex = null;

        sHex = "";
        for (I = 1; I <= (sBinario == null ? 0 : sBinario.length()); I += 4) {
            sHex = sHex + PasaBINASCII(sBinario.substring((int) I - 1, 4));
        }
        return sHex;

    }

    //------------------------------------------------------------------
    // Función que pasa de HEX a BINARIO
    //------------------------------------------------------------------
    public final String HEXtoBIN(String sHex) {
        int I = 0;
        String sBin = null;

        sBin = "";
        for (I = 1; I <= (sHex == null ? 0 : sHex.length()); I++) {
            sBin = sBin + PasaHEXABinario(tangible.DotNetToJavaStringHelper.substring(sHex, I - 1, 1));
        }
        return sBin;

    }

    //------------------------------------------------------------------
    // Función que pasa de ASCII to HEX
    //------------------------------------------------------------------
    //Public Function ASCIItoHEX(ByVal sAsciiData As String) As String
    //    Dim NadEnc As New NadEncriptar
    //    Return NadEnc.ASCIItoHEX(sAsciiData)
    //End Function
    //'------------------------------------------------------------------
    //' Función que pasa de HEX to ASCII
    //'------------------------------------------------------------------
    //Public Function HEXtoASCII(ByVal sHexData As String) As String
    //    Dim NadEnc As New NadEncriptar
    //    Return NadEnc.HEXtoASCII(sHexData)
    //End Function
    //------------------------------------------------------------------
    // Funcion que devuelve el nro. BINARIO de uno HEXADECIMAL adaptada
    //------------------------------------------------------------------
    private String PasaHEXABinario(String NroHex) {
        String tempPasaHEXABinario = null;
        String trimmed = NroHex.trim();
        if (trimmed == "0") {
            tempPasaHEXABinario = "0000";
        } else if (trimmed == "1") {
            tempPasaHEXABinario = "0001";
        } else if (trimmed == "2") {
            tempPasaHEXABinario = "0010";
        } else if (trimmed == "3") {
            tempPasaHEXABinario = "0011";
        } else if (trimmed == "4") {
            tempPasaHEXABinario = "0100";
        } else if (trimmed == "5") {
            tempPasaHEXABinario = "0101";
        } else if (trimmed == "6") {
            tempPasaHEXABinario = "0110";
        } else if (trimmed == "7") {
            tempPasaHEXABinario = "0111";
        } else if (trimmed == "8") {
            tempPasaHEXABinario = "1000";
        } else if (trimmed == "9") {
            tempPasaHEXABinario = "1001";
        } else if (trimmed == "A") {
            tempPasaHEXABinario = "1010";
        } else if (trimmed == "B") {
            tempPasaHEXABinario = "1011";
        } else if (trimmed == "C") {
            tempPasaHEXABinario = "1100";
        } else if (trimmed == "D") {
            tempPasaHEXABinario = "1101";
        } else if (trimmed == "E") {
            tempPasaHEXABinario = "1110";
        } else if (trimmed == "F") {
            tempPasaHEXABinario = "1111";
        } else {
            tempPasaHEXABinario = "";
        }
        return tempPasaHEXABinario;
    }

    //------------------------------------------------------------------
    // Funcion que devuelve el nro. BINARIO de uno decimal Long
    //------------------------------------------------------------------
    //##ModelId=38B418E4023A
    public final String PasaDECABinario(int NroDecimal) {
        return PasaDECABinario(NroDecimal, 0);
    }

    public final String PasaDECABinario(int NroDecimal, int Longitud) {
        String Aux = null;
        int I = 0;
        int Nro = 0;
        int NroAnt = 0;
        Nro = NroDecimal;
        Aux = "";
        while (Nro >= 2) {
            NroAnt = Nro;
            Nro = Nro / 2;
            Aux = (NroAnt - (Nro * 2)) + Aux;
        }
        Aux = (Nro + Aux).trim();
        if (Longitud > 0) {
            if (Longitud > (Aux == null ? 0 : Aux.length())) {
                for (I = 1; I <= Longitud - (Aux == null ? 0 : Aux.length()); I++) {
                    Aux = "0" + Aux;
                }
            }
        }
        return Aux;
    }

    //------------------------------------------------------------------
    // Funcion que devuelve el nro. HEXADECIMAL de un BINARIO
    //------------------------------------------------------------------
    private String PasaBINASCII(String Binario) {
        String tempPasaBINASCII = null;
        if ("0000".equals(Binario)) {

            tempPasaBINASCII = "0";

            if ("0001".equals(Binario)) {
                tempPasaBINASCII = "1";
            }
            if ("0010".equals(Binario)) {
                tempPasaBINASCII = "2";
            }
            if ("0011".equals(Binario)) {
                tempPasaBINASCII = "3";
            }
            if ("0100".equals(Binario)) {
                tempPasaBINASCII = "4";
            }
            if ("0101".equals(Binario)) {
                tempPasaBINASCII = "5";
            }
            if ("0110".equals(Binario)) {
                tempPasaBINASCII = "6";
            }
            if ("0111".equals(Binario)) {
                tempPasaBINASCII = "7";
            }
            if ("1000".equals(Binario)) {
                tempPasaBINASCII = "8";
            }
            if ("1001".equals(Binario)) {
                tempPasaBINASCII = "9";
            }
            if ("1010".equals(Binario)) {
                tempPasaBINASCII = "A";
            }
            if ("1011".equals(Binario)) {
                tempPasaBINASCII = "B";
            }
            if ("1100".equals(Binario)) {
                tempPasaBINASCII = "C";
            }
            if ("1101".equals(Binario)) {
                tempPasaBINASCII = "D";
            }
            if ("1110".equals(Binario)) {
                tempPasaBINASCII = "E";
            }
            if ("1111".equals(Binario)) {
                tempPasaBINASCII = "F";
            } else {
                tempPasaBINASCII = "";
            }

        }
        return tempPasaBINASCII;
    }

    //------------------------------------------------------------------
    // Funcion que devuelve el nro. DECIMAL de uno BINARIO
    //------------------------------------------------------------------
    private int PasaBINADecimal(String NroBinario) {
        int Pos = 0;
        int SubTotal = 0;
        String S = null;
        if (NroBinario.indexOf("1") + 1 == 0) {
            return 0;
        }
        SubTotal = 0;
        S = NroBinario.trim();
        Pos = 0;
        while (S.toUpperCase().indexOf("1".toUpperCase(), Pos) + 1 != 0) {
            Pos = S.toUpperCase().indexOf("1".toUpperCase(), Pos) + 1;
            SubTotal = SubTotal + Exponencial(2, ((S == null ? 0 : S.length()) - Pos));
        }
        return SubTotal;

    }

    //------------------------------------------------------------------
    // Funcion que devuelve el resultado de elevar una Base a un
    // Exponente --> Exponenciacion
    //------------------------------------------------------------------
    private int Exponencial(int Base, int Exponente) {
        int I = 0;
        int Aux = 0;
        if (Exponente == 0) {
            return 1;
        }
        Aux = 1;
        for (I = 1; I <= Exponente; I++) {
            Aux = Aux * Base;
        }
        return Aux;
    }

    ///////////////////ADDED FUNCTIONS////////////////////////////
    private boolean isNumeric(String s) { //equivalente al isNumeric de VB
        return s.matches("[-+]?\\d*\\.?\\d+");
    }

    //trim a cualquier posicion de la cadena
    public static String trimStringFromPosition(String cadena, int left, int rigth, int posD, int posH) {
        String resultTrim = "";
        for (int i = posD; i < posH; i++) {
            if (left == 0 && rigth == 0) {
                if (cadena.charAt(i) != ' ') {
                    resultTrim = cadena.substring(i, posH);
                    i = posH;
                }
            }
            if (left == 0 && rigth == 1) {
                resultTrim = cadena.substring(posD, posH).trim();
                i = posH;
            }
            if (left == 1 && rigth == 1) {
                resultTrim = reverse(trimStringFromPosition(reverse(cadena), 0, 0, posD, posH));
            }
            if (left == 2 && rigth == 2) {
                if (cadena.charAt(i) != ' ') {
                    resultTrim += Character.toString(cadena.charAt(i));
                }
            }
        }
        return resultTrim;
    }

    //reversar un string
    public static String reverse(String input) {
        char[] in = input.toCharArray();
        int begin = 0;
        int end = in.length - 1;
        char temp;
        while (end > begin) {
            temp = in[begin];
            in[begin] = in[end];
            in[end] = temp;
            end--;
            begin++;
        }
        return new String(in);
    }
    
    /**
     * 
     * @param fecha
     * Recibe una cadena en formato 2018-01-31 12:00:00 y retorna el objeto Date generado
     * @return 
     */
    public Date ConvertirStringAFechaHora(String fecha) {
        Date resultDate = null;
        if (fecha.length() > 0) {

            String[] l = fecha.split(" ");
            String[] fechaS = l[0].split("-");
            String[] timeS = l[1].split(":");
            int year, month, day, hour, minute, second = 0;
            year = Integer.parseInt(fechaS[0]);
            month = Integer.parseInt(fechaS[1]);
            day = Integer.parseInt(fechaS[2]);
            hour = Integer.parseInt(timeS[0]);
            minute = Integer.parseInt(timeS[1]);
            second = Integer.parseInt(timeS[2]);
            resultDate = new Date(year - 1900, month - 1, day, hour, minute, second);
        }
        return resultDate;
    }
    
     /**
     * 
     * @param fecha
     * Recibe una cadena en formato 2018-01-31 12:00:00 y retorna el objeto Date generado
     * @return 
     */
    public Date ConvertirStringAFecha(String fecha) {
        Date resultDate = null;
        if (fecha.length() > 0) {
            String[] fechaS = fecha.split("-");
            int year, month, day = 0;
            year = Integer.parseInt(fechaS[0]);
            month = Integer.parseInt(fechaS[1]);
            day = Integer.parseInt(fechaS[2]);
            resultDate = new Date(year - 1900, month - 1, day);
        }
        return resultDate;
    }
}