package TAFE2ApiFirmar;

//import TAFACE2ApiEntidad.*;

import TAFACE2ApiEntidad.TAException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.sql.DatabaseMetaData;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.sqldroid.SQLDroidConnection;

public class NadSQLite {

    /**
     * @return the mvarStrConexion
     */
    public String getMvarStrConexion() {
        return mvarStrConexion;
    }

    /**
     * @param mvarStrConexion the mvarStrConexion to set
     */
    public void setMvarStrConexion(String mvarStrConexion) {
        this.mvarStrConexion = mvarStrConexion;
    }
    //Formatos aceptados de fechas
    //1. YYYY-MM-DD
    //2. YYYY-MM-DD HH:MM
    //3. YYYY-MM-DD HH:MM:SS
    //4. YYYY-MM-DD HH:MM:SS.SSS
    //5. YYYY-MM-DDTHH:MM
    //6. YYYY-MM-DDTHH:MM:SS
    //7. YYYY-MM-DDTHH:MM:SS.SSS
    //8. HH:MM
    //9. HH:MM:SS
    //10. HH:MM:SS.SSS

    public enum TIPO_PROVEEDOR {

        tpSQLiteClient(3);
        private int intValue;
        private static java.util.HashMap<Integer, TIPO_PROVEEDOR> mappings;

        private static java.util.HashMap<Integer, TIPO_PROVEEDOR> getMappings() {
                if (mappings == null) {
                    mappings = new java.util.HashMap<Integer, TIPO_PROVEEDOR>();
                }
            return mappings;
        }

        private TIPO_PROVEEDOR(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static TIPO_PROVEEDOR forValue(int value) {
            return getMappings().get(value);
        }
    }
    private NadFormato Fto = new NadFormato();
    private static final String PassCon = "hf93spdfk+4vepm-52731-jt908as23ldkmfas.df[4230589df253vc69384fyj";
    private static final int EsperaTimeOut = 10;
    private boolean mvarMultipleActiveDataReader; // Permite mantener activo mas de un DataReader simultaneo - Requisito SQL 2005 o superior
    private boolean mvarControlConcurrencia; // Control de Concurrencia - se anula si es factible aplicar DataReader simultaneo
    private boolean mvarPermiso;
    private SQLDroidConnection mvarConexion;
    private boolean mvarPerdirAccesoDato;
    private int mvarCommandTimeout = 60;
    private String mvarArchivoConexion = "";
    private String mvarRegApNombre = "";
    private String mvarNombreArchivo = "";
    private String mvarStrConexion = "";
    private String mvarBaseDato = "";
    private String mvarApNombre = "VS2010";
    private String mvarPassConexion = "";
    private boolean mvarControlError = false;
    private TIPO_PROVEEDOR mvarCxProveedor = TIPO_PROVEEDOR.tpSQLiteClient;

    public final TIPO_PROVEEDOR getProveedorConexion() {
        return mvarCxProveedor;
    }

    public final void setProveedorConexion(TIPO_PROVEEDOR Value) {
        mvarCxProveedor = Value;
    }

    public final void setPedirAccesoDato(boolean Value) {
        mvarPerdirAccesoDato = Value;
    }

    public final void setSetCommandTimeout(int Value) {
        mvarCommandTimeout = Value;
    }

    public final boolean getControlError() {
        return mvarControlError;
    }

    public final void setControlError(boolean Value) {
        mvarControlError = Value;
    }

    public final boolean getControlConcurrencia() {
        return mvarControlConcurrencia;
    }

    public final void setControlConcurrencia(boolean Value) {
        mvarControlConcurrencia = Value;
    }

    public final boolean getMultipleActiveDataReader() {
        return mvarMultipleActiveDataReader;
    }

    public final void setMultipleActiveDataReader(boolean Value) {
        mvarMultipleActiveDataReader = Value;
    }

    public final void setPassConexion(String Value) {
        mvarPassConexion = Value;
    }

    public final String getArchivoConexion() {
        return mvarArchivoConexion;
    }

    public final void setArchivoConexion(String Value) {
        mvarArchivoConexion = Value;
    }

    public final String getNombreArchivo() {
        return mvarNombreArchivo;
    }

    public final void setNombreArchivo(String Value) {
        mvarNombreArchivo = Value;
    }

    public final String getAplicacionNombre() {
        return mvarApNombre;
    }

    public final void setAplicacionNombre(String Value) {
        mvarApNombre = Value;
    }

    public final String getRegistroAplicacion() {
        return mvarRegApNombre;
    }

    public final void setRegistroAplicacion(String Value) {
        mvarRegApNombre = Value;
    }

    public final boolean GrabarArchivo(String BaseDato) throws TAException {

        boolean tempGrabarArchivo = false;
        NadEncriptar NadEnc = new NadEncriptar();
        OutputStreamWriter bw = null;
        String Conexion = null;

        try {
            if (mvarArchivoConexion.trim().equals("")) {
                throw new TAException("Falta Especificar Ruta de Archivo de Conexion a BD");
            }
            mvarBaseDato = BaseDato;
            if (mvarPassConexion == null || mvarPassConexion.length() == 0) {
                mvarPassConexion = PassCon;
            }

            Conexion = "jdbc:sqlite:" + BaseDato;
            Conexion = NadEnc.EncriptarNad(Conexion, mvarPassConexion, true);

            bw = new OutputStreamWriter(new FileOutputStream(mvarArchivoConexion), Charset.forName("UTF-8").newEncoder());
            PrintWriter writer = new PrintWriter(bw);
            writer.print(Conexion);
            bw.close();

            tempGrabarArchivo = true;
        } catch (IOException ex) {
            if (mvarControlError) {
                throw new TAException(ex.getMessage(), ex);
            }
        }
        return tempGrabarArchivo;
    }

    private String SacarCarASCII(String sDato) {
        String sResp = "";
        int nPos = 0;
        sDato = String.valueOf(sDato.replace("\r\n", "")).trim();
        for (nPos = 1; nPos <= (sDato == null ? 0 : sDato.length()); nPos++) {
            if ("0123456789ABCDEF".indexOf(tangible.DotNetToJavaStringHelper.substring(sDato, nPos - 1, 1)) + 1 != 0) {
                sResp = sResp + tangible.DotNetToJavaStringHelper.substring(sDato, nPos - 1, 1);
            }
        }
        return sResp;
    }

    public final boolean Conectar(String BaseDato) throws IOException, TAException, SQLException {
        boolean tempConectar = false;
        try {
            if (!((new java.io.File(mvarStrConexion)).isFile())) {
                throw new IOException("No se encuentra base de datos");
            }
            mvarBaseDato = "TAFACEApi.db";
            tempConectar = VerificarConexion(getMvarStrConexion());
        } catch (IOException ex) {
            if (!mvarControlError) {
                throw ex;
            }
        }
        return tempConectar;
    }

    public final boolean Conectar() throws TAException, SQLException {
        boolean tempConectar = false;
        try {
            mvarBaseDato = "TAFACEApi.db";
            tempConectar = VerificarConexion(mvarStrConexion);
        } catch (TAException TaExBD) {
            if (!mvarControlError) {
                throw TaExBD;
            }
        } catch (IOException ex) {
            if (!mvarControlError) {
                throw new TAException(ex.getMessage(), ex);
            }
        }
        return tempConectar;
    }

    public final void schrinkBD() throws SQLException {
        SQLDroidConnection sqliteConn = (SQLDroidConnection) DriverManager.getConnection("jdbc:sqldroid:" + mvarStrConexion);
        sqliteConn.getDb().beginTransaction();
        sqliteConn.getDb().execSQL("pragma vacuum");
        sqliteConn.getDb().endTransaction();
    }

    private boolean VerificarConexion(String Conexion) throws TAException, SQLException {
            try {
                DriverManager.registerDriver((Driver) Class.forName("org.sqldroid.SQLDroidDriver").newInstance());
                String jdbcUrl = "jdbc:sqldroid:" + Conexion;
                mvarConexion = (SQLDroidConnection) DriverManager.getConnection(jdbcUrl);
                return true;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        return false;
    }

    public final String getBaseDato() {
        if (!((mvarConexion == null))) {
            return mvarBaseDato;
        } else {
            return mvarBaseDato;
        }
    }

    public final boolean CreateBD(String RutaBD, String ScriptDB) throws TAException, SQLException, Exception {
        mvarStrConexion = RutaBD;
        //primero pregunto si existe, entonces no la creo
        File file = new java.io.File(RutaBD);
        String extension = "";
        int i = RutaBD.lastIndexOf('.');
        if (i > 0) {
            extension = RutaBD.substring(i + 1);
        }
        try {
            if (extension.equals("db")) {
                VerificarConexion(RutaBD);
                if (mvarConexion != null) {
                    DatabaseMetaData meta = mvarConexion.getMetaData();
                    Ejecutar(ScriptDB);
                    return true;
                }
            }
        } catch (SQLException e) {
            throw new TAException(e.getMessage());
        }
        return file.isFile() && extension.equals("db");
    }

    public final void CerrarConexion() throws IOException, SQLException {
        try {
            FinPermiso();
            if (!((mvarConexion == null))) {
                if (!mvarConexion.isClosed()) {
                    mvarConexion.close();
                }
            }
        } catch (SQLException ex) {
            throw ex;
        }
        mvarConexion = null;
    }

    private void FinPermiso() {
        mvarPermiso = true;
    }

    private void InicioPermiso() throws InterruptedException {
        if (!mvarControlConcurrencia) {
            return;
        }
        while (!mvarPermiso) {
            Thread.sleep(1);
        }
        mvarPermiso = false;
    }

    public final SQLDroidConnection GetConnection() {
        if (mvarConexion != null) {
            return mvarConexion;
        }
        return null;
    }

    public final ResultSet GetTable(String Sentencia) throws SQLException {

        ResultSet setResult = null;
        PreparedStatement stmt = null;
        try {
            if (mvarConexion == null) {
                Conectar();
                stmt = mvarConexion.prepareStatement(Sentencia);
                setResult = stmt.executeQuery();
            } else {
                stmt = mvarConexion.prepareStatement(Sentencia);
                setResult = stmt.executeQuery();
            }
        } catch (TAException ex) {
            Logger.getLogger(NadSQLite.class.getName()).log(Level.SEVERE, null, ex);
        }
        return setResult;
    }

    //esto en la API de VB6 lo que hace es ensamblar una query SQL a partir de un DataRow, no es necesario
    public final String GetInsertSQL(/*DataRow Fila*/) {
        String SQL = null;
        return SQL;
    }

    //retorna true si la consulta fue exitosa y se obtiene Table con valores
    public final boolean GetTableReg(String Sentencia, tangible.RefObject<ResultSet> Table) throws InterruptedException, SQLException, TAException {
        boolean tempGetTableReg = false;
        ResultSet Ds = null;
        Statement stmt = null;
        try {
            Conectar();
            stmt = mvarConexion.createStatement();
            Ds = stmt.executeQuery(Sentencia);
            if (Ds != null) {
                Table.argValue = Ds;
                tempGetTableReg = true;
            }
        } catch (SQLException ex) {
            throw ex;
        } finally {
            Ds = null;
            FinPermiso();
        }
        return tempGetTableReg;
    }

    //esto lo obtengo del AssemblyQuery
    public final String GetInsertSQL(String Fila) {
        String SQL = null;
        return SQL;
    }

    //esto lo obtengo del AssemblyQuery
    public final String GetUpdateSQL(String Fila) {
        String SQL = null;
        String SQL2 = null;

        return SQL + SQL2;
    }

    public final String GetSQLIdentity(String Tabla, String IdCampo) {
        return "Select * " + IdCampo + " From " + Tabla + " Order By " + IdCampo + " Desc";
    }

    public final Object GetMaxNumero(String Tabla, String IdCampo, String Filtro) throws Exception {
        return GetMaxNumero(Tabla, IdCampo, Filtro, 1);
    }

    public final Object GetMaxNumero(String Tabla, String IdCampo) throws Exception {
        return GetMaxNumero(Tabla, IdCampo, "", 1);
    }

    public final Object GetMaxNumero(String Tabla, String IdCampo, String Filtro, int Inicio) throws Exception {
        String cmd = null;
        long Nro = 0;

        cmd = "Select Max(" + IdCampo + ") + 1 As '" + IdCampo + "'  From " + Tabla;
        cmd += (Filtro != null && Filtro.length() > 0) ? " Where " + Filtro : "";
        Conectar();
        ResultSet set = Ejecutar(cmd);
        while (set.next()) {
            Nro = set.getInt(IdCampo);
        }
        if (Nro < Inicio) {
            Nro = Inicio;
        }
        return Nro;
    }

    public ResultSet Ejecutar(String sql) throws Exception {
        try {

            ResultSet RecordSet = null;
            PreparedStatement objEs;
            Conectar();
            if (mvarConexion == null) {
                throw new Exception("ERROR de Conexion a Base de Datos");
            }
            if (mvarConexion.isClosed()) {
                throw new Exception("ERROR de Conexion a Base de Datos");
            }
            String[] tables = sql.split(";");

            if (tables.length > 0) {
                for (int i = 0; i < tables.length; i++) {
                    objEs = mvarConexion.prepareStatement(tables[i]);
                    objEs.execute();
                }
            } else {
                objEs = mvarConexion.prepareStatement(sql);
                RecordSet = objEs.executeQuery();
            }

            return RecordSet;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void EjecutarModificacionBD(String sql) throws Exception {
        try {

            Statement objEs;
            Conectar();
            if (mvarConexion == null) {
                throw new Exception("ERROR de Conexion a Base de Datos");
            }
            if (mvarConexion.isClosed()) {
                throw new Exception("ERROR de Conexion a Base de Datos");
            }
            objEs = mvarConexion.createStatement();
            objEs.executeQuery(sql);

        } catch (Exception ex) {
            throw ex;
        }
    }

    public boolean IniTrans() throws Exception {
        try {
            if (this.Conectado()) {
                if (mvarConexion.getAutoCommit()) {
                    mvarConexion.setAutoCommit(false);
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void FinTrans(boolean Comitear) throws Exception {
      boolean autocommit = false;
       try {
            if (this.Conectado()) {
                if (mvarConexion.getAutoCommit()) {
                    autocommit = true;
                } else {
                    if (Comitear == true) {
                        mvarConexion.commit();
                    } else {
                        mvarConexion.rollback();
                    }
                }
            }
            if(!autocommit)
                mvarConexion.getDb().endTransaction();
            CerrarConexion();
        } catch (Exception ex) {
            throw ex;
        } finally {
            try {
                mvarConexion.setAutoCommit(true);
            } catch (Exception ex) {
            }
        }
    }

    public boolean Conectado() {
        try {
            return mvarConexion.isClosed() == false;
        } catch (Exception ex) {
            return false;
        }
    }

    public final boolean ExisteCampo(String Tabla, String Campo) throws Exception {
        boolean tempExisteCampo = false;
        Conectar();
        DatabaseMetaData md2 = mvarConexion.getMetaData();
        ResultSet rsTables = md2.getColumns(null, null, Tabla, Campo);
        if (rsTables.next()) {
            tempExisteCampo = true;
        }
        return tempExisteCampo;
    }

    public final boolean ExisteTabla(String Tabla) throws Exception {
        boolean tempExisteTabla = false;
        Conectar();
        DatabaseMetaData md = mvarConexion.getMetaData();
        ResultSet rs = md.getTables(null, null, "%", null);
        while (rs.next()) {
            if (rs.getString(3).equals(Tabla)) {
                tempExisteTabla = true;
            }
        }
        return tempExisteTabla;
    }

    public final boolean ExisteIndice(String Tabla, String NomIndice) throws Exception {
        boolean tempExisteIndice = false;
        Conectar();
        DatabaseMetaData md2 = mvarConexion.getMetaData();
        ResultSet rsTables = md2.getIndexInfo(null, null, Tabla, true, true);
        if (rsTables.next()) {
            tempExisteIndice = true;
        }
        return tempExisteIndice;
    }

    public final boolean BorrarTabla(String Tabla) throws TAException, Exception {
        boolean tempBorrarTabla = false;
        String cmd = null;
        try {
            Conectar();
            if (ExisteTabla(Tabla)) {
                cmd = "DROP TABLE " + Tabla;
                try {
                    Ejecutar(cmd);
                } catch (Exception ex) {
                    throw new TAException("Error al borrar la tabla en la BD.");
                }
            }
            tempBorrarTabla = true;
        } catch (IOException ex) {
            throw new TAException(ex.getMessage(), ex);
        }
        return tempBorrarTabla;
    }

    // AUX METHOD TO COUNT THE ROWS OF RESULT SET
    public int getRowCount(ResultSet resultSet) {
        if (resultSet == null) {
            return 0;
        }
        int i = 0;
        try {
            while (resultSet.next()) {
                i++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(NadSQLite.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
}