package TAFE2ApiFirmar;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TACertificadoDGI {

    public static class CertificadoDGI {

        /**
         * @return the EmpId
         */
        public static String getEmpId() {
            return EmpId;
        }

        /**
         * @param aEmpId the EmpId to set
         */
        public static void setEmpId(String aEmpId) {
            EmpId = aEmpId;
        }

        /**
         * @return the CertDGIId
         */
        public static String getCertDGIId() {
            return CertDGIId;
        }

        /**
         * @param aCertDGIId the CertDGIId to set
         */
        public static void setCertDGIId(String aCertDGIId) {
            CertDGIId = aCertDGIId;
        }

        /**
         * @return the CertDGICN
         */
        public static String getCertDGICN() {
            return CertDGICN;
        }

        /**
         * @param aCertDGICN the CertDGICN to set
         */
        public static void setCertDGICN(String aCertDGICN) {
            CertDGICN = aCertDGICN;
        }

        /**
         * @return the CertDGIArchivo
         */
        public static String getCertDGIArchivo() {
            return CertDGIArchivo;
        }

        /**
         * @param aCertDGIArchivo the CertDGIArchivo to set
         */
        public static void setCertDGIArchivo(String aCertDGIArchivo) {
            CertDGIArchivo = aCertDGIArchivo;
        }

        /**
         * @return the CertDGIVigencia
         */
        public static String getCertDGIVigencia() {
            return CertDGIVigencia;
        }

        /**
         * @param aCertDGIVigencia the CertDGIVigencia to set
         */
        public static void setCertDGIVigencia(String aCertDGIVigencia) {
            CertDGIVigencia = aCertDGIVigencia;
        }

        /**
         * @return the CertDGIVencimiento
         */
        public static String getCertDGIVencimiento() {
            return CertDGIVencimiento;
        }

        /**
         * @param aCertDGIVencimiento the CertDGIVencimiento to set
         */
        public static void setCertDGIVencimiento(String aCertDGIVencimiento) {
            CertDGIVencimiento = aCertDGIVencimiento;
        }

        /**
         * @return the CertDGIFchHoraSync
         */
        public static String getCertDGIFchHoraSync() {
            return CertDGIFchHoraSync;
        }

        /**
         * @param aCertDGIFchHoraSync the CertDGIFchHoraSync to set
         */
        public static void setCertDGIFchHoraSync(String aCertDGIFchHoraSync) {
            CertDGIFchHoraSync = aCertDGIFchHoraSync;
        }

        /**
         * @return the CertDGIRowGUID
         */
        public static String getCertDGIRowGUID() {
            return CertDGIRowGUID;
        }

        /**
         * @param aCertDGIRowGUID the CertDGIRowGUID to set
         */
        public static void setCertDGIRowGUID(String aCertDGIRowGUID) {
            CertDGIRowGUID = aCertDGIRowGUID;
        }
        private Object x;
        private static String EmpId = "EmpId";
        private static String CertDGIId = "CertDGIId";
        private static String CertDGICN = "CertDGICN";
        private static String CertDGIArchivo = "CertDGIArchivo";
        private static String CertDGIVigencia = "CertDGIVigencia";
        private static String CertDGIVencimiento = "CertDGIVencimiento";
        private static String CertDGIFchHoraSync = "CertDGIFchHoraSync";
        private static String CertDGIRowGUID = "CertDGIRowGUID";
    }
    private String sSQL;
    private NadMADato NadDato;
    private NadFormato Fto = new NadFormato();
    private AssemblyQuery assembly = new AssemblyQuery();

    public final boolean CargarCertificadoDGI(int EmpId, java.util.Date FechaGenerado, short DiasAntelacionCertVenc, tangible.RefObject<String> CertDGIArchivo, tangible.RefObject<Integer> CertDGIId, tangible.RefObject<String> WarningMsg) throws SQLException, Exception {

        boolean tempCargarCertificadoDGI = false;
        try {
            ResultSet dt = null;
            tempCargarCertificadoDGI = false;
            sSQL = "Select * From CertificadoDGI Where EmpId = " + EmpId + " And CertDGIVigencia <= " + Fto.FechaSQL(FechaGenerado) + " And CertDGIVencimiento > " + Fto.FechaSQL(FechaGenerado) + " Order by CertDGIVigencia Desc";
            dt = NadDato.GetTable(sSQL);
            if (NadDato.getRowCount(dt) > 0) {
                CertDGIArchivo.argValue = dt.getString(CertificadoDGI.getCertDGIArchivo());
                CertDGIId.argValue = dt.getInt(CertificadoDGI.getCertDGIId());
                Date dat = dt.getDate(TACertificado.Certificado.getCertVencimiento());
                FechaGenerado.setDate(FechaGenerado.getDate() + DiasAntelacionCertVenc);
                if (dat.before(FechaGenerado)) {
                    if (!(HayCertDisponible(dt, FechaGenerado, DiasAntelacionCertVenc))) {
                        if (!WarningMsg.argValue.trim().equals("")) {
                            WarningMsg.argValue += "\r\n";
                        }
                        WarningMsg.argValue += "***ATENCIÓN***: Por favor solicite un nuevo Certificado Digital de DGI para Factura Eletrónica. Verifique el actual porque está próximo a caducar.";
                    }
                }
                tempCargarCertificadoDGI = true;
            }
        } catch (Exception ex) {
            Logger.getLogger(TACertificadoDGI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tempCargarCertificadoDGI;
    }

    private boolean HayCertDisponible(ResultSet dt, java.util.Date FechaGenerado, short DiasAntelacionCertVenc) {
        //no esta implementado, no se usa en la API
        return false;
    }

    public final ResultSet ConsCertificado(int EmpId) throws SQLException {
        ResultSet dt = null;
        sSQL = "Select * From CertificadoDGI Where EmpId = " + EmpId;
        dt = NadDato.GetTable(sSQL);
        return dt;
    }

    public final boolean Eliminar(int EmpId, int CertId) throws Exception {
        boolean tempEliminar = false;
        sSQL = "Delete From CertificadoDGI Where EmpId = " + EmpId + " And CertDGIId = " + CertId;
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

        try {
            sttment.execute();
            tempEliminar = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al eliminar certificado DGI.");
        } finally {
            NadDato.FinTrans();
        }
        return tempEliminar;
    }

    public final boolean Guardar(Map<String, Object> drCert) throws SQLException, Exception {
        boolean tempGuardar = false;
        sSQL = "Select * From CertificadoDGI Where EmpId = " + Integer.parseInt(drCert.get(CertificadoDGI.getEmpId()).toString()) + " And CertDGIId = " + Integer.parseInt(drCert.get(CertificadoDGI.getCertDGIId()).toString());
        if (NadDato.getRowCount(NadDato.GetTable(sSQL)) == 0) {
            sSQL = assembly.AssemblyCertificadoDGIInsert(drCert);
        } else {
            sSQL = assembly.AssemblyCertficadoDGIUpdate(drCert) + " Where EmpId = " + Integer.parseInt(drCert.get(CertificadoDGI.getEmpId()).toString()) + " And CertDGIId = " + Integer.parseInt(drCert.get(CertificadoDGI.getCertDGIId()).toString());
        }
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

        try {
            sttment.execute();
            tempGuardar = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al guardar certificado DGI.");
        } finally {
            NadDato.FinTrans();
        }
        return tempGuardar;
    }

    public final Map<String, Object> CrearCertificadoDGI() {
        //DataTable dt = new DataTable(Tb.CertificadoDGI.Nombre);
        //dt.TableName = Tb.CertificadoDGI.Nombre;
        Map<String, Object> dt = new HashMap<String, Object>();
        dt.put(CertificadoDGI.getEmpId(), Integer.class);
        dt.put(CertificadoDGI.getCertDGIId(), Integer.class);
        dt.put(CertificadoDGI.getCertDGICN(), String.class);
        dt.put(CertificadoDGI.getCertDGIArchivo(), String.class);
        dt.put(CertificadoDGI.getCertDGIVigencia(), java.util.Date.class);
        dt.put(CertificadoDGI.getCertDGIVencimiento(), java.util.Date.class);
        dt.put(CertificadoDGI.getCertDGIFchHoraSync(), String.class);
        dt.put(CertificadoDGI.getCertDGIRowGUID(), String.class);
        return dt;
    }

    public TACertificadoDGI(NadMADato Dato) {
        NadDato = Dato;
        Fto.setProveedorConexion(Dato.getProveedorConexion().getValue());
    }
}