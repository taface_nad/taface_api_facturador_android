package TAFE2ApiFirmar;

import Negocio.ExecutorWS;
import TAFACE2ApiEntidad.TAException;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TAConexion {

    private String ApNombre;
    private String PassCon;
    private NadMADato CxDato;
    private NadMADato.TIPO_PROVEEDOR nProveedorConexion;
    private String mVarStringConexion = "";

    public TAConexion() {

        ApNombre = "TAFACEApiFirmar";
        PassCon = "hf93spdfk+4vepm-52731-jt908as23ldkmfas.df[4230589df253vc69384fyj";
        nProveedorConexion = NadMADato.TIPO_PROVEEDOR.tpSQLiteClient;
        CxDato = new NadMADato();
    }

    public NadMADato.TIPO_PROVEEDOR getProveedorConexion() {
        return nProveedorConexion;
    }

    public void setProveedorConexion(NadMADato.TIPO_PROVEEDOR value) {
        nProveedorConexion = value;
    }

    public String getPassConexion() {
        return PassCon;
    }

    public void Conectar(String Servidor, String BaseDato, String Usuario, String Clave) throws IOException, TAException, SQLException {
        IniciarNadDato();
        CxDato.Conectar(Servidor, BaseDato, Usuario, Clave);
    }

    public void ConectarStr(String StrConexion) throws IOException, SQLException {
        IniciarNadDato();
        CxDato.Conectar(StrConexion);
    }

    public boolean Conectar(String RutaConexion, String UrlServer, short SegudosTimeOut, String EmpRUT, int SucId, int CajId, tangible.RefObject<Boolean> NuevaBD, tangible.RefObject<Boolean> GrabarLogRotura) throws TAException, UnsupportedEncodingException, IOException, SQLException, Exception {

        boolean tempConectar = false;
        int I = 0;
        NuevaBD.argValue = false;
        GrabarLogRotura.argValue = false;
        try {

            while (I < 2) {
                File bdFile = new File(RutaConexion);
                if (!bdFile.isFile()) {

                    if (nProveedorConexion == NadMADato.TIPO_PROVEEDOR.tpSQLiteClient) {

                        CrearBDSQLite(RutaConexion, UrlServer, SegudosTimeOut, EmpRUT, SucId, CajId);

                        NuevaBD.argValue = true;
                    } else {
                        throw new RuntimeException("No se encuentra el punto de conexion. Ruta: " + RutaConexion);
                    }
                }
                IniciarNadDato();
                CxDato.setMvarStringConexion(RutaConexion);
                CxDato.setArchivoConexion(RutaConexion);

                if (CxDato.Conectar()) {

                    tempConectar = true;
                    break;
                } else {

                    if (nProveedorConexion == NadMADato.TIPO_PROVEEDOR.tpSQLiteClient) {

                        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
                        String strRuta = RutaConexion + formato.format(new Date());

                        CxDato.CerrarConexion();

                        File afile = new File(RutaConexion);
                        afile.renameTo(new File(strRuta + afile.getName()));
                        GrabarLogRotura.argValue = true;
                        //Generar log de error
                        TAFACE2ApiEntidad.TALog objLog = new TAFACE2ApiEntidad.TALog(RutaConexion, EmpRUT, SucId, CajId); //check RutaConexion full path
                        objLog.EscribirLog("Error al abrir base de datos, posible rotura. Base de datos renombrada a " + strRuta, TAFACE2ApiEntidad.TALog.enumTipoLog.LogError);
                    } else if (nProveedorConexion == NadMADato.TIPO_PROVEEDOR.tpSQLClient) {
                        CrearBDSQLServer(RutaConexion, UrlServer, SegudosTimeOut, EmpRUT, SucId, CajId);
                        NuevaBD.argValue = true;
                    }
                }
                I += 1;
            }
        } catch (RuntimeException ex) {
            throw ex;
        }
        return tempConectar;
    }

    public NadMADato NadDato() {
        if ((CxDato == null)) {
            CxDato = new NadMADato();
            CxDato.setAplicacionNombre(ApNombre);
            CxDato.setPassConexion(PassCon);
            IniciarNadDato();
        }
        CxDato.setMvarStringConexion(mVarStringConexion);
        return CxDato;
    }

    public void setNadDato(NadMADato dat) {
        CxDato = dat;
        IniciarNadDato();
    }

    public void IniciarNadDato() {
        CxDato.setProveedorConexion(nProveedorConexion);
        CxDato.IniciarNadDato();
        CxDato.setAplicacionNombre(ApNombre);
        CxDato.setPassConexion(PassCon);
        CxDato.setMvarStringConexion(mVarStringConexion);
    }

    public void CrearBDSQLite(String RutaBD, String UrlServidor, short SegundosTimeOut, String EmpRUT, int SucId, int CajId) throws TAException, SQLException, Exception {
        ExecutorWS SWScript = new ExecutorWS();
        SWScript.setURLServer(UrlServidor);
        boolean ErrorReturn = false;
        String ErrorMsj = null;
        String ScriptDB = null;
        CxDato.setProveedorConexion(nProveedorConexion);
        CxDato.setMvarStringConexion(mVarStringConexion);
        tangible.RefObject<Boolean> tempRef_ErrorReturn = new tangible.RefObject<Boolean>(ErrorReturn);
        tangible.RefObject<String> tempRef_ErrorMsj = new tangible.RefObject<String>(ErrorMsj);
        ScriptDB = SWScript.executeScriptBDI((short) CajId, EmpRUT, SucId);
        ErrorReturn = tempRef_ErrorReturn.argValue;
        ErrorMsj = tempRef_ErrorMsj.argValue;
        if (ErrorReturn) {
            throw new TAFACE2ApiEntidad.TAException("Error al obtener script bd" + "\r\n" + ErrorMsj);
        }
        if (ScriptDB.trim().equals("")) {
            throw new TAFACE2ApiEntidad.TAException("ScriptDB recibido es vacio, verificar configuracion en el servidor.", 3203);
        }
        CxDato.IniciarNadDato();
        CxDato.CrearBD(RutaBD, ScriptDB);
    }

    public void CrearBDSQLServer(String ArchivoConexion, String UrlServidor, short SegundosTimeOut, String EmpRUT, int SucId, int CajId) throws TAException, SQLException, Exception {
        ExecutorWS SWScript = new ExecutorWS();
        SWScript.setURLServer(UrlServidor);
        boolean ErrorReturn = false;
        String ErrorMsj = null;
        String ScriptDB = null;
        CxDato.setProveedorConexion(nProveedorConexion);
        tangible.RefObject<Boolean> tempRef_ErrorReturn = new tangible.RefObject<Boolean>(ErrorReturn);
        tangible.RefObject<String> tempRef_ErrorMsj = new tangible.RefObject<String>(ErrorMsj);
        ScriptDB = SWScript.executeScriptBDI((short) CajId, EmpRUT, SucId);
        ErrorReturn = tempRef_ErrorReturn.argValue;
        ErrorMsj = tempRef_ErrorMsj.argValue;
        if (ErrorReturn) {
            throw new TAFACE2ApiEntidad.TAException("Error al obtener script bd" + "\r\n" + ErrorMsj);
        }
        if (ScriptDB.trim().equals("")) {
            throw new TAFACE2ApiEntidad.TAException("ScriptDB recibido es vacio, verificar configuracion en el servidor.", 3203);
        }
        CxDato.IniciarNadDato();
        CxDato.setArchivoConexion(ArchivoConexion);
        CxDato.CrearBD("", ScriptDB);
    }

    public void Liberar(boolean Cerrar) throws IOException, SQLException, Exception {
        try {
            if (!((CxDato == null))) {
                try {
                    CxDato.FinTrans(true);
                } catch (RuntimeException ex) {
                }
                CxDato.CerrarConexion();
            }
            if (Cerrar) {
                CxDato = null;
            }
        } catch (RuntimeException ex) {
            throw ex;
        }
    }

    /**
     * @return the mVarStringConexion
     */
    public String getmVarStringConexion() {
        return mVarStringConexion;
    }

    /**
     * @param mVarStringConexion the mVarStringConexion to set
     */
    public void setmVarStringConexion(String mVarStringConexion) {
        this.mVarStringConexion = mVarStringConexion;
        CxDato.setMvarStringConexion(mVarStringConexion);
    }

    public void schrinkBDSQlite() {
        if (!((CxDato == null))) {
            CxDato.schrinkBD();
        }
    }
}