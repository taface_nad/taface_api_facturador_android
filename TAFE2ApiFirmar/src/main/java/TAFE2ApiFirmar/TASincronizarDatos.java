package TAFE2ApiFirmar;

import Negocio.ExecutorWS;
import Negocio.WSInterface.CAEParte.SDTCAEParteResponse206SDTCAEParteResponse206Item;
import Negocio.WSInterface.CAEParte.WSSincronizacionCAEParte0206ExecuteResponse;
import Negocio.WSInterface.CAEParteAnulado.WSSincronizacionCAEParteAnulada0200ExecuteResponse;
import Negocio.WSInterface.CAEParteUtilizado.WSSincronizacionCAEParteUtilizado0200ExecuteResponse;
import Negocio.WSInterface.CAEParteUtilizadoAnulado.WSSincronizacionCAEParteUtilizadoAnulado0203ExecuteResponse;
import Negocio.WSInterface.Certificado.SDTCertificadoRequestSDTCertificadoRequestItem;
import Negocio.WSInterface.Certificado.SDTCertificadoResponseSDTCertificadoResponseItem;
import Negocio.WSInterface.Certificado.WSSincronizacionCertificados0200ExecuteResponse;
import Negocio.WSInterface.Certificado.XMLCERTIFICADOREQUEST;
import Negocio.WSInterface.CertificadoDGI.SDTCertificadoDGIRequest;
import Negocio.WSInterface.CertificadoDGI.SDTCertificadoDGIRequestSDTCertificadoDGIRequestItem;
import Negocio.WSInterface.CertificadoDGI.SDTCertificadoDGIResponseSDTCertificadoDGIResponseItem;
import Negocio.WSInterface.CertificadoDGI.WSSincronizacionCertificadosDGI0202ExecuteResponse;
import Negocio.WSInterface.Comprobante.WSSincronizacionComprobantes0200ExecuteResponse;
import Negocio.WSInterface.Empresa.SDTEmpresaResponse202SDTEmpresaResponse202Item;
import Negocio.WSInterface.Empresa.WSSincronizacionEmpresas0202ExecuteResponse;
import Negocio.WSInterface.LogErrores.WSSincronizacionLogErrores0200ExecuteResponse;
import Negocio.WSInterface.Sucursal.SDTSucursalResponse205SDTSucursalResponse205Item;
import Negocio.WSInterface.Sucursal.SDTSucursalResponse205SDTSucursalResponse205ItemCajasItem;
import Negocio.WSInterface.Sucursal.WSSincronizacionSucursales0205ExecuteResponse;
import TAFACE2ApiEntidad.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.xml.datatype.DatatypeConfigurationException;
import TAFE2ApiFirmar.TACertificado.Certificado;
import TAFE2ApiFirmar.TACertificadoDGI.CertificadoDGI;

//REFERENCIAS PARA LOS WEB SERVICES
import java.net.MalformedURLException;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import tangible.RefObject;

public class TASincronizarDatos {

    private NadMADato NadDato;
    private NadFormato Fto = new NadFormato();
    ExecutorWS executorWS = new ExecutorWS(); //Object to call the web reference execute

    public final boolean SincronizarCertificadosDGI(int EmpId, int SucId, int CajId, String EmpRUT, String URLWS, int TimeOut) throws SQLException, Exception {
        boolean tempSincronizarCertificadosDGI = false;
        tempSincronizarCertificadosDGI = false;
        TACertificadoDGI CertDGI = null;
        executorWS.setURLServer(URLWS);
        WSSincronizacionCertificadosDGI0202ExecuteResponse lstResp = new WSSincronizacionCertificadosDGI0202ExecuteResponse();
        ResultSet dtCertDGI = null;
        Map<String, Object> drCertDGI = new HashMap<String, Object>();
        ArrayList<Map<String, Object>> listdrCertDGI = new ArrayList<Map<String, Object>>();
        try {
            SDTCertificadoDGIRequest request = new SDTCertificadoDGIRequest();
            CertDGI = new TACertificadoDGI(NadDato);
            dtCertDGI = CertDGI.ConsCertificado(EmpId);
            while (dtCertDGI.next()) {
                SDTCertificadoDGIRequestSDTCertificadoDGIRequestItem Req = new SDTCertificadoDGIRequestSDTCertificadoDGIRequestItem();
                Req.setCertDGIId(dtCertDGI.getInt(CertificadoDGI.getEmpId()));
                Req.setCertDGIRowGUID(dtCertDGI.getString(CertificadoDGI.getCertDGIRowGUID()));
                Req.setEmpId(dtCertDGI.getInt(CertificadoDGI.getEmpId()));

                request.getSDTCertificadoDGIRequestSDTCertificadoDGIRequestItem().add(Req);
            }

            dtCertDGI.close(); //Cierro ResultSet

            lstResp = executorWS.executeCertificadoDGI((short) CajId, EmpId, EmpRUT, SucId, request.getSDTCertificadoDGIRequestSDTCertificadoDGIRequestItem().size() > 0 ? null: request );
            List<SDTCertificadoDGIResponseSDTCertificadoDGIResponseItem> list = lstResp.getPsdtcertificadodgiresponse().getSDTCertificadoDGIResponseSDTCertificadoDGIResponseItem();
            if (lstResp.isPerrorreturn()) {
                throw new TAException(lstResp.getPerrormessage(), 3602);
            }
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).isCertDGIEliminado()) {
                    CertDGI.Eliminar(list.get(i).getEmpId(), list.get(i).getCertDGIId());
                } else {
                    if ((list.get(i).getCertDGIVigencia() == null)) {
                        throw new TAException("CertDGIVigencia no puede ser vacio EmpId: " + list.get(i).getEmpId() + " CertDGIId: " + list.get(i).getCertDGIId());
                    }
                    if ((list.get(i).getCertDGIVencimiento() == null)) {
                        throw new TAException("CertDGIVencimiento no puede ser vacio EmpId: " + list.get(i).getEmpId() + " CertDGIId: " + list.get(i).getCertDGIId());
                    }
                    drCertDGI = CertDGI.CrearCertificadoDGI();
                    drCertDGI.put(CertificadoDGI.getEmpId(), list.get(i).getEmpId());
                    drCertDGI.put(CertificadoDGI.getCertDGIId(), list.get(i).getCertDGIId());
                    drCertDGI.put(CertificadoDGI.getCertDGICN(), list.get(i).getCertDGICN());
                    drCertDGI.put(CertificadoDGI.getCertDGIArchivo(), list.get(i).getCertDGIArchivo());
                    drCertDGI.put(CertificadoDGI.getCertDGIVigencia(), list.get(i).getCertDGIVigencia());
                    drCertDGI.put(CertificadoDGI.getCertDGIVencimiento(), list.get(i).getCertDGIVencimiento());
                    drCertDGI.put(CertificadoDGI.getCertDGIFchHoraSync(), Fto.FechaHoraSQL(new java.util.Date()).replace("'", ""));
                    drCertDGI.put(CertificadoDGI.getCertDGIRowGUID(), list.get(i).getCertDGIRowGUID());
                    listdrCertDGI.add(drCertDGI);
                }
            }

            //recorro finalmente la lista seteando solamente en BD los objetos ya sincronizados
            for (int i = 0; i < listdrCertDGI.size(); i++) {
                CertDGI.Guardar(listdrCertDGI.get(i));
            }
            tempSincronizarCertificadosDGI = true;
        } catch (TAException TaEx) {
            throw TaEx;
        } catch (RuntimeException ex) {
            throw new TAException(ex.getMessage(), 3601);
        }
        return tempSincronizarCertificadosDGI;
    }

    public final boolean SincronizarCertificados(int EmpId, int SucId, int CajId, String EmpRUT, String URLWS, int TimeOut) throws SQLException, Exception {
        boolean tempSincronizarCertificados = false;
        tempSincronizarCertificados = false;
        TACertificado Cert = null;
        executorWS.setURLServer(URLWS);
        WSSincronizacionCertificados0200ExecuteResponse lstResp = new WSSincronizacionCertificados0200ExecuteResponse();
        boolean ErrReturn = false;
        String ErrMensaje = "";
        ResultSet dtCert = null;
        Map<String, Object> drCert = null;
        ArrayList<Map<String, Object>> listdrCert = new ArrayList<Map<String, Object>>();
        try {
            XMLCERTIFICADOREQUEST request = new XMLCERTIFICADOREQUEST();
            Cert = new TACertificado(NadDato);
            dtCert = Cert.ConsCertificado(EmpId);
            while (dtCert.next()) {
                SDTCertificadoRequestSDTCertificadoRequestItem Req = new SDTCertificadoRequestSDTCertificadoRequestItem();
                Req.setCertId(dtCert.getInt(Certificado.getCertId()));
                Req.setCertRowGUID(dtCert.getString(Certificado.getCertRowGUID()));
                Req.setEmpId(dtCert.getInt(Certificado.getEmpId()));
                request.getSDTCertificadoRequestSDTCertificadoRequestItem().add(Req);
            }

            dtCert.close();

            lstResp = executorWS.executeCertificado((short) CajId, EmpId, EmpRUT, SucId, request);
            List<SDTCertificadoResponseSDTCertificadoResponseItem> list = lstResp.getPsdtcertificadoresponse().getSDTCertificadoResponseSDTCertificadoResponseItem();
            if (lstResp.isPerrorreturn()) {
                throw new TAException(lstResp.getPerrormessage(), 3602);
            }
            for (int i = 0; i < list.size(); i++) {

                if (list.get(i).isCertEliminado()) {
                    Cert.Eliminar(list.get(i).getEmpId(), list.get(i).getCertId());
                } else {
                    if ((list.get(i).getCertVigencia() == null)) {
                        throw new TAException("CertVigencia no puede ser vacio EmpId: " + list.get(i).getEmpId() + " CertId: " + list.get(i).getCertId());
                    }
                    if ((list.get(i).getCertVencimiento() == null)) {
                        throw new TAException("CertVencimiento no puede ser vacio EmpId: " + list.get(i).getEmpId() + " CertId: " + list.get(i).getCertId());
                    }
                    drCert = Cert.CrearCertificado();
                    drCert.put(Certificado.getEmpId(), list.get(i).getEmpId());
                    drCert.put(Certificado.getCertId(), list.get(i).getCertId());
                    drCert.put(Certificado.getCertClaveEncriptacion(), list.get(i).getCertClaveEncriptacion());
                    drCert.put(Certificado.getCertArchivo(), list.get(i).getCertArchivo());
                    drCert.put(Certificado.getCertVigencia(), Fto.FechaSQL(list.get(i).getCertVigencia().getTime()).replace("'", ""));
                    drCert.put(Certificado.getCertVencimiento(), Fto.FechaSQL(list.get(i).getCertVencimiento().getTime()).replace("'", ""));
                    drCert.put(Certificado.getCertFchHoraSync(), Fto.FechaHoraSQL(new java.util.Date()).replace("'", ""));
                    drCert.put(Certificado.getCertRowGUID(), list.get(i).getCertRowGUID());
                    listdrCert.add(drCert);
                }
            }

            //recorro finalmente la lista seteando solamente en BD los objetos ya sincronizados
            for (int i = 0; i < listdrCert.size(); i++) {
                Cert.Guardar(listdrCert.get(i));
            }

            tempSincronizarCertificados = true;
        } catch (TAException TaEx) {
            throw TaEx;
        } catch (RuntimeException ex) {
            throw new TAException(ex.getMessage(), 3601);
        }
        return tempSincronizarCertificados;
    }

    public final boolean SincronizarCAEParte(int EmpId, int SucId, int CajId, String EmpRUT, String URLWS, int TimeOut) throws TAException, SQLException, Exception {
        boolean tempSincronizarCAEParte = false;
        tempSincronizarCAEParte = false;
        TACAE CAE = null;
        executorWS.setURLServer(URLWS);
        WSSincronizacionCAEParte0206ExecuteResponse Resp = null;
        List<SDTCAEParteResponse206SDTCAEParteResponse206Item> listaItem = null;
        List<Map<String, Object>> dtCAEParte = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> dtCAEParteaSincronizar = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> dtCAEaSincronizar = new ArrayList<Map<String, Object>>();
        Map<String, Object> drCAE = new HashMap<String, Object>();
        Map<String, Object> drCAEParte = new HashMap<String, Object>();
        try {
            CAE = new TACAE(NadDato);
            dtCAEParte = CAE.ConsListMapCAEParte(EmpId);
            WakeUpServer(URLWS, TimeOut);
            Resp = executorWS.executeCAEParte(EmpId, EmpRUT, SucId, (short) CajId, dtCAEParte);
            if (Resp.isPerrorreturn()) {
                throw new TAException(Resp.getPerrormessage(), 3604);
            }
            listaItem = Resp.getPsdtcaeparteresponse().getSDTCAEParteResponse206SDTCAEParteResponse206Item();
            if (listaItem != null) {
                for (int i = 0; i < listaItem.size(); i++) {
                    if (listaItem.get(i).isCAEParteEliminado()) {
                        CAE.EliminarCAEParte(listaItem.get(i).getEmpId(), listaItem.get(i).getCAEId(), listaItem.get(i).getCAEParteId());
                    } else {
                        //drCAE.put(CAE.Tb.CAE.Cp.EmpId.toString(), listaItem.get(i).getCAE().getEmpId());

                        drCAE = CAE.CrearCAE();
                        drCAE.put(TACAE.CAE.getEmpId(), listaItem.get(i).getCAE().getEmpId());
                        drCAE.put(TACAE.CAE.getCAEId(), listaItem.get(i).getCAE().getCAEId());
                        drCAE.put(TACAE.CAE.getCAENroAutorizacion(), listaItem.get(i).getCAE().getCAENroAutorizacion());
                        drCAE.put(TACAE.CAE.getCAETipoCFE(), listaItem.get(i).getCAE().getCAETipoCFE());
                        drCAE.put(TACAE.CAE.getCAESerie(), listaItem.get(i).getCAE().getCAESerie());
                        drCAE.put(TACAE.CAE.getCAENroDesde(), listaItem.get(i).getCAE().getCAENroDesde());
                        drCAE.put(TACAE.CAE.getCAENroHasta(), listaItem.get(i).getCAE().getCAENroHasta());
                        drCAE.put(TACAE.CAE.getCAEVigencia(),Fto.FechaSQL(listaItem.get(i).getCAE().getCAEVigencia().getTime()).replace("'", ""));
                        drCAE.put(TACAE.CAE.getCAEVencimiento(),Fto.FechaSQL(listaItem.get(i).getCAE().getCAEVencimiento().getTime()).replace("'", ""));
                        drCAE.put(TACAE.CAE.getCAEAnulado(), listaItem.get(i).getCAE().isCAEAnulado() ? 1 : 0);
                        drCAE.put(TACAE.CAE.getCAEFchHoraSync(), Fto.FechaHoraSQL(listaItem.get(i).getCAE().getCAEFchHora().getTime()).replace("'", ""));
                        drCAE.put(TACAE.CAE.getCAEEspecial(), listaItem.get(i).getCAE().getCAEEspecial());
                        drCAE.put(TACAE.CAE.getCAECAusalTipo(), listaItem.get(i).getCAE().getCAECAusalTipo());
                        dtCAEaSincronizar.add(drCAE);

                        drCAEParte = CAE.CrearCAEParte();
                        drCAEParte.put(TACAE.CAEParte.getEmpId(), listaItem.get(i).getEmpId());
                        drCAEParte.put(TACAE.CAEParte.getSucId(), listaItem.get(i).getSucId());
                        drCAEParte.put(TACAE.CAEParte.getCajaId(), listaItem.get(i).getCajaId());
                        drCAEParte.put(TACAE.CAEParte.getCAEId(), listaItem.get(i).getCAEId());
                        drCAEParte.put(TACAE.CAEParte.getCAEParteId(), listaItem.get(i).getCAEParteId());
                        drCAEParte.put(TACAE.CAEParte.getCAEParteNroDesde(), listaItem.get(i).getCAEParteNroDesde());
                        drCAEParte.put(TACAE.CAEParte.getCAEParteNroHasta(), listaItem.get(i).getCAEParteNroHasta());
                        drCAEParte.put(TACAE.CAEParte.getCAEParteFchHoraSync(), Fto.FechaHoraSQL(new java.util.Date()).replace("'", ""));
                        drCAEParte.put(TACAE.CAEParte.getCAEParteUltAsignado(), listaItem.get(i).getCAEParteUltAsignado());
                        drCAEParte.put(TACAE.CAEParte.getCAEParteFchHoraUltSync(), Fto.FechaHoraSQL(new java.util.Date()).replace("'", ""));
                        drCAEParte.put(TACAE.CAEParte.getCAEParteRowGUID(), listaItem.get(i).getCAEParteRowGUID());
                        drCAEParte.put(TACAE.CAEParte.getCAEParteAnulado(), listaItem.get(i).getCAE().isCAEAnulado() ? 1 : 0);
                        drCAEParte.put(TACAE.CAEParte.getCAEParteAnuladoSync(), 0);
                        dtCAEParteaSincronizar.add(drCAEParte);
                    }
                }
            }

            for (int i = 0; i < dtCAEaSincronizar.size(); i++) {
                CAE.GrabarCAE(dtCAEaSincronizar.get(i));
            }

            for (int i = 0; i < dtCAEParteaSincronizar.size(); i++) {
                CAE.GrabarCAEParte(dtCAEParteaSincronizar.get(i));
            }

            for (int i = 0; i < listaItem.size(); i++) {
                CAE.CancelarCAEReservadosCAEParteAnulada(listaItem.get(i).getEmpId(), listaItem.get(i).getCAEId(), listaItem.get(i).getCAEParteId());
            }

            tempSincronizarCAEParte = true;
        } catch (TAException TaEx) {
            throw TaEx;
        } catch (RuntimeException ex) {
            throw new TAException(ex.getMessage(), 3603);
        }
        return tempSincronizarCAEParte;
    }

    public final WSSincronizacionCAEParte0206ExecuteResponse ObtenerCAEParte(int EmpId, int SucId, int CajId, String EmpRUT, String URLWS, int TimeOut) throws TAException, SQLException {
        WSSincronizacionCAEParte0206ExecuteResponse lstResp = null;
        int Cont = 0;
        boolean ErrReturn = false;
        List<Map<String, Object>> dtCAEParte = new ArrayList<Map<String, Object>>();
        String ErrMensaje = "";
        executorWS.setURLServer(URLWS);
        try {
            WakeUpServer(URLWS, TimeOut);
            lstResp = executorWS.executeCAEParte(EmpId, EmpRUT, SucId, (short) CajId, dtCAEParte);
            if (lstResp.isPerrorreturn()) {
                throw new TAException(lstResp.getPerrormessage(), 3604);
            }
            return lstResp;
        } catch (TAException TaEx) {
            throw TaEx;
        } catch (RuntimeException ex) {
            throw new TAException(ex.getMessage(), 3603);
        }
    }

    public final boolean SincronizarEmpresas(String EmpRUT, int SucId, int CajId, String URLWS, int TimeOut, tangible.RefObject<Integer> EmpId) throws SQLException, TAException {
        boolean tempSincronizarEmpresas = false;
        tempSincronizarEmpresas = false;
        TAEmpresa Emp = null;
        executorWS.setURLServer(URLWS);
        WSSincronizacionEmpresas0202ExecuteResponse lstResp = new WSSincronizacionEmpresas0202ExecuteResponse();
        String ErrMensaje = "";
        String EmpRowGUID = "";
        ResultSet dtEmp = null;
        Map<String, Object> drEmp = new HashMap<String, Object>();
        try {
            Emp = new TAEmpresa(NadDato);
            dtEmp = Emp.ConsEmpresaRut(EmpRUT);
            if (NadDato.getRowCount(dtEmp) > 0) {
                dtEmp = Emp.ConsEmpresaRut(EmpRUT);
                while (dtEmp.next()) {
                    EmpRowGUID = dtEmp.getString(TAEmpresa.Empresa.getEmpRowGUID());
                    EmpId.argValue = dtEmp.getInt(TAEmpresa.Empresa.getEmpId());
                }
            }

            dtEmp.close();

            lstResp = executorWS.executeEmpresa((short) CajId, EmpRowGUID, EmpRUT, SucId);
            if (lstResp != null) {
                if (lstResp.isPerrorreturn()) {
                    throw new TAException(ErrMensaje, 3608);
                }

                List<SDTEmpresaResponse202SDTEmpresaResponse202Item> listaItem = lstResp.getPsdtempresaresponse().getSDTEmpresaResponse202SDTEmpresaResponse202Item();
                for (int i = 0; i < listaItem.size(); i++) {
                    drEmp = Emp.CrearEmpresa();
                    drEmp.put(TAEmpresa.Empresa.getEmpId(), listaItem.get(i).getEmpId());
                    drEmp.put(TAEmpresa.Empresa.getEmpRUC(), listaItem.get(i).getEmpRUC());
                    drEmp.put(TAEmpresa.Empresa.getEmpNom(), listaItem.get(i).getEmpNom());
                    drEmp.put(TAEmpresa.Empresa.getEmpEmiteTickets(), listaItem.get(i).isEmpEmiteTickets() ? 1 : 0);
                    drEmp.put(TAEmpresa.Empresa.getEmpEmiteFacturas(), listaItem.get(i).isEmpEmiteFacturas() ? 1 : 0);
                    drEmp.put(TAEmpresa.Empresa.getEmpEmiteRemitos(), listaItem.get(i).isEmpEmiteRemitos() ? 1 : 0);
                    drEmp.put(TAEmpresa.Empresa.getEmpEmiteResguardos(), listaItem.get(i).isEmpEmiteResguardos() ? 1 : 0);
                    drEmp.put(TAEmpresa.Empresa.getEmpEmiteFacturasExportacion(), listaItem.get(i).isEmpEmiteFacturasExportacion() ? 1 : 0);
                    drEmp.put(TAEmpresa.Empresa.getEmpEmiteRemitosExportacion(), listaItem.get(i).isEmpEmiteRemitosExportacion() ? 1 : 0);
                    drEmp.put(TAEmpresa.Empresa.getEmpEmiteCuentaAjena(), listaItem.get(i).isEmpEmiteCuentaAjena() ? 1 : 0);
                    drEmp.put(TAEmpresa.Empresa.getEmpResolucionIVA(), listaItem.get(i).getEmpResolucionIVA());
                    drEmp.put(TAEmpresa.Empresa.getEmpLogoImpresion(), listaItem.get(i).getEmpLogoImpresion());
                    drEmp.put(TAEmpresa.Empresa.getEmpLogoMiniatura(), listaItem.get(i).getEmpLogoMiniatura());
                    drEmp.put(TAEmpresa.Empresa.getEmpUrlConsultaCFE(), listaItem.get(i).getEmpUrlConsultaCFE());
                    drEmp.put(TAEmpresa.Empresa.getEmpFchHoraSync(), Fto.FechaHoraSQL(new java.util.Date()).replace("'", ""));
                    drEmp.put(TAEmpresa.Empresa.getEmpRowGUID(), listaItem.get(i).getEmpRowGUID());
                    Emp.GrabarEmpresa(drEmp);
                    EmpId.argValue = listaItem.get(i).getEmpId();
                }
                tempSincronizarEmpresas = true;
            }
        } catch (TAException TaEx) {
            throw TaEx;
        } catch (RuntimeException ex) {
            throw new TAException(ex.getMessage(), 3607);
        } catch (MalformedURLException ex) {
            throw new TAException(ex.getMessage(), 3607);
        } catch (Exception ex) {
            throw new TAException(ex.getMessage(), 3607);
        }
        return tempSincronizarEmpresas;
    }

    public final boolean SincronizarSucursales(int EmpId, int SucId, int CajaId, String EmpRUT, String URLWS, int TimeOut, boolean IsBDNueva, String CajaLicenceId, RefObject<Boolean> FlagCerrarFirmaCajaUnId) throws SQLException, Exception {
        boolean tempSincronizarSucursales = false;
        tempSincronizarSucursales = false;
        TAEmpresa Sucursal = null;
        executorWS.setURLServer(URLWS);
        WSSincronizacionSucursales0205ExecuteResponse lstResp = new WSSincronizacionSucursales0205ExecuteResponse();
        String SucRowGUID = "";
        String ListaIdCajas = "";
        ResultSet dtSuc = null;
        Map<String, Object> drSuc = new HashMap<String, Object>();
        Map<String, Object> drCaja = new HashMap<String, Object>();
        List<Map<String, Object>> drListSuc = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> drListCaja = new ArrayList<Map<String, Object>>();
        List<String> listCajasQueNoEstanEnLista = new ArrayList<String>();
        try {
            Sucursal = new TAEmpresa(NadDato);
            dtSuc = Sucursal.ConsSucursal(EmpId, SucId);
            ResultSet temp = dtSuc;
            if (NadDato.getRowCount(temp) > 0) {
                while (dtSuc.next()) {
                    SucRowGUID = dtSuc.getString(TAEmpresa.Sucursal.getSucRowGUID());
                }
            }

            lstResp = executorWS.executeSucursal(EmpId, EmpRUT, SucId, SucRowGUID);
            if (lstResp.isPerrorreturn()) {
                throw new TAException(lstResp.getPerrormessage(), 3610);
            }
            List<SDTSucursalResponse205SDTSucursalResponse205Item> list = lstResp.getPsdtsucursalresponse().getSDTSucursalResponse205SDTSucursalResponse205Item();
            if ((list != null)) {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).isSucEliminada()) {
                        Sucursal.EliminarEmpresa(list.get(i).getEmpId());
                    } else {
                        drSuc = Sucursal.CrearSucursal();
                        drSuc.put(TAEmpresa.Sucursal.getEmpId(), list.get(i).getEmpId());
                        drSuc.put(TAEmpresa.Sucursal.getSucId(), list.get(i).getSucId());
                        drSuc.put(TAEmpresa.Sucursal.getSucNom(), list.get(i).getSucNom());
                        drSuc.put(TAEmpresa.Sucursal.getSucCodEnDGI(), list.get(i).getSucCodEnDGI());
                        drSuc.put(TAEmpresa.Sucursal.getSucRowGUID(), list.get(i).getSucRowGUID());
                        drSuc.put(TAEmpresa.Sucursal.getSucFchHoraUltSync(), Fto.FechaHoraSQL(new java.util.Date()).replace("'", ""));

                        drListSuc.add(drSuc);
                        ListaIdCajas = "";
                        List<SDTSucursalResponse205SDTSucursalResponse205ItemCajasItem> listCajas = list.get(i).getCajas().getCajasItem();
                        for (int j = 0; j < listCajas.size(); j++) {

                            drCaja = Sucursal.CrearSucursalCaja();
                            drCaja.put(TAEmpresa.SucursalCaja.getEmpId(), list.get(i).getEmpId());
                            drCaja.put(TAEmpresa.SucursalCaja.getSucId(), list.get(i).getSucId());
                            drCaja.put(TAEmpresa.SucursalCaja.getCajaId(), listCajas.get(j).getCajaId());
                            //Compruebo que el CajaLicenceId que envia el Server coincide con el que tengo en la SQlite, de lo contrario detengo firma renombro SQlite y borro config de la sucursal
                            if (IsBDNueva) {
                                drCaja.put(TAEmpresa.SucursalCaja.getCajaUniqueId(), CajaLicenceId);
                            } else {
                                if (listCajas.get(j).getCajaId() == CajaId && (CajaLicenceId.equals(listCajas.get(j).getCajaUniqueId()) || listCajas.get(j).getCajaUniqueId().equals(""))) {
                                    drCaja.put(TAEmpresa.SucursalCaja.getCajaUniqueId(), CajaLicenceId);
                                } else if (listCajas.get(j).getCajaId() == CajaId && !CajaLicenceId.equals(listCajas.get(j).getCajaUniqueId())) {
                                    //TENGO QUE RENOMBRAR LA SQLITE Y BORRAR FICHERO CONFIG SUC
                                    FlagCerrarFirmaCajaUnId.argValue = true;
                                    drCaja.put(TAEmpresa.SucursalCaja.getCajaUniqueId(), CajaLicenceId);
                                } else {
                                    drCaja.put(TAEmpresa.SucursalCaja.getCajaUniqueId(), listCajas.get(j).getCajaUniqueId());
                                }
                            }
                            drListCaja.add(drCaja);
                            ListaIdCajas += listCajas.get(j).getCajaId() + ",";
                        }
                        ListaIdCajas = ListaIdCajas.substring(0, ListaIdCajas.length() - 1); // saco ultima coma ","
                        //Elimino las cajas que no estan en la lista de cajas recibidas
                        listCajasQueNoEstanEnLista.add(ListaIdCajas);
                    }
                }
            }

            for (int i = 0; i < drListSuc.size(); i++) {
                Sucursal.GrabarSucursal(drListSuc.get(i));
                Sucursal.EliminarCajasQueNoEstanEnLista(Integer.parseInt(drListSuc.get(i).get("EmpId").toString()), Integer.parseInt(drListSuc.get(i).get("SucId").toString()), listCajasQueNoEstanEnLista.get(i));
            }

            for (int i = 0; i < drListCaja.size(); i++) {
                Sucursal.GrabarCaja(drListCaja.get(i));
            }


            tempSincronizarSucursales = true;
        } catch (TAException TaEx) {
            throw TaEx;
        } catch (RuntimeException ex) {
            throw new TAException(ex.getMessage(), 3609);
        }
        return tempSincronizarSucursales;
    }

    public final boolean SincronizarComprobantes(int EmpId, int SucId, int CajaId, int CantFacturas, String URLWS, int TimeOut) throws SQLException, TAException, Exception {
        boolean tempSincronizarCombrobantes = false;
        TAComprobante Comprobante = null;
        String ErrMensajeRet = "";
        ResultSet dtComprobantes = null;
        executorWS.setURLServer(URLWS);
        WSSincronizacionComprobantes0200ExecuteResponse dr = null;
        List<Map<String, Object>> cteMirrorList = resultSetToListMapArray(dtComprobantes); // mirror list para mantener el estado de los comprobantes sincronizados
        int PCteId = -1;
        try {
            Comprobante = new TAComprobante(NadDato);
            dtComprobantes = Comprobante.ConsComprobantesPorSincronizar(EmpId, SucId, (short) CajaId, CantFacturas);
            if (NadDato.getRowCount(dtComprobantes) == 0) {
                return true;
            } else {
                dtComprobantes = Comprobante.ConsComprobantesPorSincronizar(EmpId, SucId, (short) CajaId, CantFacturas);
                List<Map<String, Object>> list = resultSetToListMapArray(dtComprobantes);

                dtComprobantes.close();

                for (int i = 0; i < list.size(); i++) {
                    try {
                        int PCAEId = Fto.CTInt(Integer.parseInt(list.get(i).get(TAComprobante.Comprobante.getCAEId()).toString()));
                        int PCAEParteId = Fto.CTInt(Integer.parseInt(list.get(i).get(TAComprobante.Comprobante.getCAEParteId()).toString()));
                        int PCertId = Fto.CTInt(Integer.parseInt(list.get(i).get(TAComprobante.Comprobante.getCertId()).toString()));
                        String PCteAdendaTexto = Fto.CNullStr(list.get(i).get(TAComprobante.Comprobante.getCteAdendaTexto()).toString());
                        String PCteCajaUniqueid = Fto.CNullStr(list.get(i).get(TAComprobante.Comprobante.getCteCajaUniqueId()).toString());
                        PCteId = Fto.CTInt(Integer.parseInt(list.get(i).get(TAComprobante.Comprobante.getCteId()).toString()));
                        String PCteSucUniqueId = "";
                        String PCteVersionApi = Fto.CNullStr(list.get(i).get(TAComprobante.Comprobante.getCteVersionAPI()).toString());
                        String PCteVersionGW = "";
                        String PCteXMLEntrada = Fto.CNullStr(list.get(i).get(TAComprobante.Comprobante.getCteXMLEntrada()).toString());
                        String PCteXMLFirmado = Fto.CNullStr(list.get(i).get(TAComprobante.Comprobante.getCteXMLFirmado()).toString());
                        String PCteXMLRespuesta = Fto.CNullStr(list.get(i).get(TAComprobante.Comprobante.getCteXMLRespuesta()).toString());

                        dr = executorWS.executeComprobante(PCAEId, PCAEParteId, (short) CajaId, PCertId,
                                PCteAdendaTexto, PCteCajaUniqueid, PCteId, PCteSucUniqueId, PCteVersionApi, PCteVersionGW, PCteXMLEntrada, PCteXMLFirmado,
                                PCteXMLRespuesta, EmpId, SucId);

                        if (dr.isPerrorreturn()) {
                            throw new TAException(dr.getPerrormessage(), 3606);
                        } else {
                            if(dr.getPcteidnew() > 0) {
                                int CteSyncId = dr.getPcteidnew();
                                list.get(i).put("CteSyncId", CteSyncId);
                                cteMirrorList.add(list.get(i));
                            }
                        }

                    } catch (RuntimeException ex) {
                        ErrMensajeRet += ex.getMessage() + "\r\n";
                    }
                }

                for (int i = 0; i < cteMirrorList.size(); i++) {
                    Comprobante.ActualizarComprobanteEnviado(Integer.parseInt(cteMirrorList.get(i).get("CteSyncId").toString()), Integer.parseInt(cteMirrorList.get(i).get("EmpId").toString()),
                            Integer.parseInt(cteMirrorList.get(i).get("SucId").toString()), Integer.parseInt(cteMirrorList.get(i).get("CajaId").toString()),
                            Integer.parseInt(cteMirrorList.get(i).get("CteId").toString()));
                }
            }
            if (!ErrMensajeRet.trim().equals("")) {
                throw new TAException(ErrMensajeRet, 3611);
            }
            tempSincronizarCombrobantes = true;
        } catch (TAException TaEx) {
            throw TaEx;
        } catch (RuntimeException ex) {
            throw new TAException(ex.getMessage(), 3605);
        }
        return tempSincronizarCombrobantes;
    }

    public final boolean SincronizarCAEUtilizado(int EmpId, String URLWS, int TimeOut) throws SQLException, TAException, DatatypeConfigurationException, Exception {
        boolean tempSincronizarCAEUtilizado = false;
        tempSincronizarCAEUtilizado = false;
        TACAE CAE = null;
        String ErrMensajeRet = "";
        ResultSet dtCAEUtil = null;
        executorWS.setURLServer(URLWS);
        try {
            CAE = new TACAE(NadDato);
            dtCAEUtil = CAE.ConsCAEUtilPorSincronizar(EmpId);
            if (NadDato.getRowCount(dtCAEUtil) == 0) {
                return true;
            } else {
                WakeUpServer(URLWS, TimeOut);
                dtCAEUtil = CAE.ConsCAEUtilPorSincronizar(EmpId);
                List<Map<String, Object>> list = resultSetToListMapArray(dtCAEUtil);

                dtCAEUtil.close();

                for (int i = 0; i < list.size(); i++) {
                    try {
                        int PCAEId = Fto.CTInt(Integer.parseInt(list.get(i).get(TACAE.CAEParteUtilizado.getCAEId()).toString()));
                        int PCAEParteId = Fto.CTInt(Integer.parseInt(list.get(i).get(TACAE.CAEParteUtilizado.getCAEParteId()).toString()));
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        Date date = formatter.parse(list.get(i).get(TACAE.CAEParteUtilizado.getCAEParteFchUtilizado()).toString());
                        int PCAEParteUtilNroDesde = (int) Fto.CTLng(Integer.parseInt(list.get(i).get(TACAE.CAEParteUtilizado.getCAEParteUtilNroDesde()).toString()));
                        int PCAEParteUtilNroHasta = (int) Fto.CTLng(Integer.parseInt(list.get(i).get(TACAE.CAEParteUtilizado.getCAEParteUtilNroHasta()).toString()));

                        WSSincronizacionCAEParteUtilizado0200ExecuteResponse resp = executorWS.executeCAEParteUtilizado(PCAEId, date, PCAEParteId, PCAEParteUtilNroDesde, PCAEParteUtilNroHasta, EmpId);
                        //executorWS.executeCAEParteUtilizado(PCAEId, date, PCAEParteId, PCAEParteUtilNroDesde, PCAEParteUtilNroHasta, EmpId);

                        if (!resp.isPerrorreturn()) {
                            //dtCAEUtil.updateInt(TACAE.CAEParteUtilizado.getCAEParteUtilSincronizado(), 1);
                            //CAE.ActualizarCAEUtilizadoEnviado(dtCAEUtil);
                            CAE.ActualizarCAEUtilizadoEnviado(EmpId, PCAEId, PCAEParteId, date, PCAEParteUtilNroHasta);
                        } else {
                            ErrMensajeRet += resp.getPerrormessage() + "\r\n";
                        }
                    } catch (RuntimeException ex) {
                        ErrMensajeRet += ex.getMessage() + "\r\n";
                    }
                }
            }
            if (!ErrMensajeRet.trim().equals("")) {
                throw new TAException(ErrMensajeRet, 3611);
            }
            tempSincronizarCAEUtilizado = true;
        } catch (TAException TaEx) {
            throw TaEx;
        } catch (RuntimeException ex) {
            throw new TAException(ex.getMessage(), 3612);
        }
        return tempSincronizarCAEUtilizado;
    }

    public final boolean SincronizarCAEParteAnulado(int EmpId, int SucId, int CajaId, String URLWS, int TimeOut) throws SQLException, Exception {
        boolean tempSincronizarCAEParteAnulado = false;
        tempSincronizarCAEParteAnulado = false;
        TACAE CAE = null;
        String ErrMensajeRet = "";
        ResultSet dtCAEParteAnulado = null;
        executorWS.setURLServer(URLWS);
        try {
            CAE = new TACAE(NadDato);
            dtCAEParteAnulado = CAE.ConsCAEParteAnuladoPorSincronizar(EmpId, SucId, CajaId);
            if (NadDato.getRowCount(dtCAEParteAnulado) == 0) {
                return true;
            } else {
                WakeUpServer(URLWS, TimeOut);
                dtCAEParteAnulado = CAE.ConsCAEParteAnuladoPorSincronizar(EmpId, SucId, CajaId);
                while (dtCAEParteAnulado.next()) {
                    try {
                        int PCAEId = Fto.CTInt(dtCAEParteAnulado.getInt(TACAE.CAEParte.getCAEId()));
                        int PCAEParteId = Fto.CTInt(dtCAEParteAnulado.getInt(TACAE.CAEParte.getCAEParteId()));

                        WSSincronizacionCAEParteAnulada0200ExecuteResponse resp = executorWS.executeCAEParteAnulado(PCAEId, PCAEParteId, EmpId);
                        if (!resp.isPerrorreturn()) {
                            CAE.ActualizarCAEParteAnulado(EmpId, PCAEId, PCAEParteId);
                        } else {
                            ErrMensajeRet += resp.getPerrormessage() + "\r\n";
                        }
                    } catch (RuntimeException ex) {
                        ErrMensajeRet += ex.getMessage() + "\r\n";
                    }
                }
            }
            if (!ErrMensajeRet.trim().equals("")) {
                throw new TAException(ErrMensajeRet, 3611);
            }
            tempSincronizarCAEParteAnulado = true;
        } catch (TAException TaEx) {
            throw TaEx;
        } catch (RuntimeException ex) {
            throw new TAException(ex.getMessage(), 3612);
        }
        return tempSincronizarCAEParteAnulado;
    }

    public final boolean SincronizarCAEUtilAnulado(int EmpId, String URLWS, int TimeOut) throws SQLException, Exception {
        boolean tempSincronizarCAEUtilAnulado = false;
        tempSincronizarCAEUtilAnulado = false;
        TACAE CAE = null;
        String ErrMensajeRet = "";
        ResultSet dtCAEUtilAnul = null;
        executorWS.setURLServer(URLWS);
        try {
            CAE = new TACAE(NadDato);
            dtCAEUtilAnul = CAE.ConsCAEUtilAnulPorSincronizar(EmpId);
            if (NadDato.getRowCount(dtCAEUtilAnul) == 0) {
                return true;
            } else {
                WakeUpServer(URLWS, TimeOut);
                dtCAEUtilAnul = CAE.ConsCAEUtilAnulPorSincronizar(EmpId);
                List<Map<String, Object>> list = resultSetToListMapArray(dtCAEUtilAnul);
                for (int i = 0; i < list.size(); i++) {
                    try {
                        int CAEId = Fto.CTInt(Integer.parseInt(list.get(i).get(TACAE.CAEParteUtilizadoAnulado.getCAEId()).toString()));
                        int CAEParteId = Fto.CTInt(Integer.parseInt(list.get(i).get(TACAE.CAEParteUtilizadoAnulado.getCAEParteId()).toString()));
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        Date CAEParteFchUtilizado = formatter.parse(list.get(i).get(TACAE.CAEParteUtilizadoAnulado.getCAEParteFchUtilizado()).toString());
                        int CAEParteAnulNro = (int) Fto.CTLng(Integer.parseInt(list.get(i).get(TACAE.CAEParteUtilizadoAnulado.getCAEParteAnulNro()).toString()));
                        WSSincronizacionCAEParteUtilizadoAnulado0203ExecuteResponse resp = executorWS.executeCAEParteUtilizadoAnulado(CAEId, CAEParteAnulNro, CAEParteFchUtilizado, CAEParteId, EmpId, false);
                        if (!resp.isPerrorreturn()) {
                            //dtCAEUtilAnul.updateInt(TACAE.CAEParteUtilizadoAnulado.getCAEParteAnulSincronizado(), 1);
                            CAE.ActualizarCAEUtilAnuladoEnviado(EmpId, CAEId, CAEParteId, CAEParteFchUtilizado, CAEParteAnulNro);
                        } else {
                            ErrMensajeRet += resp.getPerrormessage() + "\r\n";
                        }
                    } catch (RuntimeException ex) {
                        ErrMensajeRet += ex.getMessage() + "\r\n";
                    }
                }
            }
            if (!ErrMensajeRet.trim().equals("")) {
                throw new TAException(ErrMensajeRet, 3611);
            }
            tempSincronizarCAEUtilAnulado = true;
        } catch (TAException TaEx) {
            throw TaEx;
        } catch (RuntimeException ex) {
            throw new TAException(ex.getMessage(), 3614);
        }
        return tempSincronizarCAEUtilAnulado;
    }

    public final boolean SincronizarLogErrores(int EmpId, String EmpRUT, int SucId, int CajaId, int LoteEnvio, String URLWS, int TimeOut) throws TAException, SQLException, Exception {
        boolean tempSincronizarLogErrores = false;
        tempSincronizarLogErrores = false;
        TALogErrores LogErr = null;
        executorWS.setURLServer(URLWS);
        String ErrMensajeRet = "";
        boolean ErrReturn = false;
        ResultSet dtLogErrores = null;
        List<Map<String, Object>> drListLog = new ArrayList<Map<String, Object>>();
        List<Integer> listLogSync = new ArrayList<Integer>();
        try {
            tangible.RefObject<NadMADato> tempRef_NadDato = new tangible.RefObject<NadMADato>(NadDato);
            LogErr = new TALogErrores(tempRef_NadDato);
            NadDato = tempRef_NadDato.argValue;
            dtLogErrores = LogErr.ConsLogErroresPorSincronizar(EmpId, SucId, (short) CajaId, LoteEnvio);
            if (NadDato.getRowCount(dtLogErrores) == 0) {
                dtLogErrores.close();
                return true;
            } else {
                dtLogErrores = LogErr.ConsLogErroresPorSincronizar(EmpId, SucId, (short) CajaId, LoteEnvio);
                drListLog = resultSetToListMapArray(dtLogErrores);
                dtLogErrores.close();
                for (int i = 0; i < drListLog.size(); i++) {
                    try {
                        Date LogErrFecha = Fto.ConvertirStringAFecha(drListLog.get(i).get(TALogErrores.LogErrores.getLogErrFecha()).toString());
                        Date LogErrFchHora = Fto.ConvertirStringAFechaHora(drListLog.get(i).get(TALogErrores.LogErrores.getLogErrFchHora()).toString());
                        String LogErrStackTrace = drListLog.get(i).get(TALogErrores.LogErrores.getLogErrStackTrace()).toString();
                        String LogErrTipoError = drListLog.get(i).get(TALogErrores.LogErrores.getLogErrTipoError()).toString();
                        short LogErrCod = Short.parseShort(drListLog.get(i).get(TALogErrores.LogErrores.getLogErrCod()).toString());
                        String LogErrMsg = drListLog.get(i).get(TALogErrores.LogErrores.getLogErrMsg()).toString();
                        String LogErrSolucion = drListLog.get(i).get(TALogErrores.LogErrores.getLogErrSolucion()).toString();
                        String LogErrUrgencia = drListLog.get(i).get(TALogErrores.LogErrores.getLogErrUrgencia()).toString();

                        WSSincronizacionLogErrores0200ExecuteResponse resp = executorWS.executeLogErrores((short) CajaId, EmpRUT, LogErrCod, LogErrFchHora,
                                LogErrFecha, 0, LogErrMsg, LogErrSolucion, LogErrStackTrace, LogErrTipoError, LogErrUrgencia, SucId);
                        if (!resp.isPerrorreturn()) {
                            listLogSync.add(Integer.parseInt(drListLog.get(i).get(TALogErrores.LogErrores.getLogErrId()).toString()));//puede ser BigDecimal el tipo de dato                            
                        } else {
                            ErrMensajeRet += resp.getPerrormessage() + "\r\n";
                        }

                    } catch (RuntimeException ex) {
                        ErrMensajeRet += ex.getMessage() + "\r\n";
                    }
                }

                for (int i = 0; i < listLogSync.size(); i++) {
                    LogErr.ActualizarLogErrorEnviado(EmpId, SucId, CajaId, listLogSync.get(i));
                }
            }
            if (!ErrMensajeRet.trim().equals("")) {
                throw new TAException(ErrMensajeRet, 3611);
            }
            tempSincronizarLogErrores = true;
        } catch (TAException TaEx) {
            throw TaEx;
        } catch (RuntimeException ex) {
            throw new TAException(ex.getMessage(), 3614);
        }
        return tempSincronizarLogErrores;
    }

    public final boolean EnviarLogErrorWS(int EmpId, String EmpRUT, int SucId, int CajaId, String Mensaje, String URLWS, int TimeOut) throws TAException {

        TALogErrores LogErr = null;
        String ErrMensaje = "";
        boolean ErrReturn = false;
        executorWS.setURLServer(URLWS);
        try {
            LogErr = new TALogErrores(NadDato);
            WakeUpServer(URLWS, TimeOut);
            WSSincronizacionLogErrores0200ExecuteResponse resp = executorWS.executeLogErrores((short) CajaId, EmpRUT, (short) 0, new java.util.Date(),
                    new java.util.Date(), 0, Mensaje, "", "", "LOG", "INS", SucId);
            ErrMensaje = resp.getPerrormessage();
            if (!ErrMensaje.trim().equals("")) {
                throw new TAException(ErrMensaje, 3611);
            }
            ErrReturn = true;
        } catch (TAException TaEx) {
            throw new TAException("Error al enviar log web service: " + TaEx.getMessage(), 3614);
        } catch (RuntimeException ex) {
            throw new TAException("Error al enviar log web service: " + ex.getMessage(), 3614);
        }
        return ErrReturn;
    }

    //Si no logra obtener respuesta del server, tira error
    private void WakeUpServer(String UrlServer, int SegundosTimeOut) throws TAException {
        int Intentos = 0;
        RuntimeException exAux = null;
        executorWS.setURLServer(UrlServer);
        try {
            while (Intentos < 2) {
                try {
                    if (executorWS.executeWakeUpServer()) {
                        return;
                    }
                } catch (RuntimeException ex) {
                    exAux = ex;
                }
                Intentos += 1;
            }
            throw new RuntimeException("Error luego de 2 intentos no logro conectarse - " + exAux.getMessage(), exAux);
        } catch (RuntimeException ex) {
            throw new TAException("Error Iniciando Conexión con Servidor - Url: " + UrlServer + " - TimeOut: " + SegundosTimeOut + "\r\n" + ex.getMessage(), 9999);
        }
    }

    /*public TASincronizarDatos(tangible.RefObject<NadMADato> Dato) {
     NadDato = Dato.argValue;
     Fto.setProveedorConexion(Dato.argValue.getProveedorConexion().getValue());
     }*/
    public TASincronizarDatos(NadMADato Dato) {
        NadDato = Dato;
        Fto.setProveedorConexion(Dato.getProveedorConexion().getValue());
    }

    public final java.util.Date DescargarFechaSistema(String UrlServer, int SegundosTimeOut) throws TAException {
        java.util.Date tempDescargarFechaSistema = new java.util.Date(0);
        try {
            String ErrorMsg = "";
            boolean ErrorReturn = false;
            executorWS.setURLServer(UrlServer);
            tangible.RefObject<Boolean> tempRef_ErrorReturn = new tangible.RefObject<Boolean>(ErrorReturn);
            tangible.RefObject<String> tempRef_ErrorMsg = new tangible.RefObject<String>(ErrorMsg);
            tempDescargarFechaSistema = executorWS.executeDescargarFechaSistema();
            ErrorReturn = tempRef_ErrorReturn.argValue;
            ErrorMsg = tempRef_ErrorMsg.argValue;
        } catch (RuntimeException ex) {
            throw ex;
        }
        return tempDescargarFechaSistema;
    }

    //aux method conversion between resultset and List<Map<String, Object>>
    public List<Map<String, Object>> resultSetToListMapArray(ResultSet rs) throws SQLException {
        List<Map<String, Object>> myList = new ArrayList<Map<String, Object>>();
        if (rs != null) {
            ResultSetMetaData meta = rs.getMetaData();
            while (rs.next()) {
                Map<String, Object> myMap = new HashMap<String, Object>();
                for (int i = 1; i <= meta.getColumnCount(); i++) {
                    String key = meta.getColumnName(i);
                    Object value = rs.getObject(key);
                    myMap.put(key, value);
                }
                myList.add(myMap);
            }
        }
        return myList;
    }
}