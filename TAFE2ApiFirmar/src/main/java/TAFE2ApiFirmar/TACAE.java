package TAFE2ApiFirmar;

import TAFACE2ApiEntidad.TAException;
import XML.XMLFACTURA;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TACAE {

    public static class CAE {

        /**
         * @return the EmpId
         */
        public static String getEmpId() {
            return EmpId;
        }

        /**
         * @param aEmpId the EmpId to set
         */
        public static void setEmpId(String aEmpId) {
            EmpId = aEmpId;
        }

        /**
         * @return the CAEId
         */
        public static String getCAEId() {
            return CAEId;
        }

        /**
         * @param aCAEId the CAEId to set
         */
        public static void setCAEId(String aCAEId) {
            CAEId = aCAEId;
        }

        /**
         * @return the CAENroAutorizacion
         */
        public static String getCAENroAutorizacion() {
            return CAENroAutorizacion;
        }

        /**
         * @param aCAENroAutorizacion the CAENroAutorizacion to set
         */
        public static void setCAENroAutorizacion(String aCAENroAutorizacion) {
            CAENroAutorizacion = aCAENroAutorizacion;
        }

        /**
         * @return the CAETipoCFE
         */
        public static String getCAETipoCFE() {
            return CAETipoCFE;
        }

        /**
         * @param aCAETipoCFE the CAETipoCFE to set
         */
        public static void setCAETipoCFE(String aCAETipoCFE) {
            CAETipoCFE = aCAETipoCFE;
        }

        /**
         * @return the CAESerie
         */
        public static String getCAESerie() {
            return CAESerie;
        }

        /**
         * @param aCAESerie the CAESerie to set
         */
        public static void setCAESerie(String aCAESerie) {
            CAESerie = aCAESerie;
        }

        /**
         * @return the CAENroDesde
         */
        public static String getCAENroDesde() {
            return CAENroDesde;
        }

        /**
         * @param aCAENroDesde the CAENroDesde to set
         */
        public static void setCAENroDesde(String aCAENroDesde) {
            CAENroDesde = aCAENroDesde;
        }

        /**
         * @return the CAENroHasta
         */
        public static String getCAENroHasta() {
            return CAENroHasta;
        }

        /**
         * @param aCAENroHasta the CAENroHasta to set
         */
        public static void setCAENroHasta(String aCAENroHasta) {
            CAENroHasta = aCAENroHasta;
        }

        /**
         * @return the CAEVigencia
         */
        public static String getCAEVigencia() {
            return CAEVigencia;
        }

        /**
         * @param aCAEVigencia the CAEVigencia to set
         */
        public static void setCAEVigencia(String aCAEVigencia) {
            CAEVigencia = aCAEVigencia;
        }

        /**
         * @return the CAEVencimiento
         */
        public static String getCAEVencimiento() {
            return CAEVencimiento;
        }

        /**
         * @param aCAEVencimiento the CAEVencimiento to set
         */
        public static void setCAEVencimiento(String aCAEVencimiento) {
            CAEVencimiento = aCAEVencimiento;
        }

        /**
         * @return the CAEAnulado
         */
        public static String getCAEAnulado() {
            return CAEAnulado;
        }

        /**
         * @param aCAEAnulado the CAEAnulado to set
         */
        public static void setCAEAnulado(String aCAEAnulado) {
            CAEAnulado = aCAEAnulado;
        }

        /**
         * @return the CAEFchHoraSync
         */
        public static String getCAEFchHoraSync() {
            return CAEFchHoraSync;
        }

        /**
         * @param aCAEFchHoraSync the CAEFchHoraSync to set
         */
        public static void setCAEFchHoraSync(String aCAEFchHoraSync) {
            CAEFchHoraSync = aCAEFchHoraSync;
        }

		/**
		 * @return the CAEEspecial
		 */
		public static String getCAEEspecial() {
			return CAEEspecial;
		}

		/**
		 * @param aCAEEspecial the CAEEspecial to set
		 */
		public static void setCAEEspecial(String aCAEEspecial) {
			CAEEspecial = aCAEEspecial;
		}

		/**
		 * @return the CausalTipoCAEEspecial
		 */
		public static String getCAECAusalTipo() {
			return CAECAusalTipo;
		}

		/**
		 * @param aCausalTipoCAEEspecial the CausalTipoCAEEspecial to set
		 */
		public static void setCAECAusalTipo(String aCausalTipoCAEEspecial) {
			CAECAusalTipo = aCausalTipoCAEEspecial;
		}
        private Object x;
        private static String EmpId = "EmpId";
        private static String CAEId = "CAEId";
        private static String CAENroAutorizacion = "CAENroAutorizacion";
        private static String CAETipoCFE = "CAETipoCFE";
        private static String CAESerie = "CAESerie";
        private static String CAENroDesde = "CAENroDesde";
        private static String CAENroHasta = "CAENroHasta";
        private static String CAEVigencia = "CAEVigencia";
        private static String CAEVencimiento = "CAEVencimiento";
        private static String CAEAnulado = "CAEAnulado";
        private static String CAEFchHoraSync = "CAEFchHoraSync";
		private static String CAEEspecial = "CAEEspecial";
		private static String CAECAusalTipo = "CAECAusalTipo";
    }

    public static class CAEParte {

        /**
         * @return the EmpId
         */
        public static String getEmpId() {
            return EmpId;
        }

        /**
         * @param aEmpId the EmpId to set
         */
        public static void setEmpId(String aEmpId) {
            EmpId = aEmpId;
        }

        /**
         * @return the CAEId
         */
        public static String getCAEId() {
            return CAEId;
        }

        /**
         * @param aCAEId the CAEId to set
         */
        public static void setCAEId(String aCAEId) {
            CAEId = aCAEId;
        }

        /**
         * @return the CAEParteId
         */
        public static String getCAEParteId() {
            return CAEParteId;
        }

        /**
         * @param aCAEParteId the CAEParteId to set
         */
        public static void setCAEParteId(String aCAEParteId) {
            CAEParteId = aCAEParteId;
        }

        /**
         * @return the SucId
         */
        public static String getSucId() {
            return SucId;
        }

        /**
         * @param aSucId the SucId to set
         */
        public static void setSucId(String aSucId) {
            SucId = aSucId;
        }

        /**
         * @return the CajaId
         */
        public static String getCajaId() {
            return CajaId;
        }

        /**
         * @param aCajaId the CajaId to set
         */
        public static void setCajaId(String aCajaId) {
            CajaId = aCajaId;
        }

        /**
         * @return the CAEParteNroDesde
         */
        public static String getCAEParteNroDesde() {
            return CAEParteNroDesde;
        }

        /**
         * @param aCAEParteNroDesde the CAEParteNroDesde to set
         */
        public static void setCAEParteNroDesde(String aCAEParteNroDesde) {
            CAEParteNroDesde = aCAEParteNroDesde;
        }

        /**
         * @return the CAEParteNroHasta
         */
        public static String getCAEParteNroHasta() {
            return CAEParteNroHasta;
        }

        /**
         * @param aCAEParteNroHasta the CAEParteNroHasta to set
         */
        public static void setCAEParteNroHasta(String aCAEParteNroHasta) {
            CAEParteNroHasta = aCAEParteNroHasta;
        }

        /**
         * @return the CAEParteFchHoraSync
         */
        public static String getCAEParteFchHoraSync() {
            return CAEParteFchHoraSync;
        }

        /**
         * @param aCAEParteFchHoraSync the CAEParteFchHoraSync to set
         */
        public static void setCAEParteFchHoraSync(String aCAEParteFchHoraSync) {
            CAEParteFchHoraSync = aCAEParteFchHoraSync;
        }

        /**
         * @return the CAEParteUltAsignado
         */
        public static String getCAEParteUltAsignado() {
            return CAEParteUltAsignado;
        }

        /**
         * @param aCAEParteUltAsignado the CAEParteUltAsignado to set
         */
        public static void setCAEParteUltAsignado(String aCAEParteUltAsignado) {
            CAEParteUltAsignado = aCAEParteUltAsignado;
        }

        /**
         * @return the CAEParteAnulado
         */
        public static String getCAEParteAnulado() {
            return CAEParteAnulado;
        }

        /**
         * @param aCAEParteAnulado the CAEParteAnulado to set
         */
        public static void setCAEParteAnulado(String aCAEParteAnulado) {
            CAEParteAnulado = aCAEParteAnulado;
        }

        /**
         * @return the CAEParteAnuladoSync
         */
        public static String getCAEParteAnuladoSync() {
            return CAEParteAnuladoSync;
        }

        /**
         * @param aCAEParteAnuladoSync the CAEParteAnuladoSync to set
         */
        public static void setCAEParteAnuladoSync(String aCAEParteAnuladoSync) {
            CAEParteAnuladoSync = aCAEParteAnuladoSync;
        }

        /**
         * @return the CAEParteFchHoraUltSync
         */
        public static String getCAEParteFchHoraUltSync() {
            return CAEParteFchHoraUltSync;
        }

        /**
         * @param aCAEParteFchHoraUltSync the CAEParteFchHoraUltSync to set
         */
        public static void setCAEParteFchHoraUltSync(String aCAEParteFchHoraUltSync) {
            CAEParteFchHoraUltSync = aCAEParteFchHoraUltSync;
        }

        /**
         * @return the CAEParteRowGUID
         */
        public static String getCAEParteRowGUID() {
            return CAEParteRowGUID;
        }

        /**
         * @param aCAEParteRowGUID the CAEParteRowGUID to set
         */
        public static void setCAEParteRowGUID(String aCAEParteRowGUID) {
            CAEParteRowGUID = aCAEParteRowGUID;
        }
        private Object x;
        private static String EmpId = "EmpId";
        private static String CAEId = "CAEId";
        private static String CAEParteId = "CAEParteId";
        private static String SucId = "SucId";
        private static String CajaId = "CajaId";
        private static String CAEParteNroDesde = "CAEParteNroDesde";
        private static String CAEParteNroHasta = "CAEParteNroHasta";
        private static String CAEParteFchHoraSync = "CAEParteFchHoraSync";
        private static String CAEParteUltAsignado = "CAEParteUltAsignado";
        private static String CAEParteAnulado = "CAEParteAnulado";
        private static String CAEParteAnuladoSync = "CAEParteAnuladoSync";
        private static String CAEParteFchHoraUltSync = "CAEParteFchHoraUltSync";
        private static String CAEParteRowGUID = "CAEParteRowGUID";
    }

    public static class CAEParteUtilizado {

        /**
         * @return the EmpId
         */
        public static String getEmpId() {
            return EmpId;
        }

        /**
         * @param aEmpId the EmpId to set
         */
        public static void setEmpId(String aEmpId) {
            EmpId = aEmpId;
        }

        /**
         * @return the CAEId
         */
        public static String getCAEId() {
            return CAEId;
        }

        /**
         * @param aCAEId the CAEId to set
         */
        public static void setCAEId(String aCAEId) {
            CAEId = aCAEId;
        }

        /**
         * @return the CAEParteId
         */
        public static String getCAEParteId() {
            return CAEParteId;
        }

        /**
         * @param aCAEParteId the CAEParteId to set
         */
        public static void setCAEParteId(String aCAEParteId) {
            CAEParteId = aCAEParteId;
        }

        /**
         * @return the CAEParteFchUtilizado
         */
        public static String getCAEParteFchUtilizado() {
            return CAEParteFchUtilizado;
        }

        /**
         * @param aCAEParteFchUtilizado the CAEParteFchUtilizado to set
         */
        public static void setCAEParteFchUtilizado(String aCAEParteFchUtilizado) {
            CAEParteFchUtilizado = aCAEParteFchUtilizado;
        }

        /**
         * @return the CAEParteUtilNroDesde
         */
        public static String getCAEParteUtilNroDesde() {
            return CAEParteUtilNroDesde;
        }

        /**
         * @param aCAEParteUtilNroDesde the CAEParteUtilNroDesde to set
         */
        public static void setCAEParteUtilNroDesde(String aCAEParteUtilNroDesde) {
            CAEParteUtilNroDesde = aCAEParteUtilNroDesde;
        }

        /**
         * @return the CAEParteUtilNroHasta
         */
        public static String getCAEParteUtilNroHasta() {
            return CAEParteUtilNroHasta;
        }

        /**
         * @param aCAEParteUtilNroHasta the CAEParteUtilNroHasta to set
         */
        public static void setCAEParteUtilNroHasta(String aCAEParteUtilNroHasta) {
            CAEParteUtilNroHasta = aCAEParteUtilNroHasta;
        }

        /**
         * @return the CAEParteUtilSincronizado
         */
        public static String getCAEParteUtilSincronizado() {
            return CAEParteUtilSincronizado;
        }

        /**
         * @param aCAEParteUtilSincronizado the CAEParteUtilSincronizado to set
         */
        public static void setCAEParteUtilSincronizado(String aCAEParteUtilSincronizado) {
            CAEParteUtilSincronizado = aCAEParteUtilSincronizado;
        }
        private Object x;
        private static String EmpId = "EmpId";
        private static String CAEId = "CAEId";
        private static String CAEParteId = "CAEParteId";
        private static String CAEParteFchUtilizado = "CAEParteFchUtilizado";
        private static String CAEParteUtilNroDesde = "CAEParteUtilNroDesde";
        private static String CAEParteUtilNroHasta = "CAEParteUtilNroHasta";
        private static String CAEParteUtilSincronizado = "CAEParteUtilSincronizado";
    }

    public static class CAEParteUtilizadoAnulado {

        /**
         * @return the EmpId
         */
        public static String getEmpId() {
            return EmpId;
        }

        /**
         * @param aEmpId the EmpId to set
         */
        public static void setEmpId(String aEmpId) {
            EmpId = aEmpId;
        }

        /**
         * @return the CAEId
         */
        public static String getCAEId() {
            return CAEId;
        }

        /**
         * @param aCAEId the CAEId to set
         */
        public static void setCAEId(String aCAEId) {
            CAEId = aCAEId;
        }

        /**
         * @return the CAEParteId
         */
        public static String getCAEParteId() {
            return CAEParteId;
        }

        /**
         * @param aCAEParteId the CAEParteId to set
         */
        public static void setCAEParteId(String aCAEParteId) {
            CAEParteId = aCAEParteId;
        }

        /**
         * @return the CAEParteFchUtilizado
         */
        public static String getCAEParteFchUtilizado() {
            return CAEParteFchUtilizado;
        }

        /**
         * @param aCAEParteFchUtilizado the CAEParteFchUtilizado to set
         */
        public static void setCAEParteFchUtilizado(String aCAEParteFchUtilizado) {
            CAEParteFchUtilizado = aCAEParteFchUtilizado;
        }

        /**
         * @return the CAEParteAnulNro
         */
        public static String getCAEParteAnulNro() {
            return CAEParteAnulNro;
        }

        /**
         * @param aCAEParteAnulNro the CAEParteAnulNro to set
         */
        public static void setCAEParteAnulNro(String aCAEParteAnulNro) {
            CAEParteAnulNro = aCAEParteAnulNro;
        }

        /**
         * @return the CAEParteAnulSincronizado
         */
        public static String getCAEParteAnulSincronizado() {
            return CAEParteAnulSincronizado;
        }

        /**
         * @param aCAEParteAnulSincronizado the CAEParteAnulSincronizado to set
         */
        public static void setCAEParteAnulSincronizado(String aCAEParteAnulSincronizado) {
            CAEParteAnulSincronizado = aCAEParteAnulSincronizado;
        }

        /**
         * @return the CAEParteAnulRestaurar
         */
        public static String getCAEParteAnulRestaurar() {
            return CAEParteAnulRestaurar;
        }

        /**
         * @param aCAEParteAnulRestaurar the CAEParteAnulRestaurar to set
         */
        public static void setCAEParteAnulRestaurar(String aCAEParteAnulRestaurar) {
            CAEParteAnulRestaurar = aCAEParteAnulRestaurar;
        }
        private Object x;
        private static String EmpId = "EmpId";
        private static String CAEId = "CAEId";
        private static String CAEParteId = "CAEParteId";
        private static String CAEParteFchUtilizado = "CAEParteFchUtilizado";
        private static String CAEParteAnulNro = "CAEParteAnulNro";
        private static String CAEParteAnulSincronizado = "CAEParteAnulSincronizado";
        private static String CAEParteAnulRestaurar = "CAEParteAnulRestaurar";
    }

    public static class CAEParteReservado {

        /**
         * @return the EmpId
         */
        public static String getEmpId() {
            return EmpId;
        }

        /**
         * @param aEmpId the EmpId to set
         */
        public static void setEmpId(String aEmpId) {
            EmpId = aEmpId;
        }

        /**
         * @return the CAEId
         */
        public static String getCAEId() {
            return CAEId;
        }

        /**
         * @param aCAEId the CAEId to set
         */
        public static void setCAEId(String aCAEId) {
            CAEId = aCAEId;
        }

        /**
         * @return the CAEParteId
         */
        public static String getCAEParteId() {
            return CAEParteId;
        }

        /**
         * @param aCAEParteId the CAEParteId to set
         */
        public static void setCAEParteId(String aCAEParteId) {
            CAEParteId = aCAEParteId;
        }

        /**
         * @return the CAEParteResNroCAE
         */
        public static String getCAEParteResNroCAE() {
            return CAEParteResNroCAE;
        }

        /**
         * @param aCAEParteResNroCAE the CAEParteResNroCAE to set
         */
        public static void setCAEParteResNroCAE(String aCAEParteResNroCAE) {
            CAEParteResNroCAE = aCAEParteResNroCAE;
        }

        /**
         * @return the CAEParteResFchHora
         */
        public static String getCAEParteResFchHora() {
            return CAEParteResFchHora;
        }

        /**
         * @param aCAEParteResFchHora the CAEParteResFchHora to set
         */
        public static void setCAEParteResFchHora(String aCAEParteResFchHora) {
            CAEParteResFchHora = aCAEParteResFchHora;
        }

        /**
         * @return the CAEParteResCaduco
         */
        public static String getCAEParteResCaduco() {
            return CAEParteResCaduco;
        }

        /**
         * @param aCAEParteResCaduco the CAEParteResCaduco to set
         */
        public static void setCAEParteResCaduco(String aCAEParteResCaduco) {
            CAEParteResCaduco = aCAEParteResCaduco;
        }

        /**
         * @return the CAEParteResEstado
         */
        public static String getCAEParteResEstado() {
            return CAEParteResEstado;
        }

        /**
         * @param aCAEParteResEstado the CAEParteResEstado to set
         */
        public static void setCAEParteResEstado(String aCAEParteResEstado) {
            CAEParteResEstado = aCAEParteResEstado;
        }
        private Object x;
        private static String EmpId = "EmpId";
        private static String CAEId = "CAEId";
        private static String CAEParteId = "CAEParteId";
        private static String CAEParteResNroCAE = "CAEParteResNroCAE";
        private static String CAEParteResFchHora = "CAEParteResFchHora";
        private static String CAEParteResCaduco = "CAEParteResCaduco";
        private static String CAEParteResEstado = "CAEParteResEstado";
    }
    private String sSQL;
    private NadMADato NadDato;
    private NadFormato Fto = new NadFormato();
    private AssemblyQuery assembly = new AssemblyQuery();

    public ResultSet ConsCAE(java.math.BigDecimal EmpId, short CAETCFE, java.util.Date CAEFA, java.util.Date CAEFVD, int CAEUAS) throws SQLException {
        ResultSet dt = null;
        sSQL = "Select * From CAE Where EmpId = " + EmpId + " And CAETipoCFE = " + CAETCFE + " And CAEFA <= " + Fto.FechaSQL(CAEFA) + " And CAEVencimiento > " + Fto.FechaSQL(CAEFVD) + " And CAEUAS < " + CAEUAS + " And CAEProxyRepitiendo = 0";
        dt = NadDato.GetTable(sSQL);
        return dt;
    }

    public ResultSet ConsCAEUtilPorSincronizar(int EmpId) throws SQLException {
        ResultSet dt = null;
        sSQL = "Select * From CAEParteUtilizado Where EmpId = " + EmpId + " And CAEParteUtilSincronizado = 0";
        dt = NadDato.GetTable(sSQL);
        return dt;
    }

    public ResultSet ConsCAEUtilAnulPorSincronizar(int EmpId) throws SQLException {
        ResultSet dt = null;
        sSQL = "Select * From CAEParteUtilizadoAnulado Where EmpId = " + EmpId + " And CAEParteAnulSincronizado = 0";
        dt = NadDato.GetTable(sSQL);
        return dt;
    }

    public ResultSet ConsCAEParteAnuladoPorSincronizar(int EmpId, int SucId, int CajId) throws SQLException {
        ResultSet dt = null;
        sSQL = "Select * From CAEParte Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajId + " And CAEParteAnuladoSync = 0 And CAEParteAnulado = 1";
        dt = NadDato.GetTable(sSQL);
        return dt;
    }

    public final boolean ActualizarCAEUtilizadoEnviado(int EmpId, int CAEId, int CAEParteId, Date CAEParteFchUtilizado, int CAEParteUtilNroHasta/*ResultSet dr*/) throws Exception {
        boolean tempActualizarCAEUtilizadoEnviado = false;
        sSQL = "Update CAEParteUtilizado Set CAEParteUtilSincronizado = 1 Where EmpId = " + EmpId + " And CAEId = " + CAEId + " And CAEParteId = " + CAEParteId + " And CAEParteFchUtilizado = " + Fto.FechaSQL(CAEParteFchUtilizado) + " And CAEParteUtilNroHasta = " + CAEParteUtilNroHasta;
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

        try {
            sttment.execute();
            tempActualizarCAEUtilizadoEnviado = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al actualizar CAEUtilizadoEnviado. " + ex.getMessage());
        } finally {
            NadDato.FinTrans();
        }
        return tempActualizarCAEUtilizadoEnviado;
    }

    public final boolean ActualizarCAEParteAnulado(int EmpId, int CAEId, int CAEParteId) throws Exception {
        boolean tempActualizarCAEParteAnulado = false;
        sSQL = "Update CAEParte Set CAEParteAnuladoSync = 1 Where EmpId = " + EmpId + " And CAEId = " + CAEId + " And CAEParteId = " + CAEParteId;
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

        try {
            sttment.execute();
            tempActualizarCAEParteAnulado = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al actualizar CAEParteAnulado.");
        } finally {
            NadDato.FinTrans();
        }
        return tempActualizarCAEParteAnulado;
    }

    public final boolean ActualizarCAEUtilAnuladoEnviado(int EmpId, int CAEId, int CAEParteId, Date CAEParteFchUtilizado, int CAEParteAnulNro) throws Exception {
        boolean tempActualizarCAEUtilAnuladoEnviado = false;
        sSQL = "Update CAEParteUtilizadoAnulado Set CAEParteAnulSincronizado = 1 Where EmpId = " + EmpId + " And CAEId = " + CAEId + " And CAEParteId = " + CAEParteId + " And CAEParteFchUtilizado = " + Fto.FechaSQL(CAEParteFchUtilizado) + " And CAEParteAnulNro = " + Fto.CTLng(CAEParteAnulNro);
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

        try {
            sttment.execute();
            tempActualizarCAEUtilAnuladoEnviado = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al actualizar CAEUtilAnuladoEnviado.");
        } finally {
            NadDato.FinTrans();
        }
        return tempActualizarCAEUtilAnuladoEnviado;
    }

    public final void AnularCAE(int EmpId, int CAEId, int CAEParteId, java.util.Date CAEParteFchUtilizado, long CAEParteAnulNro) throws Exception {
        sSQL = "Insert Into CAEParteUtilizadoAnulado (EmpId,CAEId,CAEParteId,CAEParteFchUtilizado,CAEParteAnulNro,CAEParteAnulSincronizado,CAEParteAnulRestaurar) " + "Values (" + EmpId + "," + CAEId + "," + CAEParteId + "," + Fto.FechaSQL(CAEParteFchUtilizado) + "," + CAEParteAnulNro + ",0,0)";
        NadDato.Ejecutar(sSQL);
    }

    public final boolean RestaurarAnulacionCAE(int EmpId, int CAEId, int CAEParteId, java.util.Date CAEParteFchUtilizado, long CAEParteAnulNro) throws Exception {
        boolean tempRestaurarAnulacionCAE = false;
        sSQL = "Update CAEParteUtilizadoAnulado Set CAEParteAnulSincronizado = 0,CAEParteAnulRestaurar = 1 Where EmpId = " + EmpId + " And CAEId = " + CAEId + " And CAEParteId = " + CAEParteId + " And CAEParteFchUtilizado = " + Fto.FechaSQL(CAEParteFchUtilizado) + " And CAEParteAnulNro = " + CAEParteAnulNro;
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

        try {
            sttment.execute();
            tempRestaurarAnulacionCAE = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al restaurar anulacion CAE.");
        } finally {
            NadDato.FinTrans();
        }
        return tempRestaurarAnulacionCAE;
    }

    //NO SE INVOCA ESTA FUNCIONALIDAD EN LA API
    public final boolean CargarDatosCae(java.math.BigDecimal EmpId, short FacGenTipoCFE, String FacGenSerie, java.util.Date FacGenFecha, int FacGenNro, tangible.RefObject<java.util.Date> CAEFA, tangible.RefObject<java.util.Date> CAEFVD, tangible.RefObject<Integer> CAEDNro, tangible.RefObject<Integer> CAEHNro, tangible.RefObject<java.math.BigDecimal> CAENA, tangible.RefObject<java.math.BigDecimal> CAERucE) throws SQLException {
        boolean tempCargarDatosCae = false;
        return tempCargarDatosCae;
    }

    public final boolean TomarCAE(int EmpId, int SucId, int CajId, short CAETipoCFE, java.util.Date FechaGenerado, short DiasAntelacionCAEVenc, byte PorcUscoAntelacionCAE, tangible.RefObject<Map<String, Object>> drCAE, tangible.RefObject<Map<String, Object>> drCAEParte, tangible.RefObject<String> WarningMsg) throws SQLException, Exception {
        boolean tempTomarCAE = false;
        ResultSet dt = null;
        WarningMsg.argValue = "";
        //Variable usada para mostrar el Tipo de CFE en el warningmsg
        XMLFACTURA.enumTipoDeComprobanteCFE NomTipoCFE = XMLFACTURA.enumTipoDeComprobanteCFE.forValue(CAETipoCFE);

        try {
            sSQL = "Select c.* From CAE c Inner join CAEParte cp on cp.CAEId = c.CAEId Where c.EmpId = " + EmpId + " And cp.SucId = " + SucId + " And cp.CajaId = " + CajId + " And c.CAETipoCFE = " + CAETipoCFE + " And c.CAEVigencia <= " + Fto.FechaSQL(FechaGenerado) + " And c.CAEVencimiento > " + Fto.FechaSQL(FechaGenerado) + " And c.CAEAnulado = 0  And cp.CAEParteNroHasta > cp.CAEParteUltAsignado Order by CAEVencimiento,CAENroAutorizacion";
            dt = NadDato.GetTable(sSQL);

            if (NadDato.getRowCount(dt) > 0) {

                dt = NadDato.GetTable(sSQL);
                drCAE.argValue = resultSetToArrayList(dt);

                sSQL = "Select * From CAEParte Where EmpId = " + EmpId + " And CAEId = " + Integer.parseInt(drCAE.argValue.get(CAE.CAEId).toString()) + " And SucId = " + SucId + " And CajaId = " + CajId + " And CAEParteNroHasta > CAEParteUltAsignado Order by CAEparteNroDesde";

                dt = NadDato.GetTable(sSQL);
                if (NadDato.getRowCount(dt) > 0) {

                    dt = NadDato.GetTable(sSQL);
                    drCAEParte.argValue = resultSetToArrayList(dt);
                    if (Integer.parseInt(drCAEParte.argValue.get(CAEParte.CAEParteUltAsignado).toString()) == 0) { //|| String.valueOf(drCAEParte.argValue.getInt(CAEParte.CAEParteUltAsignado)).trim().equals(0)
                        drCAEParte.argValue.put(CAEParte.CAEParteUltAsignado, drCAEParte.argValue.get(CAEParte.CAEParteNroDesde));
                    } else {
                        drCAEParte.argValue.put(CAEParte.CAEParteUltAsignado, Fto.CTInt(Integer.parseInt(drCAEParte.argValue.get(CAEParte.CAEParteUltAsignado).toString()) + 1));
                        java.math.BigDecimal PorcUso = CalcularPortcentajeUsoCAEParte(drCAEParte.argValue);
                        if (PorcUso.compareTo(new BigDecimal(PorcUscoAntelacionCAE)) >= 0) {
                            if (!(HayCAEParteDisponible(EmpId, SucId, CajId, FechaGenerado, CAETipoCFE, DiasAntelacionCAEVenc, PorcUscoAntelacionCAE))) {
                                if (!WarningMsg.argValue.trim().equals("")) {
                                    WarningMsg.argValue += "\r\n";
								}
                                    WarningMsg.argValue += "***ATENCIÓN***: Solicite nuevas asignaciones de CAE, se están agotando los CAE disponibles de Factura Electrónica de tipo " + NomTipoCFE.toString() + ", una vez terminados no se podrá continuar con la facturación.";
                            }
                        }
                    }

                    if (EstaPorVencerCAE(drCAE.argValue, FechaGenerado, DiasAntelacionCAEVenc)) {
                        if (!(HayCAEParteDisponible(EmpId, SucId, CajId, FechaGenerado, CAETipoCFE, DiasAntelacionCAEVenc, PorcUscoAntelacionCAE))) {
                            if (!WarningMsg.argValue.trim().equals("")) {
                                WarningMsg.argValue += "\r\n";
							}
                                WarningMsg.argValue += "***ATENCIÓN***: El CAE de : " + NomTipoCFE.toString() + " que está utilizando esta próximo a caducar, una vez vencido no se podrá continuar con la facturación, solicite la asignación de uno nuevo a esta sucursal.";
                        }
                    }
                    sSQL = "Update CAEParte Set CAEParteUltAsignado = " + Integer.parseInt(drCAEParte.argValue.get(CAEParte.CAEParteUltAsignado).toString()) + " Where EmpId = " + EmpId + " And CAEId = " + Integer.parseInt(drCAEParte.argValue.get(CAEParte.CAEId).toString()) + " And CAEParteId = " + Integer.parseInt(drCAEParte.argValue.get(CAEParte.CAEParteId).toString());

                    NadDato.IniTrans();
                    PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

                    try {
                        sttment.execute();
                    } catch (SQLException ex) {
                        throw new TAException("Error durante la ejecucion de la query: " + ex.getMessage());
                    } finally {
                        NadDato.FinTrans();
                    }

                    sSQL = "Select * From CAEParteUtilizado Where EmpId = " + EmpId + " And CAEId = " + Integer.parseInt(drCAEParte.argValue.get(CAEParte.CAEId).toString()) + " And CAEParteId = " + Integer.parseInt(drCAEParte.argValue.get(CAEParte.CAEParteId).toString()) + " And CAEParteFchUtilizado = " + Fto.FechaSQL(FechaGenerado);
                    dt = NadDato.GetTable(sSQL);

                    if (NadDato.getRowCount(dt) > 0) {
                        sSQL = "Update CAEParteUtilizado Set CAEParteUtilNroHasta = " + Integer.parseInt(drCAEParte.argValue.get(CAEParte.CAEParteUltAsignado).toString()) + ",CAEParteUtilSincronizado=0 Where EmpId = " + EmpId + " And CAEId = " + Integer.parseInt(drCAEParte.argValue.get(CAEParte.CAEId).toString()) + " And CAEParteId = " + Integer.parseInt(drCAEParte.argValue.get(CAEParte.CAEParteId).toString()) + " And CAEParteFchUtilizado = " + Fto.FechaSQL(FechaGenerado);
                    } else {
                        Map<String, Object> drCAEUtilizado = CrearCAEParteUtilizado();
                        drCAEUtilizado.put(CAEParteUtilizado.CAEId, Integer.parseInt(drCAEParte.argValue.get(CAEParte.CAEId).toString()));
                        drCAEUtilizado.put(CAEParteUtilizado.CAEParteFchUtilizado, Fto.FechaSQL(FechaGenerado));
                        drCAEUtilizado.put(CAEParteUtilizado.CAEParteId, Integer.parseInt(drCAEParte.argValue.get(CAEParte.CAEParteId).toString()));
                        drCAEUtilizado.put(CAEParteUtilizado.CAEParteUtilNroDesde, Integer.parseInt(drCAEParte.argValue.get(CAEParte.CAEParteUltAsignado).toString()));
                        drCAEUtilizado.put(CAEParteUtilizado.CAEParteUtilNroHasta, Integer.parseInt(drCAEParte.argValue.get(CAEParte.CAEParteUltAsignado).toString()));
                        drCAEUtilizado.put(CAEParteUtilizado.EmpId, Integer.parseInt(drCAEParte.argValue.get(CAEParte.EmpId).toString()));
                        drCAEUtilizado.put(CAEParteUtilizado.CAEParteUtilSincronizado, 0);
                        sSQL = assembly.AssemblyCAEParteUtilizadoInsert(drCAEUtilizado);
                    }
                    NadDato.Conectar();
                    NadDato.IniTrans();
                    PreparedStatement sttment1 = NadDato.GetConnectionLite().prepareStatement(sSQL);
                    try {
                        sttment1.execute();
                        tempTomarCAE = true;
                    } catch (SQLException ex) {
                        throw new TAException("Error durante la ejecucion de la query: " + ex.getMessage());
                    } finally {
                        NadDato.FinTrans();
                    }
                } else {
                    throw new TAException(getNombreMetodo() + " - No se encuentra CAEParte disponible. " + "\r\n" + "Emp: " + EmpId + " - Suc: " + SucId + " - Caj: " + CajId + " - TipoCte: " + CAETipoCFE);
                }
            } else {
                //No se entontro nignun CAE Disponible
                throw new TAException(getNombreMetodo() + " - No se encuentra CAE disponible. " + "\r\n" + "Emp: " + EmpId + " - Suc: " + SucId + " - Caj: " + CajId + " - TipoCte: " + CAETipoCFE);
            }

        } catch (RuntimeException ex) {
            throw new TAException("Error al tomar CAE. " + ex.getMessage());
        }
        return tempTomarCAE;
    }

    public final ResultSet ConsCAEParte(int EmpId, int SucId, int CajId) throws SQLException {
        ResultSet dt = null;
        sSQL = "Select * From CAEParte Where EmpId = " + EmpId + " And SucId=" + SucId + " And CajaId=" + CajId;
        dt = NadDato.GetTable(sSQL);
        return dt;
    }

    public final boolean GrabarCAEAnulado(int EmpId, int CAEId, int CAEParteId, int ParteAnulNro, java.util.Date FechaUtilizado) throws Exception {
        boolean tempGrabarCAEAnulado = false;

        tempGrabarCAEAnulado = false;
        try {
            Map<String, Object> drCAEAnulado = CrearCAEParteUtilizadoAnulado();
            drCAEAnulado.put(CAEParteUtilizadoAnulado.EmpId, EmpId);
            drCAEAnulado.put(CAEParteUtilizadoAnulado.CAEId, CAEId);
            drCAEAnulado.put(CAEParteUtilizadoAnulado.CAEParteAnulNro, ParteAnulNro);
            drCAEAnulado.put(CAEParteUtilizadoAnulado.CAEParteFchUtilizado, Fto.FechaSQL(FechaUtilizado));
            drCAEAnulado.put(CAEParteUtilizadoAnulado.CAEParteAnulSincronizado, 0);
            drCAEAnulado.put(CAEParteUtilizadoAnulado.CAEParteId, CAEParteId);
            sSQL = assembly.AssemblyCAEParteUtilizadoAnuladoInsert(drCAEAnulado);
            NadDato.Conectar();
            NadDato.IniTrans();
            PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

            try {
                sttment.execute();
                tempGrabarCAEAnulado = true;
            } catch (SQLException ex) {
                throw new TAFACE2ApiEntidad.TAException("Error al grabar CAEAnulado. " + ex.getMessage());
            } finally {
                NadDato.FinTrans();
            }
            tempGrabarCAEAnulado = true;
        } catch (RuntimeException ex) {
            throw new TAException("Error al grabar CAEAnulado. " + ex.getMessage());
        }
        return tempGrabarCAEAnulado;
    }

    public final ResultSet ConsCAEParte(int EmpId) throws SQLException {
        ResultSet dt = null;
        sSQL = "Select * From CAEParte Where EmpId = " + EmpId;
        dt = NadDato.GetTable(sSQL);
        return dt;
    }
    
    public final List<Map<String, Object>> ConsListMapCAEParte(int EmpId) throws SQLException {
        List<Map<String, Object>> mapListCAEParte = new ArrayList<Map<String, Object>>();
        ResultSet dt = null;
        sSQL = "Select * From CAEParte Where EmpId = " + EmpId;
        dt = NadDato.GetTable(sSQL);
        mapListCAEParte = resultSetToArrayListFull(dt);
        return mapListCAEParte;
    }

    public final boolean EliminarCAEParte(int EmpId, int CAEId, int CAEParteId) throws Exception {
        boolean tempEliminarCAEParte = false;
        sSQL = "Delete From CAEParte Where EmpId = " + EmpId + " And CAEId = " + CAEId + " And CAEParteId = " + CAEParteId;
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

        try {
            sttment.execute();
            tempEliminarCAEParte = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al eliminar CAEParte. " + ex.getMessage());
        } finally {
            NadDato.FinTrans();
        }
        return tempEliminarCAEParte;
    }

    public final boolean EliminarCAE(int EmpId, int CAEId) throws Exception {
        boolean tempEliminarCAE = false;
        sSQL = "Delete From CAE Where EmpId = " + EmpId + " And CAEId = " + CAEId;
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

        try {
            sttment.execute();
            tempEliminarCAE = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al eliminar CAE. " + ex.getMessage());
        } finally {
            NadDato.FinTrans();
        }
        return tempEliminarCAE;
    }

    public final boolean GrabarCAE(Map<String, Object> drCAE) throws SQLException, Exception {
        boolean tempGrabarCAE = false;
        sSQL = "Select * From CAE Where EmpId = " + Integer.parseInt(drCAE.get(CAE.EmpId).toString()) + " And CAEId = " + Integer.parseInt(drCAE.get(CAE.CAEId).toString());

        if (NadDato.getRowCount(NadDato.GetTable(sSQL)) == 0) {
            sSQL = assembly.AssemblyCAEInsert(drCAE);
        } else {
            sSQL = assembly.AssemblyCAEUpdate(drCAE) + " Where EmpId = " + Integer.parseInt(drCAE.get(CAE.EmpId).toString()) + " And CAEId = " + Integer.parseInt(drCAE.get(CAE.CAEId).toString());
        }
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

        try {
            sttment.execute();
            tempGrabarCAE = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al grabar CAE. " + ex.getMessage());
        } finally {
            NadDato.FinTrans();
        }
        return tempGrabarCAE;
    }

    public final boolean GrabarCAEParte(Map<String, Object> drCAEParte) throws SQLException, Exception {
        boolean tempGrabarCAEParte = false;
        sSQL = "Select * From CAEParte Where EmpId = " + Integer.parseInt(drCAEParte.get(CAEParte.EmpId).toString()) + " And CAEId = " + Integer.parseInt(drCAEParte.get(CAEParte.CAEId).toString()) + " And CAEParteId = " + Integer.parseInt(drCAEParte.get(CAEParte.CAEParteId).toString());

        if (NadDato.getRowCount(NadDato.GetTable(sSQL)) == 0) {
            sSQL = assembly.AssemblyCAEParteInsert(drCAEParte);
        } else {
            sSQL = assembly.AssemblyCAEParteUpdate(drCAEParte) + " Where EmpId = " + Integer.parseInt(drCAEParte.get(CAEParte.EmpId).toString()) + " And CAEId = " + Integer.parseInt(drCAEParte.get(CAEParte.CAEId).toString()) + " And CAEParteId = " + Integer.parseInt(drCAEParte.get(CAEParte.CAEParteId).toString());;
        }
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

        try {
            sttment.execute();
            tempGrabarCAEParte = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al grabar CAEParte. " + ex.getMessage());
        } finally {
            NadDato.FinTrans();
        }
        return tempGrabarCAEParte;
    }

	private java.math.BigDecimal CalcularPortcentajeUsoCAEParte(Map<String, Object> drCAEParte) throws SQLException {
		int parte = (Integer.parseInt(drCAEParte.get(CAEParte.CAEParteUltAsignado).toString()) - (Integer.parseInt(drCAEParte.get(CAEParte.CAEParteNroDesde).toString()) - 1));
		int total = (Integer.parseInt(drCAEParte.get(CAEParte.CAEParteNroHasta).toString()) - (Integer.parseInt(drCAEParte.get(CAEParte.CAEParteNroDesde).toString()) - 1));
		return new BigDecimal((100 * parte) / total);
	}

    private boolean EstaPorVencerCAE(Map<String, Object> drCAE, java.util.Date FechaGenerado, int DiasAntelacionCAEVenc) throws SQLException, TAException {
        java.util.Date FchGen = new Date(FechaGenerado.getDate());
        boolean result = false;
        //Sumo +1 a los dias de antelacion vencimiento ya que se debe dejar de utilizar 1 dia antes del vencimiento 
        FchGen.setDate(FchGen.getDate() + DiasAntelacionCAEVenc + 1);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date CAEVencimiento = formatter.parse(drCAE.get(CAE.CAEVencimiento).toString());
            if (CAEVencimiento.getDate() <= FchGen.getDate()) {
                Date CAEVigencia = formatter.parse(drCAE.get(CAE.CAEVigencia).toString());
                if (CAEVigencia.getDate() <= FechaGenerado.getDate()) {
                    result = true;
                } else {
                    result = false;
                }
            }
        } catch (ParseException ex) {
            throw new TAException("Error al determinar la fecha de vencimiento del CAE. " + ex.getMessage());
        }
        return result;
    }

    private boolean HayCAEParteDisponible(int EmpId, int SucId, int CajaId, java.util.Date FechaGenerado, int TipoCFE, short DiasAntelacionCAEVenc, byte PorcUscoAntelacionCAE) throws SQLException, TAException {
		ResultSet dtCAE_CAEParte = null;
		java.util.Date FchGen = new Date(FechaGenerado.getDate());
		FchGen.setDate(FchGen.getDate() + DiasAntelacionCAEVenc);
		double asignados = 0;
		double restantes = 0;

		String sSql = "Select SUM(CAEParteNrohasta -  CAEParteNrodesde + 1) as Asignado, SUM((CAEParteNrohasta -  CAEParteNrodesde) - (CAEParteUltAsignado - CAEParteNrodesde - 1)) "
				+ "as Resto From CAE c Inner Join CAEParte cp on c.CAEId = cp.CAEId Where cp.EmpId = " + EmpId + "  And c.CAETipoCFE = " + TipoCFE + " And c.CAEVigencia <= " + Fto.FechaSQL(FechaGenerado) + "  And "
				+ "c.CAEVencimiento > " + Fto.FechaSQL(FchGen) + " And c.CAEAnulado = 0 And cp.SucId = " + SucId + " And cp.CajaId = " + CajaId + " And cp.CAEParteNroHasta > cp.CAEParteUltAsignado And "
				+ "CAEParteUltAsignado <> 0";

		dtCAE_CAEParte = NadDato.GetTable(sSql);
		if (NadDato.getRowCount(dtCAE_CAEParte) > 0) {
			dtCAE_CAEParte = NadDato.GetTable(sSql);
			asignados += dtCAE_CAEParte.getDouble("Asignado");
			restantes += dtCAE_CAEParte.getDouble("Resto");
		}

		sSql = "Select SUM(cp.CAEParteNrohasta -  cp.CAEParteNrodesde + 1) as Asignado, SUM((cp.CAEParteNrohasta -  cp.CAEParteNrodesde) - (cp.CAEParteUltAsignado - 1)) "
				+ "as Resto From CAEParte cp Inner Join CAE c on c.CAEId = cp.CAEId Where cp.EmpId = " + EmpId + "  And c.CAETipoCFE = " + TipoCFE + " And c.CAEVigencia <= " + Fto.FechaSQL(FechaGenerado) + "  And "
				+ "c.CAEVencimiento > " + Fto.FechaSQL(FchGen) + " And c.CAEAnulado = 0 And cp.SucId = " + SucId + " And cp.CajaId = " + CajaId + " And cp.CAEParteNroHasta > cp.CAEParteUltAsignado And "
				+ "cp.CAEParteUltAsignado = 0";
		dtCAE_CAEParte = NadDato.GetTable(sSql);
		if (NadDato.getRowCount(dtCAE_CAEParte) > 0) {
			dtCAE_CAEParte = NadDato.GetTable(sSql);
			asignados += dtCAE_CAEParte.getDouble("Asignado");
			restantes += dtCAE_CAEParte.getDouble("Resto");
		}
		if ((asignados - restantes) / asignados * 100 < PorcUscoAntelacionCAE) {
			return true;
		} else {
			return false;
		}
	}

    public final boolean ReservaNroCAEEstaVigente(int EmpId, long CAENroAutorizacion, String CAESerie, long CAEParteResNroCAE, int MinutosCaducidadReservaCAE) throws SQLException, TAException, Exception {
        boolean tempReservaNroCAEEstaVigente = false;
        ResultSet dt = null;
        ResultSet drCAE = null;
        ResultSet drCAEParte = null;
        java.util.Date FechaCaducidadCAE = new java.util.Date();

        //resto un minuto para que si van a usar esta consulta para saber si firmar o no, les de un minuto para usar el Nro de CAE y no este por anularlo
        MinutosCaducidadReservaCAE += 1;
        FechaCaducidadCAE.setMinutes(FechaCaducidadCAE.getMinutes() + MinutosCaducidadReservaCAE * -1);

        try {
            sSQL = "Select * From CAE Where EmpId = " + EmpId + " AND CAENroAutorizacion = " + CAENroAutorizacion;
            dt = NadDato.GetTable(sSQL);
            if (NadDato.getRowCount(dt) > 0) {

                dt = NadDato.GetTable(sSQL);
                drCAE = dt;
                int CAEId = drCAE.getInt(CAE.CAEId);
                sSQL = "Select * From CAEParte Where EmpId = " + EmpId + " And CAEId = " + drCAE.getInt(CAE.CAEId) + " And " + CAEParteResNroCAE + " BETWEEN CAEParteNroDesde AND CAEParteNroHasta";

                dt = NadDato.GetTable(sSQL);
                if (NadDato.getRowCount(dt) > 0) {

                    dt = NadDato.GetTable(sSQL);
                    drCAEParte = dt;
                    sSQL = "Select * From CAEParteReservado Where EmpId = " + EmpId + " And CAEId = " + CAEId + " And CAEParteId = " + drCAEParte.getInt(CAEParte.CAEParteId) + " And CAEParteResNroCAE = " + CAEParteResNroCAE + " And CAEParteResEstado = " + "'PEN'" + " And CAEParteResFchHora > " + Fto.FechaHoraSQL(FechaCaducidadCAE);

                    dt = NadDato.GetTable(sSQL);
                    if (NadDato.getRowCount(dt) > 0) {
                        tempReservaNroCAEEstaVigente = true;
                    }
                }
            }
        } catch (RuntimeException ex) {
            throw new TAException("Error al reservar nro CAE. " + ex.getMessage());
        } catch (Exception ex) {
            throw new TAException("Error al reservar nro CAE. " + ex.getMessage());
        }
        return tempReservaNroCAEEstaVigente;
    }

    public final boolean CancelarCAEReservadosCaducados(int EmpId, int MinutosCaducidadReservaCAE) throws Exception {
        boolean tempCancelarCAEReservadosCaducados = false;
        java.util.Date FechaCaducidadCAE = new java.util.Date();

        FechaCaducidadCAE.setMinutes(FechaCaducidadCAE.getMinutes() + MinutosCaducidadReservaCAE * -1);

        try {
            sSQL = "Update CAEParteReservado Set CAEParteResEstado = 'ANU',CAEParteResCaduco = 1 Where EmpId = " + EmpId + " AND CAEParteResEstado = 'PEN' AND CAEParteResFchHora < " + Fto.FechaHoraSQL(FechaCaducidadCAE);
            NadDato.Conectar();
            NadDato.IniTrans();
            PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);
            try {
                sttment.execute();
                tempCancelarCAEReservadosCaducados = true;
            } catch (SQLException ex) {
                throw new TAFACE2ApiEntidad.TAException("Error al cancelar CAEReservadosCaducados.");
            } finally {
                NadDato.FinTrans();
            }
        } catch (RuntimeException ex) {
            throw new TAException("Error al cancelar CAE reservado caducado. " + ex.getMessage());
        }
        return tempCancelarCAEReservadosCaducados;
    }

    public final boolean CancelarCAEReservadosCAEParteAnulada(int EmpId, int CAEId, int CAEParteId) throws Exception {
        boolean tempCancelarCAEReservadosCAEParteAnulada = false;
        java.util.Date FechaCaducidadCAE = new java.util.Date();
        try {
            if (ExistenCAEReservadosCAEParteParaAnular(EmpId, CAEId, CAEParteId)) {
                sSQL = "Update CAEParteReservado Set CAEParteResEstado = 'ANU' Where EmpId = " + EmpId + " AND CAEParteResEstado = 'PEN' AND CAEId = " + CAEId + " AND CAEParteId = " + CAEParteId;
                NadDato.Conectar();
                NadDato.IniTrans();
                PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

                try {
                    sttment.execute();
                    tempCancelarCAEReservadosCAEParteAnulada = true;
                } catch (SQLException ex) {
                    throw new TAFACE2ApiEntidad.TAException("Error al cancelar CAEReservados CAEParteAnulado.");
                } finally {
                    NadDato.FinTrans();
                }
            }
        } catch (RuntimeException ex) {
            throw new TAException("Error al cancelar CAE reservado CAEParteAnulado. " + ex.getMessage());
        }

        return tempCancelarCAEReservadosCAEParteAnulada;
    }

    public final boolean ExistenCAEReservadosCAEParteParaAnular(int EmpId, int CAEId, int CAEParteId) throws SQLException, TAException {
        boolean ExistenCAEReservadosCAEParteParaAnular = false;
        try {
            sSQL = "Select * from CAEParteReservado Where EmpId = " + EmpId + " AND CAEParteResEstado = 'PEN' AND CAEId = " + CAEId + " AND CAEParteId = " + CAEParteId;
            try {
                if (NadDato.getRowCount(NadDato.GetTable(sSQL)) == 0) {
                    ExistenCAEReservadosCAEParteParaAnular = false;
                } else {
                    ExistenCAEReservadosCAEParteParaAnular = true;
                }
            } catch (Exception ex) {
                Logger.getLogger(TACAE.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (Exception ex) {
            throw new TAException("Error al cancelar CAE reservado CAEParteAnulado. " + ex.getMessage());
        }
        return ExistenCAEReservadosCAEParteParaAnular;

    }

    public final boolean SetEstadoCAEReservadoUtilizado(int EmpId, int CAEId, int CAEParteId, int CAEParteResNroCAE) throws SQLException, Exception {
        boolean tempSetEstadoCAEReservadoUtilizado = false;
        ResultSet dt = null;
        try {
            sSQL = "Select * From CAEParteReservado Where EmpId = " + EmpId + " And CAEId = " + CAEId + " And CAEParteId = " + CAEParteId + " And CAEParteResNroCAE = " + CAEParteResNroCAE;
            dt = NadDato.GetTable(sSQL);
            if (NadDato.getRowCount(dt) > 0) {
                sSQL = "Select * From CAEParteReservado Where EmpId = " + EmpId + " And CAEId = " + CAEId + " And CAEParteId = " + CAEParteId + " And CAEParteResNroCAE = " + CAEParteResNroCAE + " And CAEParteResEstado = " + "'PEN'";
                dt = NadDato.GetTable(sSQL);
                if (NadDato.getRowCount(dt) > 0) {
                    NadDato.Conectar();
                    NadDato.IniTrans();
                    PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);
                    try {
                        sttment.execute();
                        tempSetEstadoCAEReservadoUtilizado = true;
                    } catch (SQLException ex) {
                    } finally {
                        NadDato.FinTrans();
                    }
                } else {
                    throw new RuntimeException(getNombreMetodo() + " - CAEParteReservado que se encontro no esta en estado Pendiente. " + "\r\n" + "Emp: " + EmpId + " - CAE: " + CAEId + " - CAEParte: " + CAEParteId + " - Nro. de CAE Reservado: " + CAEParteResNroCAE);
                }
            } else {
                throw new RuntimeException(getNombreMetodo() + " - No se encuentra CAEParteReservado. " + "\r\n" + "Emp: " + EmpId + " - CAE: " + CAEId + " - CAEParte: " + CAEParteId + " - Nro. de CAE Reservado: " + CAEParteResNroCAE);
            }
        } catch (RuntimeException ex) {
            throw ex;
        }
        return tempSetEstadoCAEReservadoUtilizado;
    }

    public final boolean SetEstadoCAEReservadoAnulado(int EmpId, int CAEId, int CAEParteId, long CAEParteResNroCAE, boolean CAEParteResCaduco) throws SQLException, Exception {
        boolean tempSetEstadoCAEReservadoAnulado = false;
        ResultSet dt = null;
        int IntBool = 0;

        if (CAEParteResCaduco) {
            IntBool = 1;
        } else {
            IntBool = 0;
        }
        try {
            sSQL = "Select * From CAEParteReservado Where EmpId = " + EmpId + " And CAEId = " + CAEId + " And CAEParteId = " + CAEParteId + " And CAEParteResNroCAE = " + CAEParteResNroCAE;
            dt = NadDato.GetTable(sSQL);
            if (NadDato.getRowCount(dt) > 0) {
                sSQL = "Select * From CAEParteReservado Where EmpId = " + EmpId + " And CAEId = " + CAEId + " And CAEParteId = " + CAEParteId + " And CAEParteResNroCAE = " + CAEParteResNroCAE + " And CAEParteResEstado = " + "'PEN'";
                dt = NadDato.GetTable(sSQL);
                if (NadDato.getRowCount(dt) > 0) {
                    sSQL = "Update CAEParteReservado Set CAEParteResEstado = " + "'ANU'" + " ,CAEParteResCaduco = " + IntBool + " Where EmpId = " + EmpId + " And CAEId = " + CAEId + " And CAEParteId = " + CAEParteId + " And CAEParteResNroCAE = " + CAEParteResNroCAE;
                    NadDato.Conectar();
                    NadDato.IniTrans();
                    PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

                    try {
                        sttment.execute();
                        tempSetEstadoCAEReservadoAnulado = true;
                    } catch (SQLException ex) {
                        throw new TAFACE2ApiEntidad.TAException("Error al cambiar estado CAEReservadoAnulado. " + ex.getMessage());
                    } finally {
                        NadDato.FinTrans();
                    }
                } else {
                    throw new RuntimeException(getNombreMetodo() + " - CAEParteReservado que se encontro no esta en estado Pendiente. " + "\r\n" + "Emp: " + EmpId + " - CAE: " + CAEId + " - CAEParte: " + CAEParteId + " - Nro. de CAE Reservado: " + CAEParteResNroCAE);
                }
            } else {
                throw new RuntimeException(getNombreMetodo() + " - No se encuentra CAEParteReservado. " + "\r\n" + "Emp: " + EmpId + " - CAE: " + CAEId + " - CAEParte: " + CAEParteId + " - Nro. de CAE Reservado: " + CAEParteResNroCAE);
            }
        } catch (RuntimeException ex) {
            throw ex;
        }
        return tempSetEstadoCAEReservadoAnulado;
    }

    public final boolean GrabarCAEParteReservado(Map<String, Object> drCAEParteReservado) throws SQLException, Exception {
        boolean tempGrabarCAEParteReservado = false;
        sSQL = "Select * From CAEParteReservado Where EmpId = " + Integer.parseInt(drCAEParteReservado.get(CAEParteReservado.EmpId).toString()) + " And CAEId = " + Integer.parseInt(drCAEParteReservado.get(CAEParteReservado.CAEId).toString()) + " And CAEParteId = " + Integer.parseInt(drCAEParteReservado.get(CAEParteReservado.CAEParteId).toString()) + " And CAEParteResNroCAE = " + Integer.parseInt(drCAEParteReservado.get(CAEParteReservado.CAEParteResNroCAE).toString());
        if (NadDato.getRowCount(NadDato.GetTable(sSQL)) == 0) {
            sSQL = assembly.AssemblyCAEParteReservadoInsert(drCAEParteReservado);
        } else {
            sSQL = assembly.AssemblyCAEParteReservadoUpdate(drCAEParteReservado) + " Where EmpId = " + Integer.parseInt(drCAEParteReservado.get(CAEParteReservado.EmpId).toString()) + " And CAEId = " + Integer.parseInt(drCAEParteReservado.get(CAEParteReservado.CAEId).toString()) + " And CAEParteId = " + Integer.parseInt(drCAEParteReservado.get(CAEParteReservado.CAEParteId).toString()) + " And CAEParteResNroCAE = " + Integer.parseInt(drCAEParteReservado.get(CAEParteReservado.CAEParteResNroCAE).toString());
        }
        NadDato.Conectar();
        NadDato.IniTrans();
        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

        try {
            sttment.execute();
            tempGrabarCAEParteReservado = true;
        } catch (SQLException ex) {
            throw new TAFACE2ApiEntidad.TAException("Error al grabar CAEParteReservado.");
        } finally {
            NadDato.FinTrans();
        }
        return tempGrabarCAEParteReservado;
    }

    public final ResultSet ConsCAEParteReservado(int EmpId, int CAEId, int CAEParteId, long CAEParteResNroCAE) throws SQLException {
        ResultSet dt = null;
        sSQL = "Select * From CAEParteReservado Where EmpId = " + EmpId + " And CAEId=" + CAEId + " And CAEParteId=" + CAEParteId + " And CAEParteResNroCAE=" + CAEParteResNroCAE;
        dt = NadDato.GetTable(sSQL);
        //dt.TableName = Tb.CAEParteReservado.Nombre;
        return dt;
    }

    public final boolean CargarDatosCAEParteReserva(int EmpId, long CAENroAutorizacion, String CAESerie, long CAEParteResNroCAE, tangible.RefObject<Integer> CAEId, tangible.RefObject<Integer> CAEParteId) throws SQLException {

        boolean tempCargarDatosCAEParteReserva = false;
        try {
            ResultSet dt = null;
            sSQL = "Select * From CAEParte CP Inner Join CAE C on CP.CAEId = C.CAEId Where C.EmpId = " + EmpId + " AND C.CAENroAutorizacion = " + CAENroAutorizacion + " And C.CAESerie = " + "'" + CAESerie + "' AND " + CAEParteResNroCAE + " BETWEEN CAEParteNroDesde AND CAEParteNroHasta";
            dt = NadDato.GetTable(sSQL);

            if (NadDato.getRowCount(dt) > 0) {
                dt = NadDato.GetTable(sSQL);
                CAEId.argValue = dt.getInt("CAEId");
                CAEParteId.argValue = dt.getInt("CAEParteId");
                tempCargarDatosCAEParteReserva = true;
            }
        } catch (Exception ex) {
            Logger.getLogger(TACAE.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tempCargarDatosCAEParteReserva;

    }

    public final boolean TomarCAEReservado(int EmpId, int CAETipoCFE, long CAENroAutorizacion, String CAESerie, long CAEParteResNroCAE, tangible.RefObject<Map<String, Object>> drCAE, tangible.RefObject<Map<String, Object>> drCAEParte, tangible.RefObject<Map<String, Object>> drCAEParteReservado) throws SQLException, Exception {
        boolean tempTomarCAEReservado = false;
        ResultSet dt = null;

        try {
            sSQL = "Select * From CAE Where EmpId = " + EmpId + " AND CAENroAutorizacion = " + CAENroAutorizacion;
            dt = NadDato.GetTable(sSQL);
            if (NadDato.getRowCount(dt) > 0) {
                dt = NadDato.GetTable(sSQL);
                drCAE.argValue = resultSetToArrayList(dt);
                if (CAETipoCFE != Integer.parseInt(drCAE.argValue.get(CAE.CAETipoCFE).toString())) {
                    throw new RuntimeException(getNombreMetodo() + " - La Reserva de CAE que esta intentando utilizar es de un tipo de CFE distinto al del comprobante " + "\r\n" + "Emp: " + EmpId + " - Nro. Autorizacion: " + CAENroAutorizacion + " - Serie: " + CAESerie + " - Nro. de CAE Reservado: " + CAEParteResNroCAE + " - Tipo de CFE de la Reserva: " + Integer.parseInt(drCAE.argValue.get(CAE.CAETipoCFE).toString()) + " - Tipo de CFE del Comprobante: " + (new Integer(CAETipoCFE)).toString().trim());
                }
                sSQL = "Select * From CAEParte Where EmpId = " + EmpId + " And CAEId = " + Integer.parseInt(drCAE.argValue.get(CAE.CAEId).toString()) + " And " + CAEParteResNroCAE + " BETWEEN CAEParteNroDesde AND CAEParteNroHasta";
                dt = NadDato.GetTable(sSQL);
                if (NadDato.getRowCount(dt) > 0) {

                    dt = NadDato.GetTable(sSQL);
                    drCAEParte.argValue = resultSetToArrayList(dt);
                    sSQL = "Select * From CAEParteReservado Where EmpId = " + EmpId + " And CAEId = " + Integer.parseInt(drCAE.argValue.get(CAE.CAEId).toString()) + " And CAEParteId = " + Integer.parseInt(drCAEParte.argValue.get(CAEParte.CAEParteId).toString()) + " And CAEParteResNroCAE = " + CAEParteResNroCAE;
                    dt = NadDato.GetTable(sSQL);
                    if (NadDato.getRowCount(dt) > 0) {
                        dt = NadDato.GetTable(sSQL);
                        drCAEParteReservado.argValue = resultSetToArrayList(dt);
                        if (drCAEParteReservado.argValue.get(CAEParteReservado.CAEParteResEstado).toString().equals("ANU")) {
                            throw new RuntimeException(getNombreMetodo() + " - La Reserva de CAE que esta intentando utilizar esta Anulada. " + "\r\n" + "Emp: " + EmpId + " - Nro. Autorizacion: " + CAENroAutorizacion + " - Serie: " + CAESerie + " - Nro. de CAE Reservado: " + CAEParteResNroCAE);
                        }

                        if (drCAEParteReservado.argValue.get(CAEParteReservado.CAEParteResEstado).toString().equals("UTI")) {
                            throw new RuntimeException(getNombreMetodo() + " - La Reserva de CAE que esta intentando utilizar ya fue Utilizada. " + "\r\n" + "Emp: " + EmpId + " - Nro. Autorizacion: " + CAENroAutorizacion + " - Serie: " + CAESerie + " - Nro. de CAE Reservado: " + CAEParteResNroCAE);
                        }

                        sSQL = "Update CAEParteReservado Set CAEParteResEstado = " + "'UTI'" + " Where EmpId = " + EmpId + " And CAEId = " + Integer.parseInt(drCAEParte.argValue.get(CAEParte.CAEId).toString()) + " And CAEParteId = " + Integer.parseInt(drCAEParte.argValue.get(CAEParte.CAEParteId).toString()) + " And CAEParteResNroCAE = " + CAEParteResNroCAE;
                        NadDato.Conectar();
                        NadDato.IniTrans();
                        PreparedStatement sttment = NadDato.GetConnectionLite().prepareStatement(sSQL);

                        try {
                            sttment.execute();
                        } catch (SQLException ex) {
                            throw new TAFACE2ApiEntidad.TAException("Error al actualizar CAEParteReservado");
                        } finally {
                            NadDato.FinTrans();
                        }
                        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
                        java.util.Date CAEParteResFchHora = null;
                        CAEParteResFchHora = formato.parse(String.valueOf(drCAEParteReservado.argValue.get(CAEParteReservado.CAEParteResFchHora)));
                        
                        //Restauro la anulacion de CAE que hice al Reservar el CAE.
                        tempTomarCAEReservado = RestaurarAnulacionCAE(EmpId, Integer.parseInt(drCAEParteReservado.argValue.get(CAEParteReservado.CAEId).toString()), Integer.parseInt(drCAEParteReservado.argValue.get(CAEParteReservado.CAEParteId).toString()), CAEParteResFchHora, Long.parseLong(drCAEParteReservado.argValue.get(CAEParteReservado.CAEParteResNroCAE).toString()));
                    }
                } else {
                    throw new RuntimeException(getNombreMetodo() + " - No se encuentra la Parte del CAE de donde proviene la Reserva. " + "\r\n" + "Emp: " + EmpId + " - Nro. Autorizacion: " + CAENroAutorizacion + " - Serie: " + CAESerie + " - Nro. de CAE Reservado: " + CAEParteResNroCAE);
                }
            } else {
                throw new RuntimeException(getNombreMetodo() + " - No se encuentra el CAE de donde proviene la Reserva. " + "\r\n" + "Emp: " + EmpId + " - Nro. Autorizacion: " + CAENroAutorizacion + " - Serie: " + CAESerie + " - Nro. de CAE Reservado: " + CAEParteResNroCAE);
            }
        } catch (RuntimeException ex) {
            throw ex;
        }
        return tempTomarCAEReservado;
    }

    public final Map<String, Object> CrearCAE() {

        Map<String, Object> records = new HashMap<String, Object>();
        records.put(CAE.EmpId, Integer.class);
        records.put(CAE.CAEId, Integer.class);
        records.put(CAE.CAENroAutorizacion, String.class);
        records.put(CAE.CAETipoCFE, Short.class);
        records.put(CAE.CAESerie, String.class);
        records.put(CAE.CAENroDesde, Integer.class);
        records.put(CAE.CAENroHasta, Integer.class);
        records.put(CAE.CAEVigencia, java.util.Date.class);
        records.put(CAE.CAEVencimiento, java.util.Date.class);
        records.put(CAE.CAEAnulado, Integer.class);
        records.put(CAE.CAEFchHoraSync, String.class);
		records.put(CAE.CAEEspecial, Integer.class);
		records.put(CAE.CAECAusalTipo, Integer.class);
        return records;
    }

    public final Map<String, Object> CrearCAEParte() {

        Map<String, Object> records = new HashMap<String, Object>();
        records.put(CAEParte.EmpId, Integer.class);
        records.put(CAEParte.CAEId, Integer.class);
        records.put(CAEParte.CAEParteId, Integer.class);
        records.put(CAEParte.SucId, Integer.class);
        records.put(CAEParte.CajaId, Integer.class);
        records.put(CAEParte.CAEParteNroDesde, Integer.class);
        records.put(CAEParte.CAEParteNroHasta, Integer.class);
        records.put(CAEParte.CAEParteFchHoraSync, String.class);
        records.put(CAEParte.CAEParteUltAsignado, Integer.class);
        records.put(CAEParte.CAEParteAnulado, Integer.class);
        records.put(CAEParte.CAEParteAnuladoSync, Integer.class);
        records.put(CAEParte.CAEParteFchHoraUltSync, String.class);
        records.put(CAEParte.CAEParteRowGUID, String.class);
        return records;
    }

    public final Map<String, Object> CrearCAEParteUtilizado() {

        Map<String, Object> records = new HashMap<String, Object>();
        records.put(CAEParteUtilizado.EmpId, Integer.class);
        records.put(CAEParteUtilizado.CAEId, Integer.class);
        records.put(CAEParteUtilizado.CAEParteId, Integer.class);
        records.put(CAEParteUtilizado.CAEParteFchUtilizado, String.class);
        records.put(CAEParteUtilizado.CAEParteUtilNroDesde, Integer.class);
        records.put(CAEParteUtilizado.CAEParteUtilNroHasta, Integer.class);
        records.put(CAEParteUtilizado.CAEParteUtilSincronizado, Integer.class);
        return records;
    }

    public final Map<String, Object> CrearCAEParteUtilizadoAnulado() {

        Map<String, Object> records = new HashMap<String, Object>();
        records.put(CAEParteUtilizadoAnulado.EmpId, Integer.class);
        records.put(CAEParteUtilizadoAnulado.CAEId, Integer.class);
        records.put(CAEParteUtilizadoAnulado.CAEParteId, Integer.class);
        records.put(CAEParteUtilizadoAnulado.CAEParteFchUtilizado, String.class);
        records.put(CAEParteUtilizadoAnulado.CAEParteAnulNro, Integer.class);
        records.put(CAEParteUtilizadoAnulado.CAEParteAnulSincronizado, Integer.class);
        records.put(CAEParteUtilizadoAnulado.CAEParteAnulRestaurar, Integer.class);
        return records;
    }

    public final Map<String, Object> CrearCAEParteReservado() {

        Map<String, Object> records = new HashMap<String, Object>();
        records.put(CAEParteReservado.EmpId, Integer.class);
        records.put(CAEParteReservado.CAEId, Integer.class);
        records.put(CAEParteReservado.CAEParteId, Integer.class);
        records.put(CAEParteReservado.CAEParteResNroCAE, Integer.class);
        records.put(CAEParteReservado.CAEParteResFchHora, String.class);
        records.put(CAEParteReservado.CAEParteResCaduco, Integer.class);
        records.put(CAEParteReservado.CAEParteResEstado, String.class);
        return records;
    }

    public TACAE(NadMADato Dato) {
        NadDato = Dato;
        Fto.setProveedorConexion(Dato.getProveedorConexion().getValue());
    }

    //Retorna el nombre del metodo desde el cual se hace el llamado
    public String getNombreMetodo() {
        return new Exception().getStackTrace()[1].getMethodName();
    }

    private Map<String, Object> resultSetToArrayList(ResultSet rs) throws SQLException {
        Map<String, Object> myMap = new HashMap<String, Object>();

        ResultSetMetaData meta = rs.getMetaData();
        int row = 0;
        while (rs.next() && row == 0) {
            for (int i = 1; i <= meta.getColumnCount(); i++) {
                String key = meta.getColumnName(i);
                Object value = rs.getObject(key);
                myMap.put(key, value);
            }
            row++;
        }
        return myMap;
    }
    
    private List<Map<String, Object>> resultSetToArrayListFull(ResultSet rs) throws SQLException {
        List<Map<String, Object>> myMapList = new ArrayList<Map<String, Object>>();
        Map<String, Object> myMap = new HashMap<String, Object>();
        ResultSetMetaData meta = rs.getMetaData();
        while (rs.next()) {
            for (int i = 1; i <= meta.getColumnCount(); i++) {
                String key = meta.getColumnName(i);
                Object value = rs.getObject(key);
                myMap.put(key, value);
            }
            myMapList.add(myMap);
        }
        return myMapList;
    }

    public Date ConvertDate(String date) {
        Date dateResult = null;
        if (date.length() > 0) {

            String[] l = date.split(" ");
            String[] fecha = l[0].split("-");
            String[] time = l[1].split(":");
            int year, month, day, hour, minute, second = 0;
            year = Integer.parseInt(fecha[0]);
            month = Integer.parseInt(fecha[1]);
            day = Integer.parseInt(fecha[2]);
            hour = Integer.parseInt(time[0]);
            minute = Integer.parseInt(time[1]);
            second = Integer.parseInt(time[2]);
            dateResult = new Date(year, month, day, hour, minute, second);
        }
        return dateResult;
    }
	
	public final boolean EliminarCAECompleto() throws Exception {
		sSQL = "DELETE from CAEParte";
		NadDato.Ejecutar(sSQL);
		return true;
	}
}