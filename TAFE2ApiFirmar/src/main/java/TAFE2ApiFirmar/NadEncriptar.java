package TAFE2ApiFirmar;

public class NadEncriptar {

    public String DesEncriptarNad(String Datos, String Pass) {
        return DesEncriptarNad(Datos, Pass, false);
    }

    public String EncriptarNad(String Datos, String Pass) {
        return EncriptarNad(Datos, Pass, false);
    }

    public String DesEncriptarNad(String Datos, String Pass, boolean Hexa) {
        String str = "";
        if (Hexa) {
            Datos = HexToString(Datos);
        }
        int length1 = Datos.length();
        String a = new StringBuilder(Datos.substring(0, (int) Math.round((double) length1 / 2.0))).reverse().toString();
        String b = new StringBuilder(Datos.substring(Datos.length() - (int) Math.round((double) length1 / 2.0))).reverse().toString();
        Datos = a + b;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < Datos.length(); i++) {
            sb.append((char) (Datos.charAt(i) ^ Pass.charAt(i % Pass.length())));
        }
        str = sb.toString();
        return str;
    }

    public String EncriptarNad(String Datos, String Pass, boolean Hexa) {
        String str = "";
        int num1 = Datos == null ? 0 : Datos.length();
        if (num1 % 2 != 0) {
            Datos = Datos + " ";
            num1 += 1;

        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < Datos.length(); i++) {
            sb.append((char) (Datos.charAt(i) ^ Pass.charAt(i % Pass.length())));
        }
        str = sb.toString();
        String c1 = new StringBuilder(str.substring(0, (int) Math.round((double) (str == null ? 0 : str.length()) / 2.0))).reverse().toString();
        String c2 = new StringBuilder(str.substring(str.length() - (int) Math.round((double) (str == null ? 0 : str.length()) / 2.0))).reverse().toString();
        String sAsciiData = c1 + c2;
        if (Hexa) {
            sAsciiData = ASCIItoHEX(sAsciiData);
        }
        return sAsciiData;
    }

    public String ASCIItoHEX(String sAsciiData) {
        String str = "";
        int num1 = sAsciiData == null ? 0 : sAsciiData.length();
        int num2 = 1;
        int num3 = num1;
        int Start = num2;
        while (Start <= num3) {
            String Expression = Integer.toHexString(substring(sAsciiData, Start - 1, 1).charAt(0)).toUpperCase();
            if ((Expression == null ? 0 : Expression.length()) < 2) {
                Expression = "0" + Expression;
            }
            str = str + Expression;
            Start += 1;

        }
        return str;
    }

    public String HexToString(String sHexData) {

        StringBuilder sb = new StringBuilder();
        StringBuilder temp = new StringBuilder();

        //49204c6f7665204a617661 split into two characters 49, 20, 4c...
        for (int i = 0; i < sHexData.length() - 1; i += 2) {

            //grab the hex in pairs
            String output = sHexData.substring(i, (i + 2));
            //convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            //convert the decimal to character
            sb.append((char) decimal);

            temp.append(decimal);
        }

        return sb.toString();
    }

    public String substring(String string, int start, int length) {
        return string.substring(start, start + length);
    }
}
