package TAFE2ApiFirmar;

import TAFACE2ApiEntidad.TAException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class TALogErrores {

    public static class LogErrores {

        /**
         * @return the EmpId
         */
        public static String getEmpId() {
            return EmpId;
        }

        /**
         * @param aEmpId the EmpId to set
         */
        public static void setEmpId(String aEmpId) {
            EmpId = aEmpId;
        }

        /**
         * @return the SucId
         */
        public static String getSucId() {
            return SucId;
        }

        /**
         * @param aSucId the SucId to set
         */
        public static void setSucId(String aSucId) {
            SucId = aSucId;
        }

        /**
         * @return the CajaId
         */
        public static String getCajaId() {
            return CajaId;
        }

        /**
         * @param aCajaId the CajaId to set
         */
        public static void setCajaId(String aCajaId) {
            CajaId = aCajaId;
        }

        /**
         * @return the LogErrId
         */
        public static String getLogErrId() {
            return LogErrId;
        }

        /**
         * @param aLogErrId the LogErrId to set
         */
        public static void setLogErrId(String aLogErrId) {
            LogErrId = aLogErrId;
        }

        /**
         * @return the LogErrFchHora
         */
        public static String getLogErrFchHora() {
            return LogErrFchHora;
        }

        /**
         * @param aLogErrFchHora the LogErrFchHora to set
         */
        public static void setLogErrFchHora(String aLogErrFchHora) {
            LogErrFchHora = aLogErrFchHora;
        }

        /**
         * @return the UsuId
         */
        public static String getUsuId() {
            return UsuId;
        }

        /**
         * @param aUsuId the UsuId to set
         */
        public static void setUsuId(String aUsuId) {
            UsuId = aUsuId;
        }

        /**
         * @return the LogErrMsg
         */
        public static String getLogErrMsg() {
            return LogErrMsg;
        }

        /**
         * @param aLogErrMsg the LogErrMsg to set
         */
        public static void setLogErrMsg(String aLogErrMsg) {
            LogErrMsg = aLogErrMsg;
        }

        /**
         * @return the LogErrTipoError
         */
        public static String getLogErrTipoError() {
            return LogErrTipoError;
        }

        /**
         * @param aLogErrTipoError the LogErrTipoError to set
         */
        public static void setLogErrTipoError(String aLogErrTipoError) {
            LogErrTipoError = aLogErrTipoError;
        }

        /**
         * @return the LogErrCod
         */
        public static String getLogErrCod() {
            return LogErrCod;
        }

        /**
         * @param aLogErrCod the LogErrCod to set
         */
        public static void setLogErrCod(String aLogErrCod) {
            LogErrCod = aLogErrCod;
        }

        /**
         * @return the LogErrUrgencia
         */
        public static String getLogErrUrgencia() {
            return LogErrUrgencia;
        }

        /**
         * @param aLogErrUrgencia the LogErrUrgencia to set
         */
        public static void setLogErrUrgencia(String aLogErrUrgencia) {
            LogErrUrgencia = aLogErrUrgencia;
        }

        /**
         * @return the LogErrEnviado
         */
        public static String getLogErrEnviado() {
            return LogErrEnviado;
        }

        /**
         * @param aLogErrEnviado the LogErrEnviado to set
         */
        public static void setLogErrEnviado(String aLogErrEnviado) {
            LogErrEnviado = aLogErrEnviado;
        }

        /**
         * @return the LogErrSolucion
         */
        public static String getLogErrSolucion() {
            return LogErrSolucion;
        }

        /**
         * @param aLogErrSolucion the LogErrSolucion to set
         */
        public static void setLogErrSolucion(String aLogErrSolucion) {
            LogErrSolucion = aLogErrSolucion;
        }

        /**
         * @return the LogErrNroLinea
         */
        public static String getLogErrNroLinea() {
            return LogErrNroLinea;
        }

        /**
         * @param aLogErrNroLinea the LogErrNroLinea to set
         */
        public static void setLogErrNroLinea(String aLogErrNroLinea) {
            LogErrNroLinea = aLogErrNroLinea;
        }

        /**
         * @return the LogErrFecha
         */
        public static String getLogErrFecha() {
            return LogErrFecha;
        }

        /**
         * @param aLogErrFecha the LogErrFecha to set
         */
        public static void setLogErrFecha(String aLogErrFecha) {
            LogErrFecha = aLogErrFecha;
        }

        /**
         * @return the LogErrStackTrace
         */
        public static String getLogErrStackTrace() {
            return LogErrStackTrace;
        }

        /**
         * @param aLogErrStackTrace the LogErrStackTrace to set
         */
        public static void setLogErrStackTrace(String aLogErrStackTrace) {
            LogErrStackTrace = aLogErrStackTrace;
        }

        /**
         * @return the LogErrSincronizado
         */
        public static String getLogErrSincronizado() {
            return LogErrSincronizado;
        }

        /**
         * @param aLogErrSincronizado the LogErrSincronizado to set
         */
        public static void setLogErrSincronizado(String aLogErrSincronizado) {
            LogErrSincronizado = aLogErrSincronizado;
        }
        private Object x;
        private static String EmpId = "EmpId";
        private static String SucId = "SucId";
        private static String CajaId = "CajaId";
        private static String LogErrId = "LogErrId";
        private static String LogErrFchHora = "LogErrFchHora";
        private static String UsuId = "UsuId";
        private static String LogErrMsg = "LogErrMsg";
        private static String LogErrTipoError = "LogErrTipoError";
        private static String LogErrCod = "LogErrCod";
        private static String LogErrUrgencia = "LogErrUrgencia";
        private static String LogErrEnviado = "LogErrEnviado";
        private static String LogErrSolucion = "LogErrSolucion";
        private static String LogErrNroLinea = "LogErrNroLinea";
        private static String LogErrFecha = "LogErrFecha";
        private static String LogErrStackTrace = "LogErrStackTrace";
        private static String LogErrSincronizado = "LogErrSincronizado";
    }
    private String sSQL;
    private NadMADato NadDato;
    private NadFormato Fto = new NadFormato();

    public final boolean GrabarError(int EmpId, int SucId, int CajaId, String LogErrMsg, String LogErrTipoErr, int LogErrCod, String LogErrUrgencia, short LogErrNroLinea, String LogErrStackTrace) throws Exception {
        boolean tempGrabarError = false;
        tempGrabarError = false;
        try {
            sSQL = "Insert Into LogErrores (EmpId,SucId,CajaId,LogErrId,LogErrFchHora,LogErrMsg,LogErrTipoError,LogErrCod,LogErrUrgencia,LogErrSolucion,LogErrNroLinea,LogErrFecha,LogErrStackTrace,LogErrSincronizado) " + "Values (" + EmpId + "," + SucId + "," + CajaId + "," + NumeradorLogErrores(EmpId) + "," + Fto.FechaHoraSQL(new java.util.Date()) + "," + Fto.TextoSQL(LogErrMsg) + "," + Fto.TextoSQL(LogErrTipoErr) + "," + LogErrCod + "," + Fto.TextoSQL(LogErrUrgencia) + ",''," + LogErrNroLinea + "," + Fto.FechaSQL(new java.util.Date()) + "," + Fto.TextoSQL(LogErrStackTrace) + ",0);";
            NadDato.Ejecutar(sSQL);
            tempGrabarError = true;
        } catch (RuntimeException ex) {
            throw new TAException("Error grabando el Log: " + ex.getMessage());
        }
        return tempGrabarError;
    }

    public final ResultSet ConsLogErroresPorSincronizar(int EmpId, int SucId, short CajaId, int Cantidad) throws SQLException {
        ResultSet dt = null;
        String sTope = "";
        if (NadDato.getProveedorConexion() == NadMADato.TIPO_PROVEEDOR.tpSQLiteClient) {
            if (Cantidad != 0) {
                sTope = "LIMIT " + Cantidad;
            }
            sSQL = "Select * From LogErrores Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajaId + " And LogErrSincronizado = 0 " + sTope;
        } else {
            if (Cantidad != 0) {
                sTope = "Top(" + Cantidad + ") ";
            }
            sSQL = "Select " + sTope + "* From LogErrores Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajaId + " And LogErrSincronizado = 0";
        }
        dt = NadDato.GetTable(sSQL);
        return dt;
    }

    public final int CantLogsPorSincAlerta(int EmpId, int SucId, short CajaId, int MinSinSincronizar) throws SQLException {
        ResultSet dt = null;
        Date current = new java.util.Date();
        current.setMinutes(current.getMinutes() + (-MinSinSincronizar));
        sSQL = "Select * From LogErrores Where EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajaId + " And LogErrSincronizado = 0 And LogErrFchHora < " + Fto.FechaHoraSQL(current);
        dt = NadDato.GetTable(sSQL);
        return NadDato.getRowCount(dt);
    }

    public final boolean EliminarLogsSincronizado(int EmpId, int SucId, short CajaId) throws Exception {
        boolean tempEliminarLogsSincronizado = false;
        sSQL = "Delete From LogErrores Where LogErrSincronizado = 1 And EmpId = " + EmpId + " And SucId = " + SucId + " And CajaId = " + CajaId;
        NadDato.Ejecutar(sSQL);
        NadDato.schrinkBD();
        return true;
    }

    public final boolean ActualizarLogErrorEnviado(int EmpId, int SucId, int CajaId, int LogErrId/*ResultSet dr*/) throws Exception {
        boolean tempActualizarLogErrorEnviado = false;
//        sSQL = "Update LogErrores Set LogErrSincronizado = 1 Where EmpId = " + dr.getInt(LogErrores.EmpId) + " And SucId = " + dr.getInt(LogErrores.SucId) + " And CajaId = " + dr.getInt(LogErrores.CajaId) + " And  LogErrId = " + dr.getInt(LogErrores.LogErrId);
        sSQL = "Update LogErrores Set LogErrSincronizado = 1 Where EmpId = " + EmpId + " And SucId = " + LogErrores.SucId + " And CajaId = " + CajaId + " And  LogErrId = " + LogErrId;
        NadDato.Ejecutar(sSQL);
        return true;
    }

    public final int NumeradorLogErrores(int EmpId) throws SQLException {
        int tempNumeradorLogErrores = 0;
        tempNumeradorLogErrores = 1;
        ResultSet dt = null;
        sSQL = "Select Max(LogErrId) As LogErrId From LogErrores Where EmpId = " + EmpId;
        dt = NadDato.GetTable(sSQL);
        ResultSet a = dt;
//        if (NadDato.getRowCount(a) > 0) {
        while (dt.next()) {
            if (dt.getInt("LogErrId") > 0) {
                tempNumeradorLogErrores = Fto.CTInt(dt.getInt("LogErrId")) + 1;
            }
        }
//        }
        return tempNumeradorLogErrores;
    }

    public TALogErrores(tangible.RefObject<NadMADato> Dato) {
        NadDato = Dato.argValue;
        Fto.setProveedorConexion(Dato.argValue.getProveedorConexion().getValue());
    }

    public TALogErrores(NadMADato Dato) {
        NadDato = Dato;
        Fto.setProveedorConexion(Dato.getProveedorConexion().getValue());
    }
}