package TAFE2ApiFirmar;

import org.sqldroid.SQLDroidConnection;

import TAFACE2ApiEntidad.TAException;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NadMADato {

    public NadMADato() {
        IniciarNadDato();
    }

    /**
     * @return the mvarStringConexion
     */
    public String getMvarStringConexion() {
        return mvarStringConexion;
    }

    /**
     * @param mvarStringConexion the mvarStringConexion to set
     */
    public void setMvarStringConexion(String mvarStringConexion) {
        this.mvarStringConexion = mvarStringConexion;

    }

    public enum TIPO_PROVEEDOR {

        tpSQLClient(1),
        tpSQLOLEDB(2),
        tpSQLiteClient(3);
        private int intValue;
        private static java.util.HashMap<Integer, TIPO_PROVEEDOR> mappings;

        private static java.util.HashMap<Integer, TIPO_PROVEEDOR> getMappings() {
            if (mappings == null) {
                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, TIPO_PROVEEDOR>();
                }
            }
            return mappings;
        }

        TIPO_PROVEEDOR(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static TIPO_PROVEEDOR forValue(int value) {
            return getMappings().get(value);
        }
    }
    private boolean mvarControlError = true;
    private TIPO_PROVEEDOR mvarCxProveedor = TIPO_PROVEEDOR.tpSQLClient;
    private NadSQLite NadLDato;
    private String mvarStringConexion = "";

    public TIPO_PROVEEDOR getProveedorConexion() {
        return mvarCxProveedor;
    }

    public void setProveedorConexion(TIPO_PROVEEDOR Value) {
        mvarCxProveedor = Value;
    }

    public void setPedirAccesoDato(boolean Value) {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            NadLDato.setPedirAccesoDato(Value);
        }
    }

    public void setSetCommandTimeout(int Value) {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            NadLDato.setSetCommandTimeout(Value);
        } else {
//            NadDato.setSetCommandTimeout(Value);
        }
    }

    public boolean getControlError() {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            return NadLDato.getControlError();
        } else {
            return false;
        }
    }

    public void setControlError(boolean Value) {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            NadLDato.setControlError(Value);
        }
    }

    public boolean getControlConcurrencia() {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            return NadLDato.getControlConcurrencia();
        } else {
            return false;
        }
    }

    public void setControlConcurrencia(boolean Value) {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            NadLDato.setControlConcurrencia(Value);
        }
    }

    public boolean getMultipleActiveDataReader() {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            return NadLDato.getMultipleActiveDataReader();
        } else {
            return false;
        }
    }

    public void setMultipleActiveDataReader(boolean Value) {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            NadLDato.setMultipleActiveDataReader(Value);
        }
    }

    public void setPassConexion(String Value) {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            NadLDato.setPassConexion(Value);
        }
    }

    public String getArchivoConexion() {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            return NadLDato.getArchivoConexion();
        } else {
            return "";
        }
    }

    public void setArchivoConexion(String Value) {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            NadLDato.setArchivoConexion(Value);
        } else {
//            NadDato.setArchivoConexion(Value);
        }
    }

    public String getNombreArchivo() {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            return NadLDato.getNombreArchivo();
        } else {
//            return NadDato.getNombreArchivo();
            return "";
        }
    }

    public void setNombreArchivo(String Value) {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            NadLDato.setNombreArchivo(Value);
        } else {
//            NadDato.setNombreArchivo(Value);
        }
    }

    public String getAplicacionNombre() {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            return NadLDato.getAplicacionNombre();
        } else {
//            return NadDato.getAplicacionNombre();
            return "";
        }
    }

    public void setAplicacionNombre(String Value) {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            NadLDato.setAplicacionNombre(Value);
        } else {
//            NadDato.setAplicacionNombre(Value);
        }
    }

    public String getRegistroAplicacion() {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            return NadLDato.getRegistroAplicacion();
        } else {
//            return NadDato.getRegistroAplicacion();
            return "";
        }
    }

    public void setRegistroAplicacion(String Value) {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            NadLDato.setRegistroAplicacion(Value);
        } else {
//            NadDato.setRegistroAplicacion(Value);
        }
    }

    public final void IniciarNadDato() {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            if ((NadLDato == null)) {
                NadLDato = new NadSQLite();
                NadLDato.setControlError(mvarControlError);
            }
        }
    }

    public boolean GrabarArchivo(String Servidor, String BaseDato, String Usuario, String Clave) throws TAException {
        boolean tempGrabarArchivo = false;

        try {
            if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
                tempGrabarArchivo = NadLDato.GrabarArchivo(BaseDato);
            } else {
//                tempGrabarArchivo = NadDato.GrabarArchivo(Servidor);
            }

        } catch (RuntimeException ex) {
            //throw new TAException(ex.getMessage(), ex);
            throw ex;
        }
        return tempGrabarArchivo;
    }

    public boolean Conectar(String Servidor, String BaseDato, String Usuario, String Clave) throws IOException, TAException, SQLException {
        boolean tempConectar = false;
        try {
            IniciarNadDato();
            if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
                tempConectar = NadLDato.Conectar(BaseDato);
            } else {
                //tempConectar = NadDato.Conectar(Servidor, BaseDato, Usuario, Clave);
            }
        } catch (RuntimeException ex) {
            if (!mvarControlError) {
                //throw new TAException(ex.getMessage(), ex);
                throw ex;
            }
        }
        return tempConectar;
    }

    public boolean Conectar(String Conexion) throws IOException, SQLException {
        boolean tempConectar = false;
        try {
            IniciarNadDato();
            if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
                tempConectar = NadLDato.Conectar(Conexion);
            } else {
                //tempConectar = NadDato.Conectar(Conexion);
            }
        } catch (TAException TaExBD) {
            throw TaExBD;
        } catch (RuntimeException ex) {
            if (!mvarControlError) {
                //throw new TAException(ex.getMessage(), ex);
                throw ex;
            }
        }
        return tempConectar;
    }

    public boolean Conectar() throws TAException, SQLException {
        boolean tempConectar = false;
        try {
            IniciarNadDato();
            if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
                NadLDato.setMvarStrConexion(mvarStringConexion);
                tempConectar = NadLDato.Conectar();
            } else {
                //tempConectar = NadDato.Conectar();
            }
        } catch (TAException TaExBD) {
            throw TaExBD;
        } catch (RuntimeException ex) {
            if (!mvarControlError) {
                throw ex;
            }
        }
        return tempConectar;
    }

    public boolean CrearBD(String RutaBD, String ScriptDB) throws TAException, SQLException, Exception {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            return NadLDato.CreateBD(RutaBD, ScriptDB);
        } else {
            //return NadDato.CreateBD(ScriptDB);
            return true;
        }
    }

    public void CerrarConexion() throws IOException, SQLException {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            NadLDato.CerrarConexion();
        } else {
            //NadDato.CerrarConexion();
        }
    }

    public SQLDroidConnection GetConnectionLite() {
        return NadLDato.GetConnection();
    }


    public ResultSet GetTable(String Sentencia) throws SQLException {
        ResultSet tempGetTable = null;
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            tempGetTable = NadLDato.GetTable(Sentencia);
        } else {
            //tempGetTable = NadDato.GetTable(Sentencia);
        }
        return tempGetTable;
    }

    public boolean GetTableReg(String Sentencia, tangible.RefObject<ResultSet> Table) throws InterruptedException, SQLException, TAException, InterruptedException, SQLException {
        boolean tempGetTableReg = false;
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            tempGetTableReg = NadLDato.GetTableReg(Sentencia, Table);
        } else {
            //tempGetTableReg = NadDato.GetTableReg(Sentencia, Table);
        }
        return tempGetTableReg;
    }

    public String GetInsertSQL(/* Fila*/) {
        String tempGetInsertSQL = null;
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            tempGetInsertSQL = NadLDato.GetInsertSQL(/*Fila*/);
        } else {
            //tempGetInsertSQL = NadDato.GetInsertSQL(Fila);
        }
        return tempGetInsertSQL;
    }

    public String GetUpdateSQL(String Fila) {
        String tempGetUpdateSQL = null;
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            tempGetUpdateSQL = NadLDato.GetUpdateSQL(Fila);
        } else {
            //tempGetUpdateSQL = NadDato.GetUpdateSQL(Fila);
        }
        return tempGetUpdateSQL;
    }

    public String GetSQLIdentity(String Tabla, String IdCampo) {
        String tempGetSQLIdentity = null;

        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            tempGetSQLIdentity = NadLDato.GetSQLIdentity(Tabla, IdCampo);
        } else {
//            tempGetSQLIdentity = NadDato.GetSQLIdentity(Tabla, IdCampo);
        }

        return tempGetSQLIdentity;
    }

    public Object GetMaxNumero(String Tabla, String IdCampo, String Filtro) throws Exception {
        return GetMaxNumero(Tabla, IdCampo, Filtro, 1);
    }

    public Object GetMaxNumero(String Tabla, String IdCampo) throws Exception {
        return GetMaxNumero(Tabla, IdCampo, "", 1);
    }

    public Object GetMaxNumero(String Tabla, String IdCampo, String Filtro, int Inicio) throws Exception {
        Object tempGetMaxNumero = null;
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            tempGetMaxNumero = NadLDato.GetMaxNumero(Tabla, IdCampo, Filtro, Inicio);
        }
        return tempGetMaxNumero;
    }

    public int Ejecutar(String Sentencia) throws Exception {
        return Ejecutar(Sentencia, false);
    }

    public int Ejecutar(String Sentencia, boolean Identity) throws Exception {
        boolean tempEjecutar = false;
        try {
            if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {

                NadLDato.Ejecutar(Sentencia); //Identity
                tempEjecutar = true;
                if (!tempEjecutar) {
                    throw new RuntimeException("Error al guardar en base de datos, verifique permisos sobre la carpeta de operacion: " + (new java.io.File(NadLDato.getArchivoConexion())).getParent());
                }
            } else {
                //tempEjecutar = NadDato.Ejecutar(Sentencia, Identity);
            }
        } catch (RuntimeException ex) {
            throw ex;//new TAException(ex.getMessage(), ex);
        }

        return (tempEjecutar) ? 1 : 0;
    }

    public int EjecutarModificacionBD(String Sentencia, boolean Identity) throws Exception {
        boolean tempEjecutar = false;
        try {
            if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
                NadLDato.EjecutarModificacionBD(Sentencia); //Identity
                if (!tempEjecutar) {
                    throw new RuntimeException("Error al guardar en base de datos, verifique permisos sobre la carpeta de operacion: " + (new java.io.File(NadLDato.getArchivoConexion())).getParent());
                }
            } else {
                //tempEjecutar = NadDato.Ejecutar(Sentencia, Identity);
            }
        } catch (RuntimeException ex) {
            throw ex;//new TAException(ex.getMessage(), ex);
        }

        return (tempEjecutar) ? 1 : 0;
    }

    public boolean IniTrans() throws Exception {
        boolean tempIniTrans = false;
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            tempIniTrans = NadLDato.IniTrans();
        } else {
            //tempIniTrans = NadDato.IniTrans();
        }
        return tempIniTrans;
    }

    public void FinTrans() throws Exception {
        FinTrans(true);
    }

    public void FinTrans(boolean Confirmar) throws Exception {
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            NadLDato.FinTrans(Confirmar);
        }
    }

    public boolean ExisteCampo(String Tabla, String Campo) throws Exception {
        boolean tempExisteCampo = false;
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            tempExisteCampo = NadLDato.ExisteCampo(Tabla, Campo);
        }
        return tempExisteCampo;
    }

    public boolean ExisteTabla(String Tabla) throws Exception {
        boolean tempExisteTabla = false;
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            tempExisteTabla = NadLDato.ExisteTabla(Tabla);
        } else {
            //tempExisteTabla = NadDato.ExisteTabla(Tabla);
        }
        return tempExisteTabla;
    }

    public boolean ExisteRestriTabla(String Tabla, String Restriccion) {
        boolean tempExisteRestriTabla = false;
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            tempExisteRestriTabla = false;
        } else {
            //tempExisteRestriTabla = NadDato.ExisteRestriTabla(Tabla, Restriccion);
        }
        return tempExisteRestriTabla;
    }

    public boolean VerificarIntegridadSQLite() throws SQLException, TAException {
        boolean tempVerificarIntegridadSQLite = false;
        try {
            if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
                ResultSet dt = null;
                NadLDato.Conectar();
                NadLDato.IniTrans();
                PreparedStatement a = NadLDato.GetConnection().prepareStatement("pragma quick_check");

                try {
                    dt = a.executeQuery();
                    ResultSetMetaData rm = dt.getMetaData();
                    while (dt.next()) {
                        if (dt.getString(rm.getColumnName(1)).equals("ok")) {
                            tempVerificarIntegridadSQLite = true;
                        }
                    }
                } catch (SQLException ex) {
                } finally {
                    NadLDato.FinTrans(true);
                }
            } else {
                //lo mismo para SQLServer
            }
        } catch (RuntimeException ex) {
            throw new TAException("Error al intentar verificar la integridad de la base de datos: " + ex.getMessage());
        } catch (Exception ex) {
             throw new TAException("Error al intentar verificar la integridad de la base de datos: " + ex.getMessage());
        }
        return tempVerificarIntegridadSQLite;
    }

    public boolean ExisteIndice(String Tabla, String NomIndice) throws Exception {
        boolean tempExisteIndice = false;
        if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
            tempExisteIndice = NadLDato.ExisteIndice(Tabla, NomIndice);
        } else {
            //tempExisteIndice = NadDato.ExisteIndice(Tabla, NomIndice);
        }
        return tempExisteIndice;
    }

    public boolean BorrarTabla(String Tabla) throws TAException, Exception {
        boolean tempBorrarTabla = false;
        try {
            if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
                tempBorrarTabla = NadLDato.BorrarTabla(Tabla);
            } else {
                //tempBorrarTabla = NadDato.BorrarTabla(Tabla);
            }
        } catch (RuntimeException ex) {
            throw ex;//new TAException(ex.getMessage(), ex);
        }
        return tempBorrarTabla;
    }

    public int getRowCount(ResultSet resultSet) {
        int count = 0;
        try {
            if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
                ResultSet aux = resultSet;
                count = NadLDato.getRowCount(aux);
            } else {
                //count = NadDato.getRowCount(resultSet);
            }
        } catch (RuntimeException ex) {
            throw ex;//new TAException(ex.getMessage(), ex);
        }
        return count;
    }

    public void schrinkBD() {
        try {
            if (mvarCxProveedor == TIPO_PROVEEDOR.tpSQLiteClient) {
                NadLDato.schrinkBD();
            } else {
                //tempBorrarTabla = NadDato.BorrarTabla(Tabla);
            }
        } catch (RuntimeException ex) {
            throw ex;//new TAException(ex.getMessage(), ex);
        } catch (SQLException ex) {
            Logger.getLogger(NadMADato.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}