/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DanielCera
 */
public class TAFEApiContingencia {

    private String _rutaArchivo;

    public TAFEApiContingencia(String CarpetaOperacion) {
        _rutaArchivo = CarpetaOperacion + File.separator + "XML"  + File.separator;
        if (!(new File(_rutaArchivo).isDirectory())) {
            new File(_rutaArchivo).mkdir();
        }

        if (!(new File(_rutaArchivo + File.separator + "Contingencia").isDirectory())) {
            new File(_rutaArchivo + File.separator + "Contingencia").mkdir();
        }
    }

    private String getCarpetaArchivo() {
        return _rutaArchivo + File.separator + "Contingencia" + File.separator;
    }

    private String getNombreArchivo(String EmpRut, int SucId, int CajId) {
        return EmpRut + "_" + SucId + "_" + CajId + "_" + new java.util.Date().getYear() + new java.util.Date().getMonth() + new java.util.Date().getDay() + new java.util.Date().getHours() + new java.util.Date().getMinutes() + new java.util.Date().getSeconds() + ".xml";
    }

    public final void Guardar(String texto, String EmpRut, int SucId, int CajId) {
        OutputStreamWriter osw = null;
        String NomArchivo = null;
        try {
            NomArchivo = getNombreArchivo(EmpRut, SucId, CajId);
            osw = new OutputStreamWriter(new FileOutputStream(getCarpetaArchivo() + NomArchivo), Charset.forName("UTF-8").newEncoder());
            PrintWriter writer = new PrintWriter(osw);
            writer.print(texto);
            writer.println();
            osw.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TAFEApiContingencia.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TAFEApiContingencia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public final boolean ObtenerXMLContigencia(tangible.RefObject<String> XML, tangible.RefObject<String> CarpetaArchivo, tangible.RefObject<String> NomArchivo, String EmpRut, int SucId, int CajId) {
        try {
            File dir = new File(_rutaArchivo + File.separator + "Contingencia" + File.separator);
            if (dir.isDirectory()) {
                File[] filesList = dir.listFiles();
                if (filesList.length > 0) {
                    for (File file : filesList) {
                        if (file.isFile()) {
                            BufferedReader br = new BufferedReader(new FileReader(file.getAbsolutePath()));
                            ObtenerCarpetaArchivo(file.getAbsolutePath(), CarpetaArchivo, NomArchivo);
                            try {
                                StringBuilder sb = new StringBuilder();
                                String line = br.readLine();
                                while (line != null) {
                                    sb.append(line);
                                    line = br.readLine();
                                }
                                br.close();
                                //Contenido
                                XML.argValue = sb.toString();
                                File movedFile = new File(CarpetaArchivo.argValue + "_" + NomArchivo.argValue);
                                boolean renamed =  file.renameTo(movedFile);
                                return renamed;
                            } catch (IOException ex) {
                                Logger.getLogger(TAFEApiContingencia.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TAFEApiContingencia.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Si no hay archivos devuelvo false
        return false;
    }

    private void ObtenerCarpetaArchivo(String RutaArchivo, tangible.RefObject<String> CarpetaArchivo, tangible.RefObject<String> NomArchivo) {
        int PosCarp = 0;
        PosCarp = RutaArchivo.lastIndexOf(File.separator);
        CarpetaArchivo.argValue = RutaArchivo.substring(0, PosCarp + 1);
        NomArchivo.argValue = RutaArchivo.substring((PosCarp + 2) - 1);
    }

    public final void Eliminar(String RutaArchivo) {
        (new java.io.File(RutaArchivo)).delete();
    }
}
