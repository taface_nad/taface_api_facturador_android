
package Negocio;

import Negocio.WSInterface.ConsultarReceptorElectronico.WSConsultarReceptorElectronicoExecuteResponse;
import Negocio.WSInterface.EnviarContingencia.WSEnviarContingencia0200ExecuteResponse;
import Negocio.WSInterface.EnviarCteSinRespuesta.WSEnviarCteSinRespuesta0200ExecuteResponse;
import Negocio.WSInterface.FirmarComprobanteConNroCAEReservado.WSFirmarComprobanteConNroCAEReservado0203ExecuteResponse;
import Negocio.WSInterface.LogErrores.WSSincronizacionLogErrores0200ExecuteResponse;
import Negocio.WSInterface.ParamsApi.WSSincronizacionParamsApi0205ExecuteResponse;
import Negocio.WSInterface.ParamsApi.WSSincronizacionParamsApi0206ExecuteResponse;
import Negocio.WSInterface.ReservarNroCAE.WSReservarNroCAE0203ExecuteResponse;
import Negocio.WSInterface.SincronizacionCFC.SDTCFCRequestSDTCFCRequestItem;
import Negocio.WSInterface.SincronizacionCFC.SDTCFCResponseSDTCFCResponseItem;
import Negocio.WSInterface.SincronizacionCFC.WSSincronizacionCFC0200ExecuteResponse;
import TAFACE2ApiEntidad.TADebug;
import TAFACE2ApiEntidad.TAException;
import TAFACE2ApiEntidad.TALog;
import XML.XMLCONFIGURACION;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;
import TAFE2ApiFirmar.TAFirmarFactura;
import XML.XMLCFC;
import XML.XMLFACTURA;
import XML.XMLRESPUESTA;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Writer;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.xml.bind.PropertyException;
import tangible.RefObject;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author DanielCera
 */
public class TAFEApi {

    public static final String NombreServicio = "TAFACE_API_SYNC";
    private boolean Inicializado = false;
    private boolean EmularGateway = false;
    private XMLCONFIGURACION objConfig;
    private TADebug objDebug = new TADebug();
    private String WarningMsg = "";
    private static final int TimeOutSincronizacion = 40;
    private static final int MaxMinutosSinSincronizar = 30;
    private ExecutorWS executorWS = new ExecutorWS();

    private enum TipoEncriptacion {

        TextoPlano(1),
        NADEncriptar(2),
        TripleDES(3);
        private int intValue;
        private static java.util.HashMap<Integer, TipoEncriptacion> mappings;

        private static java.util.HashMap<Integer, TipoEncriptacion> getMappings() {
            if (mappings == null) {
                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, TipoEncriptacion>();
                    }
            }
            return mappings;
        }

        private TipoEncriptacion(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static TipoEncriptacion forValue(int value) {
            return getMappings().get(value);
        }
    }

    private enum ModoFirma {

        ServicioWeb(1),
        FirmaLocal(2);
        private int intValue;
        private static java.util.HashMap<Integer, ModoFirma> mappings;

        private static java.util.HashMap<Integer, ModoFirma> getMappings() {
            if (mappings == null) {
                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, ModoFirma>();
                    }
            }
            return mappings;
        }

        private ModoFirma(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static ModoFirma forValue(int value) {
            return getMappings().get(value);
        }
    }

    private enum TIPO_PROVEEDOR {

        tpSQLClient(1),
        tpSQLiteClient(3);
        private int intValue;
        private static java.util.HashMap<Integer, TIPO_PROVEEDOR> mappings;

        private static java.util.HashMap<Integer, TIPO_PROVEEDOR> getMappings() {
            if (mappings == null) {
                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, TIPO_PROVEEDOR>();
                    }
            }
            return mappings;
        }

        private TIPO_PROVEEDOR(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static TIPO_PROVEEDOR forValue(int value) {
            return getMappings().get(value);
        }
    }
    //Variables de Seteo
    private String cfgUrlServidor;
    private int cfgSegundosTimeout;
    private String cfgCarpetaOperacion = "";
    private ModoFirma cfgModoFirma = ModoFirma.ServicioWeb;
    private TIPO_PROVEEDOR cfgMotorBD = TIPO_PROVEEDOR.tpSQLClient;
    private short cfgLoteSincronizacion;
    private java.math.BigDecimal cfgTopeUIPlaza = new java.math.BigDecimal(0);
    private java.math.BigDecimal cfgTopeUIFreeshop = new java.math.BigDecimal(0);
    private String sPuntoConexionBD = "";
    private int nTipoDeNegocio;
    private int nHorasSinConexion = 0;
    private String EmpRUT;
    private int SucId;
    private int CajId;


    private void LimpiarVariables() {
        cfgUrlServidor = "";
        cfgSegundosTimeout = 40;
        cfgLoteSincronizacion = 0;
        cfgCarpetaOperacion = "";
        cfgModoFirma = ModoFirma.ServicioWeb;
        sPuntoConexionBD = "";
        nTipoDeNegocio = 1;
        EmpRUT = "";
        SucId = 0;
        CajId = 0;
        WarningMsg = "";
        objConfig = null;
    }

    public boolean Inicializar(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String UrlServidorTAFirmoGateway, int SegundosTimeout, long EmpresaRUT, int SucursalId, int CajaId) throws TAException {
        return Inicializar(ErrorMsg, ErrorCod, UrlServidorTAFirmoGateway, SegundosTimeout, EmpresaRUT, SucursalId, CajaId, "");
    }

    public  boolean Inicializar(RefObject<String> ErrorMsg, RefObject<Integer> ErrorCod, String UrlServidorTAFirmoGateway, int SegundosTimeout, long EmpresaRUT, int SucursalId, int CajaId, String RutaCarpetaOperacion) throws TAException {
        try {
            ErrorMsg.argValue = "";
            ErrorCod.argValue = 0;

            if (EmpresaRUT == 0) {
                throw new TAException("Debe ingresar el RUT de la empresa.", 4178);
            }
            if (SucursalId == 0) {
                throw new TAException("Debe ingresar el numero de sucursal.", 4179);
            }
            if (CajaId == 0) {
                throw new TAException("Debe ingresar el numero de la caja.", 4180);
            }
            LimpiarVariables();

            EmpRUT = String.format("%012d", EmpresaRUT).replace(' ', '0');
            SucId = SucursalId;
            CajId = CajaId;

            if (EmularGateway) {
                Inicializado = true;
                return true;
            }
            ErrorMsg.argValue = "";
            ErrorCod.argValue = 0;
            RutaCarpetaOperacion = RutaCarpetaOperacion + "";
            if (RutaCarpetaOperacion.trim().equals("")) {
                throw new TAException("Debe inicializar la API con la ruta de acceso correctamente");//cfgCarpetaOperacion = System.AppDomain.CurrentDomain.BaseDirectory;
            } else {
                if (!RutaCarpetaOperacion.substring(RutaCarpetaOperacion.length() - 1).equals(File.separator) && !RutaCarpetaOperacion.substring(RutaCarpetaOperacion.length() - 1).equals(File.separator)) {
                    RutaCarpetaOperacion = RutaCarpetaOperacion + File.separator;
                }
                cfgCarpetaOperacion = "";
                cfgCarpetaOperacion = RutaCarpetaOperacion.toString();
            }
            objDebug.setRutaCarpetaDebug(cfgCarpetaOperacion);
            //***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
            Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "String UrlServidorTAFirmoGateway", "int SegundosTimeout", "long EmpresaRUT", "int SucursalId", "int CajaId", "String RutaCarpetaOperacion"};
            Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, UrlServidorTAFirmoGateway, SegundosTimeout, EmpresaRUT, SucursalId, CajaId, RutaCarpetaOperacion};
            objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

            if (UrlServidorTAFirmoGateway.trim().equals("")) {
                throw new TAException("Debe ingresar el URL del Servidor de TAFACE", 1032);
            } else {
                if (!UrlServidorTAFirmoGateway.substring(UrlServidorTAFirmoGateway.length() - 1).equals("/")) {
                    UrlServidorTAFirmoGateway += "/";
                }
                cfgUrlServidor = UrlServidorTAFirmoGateway;
                cfgSegundosTimeout = SegundosTimeout;
                executorWS.setURLServer(cfgUrlServidor);
            }

            //Carga la configuracion del archivo TAFEApi.config que se encuentra en la carpeta de operacion.
            objConfig = new XMLCONFIGURACION();
            objConfig = objConfig.CargarConfiguracion(cfgCarpetaOperacion, EmpRUT, SucursalId);

            //A este If entra solamente una vez en toda la vida
            if ((objConfig == null)) {
                //Despierta al server
                try {
                    WakeUpServer();
                } catch (RuntimeException ex) {
                    throw new TAException("Error descargando config. inicial: Servidor No Disponible, para operar Contingencia debe conectarse al menos una vez." + ex.getMessage());
                }

                //Si no logra cargar el archivo lo descarga del servidor y lo guarda localmente.
                //aca tengo que pasarle tb el uniqueId al gateway para que ya lo setee en la BD y asocie la caja con la MAC, ademas la licencia de uso de la empresa

                WSSincronizacionParamsApi0205ExecuteResponse Conf = null;
                    Conf = DescargarConfiguracionAPI(EmpRUT, SucId, CajId);

                    objConfig = new XMLCONFIGURACION();
                    if ((Conf == null)) {
                        throw new RuntimeException("Error descargando config. inicial: Servidor retorno nothing, para operar debe conectarse al menos una vez.");
                    }
                    if (Conf.getPerrormessage().equals("")) {
                        objConfig.setMODOFIRMA(Conf.getPsdtparamtafeapi().getModoFirma());
                        objConfig.setTIPOENCRIPTACION(Conf.getPsdtparamtafeapi().getTipoEncriptacion());
                        objConfig.setTIPOPROVEEDORSQL(Conf.getPsdtparamtafeapi().getTipoProveedorSQL());
                        objConfig.setTOPEUIFREESHOP(Conf.getPsdtparamtafeapi().getTopeUIFreeShop());
                        objConfig.setTOPEUIPLAZA(Conf.getPsdtparamtafeapi().getTopeUIPlaza());
                        objConfig.setVALIDARCFC((short) (Conf.getPsdtparamtafeapi().isValidarCFC() ? 1 : 0));
                        objConfig.setLOTESINCRONIZACION(Conf.getPsdtparamtafeapi().getLoteSincronizacion());
                        objConfig.setHORASSINCONEXION((short) 0);
                        objConfig.setCAESPORCAVANCE(Conf.getPsdtparamtafeapi().getCAEsPorcAvance());
                        objConfig.setCANTDIASANTESCAEVENCER(Conf.getPsdtparamtafeapi().getCantDiasAntesCAEVencer());
                        objConfig.setCANTDIASANTESCERTIFICADOVENCER(Conf.getPsdtparamtafeapi().getCantDiasAntesCertificadoVencer());
                        objConfig.setMINDATOSCAJASINSINC(Conf.getPsdtparamtafeapi().getMinDatosCajaSinSinc());
                        objConfig.setGUID(Conf.getPsdtparamtafeapi().getParamGUID());
                        objConfig.setENCRIPTARCOMPLEMENTOFISCAL((short) (Conf.getPsdtparamtafeapi().isEncriptarComplementoFiscal() ? 1 : 0));
                        objConfig.setMAXIMOTIEMPOTAREAPROGRAMADA(30);
                        objConfig.setMINUTOSCADUCIDADRESERVACAE(Conf.getPsdtparamtafeapi().getMinutosCaducidadReservaCAE());
                        objConfig.setLARGOMINIMOTICKET(Conf.getPsdtparamtafeapi().getLargoMinimoTicket());
                        objConfig.setABRIRCAJON((short) (Conf.getPsdtparamtafeapi().isAperturaDeCajon() ? 1 : 0));
                        objConfig.setNOVALIDARFECHAFIRMA((short) (Conf.getPsdtparamtafeapi().isNoValidarFechaFirma() ? 1 : 0));
                        objConfig.setDIASFIRMARRETROACTIVO(objConfig.getDIASFIRMARRETROACTIVO());
                        objConfig.setELIMINARCTESINCRONIZADODIAS(objConfig.getELIMINARCTESINCRONIZADODIAS());
                        //Si TipoSQL es SQLServer y firma en caja, creo el punto de conexion que deberia venir en -> 'Conf.CadenaCnxSQL
                        if (Conf.getPsdtparamtafeapi().getTipoProveedorSQL() == 1 && Conf.getPsdtparamtafeapi().getModoFirma() == 2) {
                            //implementacion de la conexion a sql server
                        }

                        objConfig.GuardarConfig(cfgCarpetaOperacion, EmpRUT, SucursalId);
                        objConfig = objConfig.CargarConfiguracion(cfgCarpetaOperacion, EmpRUT, SucursalId);
                        if ((objConfig == null)) {
                            throw new RuntimeException("Error al leer archivo de configuracion, verificar permisos sobre la carpeta de operacion.");
                        }
                    }
                 else {
                    throw new TAException("No se encuentra fichero de licencia para la autenticación con el servidor. Antes de realizar la primera sincronización debe proveer una licencia válida.");
                }
            }
            //Seteo desde integrador                        
            cfgLoteSincronizacion = objConfig.getLOTESINCRONIZACION();
            cfgTopeUIPlaza = new BigDecimal(objConfig.getTOPEUIPLAZA());
            cfgTopeUIFreeshop = new BigDecimal(objConfig.getTOPEUIFREESHOP());
            cfgModoFirma = ModoFirma.forValue(objConfig.getMODOFIRMA());
            cfgMotorBD = TIPO_PROVEEDOR.forValue(objConfig.getTIPOPROVEEDORSQL());
            nHorasSinConexion = objConfig.getHORASSINCONEXION();
            if (cfgMotorBD == TIPO_PROVEEDOR.tpSQLiteClient) {
                sPuntoConexionBD = cfgCarpetaOperacion + "TAFACEApi.db";
            } else {
                sPuntoConexionBD = cfgCarpetaOperacion + EmpRUT + "_" + SucId + "_TAFEApi.conexion";
            }

            //Si el archivo de config no tenia seteado el maximo tiempo que se espera por la sincronizacion de la tarea programada le seteo 30 minutos
            if (objConfig.getMAXIMOTIEMPOTAREAPROGRAMADA() == 0) {
                objConfig.setMAXIMOTIEMPOTAREAPROGRAMADA(30);
            }

            if (objConfig.getMINUTOSCADUCIDADRESERVACAE() == 0) {
                objConfig.setMINUTOSCADUCIDADRESERVACAE(1440);
            }

            //si el flag esta en 0 elimino los cte sincronizados
            if (objConfig.getSYNC().compareTo(objConfig.getFECHASISTEMA()) != 0) {
                TAFirmarFactura DLLFirmarFactura = new TAFirmarFactura();
                DLLFirmarFactura.setRutaConexion(sPuntoConexionBD);
                DLLFirmarFactura.setProveedorConexion(cfgMotorBD.getValue());
                DLLFirmarFactura.LimpiarComprobantesSincronizadosDias(cfgUrlServidor, (short) 105, EmpRUT, SucId, CajId, objConfig.getELIMINARCTESINCRONIZADODIAS(), ErrorMsg);

                objConfig.setSYNC(objConfig.getFECHASISTEMA());
                objConfig.setsRutaArchivoConfiguracion("");
                objConfig.GuardarConfig(cfgCarpetaOperacion, EmpRUT, SucursalId);
            }
            Inicializado = true;
        } catch (Exception ex) {
            Inicializado = false;

            ErrorCod.argValue = 8;

            ErrorMsg.argValue = "Error inicializando: " + ex.getMessage();
            TALog objLog = new TALog(cfgCarpetaOperacion, String.valueOf(EmpresaRUT), SucursalId, CajaId);
            if (ErrorCod.argValue == 9) {
                try {
                    objLog.EscribirLog("Error inicializando: " + ex.getMessage(), TAFACE2ApiEntidad.TALog.enumTipoLog.LogErrorDeConexion);
                } catch (UnsupportedEncodingException ex1) {
                    Logger.getLogger(TAFEApi.class.getName()).log(Level.SEVERE, null, ex1);
                }
            } else {
                try {
                    objLog.EscribirLog("Error inicializando: " + ex.getMessage(), TAFACE2ApiEntidad.TALog.enumTipoLog.LogError);
                } catch (UnsupportedEncodingException ex1) {
                    throw new TAException(ex1.getMessage());
                }
            }
        }
        return Inicializado;
    }

    public  boolean FirmarFactura(RefObject<String> ErrorMsg, RefObject<Integer> ErrorCod, String XMLFactura, RefObject<String> XMLRespuesta, long CAENroAutorizacion, String CAESerie, long CAENroReservado) throws TAException {

        boolean Firmar = false;
        try {
            ErrorMsg.argValue = "";
            ErrorCod.argValue = 0;
            if (!EmularGateway) {
                /* ***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.*** */
                Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "String XMLFactura", "tangible.RefObject<String> XMLRespuesta", "long CAENroAutorizacion", "String CAESerie", "long CAENroReservado"};
                Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, XMLFactura, XMLRespuesta.argValue, CAENroAutorizacion, CAESerie, CAENroReservado};
                objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);
            }
            XMLFACTURA objFactura = new XMLFACTURA();
            tangible.RefObject<Boolean> FirmadoOk = new RefObject<Boolean>(false);
            boolean ErrorReturn = true;
            //Verifico que se haya inicializado el componente
            if (!Inicializado)
                throw new TAException("Debe Inicializar la API primero invocando al metodo Inicializar", 4131);

            XMLRespuesta.argValue = "";
            objFactura = objFactura.LoadXML(XMLFactura);

            //Obtengo tipo de negocio
            nTipoDeNegocio = objFactura.getDATOSADICIONALES().getTIPODENEGOCIO();
            //Controlo que se haya pasado datos de factura original
            if ((objFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALTIPO() == null || objFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALTIPO().trim().length() == 0)) {
                throw new TAException("Tipo de documento original es obligatorio.", 1027);
            }
            if ((objFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALSERIE() == null || objFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALSERIE().trim().length() == 0)) {
                throw new TAException("Serie de documento original es obligatorio.", 1028);
            }
            if (objFactura.getDATOSADICIONALES().getDOCUMENTOORIGINALNRO() <= (long) 0) {
                throw new TAException("Numero de documento original es obligatorio.", 1029);
            }
            if (ExistenContingPendSincronizar(objConfig.getMINDATOSCAJASINSINC())) {
                if (!WarningMsg.trim().equals("")) WarningMsg += "\r\n";
                WarningMsg += "***ATENCIÓN***: Existen comprobantes de contingencia pendientes de envío, verifique la conexion con el servidor.";
            }
            if (ExistenSinRespPendSincronizar(objConfig.getMINDATOSCAJASINSINC())) {
                if (!WarningMsg.trim().equals("")) {
                    WarningMsg += "\r\n";
                }
                WarningMsg += "***ATENCIÓN***: Existen Archivos SinRespuesta pendientes de envío, verifique la conexion con el servidor.";
            }

            //antes de entrar a la firma voy a comprobar si es eBoleta y el RUC del receptor es emisor electronico lanzo error
            //Valido que si es eBoleta el RUC del receptor no puede ser el de un emisor electronico
            if (objFactura.EsBoleta()) {
                if (objFactura.getDGI().getRECTipoDocRecep() == 2)
                    if (ConsultaRUCEmisorElectronico(objFactura.getDGI().getRECDocRecep()))
                        throw new TAException("No se realizo la firma de la eBoleta, el RUC del receptor corresponde a un emisor electronico, por lo que no es necesario emitir eBoleta");
            }

            if (!objFactura.EsContingencia()) {
                if (EmularGateway) {
                    //Emulador de Gateway
                    EmuladorFirmarFacturaOK(ErrorMsg.argValue, ErrorCod, XMLRespuesta);
                } else {
                    //Envio facturas de contingencia y sin respuesta sincronico con la firma 
                    //para evitar que el comprobante ya se encuentre en el server
                    if (!(EnviarContingencia(ErrorMsg, ErrorCod, false))) {
                        throw new TAException(ErrorMsg.argValue, 3615);
                    }
                    if (cfgModoFirma == ModoFirma.FirmaLocal) {
                        TAFirmarFactura DLLFirmarFactura = new TAFirmarFactura();
                        DLLFirmarFactura.setRutaConexion(sPuntoConexionBD);
                        DLLFirmarFactura.setProveedorConexion(cfgMotorBD.getValue());
                        String LicenciaId = CajaLicenseId();
                        //Paso a comprobar la fecha de actualizacion del fichero de configuracion de la sucursal, si coincide con la fecha del PC quiere decir que la
                        //sincronizacion esta corriendo, de lo contrario se asume fecha del PC
                        boolean ComprobarDiffFechaSistema = false;
                        if (HayDiffUltimaModificacionConfigSuc(cfgCarpetaOperacion,EmpRUT,SucId)) {
                            ComprobarDiffFechaSistema = true;
                        }

                        Object[] DebugNamesArray = {"XMLFactura", "LicenciaId", "XMLRespuesta", "FirmadoOk", "SucId", "CajId", "VersionActual()", "cfgUrlServidor", "(short) TimeOutSincronizacion", "cfgTopeUIFreeshop", "cfgTopeUIPlaza", "objConfig.getCANTDIASANTESCAEVENCER()", "objConfig.getCANTDIASANTESCERTIFICADOVENCER()", "objConfig.getCAESPORCAVANCE()", "objConfig.getMINDATOSCAJASINSINC()", "objConfig.getENCRIPTARCOMPLEMENTOFISCAL()", "CAENroAutorizacion", "CAESerie", "CAENroReservado", "ErrorCod", "objConfig.getFECHASISTEMA()", "objConfig.getNOVALIDARFECHAFIRMA()", "objConfig.getDIASFIRMARRETROACTIVO()", "ComprobarDiffFechaSistema"};
                        Object[] DebugValuesArray = {XMLFactura, LicenciaId, XMLRespuesta, FirmadoOk, SucId, CajId, VersionActual(), cfgUrlServidor, (short) TimeOutSincronizacion, cfgTopeUIFreeshop, cfgTopeUIPlaza, objConfig.getCANTDIASANTESCAEVENCER(), objConfig.getCANTDIASANTESCERTIFICADOVENCER(), objConfig.getCAESPORCAVANCE(), objConfig.getMINDATOSCAJASINSINC(), objConfig.getENCRIPTARCOMPLEMENTOFISCAL(), CAENroAutorizacion, CAESerie, CAENroReservado, ErrorCod, objConfig.getFECHASISTEMA(), objConfig.getNOVALIDARFECHAFIRMA(), objConfig.getDIASFIRMARRETROACTIVO(), ComprobarDiffFechaSistema};
                        objDebug.EscribirDebug("TAFactura.FirmarFactura", DebugNamesArray, DebugValuesArray);

                        ErrorMsg.argValue = DLLFirmarFactura.FirmarFactura(XMLFactura, LicenciaId, XMLRespuesta, FirmadoOk, SucId, CajId, VersionActual(), cfgUrlServidor, (short) TimeOutSincronizacion, cfgTopeUIFreeshop, cfgTopeUIPlaza, objConfig.getCANTDIASANTESCAEVENCER(), objConfig.getCANTDIASANTESCERTIFICADOVENCER(), objConfig.getCAESPORCAVANCE(), objConfig.getMINDATOSCAJASINSINC(), objConfig.getENCRIPTARCOMPLEMENTOFISCAL(), CAENroAutorizacion, CAESerie, CAENroReservado, ErrorCod, objConfig.getFECHASISTEMA(), objConfig.getNOVALIDARFECHAFIRMA(), objConfig.getDIASFIRMARRETROACTIVO(), ComprobarDiffFechaSistema);

                        if (!FirmadoOk.argValue) {
                            TAFACE2ApiEntidad.TALog objLog = new TAFACE2ApiEntidad.TALog(cfgCarpetaOperacion, EmpRUT, SucId, CajId);
                            if (ErrorCod.argValue == 9) {
                                objLog.EscribirLog(ErrorCod.argValue + " - " + ErrorMsg.argValue, TAFACE2ApiEntidad.TALog.enumTipoLog.LogErrorDeConexion);
                            } else {
                                objLog.EscribirLog(ErrorCod.argValue + " - " + ErrorMsg.argValue, TAFACE2ApiEntidad.TALog.enumTipoLog.LogError);
                            }
                            throw new TAException(ErrorMsg.argValue, 6001);
                        }
                    } else {
                        WakeUpServer();
                        try {
                            //Aca hago el switch entre si firma sin reserva o me pasan el Nro de CAE por parametro
                            if (CAENroReservado == 0) {
                                String LicenciaId = CajaLicenseId();
                                ErrorReturn = executorWS.executeFirmarComprobante((short) CajId, LicenciaId, VersionActual(), XMLFactura, EmpRUT, SucId).isPerrorreturn();
                            } else {
                                WSFirmarComprobanteConNroCAEReservado0203ExecuteResponse WSFirmarComprobanteConNroCAEReservado = new WSFirmarComprobanteConNroCAEReservado0203ExecuteResponse();
                                String LicenciaId = CajaLicenseId();
                                WSFirmarComprobanteConNroCAEReservado = executorWS.executeFirmarComprobanteConNroCAEReservado(CAENroAutorizacion, (int) CAENroReservado, CAESerie, (short) CajId, LicenciaId, VersionActual(), XMLFactura, EmpRUT, SucId);
                                ErrorReturn = WSFirmarComprobanteConNroCAEReservado.isPerrorreturn();
                            }

                            if (ErrorReturn) {
                                FirmadoOk.argValue = false;
                                throw new TAException(ErrorMsg.argValue, 1023);
                            } else {
                                //Envio Confirmacion de Respuesta
                                ErrorReturn = executorWS.executeConfirmarfirmacomprobante((short) CajId, XMLFactura, EmpRUT, SucId).isPerrorreturn();
                                if (ErrorReturn) {
                                    FirmadoOk.argValue = false;
                                    throw new TAException(ErrorMsg.argValue, 1024);
                                }
                            }
                        } catch (TAException taex) {
                            try {
                                //Grabo XML en la carpeta de los Sin Respuesta
                                TAFEApiSinRespuesta objSRE = new TAFEApiSinRespuesta(cfgCarpetaOperacion);
                                objSRE.Guardar(XMLFactura, EmpRUT, SucId, CajId);
                            } catch (RuntimeException ex2) {
                                //Si Sale por excepcion:
                                //Grabo la excepcion en el LOG
                                TAFACE2ApiEntidad.TALog objLog = new TAFACE2ApiEntidad.TALog(cfgCarpetaOperacion, EmpRUT, SucId, CajId);
                                objLog.EscribirLog("Error guardando xml en carpeta SinRespuesta: " + ex2.toString(), TAFACE2ApiEntidad.TALog.enumTipoLog.LogError);
                            }
                            throw new TAException("Error durante la firma del comprobante mediante servicios web: " + taex.getMessage(), taex, 3101);
                        } catch (RuntimeException ex) {
                            try {
                                //Grabo XML en la carpeta de los Sin Respuesta
                                TAFEApiSinRespuesta objSRE = new TAFEApiSinRespuesta(cfgCarpetaOperacion);
                                objSRE.Guardar(XMLFactura, EmpRUT, SucId, CajId);
                            } catch (RuntimeException ex2) {
                                //Si Sale por excepcion:
                                //Grabo la excepcion en el LOG
                                TAFACE2ApiEntidad.TALog objLog = new TAFACE2ApiEntidad.TALog(cfgCarpetaOperacion, EmpRUT, SucId, CajId);
                                objLog.EscribirLog("Error guardando xml en carpeta SinRespuesta: " + ex2.toString(), TAFACE2ApiEntidad.TALog.enumTipoLog.LogError);
                            }
                            throw new TAException("Error durante la firma del comprobante mediante servicios web: " + ex.getMessage(), 6000);
                        }
                    }
                }
                if (!WarningMsg.trim().equals("")) {
                    XMLRESPUESTA XMLRespTemp = new XMLRESPUESTA();
                    XMLRespTemp = XMLRespTemp.LoadXML(XMLRespuesta.argValue);
                    if (!((XMLRespTemp.getWARNINGMSG() == null)) && !XMLRespTemp.getWARNINGMSG().trim().equals("")) {
                        XMLRespTemp.setWARNINGMSG(XMLRespTemp.getWARNINGMSG() + "\r\n");
                    }
                    XMLRespTemp.setWARNINGMSG(XMLRespTemp.getWARNINGMSG() + WarningMsg);
                    XMLRespuesta.argValue = XMLRespTemp.ToXML();
                }
            } else {
                //Si el tipo de documento es de contingencia no intento enviar
                if (!(ExisteCFC(ErrorMsg, ErrorCod, objFactura.getDATOSCONTINGENCIA().getTIPOCFE(), objFactura.getDATOSCONTINGENCIA().getSERIE(), objFactura.getDATOSCONTINGENCIA().getNRO(), objFactura.getDATOSCONTINGENCIA().getCAENROAUTORIZACION(), objFactura.getDATOSCONTINGENCIA().getCAENRODESDE(), objFactura.getDATOSCONTINGENCIA().getCAENROHASTA(), objFactura.getDATOSCONTINGENCIA().getCAEVENCIMIENTO()))) {
                    throw new TAException("No se pudo verificar el Comprobante de contingencia: " + "\r\n" + ErrorMsg.argValue, 1034);
                }
                //Grabo directamente en el disco el XML
                //Grabo XML en la carpeta de los comprobantes en Contingencia
                TAFEApiContingencia objCNT = new TAFEApiContingencia(cfgCarpetaOperacion);
                objCNT.Guardar(XMLFactura, EmpRUT, SucId, CajId);
                //Armo XML de Respuesta con datos recibidos de contingencia
                XMLRESPUESTA XMLRespContingencia = new XMLRESPUESTA();
                java.util.Date FechaHoraActual = new java.util.Date();
                XMLRespContingencia.setFIRMADOOK((short) 1);
                XMLRespContingencia.setWARNINGMSG(WarningMsg);
                XMLRespContingencia.setFIRMADOFCHHORA(convertirFechaXMLSincronizar(FechaHoraActual));
                XMLRespContingencia.setCAENA(objFactura.getDATOSCONTINGENCIA().getCAENROAUTORIZACION());
                XMLRespContingencia.setCAENROINICIAL(objFactura.getDATOSCONTINGENCIA().getCAENRODESDE());
                XMLRespContingencia.setCAENROFINAL(objFactura.getDATOSCONTINGENCIA().getCAENROHASTA());
                XMLRespContingencia.setCAEVENCIMIENTO(objFactura.getDATOSCONTINGENCIA().getCAEVENCIMIENTO());
                XMLRespContingencia.setCAESERIE(objFactura.getDATOSCONTINGENCIA().getSERIE());
                XMLRespContingencia.setCAENRO(objFactura.getDATOSCONTINGENCIA().getNRO());
                XMLRespContingencia.setCODSEGURIDAD("######");
                XMLRespContingencia.setURLPARAVERIFICARTEXTO("");
                XMLRespContingencia.setRESOLUCIONIVA("");
                XMLRespContingencia.setURLPARAVERIFICARQR("");
                XMLRespuesta.argValue = XMLRespContingencia.ToXML();
            }
            Firmar = true;
        } catch (Exception ex) {
            ErrorCod.argValue = 2;
            ErrorMsg.argValue = ex.getMessage();
            TAFACE2ApiEntidad.TALog objLog = new TAFACE2ApiEntidad.TALog(cfgCarpetaOperacion, EmpRUT, SucId, CajId);
            if (ErrorCod.argValue == 9) {
                try {
                    objLog.EscribirLog(ex.toString() + " - " + getNombreMetodo(), TAFACE2ApiEntidad.TALog.enumTipoLog.LogErrorDeConexion);
                } catch (UnsupportedEncodingException ex1) {
                    Logger.getLogger(TAFEApi.class.getName()).log(Level.SEVERE, null, ex1);
                }
            } else {
                try {
                    objLog.EscribirLog(ex.toString() + " - " + getNombreMetodo(), TAFACE2ApiEntidad.TALog.enumTipoLog.LogError);
                } catch (UnsupportedEncodingException ex1) {
                    Logger.getLogger(TAFEApi.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
            Firmar = false;
        }
        return Firmar;
    }

    private boolean EnviarContingencia(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, boolean EnviarSinRespuestaConDelay) throws FileNotFoundException {


        boolean EnviaContingencia = false;
        String RutaCarpXMLCNT = "";
        String NomArchivoXMLCNT = "";
        String RutaCarpXMLSRE = "";
        String NomArchivoXMLSRE = "";
        try {
            Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "boolean EnviarSinRespuestaConDelay"};
            Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue};
            objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

            //Envio al gateway todos los XML Sin Respuesta
            String XMLFACTURA = "";
            tangible.RefObject<Boolean> EnviarOk = new RefObject<Boolean>(false);
            if (cfgModoFirma == ModoFirma.ServicioWeb) {
                TAFEApiSinRespuesta objSRE = new TAFEApiSinRespuesta(cfgCarpetaOperacion);
                tangible.RefObject<String> tempRef_XMLFACTURA = new tangible.RefObject<String>(XMLFACTURA);
                tangible.RefObject<String> tempRef_RutaCarpXMLSRE = new tangible.RefObject<String>(RutaCarpXMLSRE);
                tangible.RefObject<String> tempRef_NomArchivoXMLSRE = new tangible.RefObject<String>(NomArchivoXMLSRE);
                boolean tempVar = objSRE.ObtenerXMLSinRespuesta(tempRef_XMLFACTURA, tempRef_RutaCarpXMLSRE, tempRef_NomArchivoXMLSRE, EmpRUT, SucId, CajId, EnviarSinRespuestaConDelay);
                XMLFACTURA = tempRef_XMLFACTURA.argValue;
                RutaCarpXMLSRE = tempRef_RutaCarpXMLSRE.argValue;
                NomArchivoXMLSRE = tempRef_NomArchivoXMLSRE.argValue;
                while (tempVar) {

                    WakeUpServer();
                    //Envio al gateway                    
                    if ((cfgUrlServidor != null && cfgUrlServidor.length() > 0) && !cfgUrlServidor.substring(cfgUrlServidor.length() - 1).equals("/")) {
                        cfgUrlServidor = cfgUrlServidor + "/";
                    }
                    boolean ErrorReturn = false;
                    WSEnviarCteSinRespuesta0200ExecuteResponse resp = executorWS.executeEnviarCteSinRespuesta((short) CajId, XMLFACTURA, EmpRUT, SucId);
                    ErrorReturn = resp.isPerrorreturn();
                    if (ErrorReturn) {
                        ErrorMsg.argValue += resp.getPerrormessage();
                        EnviarOk.argValue = false;
                    } else {
                        EnviarOk.argValue = true;
                    }

                    if (EnviarOk.argValue == false) {
                        throw new TAException("Error al enviar comprobantes sin respuesta: " + ErrorMsg.argValue, 4130);
                    }
                    //Si envioOK borro el XML
                    objSRE.Eliminar(RutaCarpXMLSRE + "_" + NomArchivoXMLSRE);
                    tangible.RefObject<String> tempRef_XMLFACTURA2 = new tangible.RefObject<String>(XMLFACTURA);
                    tangible.RefObject<String> tempRef_RutaCarpXMLSRE2 = new tangible.RefObject<String>(RutaCarpXMLSRE);
                    tangible.RefObject<String> tempRef_NomArchivoXMLSRE2 = new tangible.RefObject<String>(NomArchivoXMLSRE);
                    tempVar = objSRE.ObtenerXMLSinRespuesta(tempRef_XMLFACTURA2, tempRef_RutaCarpXMLSRE2, tempRef_NomArchivoXMLSRE2, EmpRUT, SucId, CajId, EnviarSinRespuestaConDelay);
                    XMLFACTURA = tempRef_XMLFACTURA2.argValue;
                    RutaCarpXMLSRE = tempRef_RutaCarpXMLSRE2.argValue;
                    NomArchivoXMLSRE = tempRef_NomArchivoXMLSRE2.argValue;
                }
            }
            RutaCarpXMLCNT = "";
            NomArchivoXMLCNT = "";

            //Envio al gateway todos los XML de contingencia
            TAFEApiContingencia objCNT = new TAFEApiContingencia(cfgCarpetaOperacion);
            tangible.RefObject<String> tempRef_XMLFACTURA3 = new tangible.RefObject<String>(XMLFACTURA);
            tangible.RefObject<String> tempRef_RutaCarpXMLCNT = new tangible.RefObject<String>(RutaCarpXMLCNT);
            tangible.RefObject<String> tempRef_NomArchivoXMLCNT = new tangible.RefObject<String>(NomArchivoXMLCNT);
            while (objCNT.ObtenerXMLContigencia(tempRef_XMLFACTURA3, tempRef_RutaCarpXMLCNT, tempRef_NomArchivoXMLCNT, EmpRUT, SucId, CajId)) {

                XMLFACTURA = tempRef_XMLFACTURA3.argValue;
                RutaCarpXMLCNT = tempRef_RutaCarpXMLCNT.argValue;
                NomArchivoXMLCNT = tempRef_NomArchivoXMLCNT.argValue;
                if (cfgModoFirma == ModoFirma.ServicioWeb) {
                    WakeUpServer();
                    //Envio al gateway                    
                    if ((cfgUrlServidor != null && cfgUrlServidor.length() > 0) && !cfgUrlServidor.substring(cfgUrlServidor.length() - 1).equals("/")) {
                        cfgUrlServidor = cfgUrlServidor + "/";
                    }
                    String LicenciaId = CajaLicenseId();
                    boolean ErrorReturn = false;
                    WSEnviarContingencia0200ExecuteResponse resp = executorWS.executeEnviarContingencia((short) CajId, LicenciaId, VersionActual(), XMLFACTURA, EmpRUT, SucId);
                    ErrorReturn = resp.isPerrorreturn();
                    if (ErrorReturn) {
                        ErrorMsg.argValue += resp.getPerrormessage();
                        EnviarOk.argValue = false;
                    } else {
                        EnviarOk.argValue = true;
                    }
                } else {
                    //Envio a la BD
                    TAFE2ApiFirmar.TAFirmarFactura objEnviarContingencia = new TAFE2ApiFirmar.TAFirmarFactura();
                    String LicenciaId = CajaLicenseId();
                    objEnviarContingencia.setRutaConexion(sPuntoConexionBD);
                    objEnviarContingencia.setProveedorConexion(cfgMotorBD.getValue());
                    ErrorMsg.argValue += objEnviarContingencia.EnviarContingencia(XMLFACTURA, LicenciaId, EnviarOk, SucId, CajId, VersionActual(), cfgUrlServidor, (short) TimeOutSincronizacion, cfgTopeUIFreeshop, cfgTopeUIPlaza, objConfig.getCANTDIASANTESCERTIFICADOVENCER(), ErrorCod);
                }
                if (EnviarOk.argValue == false) {
                    throw new TAException("Error al enviar contingencia: " + ErrorMsg.argValue, 4129);
                }
                //Si envioOK borro el XML
                objCNT.Eliminar(RutaCarpXMLCNT + "_" + NomArchivoXMLCNT);
            }
            EnviaContingencia = true;
        } catch (RuntimeException ex) {
            if ((new java.io.File(RutaCarpXMLCNT + "_" + NomArchivoXMLCNT)).isFile()) {
                File a = new File(RutaCarpXMLCNT + "_" + NomArchivoXMLCNT);
                a.renameTo(new File(RutaCarpXMLCNT + NomArchivoXMLCNT));
            }
            if ((new java.io.File(RutaCarpXMLSRE + "_" + NomArchivoXMLSRE)).isFile()) {
                File a = new File(RutaCarpXMLSRE + "_" + NomArchivoXMLSRE);
                a.renameTo(new File(RutaCarpXMLSRE + NomArchivoXMLSRE));
            }
            ErrorCod.argValue = 4;
            ErrorMsg.argValue = ex.getMessage();
            TAFACE2ApiEntidad.TALog objLog = new TAFACE2ApiEntidad.TALog(cfgCarpetaOperacion, EmpRUT, SucId, CajId);
            if (ErrorCod.argValue == 9) {
                try {
                    objLog.EscribirLog(ex.toString() + " - " + getNombreMetodo(), TAFACE2ApiEntidad.TALog.enumTipoLog.LogErrorDeConexion);
                } catch (UnsupportedEncodingException ex1) {
                    Logger.getLogger(TAFEApi.class.getName()).log(Level.SEVERE, null, ex1);
                }
            } else {
                try {
                    objLog.EscribirLog(ex.toString() + " - " + getNombreMetodo(), TAFACE2ApiEntidad.TALog.enumTipoLog.LogError);
                } catch (UnsupportedEncodingException ex1) {
                    Logger.getLogger(TAFEApi.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
            EnviaContingencia = false;
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(TAFEApi.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TAFEApi.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TAFEApi.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(TAFEApi.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(TAFEApi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return EnviaContingencia;
    }

    public  boolean FacturaEsAnulable(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, long EmpresaRUC, short SucursalId, short Cajaid, int enumTipoDeComprobanteCFE, String Serie, int Nro) throws TAException {

        boolean Anulable = false;
        try {
            //***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
            Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "long EmpresaRUC", "short SucursalId", "short Cajaid", "int enumTipoDeComprobanteCFE", "String Serie", "int Nro"};
            Object[] DebugValuesArray = {ErrorMsg.argValue, EmpresaRUC, SucursalId, Cajaid, enumTipoDeComprobanteCFE, Serie, Nro};
            objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);
            //Verifico que se haya inicializado el componente
            if (!Inicializado) {
                throw new TAException("Debe Inicializar la API primero invocando al metodo Inicializar", 4128);
            }
            if (cfgModoFirma == ModoFirma.ServicioWeb) {
                WakeUpServer();
//                Object WSFacturaEsAnulable = new Object(); //WSFacturaEsAnulable_v0100.WSFacturaEsAnulable_v0100
//                WSFacturaEsAnulable.setUrl(cfgUrlServidor + "aWSFacturaEsAnulable_v0200.aspx");
//                WSFacturaEsAnulable.setTimeout(cfgSegundosTimeout * 1000);
//                Anulable = executorWS.(EmpresaRUC, SucursalId, Cajaid, enumTipoDeComprobanteCFE, Serie, Nro, ErrorMsg.argValue);
            } else {
                throw new RuntimeException("Metodo no implementado firma en caja");
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(TAFEApi.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TAFEApi.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TAFEApi.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JAXBException ex) {
            Logger.getLogger(TAFEApi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Anulable;
    }

    public  boolean AnularFactura(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, long EmpresaRUC, short SucursalId, short Cajaid, int enumTipoDeComprobanteCFE, String Serie, int Nro) {
        try {
            ErrorMsg.argValue = "";
            ErrorCod.argValue = 0;
            //***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
            Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod", "long EmpresaRUC", "short SucursalId", "short Cajaid", "int enumTipoDeComprobanteCFE", "String Serie", "int Nro"};
            Object[] DebugValuesArray = {ErrorMsg.argValue, EmpresaRUC, SucursalId, Cajaid, enumTipoDeComprobanteCFE, Serie, Nro};
            objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);
            //***IMPLEMENTAR!!!***
            if (cfgModoFirma == ModoFirma.ServicioWeb) {
                WakeUpServer();
                throw new RuntimeException("Metodo no implementado firma en gateway");
            } else {
                throw new RuntimeException("Metodo no implementado firma en caja");
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(TAFEApi.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TAFEApi.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TAFEApi.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JAXBException ex) {
            Logger.getLogger(TAFEApi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public  boolean Sincronizar(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod) throws TAException {

        boolean Sincroniza = false;
        try {
            ErrorMsg.argValue = "";
            ErrorCod.argValue = 0;
            //***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
            objDebug.EscribirDebug(getNombreMetodo(), new Object[]{"Inicio Sincronizacion"}, new Object[]{""});

            int CantLogsPend = 0;
            int CantCtePend = 0;
            int CantKeepAliveLogsPend = 0;
            int CantKeepAliveCtePend = 0;
            String ErrorMsgInterno = "";

            //Verifico que se haya inicializado el componente
            if (!Inicializado) {
                throw new TAException("Debe Inicializar la API primero invocando al metodo Inicializar", 4232);
            }

            //Si se modifico la configuracion actualizo los datos
            tangible.RefObject<String> tempRef_ErrorMsgInterno = new tangible.RefObject<String>(ErrorMsgInterno);
            boolean tempVar = !(ActualizarConfiguracion(tempRef_ErrorMsgInterno, ErrorCod));
            ErrorMsgInterno = tempRef_ErrorMsgInterno.argValue;
            if (tempVar) {
                ErrorMsg.argValue += "Error al actualizar configuracion - " + tempRef_ErrorMsgInterno.argValue + "\r\n";
            } else {
                ErrorMsg.argValue += "Sincronizar configuracion - ok" + "\r\n";
            }

            //Envio logs de errores (Archvios)
            tangible.RefObject<String> tempRef_ErrorMsgInterno2 = new tangible.RefObject<String>(ErrorMsgInterno);
            boolean tempVar2 = !(EnviarLog(tempRef_ErrorMsgInterno2, ErrorCod));
            ErrorMsgInterno = tempRef_ErrorMsgInterno2.argValue;
            if (tempVar2) {
                ErrorMsg.argValue += "Error al enviar Logs - " + tempRef_ErrorMsgInterno2.argValue + "\r\n";
            } else {
                ErrorMsg.argValue += "Sincronizar log errores - ok" + "\r\n";
            }

            //Envio Contingencias (Archvios)
            tangible.RefObject<String> tempRef_ErrorMsgInterno3 = new tangible.RefObject<String>(ErrorMsgInterno);
            boolean tempVar3 = !(EnviarContingencia(tempRef_ErrorMsgInterno3, ErrorCod, true));
            ErrorMsgInterno = tempRef_ErrorMsgInterno3.argValue;
            if (tempVar3) {
                ErrorMsg.argValue += "Error al enviar contingencia - " + tempRef_ErrorMsgInterno3.argValue + "\r\n";
            } else {
                ErrorMsg.argValue += "Sincronizar contingencias - ok" + "\r\n";
            }
            if (objConfig.getVALIDARCFC() == 1) {
                if (!(DescargarCFC(ErrorMsg, tempRef_ErrorMsgInterno3))) {
                    ErrorMsg.argValue += "Error al descargar CFC - " + tempRef_ErrorMsgInterno3.argValue + "\r\n";
                } else {
                    ErrorMsg.argValue += "Sincronizar CFC - ok" + "\r\n";
                }
            }

            //Sumo cantidad de datos pendientes de sincronizar para enviar al WS KeepAlive
            CantLogsPend += CantidadLogsPendSincronizar(MaxMinutosSinSincronizar);
            CantCtePend += CantidadContingPendSincronizar(MaxMinutosSinSincronizar);

            CantKeepAliveLogsPend = CantLogsPend;
            CantKeepAliveCtePend = CantCtePend;
            if (cfgModoFirma == ModoFirma.FirmaLocal) {
                //Sincronizo datos de bd sqlite
                TAFirmarFactura SincDatos = new TAFirmarFactura();
                SincDatos.setRutaConexion(sPuntoConexionBD);
                SincDatos.setProveedorConexion(cfgMotorBD.getValue());
                if (SincDatos.SincronizarDatos(cfgUrlServidor, (short) TimeOutSincronizacion, cfgLoteSincronizacion, EmpRUT, SucId, CajId, VersionActual(), ErrorMsg, ErrorCod, objConfig.getMINUTOSCADUCIDADRESERVACAE(), CajaLicenseId())) {
                    ErrorMsg.argValue += "Sincronizar comprobantes - ok" + "\r\n";
                } else {
                    throw new RuntimeException(" Error al Sincronizar datos: " + "\r\n" + ErrorMsgInterno + "\r\n");
                }

                CantLogsPend += SincDatos.CantidadLogsPendSincronizar(cfgUrlServidor, (short) TimeOutSincronizacion, EmpRUT, SucId, CajId, MaxMinutosSinSincronizar);
                CantCtePend += SincDatos.CantidadCompPendSincronizar(cfgUrlServidor, (short) TimeOutSincronizacion, EmpRUT, SucId, CajId, MaxMinutosSinSincronizar);
                CantKeepAliveLogsPend += CantLogsPend;
                CantKeepAliveCtePend += SincDatos.CantidadCompPendSincronizar(cfgUrlServidor, (short) TimeOutSincronizacion, EmpRUT, SucId, CajId, 72 * 60);
            }

            tangible.RefObject<String> tempRef_ErrorMsgInterno5 = new tangible.RefObject<String>(ErrorMsgInterno);
            boolean tempVar5 = !(KeepAlive(tempRef_ErrorMsgInterno5, CantKeepAliveCtePend, CantKeepAliveLogsPend));
            ErrorMsgInterno = tempRef_ErrorMsgInterno5.argValue;
            if (tempVar5) {
                ErrorMsg.argValue += "Error al Sincronizar KeepAlive - " + ErrorMsgInterno;
            } else {
                ErrorMsg.argValue += "Sincronizar KeepAlive - ok";
            }

            objDebug.EscribirDebug(getNombreMetodo(), new String[]{"Fin Sincronizacion"}, new Object[]{""});
            Sincroniza = true;
        } catch (Exception ex) {
            ErrorMsg.argValue += ex.getMessage();
            TAFACE2ApiEntidad.TALog objLog = new TAFACE2ApiEntidad.TALog(cfgCarpetaOperacion, EmpRUT, SucId, CajId);
            if (ErrorCod.argValue == 9) {
                try {
                    objLog.EscribirLog(ex.toString() + " - " + getNombreMetodo(), TAFACE2ApiEntidad.TALog.enumTipoLog.LogErrorDeConexion);
                } catch (UnsupportedEncodingException ex1) {
                    throw new TAException(ex1.getMessage());
                }
            } else {
                try {
                    objLog.EscribirLog(ex.toString() + " - " + getNombreMetodo(), TAFACE2ApiEntidad.TALog.enumTipoLog.LogError);
                } catch (UnsupportedEncodingException ex1) {
                    throw new TAException(ex1.getMessage());
                }
            }
            Sincroniza = false;
        }
        return Sincroniza;
    }

    public  boolean CerrarCaja(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String UrlServidorTAFirmoGateway, long EmpresaRUT, int SucursalId, int CajaId, String RutaCarpetaOperacion) throws TAException {

        boolean CerrarCaja = false;
        try {
            ErrorMsg.argValue = "";
            ErrorCod.argValue = 0;
            int CantCtePend = 0;
            int MinutosSinSinc = 30;
            String ErrorMsgInterno = "";

            try {
                RutaCarpetaOperacion = RutaCarpetaOperacion + "";
                if (RutaCarpetaOperacion.trim().equals("")) {
                    throw new TAException("Debe especificar la ruta de acceso a los archivos de TAFACE");//cfgCarpetaOperacion = System.AppDomain.CurrentDomain.BaseDirectory;
                } else {
                    if (!RutaCarpetaOperacion.substring(RutaCarpetaOperacion.length() - 1).equals(File.separator) && !RutaCarpetaOperacion.substring(RutaCarpetaOperacion.length() - 1).equals(File.separator)) {
                        RutaCarpetaOperacion = RutaCarpetaOperacion + File.separator;
                    }
                }
                if (!((new java.io.File(RutaCarpetaOperacion + File.separator)).isDirectory())) {
                    return true;
                }
                EmpRUT = String.format("%012d", EmpresaRUT).replace(' ', '0');
                objConfig = new XMLCONFIGURACION();
                objConfig = objConfig.CargarConfiguracion(RutaCarpetaOperacion, EmpRUT, SucursalId);
                if ((objConfig == null)) {
                    return true;
                }
                if (!((new java.io.File(RutaCarpetaOperacion + File.separator + "XML" + File.separator + "Contingencia")).isDirectory()) && !((new java.io.File(RutaCarpetaOperacion + File.separator + "LOG")).isDirectory())) {
                    return true;
                }
                if (objConfig.getTIPOPROVEEDORSQL() == TAFE2ApiFirmar.NadMADato.TIPO_PROVEEDOR.tpSQLiteClient.getValue() && !((new java.io.File(RutaCarpetaOperacion + File.separator + "TAFACEApi.db")).isFile())) {
                    return true;
                }
            } catch (RuntimeException ex) {
                throw new TAException(ex.getMessage());
            } catch (FileNotFoundException ex) {
                throw new TAException(ex.getMessage());
            } catch (IOException ex) {
                throw new TAException(ex.getMessage());
            }

            //Inicializo la API
            Inicializar(ErrorMsg, ErrorCod, UrlServidorTAFirmoGateway, TimeOutSincronizacion, EmpresaRUT, SucursalId, CajaId, RutaCarpetaOperacion);

            MinutosSinSinc = nHorasSinConexion * 60;

            //Envio logs de errores (Archivos)
            tangible.RefObject<String> tempRef_ErrorMsgInterno = new tangible.RefObject<String>(ErrorMsgInterno);
            tangible.RefObject<Integer> tempRef_Object = new tangible.RefObject<Integer>(0);
            EnviarLog(tempRef_ErrorMsgInterno, tempRef_Object);
            ErrorMsgInterno = tempRef_ErrorMsgInterno.argValue;

            //Envio Contingencias (Archivos)
            tangible.RefObject<String> tempRef_ErrorMsgInterno2 = new tangible.RefObject<String>(ErrorMsgInterno);
            tangible.RefObject<Integer> tempRef_Object2 = new tangible.RefObject<Integer>(0);
            boolean tempVar = !(EnviarContingencia(tempRef_ErrorMsgInterno2, tempRef_Object2, false));
            ErrorMsgInterno = tempRef_ErrorMsgInterno2.argValue;
            if (tempVar) {
                ErrorMsg.argValue += " Error al enviar contingencia - " + ErrorMsgInterno + "\r\n";
            }

            CantCtePend += CantidadContingPendSincronizar(MinutosSinSinc);

            if (cfgModoFirma == ModoFirma.FirmaLocal) {
                //Sincronizo datos de bd sqlite
                TAFirmarFactura SincDatos = new TAFirmarFactura();
                SincDatos.setRutaConexion(sPuntoConexionBD);
                SincDatos.setProveedorConexion(cfgMotorBD.getValue());
                if (!(SincDatos.SincronizarDatos(cfgUrlServidor, (short) TimeOutSincronizacion, (short) 0, EmpRUT, SucId, CajId, VersionActual(), tempRef_ErrorMsgInterno2, ErrorCod, objConfig.getMINUTOSCADUCIDADRESERVACAE(), CajaLicenseId()))) {
                    ErrorMsg.argValue += " Error al Sincronizar datos - " + ErrorMsgInterno + "\r\n";
                } else {
                    if (!(SincDatos.LimpiarComprobantesSincronizados(cfgUrlServidor, (short) TimeOutSincronizacion, EmpRUT, SucId, CajId, tempRef_ErrorMsgInterno2))) {
                        ErrorMsg.argValue += "\r\n" + ErrorMsgInterno;
                    }
                    if (!(SincDatos.LimpiarLogsSincronizados(cfgUrlServidor, (short) TimeOutSincronizacion, EmpRUT, SucId, CajId, tempRef_ErrorMsgInterno2))) {
                        ErrorMsg.argValue += "\r\n" + ErrorMsgInterno;
                    }
                }
                CantCtePend += SincDatos.CantidadCompPendSincronizar(cfgUrlServidor, (short) TimeOutSincronizacion, EmpRUT, SucId, CajId, MinutosSinSinc);
                SincDatos.schrinkBDSQlite();
                if (CantCtePend > 0) {
                    ErrorMsg.argValue = "Existe" + ((CantCtePend > 1) ? "s " : " ") + CantCtePend + " comprobante" + ((CantCtePend > 1) ? "s" : "") + " pendiente de sincronizar." + "\r\n" + ErrorMsg.argValue;
                    ErrorCod.argValue = 1;
                    CerrarCaja = false;
                }
            }
            CerrarCaja = true;
        } catch (RuntimeException ex) {
            ErrorCod.argValue = 8;
            ErrorMsg.argValue += "\r\n" + ex.getMessage();
            return false;
        } catch (Exception ex) {
            throw new TAException(ex.getMessage());
        }
        return CerrarCaja;
    }

    public  boolean ExisteCFC(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, int TipoCFC, String SerieCFC, long NroCFC, long NroAutorizacionCFC, long NroDesdeCFC, long NroHastaCFC, java.util.Date FechaVencimientoCFC) throws TAException {

        boolean existeCFC = false;
        try {
            ErrorMsg.argValue = "";
            ErrorCod.argValue = 0;
            tangible.RefObject<String> ErrorCodeCFC = new RefObject<String>("");
            if (!Inicializado) {
                throw new TAException("Debe Inicializar la API primero invocando al metodo Inicializar", 4127);
            }
            if (objConfig.getVALIDARCFC() == 0) {
                return true;
            }
            XMLCFC XmlCfc = new XMLCFC();
            XmlCfc = XmlCfc.CargarCFC(cfgCarpetaOperacion, EmpRUT, SucId, CajId);
            if ((XmlCfc == null)) {
                DescargarCFC(ErrorMsg, ErrorCodeCFC);
                if (!((XmlCfc == null))) {
                    XmlCfc = XmlCfc.CargarCFC(cfgCarpetaOperacion, EmpRUT, SucId, CajId);
                }
            }
            if ((XmlCfc == null)) {
                throw new TAException("No se pudo cargar archivo de CFC. Error: " + "\r\n" + ErrorMsg.argValue);
            }
            for (int i = 0; i < XmlCfc.getCFCITEMS().length; i++) { //checar fecha, si es yyyyMMddHHmmss o yyyyMMdd
                if (XmlCfc.getCFCITEMS()[i].getCFCTipoCFE() == TipoCFC && XmlCfc.getCFCITEMS()[i].getCFCNroAutorizacion() == NroAutorizacionCFC && XmlCfc.getCFCITEMS()[i].getCFCNroDesde() == NroDesdeCFC && XmlCfc.getCFCITEMS()[i].getCFCNroHasta() == NroHastaCFC && XmlCfc.getCFCITEMS()[i].getCFCSerie().trim().equals(SerieCFC.trim()) && NroDesdeCFC <= NroCFC && NroHastaCFC >= NroCFC && (XmlCfc.getCFCITEMS()[i].getCFCVencimiento().getDate() == FechaVencimientoCFC.getDate())) {
                    existeCFC = true;
                }
            }
            ErrorMsg.argValue = "Los datos de contingencia ingresados no son validos. El CFC ingresado tiene los siguientes datos. Tipo CFC: " + TipoCFC + " - Serie CFC:" + SerieCFC + " - Nro. CFC:" + NroCFC + " - Nro. de Autorización del CFC:" + NroAutorizacionCFC + " - Nro. Desde CFC:" + NroDesdeCFC + " - Nro. Hasta CFC:" + NroHastaCFC + " - Fecha Vencimiento CFC:" + FechaVencimientoCFC.toString().trim();
            ErrorCod.argValue = 2;
            existeCFC = false;
        } catch (Exception ex) {
            ErrorMsg.argValue = ex.getMessage();
            ErrorCod.argValue = 8;
            existeCFC = false;
        }
        return existeCFC;
    }

    //Si no logra obtener respuesta del server, lanza un error
    private void WakeUpServer() throws TAException {
        int Intentos = 0;
        RuntimeException exAux = null;
        try {
            while (Intentos < 2) {
                try {
                    if (executorWS.executeWakeUpServer()) {
                        return;
                    }
                } catch (RuntimeException ex) {
                    exAux = ex;
                }
                Intentos += 1;
            }
            //Comprueba que no sea una versión vieja
            if (!(VersionAppMayorIgual())) {
                throw new RuntimeException("La versión de la API es mayor a la versión de la aplicación web. Por lo tanto no se puede continuar con la operación.");
            }
            if (exAux != null) {
                throw new RuntimeException("Error luego de 2 intentos no logro conectarse - " + exAux.getMessage(), exAux);
            } else {
                throw new RuntimeException("Error luego de 2 intentos no logro conectarse");
            }

        } catch (RuntimeException ex) {
            throw new TAException("Error Iniciando Conexión con Servidor - Url: " + cfgUrlServidor + " - TimeOut: " + cfgSegundosTimeout + "\r\n" + ex.getMessage(), 9999);
        }
    }

    private boolean VersionAppMayorIgual() throws TAException {
        boolean GEQ = false; //Greater than or equal
        String strVersionApp = "";
        int intVersionApi = 0;
        int intVersionApp = 0;

        String strVersionApi = VersionActual();
        //No utilizo el método VersionActual() porque no me devuelve lo que necesito

        try {
            strVersionApp = executorWS.executeVersionActual();
        } catch (RuntimeException ex) {
            throw new TAException("Error Iniciando Conexión con Servidor - Url: " + cfgUrlServidor + " - TimeOut: " + cfgSegundosTimeout + "\r\n" + ex.getMessage(), 9999);
        }
        intVersionApi = Integer.parseInt("1" + strVersionApi.replace(".", ""));
        intVersionApp = Integer.parseInt("1" + strVersionApp.replace(".", ""));

        if (intVersionApp >= intVersionApi) {
            GEQ = true;
        }

        return GEQ;
    }

    private boolean KeepAlive(tangible.RefObject<String> ErrorMsg, int CantCtePend, int CantLogPend) throws TAException {

        boolean keepAlive = false;
        try {
            ErrorMsg.argValue = "";
            boolean ErrReturn = false;
            WakeUpServer();
            ErrReturn = executorWS.executeKeepAlive(EmpRUT, (short) CajId, SucId, CantCtePend, CantLogPend, new java.util.Date());
            if (ErrReturn) {
                keepAlive = false;
            } else {
                keepAlive = true;
            }
        } catch (RuntimeException ex) {
            ErrorMsg.argValue = ex.getMessage();
            keepAlive = false;
        } catch (TAException ex) {
            throw ex;
        }
        return keepAlive;
    }

    private boolean ActualizarConfiguracion(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod) throws TAException {

        boolean actualizarConfig = false;
        Date flagBorradoCteSinc = null;
        int cantDiasFirmoRetroactivo = 0;
        int eliminarCteSincronizadosDias = 0;
        try {
            ErrorMsg.argValue = "";
            ErrorCod.argValue = 0;
            String GUIDConfig = "";
            if (objConfig != null) {
                GUIDConfig = objConfig.getGUID();
                flagBorradoCteSinc = objConfig.getSYNC();
                cantDiasFirmoRetroactivo = objConfig.getDIASFIRMARRETROACTIVO();
                eliminarCteSincronizadosDias = objConfig.getELIMINARCTESINCRONIZADODIAS();
            }
            WakeUpServer();
            WSSincronizacionParamsApi0205ExecuteResponse Conf = null;
            Conf = executorWS.executeParamsApi(EmpRUT, (short) CajId, SucId);

            if ((Conf == null)) {
                throw new RuntimeException("Error descargando config. inicial: Servidor retorno nothing, para operar debe conectarse al menos una vez.");
            }
            if (Conf.getPsdtparamtafeapi().getParamGUID() != null && !Conf.getPsdtparamtafeapi().getParamGUID().trim().equals("")) {
                objConfig = new XMLCONFIGURACION();
                objConfig.setMODOFIRMA(Conf.getPsdtparamtafeapi().getModoFirma());
                objConfig.setTIPOENCRIPTACION(Conf.getPsdtparamtafeapi().getTipoEncriptacion());
                objConfig.setTIPOPROVEEDORSQL(Conf.getPsdtparamtafeapi().getTipoProveedorSQL());
                objConfig.setTOPEUIFREESHOP(Conf.getPsdtparamtafeapi().getTopeUIFreeShop());
                objConfig.setTOPEUIPLAZA(Conf.getPsdtparamtafeapi().getTopeUIPlaza());
                objConfig.setVALIDARCFC((short) (Conf.getPsdtparamtafeapi().isValidarCFC() ? 1 : 0));
                objConfig.setLOTESINCRONIZACION(Conf.getPsdtparamtafeapi().getLoteSincronizacion());
                objConfig.setHORASSINCONEXION((short) nHorasSinConexion);
                objConfig.setCAESPORCAVANCE(Conf.getPsdtparamtafeapi().getCAEsPorcAvance());
                objConfig.setCANTDIASANTESCAEVENCER(Conf.getPsdtparamtafeapi().getCantDiasAntesCAEVencer());
                objConfig.setCANTDIASANTESCERTIFICADOVENCER(Conf.getPsdtparamtafeapi().getCantDiasAntesCertificadoVencer());
                objConfig.setMINDATOSCAJASINSINC(Conf.getPsdtparamtafeapi().getMinDatosCajaSinSinc());
                objConfig.setGUID(Conf.getPsdtparamtafeapi().getParamGUID());
                objConfig.setENCRIPTARCOMPLEMENTOFISCAL((short) (Conf.getPsdtparamtafeapi().isEncriptarComplementoFiscal() ? 1 : 0));
                objConfig.setMINUTOSCADUCIDADRESERVACAE(Conf.getPsdtparamtafeapi().getMinutosCaducidadReservaCAE());
                objConfig.setLARGOMINIMOTICKET(Conf.getPsdtparamtafeapi().getLargoMinimoTicket());
                objConfig.setABRIRCAJON((short) (Conf.getPsdtparamtafeapi().isAperturaDeCajon() ? 1 : 0));
                objConfig.setNOVALIDARFECHAFIRMA((short) (Conf.getPsdtparamtafeapi().isNoValidarFechaFirma() ? 1 : 0));
                objConfig.setSYNC(flagBorradoCteSinc);
                objConfig.setDIASFIRMARRETROACTIVO(cantDiasFirmoRetroactivo);
                objConfig.setELIMINARCTESINCRONIZADODIAS(eliminarCteSincronizadosDias);
                //Si TipoSQL es SQLServer y firma en caja, creo el punto de conexion que deberia venir en -> 'Conf.CadenaCnxSQL
                if (Conf.getPsdtparamtafeapi().getTipoProveedorSQL() == 1 && Conf.getPsdtparamtafeapi().getModoFirma() == 2) {
//                    NadSQLServer NadDato = new NadSQLServer();
//                    NadDato.ArchivoConexion = cfgCarpetaOperacion + "TAFACE\\" + EmpRUT + "_" + SucId + "_TAFEApi.conexion";
//                    NadDato.GrabarArchivo(Conf.getCadenaCnxSQL());
//                    //Valido que pueda conectar con sql server
//                    if (!(NadDato.Conectar())) {
//                        //Si no conecta lanzo una TAException asi no guarda el archivo de configuracion e intenta descargarlo la proxima vez
//                        throw new TAException("Error al conectar con SQL Server.", 1031);
//                    } else {
//                        //Si logra conectar sigo y no hago nada
//                        NadDato = null;
//                    }
                }
                objConfig.GuardarConfig(cfgCarpetaOperacion, EmpRUT, SucId);
            }

            //Actualizo la Hora del sistema desde el GW
            if (objConfig != null) {
                //llamo al WS que me retorna la Hora desde el GW
                java.util.Date HoraSistemaGW = new java.util.Date(0);

                if (objConfig.getFECHASISTEMA() != tangible.DotNetToJavaDateHelper.today()) //Disparo actualizacion de Fecha
                {
                    HoraSistemaGW = DescargarFechaSistema();
                }

                if (!HoraSistemaGW.equals(objConfig.getFECHASISTEMA()) && !HoraSistemaGW.equals(new java.util.Date(0))) {
                    objConfig.setFECHASISTEMA(HoraSistemaGW);
                    objConfig.GuardarConfig(cfgCarpetaOperacion, EmpRUT, SucId);
                }
            }
            actualizarConfig = true;
        } catch (RuntimeException ex) {
            ErrorMsg.argValue = ex.getMessage();
            actualizarConfig = false;
        } catch (TAException ex) {
            throw ex;
        } catch (IOException ex) {
            throw new TAException(ex.getMessage());
        }
        return actualizarConfig;
    }

    private boolean EnviarLog(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod) throws TAException {
        tangible.RefObject<String> CarpArchXMLLog = new RefObject<String>("");
        tangible.RefObject<String> NomArchXMLLog = new RefObject<String>("");
        try {
            ErrorMsg.argValue = "";
            ErrorCod.argValue = 0;
            //***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
            Object[] DebugNamesArray = {"tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Integer> ErrorCod"};
            Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue};
            objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);

            //Verifico que se haya inicializado el componente
            if (!Inicializado) {
                throw new TAException("Debe Inicializar la API primero invocando al metodo Inicializar", 4127);
            }

            //Envio al gateway todos los XML de contingencia
            TAFACE2ApiEntidad.TALog objLog = new TAFACE2ApiEntidad.TALog(cfgCarpetaOperacion, EmpRUT, SucId, CajId);
            tangible.RefObject<String> XMLLog = new RefObject<String>("");
            tangible.RefObject<java.util.Date> CreacionLog = new RefObject<Date>(new Date());


            while (objLog.ObtenerXMLLog(XMLLog, CarpArchXMLLog, NomArchXMLLog, CreacionLog)) {
                if (!XMLLog.argValue.trim().equals("")) {
                    WakeUpServer();
                    //Envio al gateway
                    if ((cfgUrlServidor != null && cfgUrlServidor.length() > 0) && !cfgUrlServidor.substring(cfgUrlServidor.length() - 1).equals("/")) {
                        cfgUrlServidor = cfgUrlServidor + "/";
                    }
                    long LogIdNew = 0;
                    String TipoError = "LOG";
                    if (NomArchXMLLog.argValue.lastIndexOf("CNX") > -1) {
                        TipoError = "CNX";
                    }
                    WSSincronizacionLogErrores0200ExecuteResponse resp = executorWS.executeLogErrores((short) CajId, EmpRUT, (short) 0, CreacionLog.argValue, new java.util.Date(), 0, XMLLog.argValue, "", "", TipoError, "NUN", SucId);
                    LogIdNew = resp.getPlogerridnew();
                    ErrorMsg.argValue = resp.getPerrormessage();
                    if (resp.isPerrorreturn() || LogIdNew == 0) {
                        throw new TAException(resp.getPerrormessage(), 6000);
                    }
                }
                //Si envioOK borro el XML
                objLog.Eliminar(CarpArchXMLLog.argValue + "_" + NomArchXMLLog.argValue);
            }
            return true;
        } catch (RuntimeException ex) {
            try {
                File file = new File(CarpArchXMLLog.argValue + "_" + NomArchXMLLog.argValue);
                if ((new java.io.File(CarpArchXMLLog.argValue + "_" + NomArchXMLLog.argValue)).isFile()) {
                    if ((new java.io.File(CarpArchXMLLog.argValue + NomArchXMLLog.argValue)).isFile()) {
                        //si se creo un log mientras estaba tratando de sincronizar renombro el archivo con un numero iterativo ya que no puede volver a su nombre anterior 
                        int i = 0;
                        while (true) {
                            file = new File(CarpArchXMLLog.argValue + i + "_" + NomArchXMLLog.argValue);
                            if (!(file.isFile())) {
                                file.renameTo(new File(CarpArchXMLLog.argValue + i + "_" + NomArchXMLLog.argValue));
                                break;
                            }
                            i += 1;
                        }
                    } else {
                        //Si no creo ningun log vuelvo a su nombre origi
                        file.renameTo(new File(CarpArchXMLLog.argValue + NomArchXMLLog.argValue));
                    }
                }
            } catch (RuntimeException ex2) {
            }
            ErrorCod.argValue = 8;

            ErrorMsg.argValue = ex.getMessage();
            TAFACE2ApiEntidad.TALog objLog = new TAFACE2ApiEntidad.TALog(cfgCarpetaOperacion, EmpRUT, SucId, CajId);
            if (ErrorCod.argValue == 9) {
                try {
                    objLog.EscribirLog(ex.toString() + " - " + getNombreMetodo(), TAFACE2ApiEntidad.TALog.enumTipoLog.LogErrorDeConexion);
                } catch (UnsupportedEncodingException ex1) {
                    throw new TAException(ex1.getMessage());
                }
            } else {
                try {
                    objLog.EscribirLog(ex.toString() + " - " + getNombreMetodo(), TAFACE2ApiEntidad.TALog.enumTipoLog.LogError);
                } catch (UnsupportedEncodingException ex1) {
                    throw new TAException(ex1.getMessage());
                }
            }
            return true;
        } catch (UnsupportedEncodingException ex) {
            throw new TAException(ex.getMessage());
        } catch (FileNotFoundException ex) {
            throw new TAException(ex.getMessage());
        } catch (IOException ex) {
            throw new TAException(ex.getMessage());
        } catch (JAXBException ex) {
            throw new TAException(ex.getMessage());
        }
    }

    private java.util.Date DescargarFechaSistema() throws TAException {
        java.util.Date tempVar = executorWS.executeDescargarFechaSistema();

        return tempVar;
    }

    private boolean DescargarCFC(tangible.RefObject<String> ErrorMsg, tangible.RefObject<String> ErrorCod) throws TAException {
        try {
            ErrorMsg.argValue = "";
            ErrorCod.argValue = String.valueOf(0);
            if (objConfig.getVALIDARCFC() == 0) {
                return true;
            }
            String NomArchivo = EmpRUT + "_" + SucId;
            java.util.ArrayList<SDTCFCRequestSDTCFCRequestItem> CFCReq = new java.util.ArrayList<SDTCFCRequestSDTCFCRequestItem>();
            WSSincronizacionCFC0200ExecuteResponse CFCResp = new WSSincronizacionCFC0200ExecuteResponse();
            XMLCFC XmlCFC = new XMLCFC();
            java.util.ArrayList<XMLCFC.CFCItem> ListCFC = new ArrayList<XMLCFC.CFCItem>();
            boolean ErrorRet = false;
            XmlCFC = XmlCFC.CargarCFC(cfgCarpetaOperacion, EmpRUT, SucId, CajId);
            if (!((XmlCFC == null))) {
                for (int i = 0; i < XmlCFC.getCFCITEMS().length; i++) {
                    SDTCFCRequestSDTCFCRequestItem itmReq = new SDTCFCRequestSDTCFCRequestItem();
                    itmReq.setCFCId(XmlCFC.getCFCITEMS()[i].getCFCId());
                    itmReq.setEmpId(XmlCFC.getCFCITEMS()[i].getEmpId());
                    itmReq.setCFCRowGUID(XmlCFC.getCFCITEMS()[i].getCFCRowGUID());
                    CFCReq.add(itmReq);
                    ListCFC.add(XmlCFC.getCFCITEMS()[i]);
                }
            }
            WakeUpServer();
            tangible.RefObject<Boolean> tempRef_ErrorRet = new tangible.RefObject<Boolean>(ErrorRet);
            CFCResp = executorWS.executeWSSincronizacionCFC(EmpRUT, SucId, (short) CajId, CFCReq);
            ErrorRet = tempRef_ErrorRet.argValue;
            if (ErrorRet) {
                throw new RuntimeException(ErrorMsg.argValue);
            }
            if (!((CFCResp == null)) && CFCResp.getPsdtcfcresponse().getSDTCFCResponseSDTCFCResponseItem().size() > 0) {
                boolean Existe = false;
                for (SDTCFCResponseSDTCFCResponseItem RespItm : CFCResp.getPsdtcfcresponse().getSDTCFCResponseSDTCFCResponseItem()) {
                    Existe = false;
                    if (!((XmlCFC == null))) {
                        for (XMLCFC.CFCItem CfcItm : ListCFC) {
                            if (RespItm.getCFCId() == CfcItm.getCFCId() && RespItm.getEmpId() == CfcItm.getEmpId() && RespItm.getSucId() == CfcItm.getSucId()) {
                                if (!RespItm.getCFCRowGUID().equals(CfcItm.getCFCRowGUID())) {
                                    CfcItm.setCFCFchHora(RespItm.getCFCFchHora().getTime());
                                    CfcItm.setCFCNroAutorizacion(RespItm.getCFCNroAutorizacion());
                                    CfcItm.setCFCNroDesde(RespItm.getCFCNroDesde());
                                    CfcItm.setCFCNroHasta(RespItm.getCFCNroHasta());
                                    CfcItm.setCFCSerie(RespItm.getCFCSerie());
                                    CfcItm.setCFCTipoCFE(RespItm.getCFCTipoCFE());
                                    CfcItm.setCFCVencimiento(RespItm.getCFCVencimiento().getTime());
                                    CfcItm.setCFCRowGUID(RespItm.getCFCRowGUID());
                                } else if (RespItm.isCFCEliminado()) {
                                    ListCFC.remove(CfcItm);
                                }
                                Existe = true;
                                break;
                            }
                        }
                    } else {
                        XmlCFC = new XMLCFC();
                    }
                    if (!Existe) {
                        XMLCFC.CFCItem CfcItm = XmlCFC.new CFCItem();
                        CfcItm.setCFCId(RespItm.getCFCId());
                        CfcItm.setEmpId(RespItm.getEmpId());
                        CfcItm.setSucId(RespItm.getSucId());
                        CfcItm.setCFCFchHora(RespItm.getCFCFchHora().getTime());
                        CfcItm.setCFCNroAutorizacion(RespItm.getCFCNroAutorizacion());
                        CfcItm.setCFCNroDesde(RespItm.getCFCNroDesde());
                        CfcItm.setCFCNroHasta(RespItm.getCFCNroHasta());
                        CfcItm.setCFCSerie(RespItm.getCFCSerie());
                        CfcItm.setCFCTipoCFE(RespItm.getCFCTipoCFE());
                        CfcItm.setCFCVencimiento(RespItm.getCFCVencimiento().getTime());
                        CfcItm.setCFCRowGUID(RespItm.getCFCRowGUID());
                        ListCFC.add(CfcItm);
                    }
                }
                XMLCFC.CFCItem[] tempCFCItemArray = new XMLCFC.CFCItem[ListCFC.size()];
                XmlCFC.setCFCITEMS(ListCFC.toArray(tempCFCItemArray));
                XmlCFC.GuardarCFC(cfgCarpetaOperacion, EmpRUT, SucId, CajId);
            }
            return true;
        } catch (RuntimeException ex) {
            ErrorCod.argValue = String.valueOf(8);
            ErrorMsg.argValue = ex.getMessage();
            return false;
        } catch (FileNotFoundException ex) {
            throw new TAException(ex.getMessage());
        } catch (IOException ex) {
            throw new TAException(ex.getMessage());
        } catch (JAXBException ex) {
            throw new TAException(ex.getMessage());
        }
    }

    private String CajaLicenseId() throws TAException {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:",b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }

    private String VersionActual() {
        return "02.06";
    }

    private String ObtenerCarpetaAplicacion() {
        return cfgCarpetaOperacion;
    }

    private boolean ExistenSinRespPendSincronizar(int MinSinSincronizar) {

        boolean pendSincronizarSinResp = false;
        try {
            File dir = new File(cfgCarpetaOperacion + File.separator + "XML" + File.separator + "SinRespuesta");
            if (dir.isDirectory()) {
                File[] filesList = dir.listFiles();
                if (filesList.length > 0) {
                    for (File file : filesList) {
                        if (file.isFile() && esArchivo(file.getAbsolutePath(), EmpRUT, String.valueOf(SucId))) {
                            Date date = new Date();
                            date.setMinutes(date.getMinutes() - MinSinSincronizar);
                            if (file.lastModified() < date.getDate()) {
                                return true;
                            }
                        }
                    }
                }
            }

        } catch (RuntimeException ex) {
            pendSincronizarSinResp = false;
        }
        return pendSincronizarSinResp;
    }

    private int CantidadContingPendSincronizar(int MinSinSincronizar) {

        int Cant = 0;
        try {
            File dir = new File(cfgCarpetaOperacion + File.separator + "XML" + File.separator + "Contingencia");
            if (dir.isDirectory()) {
                File[] filesList = dir.listFiles();
                if (filesList.length > 0) {
                    for (File file : filesList) {
                        if (file.isFile() && esArchivo(file.getAbsolutePath(), EmpRUT, String.valueOf(SucId))) {
                            Date date = new Date();
                            date.setMinutes(date.getMinutes() - MinSinSincronizar);
                            if (file.lastModified() < date.getDate()) {
                                Cant++;
                            }
                        }
                    }
                }
            }

        } catch (RuntimeException ex) {
            return -1;
        }
        return Cant;
    }

    private int CantidadLogsPendSincronizar(int MinSinSincronizar) {

        int Cant = 0;
        try {
            File dir = new File(cfgCarpetaOperacion + File.separator + "LOG");
            if (dir.isDirectory()) {
                File[] filesList = dir.listFiles();
                if (filesList.length > 0) {
                    for (File file : filesList) {
                        if (file.isFile() && esArchivo(file.getAbsolutePath(), EmpRUT, String.valueOf(SucId))) {
                            Date date = new Date();
                            date.setMinutes(date.getMinutes() - MinSinSincronizar);
                            if (file.lastModified() < date.getDate()) {
                                Cant++;
                            }
                        }
                    }
                }
            }

        } catch (RuntimeException ex) {
            return -1;
        }
        return Cant;
    }

    private boolean ExistenContingPendSincronizar(int MinSinSincronizar) {

        boolean pendSincronizarConting = false;
        try {
            File dir = new File(cfgCarpetaOperacion + File.separator + "XML" + File.separator + "Contingencia");
            if (dir.isDirectory()) {
                File[] filesList = dir.listFiles();
                if (filesList.length > 0) {
                    for (File file : filesList) {
                        if (file.isFile() && esArchivo(file.getAbsolutePath(), EmpRUT, String.valueOf(SucId))) {
                            Date date = new Date();
                            date.setMinutes(date.getMinutes() - MinSinSincronizar);
                            if (file.lastModified() < date.getDate()) {
                                return true;
                            }
                        }
                    }
                }
            }

        } catch (RuntimeException ex) {
            pendSincronizarConting = false;
        }
        return pendSincronizarConting;
    }

    public boolean ReservarNroCAE(tangible.RefObject<Integer> ErrorCod, tangible.RefObject<String> ErrorMsg, int TipoCFE, tangible.RefObject<Long> CAENroAutorizacion, tangible.RefObject<String> CAESerie, tangible.RefObject<Long> CAENroReservado) throws TAException {
        boolean tempReservarNroCAE = false;
        tangible.RefObject<Boolean> reserv = new RefObject<Boolean>(false);
        boolean ErrorReturn = true;
        tempReservarNroCAE = false;

        //Verifico que se haya inicializado el componente
        if (!Inicializado) {
            throw new TAException("Debe Inicializar la API primero invocando al metodo Inicializar", 4301);
        }

        try {
            ErrorMsg.argValue = "";
            ErrorCod.argValue = 0;
            if (!EmularGateway) {
                //***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
                Object[] DebugNamesArray = {"tangible.RefObject<Integer> ErrorCod", "tangible.RefObject<String> ErrorMsg", "int TipoCFE", "tangible.RefObject<Long> CAENroAutorizacion", "tangible.RefObject<String> CAESerie", "tangible.RefObject<Long> CAENroReservado"};
                Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, TipoCFE, CAENroAutorizacion.argValue, CAESerie.argValue, CAENroReservado.argValue};
                objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);
            }

            if (EmularGateway) {
                tempReservarNroCAE = true;
                CAENroAutorizacion.argValue = (long) 1;
                CAESerie.argValue = "A";
                CAENroReservado.argValue = (long) (new Random().nextFloat() + 1);
            } else {
                if (cfgModoFirma == ModoFirma.FirmaLocal) {
                    TAFirmarFactura DLLFirmarFactura = new TAFirmarFactura();
                    DLLFirmarFactura.setRutaConexion(sPuntoConexionBD);
                    DLLFirmarFactura.setProveedorConexion(cfgMotorBD.getValue());
                    ErrorMsg.argValue = DLLFirmarFactura.ReservarNroCAE(EmpRUT, SucId, CajId, TipoCFE, objConfig.getCANTDIASANTESCAEVENCER(), objConfig.getCAESPORCAVANCE(), reserv, CAENroAutorizacion, CAESerie, CAENroReservado, ErrorCod, VersionActual(), cfgUrlServidor, (short) TimeOutSincronizacion);
                    if (!ErrorMsg.argValue.equals("")) {
                        TAFACE2ApiEntidad.TALog objLog = new TAFACE2ApiEntidad.TALog(cfgCarpetaOperacion, EmpRUT, SucId, CajId);
                        if (ErrorCod.argValue == 9) {
                            objLog.EscribirLog(ErrorCod.argValue + " - " + ErrorMsg.argValue, TAFACE2ApiEntidad.TALog.enumTipoLog.LogErrorDeConexion);
                        } else {
                            objLog.EscribirLog(ErrorCod.argValue + " - " + ErrorMsg.argValue, TAFACE2ApiEntidad.TALog.enumTipoLog.LogError);
                        }
                        throw new TAException(ErrorMsg.argValue, 6002);
                    } else {
                        tempReservarNroCAE = true;
                    }
                } else {
                    WakeUpServer();
                    WSReservarNroCAE0203ExecuteResponse resp = new WSReservarNroCAE0203ExecuteResponse();
                    resp = executorWS.executeReservarNroCAE(EmpRUT, SucId, (short) TipoCFE);
                    CAENroAutorizacion.argValue = resp.getPcaenroautorizacion();

                    if (resp.isPerrorreturn()) {
                        tempReservarNroCAE = false;
                        throw new TAException(ErrorMsg.argValue, 1056);
                    } else {
                        tempReservarNroCAE = true;
                    }
                }
            }
        } catch (Exception ex) {
            ErrorCod.argValue = 2;
            ErrorMsg.argValue = ex.getMessage();
            TAFACE2ApiEntidad.TALog objLog = new TAFACE2ApiEntidad.TALog(cfgCarpetaOperacion, EmpRUT, SucId, CajId);
            if (ErrorCod.argValue == 9) {
                try {
                    objLog.EscribirLog(ex.toString() + " - " + getNombreMetodo(), TAFACE2ApiEntidad.TALog.enumTipoLog.LogErrorDeConexion);
                } catch (UnsupportedEncodingException ex1) {
                    throw new TAException(ex1.getMessage());
                }
            } else {
                try {
                    objLog.EscribirLog(ex.toString() + " - " + getNombreMetodo(), TAFACE2ApiEntidad.TALog.enumTipoLog.LogError);
                } catch (UnsupportedEncodingException ex1) {
                    throw new TAException(ex1.getMessage());
                }
            }
            return false;
        }
        return tempReservarNroCAE;
    }

    public boolean ReservaNroCAEEstaVigente(tangible.RefObject<Integer> ErrorCod, tangible.RefObject<String> ErrorMsg, tangible.RefObject<Long> CAENroAutorizacion, tangible.RefObject<String> CAESerie, tangible.RefObject<Long> CAENroReservado, tangible.RefObject<Boolean> ReservaVigente) throws TAException {

        boolean tempReservaNroCAEEstaVigente = false;

        //Verifico que se haya inicializado el componente
        if (!Inicializado) {
            throw new TAException("Debe Inicializar la API primero invocando al metodo Inicializar", 4302);
        }

        try {
            ErrorMsg.argValue = "";
            ErrorCod.argValue = 0;
            if (!EmularGateway) {
                //***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
                Object[] DebugNamesArray = {"tangible.RefObject<Integer> ErrorCod", "tangible.RefObject<String> ErrorMsg", "tangible.RefObject<Long> CAENroAutorizacion", "tangible.RefObject<String> CAESerie", "tangible.RefObject<Long> CAENroReservado", "tangible.RefObject<Boolean> ReservaVigente"};
                Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, CAENroAutorizacion.argValue, CAESerie.argValue, CAENroReservado.argValue, ReservaVigente.argValue};
                objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);
            }

            if (EmularGateway) {
                return true;
            } else {
                if (cfgModoFirma == ModoFirma.FirmaLocal) {
                    TAFirmarFactura DLLFirmarFactura = new TAFirmarFactura();
                    DLLFirmarFactura.setRutaConexion(sPuntoConexionBD);
                    DLLFirmarFactura.setProveedorConexion(cfgMotorBD.getValue());
                    tempReservaNroCAEEstaVigente = DLLFirmarFactura.ReservaNroCAEEstaVigente(ErrorCod, ErrorMsg, EmpRUT, SucId, CajId, CAENroAutorizacion.argValue, CAESerie.argValue, CAENroReservado.argValue, ReservaVigente, cfgUrlServidor, (short) cfgSegundosTimeout, (short) TimeOutSincronizacion, VersionActual(), objConfig.getMINUTOSCADUCIDADRESERVACAE());
                    if (tempReservaNroCAEEstaVigente == false) {
                        throw new TAException(ErrorMsg.argValue, 1057);
                    }
                } else {
                    WakeUpServer();

                    ReservaVigente.argValue = executorWS.executeReservaNroCAEEstaVigente(CAENroAutorizacion.argValue, new BigDecimal(CAENroReservado.argValue).intValueExact(), CAESerie.argValue, EmpRUT).isPerrorreturn();

                    if (ReservaVigente.argValue) {
                        tempReservaNroCAEEstaVigente = false;
                        throw new TAException(ErrorMsg.argValue, 1058);
                    } else {
                        tempReservaNroCAEEstaVigente = true;
                    }
                }
            }
        } catch (Exception ex) {
            ErrorCod.argValue = 2;
            ErrorMsg.argValue = ex.getMessage();
            TAFACE2ApiEntidad.TALog objLog = new TAFACE2ApiEntidad.TALog(cfgCarpetaOperacion, EmpRUT, SucId, CajId);
            if (ErrorCod.argValue == 9) {
                try {
                    objLog.EscribirLog(ex.toString() + " - " + getNombreMetodo(), TAFACE2ApiEntidad.TALog.enumTipoLog.LogErrorDeConexion);
                } catch (UnsupportedEncodingException ex1) {
                    throw new TAException(ex1.getMessage());
                }
            } else {
                try {
                    objLog.EscribirLog(ex.toString() + " - " + getNombreMetodo(), TAFACE2ApiEntidad.TALog.enumTipoLog.LogError);
                } catch (UnsupportedEncodingException ex1) {
                    throw new TAException(ex1.getMessage());
                }
            }
            return false;
        }
        return tempReservaNroCAEEstaVigente;
    }

    public boolean CancelarReservaNroCAE(tangible.RefObject<Integer> ErrorCod, tangible.RefObject<String> ErrorMsg, long CAENroAutorizacion, String CAESerie, long CAENroReservado) throws TAException {

        boolean tempCancelarReservaNroCAE = false;
        tangible.RefObject<Boolean> cancelado = new RefObject<Boolean>(false);

        //Verifico que se haya inicializado el componente
        if (!Inicializado) {
            throw new TAException("Debe Inicializar la API primero invocando al metodo Inicializar", 4303);
        }

        try {
            ErrorMsg.argValue = "";
            ErrorCod.argValue = 0;
            if (!EmularGateway) {
                //***ATENCION: Si se agregan/modifican/eliminan parametros se debe modificar este array.***
                Object[] DebugNamesArray = {"tangible.RefObject<Integer> ErrorCod", "tangible.RefObject<String> ErrorMsg", "long CAENroAutorizacion", "String CAESerie", "long CAENroReservado"};
                Object[] DebugValuesArray = {ErrorMsg.argValue, ErrorCod.argValue, CAENroAutorizacion, CAESerie, CAENroReservado};
                objDebug.EscribirDebug(getNombreMetodo(), DebugNamesArray, DebugValuesArray);
            }

            if (EmularGateway) {
                return true;
            } else {
                if (cfgModoFirma == ModoFirma.FirmaLocal) {
                    TAFirmarFactura DLLFirmarFactura = new TAFirmarFactura();
                    DLLFirmarFactura.setRutaConexion(sPuntoConexionBD);
                    DLLFirmarFactura.setProveedorConexion(cfgMotorBD.getValue());
                    String err = DLLFirmarFactura.CancelarNroCAEReservado(EmpRUT, SucId, CajId, CAENroAutorizacion, CAESerie, CAENroReservado, false, VersionActual(), cfgUrlServidor, (short) TimeOutSincronizacion, cancelado, ErrorCod);
                    ErrorMsg.argValue = "";
                    if (ErrorMsg.argValue == null || ErrorMsg.argValue.equals("")) {
                        tempCancelarReservaNroCAE = true;
                    }
                } else {
                    WakeUpServer();

                    boolean ErrorReturn = executorWS.executeCancelarNroCAEReservado(CAENroAutorizacion, (int) CAENroReservado, CAESerie, EmpRUT).isPerrorreturn();

                    if (ErrorReturn) {
                        tempCancelarReservaNroCAE = false;
                        throw new TAException(ErrorMsg.argValue, 1057);
                    } else {
                        tempCancelarReservaNroCAE = true;
                    }
                }
            }
        } catch (Exception ex) {
            ErrorCod.argValue = 2;
            ErrorMsg.argValue = ex.getMessage();
            TAFACE2ApiEntidad.TALog objLog = new TAFACE2ApiEntidad.TALog(cfgCarpetaOperacion, EmpRUT, SucId, CajId);
            if (ErrorCod.argValue == 9) {
                try {
                    objLog.EscribirLog(ex.toString() + " - " + getNombreMetodo(), TAFACE2ApiEntidad.TALog.enumTipoLog.LogErrorDeConexion);
                } catch (UnsupportedEncodingException ex1) {
                    throw new TAException(ex1.getMessage());
                }
            } else {
                try {
                    objLog.EscribirLog(ex.toString() + " - " + getNombreMetodo(), TAFACE2ApiEntidad.TALog.enumTipoLog.LogError);
                } catch (UnsupportedEncodingException ex1) {
                    throw new TAException(ex1.getMessage());
                }
            }
            return false;
        }
        return tempCancelarReservaNroCAE;
    }

    public int ObtenerModoFirma(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod) throws TAException {
        int tempObtenerModoFirma = 0;
        try {
            ErrorMsg.argValue = "";
            ErrorCod.argValue = 0;
            if (!Inicializado) {
                throw new TAException("Debe Inicializar la API primero invocando al metodo Inicializar", 4131);
            }
            if (!((objConfig == null))) {
                tempObtenerModoFirma = objConfig.getMODOFIRMA();
            } else {
                throw new Exception("No se pudo acceder a la configuracion.");
            }
        } catch (Exception ex) {
            ErrorCod.argValue = 8;
            ErrorMsg.argValue = ex.getMessage();
            tempObtenerModoFirma = 0;
        }
        return tempObtenerModoFirma;
    }

    public boolean GenerarQRCodeGX(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String Url, tangible.RefObject<String> ImagenQRBase64) throws TAException {
        try {
            //Validaciones
            if ((Url == null) || (Url.trim() == null || Url.trim().length() == 0)) {
                throw new TAException("Debe ingresar URL para generar el Codigo QR.", 1033);
            }
            ImagenQRBase64.argValue = encodeToString(generarQR(Url), "png");
            return true;

        } catch (Exception ex) {
            ErrorCod.argValue = 8;
            ErrorMsg.argValue = "Error generando QRCode: " + ex.getMessage();
            return false;
        }

    }

    public boolean GenerarQRCode(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String Url, tangible.RefObject<Object> ImagenQR) throws TAException {
        try {
            ErrorMsg.argValue = "";
            ErrorCod.argValue = 0;
            //Validaciones
            if ((Url == null) || (Url.trim() == null || Url.trim().length() == 0)) {
                throw new TAException("Debe ingresar URL para generar el Codigo QR.", 1033);
            }
            ImagenQR.argValue = new ImageIcon(generarQR(Url));
            return true;

        } catch (RuntimeException ex) {
            ErrorCod.argValue = 8;
            ErrorMsg.argValue = "Error generando QRCode: " + ex.getMessage();
            return false;
        }
    }

    public void ActivarEmulacionGatewayOK() {
        EmularGateway = true;
    }

    private boolean EmuladorFirmarFacturaOK(String ErrorMsg, tangible.RefObject<Integer> ErrorCod, tangible.RefObject<String> XMLRespuesta) throws TAException {

        try {
            ErrorMsg = "";
            ErrorCod.argValue = 0;
            XMLRESPUESTA xmlresp = new XMLRESPUESTA();
            java.util.Random rnd = new java.util.Random();
            xmlresp.setERRORMSG("MSG Error");
            xmlresp.setWARNINGMSG("MSG Warning");
            xmlresp.setFIRMADOOK((short) 1);
            //formato de fechaHora del XML
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
            xmlresp.setFIRMADOFCHHORA(formatter.format(new java.util.Date()));
            xmlresp.setCAENA(new Long("90120000168"));
            xmlresp.setCAENROINICIAL(1);
            xmlresp.setCAENROFINAL(9999999);
            Date dateCAEVenc = new Date();
            dateCAEVenc.setMonth(dateCAEVenc.getMonth() + 6);
            xmlresp.setCAEVENCIMIENTO(dateCAEVenc);
            xmlresp.setCAESERIE("A");
            int numeroAleatorio = (int) (xmlresp.getCAENROINICIAL() + rnd.nextInt((int) xmlresp.getCAENROFINAL()));
            xmlresp.setCAENRO(rnd.nextInt(numeroAleatorio));
            xmlresp.setCODSEGURIDAD("rUngF7");
            xmlresp.setURLPARAVERIFICARTEXTO("http://www.new-age-data.com/consultaCFE");
            xmlresp.setURLPARAVERIFICARQR("http://www.new-age-data.com/consultaCFE");
            //Devuevlo el XML parseado
            XMLRespuesta.argValue = xmlresp.ToXML();

            return true;
        } catch (RuntimeException ex) {
            ErrorCod.argValue = 8;
            ErrorMsg = ex.getMessage();
            return false;
        } catch (PropertyException ex) {
            throw new TAException(ex.getMessage());
        } catch (JAXBException ex) {
            throw new TAException(ex.getMessage());
        } catch (IOException ex) {
            throw new TAException(ex.getMessage());
        }
    }

    public boolean ConsultaRUCEmisorElectronico(String RUCEmisor) throws TAException {
        boolean tempConsultaRUCEmisorElectronico = false;
        //aca tengo que llamar al web service del gtw que me dice si el RUC que me estan pasando es el de un emisor electronico
        try {
            WakeUpServer();
            WSConsultarReceptorElectronicoExecuteResponse response = executorWS.executeConsultarReceptorElectronico(RUCEmisor);
            if (response.isPerrorreturn()) {
                throw new TAException(response.getPerrormessage());
            } else {
                if (response.isEsreceptorelectronico()) {
                    tempConsultaRUCEmisorElectronico = true;
                }
            }
        } catch (RuntimeException ex) {
            throw ex;
        }
        return tempConsultaRUCEmisorElectronico;
    }

    private WSSincronizacionParamsApi0205ExecuteResponse DescargarConfiguracionAPI(String EmpRUC, int SucId, int CajId) throws TAException {
        WSSincronizacionParamsApi0205ExecuteResponse Conf = null;

        Conf = executorWS.executeParamsApi(EmpRUC, (short) CajId, SucId);
        if (Conf != null && Conf.isPerrorreturn()) {
            throw new TAException(Conf.getPerrormessage() + "\r\n" + " Server: " + cfgUrlServidor, 4148);
        }
        return Conf;
    }



    /////UTILS METHODS
    public String getNombreMetodo() {
        //Retorna el nombre del metodo desde el cual se hace el llamado
        return new Exception().getStackTrace()[1].getMethodName();
    }

    public String convertirFechaXMLSincronizar(Date date) {
        //2017-05-02T12:47:52.3558396-03:00
        String form = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSSS").format(date);
        String concat = form.split(" ")[0] + "T" + form.split(" ")[1] + "-03:00";
        return concat;
    }

    public BufferedImage generarQR(String datos) {
        BitMatrix matrix;
        Writer writer = new QRCodeWriter();
        try {

            matrix = writer.encode(datos, BarcodeFormat.QR_CODE, 150, 150);

        } catch (WriterException e) {
            e.printStackTrace(System.err);
            return null;
        }

        BufferedImage image = new BufferedImage(150,
                150, BufferedImage.TYPE_INT_RGB);

        for (int y = 0; y < 150; y++) {
            for (int x = 0; x < 150; x++) {
                int grayValue = (matrix.get(x, y) ? 0 : 1) & 0xff;
                image.setRGB(x, y, (grayValue == 0 ? 0 : 0xFFFFFF));
            }
        }
        return image;
    }

    private String encodeToString(BufferedImage image, String type) {
        String imageString = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, type, bos);
            byte[] imageBytes = bos.toByteArray();

            imageString = DatatypeConverter.printBase64Binary(imageBytes);

            bos.close();
        } catch (IOException e) {
        }
        return imageString;
    }

    public boolean esArchivo(String pathFile, String EmpRUT, String SucId) {

        boolean esArchivo = false;
        String[] list = pathFile.split(Pattern.quote(File.separator));
        if (list.length > 2) {
            String[] places = list[4].split("_");
            if (places.length > 2) {
                if (places[0].equals(EmpRUT) && places[1].equals(SucId)) {
                    esArchivo = true;
                }
            }
        }
        return esArchivo;
    }

    //Obtengo si hay diferencia entre la ultima modificacion que se ralizo al fichero de configuracion con el dia de la PC, si la diferencia entre las dos fechas no es 0, quiere decir
    //que el fichero de configuracion de la sucursal no se ha modificado en el dia, lo que quiere decir o q la tarea no se esta ejecutando o que no se esta sincronizando con el gateway.
    public boolean HayDiffUltimaModificacionConfigSuc(String CarpetaOperacion, String EmpRUC, int SucId) {
        boolean DiasDiffUltimaModificacionConfigSuc = false;
        try {
            objConfig = objConfig.CargarConfiguracion(CarpetaOperacion, EmpRUC, SucId);
            if (objConfig != null) {
            File fichero = new File(CarpetaOperacion + EmpRUC + "_" + SucId + "_" + "TAFEApi.config");
            long ms = fichero.lastModified();
            Date d = new Date(ms);
            d = new Date(d.getYear(),d.getMonth(),d.getDate());
            Date fechaAComp = new Date();
            fechaAComp = new Date(fechaAComp.getYear(),fechaAComp.getMonth(),fechaAComp.getDate());
            int diff = d.compareTo(fechaAComp);
            if (diff != 0) {
                DiasDiffUltimaModificacionConfigSuc = true;
            }
        }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TAFEApi.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TAFEApi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return DiasDiffUltimaModificacionConfigSuc;
    }
}
