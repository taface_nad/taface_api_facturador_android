/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Fachada;

/**
 *
 * @author DanielCera
 */
public interface ITAFEApi_v0206 {

	boolean InicializarImpresion(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String NombreImpresora, int TipoImpresora);

	boolean Inicializar(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String UrlServidorTAFirmoGateway, int SegundosTimeout, double EmpresaRUT, int SucursalId, int CajaId);

	boolean Inicializar(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String UrlServidorTAFirmoGateway, int SegundosTimeout, double EmpresaRUT, int SucursalId, int CajaId, String RutaCarpetaOperacion);

	int ObtenerModoFirma(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod);

	boolean FirmarFactura(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String XMLFactura, tangible.RefObject<String> XMLRespuesta);

	boolean FacturaEsAnulable(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, double EmpresaRUC, short SucursalId, short Cajaid, int TipoDeComprobante, String SerieComprobante, int NroComprobante, boolean Anulable, String Motivo);

	boolean AnularFactura(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, double EmpresaRUC, short SucursalId, short Cajaid, int TipoDeComprobante, String SerieComprobante, int NroComprobante);

	boolean ReimprimirFactura(tangible.RefObject<String> ErrorMsg, int ErrorCod, double EmpresaRUC, short SucursalId, short Cajaid, int TipoDeComprobante, String SerieComprobante, int NroComprobante);

	boolean ImprimirFactura(tangible.RefObject<String> ErrorMsg, int ErrorCod, double EmpresaRUC, short SucursalId, short Cajaid, int TipoDeComprobante, String SerieComprobante, int NroComprobante);

	boolean Sincronizar(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod);

	boolean CerrarCaja(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String UrlServidorTAFirmoGateway, double EmpresaRUT, int SucursalId, int CajaId);

	boolean CerrarCaja(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String UrlServidorTAFirmoGateway, double EmpresaRUT, int SucursalId, int CajaId, String RutaCarpetaOperacion);

	boolean ExisteCFC(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, int TipoCFC, String SerieCFC, double NroCFC, double NroAutorizacionCFC, double NroDesdeCFC, double NroHastaCFC, java.util.Date FechaVencimientoCFC);

	boolean FirmarFacturaConReserva(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String XMLFactura, tangible.RefObject<String> XMLRespuesta, double CAENroAutorizacion, String CAESerie, double CAENroReservado);

	boolean ReservarNroCAE(tangible.RefObject<Integer> ErrorCod, tangible.RefObject<String> ErrorMsg, int TipoCFE, tangible.RefObject<Double> CAENroAutorizacion, tangible.RefObject<String> CAESerie, tangible.RefObject<Double> CAENroReservado);

	boolean ReservaNroCAEEstaVigente(tangible.RefObject<Integer> ErrorCod, tangible.RefObject<String> ErrorMsg, tangible.RefObject<Double> CAENroAutorizacion, String CAESerie, double CAENroReservado, tangible.RefObject<Boolean> ReservaVigente);

	boolean CancelarReservaNroCAE(tangible.RefObject<Integer> ErrorCod, tangible.RefObject<String> ErrorMsg, double CAENroAutorizacion, String CAESerie, double CAENroReservado);

	boolean GenerarQRCodeGX(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String Url, tangible.RefObject<String> ImagenQRBase64);

	boolean GenerarQRCode(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String Url, tangible.RefObject<Object> ImagenQR);

	boolean GenerarQRCodeVB6(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String Url, tangible.RefObject<Object> IPD);

	void ActivarEmulacionGatewayOK();
}
