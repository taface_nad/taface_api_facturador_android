/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Fachada;

import Negocio.TAFEApi;
import TAFACE2ApiEntidad.TAException;

/**
 *
 * @author DanielCera
 */
public class TAFEApi_v0206 {

    private TAFEApi objTAFEApi;

    public TAFEApi_v0206(){
        objTAFEApi = new TAFEApi();
    }

    public boolean Inicializar(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String UrlServidorTAFirmoGateway, int SegundosTimeout, long EmpresaRUT, int SucursalId, int CajaId) throws TAException {
        return Inicializar(ErrorMsg, ErrorCod, UrlServidorTAFirmoGateway, SegundosTimeout, EmpresaRUT, SucursalId, CajaId, "");
    }

    public boolean Inicializar(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String UrlServidorTAFirmoGateway, int SegundosTimeout, long EmpresaRUT, int SucursalId, int CajaId, String RutaCarpetaOperacion) throws TAException {
        return objTAFEApi.Inicializar(ErrorMsg, ErrorCod, UrlServidorTAFirmoGateway, SegundosTimeout, EmpresaRUT, SucursalId, CajaId, RutaCarpetaOperacion);
    }

    public int ObtenerModoFirma(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod) throws TAException {
        return objTAFEApi.ObtenerModoFirma(ErrorMsg, ErrorCod);
    }

    public boolean FirmarFactura(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String XMLFactura, tangible.RefObject<String> XMLRespuesta) throws TAException {
        return objTAFEApi.FirmarFactura(ErrorMsg, ErrorCod, XMLFactura, XMLRespuesta, (long)0, "", (long)0);
    }

    public boolean FacturaEsAnulable(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, long EmpresaRUC, short SucursalId, short Cajaid, int TipoDeComprobante, String SerieComprobante, int NroComprobante, boolean Anulable, String Motivo) throws TAException {
        return objTAFEApi.FacturaEsAnulable(ErrorMsg, ErrorCod, EmpresaRUC, SucursalId, Cajaid, TipoDeComprobante, SerieComprobante, NroComprobante);
    }

    public boolean AnularFactura(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, long EmpresaRUC, short SucursalId, short Cajaid, int TipoDeComprobante, String SerieComprobante, int NroComprobante) {
        return objTAFEApi.AnularFactura(ErrorMsg, ErrorCod, EmpresaRUC, SucursalId, Cajaid, TipoDeComprobante, SerieComprobante, NroComprobante);
    }

    public boolean Sincronizar(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod) throws TAException {
        return objTAFEApi.Sincronizar(ErrorMsg, ErrorCod);
    }

    public boolean CerrarCaja(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String UrlServidorTAFirmoGateway, long EmpresaRUT, int SucursalId, int CajaId) throws TAException {
        return CerrarCaja(ErrorMsg, ErrorCod, UrlServidorTAFirmoGateway, EmpresaRUT, SucursalId, CajaId, "");
    }

    public boolean CerrarCaja(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String UrlServidorTAFirmoGateway, long EmpresaRUT, int SucursalId, int CajaId, String RutaCarpetaOperacion) throws TAException {
        return objTAFEApi.CerrarCaja(ErrorMsg, ErrorCod, UrlServidorTAFirmoGateway, EmpresaRUT, SucursalId, CajaId, RutaCarpetaOperacion);
    }

    public boolean ExisteCFC(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, int TipoCFC, String SerieCFC, long NroCFC, long NroAutorizacionCFC, long NroDesdeCFC, long NroHastaCFC, java.util.Date FechaVencimientoCFC) throws TAException {
        return objTAFEApi.ExisteCFC(ErrorMsg, ErrorCod, TipoCFC, SerieCFC, NroCFC, NroAutorizacionCFC, NroDesdeCFC, NroHastaCFC, FechaVencimientoCFC);
    }

    public boolean FirmarFacturaConReserva(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String XMLFactura, tangible.RefObject<String> XMLRespuesta, long CAENroAutorizacion, String CAESerie, long CAENroReservado) throws TAException {
        return objTAFEApi.FirmarFactura(ErrorMsg, ErrorCod, XMLFactura, XMLRespuesta, CAENroAutorizacion, CAESerie, CAENroReservado);
    }

    public boolean ReservarNroCAE(tangible.RefObject<Integer> ErrorCod, tangible.RefObject<String> ErrorMsg, int TipoCFE, tangible.RefObject<Long> CAENroAutorizacion, tangible.RefObject<String> CAESerie, tangible.RefObject<Long> CAENroReservado) throws TAException {
        return objTAFEApi.ReservarNroCAE(ErrorCod, ErrorMsg, TipoCFE, CAENroAutorizacion, CAESerie, CAENroReservado);
    }

    public boolean ReservaNroCAEEstaVigente(tangible.RefObject<Integer> ErrorCod, tangible.RefObject<String> ErrorMsg, tangible.RefObject<Long> CAENroAutorizacion, String CAESerie, long CAENroReservado, tangible.RefObject<Boolean> ReservaVigente) throws TAException {
        tangible.RefObject<String> tempRef_CAESerie = new tangible.RefObject<String>(CAESerie);
        tangible.RefObject<Long> tempRef_CAENroReservado = new tangible.RefObject<Long>(CAENroReservado);
        boolean tempVar = objTAFEApi.ReservaNroCAEEstaVigente(ErrorCod, ErrorMsg, CAENroAutorizacion, tempRef_CAESerie, tempRef_CAENroReservado, ReservaVigente);
        CAESerie = tempRef_CAESerie.argValue;
        CAENroReservado = tempRef_CAENroReservado.argValue;
        return tempVar;
    }

    public boolean CancelarReservaNroCAE(tangible.RefObject<Integer> ErrorCod, tangible.RefObject<String> ErrorMsg, long CAENroAutorizacion, String CAESerie, long CAENroReservado) throws TAException {
        return objTAFEApi.CancelarReservaNroCAE(ErrorCod, ErrorMsg, CAENroAutorizacion, CAESerie, CAENroReservado);
    }

    public boolean GenerarQRCodeGX(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String Url, tangible.RefObject<String> ImagenQRBase64) throws TAException {
        return objTAFEApi.GenerarQRCodeGX(ErrorMsg, ErrorCod, Url, ImagenQRBase64);
    }

    public boolean GenerarQRCode(tangible.RefObject<String> ErrorMsg, tangible.RefObject<Integer> ErrorCod, String Url, tangible.RefObject<Object> ImagenQR) throws TAException {
        return objTAFEApi.GenerarQRCode(ErrorMsg, ErrorCod, Url, ImagenQR);
    }

    public void ActivarEmulacionGatewayOK() {
        objTAFEApi.ActivarEmulacionGatewayOK();
    }
}
