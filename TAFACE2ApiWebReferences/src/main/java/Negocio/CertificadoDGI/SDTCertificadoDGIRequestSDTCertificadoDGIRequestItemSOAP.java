package Negocio.CertificadoDGI;

import java.util.Hashtable;
import org.ksoap2.serialization.*;

public class SDTCertificadoDGIRequestSDTCertificadoDGIRequestItemSOAP extends AttributeContainer implements KvmSerializable
{

    
    public Integer EmpId=0;
    
    public Integer CertDGIId=0;
    
    public String CertDGIRowGUID;
    private transient java.lang.Object __source;    
    

    
    
    
    public void loadFromSoap(java.lang.Object paramObj,ExtendedSoapSerializationEnvelopeSOAP __envelope)
    {
        if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;
        __source=inObj; 
        if(inObj instanceof SoapObject)
        {
            SoapObject soapObject=(SoapObject)inObj;
            int size = soapObject.getPropertyCount();
            for (int i0=0;i0< size;i0++)
            {
                PropertyInfo info=soapObject.getPropertyInfo(i0);
                if(!loadProperty(info,soapObject,__envelope))
                {
                }
            } 
        }

    }

    
    protected boolean loadProperty(PropertyInfo info,SoapObject soapObject,ExtendedSoapSerializationEnvelopeSOAP __envelope)
    {
        java.lang.Object obj = info.getValue();
        if (info.name.equals("EmpId"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.EmpId = Integer.parseInt(j.toString());
                    }
                }
                else if (obj instanceof Integer){
                    this.EmpId = (Integer)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CertDGIId"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CertDGIId = Integer.parseInt(j.toString());
                    }
                }
                else if (obj instanceof Integer){
                    this.CertDGIId = (Integer)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CertDGIRowGUID"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CertDGIRowGUID = j.toString();
                    }
                }
                else if (obj instanceof String){
                    this.CertDGIRowGUID = (String)obj;
                }
            }
            return true;
        }
        return false;
    }
    
    public java.lang.Object getOriginalXmlSource()
    {
        return __source;
    }    
    

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        if(propertyIndex==0)
        {
            return EmpId;
        }
        if(propertyIndex==1)
        {
            return CertDGIId;
        }
        if(propertyIndex==2)
        {
            return CertDGIRowGUID;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 3;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "EmpId";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "CertDGIId";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "CertDGIRowGUID";
            info.namespace= "TAFACE";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    
}

