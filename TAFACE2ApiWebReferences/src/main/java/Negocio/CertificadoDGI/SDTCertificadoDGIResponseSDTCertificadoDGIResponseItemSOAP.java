package Negocio.CertificadoDGI;

import java.util.Hashtable;
import org.ksoap2.serialization.*;

public class SDTCertificadoDGIResponseSDTCertificadoDGIResponseItemSOAP extends AttributeContainer implements KvmSerializable
{

    
    public Integer EmpId=0;
    
    public Integer CertDGIId=0;
    
    public java.util.Date CertDGIVigencia;
    
    public java.util.Date CertDGIVencimiento;
    
    public String CertDGIArchivo;
    
    public java.util.Date CertDGIFchHora;
    
    public String CertDGICN;
    
    public String CertDGIRowGUID;
    
    public Boolean CertDGIEliminado=false;
    private transient java.lang.Object __source;    
    

    
    
    
    public void loadFromSoap(java.lang.Object paramObj,ExtendedSoapSerializationEnvelopeSOAP __envelope)
    {
        if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;
        __source=inObj; 
        if(inObj instanceof SoapObject)
        {
            SoapObject soapObject=(SoapObject)inObj;
            int size = soapObject.getPropertyCount();
            for (int i0=0;i0< size;i0++)
            {
                PropertyInfo info=soapObject.getPropertyInfo(i0);
                if(!loadProperty(info,soapObject,__envelope))
                {
                }
            } 
        }

    }

    
    protected boolean loadProperty(PropertyInfo info,SoapObject soapObject,ExtendedSoapSerializationEnvelopeSOAP __envelope)
    {
        java.lang.Object obj = info.getValue();
        if (info.name.equals("EmpId"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.EmpId = Integer.parseInt(j.toString());
                    }
                }
                else if (obj instanceof Integer){
                    this.EmpId = (Integer)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CertDGIId"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CertDGIId = Integer.parseInt(j.toString());
                    }
                }
                else if (obj instanceof Integer){
                    this.CertDGIId = (Integer)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CertDGIVigencia"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CertDGIVigencia = HelperSOAP.ConvertFromWebService(j.toString());
                    }
                }
                else if (obj instanceof java.util.Date){
                    this.CertDGIVigencia = (java.util.Date)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CertDGIVencimiento"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CertDGIVencimiento = HelperSOAP.ConvertFromWebService(j.toString());
                    }
                }
                else if (obj instanceof java.util.Date){
                    this.CertDGIVencimiento = (java.util.Date)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CertDGIArchivo"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CertDGIArchivo = j.toString();
                    }
                }
                else if (obj instanceof String){
                    this.CertDGIArchivo = (String)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CertDGIFchHora"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CertDGIFchHora = HelperSOAP.ConvertFromWebService(j.toString());
                    }
                }
                else if (obj instanceof java.util.Date){
                    this.CertDGIFchHora = (java.util.Date)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CertDGICN"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CertDGICN = j.toString();
                    }
                }
                else if (obj instanceof String){
                    this.CertDGICN = (String)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CertDGIRowGUID"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CertDGIRowGUID = j.toString();
                    }
                }
                else if (obj instanceof String){
                    this.CertDGIRowGUID = (String)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CertDGIEliminado"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CertDGIEliminado = new Boolean(j.toString());
                    }
                }
                else if (obj instanceof Boolean){
                    this.CertDGIEliminado = (Boolean)obj;
                }
            }
            return true;
        }
        return false;
    }
    
    public java.lang.Object getOriginalXmlSource()
    {
        return __source;
    }    
    

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return EmpId;
        }
        if(propertyIndex==1)
        {
            return CertDGIId;
        }
        if(propertyIndex==2)
        {
            return this.CertDGIVigencia!=null?HelperSOAP.getDateFormat().format(this.CertDGIVigencia):SoapPrimitive.NullNilElement;
        }
        if(propertyIndex==3)
        {
            return this.CertDGIVencimiento!=null?HelperSOAP.getDateFormat().format(this.CertDGIVencimiento):SoapPrimitive.NullNilElement;
        }
        if(propertyIndex==4)
        {
            return CertDGIArchivo;
        }
        if(propertyIndex==5)
        {
            return this.CertDGIFchHora!=null?HelperSOAP.getDateTimeFormat().format(this.CertDGIFchHora):SoapPrimitive.NullNilElement;
        }
        if(propertyIndex==6)
        {
            return CertDGICN;
        }
        if(propertyIndex==7)
        {
            return CertDGIRowGUID;
        }
        if(propertyIndex==8)
        {
            return CertDGIEliminado;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 9;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "EmpId";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "CertDGIId";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "CertDGIVigencia";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==3)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "CertDGIVencimiento";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==4)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "CertDGIArchivo";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==5)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "CertDGIFchHora";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==6)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "CertDGICN";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==7)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "CertDGIRowGUID";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==8)
        {
            info.type = PropertyInfo.BOOLEAN_CLASS;
            info.name = "CertDGIEliminado";
            info.namespace= "TAFACE";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    
}

