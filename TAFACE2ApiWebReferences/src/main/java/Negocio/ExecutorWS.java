package Negocio;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import Negocio.CAEParte.SDTCAEParteRequest206SDTCAEParteRequest206Item;
import Negocio.CAEParte.WSSincronizacionCAEParte_0206SoapBinding;
import Negocio.CAEParte.WSSincronizacionCAEParte_0206ExecuteResponse;
import Negocio.CAEParte.XMLCAEPARTEREQUEST;
import Negocio.CAEParteAnulado.WSSincronizacionCAEParteAnulada0200ExecuteResponseSOAP;
import Negocio.CAEParteAnulado.WSSincronizacionCAEParteAnulada0200SoapBindingSOAP;
import Negocio.CAEParteUtilizado.WSSincronizacionCAEParteUtilizado0200ExecuteResponseSOAP;
import Negocio.CAEParteUtilizado.WSSincronizacionCAEParteUtilizado0200SoapBindingSOAP;
import Negocio.CAEParteUtilizadoAnulado.WSSincronizacionCAEParteUtilizadoAnulado0203ExecuteResponseSOAP;
import Negocio.CAEParteUtilizadoAnulado.WSSincronizacionCAEParteUtilizadoAnulado0203SoapBindingSOAP;
import Negocio.CancelarNroCaeReservado.WSCancelarNroCAEReservado0203ExecuteResponseSOAP;
import Negocio.CancelarNroCaeReservado.WSCancelarNroCAEReservado0203SoapBindingSOAP;
import Negocio.Certificado.SDTCertificadoRequestSDTCertificadoRequestItemSOAP;
import Negocio.Certificado.WSSincronizacionCertificados0200ExecuteResponseSOAP;
import Negocio.Certificado.WSSincronizacionCertificados0200SoapBindingSOAP;
import Negocio.Certificado.XMLCERTIFICADOREQUESTSOAP;
import Negocio.CertificadoDGI.SDTCertificadoDGIRequestSDTCertificadoDGIRequestItemSOAP;
import Negocio.CertificadoDGI.SDTCertificadoDGIRequestSOAP;
import Negocio.CertificadoDGI.WSSincronizacionCertificadosDGI0202ExecuteResponseSOAP;
import Negocio.CertificadoDGI.WSSincronizacionCertificadosDGI0202SoapBindingSOAP;
import Negocio.Comprobante.WSSincronizacionComprobantes0200ExecuteResponseSOAP;
import Negocio.Comprobante.WSSincronizacionComprobantes0200SoapBindingSOAP;
import Negocio.ConfirmarFirmarComprobante.WSConfirmarFirmaComprobante0200ExecuteResponseSOAP;
import Negocio.ConfirmarFirmarComprobante.WSConfirmarFirmaComprobante0200SoapBindingSOAP;
import Negocio.ConsultarEmisorElectronico.WSConsultarReceptorElectronicoExecuteResponseSOAP;
import Negocio.ConsultarEmisorElectronico.WSConsultarReceptorElectronicoSoapBindingSOAP;
import Negocio.Empresa.WSSincronizacionEmpresas0202SoapBindingSOAP;
import Negocio.Empresa.WSSincronizacionEmpresas_0202ExecuteResponseSOAP;
import Negocio.EnviarContingencia.WSEnviarContingencia0200ExecuteResponseSOAP;
import Negocio.EnviarContingencia.WSEnviarContingencia0200SoapBindingSOAP;
import Negocio.EnviarCteSinRespuesta.WSEnviarCteSinRespuesta0200ExecuteResponseSOAP;
import Negocio.EnviarCteSinRespuesta.WSEnviarCteSinRespuesta0200SoapBindingSOAP;
import Negocio.FechaSistema.WSGetFechaSistema0205ExecuteResponseSOAP;
import Negocio.FechaSistema.WSGetFechaSistema0205SoapBindingSOAP;
import Negocio.FirmarComprobante.WSFirmarComprobante0200ExecuteResponseSOAP;
import Negocio.FirmarComprobante.WSFirmarComprobante0200SoapBindingSOAP;
import Negocio.FirmarComprobanteConCaeNroReservado.WSFirmarComprobanteConNroCAEReservado0203ExecuteResponseSOAP;
import Negocio.FirmarComprobanteConCaeNroReservado.WSFirmarComprobanteConNroCAEReservado0203SoapBindingSOAP;
import Negocio.KeepsAlive.WSKeepAlive0200ExecuteResponseSOAP;
import Negocio.KeepsAlive.WSKeepAlive0200SoapBindingSOAP;
import Negocio.LogErrores.WSSincronizacionLogErrores0200ExecuteResponseSOAP;
import Negocio.LogErrores.WSSincronizacionLogErrores0200SoapBindingSOAP;
import Negocio.ParamsApi205.WSSincronizacionParamsApi0205ExecuteResponse205SOAP;
import Negocio.ParamsApi205.WSSincronizacionParamsApi0205SoapBinding205SOAP;
import Negocio.ParamsApi206.WSSincronizacionParamsApi0206ExecuteResponse206SOAP;
import Negocio.ParamsApi206.WSSincronizacionParamsApi0206SoapBinding206SOAP;
import Negocio.ReservaNroCaeEstaVigente.WSReservaNroCAEEstaVigente0203ExecuteResponseSOAP;
import Negocio.ReservaNroCaeEstaVigente.WSReservaNroCAEEstaVigente0203SoapBindingSOAP;
import Negocio.ReservarNroCAE.WSReservarNroCAE0203ExecuteResponseSOAP;
import Negocio.ReservarNroCAE.WSReservarNroCAE0203SoapBindingSOAP;
import Negocio.ScriptBDApi.WSSincronizacionScriptBDApi0201ExecuteResponseSOAP;
import Negocio.ScriptBDApi.WSSincronizacionScriptBDApi0201SoapBindingSOAP;
import Negocio.SincronizacionCFC.SDTCFCRequestSDTCFCRequestItemSOAP;
import Negocio.SincronizacionCFC.SDTCFCRequestSOAP;
import Negocio.SincronizacionCFC.WSSincronizacionCFC0200ExecuteResponseSOAP;
import Negocio.SincronizacionCFC.WSSincronizacionCFC0200SoapBindingSOAP;
import Negocio.Sucursal.WSSincronizacionSucursales0205ExecuteResponseSOAP;
import Negocio.Sucursal.WSSincronizacionSucursales0205SoapBindingSOAP;
import Negocio.WSInterface.CAEParte.SDTCAEParteResponse206SDTCAEParteResponse206Item;
import Negocio.WSInterface.CAEParte.SDTCAEParteResponse206SDTCAEParteResponse206ItemCAE;
import Negocio.WSInterface.CAEParte.WSSincronizacionCAEParte0206ExecuteResponse;
import Negocio.WSInterface.CAEParte.XMLCAEPARTERESPONSE;
import Negocio.WSInterface.CAEParteAnulado.WSSincronizacionCAEParteAnulada0200ExecuteResponse;
import Negocio.WSInterface.CAEParteUtilizado.WSSincronizacionCAEParteUtilizado0200ExecuteResponse;
import Negocio.WSInterface.CAEParteUtilizadoAnulado.WSSincronizacionCAEParteUtilizadoAnulado0203ExecuteResponse;
import Negocio.WSInterface.CancelarNroCAEReservado.WSCancelarNroCAEReservado0203ExecuteResponse;
import Negocio.WSInterface.Certificado.SDTCertificadoResponseSDTCertificadoResponseItem;
import Negocio.WSInterface.Certificado.WSSincronizacionCertificados0200ExecuteResponse;
import Negocio.WSInterface.Certificado.XMLCERTIFICADOREQUEST;
import Negocio.WSInterface.Certificado.XMLCERTIFICADORESPONSE;
import Negocio.WSInterface.CertificadoDGI.SDTCertificadoDGIRequest;
import Negocio.WSInterface.CertificadoDGI.SDTCertificadoDGIResponse;
import Negocio.WSInterface.CertificadoDGI.SDTCertificadoDGIResponseSDTCertificadoDGIResponseItem;
import Negocio.WSInterface.CertificadoDGI.WSSincronizacionCertificadosDGI0202ExecuteResponse;
import Negocio.WSInterface.Comprobante.WSSincronizacionComprobantes0200ExecuteResponse;
import Negocio.WSInterface.ConfirmarFirmaComprobante.WSConfirmarFirmaComprobante0200ExecuteResponse;
import Negocio.WSInterface.ConsultarReceptorElectronico.WSConsultarReceptorElectronicoExecuteResponse;
import Negocio.WSInterface.Empresa.SDTEmpresaResponse202SDTEmpresaResponse202Item;
import Negocio.WSInterface.Empresa.WSSincronizacionEmpresas0202ExecuteResponse;
import Negocio.WSInterface.Empresa.XMLEMPRESARESPONSE;
import Negocio.WSInterface.EnviarContingencia.WSEnviarContingencia0200ExecuteResponse;
import Negocio.WSInterface.EnviarCteSinRespuesta.WSEnviarCteSinRespuesta0200ExecuteResponse;
import Negocio.WSInterface.FirmarComprobante.WSFirmarComprobante0200ExecuteResponse;
import Negocio.WSInterface.FirmarComprobanteConNroCAEReservado.WSFirmarComprobanteConNroCAEReservado0203ExecuteResponse;
import Negocio.WSInterface.LogErrores.WSSincronizacionLogErrores0200ExecuteResponse;
import Negocio.WSInterface.ParamsApi.PARAMETROSTAFEAPI205;
import Negocio.WSInterface.ParamsApi.PARAMETROSTAFEAPI206;
import Negocio.WSInterface.ParamsApi.WSSincronizacionParamsApi0205ExecuteResponse;
import Negocio.WSInterface.ParamsApi.WSSincronizacionParamsApi0206ExecuteResponse;
import Negocio.WSInterface.ReservaNroCAEEstaVigente.WSReservaNroCAEEstaVigente0203ExecuteResponse;
import Negocio.WSInterface.ReservarNroCAE.WSReservarNroCAE0203ExecuteResponse;
import Negocio.WSInterface.SincronizacionCFC.SDTCFCRequestSDTCFCRequestItem;
import Negocio.WSInterface.SincronizacionCFC.SDTCFCResponse;
import Negocio.WSInterface.SincronizacionCFC.SDTCFCResponseSDTCFCResponseItem;
import Negocio.WSInterface.SincronizacionCFC.WSSincronizacionCFC0200ExecuteResponse;
import Negocio.WSInterface.Sucursal.SDTSucursalResponse205SDTSucursalResponse205Item;
import Negocio.WSInterface.Sucursal.SDTSucursalResponse205SDTSucursalResponse205ItemCajasItem;
import Negocio.WSInterface.Sucursal.WSSincronizacionSucursales0205ExecuteResponse;
import Negocio.WSInterface.Sucursal.XMLSUCURSALRESPONSE;
import Negocio.WSInterface.VersionActual.WSWizardVersionActual0203ExecuteResponse;
import Negocio.WakeUp.WSWakeUp0200SoapBindingSOAP;
import Negocio.WizardVersionActual.WSWizardVersionActual0203SoapBindingSOAP;


/**
 *
 * @author danielcera
 */

public class ExecutorWS {

    //Parametro para setear la URL del servidor
    private String URLServer = "";

    public String getURLServer() {
        return URLServer;
    }

    public void setURLServer(String URLServ) {
        URLServer = URLServ;
    }

    ////////////////////////////////WEB SERVICES TAFACEAPIFIRMAR///////////////////////////////////////////////

    private WSGetFechaSistema0205ExecuteResponseSOAP executeFechaSistemaWS() {
        final WSGetFechaSistema0205ExecuteResponseSOAP[] response = {new WSGetFechaSistema0205ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "aWSGetFechaSistema_0205.aspx?wsdl";

                    WSGetFechaSistema0205SoapBindingSOAP service = new WSGetFechaSistema0205SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private Boolean executeWakeUpWS() {
        final Boolean response[] = new Boolean[1];
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "awswakeup_0200.aspx?wsdl";

                    WSWakeUp0200SoapBindingSOAP service = new WSWakeUp0200SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0].booleanValue();
    }

    private WSSincronizacionCAEParte_0206ExecuteResponse executeCAEParteWS(final int EmpId, final String EmpRUT, final int SucId, final int CajId, final XMLCAEPARTEREQUEST setCAEParteenBD) {
        final WSSincronizacionCAEParte_0206ExecuteResponse[] response = {new WSSincronizacionCAEParte_0206ExecuteResponse()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "aWSSincronizacionCAEParte_0206.aspx?wsdl";

                    WSSincronizacionCAEParte_0206SoapBinding service = new WSSincronizacionCAEParte_0206SoapBinding(URLserv);

                    try {
                        response[0] =  service.Execute(EmpId, EmpRUT,SucId, CajId, setCAEParteenBD);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSSincronizacionCAEParteAnulada0200ExecuteResponseSOAP executeCAEParteAnuladoWS(final int PCAEId, final int PCAEParteId, final int PEmpId) {
        final WSSincronizacionCAEParteAnulada0200ExecuteResponseSOAP[] response = {new WSSincronizacionCAEParteAnulada0200ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "aWSSincronizacionCAEParteAnulada_0200.aspx?wsdl";

                    WSSincronizacionCAEParteAnulada0200SoapBindingSOAP service = new WSSincronizacionCAEParteAnulada0200SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(PEmpId, PCAEId, PCAEParteId);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSSincronizacionCAEParteUtilizado0200ExecuteResponseSOAP executeCAEParteUtilizadoWS(final int PCaeId, final Date PCAEParteFchUtilizado, final int PCAEParteId, final int PCAEParteUtilNroDesde,
                                                                                                final int PCAEParteUtilNroHasta, final int PEmpid) {
        final WSSincronizacionCAEParteUtilizado0200ExecuteResponseSOAP[] response = {new WSSincronizacionCAEParteUtilizado0200ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "awssincronizacioncaeparteutilizado_0200.aspx?wsdl";

                    WSSincronizacionCAEParteUtilizado0200SoapBindingSOAP service = new WSSincronizacionCAEParteUtilizado0200SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(PEmpid,PCaeId, PCAEParteId, PCAEParteFchUtilizado,PCAEParteUtilNroDesde, PCAEParteUtilNroHasta);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSSincronizacionCAEParteUtilizadoAnulado0203ExecuteResponseSOAP executeCAEParteUtilizadoAnuladoWS(final int PCAEId, final int PCAEParteAnulNro, final Date PCAEPartefchutilizado,
                                                                                                              final int PCAEParteid, final int PEmpId, final boolean PRestaurarAnulacion) {
        final WSSincronizacionCAEParteUtilizadoAnulado0203ExecuteResponseSOAP[] response = {new WSSincronizacionCAEParteUtilizadoAnulado0203ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "aWSSincronizacionCAEParteUtilizadoAnulado_0203.aspx?wsdl";

                    WSSincronizacionCAEParteUtilizadoAnulado0203SoapBindingSOAP service = new WSSincronizacionCAEParteUtilizadoAnulado0203SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(PEmpId, PCAEId, PCAEParteid,PCAEPartefchutilizado,PCAEParteAnulNro,PRestaurarAnulacion);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSSincronizacionCertificados0200ExecuteResponseSOAP executeCertificadosWS(final short CajId, final int EmpId, final String EmpRUT, final int SucId, final XMLCERTIFICADOREQUESTSOAP request) {
        final WSSincronizacionCertificados0200ExecuteResponseSOAP[] response = {new WSSincronizacionCertificados0200ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "aWSSincronizacionCertificados_0200.aspx?wsdl";

                    WSSincronizacionCertificados0200SoapBindingSOAP service = new WSSincronizacionCertificados0200SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(EmpId, EmpRUT, SucId, (int)CajId, request);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSSincronizacionCertificadosDGI0202ExecuteResponseSOAP executeCertificadosDGIWS(final short CajId, final int EmpId, final String EmpRUT, final int SucId, final SDTCertificadoDGIRequestSOAP request) {
        final WSSincronizacionCertificadosDGI0202ExecuteResponseSOAP[] response = {new WSSincronizacionCertificadosDGI0202ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "awsSincronizacionCertificadosDGI_0202.aspx?wsdl";

                    WSSincronizacionCertificadosDGI0202SoapBindingSOAP service = new WSSincronizacionCertificadosDGI0202SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(EmpId, EmpRUT, SucId, (int)CajId, request);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSSincronizacionComprobantes0200ExecuteResponseSOAP executeComprobantesWS(final int CAEId, final int CAEParteId, final short CajaId, final int CertId, final String CteAdendaTexto, final String CteCajaUniqueid,
                                                                                      final int CteId, final String CteSucUniqueId, final String CteVersionApi, final String CteVersionGW, final String CteXMLEntrada,
                                                                                      final String CteXMLFirmado, final String CteXMLRespuesta, final int EmpId, final int SucId) {
        final WSSincronizacionComprobantes0200ExecuteResponseSOAP[] response = {new WSSincronizacionComprobantes0200ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "aWSSincronizacionComprobantes_0200.aspx?WSDL";

                    WSSincronizacionComprobantes0200SoapBindingSOAP service = new WSSincronizacionComprobantes0200SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(EmpId, SucId, (int)CajaId,CteId, CAEId, CAEParteId, CertId, CteXMLEntrada, CteXMLRespuesta, CteXMLFirmado, CteAdendaTexto, CteSucUniqueId,
                                CteCajaUniqueid, CteVersionApi, CteVersionGW);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSSincronizacionEmpresas_0202ExecuteResponseSOAP executeEmpresasWS(final String EmpRUC, final String EmpROWGUID, final Integer SucId, final short CajaId) {
        final WSSincronizacionEmpresas_0202ExecuteResponseSOAP[] response = {new WSSincronizacionEmpresas_0202ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "aWSSincronizacionEmpresas_0202.aspx?wsdl";

                    WSSincronizacionEmpresas0202SoapBindingSOAP service = new WSSincronizacionEmpresas0202SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(EmpRUC,EmpROWGUID,SucId, (int)CajaId);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSSincronizacionLogErrores0200ExecuteResponseSOAP executeLogErroresWS(final short PCajaid, final String PEmpruc, final short PLogerrcod,
                                                                                  final Date Plogerrfchhora, final Date Plogerrfecha, final long Plogerrid, final String Plogerrmsg, final String Plogerrsolucion,
                                                                                  final String Plogerrstacktrace, final String Plogerrtipoerror, final String Plogerrurgencia, final int Psucid) {
        final WSSincronizacionLogErrores0200ExecuteResponseSOAP[] response = {new WSSincronizacionLogErrores0200ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "aWSSincronizacionLogErrores_0200.aspx?wsdl";

                    WSSincronizacionLogErrores0200SoapBindingSOAP service = new WSSincronizacionLogErrores0200SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(PEmpruc, Psucid, (int)PCajaid,Plogerrid,Plogerrfecha,Plogerrfchhora,Plogerrstacktrace,Plogerrtipoerror,(int)PLogerrcod,Plogerrmsg,
                                Plogerrsolucion,Plogerrurgencia);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSSincronizacionScriptBDApi0201ExecuteResponseSOAP executeScriptBDApiWS(final String PEmpruc, final Integer PSucid, final short PCajaid) {
        final WSSincronizacionScriptBDApi0201ExecuteResponseSOAP[] response = {new WSSincronizacionScriptBDApi0201ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "aWSSincronizacionScriptBDApi_0201.aspx?wsdl";

                    WSSincronizacionScriptBDApi0201SoapBindingSOAP service = new WSSincronizacionScriptBDApi0201SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(PEmpruc, PSucid, (int)PCajaid);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSSincronizacionSucursales0205ExecuteResponseSOAP executeSucursalesWS(final int EmpId, final String EmpRUC, final int SucId, final String SucRowGUID) {
        final WSSincronizacionSucursales0205ExecuteResponseSOAP[] response = {new WSSincronizacionSucursales0205ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "aWSSincronizacionSucursales_0205.aspx?wsdl";

                    WSSincronizacionSucursales0205SoapBindingSOAP service = new WSSincronizacionSucursales0205SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(EmpId,EmpRUC,SucId, SucRowGUID);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }


    ////////////////////////////WEB SERVICES TAFEAPI//////////////////////////////////////////////
    private WSSincronizacionParamsApi0205ExecuteResponse205SOAP executeWSSincronizacionParamsApi205WS(final String PEmpruc, final Integer PSucid, final short PCajaid) {
        final WSSincronizacionParamsApi0205ExecuteResponse205SOAP[] response = {new WSSincronizacionParamsApi0205ExecuteResponse205SOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "aWSSincronizacionParamsApi_0205.aspx?wsdl";

                    WSSincronizacionParamsApi0205SoapBinding205SOAP service = new WSSincronizacionParamsApi0205SoapBinding205SOAP(URLserv);

                    try {
                        response[0] =  service.Execute(PEmpruc, PSucid, (int)PCajaid, "");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSSincronizacionParamsApi0206ExecuteResponse206SOAP executeWSSincronizacionParamsApi206WS(final String PEmpruc, final Integer PSucid, final short PCajaid, final String PParamguid, final String PCtecajauniqueid, final String PTextolicencia) {
        final WSSincronizacionParamsApi0206ExecuteResponse206SOAP[] response = {new WSSincronizacionParamsApi0206ExecuteResponse206SOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "aWSSincronizacionParamsApi_0206.aspx?wsdl";

                    WSSincronizacionParamsApi0206SoapBinding206SOAP service = new WSSincronizacionParamsApi0206SoapBinding206SOAP(URLserv);

                    try {
                        response[0] =  service.Execute(PEmpruc, PSucid, (int)PCajaid, PParamguid, PCtecajauniqueid, PTextolicencia);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSKeepAlive0200ExecuteResponseSOAP executeWSKeepAliveWS(final String EmpRUC, final short CajaId, final int SucId, final int CantCTEpendSync, final int CantLogPendSync, final Date FechahoraUltSync) {
        final WSKeepAlive0200ExecuteResponseSOAP[] response = {new WSKeepAlive0200ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "aWSKeepAlive_0200.aspx?wsdl";

                    WSKeepAlive0200SoapBindingSOAP service = new WSKeepAlive0200SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(EmpRUC, SucId, (int)CajaId,CantCTEpendSync,CantLogPendSync,FechahoraUltSync);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSSincronizacionCFC0200ExecuteResponseSOAP executeWSSincronizacionCFCWS(final String EmpRUC, final int SucId, final short CajaId, final SDTCFCRequestSOAP request) {
        final WSSincronizacionCFC0200ExecuteResponseSOAP[] response = {new WSSincronizacionCFC0200ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "aWSSincronizacionCFC_0200.aspx?wsdl";

                    WSSincronizacionCFC0200SoapBindingSOAP service = new WSSincronizacionCFC0200SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(EmpRUC, SucId, (int)CajaId,request);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private String executeWSVersionActualWS() {
        final String[] response = {new String()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "awswizardversionactual_0203.aspx?wsdl";

                    WSWizardVersionActual0203SoapBindingSOAP service = new WSWizardVersionActual0203SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSFirmarComprobanteConNroCAEReservado0203ExecuteResponseSOAP executeFirmarComprobanteConNroCAEReservadoWS(final long CAENroautorizacion, final int CAENroreservado, final String CAESerie,
                                                                                                                      final short CajaId, final String CteCajaUniqueId, final String CTEVersionapi, final String CTEXMLEntrada, final String EmpRUC, final int SucId) {
        final WSFirmarComprobanteConNroCAEReservado0203ExecuteResponseSOAP[] response = {new WSFirmarComprobanteConNroCAEReservado0203ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "aWSFirmarComprobanteConNroCAEReservado_0203.aspx?wsdl";

                    WSFirmarComprobanteConNroCAEReservado0203SoapBindingSOAP service = new WSFirmarComprobanteConNroCAEReservado0203SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(EmpRUC,SucId, (int)CajaId,CTEXMLEntrada,CteCajaUniqueId,CTEVersionapi,CAENroautorizacion,CAESerie,CAENroreservado);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSCancelarNroCAEReservado0203ExecuteResponseSOAP executeCancelarNroCAEReservadoWS(final long CAENroAutorizacion, final int CAEParteResNroCAE, final String CAESerie, final String EmpRUC) {
        final WSCancelarNroCAEReservado0203ExecuteResponseSOAP[] response = {new WSCancelarNroCAEReservado0203ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "aWSCancelarNroCAEReservado_0203.aspx?wsdl";

                    WSCancelarNroCAEReservado0203SoapBindingSOAP service = new WSCancelarNroCAEReservado0203SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(EmpRUC,CAENroAutorizacion,CAESerie,CAEParteResNroCAE);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSConfirmarFirmaComprobante0200ExecuteResponseSOAP executeConfirmarfirmacomprobanteWS(final short CajaId, final String XMLEntrada, final String EmpRUC, final int SucId) {
        final WSConfirmarFirmaComprobante0200ExecuteResponseSOAP[] response = {new WSConfirmarFirmaComprobante0200ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "awsConfirmarfirmacomprobante_0200.aspx?wsdl";

                    WSConfirmarFirmaComprobante0200SoapBindingSOAP service = new WSConfirmarFirmaComprobante0200SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(EmpRUC,SucId,(int)CajaId,XMLEntrada);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSEnviarContingencia0200ExecuteResponseSOAP executeEnviarContingenciaWS(final short CajaId, final String CTECajaUniqueId, final String VersionApi, final String XMLEntrada, final String EmpRUC, final int SucId) {
        final WSEnviarContingencia0200ExecuteResponseSOAP[] response = {new WSEnviarContingencia0200ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "aWSEnviarContingencia_0200.aspx?wsdl";

                    WSEnviarContingencia0200SoapBindingSOAP service = new WSEnviarContingencia0200SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(EmpRUC,SucId,(int)CajaId,XMLEntrada,CTECajaUniqueId,VersionApi);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSEnviarCteSinRespuesta0200ExecuteResponseSOAP executeEnviarCteSinRespuestaWS(final short CajaId, final String XMLEntrada, final String EmpRUC, final int SucId) {
        final WSEnviarCteSinRespuesta0200ExecuteResponseSOAP[] response = {new WSEnviarCteSinRespuesta0200ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "aWSEnviarCteSinRespuesta_0200.aspx?wsdl";

                    WSEnviarCteSinRespuesta0200SoapBindingSOAP service = new WSEnviarCteSinRespuesta0200SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(EmpRUC,SucId,(int)CajaId,XMLEntrada);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSFirmarComprobante0200ExecuteResponseSOAP executeFirmarComprobanteWS(final short CajaId, final String CTECajaUniqueId, final String VersionApi, final String XMLEntrada, final String EmpRUC, final int SucId) {
        final WSFirmarComprobante0200ExecuteResponseSOAP[] response = {new WSFirmarComprobante0200ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "awsfirmarcomprobante_0200.aspx?wsdl";

                    WSFirmarComprobante0200SoapBindingSOAP service = new WSFirmarComprobante0200SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(EmpRUC,SucId,(int)CajaId,XMLEntrada,CTECajaUniqueId,VersionApi);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSReservaNroCAEEstaVigente0203ExecuteResponseSOAP executeReservaNroCAEEstaVigenteWS(final long CAENroAutorizacion, final int CAEParteResNroCAE, final String CAESerie, final String EmpRUC) {
        final WSReservaNroCAEEstaVigente0203ExecuteResponseSOAP[] response = {new WSReservaNroCAEEstaVigente0203ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "awsReservaNroCAEEstaVigente_0203.aspx?wsdl";

                    WSReservaNroCAEEstaVigente0203SoapBindingSOAP service = new WSReservaNroCAEEstaVigente0203SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(EmpRUC,CAENroAutorizacion,CAESerie,CAEParteResNroCAE);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private WSReservarNroCAE0203ExecuteResponseSOAP executeReservarNroCAEWS(final String EmpRUC, final int SucId, final short TipoCFE) {
        final WSReservarNroCAE0203ExecuteResponseSOAP[] response = {new WSReservarNroCAE0203ExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "awsReservarNroCAE_0203.aspx?wsdl";

                    WSReservarNroCAE0203SoapBindingSOAP service = new WSReservarNroCAE0203SoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(EmpRUC,SucId,(int)TipoCFE);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }

    private  WSConsultarReceptorElectronicoExecuteResponseSOAP executeConsultaReceptorElectronicoWS(final String RUC) {
        final WSConsultarReceptorElectronicoExecuteResponseSOAP[] response = {new WSConsultarReceptorElectronicoExecuteResponseSOAP()};
        final Thread background = new Thread(new Runnable() {
            public void run() {
                try {
                    String URLserv = URLServer + "aWSConsultarReceptorElectronico.aspx?wsdl";

                    WSConsultarReceptorElectronicoSoapBindingSOAP service = new WSConsultarReceptorElectronicoSoapBindingSOAP(URLserv);

                    try {
                        response[0] =  service.Execute(RUC);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        background.start();
        try {
            background.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response[0];
    }
    
    //////////////INVOKE EXECUTE METHODS TAFACEAPIFIRMAR///////////////////////////////////////
    //Invoke executeScriptBDI() to create tables in BD
    public String executeScriptBDI(short CajaID, String EmpRUT, int SucID){
        String scriptResult = null;
        WSSincronizacionScriptBDApi0201ExecuteResponseSOAP resp1 = executeScriptBDApiWS(EmpRUT,SucID,CajaID);
        scriptResult = resp1.Pscriptbd;
        return scriptResult;
    }

//    //Invoke executeWakeUp() to wakeUp if it's slept the server
    public boolean executeWakeUpServer() {
        Boolean resp1 = executeWakeUpWS();
        return resp1;
    }

    //Invoke executeFechaSistema() to get current Date of server in BD
    public Date executeDescargarFechaSistema(){
        Date FechaHoraActual = new Date(0);
        try {
            WSGetFechaSistema0205ExecuteResponseSOAP resp1 = executeFechaSistemaWS();
            FechaHoraActual = resp1.Pfechasistema;

        } catch (Throwable t) {
           t.printStackTrace();
        }
        return FechaHoraActual;
    }

    //Invoke executeCAEParte() to sinchronize DB with GTW DB
    public WSSincronizacionCAEParte0206ExecuteResponse executeCAEParte(int EmpId, String EmpRUT, int SucId, short CajId, List<Map<String, Object>> setCAEParteenBD) {

        XMLCAEPARTEREQUEST request = new XMLCAEPARTEREQUEST();
        for (int i = 0; i < setCAEParteenBD.size(); i++) {
            SDTCAEParteRequest206SDTCAEParteRequest206Item item1 = new SDTCAEParteRequest206SDTCAEParteRequest206Item();
            item1.CAEId = Integer.parseInt(setCAEParteenBD.get(i).get("CAEId").toString());
            item1.CAEParteId =Integer.parseInt(setCAEParteenBD.get(i).get("CAEParteId").toString());
            item1.CAEParteRowGUID = setCAEParteenBD.get(i).get("CAEParteRowGUID").toString();
            item1.EmpId  =Integer.parseInt(setCAEParteenBD.get(i).get("EmpId").toString());
            request.add(item1);
        }
        WSSincronizacionCAEParte_0206ExecuteResponse resp1 = executeCAEParteWS(EmpId, EmpRUT, SucId, CajId, request);
        WSSincronizacionCAEParte0206ExecuteResponse resp2 = new WSSincronizacionCAEParte0206ExecuteResponse();
        resp2.setPerrormessage(resp1.Perrormessage == null ? "" : resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        XMLCAEPARTERESPONSE xmlCAEParte = new XMLCAEPARTERESPONSE();
        List<SDTCAEParteResponse206SDTCAEParteResponse206Item> list = new ArrayList<SDTCAEParteResponse206SDTCAEParteResponse206Item>();
        if(resp1.Psdtcaeparteresponse != null) {
            for (int i = 0; i < resp1.Psdtcaeparteresponse.size(); i++) {

                SDTCAEParteResponse206SDTCAEParteResponse206Item caeParteItem = new SDTCAEParteResponse206SDTCAEParteResponse206Item();
                caeParteItem.setEmpId(resp1.Psdtcaeparteresponse.get(i).EmpId);
                caeParteItem.setSucId(resp1.Psdtcaeparteresponse.get(i).SucId);
                caeParteItem.setCajaId(resp1.Psdtcaeparteresponse.get(i).CajaId.shortValue());
                caeParteItem.setCAEId(resp1.Psdtcaeparteresponse.get(i).CAEId);
                caeParteItem.setCAEParteId(resp1.Psdtcaeparteresponse.get(i).CAEParteId);
                caeParteItem.setCAEParteNroDesde(resp1.Psdtcaeparteresponse.get(i).CAEParteNroDesde);
                caeParteItem.setCAEParteNroHasta(resp1.Psdtcaeparteresponse.get(i).CAEParteNroHasta);
                caeParteItem.setCAEParteUltAsignado(resp1.Psdtcaeparteresponse.get(i).CAEParteUltAsignado);
                caeParteItem.setCAEParteRowGUID(resp1.Psdtcaeparteresponse.get(i).CAEParteRowGUID);
                caeParteItem.setCAEParteEliminado(resp1.Psdtcaeparteresponse.get(i).CAEParteEliminado);

                SDTCAEParteResponse206SDTCAEParteResponse206ItemCAE caeItem = new SDTCAEParteResponse206SDTCAEParteResponse206ItemCAE();
                caeItem.setEmpId(resp1.Psdtcaeparteresponse.get(i).CAE.EmpId);
                caeItem.setCAEId(resp1.Psdtcaeparteresponse.get(i).CAE.CAEId);
                caeItem.setCAENroDesde(resp1.Psdtcaeparteresponse.get(i).CAE.CAENroDesde);
                caeItem.setCAENroHasta(resp1.Psdtcaeparteresponse.get(i).CAE.CAENroHasta);
                caeItem.setCAETipoCFE(resp1.Psdtcaeparteresponse.get(i).CAE.CAETipoCFE.shortValue());
                caeItem.setCAEAnulado(resp1.Psdtcaeparteresponse.get(i).CAE.CAEAnulado);
                GregorianCalendar calendar = new GregorianCalendar();
                calendar.setTime(resp1.Psdtcaeparteresponse.get(i).CAE.CAEFchHora);
                caeItem.setCAEFchHora(calendar);
                caeItem.setCAENroAutorizacion(resp1.Psdtcaeparteresponse.get(i).CAE.CAENroAutorizacion);
                caeItem.setCAESerie(resp1.Psdtcaeparteresponse.get(i).CAE.CAESerie);
                calendar = new GregorianCalendar();
                calendar.setTime(resp1.Psdtcaeparteresponse.get(i).CAE.CAEVencimiento);
                caeItem.setCAEVencimiento(calendar);
                calendar = new GregorianCalendar();
                calendar.setTime(resp1.Psdtcaeparteresponse.get(i).CAE.CAEVigencia);
                caeItem.setCAEVigencia(calendar);

                caeParteItem.setCAE(caeItem);
                list.add(caeParteItem);
            }
        }
        xmlCAEParte.setSDTCAEParteResponse206SDTCAEParteResponse206Item(list);
        resp2.setPsdtcaeparteresponse(xmlCAEParte);
        return resp2;
    }

    //Invoke executeCertificadosDGI() to sinchronize DB with GTW DB
    public WSSincronizacionCertificadosDGI0202ExecuteResponse executeCertificadoDGI(short CajId, int EmpId, String EmpRUC, int SucId, SDTCertificadoDGIRequest request) {
        SDTCertificadoDGIRequestSOAP requestSOAP = new SDTCertificadoDGIRequestSOAP();

        for (int i = 0; i< request.getSDTCertificadoDGIRequestSDTCertificadoDGIRequestItem().size(); i++){
            SDTCertificadoDGIRequestSDTCertificadoDGIRequestItemSOAP itemSOAP = new SDTCertificadoDGIRequestSDTCertificadoDGIRequestItemSOAP();
            itemSOAP.CertDGIId = request.getSDTCertificadoDGIRequestSDTCertificadoDGIRequestItem().get(i).getCertDGIId();
            itemSOAP.CertDGIRowGUID = request.getSDTCertificadoDGIRequestSDTCertificadoDGIRequestItem().get(i).getCertDGIRowGUID();
            itemSOAP.EmpId = request.getSDTCertificadoDGIRequestSDTCertificadoDGIRequestItem().get(i).getEmpId();
            requestSOAP.add(itemSOAP);
        }

        WSSincronizacionCertificadosDGI0202ExecuteResponseSOAP resp1 = executeCertificadosDGIWS(CajId, EmpId, EmpRUC, SucId, requestSOAP);
        Negocio.WSInterface.CertificadoDGI.WSSincronizacionCertificadosDGI0202ExecuteResponse resp2= new Negocio.WSInterface.CertificadoDGI.WSSincronizacionCertificadosDGI0202ExecuteResponse();
        SDTCertificadoDGIResponse certificadoDGI = new SDTCertificadoDGIResponse();
        List<SDTCertificadoDGIResponseSDTCertificadoDGIResponseItem> list  = new ArrayList<>();
        if(resp1.psdtcertificadodgiresponse != null) {
            for (int i = 0; i < resp1.psdtcertificadodgiresponse.size(); i++) {
                SDTCertificadoDGIResponseSDTCertificadoDGIResponseItem item = new SDTCertificadoDGIResponseSDTCertificadoDGIResponseItem();
                item.setCertDGIArchivo(resp1.psdtcertificadodgiresponse.get(i).CertDGIArchivo);
                item.setCertDGICN(resp1.psdtcertificadodgiresponse.get(i).CertDGICN);
                item.setCertDGIEliminado(resp1.psdtcertificadodgiresponse.get(i).CertDGIEliminado);
                GregorianCalendar calendar = new GregorianCalendar();
                calendar.setTime(resp1.psdtcertificadodgiresponse.get(i).CertDGIFchHora);
                item.setCertDGIFchHora(calendar);
                item.setCertDGIId(resp1.psdtcertificadodgiresponse.get(i).CertDGIId);
                item.setCertDGIRowGUID(resp1.psdtcertificadodgiresponse.get(i).CertDGIRowGUID);
                calendar = new GregorianCalendar();
                calendar.setTime(resp1.psdtcertificadodgiresponse.get(i).CertDGIVencimiento);
                item.setCertDGIVencimiento(calendar);
                calendar.setTime(resp1.psdtcertificadodgiresponse.get(i).CertDGIVigencia);
                item.setCertDGIVigencia(calendar);
                item.setEmpId(resp1.psdtcertificadodgiresponse.get(i).EmpId);
                list.add(item);
            }
        }
        certificadoDGI.setSDTCertificadoDGIResponseSDTCertificadoDGIResponseItem(list);
        resp2.setPsdtcertificadodgiresponse(certificadoDGI);
        resp2.setPerrormessage(resp1.Perrormessage == null ? "" : resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        return resp2;
    }

    //Invoke executeCertificados() to sinchronize DB with GTW DB
    public WSSincronizacionCertificados0200ExecuteResponse executeCertificado(short CajId, int EmpId, String EmpRUC, int SucId, XMLCERTIFICADOREQUEST request) {

        XMLCERTIFICADOREQUESTSOAP requestSOAP = new XMLCERTIFICADOREQUESTSOAP();
        for (int i = 0; i< request.getSDTCertificadoRequestSDTCertificadoRequestItem().size(); i++){
            SDTCertificadoRequestSDTCertificadoRequestItemSOAP itemSOAP = new SDTCertificadoRequestSDTCertificadoRequestItemSOAP();
            itemSOAP.CertId = request.getSDTCertificadoRequestSDTCertificadoRequestItem().get(i).getCertId();
            itemSOAP.CertRowGUID = request.getSDTCertificadoRequestSDTCertificadoRequestItem().get(i).getCertRowGUID();
            itemSOAP.EmpId = request.getSDTCertificadoRequestSDTCertificadoRequestItem().get(i).getEmpId();
            requestSOAP.add(itemSOAP);
        }

        WSSincronizacionCertificados0200ExecuteResponseSOAP resp1 = executeCertificadosWS(CajId,EmpId, EmpRUC, SucId,requestSOAP);
        WSSincronizacionCertificados0200ExecuteResponse resp2 = new WSSincronizacionCertificados0200ExecuteResponse();
        XMLCERTIFICADORESPONSE certificado = new XMLCERTIFICADORESPONSE();
        List<SDTCertificadoResponseSDTCertificadoResponseItem> list  = new  ArrayList<SDTCertificadoResponseSDTCertificadoResponseItem>();
        if(resp1.Psdtcertificadoresponse != null) {
            for (int i = 0; i < resp1.Psdtcertificadoresponse.size(); i++) {
                SDTCertificadoResponseSDTCertificadoResponseItem item = new SDTCertificadoResponseSDTCertificadoResponseItem();
                item.setCertArchivo(resp1.Psdtcertificadoresponse.get(i).CertArchivo);
                item.setCertClaveEncriptacion(resp1.Psdtcertificadoresponse.get(i).CertClaveEncriptacion);
                item.setCertEliminado(resp1.Psdtcertificadoresponse.get(i).CertEliminado);
                GregorianCalendar calendar = new GregorianCalendar();
                calendar.setTime(resp1.Psdtcertificadoresponse.get(i).CertFchHora);
                item.setCertFchHora(calendar);
                item.setCertId(resp1.Psdtcertificadoresponse.get(i).CertId);
                item.setCertRowGUID(resp1.Psdtcertificadoresponse.get(i).CertRowGUID);
                calendar = new GregorianCalendar();
                calendar.setTime(resp1.Psdtcertificadoresponse.get(i).CertVencimiento);
                item.setCertVencimiento(calendar);
                calendar = new GregorianCalendar();
                calendar.setTime(resp1.Psdtcertificadoresponse.get(i).CertVigencia);
                item.setCertVigencia(calendar);
                item.setEmpId(resp1.Psdtcertificadoresponse.get(i).EmpId);
                list.add(item);
            }
        }
        certificado.setSDTCertificadoResponseSDTCertificadoResponseItem(list);
        resp2.setPsdtcertificadoresponse(certificado);
        resp2.setPerrormessage(resp1.Perrormessage == null ? "" : resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);


        return resp2;
    }

    //Invoke executeEmpresas() to sinchronize DB with GTW DB
    public WSSincronizacionEmpresas0202ExecuteResponse executeEmpresa(short CajId, String EmpRowGUID, String EmpRUC, int SucId) {

        WSSincronizacionEmpresas_0202ExecuteResponseSOAP resp1 = executeEmpresasWS(EmpRUC, EmpRowGUID, SucId, CajId);
        WSSincronizacionEmpresas0202ExecuteResponse resp2 = new WSSincronizacionEmpresas0202ExecuteResponse();
        XMLEMPRESARESPONSE empresa=  new XMLEMPRESARESPONSE();
        List<SDTEmpresaResponse202SDTEmpresaResponse202Item> lista = new ArrayList<SDTEmpresaResponse202SDTEmpresaResponse202Item>();
        if(resp1.Psdtempresaresponse != null) {
            for (int i = 0; i < resp1.Psdtempresaresponse.size(); i++) {
                SDTEmpresaResponse202SDTEmpresaResponse202Item item = new SDTEmpresaResponse202SDTEmpresaResponse202Item();
                item.setEmpEmiteCuentaAjena(resp1.Psdtempresaresponse.get(i).EmpEmiteCuentaAjena);
                item.setEmpEmiteFacturas(resp1.Psdtempresaresponse.get(i).EmpEmiteFacturas);
                item.setEmpEmiteFacturasExportacion(resp1.Psdtempresaresponse.get(i).EmpEmiteFacturasExportacion);
                item.setEmpEmiteRemitos(resp1.Psdtempresaresponse.get(i).EmpEmiteRemitos);
                item.setEmpEmiteRemitosExportacion(resp1.Psdtempresaresponse.get(i).EmpEmiteRemitosExportacion);
                item.setEmpEmiteResguardos(resp1.Psdtempresaresponse.get(i).EmpEmiteResguardos);
                item.setEmpEmiteTickets(resp1.Psdtempresaresponse.get(i).EmpEmiteTickets);
                item.setEmpId(resp1.Psdtempresaresponse.get(i).EmpId);
                item.setEmpLogoImpresion(resp1.Psdtempresaresponse.get(i).EmpLogoImpresion);
                item.setEmpLogoMiniatura(resp1.Psdtempresaresponse.get(i).EmpLogoMiniatura);
                item.setEmpNom(resp1.Psdtempresaresponse.get(i).EmpNom);
                item.setEmpResolucionIVA(resp1.Psdtempresaresponse.get(i).EmpResolucionIVA == null ? "" : resp1.Psdtempresaresponse.get(i).EmpResolucionIVA);
                item.setEmpRowGUID(resp1.Psdtempresaresponse.get(i).EmpRowGUID);
                item.setEmpRUC(resp1.Psdtempresaresponse.get(i).EmpRUC);
                item.setEmpUrlConsultaCFE(resp1.Psdtempresaresponse.get(i).EmpUrlConsultaCFE == null ? "" : resp1.Psdtempresaresponse.get(i).EmpUrlConsultaCFE);
                lista.add(item);
            }
        }
        empresa.setSDTEmpresaResponse202SDTEmpresaResponse202Item(lista);
        resp2.setPsdtempresaresponse(empresa);
        resp2.setPerrormessage(resp1.Perrormessage == null ? "" : resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        return resp2;
    }
    //
    //Invoke executeSucursales() to sinchronize DB with GTW DB
    public WSSincronizacionSucursales0205ExecuteResponse executeSucursal(int EmpId, String EmpRUC, int SucId, String SucRowGUID) {

        WSSincronizacionSucursales0205ExecuteResponseSOAP resp1 = executeSucursalesWS(EmpId, EmpRUC, SucId, SucRowGUID);
        WSSincronizacionSucursales0205ExecuteResponse resp2 = new WSSincronizacionSucursales0205ExecuteResponse();
        XMLSUCURSALRESPONSE sucursal = new XMLSUCURSALRESPONSE();
        List<SDTSucursalResponse205SDTSucursalResponse205Item> list = new ArrayList<SDTSucursalResponse205SDTSucursalResponse205Item>();
        if(resp1.Psdtsucursalresponse != null) {
        for (int i = 0; i < resp1.Psdtsucursalresponse.size(); i++) {
            SDTSucursalResponse205SDTSucursalResponse205Item.Cajas sdtCajas = new SDTSucursalResponse205SDTSucursalResponse205Item.Cajas();
            List<SDTSucursalResponse205SDTSucursalResponse205ItemCajasItem> listCajas = new ArrayList<SDTSucursalResponse205SDTSucursalResponse205ItemCajasItem>();
            for (int j = 0; j < resp1.Psdtsucursalresponse.get(i).Cajas.size(); j++) {
                SDTSucursalResponse205SDTSucursalResponse205ItemCajasItem caja = new SDTSucursalResponse205SDTSucursalResponse205ItemCajasItem();
                caja.setCajaId(resp1.Psdtsucursalresponse.get(i).Cajas.get(j).CajaId.shortValue());
                caja.setCajaUniqueId(resp1.Psdtsucursalresponse.get(i).Cajas.get(j).CajaUniqueId == null ? "" : resp1.Psdtsucursalresponse.get(i).Cajas.get(j).CajaUniqueId);
                listCajas.add(caja);
            }
            sdtCajas.setCajasItem(listCajas);
            SDTSucursalResponse205SDTSucursalResponse205Item item = new SDTSucursalResponse205SDTSucursalResponse205Item();
            item.setCajas(sdtCajas);
            item.setEmpId(resp1.Psdtsucursalresponse.get(i).EmpId);
            item.setSucCodEnDGI(resp1.Psdtsucursalresponse.get(i).SucCodEnDGI.shortValue());
            item.setSucHorasOfflinePermite(resp1.Psdtsucursalresponse.get(i).SucHorasOfflinePermite.shortValue());
            item.setSucId(resp1.Psdtsucursalresponse.get(i).SucId);
            item.setSucModoFirma(resp1.Psdtsucursalresponse.get(i).SucModoFirma.shortValue());
            item.setSucModoSincronizacion(resp1.Psdtsucursalresponse.get(i).SucModoSincronizacion.byteValue());
            item.setSucTipoProvSQLApi(resp1.Psdtsucursalresponse.get(i).SucTipoProvSQLApi.shortValue());
            item.setSucCadenaCnxSQLApi(resp1.Psdtsucursalresponse.get(i).SucCadenaCnxSQLApi);
            item.setSucEliminada(resp1.Psdtsucursalresponse.get(i).SucEliminada);
            item.setSucLogoImpresion(resp1.Psdtsucursalresponse.get(i).SucLogoImpresion);
            item.setSucLogoMiniatura(resp1.Psdtsucursalresponse.get(i).SucLogoMiniatura);
            item.setSucNom(resp1.Psdtsucursalresponse.get(i).SucNom);
            item.setSucRowGUID(resp1.Psdtsucursalresponse.get(i).SucRowGUID);
            list.add(item);
        }
    }
        sucursal.setSDTSucursalResponse205SDTSucursalResponse205Item(list);
        resp2.setPsdtsucursalresponse(sucursal);
        resp2.setPerrormessage(resp1.Perrormessage == null ? "" : resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        return resp2;
    }

    //Invoke executeComprobantes() to sinchronize DB with GTW DB
    public WSSincronizacionComprobantes0200ExecuteResponse executeComprobante(int CAEId, int CAEParteId, short CajaId, int CertId, String CteAdendaTexto, String CteCajaUniqueid,
                                                                              int CteId, String CteSucUniqueId, String CteVersionApi, String CteVersionGW, String CteXMLEntrada,
                                                                              String CteXMLFirmado, String CteXMLRespuesta, int EmpId, int SucId)  {
        WSSincronizacionComprobantes0200ExecuteResponseSOAP resp1 = executeComprobantesWS(CAEId, CAEParteId,  CajaId, CertId, CteAdendaTexto, CteCajaUniqueid,  CteId,  CteSucUniqueId,
                CteVersionApi,  CteVersionGW, CteXMLEntrada, CteXMLFirmado, CteXMLRespuesta, EmpId, SucId);
        WSSincronizacionComprobantes0200ExecuteResponse resp2 = new WSSincronizacionComprobantes0200ExecuteResponse();
        resp2.setPcteidnew(resp1.Pcteidnew);
        resp2.setPerrormessage(resp1.Perrormessage == null ? "" : resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        return resp2;
    }

    //Invoke executeCAEParteUtilizado() to sinchronize DB with GTW DB
    public final WSSincronizacionCAEParteUtilizado0200ExecuteResponse executeCAEParteUtilizado(int PCaeId, Date PCAEParteFchUtilizado, int PCAEParteId, int PCAEParteUtilNroDesde,
                                                                                               int PCAEParteUtilNroHasta, int PEmpid) {
        WSSincronizacionCAEParteUtilizado0200ExecuteResponseSOAP resp1 = executeCAEParteUtilizadoWS(PCaeId, PCAEParteFchUtilizado, PCAEParteId, PCAEParteUtilNroDesde,
        PCAEParteUtilNroHasta, PEmpid);
        WSSincronizacionCAEParteUtilizado0200ExecuteResponse resp2 = new WSSincronizacionCAEParteUtilizado0200ExecuteResponse();
        resp2.setPerrormessage(resp1.Perrormessage == null ? "" : resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        return resp2;
    }

    //Invoke executeCAEParteAnulado() to sinchronize DB with GTW DB
    public WSSincronizacionCAEParteAnulada0200ExecuteResponse executeCAEParteAnulado(int PCAEId, int PCAEParteId, int PEmpId) {

        WSSincronizacionCAEParteAnulada0200ExecuteResponseSOAP resp1 = executeCAEParteAnuladoWS(PCAEId, PCAEParteId, PEmpId);
        WSSincronizacionCAEParteAnulada0200ExecuteResponse resp2 = new WSSincronizacionCAEParteAnulada0200ExecuteResponse();
        resp2.setPerrormessage(resp1.Perrormessage == null ? "" : resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        return resp2;
    }

    //Invoke executeCAEParteUtilizadoAnulado() to sinchronize DB with GTW DB
    public WSSincronizacionCAEParteUtilizadoAnulado0203ExecuteResponse executeCAEParteUtilizadoAnulado(int PCAEId, int PCAEParteAnulNro, Date PCAEPartefchutilizado,
                                                                                                       int PCAEParteid, int PEmpId, boolean PRestaurarAnulacion) {
        WSSincronizacionCAEParteUtilizadoAnulado0203ExecuteResponseSOAP resp1 = executeCAEParteUtilizadoAnuladoWS(PCAEId, PCAEParteAnulNro, PCAEPartefchutilizado, PCAEParteid, PEmpId, PRestaurarAnulacion);
        WSSincronizacionCAEParteUtilizadoAnulado0203ExecuteResponse resp2 = new WSSincronizacionCAEParteUtilizadoAnulado0203ExecuteResponse();
        resp2.setPerrormessage(resp1.Perrormessage == null ? "" : resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        return resp2;
    }

    //Invoke executeLogErrores() to sinchronize DB with GTW DB
    public WSSincronizacionLogErrores0200ExecuteResponse executeLogErrores(short PCajaid, String PEmpruc, short PLogerrcod,
                                                                           Date Plogerrfchhora, Date Plogerrfecha, long Plogerrid, String Plogerrmsg, String Plogerrsolucion,
                                                                           String Plogerrstacktrace, String Plogerrtipoerror, String Plogerrurgencia, int Psucid)  {

        WSSincronizacionLogErrores0200ExecuteResponseSOAP resp1 = executeLogErroresWS(PCajaid, PEmpruc, PLogerrcod, Plogerrfchhora, Plogerrfecha, Plogerrid, Plogerrmsg,
                     Plogerrsolucion, Plogerrstacktrace,  Plogerrtipoerror, Plogerrurgencia, Psucid);
        WSSincronizacionLogErrores0200ExecuteResponse resp2 = new WSSincronizacionLogErrores0200ExecuteResponse();
        resp2.setPerrormessage(resp1.Perrormessage == null ? "" : resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        resp2.setPlogerridnew(resp1.Plogerridnew);
        return resp2;
    }

    /////////////////////////////////INVOKE EXECUTE METHODS TAFEAPI///////////////////////////////////////
    public WSSincronizacionParamsApi0205ExecuteResponse executeParamsApi(String EmpRUC, short CajaId, int SucId)  {
        WSSincronizacionParamsApi0205ExecuteResponse205SOAP resp1 = executeWSSincronizacionParamsApi205WS(EmpRUC,SucId,CajaId);
        WSSincronizacionParamsApi0205ExecuteResponse resp2 = new WSSincronizacionParamsApi0205ExecuteResponse();
        resp2.setPerrormessage(resp1.Perrormessage == null ? "" : resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        PARAMETROSTAFEAPI205 parametros205 = new PARAMETROSTAFEAPI205();
        parametros205.setAperturaDeCajon(resp1.Psdtparamtafeapi.AperturaDeCajon);
        parametros205.setEncriptarComplementoFiscal(resp1.Psdtparamtafeapi.EncriptarComplementoFiscal);
        parametros205.setNoValidarFechaFirma(resp1.Psdtparamtafeapi.NoValidarFechaFirma);
        parametros205.setValidarCFC(resp1.Psdtparamtafeapi.ValidarCFC);
        parametros205.setCadenaCnxSQL(resp1.Psdtparamtafeapi.CadenaCnxSQL);
        parametros205.setCAEsPorcAvance(resp1.Psdtparamtafeapi.CAEsPorcAvance.byteValue());
        parametros205.setCantDiasAntesCAEVencer(resp1.Psdtparamtafeapi.CantDiasAntesCAEVencer.shortValue());
        parametros205.setLargoMinimoTicket(resp1.Psdtparamtafeapi.LargoMinimoTicket.shortValue());
        parametros205.setCantDiasAntesCertificadoVencer(resp1.Psdtparamtafeapi.CantDiasAntesCertificadoVencer.shortValue());
        parametros205.setLoteSincronizacion(resp1.Psdtparamtafeapi.LoteSincronizacion.shortValue());
        parametros205.setMinDatosCajaSinSinc(resp1.Psdtparamtafeapi.MinDatosCajaSinSinc.shortValue());
        parametros205.setMinutosCaducidadReservaCAE(resp1.Psdtparamtafeapi.MinutosCaducidadReservaCAE);
        parametros205.setModoFirma(resp1.Psdtparamtafeapi.ModoFirma.shortValue());
        parametros205.setParamGUID(resp1.Psdtparamtafeapi.ParamGUID);
        parametros205.setSucModoSincronizacion(resp1.Psdtparamtafeapi.SucModoSincronizacion.byteValue());
        parametros205.setTipoEncriptacion(resp1.Psdtparamtafeapi.TipoEncriptacion.shortValue());
        parametros205.setTipoProveedorSQL(resp1.Psdtparamtafeapi.TipoProveedorSQL.shortValue());
        parametros205.setTopeUIFreeShop(resp1.Psdtparamtafeapi.TopeUIFreeShop);
        parametros205.setTopeUIPlaza(resp1.Psdtparamtafeapi.TopeUIPlaza);
        resp2.setPsdtparamtafeapi(parametros205);
        return resp2;
    }

	public WSSincronizacionParamsApi0206ExecuteResponse executeParamsApi206(String EmpRUC, short CajaId, int SucId, String CajaUniqueId, String LicenciaServer) {
        WSSincronizacionParamsApi0206ExecuteResponse206SOAP resp1 = executeWSSincronizacionParamsApi206WS(EmpRUC,SucId,CajaId,"", CajaUniqueId,LicenciaServer);
        WSSincronizacionParamsApi0206ExecuteResponse resp2 = new WSSincronizacionParamsApi0206ExecuteResponse();
        resp2.setPerrormessage(resp1.Perrormessage == null ? "" : resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        PARAMETROSTAFEAPI206 parametros206 = new PARAMETROSTAFEAPI206();
        parametros206.setAperturaDeCajon(resp1.Psdtparamtafeapi.AperturaDeCajon);
        parametros206.setEncriptarComplementoFiscal(resp1.Psdtparamtafeapi.EncriptarComplementoFiscal);
        parametros206.setNoValidarFechaFirma(resp1.Psdtparamtafeapi.NoValidarFechaFirma);
        parametros206.setValidarCFC(resp1.Psdtparamtafeapi.ValidarCFC);
        parametros206.setCadenaCnxSQL(resp1.Psdtparamtafeapi.CadenaCnxSQL);
        parametros206.setCAEsPorcAvance(resp1.Psdtparamtafeapi.CAEsPorcAvance.byteValue());
        parametros206.setCantDiasAntesCAEVencer(resp1.Psdtparamtafeapi.CantDiasAntesCAEVencer.shortValue());
        parametros206.setLargoMinimoTicket(resp1.Psdtparamtafeapi.LargoMinimoTicket.shortValue());
        parametros206.setCantDiasAntesCertificadoVencer(resp1.Psdtparamtafeapi.CantDiasAntesCertificadoVencer.shortValue());
        parametros206.setLoteSincronizacion(resp1.Psdtparamtafeapi.LoteSincronizacion.shortValue());
        parametros206.setMinDatosCajaSinSinc(resp1.Psdtparamtafeapi.MinDatosCajaSinSinc.shortValue());
        parametros206.setMinutosCaducidadReservaCAE(resp1.Psdtparamtafeapi.MinutosCaducidadReservaCAE);
        parametros206.setModoFirma(resp1.Psdtparamtafeapi.ModoFirma.shortValue());
        parametros206.setParamGUID(resp1.Psdtparamtafeapi.ParamGUID);
        parametros206.setSucModoSincronizacion(resp1.Psdtparamtafeapi.SucModoSincronizacion.byteValue());
        parametros206.setTipoEncriptacion(resp1.Psdtparamtafeapi.TipoEncriptacion.shortValue());
        parametros206.setTipoProveedorSQL(resp1.Psdtparamtafeapi.TipoProveedorSQL.shortValue());
        parametros206.setTopeUIFreeShop(resp1.Psdtparamtafeapi.TopeUIFreeShop);
        parametros206.setTopeUIPlaza(resp1.Psdtparamtafeapi.TopeUIPlaza);
        resp2.setPsdtparamtafeapi(parametros206);
        return resp2;
	}

    public boolean executeKeepAlive(String EmpRUC, short CajaId, int SucId, int CantCTEpendSync, int CantLogPendSync, Date FechahoraUltSync) {
        WSKeepAlive0200ExecuteResponseSOAP resp1 = executeWSKeepAliveWS(EmpRUC,CajaId,SucId,CantCTEpendSync,CantLogPendSync,FechahoraUltSync);
        return resp1.Perrorreturn;
    }

    public WSSincronizacionCFC0200ExecuteResponse executeWSSincronizacionCFC(String EmpRUC, int SucId, short CajaId, List<SDTCFCRequestSDTCFCRequestItem> request) {

        SDTCFCRequestSOAP SDTrequest = new SDTCFCRequestSOAP();
        for (int i = 0; i < request.size(); i++) {
            SDTCFCRequestSDTCFCRequestItemSOAP item = new SDTCFCRequestSDTCFCRequestItemSOAP();
            item.CFCId = request.get(i).getCFCId();
            item.CFCRowGUID = request.get(i).getCFCRowGUID();
            item.EmpId = request.get(i).getEmpId();

            SDTrequest.add(item);
        }
        WSSincronizacionCFC0200ExecuteResponseSOAP resp1 = executeWSSincronizacionCFCWS(EmpRUC, SucId, CajaId, SDTrequest);
        WSSincronizacionCFC0200ExecuteResponse resp2 = new WSSincronizacionCFC0200ExecuteResponse();

        SDTCFCResponse response = new SDTCFCResponse();
        List<SDTCFCResponseSDTCFCResponseItem> list = new ArrayList<SDTCFCResponseSDTCFCResponseItem>();
        if(resp1.Psdtcfcresponse != null) {
        for (int i = 0; i < resp1.Psdtcfcresponse.size(); i++) {
            SDTCFCResponseSDTCFCResponseItem item = new SDTCFCResponseSDTCFCResponseItem();
            item.setCFCEliminado(resp1.Psdtcfcresponse.get(i).CFCEliminado);
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(resp1.Psdtcfcresponse.get(i).CFCFchHora);
            item.setCFCFchHora(calendar);
            item.setCFCId(resp1.Psdtcfcresponse.get(i).CFCId);
            item.setCFCNroAutorizacion(resp1.Psdtcfcresponse.get(i).CFCNroAutorizacion);
            item.setCFCNroDesde(resp1.Psdtcfcresponse.get(i).CFCNroDesde);
            item.setCFCNroHasta(resp1.Psdtcfcresponse.get(i).CFCNroHasta);
            item.setCFCRowGUID(resp1.Psdtcfcresponse.get(i).CFCRowGUID);
            item.setCFCSerie(resp1.Psdtcfcresponse.get(i).CFCSerie);
            item.setCFCTipoCFE(resp1.Psdtcfcresponse.get(i).CFCTipoCFE.shortValue());
            item.setEmpId(resp1.Psdtcfcresponse.get(i).EmpId);
            calendar.setTime(resp1.Psdtcfcresponse.get(i).CFCVencimiento);
            item.setCFCVencimiento(calendar);
            item.setSucId(resp1.Psdtcfcresponse.get(i).SucId);
            list.add(item);
        }
    }
        response.setSDTCFCResponseSDTCFCResponseItem(list);
        resp2.setPerrormessage(resp1.Perrormessage == null ? "" : resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        resp2.setPsdtcfcresponse(response);
        return resp2;
    }

    public String executeVersionActual() {
        String resp = executeWSVersionActualWS();
        return resp;
    }

    public WSFirmarComprobanteConNroCAEReservado0203ExecuteResponse executeFirmarComprobanteConNroCAEReservado(long CAENroautorizacion, int CAENroreservado, String CAESerie, short CajaId, String CteCajaUniqueId, String CTEVersionapi,
                                                                                                                 String CTEXMLEntrada, String EmpRUC, int SucId) {
        WSFirmarComprobanteConNroCAEReservado0203ExecuteResponseSOAP resp1 = executeFirmarComprobanteConNroCAEReservadoWS(CAENroautorizacion,CAENroreservado,CAESerie,CajaId,CteCajaUniqueId,CTEVersionapi,
                CTEXMLEntrada,EmpRUC,SucId);
        WSFirmarComprobanteConNroCAEReservado0203ExecuteResponse resp2 = new WSFirmarComprobanteConNroCAEReservado0203ExecuteResponse();
        resp2.setPctexmlrespuesta(resp1.Pctexmlrespuesta);
        resp2.setPerrormessage(resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        return resp2;
    }

    public WSCancelarNroCAEReservado0203ExecuteResponse executeCancelarNroCAEReservado(long CAENroAutorizacion, int CAEParteResNroCAE, String CAESerie, String EmpRUC) throws MalformedURLException {
        WSCancelarNroCAEReservado0203ExecuteResponseSOAP resp1 = executeCancelarNroCAEReservadoWS(CAENroAutorizacion,CAEParteResNroCAE,CAESerie,EmpRUC);
        WSCancelarNroCAEReservado0203ExecuteResponse resp2 = new WSCancelarNroCAEReservado0203ExecuteResponse();
        resp2.setPerrormessage(resp1.Perrormessage == null ? "" : resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        return resp2;
    }

    public WSConfirmarFirmaComprobante0200ExecuteResponse executeConfirmarfirmacomprobante(short CajaId, String XMLEntrada, String EmpRUC, int SucId) {
        WSConfirmarFirmaComprobante0200ExecuteResponseSOAP resp1 = executeConfirmarfirmacomprobanteWS(CajaId,XMLEntrada,EmpRUC,SucId);
        WSConfirmarFirmaComprobante0200ExecuteResponse resp2 = new WSConfirmarFirmaComprobante0200ExecuteResponse();
        resp2.setPerrormessage(resp1.Perrormessage == null ? "" : resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        return resp2;
    }

    public WSEnviarContingencia0200ExecuteResponse executeEnviarContingencia(short CajaId, String CTECajaUniqueId, String VersionApi, String XMLEntrada, String EmpRUC, int SucId) {
        WSEnviarContingencia0200ExecuteResponseSOAP resp1 = executeEnviarContingenciaWS(CajaId,CTECajaUniqueId,VersionApi,XMLEntrada,EmpRUC,SucId);
        WSEnviarContingencia0200ExecuteResponse resp2 = new WSEnviarContingencia0200ExecuteResponse();
        resp2.setPerrormessage(resp1.Perrormessage == null ? "" : resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        return resp2;
    }

    public WSEnviarCteSinRespuesta0200ExecuteResponse executeEnviarCteSinRespuesta(short CajaId, String XMLEntrada, String EmpRUC, int SucId) {
        WSEnviarCteSinRespuesta0200ExecuteResponseSOAP resp1 = executeEnviarCteSinRespuestaWS(CajaId,XMLEntrada,EmpRUC,SucId);
        WSEnviarCteSinRespuesta0200ExecuteResponse resp2 = new WSEnviarCteSinRespuesta0200ExecuteResponse();
        resp2.setPerrormessage(resp1.Perrormessage == null ? "" : resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        return resp2;
    }

    public WSFirmarComprobante0200ExecuteResponse executeFirmarComprobante(short CajaId, String CTECajaUniqueId, String VersionApi, String XMLEntrada, String EmpRUC, int SucId) {
        WSFirmarComprobante0200ExecuteResponseSOAP resp1 = executeFirmarComprobanteWS(CajaId,CTECajaUniqueId,VersionApi,XMLEntrada,EmpRUC,SucId);
        WSFirmarComprobante0200ExecuteResponse resp2 = new WSFirmarComprobante0200ExecuteResponse();
        resp2.setPctexmlrespuesta(resp1.Pctexmlrespuesta);
        resp2.setPerrormessage(resp1.Perrormessage == null ? "" : resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        return resp2;
    }

    public WSReservaNroCAEEstaVigente0203ExecuteResponse executeReservaNroCAEEstaVigente(long CAENroAutorizacion, int CAEParteResNroCAE, String CAESerie, String EmpRUC) {
        WSReservaNroCAEEstaVigente0203ExecuteResponseSOAP resp1 = executeReservaNroCAEEstaVigenteWS(CAENroAutorizacion,CAEParteResNroCAE,CAESerie, EmpRUC);
        WSReservaNroCAEEstaVigente0203ExecuteResponse resp2 = new WSReservaNroCAEEstaVigente0203ExecuteResponse();
        resp2.setPerrormessage(resp1.Perrormessage == null ? "" : resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        resp2.setPreservavigente(resp1.Preservavigente);
        return resp2;
    }

    public WSReservarNroCAE0203ExecuteResponse executeReservarNroCAE(String EmpRUC, int SucId, short TipoCFE) {
        WSReservarNroCAE0203ExecuteResponseSOAP resp1 = executeReservarNroCAEWS(EmpRUC,SucId,TipoCFE);
        WSReservarNroCAE0203ExecuteResponse resp2 = new WSReservarNroCAE0203ExecuteResponse();
        resp2.setPcaenroautorizacion(resp1.Pcaenroautorizacion);
        resp2.setPcaenroreservado(resp1.Pcaenroreservado);
        resp2.setPcaeserie(resp1.Pcaeserie);
        resp2.setPerrormessage(resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        return resp2;
    }

    public WSConsultarReceptorElectronicoExecuteResponse executeConsultarReceptorElectronico(String RUC) {

        WSConsultarReceptorElectronicoExecuteResponseSOAP resp1 = executeConsultaReceptorElectronicoWS(RUC);
        WSConsultarReceptorElectronicoExecuteResponse resp2 = new WSConsultarReceptorElectronicoExecuteResponse();
        resp2.setEsreceptorelectronico(resp1.Esreceptorelectronico);
        resp2.setPerrormessage(resp1.Perrormessage == null ? "" : resp1.Perrormessage);
        resp2.setPerrorreturn(resp1.Perrorreturn);
        return resp2;
        }
}