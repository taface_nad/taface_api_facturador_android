
package Negocio.WSInterface.Sucursal;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ArrayOfSDTSucursalResponse205.SDTSucursalResponse205Item.CajasItem complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSDTSucursalResponse205.SDTSucursalResponse205Item.CajasItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDTSucursalResponse205.SDTSucursalResponse205Item.CajasItem" type="{TAFACE}SDTSucursalResponse205.SDTSucursalResponse205Item.CajasItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSDTSucursalResponse205.SDTSucursalResponse205Item.CajasItem", propOrder = {
    "sdtSucursalResponse205SDTSucursalResponse205ItemCajasItem"
})
public class ArrayOfSDTSucursalResponse205SDTSucursalResponse205ItemCajasItem {

    @XmlElement(name = "SDTSucursalResponse205.SDTSucursalResponse205Item.CajasItem")
    protected List<SDTSucursalResponse205SDTSucursalResponse205ItemCajasItem> sdtSucursalResponse205SDTSucursalResponse205ItemCajasItem;

    /**
     * Gets the value of the sdtSucursalResponse205SDTSucursalResponse205ItemCajasItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdtSucursalResponse205SDTSucursalResponse205ItemCajasItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDTSucursalResponse205SDTSucursalResponse205ItemCajasItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDTSucursalResponse205SDTSucursalResponse205ItemCajasItem }
     * 
     * 
     */
    public List<SDTSucursalResponse205SDTSucursalResponse205ItemCajasItem> getSDTSucursalResponse205SDTSucursalResponse205ItemCajasItem() {
        if (sdtSucursalResponse205SDTSucursalResponse205ItemCajasItem == null) {
            sdtSucursalResponse205SDTSucursalResponse205ItemCajasItem = new ArrayList<SDTSucursalResponse205SDTSucursalResponse205ItemCajasItem>();
        }
        return this.sdtSucursalResponse205SDTSucursalResponse205ItemCajasItem;
    }

    public void setSDTSucursalResponse205SDTSucursalResponse205ItemCajasItem(List<SDTSucursalResponse205SDTSucursalResponse205ItemCajasItem> value){
        this.sdtSucursalResponse205SDTSucursalResponse205ItemCajasItem = value;
    }
}
