
package Negocio.WSInterface.Sucursal;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SDTSucursalResponse205.SDTSucursalResponse205Item complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SDTSucursalResponse205.SDTSucursalResponse205Item">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EmpId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="SucId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="SucNom" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SucCodEnDGI" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="SucModoFirma" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="SucTipoProvSQLApi" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="SucCadenaCnxSQLApi" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SucHorasOfflinePermite" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="SucRowGUID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SucEliminada" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SucLogoMiniatura" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SucLogoImpresion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SucModoSincronizacion" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="Cajas">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CajasItem" type="{TAFACE}SDTSucursalResponse205.SDTSucursalResponse205Item.CajasItem" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDTSucursalResponse205.SDTSucursalResponse205Item", propOrder = {
    "empId",
    "sucId",
    "sucNom",
    "sucCodEnDGI",
    "sucModoFirma",
    "sucTipoProvSQLApi",
    "sucCadenaCnxSQLApi",
    "sucHorasOfflinePermite",
    "sucRowGUID",
    "sucEliminada",
    "sucLogoMiniatura",
    "sucLogoImpresion",
    "sucModoSincronizacion",
    "cajas"
})
public class SDTSucursalResponse205SDTSucursalResponse205Item {

    @XmlElement(name = "EmpId")
    protected int empId;
    @XmlElement(name = "SucId")
    protected int sucId;
    @XmlElement(name = "SucNom", required = true)
    protected String sucNom;
    @XmlElement(name = "SucCodEnDGI")
    protected short sucCodEnDGI;
    @XmlElement(name = "SucModoFirma")
    protected short sucModoFirma;
    @XmlElement(name = "SucTipoProvSQLApi")
    protected short sucTipoProvSQLApi;
    @XmlElement(name = "SucCadenaCnxSQLApi", required = true)
    protected String sucCadenaCnxSQLApi;
    @XmlElement(name = "SucHorasOfflinePermite")
    protected short sucHorasOfflinePermite;
    @XmlElement(name = "SucRowGUID", required = true)
    protected String sucRowGUID;
    @XmlElement(name = "SucEliminada")
    protected boolean sucEliminada;
    @XmlElement(name = "SucLogoMiniatura", required = true)
    protected String sucLogoMiniatura;
    @XmlElement(name = "SucLogoImpresion", required = true)
    protected String sucLogoImpresion;
    @XmlElement(name = "SucModoSincronizacion")
    protected byte sucModoSincronizacion;
    @XmlElement(name = "Cajas", required = true)
    protected SDTSucursalResponse205SDTSucursalResponse205Item.Cajas cajas;

    /**
     * Obtiene el valor de la propiedad empId.
     * 
     */
    public int getEmpId() {
        return empId;
    }

    /**
     * Define el valor de la propiedad empId.
     * 
     */
    public void setEmpId(int value) {
        this.empId = value;
    }

    /**
     * Obtiene el valor de la propiedad sucId.
     * 
     */
    public int getSucId() {
        return sucId;
    }

    /**
     * Define el valor de la propiedad sucId.
     * 
     */
    public void setSucId(int value) {
        this.sucId = value;
    }

    /**
     * Obtiene el valor de la propiedad sucNom.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSucNom() {
        return sucNom;
    }

    /**
     * Define el valor de la propiedad sucNom.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSucNom(String value) {
        this.sucNom = value;
    }

    /**
     * Obtiene el valor de la propiedad sucCodEnDGI.
     * 
     */
    public short getSucCodEnDGI() {
        return sucCodEnDGI;
    }

    /**
     * Define el valor de la propiedad sucCodEnDGI.
     * 
     */
    public void setSucCodEnDGI(short value) {
        this.sucCodEnDGI = value;
    }

    /**
     * Obtiene el valor de la propiedad sucModoFirma.
     * 
     */
    public short getSucModoFirma() {
        return sucModoFirma;
    }

    /**
     * Define el valor de la propiedad sucModoFirma.
     * 
     */
    public void setSucModoFirma(short value) {
        this.sucModoFirma = value;
    }

    /**
     * Obtiene el valor de la propiedad sucTipoProvSQLApi.
     * 
     */
    public short getSucTipoProvSQLApi() {
        return sucTipoProvSQLApi;
    }

    /**
     * Define el valor de la propiedad sucTipoProvSQLApi.
     * 
     */
    public void setSucTipoProvSQLApi(short value) {
        this.sucTipoProvSQLApi = value;
    }

    /**
     * Obtiene el valor de la propiedad sucCadenaCnxSQLApi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSucCadenaCnxSQLApi() {
        return sucCadenaCnxSQLApi;
    }

    /**
     * Define el valor de la propiedad sucCadenaCnxSQLApi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSucCadenaCnxSQLApi(String value) {
        this.sucCadenaCnxSQLApi = value;
    }

    /**
     * Obtiene el valor de la propiedad sucHorasOfflinePermite.
     * 
     */
    public short getSucHorasOfflinePermite() {
        return sucHorasOfflinePermite;
    }

    /**
     * Define el valor de la propiedad sucHorasOfflinePermite.
     * 
     */
    public void setSucHorasOfflinePermite(short value) {
        this.sucHorasOfflinePermite = value;
    }

    /**
     * Obtiene el valor de la propiedad sucRowGUID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSucRowGUID() {
        return sucRowGUID;
    }

    /**
     * Define el valor de la propiedad sucRowGUID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSucRowGUID(String value) {
        this.sucRowGUID = value;
    }

    /**
     * Obtiene el valor de la propiedad sucEliminada.
     * 
     */
    public boolean isSucEliminada() {
        return sucEliminada;
    }

    /**
     * Define el valor de la propiedad sucEliminada.
     * 
     */
    public void setSucEliminada(boolean value) {
        this.sucEliminada = value;
    }

    /**
     * Obtiene el valor de la propiedad sucLogoMiniatura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSucLogoMiniatura() {
        return sucLogoMiniatura;
    }

    /**
     * Define el valor de la propiedad sucLogoMiniatura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSucLogoMiniatura(String value) {
        this.sucLogoMiniatura = value;
    }

    /**
     * Obtiene el valor de la propiedad sucLogoImpresion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSucLogoImpresion() {
        return sucLogoImpresion;
    }

    /**
     * Define el valor de la propiedad sucLogoImpresion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSucLogoImpresion(String value) {
        this.sucLogoImpresion = value;
    }

    /**
     * Obtiene el valor de la propiedad sucModoSincronizacion.
     * 
     */
    public byte getSucModoSincronizacion() {
        return sucModoSincronizacion;
    }

    /**
     * Define el valor de la propiedad sucModoSincronizacion.
     * 
     */
    public void setSucModoSincronizacion(byte value) {
        this.sucModoSincronizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad cajas.
     * 
     * @return
     *     possible object is
     *     {@link SDTSucursalResponse205SDTSucursalResponse205Item.Cajas }
     *     
     */
    public SDTSucursalResponse205SDTSucursalResponse205Item.Cajas getCajas() {
        return cajas;
    }

    /**
     * Define el valor de la propiedad cajas.
     * 
     * @param value
     *     allowed object is
     *     {@link SDTSucursalResponse205SDTSucursalResponse205Item.Cajas }
     *     
     */
    public void setCajas(SDTSucursalResponse205SDTSucursalResponse205Item.Cajas value) {
        this.cajas = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CajasItem" type="{TAFACE}SDTSucursalResponse205.SDTSucursalResponse205Item.CajasItem" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cajasItem"
    })
    public static class Cajas {

        @XmlElement(name = "CajasItem")
        protected List<SDTSucursalResponse205SDTSucursalResponse205ItemCajasItem> cajasItem;

        /**
         * Gets the value of the cajasItem property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the cajasItem property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCajasItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SDTSucursalResponse205SDTSucursalResponse205ItemCajasItem }
         * 
         * 
         */
        public List<SDTSucursalResponse205SDTSucursalResponse205ItemCajasItem> getCajasItem() {
            if (cajasItem == null) {
                cajasItem = new ArrayList<SDTSucursalResponse205SDTSucursalResponse205ItemCajasItem>();
            }
            return this.cajasItem;
        }

        public void setCajasItem(List<SDTSucursalResponse205SDTSucursalResponse205ItemCajasItem> value) {
            this.cajasItem = value;
        }

    }

}
