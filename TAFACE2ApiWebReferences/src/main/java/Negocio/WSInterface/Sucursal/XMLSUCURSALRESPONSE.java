
package Negocio.WSInterface.Sucursal;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para XMLSUCURSALRESPONSE complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="XMLSUCURSALRESPONSE">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDTSucursalResponse205.SDTSucursalResponse205Item" type="{TAFACE}SDTSucursalResponse205.SDTSucursalResponse205Item" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XMLSUCURSALRESPONSE", propOrder = {
    "sdtSucursalResponse205SDTSucursalResponse205Item"
})
public class XMLSUCURSALRESPONSE {

    @XmlElement(name = "SDTSucursalResponse205.SDTSucursalResponse205Item")
    protected List<SDTSucursalResponse205SDTSucursalResponse205Item> sdtSucursalResponse205SDTSucursalResponse205Item;

    /**
     * Gets the value of the sdtSucursalResponse205SDTSucursalResponse205Item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdtSucursalResponse205SDTSucursalResponse205Item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDTSucursalResponse205SDTSucursalResponse205Item().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDTSucursalResponse205SDTSucursalResponse205Item }
     * 
     * 
     */
    public List<SDTSucursalResponse205SDTSucursalResponse205Item> getSDTSucursalResponse205SDTSucursalResponse205Item() {
        if (sdtSucursalResponse205SDTSucursalResponse205Item == null) {
            sdtSucursalResponse205SDTSucursalResponse205Item = new ArrayList<SDTSucursalResponse205SDTSucursalResponse205Item>();
        }
        return this.sdtSucursalResponse205SDTSucursalResponse205Item;
    }

    public void setSDTSucursalResponse205SDTSucursalResponse205Item(List<SDTSucursalResponse205SDTSucursalResponse205Item> value) {
        this.sdtSucursalResponse205SDTSucursalResponse205Item = value;
    }

}
