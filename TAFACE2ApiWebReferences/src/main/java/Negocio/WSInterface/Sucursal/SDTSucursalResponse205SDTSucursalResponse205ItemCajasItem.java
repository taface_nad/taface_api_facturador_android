
package Negocio.WSInterface.Sucursal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SDTSucursalResponse205.SDTSucursalResponse205Item.CajasItem complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SDTSucursalResponse205.SDTSucursalResponse205Item.CajasItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CajaId" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="CajaUniqueId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDTSucursalResponse205.SDTSucursalResponse205Item.CajasItem", propOrder = {
    "cajaId",
    "cajaUniqueId"
})
public class SDTSucursalResponse205SDTSucursalResponse205ItemCajasItem {

    @XmlElement(name = "CajaId")
    protected short cajaId;
    @XmlElement(name = "CajaUniqueId", required = true)
    protected String cajaUniqueId;

    /**
     * Obtiene el valor de la propiedad cajaId.
     * 
     */
    public short getCajaId() {
        return cajaId;
    }

    /**
     * Define el valor de la propiedad cajaId.
     * 
     */
    public void setCajaId(short value) {
        this.cajaId = value;
    }

    /**
     * Obtiene el valor de la propiedad cajaUniqueId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCajaUniqueId() {
        return cajaUniqueId;
    }

    /**
     * Define el valor de la propiedad cajaUniqueId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCajaUniqueId(String value) {
        this.cajaUniqueId = value;
    }

}
