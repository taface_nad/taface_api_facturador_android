
package Negocio.WSInterface.CertificadoDGI;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="psdtcertificadodgiresponse" type="{TAFACE}SDTCertificadoDGIResponseSOAP"/>
 *         &lt;element name="Perrorreturn" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Perrormessage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "psdtcertificadodgiresponse",
    "perrorreturn",
    "perrormessage"
})
@XmlRootElement(name = "WSSincronizacionCertificadosDGI_0202.ExecuteResponse")
public class WSSincronizacionCertificadosDGI0202ExecuteResponse {

    @XmlElement(name = "psdtcertificadodgiresponse", required = true)
    protected SDTCertificadoDGIResponse psdtcertificadodgiresponse;
    @XmlElement(name = "Perrorreturn")
    protected boolean perrorreturn;
    @XmlElement(name = "Perrormessage", required = true)
    protected String perrormessage;

    /**
     * Obtiene el valor de la propiedad psdtcertificadodgiresponse.
     * 
     * @return
     *     possible object is
     *     {@link SDTCertificadoDGIResponse }
     *     
     */
    public SDTCertificadoDGIResponse getPsdtcertificadodgiresponse() {
        return psdtcertificadodgiresponse;
    }

    /**
     * Define el valor de la propiedad psdtcertificadodgiresponse.
     * 
     * @param value
     *     allowed object is
     *     {@link SDTCertificadoDGIResponse }
     *     
     */
    public void setPsdtcertificadodgiresponse(SDTCertificadoDGIResponse value) {
        this.psdtcertificadodgiresponse = value;
    }

    /**
     * Obtiene el valor de la propiedad perrorreturn.
     * 
     */
    public boolean isPerrorreturn() {
        return perrorreturn;
    }

    /**
     * Define el valor de la propiedad perrorreturn.
     * 
     */
    public void setPerrorreturn(boolean value) {
        this.perrorreturn = value;
    }

    /**
     * Obtiene el valor de la propiedad perrormessage.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerrormessage() {
        return perrormessage;
    }

    /**
     * Define el valor de la propiedad perrormessage.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerrormessage(String value) {
        this.perrormessage = value;
    }

    /**
     * Obtiene el valor de la propiedad perrorreturn.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public boolean getPerrorreturn() {
        return perrorreturn;
    }

}
