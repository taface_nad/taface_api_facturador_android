
package Negocio.WSInterface.CertificadoDGI;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SDTCertificadoDGIRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SDTCertificadoDGIRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDTCertificadoDGIRequest.SDTCertificadoDGIRequestItem" type="{TAFACE}SDTCertificadoDGIRequest.SDTCertificadoDGIRequestItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDTCertificadoDGIRequest", propOrder = {
    "sdtCertificadoDGIRequestSDTCertificadoDGIRequestItem"
})
public class SDTCertificadoDGIRequest {

    @XmlElement(name = "SDTCertificadoDGIRequest.SDTCertificadoDGIRequestItem")
    protected List<SDTCertificadoDGIRequestSDTCertificadoDGIRequestItem> sdtCertificadoDGIRequestSDTCertificadoDGIRequestItem;

    /**
     * Gets the value of the sdtCertificadoDGIRequestSDTCertificadoDGIRequestItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdtCertificadoDGIRequestSDTCertificadoDGIRequestItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDTCertificadoDGIRequestSDTCertificadoDGIRequestItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDTCertificadoDGIRequestSDTCertificadoDGIRequestItem }
     * 
     * 
     */
    public List<SDTCertificadoDGIRequestSDTCertificadoDGIRequestItem> getSDTCertificadoDGIRequestSDTCertificadoDGIRequestItem() {
        if (sdtCertificadoDGIRequestSDTCertificadoDGIRequestItem == null) {
            sdtCertificadoDGIRequestSDTCertificadoDGIRequestItem = new ArrayList<SDTCertificadoDGIRequestSDTCertificadoDGIRequestItem>();
        }
        return this.sdtCertificadoDGIRequestSDTCertificadoDGIRequestItem;
    }

    public void setSDTCertificadoDGIRequestSDTCertificadoDGIRequestItem(List<SDTCertificadoDGIRequestSDTCertificadoDGIRequestItem> value) {
        this.sdtCertificadoDGIRequestSDTCertificadoDGIRequestItem = value;
    }

}
