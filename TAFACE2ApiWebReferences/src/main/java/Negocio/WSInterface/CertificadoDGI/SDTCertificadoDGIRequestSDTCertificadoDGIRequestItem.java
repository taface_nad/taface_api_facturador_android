
package Negocio.WSInterface.CertificadoDGI;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SDTCertificadoDGIRequest.SDTCertificadoDGIRequestItem complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SDTCertificadoDGIRequest.SDTCertificadoDGIRequestItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EmpId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CertDGIId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CertDGIRowGUID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDTCertificadoDGIRequest.SDTCertificadoDGIRequestItem", propOrder = {
    "empId",
    "certDGIId",
    "certDGIRowGUID"
})
public class SDTCertificadoDGIRequestSDTCertificadoDGIRequestItem {

    @XmlElement(name = "EmpId")
    protected int empId;
    @XmlElement(name = "CertDGIId")
    protected int certDGIId;
    @XmlElement(name = "CertDGIRowGUID", required = true)
    protected String certDGIRowGUID;

    /**
     * Obtiene el valor de la propiedad empId.
     * 
     */
    public int getEmpId() {
        return empId;
    }

    /**
     * Define el valor de la propiedad empId.
     * 
     */
    public void setEmpId(int value) {
        this.empId = value;
    }

    /**
     * Obtiene el valor de la propiedad certDGIId.
     * 
     */
    public int getCertDGIId() {
        return certDGIId;
    }

    /**
     * Define el valor de la propiedad certDGIId.
     * 
     */
    public void setCertDGIId(int value) {
        this.certDGIId = value;
    }

    /**
     * Obtiene el valor de la propiedad certDGIRowGUID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertDGIRowGUID() {
        return certDGIRowGUID;
    }

    /**
     * Define el valor de la propiedad certDGIRowGUID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertDGIRowGUID(String value) {
        this.certDGIRowGUID = value;
    }

}
