
package Negocio.WSInterface.CertificadoDGI;

import java.util.GregorianCalendar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para SDTCertificadoDGIResponseSOAP.SDTCertificadoDGIResponseItem complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SDTCertificadoDGIResponseSOAP.SDTCertificadoDGIResponseItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EmpId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CertDGIId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CertDGIVigencia" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="CertDGIVencimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="CertDGIArchivo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CertDGIFchHora" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="CertDGICN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CertDGIRowGUID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CertDGIEliminado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDTCertificadoDGIResponseSOAP.SDTCertificadoDGIResponseItem", propOrder = {
    "empId",
    "certDGIId",
    "certDGIVigencia",
    "certDGIVencimiento",
    "certDGIArchivo",
    "certDGIFchHora",
    "certDGICN",
    "certDGIRowGUID",
    "certDGIEliminado"
})
public class SDTCertificadoDGIResponseSDTCertificadoDGIResponseItem {

    @XmlElement(name = "EmpId")
    protected int empId;
    @XmlElement(name = "CertDGIId")
    protected int certDGIId;
    @XmlElement(name = "CertDGIVigencia", required = true, nillable = true)
    @XmlSchemaType(name = "date")
    protected GregorianCalendar certDGIVigencia;
    @XmlElement(name = "CertDGIVencimiento", required = true, nillable = true)
    @XmlSchemaType(name = "date")
    protected GregorianCalendar certDGIVencimiento;
    @XmlElement(name = "CertDGIArchivo", required = true)
    protected String certDGIArchivo;
    @XmlElement(name = "CertDGIFchHora", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected GregorianCalendar certDGIFchHora;
    @XmlElement(name = "CertDGICN", required = true)
    protected String certDGICN;
    @XmlElement(name = "CertDGIRowGUID", required = true)
    protected String certDGIRowGUID;
    @XmlElement(name = "CertDGIEliminado")
    protected boolean certDGIEliminado;

    /**
     * Obtiene el valor de la propiedad empId.
     * 
     */
    public int getEmpId() {
        return empId;
    }

    /**
     * Define el valor de la propiedad empId.
     * 
     */
    public void setEmpId(int value) {
        this.empId = value;
    }

    /**
     * Obtiene el valor de la propiedad certDGIId.
     * 
     */
    public int getCertDGIId() {
        return certDGIId;
    }

    /**
     * Define el valor de la propiedad certDGIId.
     * 
     */
    public void setCertDGIId(int value) {
        this.certDGIId = value;
    }

    /**
     * Obtiene el valor de la propiedad certDGIVigencia.
     * 
     * @return
     *     possible object is
     *     {@link GregorianCalendar }
     *     
     */
    public GregorianCalendar getCertDGIVigencia() {
        return certDGIVigencia;
    }

    /**
     * Define el valor de la propiedad certDGIVigencia.
     * 
     * @param value
     *     allowed object is
     *     {@link GregorianCalendar }
     *     
     */
    public void setCertDGIVigencia(GregorianCalendar value) {
        this.certDGIVigencia = value;
    }

    /**
     * Obtiene el valor de la propiedad certDGIVencimiento.
     * 
     * @return
     *     possible object is
     *     {@link GregorianCalendar }
     *     
     */
    public GregorianCalendar getCertDGIVencimiento() {
        return certDGIVencimiento;
    }

    /**
     * Define el valor de la propiedad certDGIVencimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link GregorianCalendar }
     *     
     */
    public void setCertDGIVencimiento(GregorianCalendar value) {
        this.certDGIVencimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad certDGIArchivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertDGIArchivo() {
        return certDGIArchivo;
    }

    /**
     * Define el valor de la propiedad certDGIArchivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertDGIArchivo(String value) {
        this.certDGIArchivo = value;
    }

    /**
     * Obtiene el valor de la propiedad certDGIFchHora.
     * 
     * @return
     *     possible object is
     *     {@link GregorianCalendar }
     *     
     */
    public GregorianCalendar getCertDGIFchHora() {
        return certDGIFchHora;
    }

    /**
     * Define el valor de la propiedad certDGIFchHora.
     * 
     * @param value
     *     allowed object is
     *     {@link GregorianCalendar }
     *     
     */
    public void setCertDGIFchHora(GregorianCalendar value) {
        this.certDGIFchHora = value;
    }

    /**
     * Obtiene el valor de la propiedad certDGICN.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCertDGICN() {
        return certDGICN;
    }

    /**
     * Define el valor de la propiedad certDGICN.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCertDGICN(String value) {
        this.certDGICN = value;
    }

    /**
     * Obtiene el valor de la propiedad certDGIRowGUID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertDGIRowGUID() {
        return certDGIRowGUID;
    }

    /**
     * Define el valor de la propiedad certDGIRowGUID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertDGIRowGUID(String value) {
        this.certDGIRowGUID = value;
    }

    /**
     * Obtiene el valor de la propiedad certDGIEliminado.
     * 
     */
    public boolean isCertDGIEliminado() {
        return certDGIEliminado;
    }

    /**
     * Define el valor de la propiedad certDGIEliminado.
     * 
     */
    public void setCertDGIEliminado(boolean value) {
        this.certDGIEliminado = value;
    }

}
