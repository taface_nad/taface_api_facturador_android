
package Negocio.WSInterface.CertificadoDGI;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the CertificadoDGIWR package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: CertificadoDGIWR
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WSSincronizacionCertificadosDGI0202ExecuteResponse }
     * 
     */
    public WSSincronizacionCertificadosDGI0202ExecuteResponse createWSSincronizacionCertificadosDGI0202ExecuteResponse() {
        return new WSSincronizacionCertificadosDGI0202ExecuteResponse();
    }

    /**
     * Create an instance of {@link SDTCertificadoDGIResponse }
     * 
     */
    public SDTCertificadoDGIResponse createSDTCertificadoDGIResponse() {
        return new SDTCertificadoDGIResponse();
    }

    /**
     * Create an instance of {@link SDTCertificadoDGIResponseSDTCertificadoDGIResponseItem }
     * 
     */
    public SDTCertificadoDGIResponseSDTCertificadoDGIResponseItem createSDTCertificadoDGIResponseSDTCertificadoDGIResponseItem() {
        return new SDTCertificadoDGIResponseSDTCertificadoDGIResponseItem();
    }

}
