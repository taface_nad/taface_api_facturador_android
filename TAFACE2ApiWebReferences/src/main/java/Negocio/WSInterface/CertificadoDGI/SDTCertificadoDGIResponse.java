
package Negocio.WSInterface.CertificadoDGI;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SDTCertificadoDGIResponseSOAP complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SDTCertificadoDGIResponseSOAP">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDTCertificadoDGIResponseSOAP.SDTCertificadoDGIResponseItem" type="{TAFACE}SDTCertificadoDGIResponseSOAP.SDTCertificadoDGIResponseItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDTCertificadoDGIResponseSOAP", propOrder = {
    "sdtCertificadoDGIResponseSDTCertificadoDGIResponseItem"
})
public class SDTCertificadoDGIResponse {

    @XmlElement(name = "SDTCertificadoDGIResponseSOAP.SDTCertificadoDGIResponseItem")
    protected List<SDTCertificadoDGIResponseSDTCertificadoDGIResponseItem> sdtCertificadoDGIResponseSDTCertificadoDGIResponseItem;

    /**
     * Gets the value of the sdtCertificadoDGIResponseSDTCertificadoDGIResponseItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdtCertificadoDGIResponseSDTCertificadoDGIResponseItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDTCertificadoDGIResponseSDTCertificadoDGIResponseItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDTCertificadoDGIResponseSDTCertificadoDGIResponseItem }
     * 
     * 
     */
    public List<SDTCertificadoDGIResponseSDTCertificadoDGIResponseItem> getSDTCertificadoDGIResponseSDTCertificadoDGIResponseItem() {
        if (sdtCertificadoDGIResponseSDTCertificadoDGIResponseItem == null) {
            sdtCertificadoDGIResponseSDTCertificadoDGIResponseItem = new ArrayList<SDTCertificadoDGIResponseSDTCertificadoDGIResponseItem>();
        }
        return this.sdtCertificadoDGIResponseSDTCertificadoDGIResponseItem;
    }

    public void setSDTCertificadoDGIResponseSDTCertificadoDGIResponseItem( List<SDTCertificadoDGIResponseSDTCertificadoDGIResponseItem> value) {
        this.sdtCertificadoDGIResponseSDTCertificadoDGIResponseItem = value;
    }

}
