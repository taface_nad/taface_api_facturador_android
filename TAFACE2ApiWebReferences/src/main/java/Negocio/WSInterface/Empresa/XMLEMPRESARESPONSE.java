
package Negocio.WSInterface.Empresa;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para XMLEMPRESARESPONSE complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="XMLEMPRESARESPONSE">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDTEmpresaResponse202.SDTEmpresaResponse202Item" type="{TAFACE}SDTEmpresaResponse202.SDTEmpresaResponse202Item" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XMLEMPRESARESPONSE", propOrder = {
    "sdtEmpresaResponse202SDTEmpresaResponse202Item"
})
public class XMLEMPRESARESPONSE {

    @XmlElement(name = "SDTEmpresaResponse202.SDTEmpresaResponse202Item")
    protected List<SDTEmpresaResponse202SDTEmpresaResponse202Item> sdtEmpresaResponse202SDTEmpresaResponse202Item;

    /**
     * Gets the value of the sdtEmpresaResponse202SDTEmpresaResponse202Item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdtEmpresaResponse202SDTEmpresaResponse202Item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDTEmpresaResponse202SDTEmpresaResponse202Item().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDTEmpresaResponse202SDTEmpresaResponse202Item }
     * 
     * 
     */
    public List<SDTEmpresaResponse202SDTEmpresaResponse202Item> getSDTEmpresaResponse202SDTEmpresaResponse202Item() {
        if (sdtEmpresaResponse202SDTEmpresaResponse202Item == null) {
            sdtEmpresaResponse202SDTEmpresaResponse202Item = new ArrayList<SDTEmpresaResponse202SDTEmpresaResponse202Item>();
        }
        return this.sdtEmpresaResponse202SDTEmpresaResponse202Item;
    }

    public void setSDTEmpresaResponse202SDTEmpresaResponse202Item(List<SDTEmpresaResponse202SDTEmpresaResponse202Item> value) {
        this.sdtEmpresaResponse202SDTEmpresaResponse202Item = value;
    }

}
