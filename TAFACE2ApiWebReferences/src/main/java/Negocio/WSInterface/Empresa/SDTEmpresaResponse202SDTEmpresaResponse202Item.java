
package Negocio.WSInterface.Empresa;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SDTEmpresaResponse202.SDTEmpresaResponse202Item complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SDTEmpresaResponse202.SDTEmpresaResponse202Item">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EmpId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="EmpResolucionIVA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EmpUrlConsultaCFE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EmpEmiteRemitosExportacion" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="EmpEmiteFacturasExportacion" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="EmpEmiteResguardos" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="EmpEmiteRemitos" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="EmpEmiteFacturas" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="EmpEmiteTickets" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="EmpEmiteCuentaAjena" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="EmpNom" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EmpRUC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EmpLogoMiniatura" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EmpLogoImpresion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EmpRowGUID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDTEmpresaResponse202.SDTEmpresaResponse202Item", propOrder = {
    "empId",
    "empResolucionIVA",
    "empUrlConsultaCFE",
    "empEmiteRemitosExportacion",
    "empEmiteFacturasExportacion",
    "empEmiteResguardos",
    "empEmiteRemitos",
    "empEmiteFacturas",
    "empEmiteTickets",
    "empEmiteCuentaAjena",
    "empNom",
    "empRUC",
    "empLogoMiniatura",
    "empLogoImpresion",
    "empRowGUID"
})
public class SDTEmpresaResponse202SDTEmpresaResponse202Item {

    @XmlElement(name = "EmpId")
    protected int empId;
    @XmlElement(name = "EmpResolucionIVA", required = true)
    protected String empResolucionIVA;
    @XmlElement(name = "EmpUrlConsultaCFE", required = true)
    protected String empUrlConsultaCFE;
    @XmlElement(name = "EmpEmiteRemitosExportacion")
    protected boolean empEmiteRemitosExportacion;
    @XmlElement(name = "EmpEmiteFacturasExportacion")
    protected boolean empEmiteFacturasExportacion;
    @XmlElement(name = "EmpEmiteResguardos")
    protected boolean empEmiteResguardos;
    @XmlElement(name = "EmpEmiteRemitos")
    protected boolean empEmiteRemitos;
    @XmlElement(name = "EmpEmiteFacturas")
    protected boolean empEmiteFacturas;
    @XmlElement(name = "EmpEmiteTickets")
    protected boolean empEmiteTickets;
    @XmlElement(name = "EmpEmiteCuentaAjena")
    protected boolean empEmiteCuentaAjena;
    @XmlElement(name = "EmpNom", required = true)
    protected String empNom;
    @XmlElement(name = "EmpRUC", required = true)
    protected String empRUC;
    @XmlElement(name = "EmpLogoMiniatura", required = true)
    protected String empLogoMiniatura;
    @XmlElement(name = "EmpLogoImpresion", required = true)
    protected String empLogoImpresion;
    @XmlElement(name = "EmpRowGUID", required = true)
    protected String empRowGUID;

    /**
     * Obtiene el valor de la propiedad empId.
     * 
     */
    public int getEmpId() {
        return empId;
    }

    /**
     * Define el valor de la propiedad empId.
     * 
     */
    public void setEmpId(int value) {
        this.empId = value;
    }

    /**
     * Obtiene el valor de la propiedad empResolucionIVA.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpResolucionIVA() {
        return empResolucionIVA;
    }

    /**
     * Define el valor de la propiedad empResolucionIVA.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpResolucionIVA(String value) {
        this.empResolucionIVA = value;
    }

    /**
     * Obtiene el valor de la propiedad empUrlConsultaCFE.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpUrlConsultaCFE() {
        return empUrlConsultaCFE;
    }

    /**
     * Define el valor de la propiedad empUrlConsultaCFE.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpUrlConsultaCFE(String value) {
        this.empUrlConsultaCFE = value;
    }

    /**
     * Obtiene el valor de la propiedad empEmiteRemitosExportacion.
     * 
     */
    public boolean isEmpEmiteRemitosExportacion() {
        return empEmiteRemitosExportacion;
    }

    /**
     * Define el valor de la propiedad empEmiteRemitosExportacion.
     * 
     */
    public void setEmpEmiteRemitosExportacion(boolean value) {
        this.empEmiteRemitosExportacion = value;
    }

    /**
     * Obtiene el valor de la propiedad empEmiteFacturasExportacion.
     * 
     */
    public boolean isEmpEmiteFacturasExportacion() {
        return empEmiteFacturasExportacion;
    }

    /**
     * Define el valor de la propiedad empEmiteFacturasExportacion.
     * 
     */
    public void setEmpEmiteFacturasExportacion(boolean value) {
        this.empEmiteFacturasExportacion = value;
    }

    /**
     * Obtiene el valor de la propiedad empEmiteResguardos.
     * 
     */
    public boolean isEmpEmiteResguardos() {
        return empEmiteResguardos;
    }

    /**
     * Define el valor de la propiedad empEmiteResguardos.
     * 
     */
    public void setEmpEmiteResguardos(boolean value) {
        this.empEmiteResguardos = value;
    }

    /**
     * Obtiene el valor de la propiedad empEmiteRemitos.
     * 
     */
    public boolean isEmpEmiteRemitos() {
        return empEmiteRemitos;
    }

    /**
     * Define el valor de la propiedad empEmiteRemitos.
     * 
     */
    public void setEmpEmiteRemitos(boolean value) {
        this.empEmiteRemitos = value;
    }

    /**
     * Obtiene el valor de la propiedad empEmiteFacturas.
     * 
     */
    public boolean isEmpEmiteFacturas() {
        return empEmiteFacturas;
    }

    /**
     * Define el valor de la propiedad empEmiteFacturas.
     * 
     */
    public void setEmpEmiteFacturas(boolean value) {
        this.empEmiteFacturas = value;
    }

    /**
     * Obtiene el valor de la propiedad empEmiteTickets.
     * 
     */
    public boolean isEmpEmiteTickets() {
        return empEmiteTickets;
    }

    /**
     * Define el valor de la propiedad empEmiteTickets.
     * 
     */
    public void setEmpEmiteTickets(boolean value) {
        this.empEmiteTickets = value;
    }

    /**
     * Obtiene el valor de la propiedad empEmiteCuentaAjena.
     * 
     */
    public boolean isEmpEmiteCuentaAjena() {
        return empEmiteCuentaAjena;
    }

    /**
     * Define el valor de la propiedad empEmiteCuentaAjena.
     * 
     */
    public void setEmpEmiteCuentaAjena(boolean value) {
        this.empEmiteCuentaAjena = value;
    }

    /**
     * Obtiene el valor de la propiedad empNom.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpNom() {
        return empNom;
    }

    /**
     * Define el valor de la propiedad empNom.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpNom(String value) {
        this.empNom = value;
    }

    /**
     * Obtiene el valor de la propiedad empRUC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpRUC() {
        return empRUC;
    }

    /**
     * Define el valor de la propiedad empRUC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpRUC(String value) {
        this.empRUC = value;
    }

    /**
     * Obtiene el valor de la propiedad empLogoMiniatura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpLogoMiniatura() {
        return empLogoMiniatura;
    }

    /**
     * Define el valor de la propiedad empLogoMiniatura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpLogoMiniatura(String value) {
        this.empLogoMiniatura = value;
    }

    /**
     * Obtiene el valor de la propiedad empLogoImpresion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpLogoImpresion() {
        return empLogoImpresion;
    }

    /**
     * Define el valor de la propiedad empLogoImpresion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpLogoImpresion(String value) {
        this.empLogoImpresion = value;
    }

    /**
     * Obtiene el valor de la propiedad empRowGUID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpRowGUID() {
        return empRowGUID;
    }

    /**
     * Define el valor de la propiedad empRowGUID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpRowGUID(String value) {
        this.empRowGUID = value;
    }

}
