
package Negocio.WSInterface.CAEParte;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the CAEParteWR package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: CAEParteWR
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WSSincronizacionCAEParte0206ExecuteResponse }
     * 
     */
    public Negocio.WSInterface.CAEParte.WSSincronizacionCAEParte0206ExecuteResponse createWSSincronizacionCAEParte0206ExecuteResponse() {
        return new Negocio.WSInterface.CAEParte.WSSincronizacionCAEParte0206ExecuteResponse();
    }

    /**
     * Create an instance of {@link XMLCAEPARTERESPONSE }
     * 
     */
    public XMLCAEPARTERESPONSE createXMLCAEPARTERESPONSE() {
        return new XMLCAEPARTERESPONSE();
    }


    /**
     * Create an instance of {@link SDTCAEParteResponse206SDTCAEParteResponse206Item }
     * 
     */
    public SDTCAEParteResponse206SDTCAEParteResponse206Item createSDTCAEParteResponse206SDTCAEParteResponse206Item() {
        return new SDTCAEParteResponse206SDTCAEParteResponse206Item();
    }

    /**
     * Create an instance of {@link SDTCAEParteResponse206SDTCAEParteResponse206ItemCAE }
     * 
     */
    public SDTCAEParteResponse206SDTCAEParteResponse206ItemCAE createSDTCAEParteResponse206SDTCAEParteResponse206ItemCAE() {
        return new SDTCAEParteResponse206SDTCAEParteResponse206ItemCAE();
    }

}
