
package Negocio.WSInterface.CAEParte;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SDTCAEParteResponse206.SDTCAEParteResponse206Item complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SDTCAEParteResponse206.SDTCAEParteResponse206Item">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EmpId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CAEId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CAEParteId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="SucId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CajaId" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="CAEParteNroDesde" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CAEParteNroHasta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CAEParteUltAsignado" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CAEParteRowGUID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CAEParteEliminado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CAE" type="{TAFACE}SDTCAEParteResponse206.SDTCAEParteResponse206Item.CAE"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDTCAEParteResponse206.SDTCAEParteResponse206Item", propOrder = {
    "empId",
    "caeId",
    "caeParteId",
    "sucId",
    "cajaId",
    "caeParteNroDesde",
    "caeParteNroHasta",
    "caeParteUltAsignado",
    "caeParteRowGUID",
    "caeParteEliminado",
    "cae"
})
public class SDTCAEParteResponse206SDTCAEParteResponse206Item {

    @XmlElement(name = "EmpId")
    protected int empId;
    @XmlElement(name = "CAEId")
    protected int caeId;
    @XmlElement(name = "CAEParteId")
    protected int caeParteId;
    @XmlElement(name = "SucId")
    protected int sucId;
    @XmlElement(name = "CajaId")
    protected short cajaId;
    @XmlElement(name = "CAEParteNroDesde")
    protected int caeParteNroDesde;
    @XmlElement(name = "CAEParteNroHasta")
    protected int caeParteNroHasta;
    @XmlElement(name = "CAEParteUltAsignado")
    protected int caeParteUltAsignado;
    @XmlElement(name = "CAEParteRowGUID", required = true)
    protected String caeParteRowGUID;
    @XmlElement(name = "CAEParteEliminado")
    protected boolean caeParteEliminado;
    @XmlElement(name = "CAE", required = true)
    protected SDTCAEParteResponse206SDTCAEParteResponse206ItemCAE cae;

    /**
     * Obtiene el valor de la propiedad empId.
     * 
     */
    public int getEmpId() {
        return empId;
    }

    /**
     * Define el valor de la propiedad empId.
     * 
     */
    public void setEmpId(int value) {
        this.empId = value;
    }

    /**
     * Obtiene el valor de la propiedad caeId.
     * 
     */
    public int getCAEId() {
        return caeId;
    }

    /**
     * Define el valor de la propiedad caeId.
     * 
     */
    public void setCAEId(int value) {
        this.caeId = value;
    }

    /**
     * Obtiene el valor de la propiedad caeParteId.
     * 
     */
    public int getCAEParteId() {
        return caeParteId;
    }

    /**
     * Define el valor de la propiedad caeParteId.
     * 
     */
    public void setCAEParteId(int value) {
        this.caeParteId = value;
    }

    /**
     * Obtiene el valor de la propiedad sucId.
     * 
     */
    public int getSucId() {
        return sucId;
    }

    /**
     * Define el valor de la propiedad sucId.
     * 
     */
    public void setSucId(int value) {
        this.sucId = value;
    }

    /**
     * Obtiene el valor de la propiedad cajaId.
     * 
     */
    public short getCajaId() {
        return cajaId;
    }

    /**
     * Define el valor de la propiedad cajaId.
     * 
     */
    public void setCajaId(short value) {
        this.cajaId = value;
    }

    /**
     * Obtiene el valor de la propiedad caeParteNroDesde.
     * 
     */
    public int getCAEParteNroDesde() {
        return caeParteNroDesde;
    }

    /**
     * Define el valor de la propiedad caeParteNroDesde.
     * 
     */
    public void setCAEParteNroDesde(int value) {
        this.caeParteNroDesde = value;
    }

    /**
     * Obtiene el valor de la propiedad caeParteNroHasta.
     * 
     */
    public int getCAEParteNroHasta() {
        return caeParteNroHasta;
    }

    /**
     * Define el valor de la propiedad caeParteNroHasta.
     * 
     */
    public void setCAEParteNroHasta(int value) {
        this.caeParteNroHasta = value;
    }

    /**
     * Obtiene el valor de la propiedad caeParteUltAsignado.
     * 
     */
    public int getCAEParteUltAsignado() {
        return caeParteUltAsignado;
    }

    /**
     * Define el valor de la propiedad caeParteUltAsignado.
     * 
     */
    public void setCAEParteUltAsignado(int value) {
        this.caeParteUltAsignado = value;
    }

    /**
     * Obtiene el valor de la propiedad caeParteRowGUID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCAEParteRowGUID() {
        return caeParteRowGUID;
    }

    /**
     * Define el valor de la propiedad caeParteRowGUID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCAEParteRowGUID(String value) {
        this.caeParteRowGUID = value;
    }

    /**
     * Obtiene el valor de la propiedad caeParteEliminado.
     * 
     */
    public boolean isCAEParteEliminado() {
        return caeParteEliminado;
    }

    /**
     * Define el valor de la propiedad caeParteEliminado.
     * 
     */
    public void setCAEParteEliminado(boolean value) {
        this.caeParteEliminado = value;
    }

    /**
     * Obtiene el valor de la propiedad cae.
     * 
     * @return
     *     possible object is
     *     {@link SDTCAEParteResponse206SDTCAEParteResponse206ItemCAE }
     *     
     */
    public SDTCAEParteResponse206SDTCAEParteResponse206ItemCAE getCAE() {
        return cae;
    }

    /**
     * Define el valor de la propiedad cae.
     * 
     * @param value
     *     allowed object is
     *     {@link SDTCAEParteResponse206SDTCAEParteResponse206ItemCAE }
     *     
     */
    public void setCAE(SDTCAEParteResponse206SDTCAEParteResponse206ItemCAE value) {
        this.cae = value;
    }

}
