
package Negocio.WSInterface.CAEParte;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para XMLCAEPARTERESPONSE complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="XMLCAEPARTERESPONSE">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDTCAEParteResponse206.SDTCAEParteResponse206Item" type="{TAFACE}SDTCAEParteResponse206.SDTCAEParteResponse206Item" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XMLCAEPARTERESPONSE", propOrder = {
    "sdtcaeParteResponse206SDTCAEParteResponse206Item"
})
public class XMLCAEPARTERESPONSE {

    @XmlElement(name = "SDTCAEParteResponse206.SDTCAEParteResponse206Item")
    protected List<SDTCAEParteResponse206SDTCAEParteResponse206Item> sdtcaeParteResponse206SDTCAEParteResponse206Item;

    /**
     * Gets the value of the sdtcaeParteResponse206SDTCAEParteResponse206Item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdtcaeParteResponse206SDTCAEParteResponse206Item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDTCAEParteResponse206SDTCAEParteResponse206Item().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDTCAEParteResponse206SDTCAEParteResponse206Item }
     * 
     * 
     */
    public List<SDTCAEParteResponse206SDTCAEParteResponse206Item> getSDTCAEParteResponse206SDTCAEParteResponse206Item() {
        if (sdtcaeParteResponse206SDTCAEParteResponse206Item == null) {
            sdtcaeParteResponse206SDTCAEParteResponse206Item = new ArrayList<SDTCAEParteResponse206SDTCAEParteResponse206Item>();
        }
        return this.sdtcaeParteResponse206SDTCAEParteResponse206Item;
    }

    public void setSDTCAEParteResponse206SDTCAEParteResponse206Item(List<SDTCAEParteResponse206SDTCAEParteResponse206Item> value) {

        this.sdtcaeParteResponse206SDTCAEParteResponse206Item = value;
    }

}
