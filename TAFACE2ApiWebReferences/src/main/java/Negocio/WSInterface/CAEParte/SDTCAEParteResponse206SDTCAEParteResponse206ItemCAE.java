
package Negocio.WSInterface.CAEParte;

import java.util.GregorianCalendar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para SDTCAEParteResponse206.SDTCAEParteResponse206Item.CAE complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SDTCAEParteResponse206.SDTCAEParteResponse206Item.CAE">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EmpId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CAEId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CAENroAutorizacion" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="CAETipoCFE" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="CAESerie" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CAENroDesde" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CAENroHasta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CAEVigencia" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="CAEVencimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="CAEAnulado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CAEFchHora" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="CAECAusalTipo" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="CAEEspecial" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDTCAEParteResponse206.SDTCAEParteResponse206Item.CAE", propOrder = {
    "empId",
    "caeId",
    "caeNroAutorizacion",
    "caeTipoCFE",
    "caeSerie",
    "caeNroDesde",
    "caeNroHasta",
    "caeVigencia",
    "caeVencimiento",
    "caeAnulado",
    "caeFchHora",
    "caecAusalTipo",
    "caeEspecial"
})
public class SDTCAEParteResponse206SDTCAEParteResponse206ItemCAE {

    @XmlElement(name = "EmpId")
    protected int empId;
    @XmlElement(name = "CAEId")
    protected int caeId;
    @XmlElement(name = "CAENroAutorizacion")
    protected long caeNroAutorizacion;
    @XmlElement(name = "CAETipoCFE")
    protected short caeTipoCFE;
    @XmlElement(name = "CAESerie", required = true)
    protected String caeSerie;
    @XmlElement(name = "CAENroDesde")
    protected int caeNroDesde;
    @XmlElement(name = "CAENroHasta")
    protected int caeNroHasta;
    @XmlElement(name = "CAEVigencia", required = true, nillable = true)
    @XmlSchemaType(name = "date")
    protected GregorianCalendar caeVigencia;
    @XmlElement(name = "CAEVencimiento", required = true, nillable = true)
    @XmlSchemaType(name = "date")
    protected GregorianCalendar caeVencimiento;
    @XmlElement(name = "CAEAnulado")
    protected boolean caeAnulado;
    @XmlElement(name = "CAEFchHora", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected GregorianCalendar caeFchHora;
    @XmlElement(name = "CAECAusalTipo")
    protected short caecAusalTipo;
    @XmlElement(name = "CAEEspecial")
    protected byte caeEspecial;

    /**
     * Obtiene el valor de la propiedad empId.
     * 
     */
    public int getEmpId() {
        return empId;
    }

    /**
     * Define el valor de la propiedad empId.
     * 
     */
    public void setEmpId(int value) {
        this.empId = value;
    }

    /**
     * Obtiene el valor de la propiedad caeId.
     * 
     */
    public int getCAEId() {
        return caeId;
    }

    /**
     * Define el valor de la propiedad caeId.
     * 
     */
    public void setCAEId(int value) {
        this.caeId = value;
    }

    /**
     * Obtiene el valor de la propiedad caeNroAutorizacion.
     * 
     */
    public long getCAENroAutorizacion() {
        return caeNroAutorizacion;
    }

    /**
     * Define el valor de la propiedad caeNroAutorizacion.
     * 
     */
    public void setCAENroAutorizacion(long value) {
        this.caeNroAutorizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad caeTipoCFE.
     * 
     */
    public short getCAETipoCFE() {
        return caeTipoCFE;
    }

    /**
     * Define el valor de la propiedad caeTipoCFE.
     * 
     */
    public void setCAETipoCFE(short value) {
        this.caeTipoCFE = value;
    }

    /**
     * Obtiene el valor de la propiedad caeSerie.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCAESerie() {
        return caeSerie;
    }

    /**
     * Define el valor de la propiedad caeSerie.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCAESerie(String value) {
        this.caeSerie = value;
    }

    /**
     * Obtiene el valor de la propiedad caeNroDesde.
     * 
     */
    public int getCAENroDesde() {
        return caeNroDesde;
    }

    /**
     * Define el valor de la propiedad caeNroDesde.
     * 
     */
    public void setCAENroDesde(int value) {
        this.caeNroDesde = value;
    }

    /**
     * Obtiene el valor de la propiedad caeNroHasta.
     * 
     */
    public int getCAENroHasta() {
        return caeNroHasta;
    }

    /**
     * Define el valor de la propiedad caeNroHasta.
     * 
     */
    public void setCAENroHasta(int value) {
        this.caeNroHasta = value;
    }

    /**
     * Obtiene el valor de la propiedad caeVigencia.
     * 
     * @return
     *     possible object is
     *     {@link GregorianCalendar }
     *     
     */
    public GregorianCalendar getCAEVigencia() {
        return caeVigencia;
    }

    /**
     * Define el valor de la propiedad caeVigencia.
     * 
     * @param value
     *     allowed object is
     *     {@link GregorianCalendar }
     *     
     */
    public void setCAEVigencia(GregorianCalendar value) {
        this.caeVigencia = value;
    }

    /**
     * Obtiene el valor de la propiedad caeVencimiento.
     * 
     * @return
     *     possible object is
     *     {@link GregorianCalendar }
     *     
     */
    public GregorianCalendar getCAEVencimiento() {
        return caeVencimiento;
    }

    /**
     * Define el valor de la propiedad caeVencimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link GregorianCalendar }
     *     
     */
    public void setCAEVencimiento(GregorianCalendar value) {
        this.caeVencimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad caeAnulado.
     * 
     */
    public boolean isCAEAnulado() {
        return caeAnulado;
    }

    /**
     * Define el valor de la propiedad caeAnulado.
     * 
     */
    public void setCAEAnulado(boolean value) {
        this.caeAnulado = value;
    }

    /**
     * Obtiene el valor de la propiedad caeFchHora.
     * 
     * @return
     *     possible object is
     *     {@link GregorianCalendar }
     *     
     */
    public GregorianCalendar getCAEFchHora() {
        return caeFchHora;
    }

    /**
     * Define el valor de la propiedad caeFchHora.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCAEFchHora(GregorianCalendar value) {
        this.caeFchHora = value;
    }

    /**
     * Obtiene el valor de la propiedad caecAusalTipo.
     * 
     */
    public short getCAECAusalTipo() {
        return caecAusalTipo;
    }

    /**
     * Define el valor de la propiedad caecAusalTipo.
     * 
     */
    public void setCAECAusalTipo(short value) {
        this.caecAusalTipo = value;
    }

    /**
     * Obtiene el valor de la propiedad caeEspecial.
     * 
     */
    public byte getCAEEspecial() {
        return caeEspecial;
    }

    /**
     * Define el valor de la propiedad caeEspecial.
     * 
     */
    public void setCAEEspecial(byte value) {
        this.caeEspecial = value;
    }

}
