
package Negocio.WSInterface.ReservarNroCAE;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Pcaenroautorizacion" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="Pcaeserie" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Pcaenroreservado" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Perrorreturn" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Perrormessage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pcaenroautorizacion",
    "pcaeserie",
    "pcaenroreservado",
    "perrorreturn",
    "perrormessage"
})
@XmlRootElement(name = "WSReservarNroCAE_0203.ExecuteResponse")
public class WSReservarNroCAE0203ExecuteResponse {

    @XmlElement(name = "Pcaenroautorizacion")
    protected long pcaenroautorizacion;
    @XmlElement(name = "Pcaeserie", required = true)
    protected String pcaeserie;
    @XmlElement(name = "Pcaenroreservado")
    protected int pcaenroreservado;
    @XmlElement(name = "Perrorreturn")
    protected boolean perrorreturn;
    @XmlElement(name = "Perrormessage", required = true)
    protected String perrormessage;

    /**
     * Obtiene el valor de la propiedad pcaenroautorizacion.
     * 
     */
    public long getPcaenroautorizacion() {
        return pcaenroautorizacion;
    }

    /**
     * Define el valor de la propiedad pcaenroautorizacion.
     * 
     */
    public void setPcaenroautorizacion(long value) {
        this.pcaenroautorizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad pcaeserie.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPcaeserie() {
        return pcaeserie;
    }

    /**
     * Define el valor de la propiedad pcaeserie.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPcaeserie(String value) {
        this.pcaeserie = value;
    }

    /**
     * Obtiene el valor de la propiedad pcaenroreservado.
     * 
     */
    public int getPcaenroreservado() {
        return pcaenroreservado;
    }

    /**
     * Define el valor de la propiedad pcaenroreservado.
     * 
     */
    public void setPcaenroreservado(int value) {
        this.pcaenroreservado = value;
    }

    /**
     * Obtiene el valor de la propiedad perrorreturn.
     * 
     */
    public boolean isPerrorreturn() {
        return perrorreturn;
    }

    /**
     * Define el valor de la propiedad perrorreturn.
     * 
     */
    public void setPerrorreturn(boolean value) {
        this.perrorreturn = value;
    }

    /**
     * Obtiene el valor de la propiedad perrormessage.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerrormessage() {
        return perrormessage;
    }

    /**
     * Define el valor de la propiedad perrormessage.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerrormessage(String value) {
        this.perrormessage = value;
    }

}
