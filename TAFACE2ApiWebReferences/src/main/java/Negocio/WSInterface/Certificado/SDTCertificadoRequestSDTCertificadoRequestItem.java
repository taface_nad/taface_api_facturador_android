
package Negocio.WSInterface.Certificado;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SDTCertificadoRequest.SDTCertificadoRequestItem complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SDTCertificadoRequest.SDTCertificadoRequestItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EmpId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CertId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CertRowGUID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDTCertificadoRequest.SDTCertificadoRequestItem", propOrder = {
    "empId",
    "certId",
    "certRowGUID"
})
public class SDTCertificadoRequestSDTCertificadoRequestItem {

    @XmlElement(name = "EmpId")
    protected int empId;
    @XmlElement(name = "CertId")
    protected int certId;
    @XmlElement(name = "CertRowGUID", required = true)
    protected String certRowGUID;

    /**
     * Obtiene el valor de la propiedad empId.
     * 
     */
    public int getEmpId() {
        return empId;
    }

    /**
     * Define el valor de la propiedad empId.
     * 
     */
    public void setEmpId(int value) {
        this.empId = value;
    }

    /**
     * Obtiene el valor de la propiedad certId.
     * 
     */
    public int getCertId() {
        return certId;
    }

    /**
     * Define el valor de la propiedad certId.
     * 
     */
    public void setCertId(int value) {
        this.certId = value;
    }

    /**
     * Obtiene el valor de la propiedad certRowGUID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertRowGUID() {
        return certRowGUID;
    }

    /**
     * Define el valor de la propiedad certRowGUID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertRowGUID(String value) {
        this.certRowGUID = value;
    }

}
