
package Negocio.WSInterface.Certificado;

import java.util.GregorianCalendar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para SDTCertificadoResponse.SDTCertificadoResponseItem complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SDTCertificadoResponse.SDTCertificadoResponseItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EmpId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CertId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CertVigencia" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="CertVencimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="CertClaveEncriptacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CertArchivo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CertFchHora" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="CertRowGUID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CertEliminado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDTCertificadoResponse.SDTCertificadoResponseItem", propOrder = {
    "empId",
    "certId",
    "certVigencia",
    "certVencimiento",
    "certClaveEncriptacion",
    "certArchivo",
    "certFchHora",
    "certRowGUID",
    "certEliminado"
})
public class SDTCertificadoResponseSDTCertificadoResponseItem {

    @XmlElement(name = "EmpId")
    protected int empId;
    @XmlElement(name = "CertId")
    protected int certId;
    @XmlElement(name = "CertVigencia", required = true, nillable = true)
    @XmlSchemaType(name = "date")
    protected GregorianCalendar certVigencia;
    @XmlElement(name = "CertVencimiento", required = true, nillable = true)
    @XmlSchemaType(name = "date")
    protected GregorianCalendar certVencimiento;
    @XmlElement(name = "CertClaveEncriptacion", required = true)
    protected String certClaveEncriptacion;
    @XmlElement(name = "CertArchivo", required = true)
    protected String certArchivo;
    @XmlElement(name = "CertFchHora", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected GregorianCalendar certFchHora;
    @XmlElement(name = "CertRowGUID", required = true)
    protected String certRowGUID;
    @XmlElement(name = "CertEliminado")
    protected boolean certEliminado;

    /**
     * Obtiene el valor de la propiedad empId.
     * 
     */
    public int getEmpId() {
        return empId;
    }

    /**
     * Define el valor de la propiedad empId.
     * 
     */
    public void setEmpId(int value) {
        this.empId = value;
    }

    /**
     * Obtiene el valor de la propiedad certId.
     * 
     */
    public int getCertId() {
        return certId;
    }

    /**
     * Define el valor de la propiedad certId.
     * 
     */
    public void setCertId(int value) {
        this.certId = value;
    }

    /**
     * Obtiene el valor de la propiedad certVigencia.
     * 
     * @return
     *     possible object is
     *     {@link GregorianCalendar }
     *     
     */
    public GregorianCalendar getCertVigencia() {
        return certVigencia;
    }

    /**
     * Define el valor de la propiedad certVigencia.
     * 
     * @param value
     *     allowed object is
     *     {@link GregorianCalendar }
     *     
     */
    public void setCertVigencia(GregorianCalendar value) {
        this.certVigencia = value;
    }

    /**
     * Obtiene el valor de la propiedad certVencimiento.
     * 
     * @return
     *     possible object is
     *     {@link GregorianCalendar }
     *     
     */
    public GregorianCalendar getCertVencimiento() {
        return certVencimiento;
    }

    /**
     * Define el valor de la propiedad certVencimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link GregorianCalendar }
     *     
     */
    public void setCertVencimiento(GregorianCalendar value) {
        this.certVencimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad certClaveEncriptacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertClaveEncriptacion() {
        return certClaveEncriptacion;
    }

    /**
     * Define el valor de la propiedad certClaveEncriptacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertClaveEncriptacion(String value) {
        this.certClaveEncriptacion = value;
    }

    /**
     * Obtiene el valor de la propiedad certArchivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertArchivo() {
        return certArchivo;
    }

    /**
     * Define el valor de la propiedad certArchivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertArchivo(String value) {
        this.certArchivo = value;
    }

    /**
     * Obtiene el valor de la propiedad certFchHora.
     * 
     * @return
     *     possible object is
     *     {@link GregorianCalendar }
     *     
     */
    public GregorianCalendar getCertFchHora() {
        return certFchHora;
    }

    /**
     * Define el valor de la propiedad certFchHora.
     * 
     * @param value
     *     allowed object is
     *     {@link GregorianCalendar }
     *     
     */
    public void setCertFchHora(GregorianCalendar value) {
        this.certFchHora = value;
    }

    /**
     * Obtiene el valor de la propiedad certRowGUID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertRowGUID() {
        return certRowGUID;
    }

    /**
     * Define el valor de la propiedad certRowGUID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertRowGUID(String value) {
        this.certRowGUID = value;
    }

    /**
     * Obtiene el valor de la propiedad certEliminado.
     * 
     */
    public boolean isCertEliminado() {
        return certEliminado;
    }

    /**
     * Define el valor de la propiedad certEliminado.
     * 
     */
    public void setCertEliminado(boolean value) {
        this.certEliminado = value;
    }

}
