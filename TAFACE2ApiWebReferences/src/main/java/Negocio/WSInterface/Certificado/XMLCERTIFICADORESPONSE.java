
package Negocio.WSInterface.Certificado;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import Negocio.WSInterface.Certificado.SDTCertificadoResponseSDTCertificadoResponseItem;


/**
 * <p>Clase Java para XMLCERTIFICADORESPONSE complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="XMLCERTIFICADORESPONSE">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDTCertificadoResponse.SDTCertificadoResponseItem" type="{TAFACE}SDTCertificadoResponse.SDTCertificadoResponseItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XMLCERTIFICADORESPONSE", propOrder = {
    "sdtCertificadoResponseSDTCertificadoResponseItem"
})
public class XMLCERTIFICADORESPONSE {

    @XmlElement(name = "SDTCertificadoResponse.SDTCertificadoResponseItem")
    protected List<SDTCertificadoResponseSDTCertificadoResponseItem> sdtCertificadoResponseSDTCertificadoResponseItem;

    /**
     * Gets the value of the sdtCertificadoResponseSDTCertificadoResponseItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdtCertificadoResponseSDTCertificadoResponseItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDTCertificadoResponseSDTCertificadoResponseItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDTCertificadoResponseSDTCertificadoResponseItem }
     * 
     * 
     */
    public List<SDTCertificadoResponseSDTCertificadoResponseItem> getSDTCertificadoResponseSDTCertificadoResponseItem() {
        if (sdtCertificadoResponseSDTCertificadoResponseItem == null) {
            sdtCertificadoResponseSDTCertificadoResponseItem = new ArrayList<SDTCertificadoResponseSDTCertificadoResponseItem>();
        }
        return this.sdtCertificadoResponseSDTCertificadoResponseItem;
    }

    public void setSDTCertificadoResponseSDTCertificadoResponseItem(List<SDTCertificadoResponseSDTCertificadoResponseItem> value) {
        this.sdtCertificadoResponseSDTCertificadoResponseItem = value;
    }

}
