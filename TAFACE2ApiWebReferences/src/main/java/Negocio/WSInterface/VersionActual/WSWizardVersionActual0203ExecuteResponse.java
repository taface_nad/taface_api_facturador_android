
package Negocio.WSInterface.VersionActual;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Pversion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pversion"
})
@XmlRootElement(name = "WSWizardVersionActual_0203.ExecuteResponse")
public class WSWizardVersionActual0203ExecuteResponse {

    @XmlElement(name = "Pversion", required = true)
    protected String pversion;

    /**
     * Obtiene el valor de la propiedad pversion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPversion() {
        return pversion;
    }

    /**
     * Define el valor de la propiedad pversion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPversion(String value) {
        this.pversion = value;
    }

}
