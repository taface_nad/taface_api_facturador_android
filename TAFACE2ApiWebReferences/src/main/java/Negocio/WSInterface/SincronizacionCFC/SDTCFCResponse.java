
package Negocio.WSInterface.SincronizacionCFC;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SDTCFCResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SDTCFCResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDTCFCResponse.SDTCFCResponseItem" type="{TAFACE}SDTCFCResponse.SDTCFCResponseItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDTCFCResponse", propOrder = {
    "sdtcfcResponseSDTCFCResponseItem"
})
public class SDTCFCResponse {

    @XmlElement(name = "SDTCFCResponse.SDTCFCResponseItem")
    protected List<SDTCFCResponseSDTCFCResponseItem> sdtcfcResponseSDTCFCResponseItem;

    /**
     * Gets the value of the sdtcfcResponseSDTCFCResponseItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdtcfcResponseSDTCFCResponseItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDTCFCResponseSDTCFCResponseItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDTCFCResponseSDTCFCResponseItem }
     * 
     * 
     */
    public List<SDTCFCResponseSDTCFCResponseItem> getSDTCFCResponseSDTCFCResponseItem() {
        if (sdtcfcResponseSDTCFCResponseItem == null) {
            sdtcfcResponseSDTCFCResponseItem = new ArrayList<SDTCFCResponseSDTCFCResponseItem>();
        }
        return this.sdtcfcResponseSDTCFCResponseItem;
    }

    public void setSDTCFCResponseSDTCFCResponseItem(List<SDTCFCResponseSDTCFCResponseItem> value) {
        this.sdtcfcResponseSDTCFCResponseItem = value;
    }

}
