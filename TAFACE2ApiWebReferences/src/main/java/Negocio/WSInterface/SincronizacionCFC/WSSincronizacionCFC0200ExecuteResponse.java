
package Negocio.WSInterface.SincronizacionCFC;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Psdtcfcresponse" type="{TAFACE}SDTCFCResponse"/>
 *         &lt;element name="Perrorreturn" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Perrormessage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "psdtcfcresponse",
    "perrorreturn",
    "perrormessage"
})
@XmlRootElement(name = "WSSincronizacionCFC_0200.ExecuteResponse")
public class WSSincronizacionCFC0200ExecuteResponse {

    @XmlElement(name = "Psdtcfcresponse", required = true)
    protected SDTCFCResponse psdtcfcresponse;
    @XmlElement(name = "Perrorreturn")
    protected boolean perrorreturn;
    @XmlElement(name = "Perrormessage", required = true)
    protected String perrormessage;

    /**
     * Obtiene el valor de la propiedad psdtcfcresponse.
     * 
     * @return
     *     possible object is
     *     {@link SDTCFCResponse }
     *     
     */
    public SDTCFCResponse getPsdtcfcresponse() {
        return psdtcfcresponse;
    }

    /**
     * Define el valor de la propiedad psdtcfcresponse.
     * 
     * @param value
     *     allowed object is
     *     {@link SDTCFCResponse }
     *     
     */
    public void setPsdtcfcresponse(SDTCFCResponse value) {
        this.psdtcfcresponse = value;
    }

    /**
     * Obtiene el valor de la propiedad perrorreturn.
     * 
     */
    public boolean isPerrorreturn() {
        return perrorreturn;
    }

    /**
     * Define el valor de la propiedad perrorreturn.
     * 
     */
    public void setPerrorreturn(boolean value) {
        this.perrorreturn = value;
    }

    /**
     * Obtiene el valor de la propiedad perrormessage.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerrormessage() {
        return perrormessage;
    }

    /**
     * Define el valor de la propiedad perrormessage.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerrormessage(String value) {
        this.perrormessage = value;
    }

}
