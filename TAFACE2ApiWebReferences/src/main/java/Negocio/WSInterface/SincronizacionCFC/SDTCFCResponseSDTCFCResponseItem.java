
package Negocio.WSInterface.SincronizacionCFC;

import java.util.GregorianCalendar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para SDTCFCResponse.SDTCFCResponseItem complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SDTCFCResponse.SDTCFCResponseItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EmpId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CFCId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CFCNroAutorizacion" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="CFCTipoCFE" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="SucId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CFCSerie" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CFCNroDesde" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CFCNroHasta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CFCVencimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="CFCFchHora" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="CFCRowGUID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CFCEliminado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDTCFCResponse.SDTCFCResponseItem", propOrder = {
    "empId",
    "cfcId",
    "cfcNroAutorizacion",
    "cfcTipoCFE",
    "sucId",
    "cfcSerie",
    "cfcNroDesde",
    "cfcNroHasta",
    "cfcVencimiento",
    "cfcFchHora",
    "cfcRowGUID",
    "cfcEliminado"
})
public class SDTCFCResponseSDTCFCResponseItem {

    @XmlElement(name = "EmpId")
    protected int empId;
    @XmlElement(name = "CFCId")
    protected int cfcId;
    @XmlElement(name = "CFCNroAutorizacion")
    protected long cfcNroAutorizacion;
    @XmlElement(name = "CFCTipoCFE")
    protected short cfcTipoCFE;
    @XmlElement(name = "SucId")
    protected int sucId;
    @XmlElement(name = "CFCSerie", required = true)
    protected String cfcSerie;
    @XmlElement(name = "CFCNroDesde")
    protected int cfcNroDesde;
    @XmlElement(name = "CFCNroHasta")
    protected int cfcNroHasta;
    @XmlElement(name = "CFCVencimiento", required = true, nillable = true)
    @XmlSchemaType(name = "date")
    protected GregorianCalendar cfcVencimiento;
    @XmlElement(name = "CFCFchHora", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected GregorianCalendar cfcFchHora;
    @XmlElement(name = "CFCRowGUID", required = true)
    protected String cfcRowGUID;
    @XmlElement(name = "CFCEliminado")
    protected boolean cfcEliminado;

    /**
     * Obtiene el valor de la propiedad empId.
     * 
     */
    public int getEmpId() {
        return empId;
    }

    /**
     * Define el valor de la propiedad empId.
     * 
     */
    public void setEmpId(int value) {
        this.empId = value;
    }

    /**
     * Obtiene el valor de la propiedad cfcId.
     * 
     */
    public int getCFCId() {
        return cfcId;
    }

    /**
     * Define el valor de la propiedad cfcId.
     * 
     */
    public void setCFCId(int value) {
        this.cfcId = value;
    }

    /**
     * Obtiene el valor de la propiedad cfcNroAutorizacion.
     * 
     */
    public long getCFCNroAutorizacion() {
        return cfcNroAutorizacion;
    }

    /**
     * Define el valor de la propiedad cfcNroAutorizacion.
     * 
     */
    public void setCFCNroAutorizacion(long value) {
        this.cfcNroAutorizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad cfcTipoCFE.
     * 
     */
    public short getCFCTipoCFE() {
        return cfcTipoCFE;
    }

    /**
     * Define el valor de la propiedad cfcTipoCFE.
     * 
     */
    public void setCFCTipoCFE(short value) {
        this.cfcTipoCFE = value;
    }

    /**
     * Obtiene el valor de la propiedad sucId.
     * 
     */
    public int getSucId() {
        return sucId;
    }

    /**
     * Define el valor de la propiedad sucId.
     * 
     */
    public void setSucId(int value) {
        this.sucId = value;
    }

    /**
     * Obtiene el valor de la propiedad cfcSerie.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCFCSerie() {
        return cfcSerie;
    }

    /**
     * Define el valor de la propiedad cfcSerie.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCFCSerie(String value) {
        this.cfcSerie = value;
    }

    /**
     * Obtiene el valor de la propiedad cfcNroDesde.
     * 
     */
    public int getCFCNroDesde() {
        return cfcNroDesde;
    }

    /**
     * Define el valor de la propiedad cfcNroDesde.
     * 
     */
    public void setCFCNroDesde(int value) {
        this.cfcNroDesde = value;
    }

    /**
     * Obtiene el valor de la propiedad cfcNroHasta.
     * 
     */
    public int getCFCNroHasta() {
        return cfcNroHasta;
    }

    /**
     * Define el valor de la propiedad cfcNroHasta.
     * 
     */
    public void setCFCNroHasta(int value) {
        this.cfcNroHasta = value;
    }

    /**
     * Obtiene el valor de la propiedad cfcVencimiento.
     * 
     * @return
     *     possible object is
     *     {@link GregorianCalendar }
     *     
     */
    public GregorianCalendar getCFCVencimiento() {
        return cfcVencimiento;
    }

    /**
     * Define el valor de la propiedad cfcVencimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link GregorianCalendar }
     *     
     */
    public void setCFCVencimiento(GregorianCalendar value) {
        this.cfcVencimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad cfcFchHora.
     * 
     * @return
     *     possible object is
     *     {@link GregorianCalendar }
     *     
     */
    public GregorianCalendar getCFCFchHora() {
        return cfcFchHora;
    }

    /**
     * Define el valor de la propiedad cfcFchHora.
     * 
     * @param value
     *     allowed object is
     *     {@link GregorianCalendar }
     *     
     */
    public void setCFCFchHora(GregorianCalendar value) {
        this.cfcFchHora = value;
    }

    /**
     * Obtiene el valor de la propiedad cfcRowGUID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCFCRowGUID() {
        return cfcRowGUID;
    }

    /**
     * Define el valor de la propiedad cfcRowGUID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCFCRowGUID(String value) {
        this.cfcRowGUID = value;
    }

    /**
     * Obtiene el valor de la propiedad cfcEliminado.
     * 
     */
    public boolean isCFCEliminado() {
        return cfcEliminado;
    }

    /**
     * Define el valor de la propiedad cfcEliminado.
     * 
     */
    public void setCFCEliminado(boolean value) {
        this.cfcEliminado = value;
    }

}
