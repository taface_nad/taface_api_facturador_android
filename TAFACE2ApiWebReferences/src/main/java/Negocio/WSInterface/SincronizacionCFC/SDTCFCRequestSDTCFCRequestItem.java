
package Negocio.WSInterface.SincronizacionCFC;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SDTCFCRequest.SDTCFCRequestItem complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SDTCFCRequest.SDTCFCRequestItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EmpId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CFCId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CFCRowGUID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDTCFCRequest.SDTCFCRequestItem", propOrder = {
    "empId",
    "cfcId",
    "cfcRowGUID"
})
public class SDTCFCRequestSDTCFCRequestItem {

    @XmlElement(name = "EmpId")
    protected int empId;
    @XmlElement(name = "CFCId")
    protected int cfcId;
    @XmlElement(name = "CFCRowGUID", required = true)
    protected String cfcRowGUID;

    /**
     * Obtiene el valor de la propiedad empId.
     * 
     */
    public int getEmpId() {
        return empId;
    }

    /**
     * Define el valor de la propiedad empId.
     * 
     */
    public void setEmpId(int value) {
        this.empId = value;
    }

    /**
     * Obtiene el valor de la propiedad cfcId.
     * 
     */
    public int getCFCId() {
        return cfcId;
    }

    /**
     * Define el valor de la propiedad cfcId.
     * 
     */
    public void setCFCId(int value) {
        this.cfcId = value;
    }

    /**
     * Obtiene el valor de la propiedad cfcRowGUID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCFCRowGUID() {
        return cfcRowGUID;
    }

    /**
     * Define el valor de la propiedad cfcRowGUID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCFCRowGUID(String value) {
        this.cfcRowGUID = value;
    }

}
