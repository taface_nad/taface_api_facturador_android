
package Negocio.WSInterface.SincronizacionCFC;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SDTCFCRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SDTCFCRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDTCFCRequest.SDTCFCRequestItem" type="{TAFACE}SDTCFCRequest.SDTCFCRequestItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDTCFCRequest", propOrder = {
    "sdtcfcRequestSDTCFCRequestItem"
})
public class SDTCFCRequest {

    @XmlElement(name = "SDTCFCRequest.SDTCFCRequestItem")
    protected List<SDTCFCRequestSDTCFCRequestItem> sdtcfcRequestSDTCFCRequestItem;

    /**
     * Gets the value of the sdtcfcRequestSDTCFCRequestItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdtcfcRequestSDTCFCRequestItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDTCFCRequestSDTCFCRequestItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDTCFCRequestSDTCFCRequestItem }
     * 
     * 
     */
    public List<SDTCFCRequestSDTCFCRequestItem> getSDTCFCRequestSDTCFCRequestItem() {
        if (sdtcfcRequestSDTCFCRequestItem == null) {
            sdtcfcRequestSDTCFCRequestItem = new ArrayList<SDTCFCRequestSDTCFCRequestItem>();
        }
        return this.sdtcfcRequestSDTCFCRequestItem;
    }

}
