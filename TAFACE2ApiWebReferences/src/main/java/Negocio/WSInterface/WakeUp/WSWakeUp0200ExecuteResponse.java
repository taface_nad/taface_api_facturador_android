
package Negocio.WSInterface.WakeUp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Pestado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pestado"
})
@XmlRootElement(name = "WSWakeUp_0200.ExecuteResponse")
public class WSWakeUp0200ExecuteResponse {

    @XmlElement(name = "Pestado")
    protected boolean pestado;

    /**
     * Obtiene el valor de la propiedad pestado.
     * 
     */
    public boolean isPestado() {
        return pestado;
    }

    /**
     * Define el valor de la propiedad pestado.
     * 
     */
    public void setPestado(boolean value) {
        this.pestado = value;
    }

}
