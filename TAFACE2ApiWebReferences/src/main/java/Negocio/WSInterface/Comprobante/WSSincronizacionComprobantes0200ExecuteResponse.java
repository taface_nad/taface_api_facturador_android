
package Negocio.WSInterface.Comprobante;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Pcteidnew" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Perrorreturn" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Perrormessage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pcteidnew",
    "perrorreturn",
    "perrormessage"
})
@XmlRootElement(name = "WSSincronizacionComprobantes_0200.ExecuteResponse")
public class WSSincronizacionComprobantes0200ExecuteResponse {

    @XmlElement(name = "Pcteidnew")
    protected int pcteidnew;
    @XmlElement(name = "Perrorreturn")
    protected boolean perrorreturn;
    @XmlElement(name = "Perrormessage", required = true)
    protected String perrormessage;

    /**
     * Obtiene el valor de la propiedad pcteidnew.
     * 
     */
    public int getPcteidnew() {
        return pcteidnew;
    }

    /**
     * Define el valor de la propiedad pcteidnew.
     * 
     */
    public void setPcteidnew(int value) {
        this.pcteidnew = value;
    }

    /**
     * Obtiene el valor de la propiedad perrorreturn.
     * 
     */
    public boolean isPerrorreturn() {
        return perrorreturn;
    }

    /**
     * Define el valor de la propiedad perrorreturn.
     * 
     */
    public void setPerrorreturn(boolean value) {
        this.perrorreturn = value;
    }

    /**
     * Obtiene el valor de la propiedad perrormessage.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerrormessage() {
        return perrormessage;
    }

    /**
     * Define el valor de la propiedad perrormessage.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerrormessage(String value) {
        this.perrormessage = value;
    }

}
