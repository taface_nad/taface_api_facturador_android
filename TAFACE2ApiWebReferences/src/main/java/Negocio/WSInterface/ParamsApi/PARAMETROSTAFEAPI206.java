
package Negocio.WSInterface.ParamsApi;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para PARAMETROSTAFEAPI complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PARAMETROSTAFEAPI">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ModoFirma" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="TipoProveedorSQL" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="CadenaCnxSQL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TopeUIPlaza" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TopeUIFreeShop" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ValidarCFC" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="TipoEncriptacion" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="LoteSincronizacion" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="ParamGUID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CAEsPorcAvance" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="CantDiasAntesCAEVencer" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="CantDiasAntesCertificadoVencer" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="MinDatosCajaSinSinc" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="EncriptarComplementoFiscal" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="MinutosCaducidadReservaCAE" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="LargoMinimoTicket" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="AperturaDeCajon" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="NoValidarFechaFirma" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SucModoSincronizacion" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PARAMETROSTAFEAPI", propOrder = {
    "modoFirma",
    "tipoProveedorSQL",
    "cadenaCnxSQL",
    "topeUIPlaza",
    "topeUIFreeShop",
    "validarCFC",
    "tipoEncriptacion",
    "loteSincronizacion",
    "paramGUID",
    "caEsPorcAvance",
    "cantDiasAntesCAEVencer",
    "cantDiasAntesCertificadoVencer",
    "minDatosCajaSinSinc",
    "encriptarComplementoFiscal",
    "minutosCaducidadReservaCAE",
    "largoMinimoTicket",
    "aperturaDeCajon",
    "noValidarFechaFirma",
    "sucModoSincronizacion"
})
public class PARAMETROSTAFEAPI206 {

    @XmlElement(name = "ModoFirma")
    protected short modoFirma;
    @XmlElement(name = "TipoProveedorSQL")
    protected short tipoProveedorSQL;
    @XmlElement(name = "CadenaCnxSQL", required = true)
    protected String cadenaCnxSQL;
    @XmlElement(name = "TopeUIPlaza")
    protected int topeUIPlaza;
    @XmlElement(name = "TopeUIFreeShop")
    protected int topeUIFreeShop;
    @XmlElement(name = "ValidarCFC")
    protected boolean validarCFC;
    @XmlElement(name = "TipoEncriptacion")
    protected short tipoEncriptacion;
    @XmlElement(name = "LoteSincronizacion")
    protected short loteSincronizacion;
    @XmlElement(name = "ParamGUID", required = true)
    protected String paramGUID;
    @XmlElement(name = "CAEsPorcAvance")
    protected byte caEsPorcAvance;
    @XmlElement(name = "CantDiasAntesCAEVencer")
    protected short cantDiasAntesCAEVencer;
    @XmlElement(name = "CantDiasAntesCertificadoVencer")
    protected short cantDiasAntesCertificadoVencer;
    @XmlElement(name = "MinDatosCajaSinSinc")
    protected short minDatosCajaSinSinc;
    @XmlElement(name = "EncriptarComplementoFiscal")
    protected boolean encriptarComplementoFiscal;
    @XmlElement(name = "MinutosCaducidadReservaCAE")
    protected int minutosCaducidadReservaCAE;
    @XmlElement(name = "LargoMinimoTicket")
    protected short largoMinimoTicket;
    @XmlElement(name = "AperturaDeCajon")
    protected boolean aperturaDeCajon;
    @XmlElement(name = "NoValidarFechaFirma")
    protected boolean noValidarFechaFirma;
    @XmlElement(name = "SucModoSincronizacion")
    protected byte sucModoSincronizacion;

    /**
     * Obtiene el valor de la propiedad modoFirma.
     * 
     */
    public short getModoFirma() {
        return modoFirma;
    }

    /**
     * Define el valor de la propiedad modoFirma.
     * 
     */
    public void setModoFirma(short value) {
        this.modoFirma = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoProveedorSQL.
     * 
     */
    public short getTipoProveedorSQL() {
        return tipoProveedorSQL;
    }

    /**
     * Define el valor de la propiedad tipoProveedorSQL.
     * 
     */
    public void setTipoProveedorSQL(short value) {
        this.tipoProveedorSQL = value;
    }

    /**
     * Obtiene el valor de la propiedad cadenaCnxSQL.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCadenaCnxSQL() {
        return cadenaCnxSQL;
    }

    /**
     * Define el valor de la propiedad cadenaCnxSQL.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCadenaCnxSQL(String value) {
        this.cadenaCnxSQL = value;
    }

    /**
     * Obtiene el valor de la propiedad topeUIPlaza.
     * 
     */
    public int getTopeUIPlaza() {
        return topeUIPlaza;
    }

    /**
     * Define el valor de la propiedad topeUIPlaza.
     * 
     */
    public void setTopeUIPlaza(int value) {
        this.topeUIPlaza = value;
    }

    /**
     * Obtiene el valor de la propiedad topeUIFreeShop.
     * 
     */
    public int getTopeUIFreeShop() {
        return topeUIFreeShop;
    }

    /**
     * Define el valor de la propiedad topeUIFreeShop.
     * 
     */
    public void setTopeUIFreeShop(int value) {
        this.topeUIFreeShop = value;
    }

    /**
     * Obtiene el valor de la propiedad validarCFC.
     * 
     */
    public boolean isValidarCFC() {
        return validarCFC;
    }

    /**
     * Define el valor de la propiedad validarCFC.
     * 
     */
    public void setValidarCFC(boolean value) {
        this.validarCFC = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoEncriptacion.
     * 
     */
    public short getTipoEncriptacion() {
        return tipoEncriptacion;
    }

    /**
     * Define el valor de la propiedad tipoEncriptacion.
     * 
     */
    public void setTipoEncriptacion(short value) {
        this.tipoEncriptacion = value;
    }

    /**
     * Obtiene el valor de la propiedad loteSincronizacion.
     * 
     */
    public short getLoteSincronizacion() {
        return loteSincronizacion;
    }

    /**
     * Define el valor de la propiedad loteSincronizacion.
     * 
     */
    public void setLoteSincronizacion(short value) {
        this.loteSincronizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad paramGUID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParamGUID() {
        return paramGUID;
    }

    /**
     * Define el valor de la propiedad paramGUID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParamGUID(String value) {
        this.paramGUID = value;
    }

    /**
     * Obtiene el valor de la propiedad caEsPorcAvance.
     * 
     */
    public byte getCAEsPorcAvance() {
        return caEsPorcAvance;
    }

    /**
     * Define el valor de la propiedad caEsPorcAvance.
     * 
     */
    public void setCAEsPorcAvance(byte value) {
        this.caEsPorcAvance = value;
    }

    /**
     * Obtiene el valor de la propiedad cantDiasAntesCAEVencer.
     * 
     */
    public short getCantDiasAntesCAEVencer() {
        return cantDiasAntesCAEVencer;
    }

    /**
     * Define el valor de la propiedad cantDiasAntesCAEVencer.
     * 
     */
    public void setCantDiasAntesCAEVencer(short value) {
        this.cantDiasAntesCAEVencer = value;
    }

    /**
     * Obtiene el valor de la propiedad cantDiasAntesCertificadoVencer.
     * 
     */
    public short getCantDiasAntesCertificadoVencer() {
        return cantDiasAntesCertificadoVencer;
    }

    /**
     * Define el valor de la propiedad cantDiasAntesCertificadoVencer.
     * 
     */
    public void setCantDiasAntesCertificadoVencer(short value) {
        this.cantDiasAntesCertificadoVencer = value;
    }

    /**
     * Obtiene el valor de la propiedad minDatosCajaSinSinc.
     * 
     */
    public short getMinDatosCajaSinSinc() {
        return minDatosCajaSinSinc;
    }

    /**
     * Define el valor de la propiedad minDatosCajaSinSinc.
     * 
     */
    public void setMinDatosCajaSinSinc(short value) {
        this.minDatosCajaSinSinc = value;
    }

    /**
     * Obtiene el valor de la propiedad encriptarComplementoFiscal.
     * 
     */
    public boolean isEncriptarComplementoFiscal() {
        return encriptarComplementoFiscal;
    }

    /**
     * Define el valor de la propiedad encriptarComplementoFiscal.
     * 
     */
    public void setEncriptarComplementoFiscal(boolean value) {
        this.encriptarComplementoFiscal = value;
    }

    /**
     * Obtiene el valor de la propiedad minutosCaducidadReservaCAE.
     * 
     */
    public int getMinutosCaducidadReservaCAE() {
        return minutosCaducidadReservaCAE;
    }

    /**
     * Define el valor de la propiedad minutosCaducidadReservaCAE.
     * 
     */
    public void setMinutosCaducidadReservaCAE(int value) {
        this.minutosCaducidadReservaCAE = value;
    }

    /**
     * Obtiene el valor de la propiedad largoMinimoTicket.
     * 
     */
    public short getLargoMinimoTicket() {
        return largoMinimoTicket;
    }

    /**
     * Define el valor de la propiedad largoMinimoTicket.
     * 
     */
    public void setLargoMinimoTicket(short value) {
        this.largoMinimoTicket = value;
    }

    /**
     * Obtiene el valor de la propiedad aperturaDeCajon.
     * 
     */
    public boolean isAperturaDeCajon() {
        return aperturaDeCajon;
    }

    /**
     * Define el valor de la propiedad aperturaDeCajon.
     * 
     */
    public void setAperturaDeCajon(boolean value) {
        this.aperturaDeCajon = value;
    }

    /**
     * Obtiene el valor de la propiedad noValidarFechaFirma.
     * 
     */
    public boolean isNoValidarFechaFirma() {
        return noValidarFechaFirma;
    }

    /**
     * Define el valor de la propiedad noValidarFechaFirma.
     * 
     */
    public void setNoValidarFechaFirma(boolean value) {
        this.noValidarFechaFirma = value;
    }

    /**
     * Obtiene el valor de la propiedad sucModoSincronizacion.
     * 
     */
    public byte getSucModoSincronizacion() {
        return sucModoSincronizacion;
    }

    /**
     * Define el valor de la propiedad sucModoSincronizacion.
     * 
     */
    public void setSucModoSincronizacion(byte value) {
        this.sucModoSincronizacion = value;
    }

}
