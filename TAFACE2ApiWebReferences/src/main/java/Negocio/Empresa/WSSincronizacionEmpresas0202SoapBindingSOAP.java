package Negocio.Empresa;




//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 5.5.1.3
//
// Created by Quasar Development 
//
//---------------------------------------------------




import org.ksoap2.HeaderProperty;
import org.ksoap2.serialization.*;

import java.util.ArrayList;
import java.util.List;


public class WSSincronizacionEmpresas0202SoapBindingSOAP
{
    interface VLJIWcfMethod
    {
        ExtendedSoapSerializationEnvelopeSOAP CreateSoapEnvelope() throws java.lang.Exception;

        java.lang.Object ProcessResult(ExtendedSoapSerializationEnvelopeSOAP __envelope, java.lang.Object result) throws java.lang.Exception;
    }

    String url="http://192.168.253.135:8088/TAFACEGatewaytest206/awssincronizacionempresas_0202.aspx";

    int timeOut=60000;
    public List< HeaderProperty> httpHeaders= new ArrayList< HeaderProperty>();
    public boolean enableLogging;

    public WSSincronizacionEmpresas0202SoapBindingSOAP(){}

    public WSSincronizacionEmpresas0202SoapBindingSOAP(String url)
    {
        this.url = url;
    }

    public WSSincronizacionEmpresas0202SoapBindingSOAP(String url, int timeOut)
    {
        this.url = url;
        this.timeOut=timeOut;
    }

    protected org.ksoap2.transport.Transport createTransport()
    {
        try
        {
            java.net.URI uri = new java.net.URI(url);
            if(uri.getScheme().equalsIgnoreCase("https"))
            {
                int port=uri.getPort()>0?uri.getPort():443;
                return new com.easywsdl.exksoap2.transport.AdvancedHttpsTransportSE(uri.getHost(), port, uri.getPath(), timeOut);
            }
            else
            {
                return new com.easywsdl.exksoap2.transport.AdvancedHttpTransportSE(url,timeOut);
            }

        }
        catch (java.net.URISyntaxException e)
        {
        }
        return null;
    }
    
    protected ExtendedSoapSerializationEnvelopeSOAP createEnvelope()
    {
        ExtendedSoapSerializationEnvelopeSOAP envelope= new ExtendedSoapSerializationEnvelopeSOAP(ExtendedSoapSerializationEnvelopeSOAP.VER11);
            return envelope;
    }
    
    protected java.util.List sendRequest(String methodName, ExtendedSoapSerializationEnvelopeSOAP envelope, org.ksoap2.transport.Transport transport , com.easywsdl.exksoap2.ws_specifications.profile.WS_Profile profile )throws java.lang.Exception
    {
        if(transport instanceof com.easywsdl.exksoap2.transport.AdvancedHttpTransportSE )
        {
            return ((com.easywsdl.exksoap2.transport.AdvancedHttpTransportSE)transport).call(methodName, envelope,httpHeaders,null,profile);
        }
        else
        {
            return ((com.easywsdl.exksoap2.transport.AdvancedHttpsTransportSE)transport).call(methodName, envelope,httpHeaders,null,profile);
        }  
    }

    java.lang.Object getResult(java.lang.Class destObj,java.lang.Object source,String resultName,ExtendedSoapSerializationEnvelopeSOAP __envelope) throws java.lang.Exception
    {
        if(source==null)
        {
            return null;
        }
        if(source instanceof SoapPrimitive)
        {
            SoapPrimitive soap =(SoapPrimitive)source;
            if(soap.getName().equals(resultName))
            {
                java.lang.Object instance=__envelope.get(source,destObj,false);
                return instance;
            }
        }
        else
        {
            SoapObject soap = (SoapObject)source;
            if (soap.hasProperty(resultName))
            {
                java.lang.Object j=soap.getProperty(resultName);
                if(j==null)
                {
                    return null;
                }
                java.lang.Object instance=__envelope.get(j,destObj,false);
                return instance;
            }
            else if( soap.getName().equals(resultName)) {
                java.lang.Object instance=__envelope.get(source,destObj,false);
                return instance;
            }
       }

       return null;
    }

        
    public WSSincronizacionEmpresas_0202ExecuteResponseSOAP Execute(final String Pempruc, final String Pemprowguid, final Integer Psucid, final Integer Pcajaid ) throws java.lang.Exception
    {
        com.easywsdl.exksoap2.ws_specifications.profile.WS_Profile __profile = new com.easywsdl.exksoap2.ws_specifications.profile.WS_Profile();
        return (WSSincronizacionEmpresas_0202ExecuteResponseSOAP)execute(new VLJIWcfMethod()
        {
            @Override
            public ExtendedSoapSerializationEnvelopeSOAP CreateSoapEnvelope(){
              ExtendedSoapSerializationEnvelopeSOAP __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("TAFACE", "WSSincronizacionEmpresas_0202.Execute");
                __envelope.setOutputSoapObject(__soapReq);
                
                PropertyInfo __info=null;
                __info = new PropertyInfo();
                __info.namespace="TAFACE";
                __info.name="Pempruc";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(Pempruc);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="TAFACE";
                __info.name="Pemprowguid";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(Pemprowguid);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="TAFACE";
                __info.name="Psucid";
                __info.type=PropertyInfo.INTEGER_CLASS;
                __info.setValue(Psucid);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="TAFACE";
                __info.name="Pcajaid";
                __info.type=PropertyInfo.INTEGER_CLASS;
                __info.setValue(Pcajaid);
                __soapReq.addProperty(__info);
                return __envelope;
            }
            
            @Override
            public java.lang.Object ProcessResult(ExtendedSoapSerializationEnvelopeSOAP __envelope, java.lang.Object __result)throws java.lang.Exception {
                return (WSSincronizacionEmpresas_0202ExecuteResponseSOAP)getResult(WSSincronizacionEmpresas_0202ExecuteResponseSOAP.class,__result,"WSSincronizacionEmpresas_0202.ExecuteResponse",__envelope);
            }
        },"TAFACEaction/AWSSINCRONIZACIONEMPRESAS_0202.Execute",__profile);
    }

    
    protected java.lang.Object execute(VLJIWcfMethod wcfMethod,String methodName,com.easywsdl.exksoap2.ws_specifications.profile.WS_Profile profile) throws java.lang.Exception
    {
        org.ksoap2.transport.Transport __httpTransport=createTransport();
        __httpTransport.debug=enableLogging;
        ExtendedSoapSerializationEnvelopeSOAP __envelope=wcfMethod.CreateSoapEnvelope();
        try
        {
            sendRequest(methodName, __envelope, __httpTransport,profile);
            
        }
        finally {
            if (__httpTransport.debug) {
                if (__httpTransport.requestDump != null) {
                    System.out.println("requestDump: "+__httpTransport.requestDump);
                    
                }
                if (__httpTransport.responseDump != null) {
                    System.out.println("responseDump: "+__httpTransport.responseDump);
                }
            }
        }
        java.lang.Object __retObj = __envelope.bodyIn;
        if (__retObj instanceof org.ksoap2.SoapFault){
            org.ksoap2.SoapFault __fault = (org.ksoap2.SoapFault)__retObj;
            throw convertToException(__fault,__envelope);
        }else{
            return wcfMethod.ProcessResult(__envelope,__retObj);
        }
    }
    
        
    protected java.lang.Exception convertToException(org.ksoap2.SoapFault fault,ExtendedSoapSerializationEnvelopeSOAP envelope)
    {
        org.ksoap2.SoapFault newException = fault;

        return newException;
    }
}


