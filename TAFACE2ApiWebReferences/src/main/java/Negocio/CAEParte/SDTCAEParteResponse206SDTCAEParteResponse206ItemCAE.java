package Negocio.CAEParte;

import org.ksoap2.serialization.AttributeContainer;
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

import java.util.Hashtable;

public class SDTCAEParteResponse206SDTCAEParteResponse206ItemCAE extends AttributeContainer implements KvmSerializable
{

    
    public Integer EmpId=0;
    
    public Integer CAEId=0;
    
    public Long CAENroAutorizacion=0L;
    
    public Integer CAETipoCFE=0;
    
    public String CAESerie;
    
    public Integer CAENroDesde=0;
    
    public Integer CAENroHasta=0;
    
    public java.util.Date CAEVigencia;
    
    public java.util.Date CAEVencimiento;
    
    public Boolean CAEAnulado=false;
    
    public java.util.Date CAEFchHora;
    private transient java.lang.Object __source;    

    
    public void loadFromSoap(java.lang.Object paramObj,ExtendedSoapSerializationEnvelope __envelope)
    {
        if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;
        __source=inObj; 
        if(inObj instanceof SoapObject)
        {
            SoapObject soapObject=(SoapObject)inObj;
            int size = soapObject.getPropertyCount();
            for (int i0=0;i0< size;i0++)
            {
                PropertyInfo info=soapObject.getPropertyInfo(i0);
                if(!loadProperty(info,soapObject,__envelope))
                {
                }
            } 
        }

    }

    protected boolean loadProperty(PropertyInfo info,SoapObject soapObject,ExtendedSoapSerializationEnvelope __envelope)
    {
        java.lang.Object obj = info.getValue();
        if (info.name.equals("EmpId"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.EmpId = Integer.parseInt(j.toString());
                    }
                }
                else if (obj instanceof Integer){
                    this.EmpId = (Integer)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CAEId"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CAEId = Integer.parseInt(j.toString());
                    }
                }
                else if (obj instanceof Integer){
                    this.CAEId = (Integer)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CAENroAutorizacion"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CAENroAutorizacion = new Long(j.toString());
                    }
                }
                else if (obj instanceof Long){
                    this.CAENroAutorizacion = (Long)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CAETipoCFE"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CAETipoCFE = Integer.parseInt(j.toString());
                    }
                }
                else if (obj instanceof Integer){
                    this.CAETipoCFE = (Integer)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CAESerie"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CAESerie = j.toString();
                    }
                }
                else if (obj instanceof String){
                    this.CAESerie = (String)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CAENroDesde"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CAENroDesde = Integer.parseInt(j.toString());
                    }
                }
                else if (obj instanceof Integer){
                    this.CAENroDesde = (Integer)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CAENroHasta"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CAENroHasta = Integer.parseInt(j.toString());
                    }
                }
                else if (obj instanceof Integer){
                    this.CAENroHasta = (Integer)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CAEVigencia"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CAEVigencia = Helper.ConvertFromWebService(j.toString());
                    }
                }
                else if (obj instanceof java.util.Date){
                    this.CAEVigencia = (java.util.Date)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CAEVencimiento"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CAEVencimiento = Helper.ConvertFromWebService(j.toString());
                    }
                }
                else if (obj instanceof java.util.Date){
                    this.CAEVencimiento = (java.util.Date)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CAEAnulado"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CAEAnulado = new Boolean(j.toString());
                    }
                }
                else if (obj instanceof Boolean){
                    this.CAEAnulado = (Boolean)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CAEFchHora"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CAEFchHora = Helper.ConvertFromWebService(j.toString());
                    }
                }
                else if (obj instanceof java.util.Date){
                    this.CAEFchHora = (java.util.Date)obj;
                }
            }
            return true;
        }
        return false;
    }
    
    public java.lang.Object getOriginalXmlSource()
    {
        return __source;
    }    
    

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return EmpId;
        }
        if(propertyIndex==1)
        {
            return CAEId;
        }
        if(propertyIndex==2)
        {
            return CAENroAutorizacion;
        }
        if(propertyIndex==3)
        {
            return CAETipoCFE;
        }
        if(propertyIndex==4)
        {
            return CAESerie;
        }
        if(propertyIndex==5)
        {
            return CAENroDesde;
        }
        if(propertyIndex==6)
        {
            return CAENroHasta;
        }
        if(propertyIndex==7)
        {
            return this.CAEVigencia!=null?Helper.getDateFormat().format(this.CAEVigencia):SoapPrimitive.NullNilElement;
        }
        if(propertyIndex==8)
        {
            return this.CAEVencimiento!=null?Helper.getDateFormat().format(this.CAEVencimiento):SoapPrimitive.NullNilElement;
        }
        if(propertyIndex==9)
        {
            return CAEAnulado;
        }
        if(propertyIndex==10)
        {
            return this.CAEFchHora!=null?Helper.getDateTimeFormat().format(this.CAEFchHora):SoapPrimitive.NullNilElement;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 11;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "EmpId";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "CAEId";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.LONG_CLASS;
            info.name = "CAENroAutorizacion";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==3)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "CAETipoCFE";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==4)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "CAESerie";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==5)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "CAENroDesde";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==6)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "CAENroHasta";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==7)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "CAEVigencia";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==8)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "CAEVencimiento";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==9)
        {
            info.type = PropertyInfo.BOOLEAN_CLASS;
            info.name = "CAEAnulado";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==10)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "CAEFchHora";
            info.namespace= "TAFACE";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    
}

