package Negocio.CAEParte;

import org.ksoap2.serialization.AttributeContainer;
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

import java.util.Hashtable;

public class SDTCAEParteResponse206SDTCAEParteResponse206Item extends AttributeContainer implements KvmSerializable
{

    
    public Integer EmpId=0;
    
    public Integer CAEId=0;
    
    public Integer CAEParteId=0;
    
    public Integer SucId=0;
    
    public Integer CajaId=0;
    
    public Integer CAEParteNroDesde=0;
    
    public Integer CAEParteNroHasta=0;
    
    public Integer CAEParteUltAsignado=0;
    
    public String CAEParteRowGUID;
    
    public Boolean CAEParteEliminado=false;
    
    public SDTCAEParteResponse206SDTCAEParteResponse206ItemCAE CAE;
    private transient java.lang.Object __source;    
    

    
    
    
    public void loadFromSoap(java.lang.Object paramObj,ExtendedSoapSerializationEnvelope __envelope)
    {
        if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;
        __source=inObj; 
        if(inObj instanceof SoapObject)
        {
            SoapObject soapObject=(SoapObject)inObj;
            int size = soapObject.getPropertyCount();
            for (int i0=0;i0< size;i0++)
            {
                PropertyInfo info=soapObject.getPropertyInfo(i0);
                if(!loadProperty(info,soapObject,__envelope))
                {
                }
            } 
        }

    }

    
    protected boolean loadProperty(PropertyInfo info,SoapObject soapObject,ExtendedSoapSerializationEnvelope __envelope)
    {
        java.lang.Object obj = info.getValue();
        if (info.name.equals("EmpId"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.EmpId = Integer.parseInt(j.toString());
                    }
                }
                else if (obj instanceof Integer){
                    this.EmpId = (Integer)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CAEId"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CAEId = Integer.parseInt(j.toString());
                    }
                }
                else if (obj instanceof Integer){
                    this.CAEId = (Integer)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CAEParteId"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CAEParteId = Integer.parseInt(j.toString());
                    }
                }
                else if (obj instanceof Integer){
                    this.CAEParteId = (Integer)obj;
                }
            }
            return true;
        }
        if (info.name.equals("SucId"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.SucId = Integer.parseInt(j.toString());
                    }
                }
                else if (obj instanceof Integer){
                    this.SucId = (Integer)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CajaId"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CajaId = Integer.parseInt(j.toString());
                    }
                }
                else if (obj instanceof Integer){
                    this.CajaId = (Integer)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CAEParteNroDesde"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CAEParteNroDesde = Integer.parseInt(j.toString());
                    }
                }
                else if (obj instanceof Integer){
                    this.CAEParteNroDesde = (Integer)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CAEParteNroHasta"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CAEParteNroHasta = Integer.parseInt(j.toString());
                    }
                }
                else if (obj instanceof Integer){
                    this.CAEParteNroHasta = (Integer)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CAEParteUltAsignado"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CAEParteUltAsignado = Integer.parseInt(j.toString());
                    }
                }
                else if (obj instanceof Integer){
                    this.CAEParteUltAsignado = (Integer)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CAEParteRowGUID"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CAEParteRowGUID = j.toString();
                    }
                }
                else if (obj instanceof String){
                    this.CAEParteRowGUID = (String)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CAEParteEliminado"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CAEParteEliminado = new Boolean(j.toString());
                    }
                }
                else if (obj instanceof Boolean){
                    this.CAEParteEliminado = (Boolean)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CAE"))
        {
            if(obj!=null)
            {
                java.lang.Object j = obj;
                this.CAE = (SDTCAEParteResponse206SDTCAEParteResponse206ItemCAE)__envelope.get(j,SDTCAEParteResponse206SDTCAEParteResponse206ItemCAE.class,false);
            }
            return true;
        }
        return false;
    }
    
    public java.lang.Object getOriginalXmlSource()
    {
        return __source;
    }    
    

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return EmpId;
        }
        if(propertyIndex==1)
        {
            return CAEId;
        }
        if(propertyIndex==2)
        {
            return CAEParteId;
        }
        if(propertyIndex==3)
        {
            return SucId;
        }
        if(propertyIndex==4)
        {
            return CajaId;
        }
        if(propertyIndex==5)
        {
            return CAEParteNroDesde;
        }
        if(propertyIndex==6)
        {
            return CAEParteNroHasta;
        }
        if(propertyIndex==7)
        {
            return CAEParteUltAsignado;
        }
        if(propertyIndex==8)
        {
            return CAEParteRowGUID;
        }
        if(propertyIndex==9)
        {
            return CAEParteEliminado;
        }
        if(propertyIndex==10)
        {
            return CAE;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 11;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "EmpId";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "CAEId";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "CAEParteId";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==3)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "SucId";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==4)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "CajaId";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==5)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "CAEParteNroDesde";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==6)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "CAEParteNroHasta";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==7)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "CAEParteUltAsignado";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==8)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "CAEParteRowGUID";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==9)
        {
            info.type = PropertyInfo.BOOLEAN_CLASS;
            info.name = "CAEParteEliminado";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==10)
        {
            info.type = SDTCAEParteResponse206SDTCAEParteResponse206ItemCAE.class;
            info.name = "CAE";
            info.namespace= "TAFACE";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    
}

