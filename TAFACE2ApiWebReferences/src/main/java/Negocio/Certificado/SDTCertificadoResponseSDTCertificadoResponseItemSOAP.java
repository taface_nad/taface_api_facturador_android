package Negocio.Certificado;


import java.util.Hashtable;
import org.ksoap2.serialization.*;

public class SDTCertificadoResponseSDTCertificadoResponseItemSOAP extends AttributeContainer implements KvmSerializable
{

    
    public Integer EmpId=0;
    
    public Integer CertId=0;
    
    public java.util.Date CertVigencia;
    
    public java.util.Date CertVencimiento;
    
    public String CertClaveEncriptacion;
    
    public String CertArchivo;
    
    public java.util.Date CertFchHora;
    
    public String CertRowGUID;
    
    public Boolean CertEliminado=false;
    private transient java.lang.Object __source;    
    

    
    
    
    public void loadFromSoap(java.lang.Object paramObj,ExtendedSoapSerializationEnvelopeSOAP __envelope)
    {
        if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;
        __source=inObj; 
        if(inObj instanceof SoapObject)
        {
            SoapObject soapObject=(SoapObject)inObj;
            int size = soapObject.getPropertyCount();
            for (int i0=0;i0< size;i0++)
            {
                PropertyInfo info=soapObject.getPropertyInfo(i0);
                if(!loadProperty(info,soapObject,__envelope))
                {
                }
            } 
        }

    }

    
    protected boolean loadProperty(PropertyInfo info,SoapObject soapObject,ExtendedSoapSerializationEnvelopeSOAP __envelope)
    {
        java.lang.Object obj = info.getValue();
        if (info.name.equals("EmpId"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.EmpId = Integer.parseInt(j.toString());
                    }
                }
                else if (obj instanceof Integer){
                    this.EmpId = (Integer)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CertId"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CertId = Integer.parseInt(j.toString());
                    }
                }
                else if (obj instanceof Integer){
                    this.CertId = (Integer)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CertVigencia"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CertVigencia = HelperSOAP.ConvertFromWebService(j.toString());
                    }
                }
                else if (obj instanceof java.util.Date){
                    this.CertVigencia = (java.util.Date)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CertVencimiento"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CertVencimiento = HelperSOAP.ConvertFromWebService(j.toString());
                    }
                }
                else if (obj instanceof java.util.Date){
                    this.CertVencimiento = (java.util.Date)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CertClaveEncriptacion"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CertClaveEncriptacion = j.toString();
                    }
                }
                else if (obj instanceof String){
                    this.CertClaveEncriptacion = (String)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CertArchivo"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CertArchivo = j.toString();
                    }
                }
                else if (obj instanceof String){
                    this.CertArchivo = (String)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CertFchHora"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CertFchHora = HelperSOAP.ConvertFromWebService(j.toString());
                    }
                }
                else if (obj instanceof java.util.Date){
                    this.CertFchHora = (java.util.Date)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CertRowGUID"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CertRowGUID = j.toString();
                    }
                }
                else if (obj instanceof String){
                    this.CertRowGUID = (String)obj;
                }
            }
            return true;
        }
        if (info.name.equals("CertEliminado"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.CertEliminado = new Boolean(j.toString());
                    }
                }
                else if (obj instanceof Boolean){
                    this.CertEliminado = (Boolean)obj;
                }
            }
            return true;
        }
        return false;
    }
    
    public java.lang.Object getOriginalXmlSource()
    {
        return __source;
    }    
    

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return EmpId;
        }
        if(propertyIndex==1)
        {
            return CertId;
        }
        if(propertyIndex==2)
        {
            return this.CertVigencia!=null?HelperSOAP.getDateFormat().format(this.CertVigencia):SoapPrimitive.NullNilElement;
        }
        if(propertyIndex==3)
        {
            return this.CertVencimiento!=null?HelperSOAP.getDateFormat().format(this.CertVencimiento):SoapPrimitive.NullNilElement;
        }
        if(propertyIndex==4)
        {
            return CertClaveEncriptacion;
        }
        if(propertyIndex==5)
        {
            return CertArchivo;
        }
        if(propertyIndex==6)
        {
            return this.CertFchHora!=null?HelperSOAP.getDateTimeFormat().format(this.CertFchHora):SoapPrimitive.NullNilElement;
        }
        if(propertyIndex==7)
        {
            return CertRowGUID;
        }
        if(propertyIndex==8)
        {
            return CertEliminado;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 9;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "EmpId";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "CertId";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "CertVigencia";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==3)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "CertVencimiento";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==4)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "CertClaveEncriptacion";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==5)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "CertArchivo";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==6)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "CertFchHora";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==7)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "CertRowGUID";
            info.namespace= "TAFACE";
        }
        if(propertyIndex==8)
        {
            info.type = PropertyInfo.BOOLEAN_CLASS;
            info.name = "CertEliminado";
            info.namespace= "TAFACE";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    
}

