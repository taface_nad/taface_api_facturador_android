package XML;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.converters.basic.DateConverter;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import java.io.IOException;
import javax.xml.bind.JAXBException;
import javax.xml.bind.PropertyException;

/**
 *
 * @author danielcera
 */
@XStreamAlias("XMLRESPUESTA")
public class XMLRESPUESTA {

    private String ERRORMSG;
    private String WARNINGMSG;
    private short FIRMADOOK;
    private String FIRMADOFCHHORA = ""; //formato esperado 2017-05-02T12:47:52.3558396-03:00
    private long CAENA;
    private long CAENROINICIAL;
    private long CAENROFINAL;
    private java.util.Date CAEVENCIMIENTO = new java.util.Date(0);
    private String CAESERIE;
    private long CAENRO;
    private String CODSEGURIDAD;
    private String URLPARAVERIFICARTEXTO;
    private String RESOLUCIONIVA;
    private String URLPARAVERIFICARQR;
    private short CORRESPONDESOBRE;

    public final String getERRORMSG() {
        return this.ERRORMSG;
    }

    public final void setERRORMSG(String value) {
        this.ERRORMSG = value;
    }

    public final String getWARNINGMSG() {
        return this.WARNINGMSG;
    }

    public final void setWARNINGMSG(String value) {
        this.WARNINGMSG = value;
    }

    public final short getFIRMADOOK() {
        return this.FIRMADOOK;
    }

    public final void setFIRMADOOK(short value) {
        this.FIRMADOOK = value;
    }

    public final String getFIRMADOFCHHORA() {
        //2017-05-02T12:47:52.3558396-03:00
        return this.FIRMADOFCHHORA;

    }

    public final void setFIRMADOFCHHORA(String value) {
        this.FIRMADOFCHHORA = value;
    }

    public final long getCAENA() {
        return this.CAENA;
    }

    public final void setCAENA(long value) {
        this.CAENA = value;
    }

    public final long getCAENROINICIAL() {
        return this.CAENROINICIAL;
    }

    public final void setCAENROINICIAL(long value) {
        this.CAENROINICIAL = value;
    }

    public final long getCAENROFINAL() {
        return this.CAENROFINAL;
    }

    public final void setCAENROFINAL(long value) {
        this.CAENROFINAL = value;
    }

    public final java.util.Date getCAEVENCIMIENTO() {
        return this.CAEVENCIMIENTO;
    }

    public final void setCAEVENCIMIENTO(java.util.Date value) {
        this.CAEVENCIMIENTO = value;
    }

    public final String getCAESERIE() {
        return this.CAESERIE;
    }

    public final void setCAESERIE(String value) {
        this.CAESERIE = value;
    }

    public final long getCAENRO() {
        return this.CAENRO;
    }

    public final void setCAENRO(long value) {
        this.CAENRO = value;
    }

    public final String getCODSEGURIDAD() {
        return this.CODSEGURIDAD;
    }

    public final void setCODSEGURIDAD(String value) {
        this.CODSEGURIDAD = value;
    }

    public final String getURLPARAVERIFICARTEXTO() {
        return this.URLPARAVERIFICARTEXTO;
    }

    public final void setURLPARAVERIFICARTEXTO(String value) {
        this.URLPARAVERIFICARTEXTO = value;
    }

    public final String getRESOLUCIONIVA() {
        return this.RESOLUCIONIVA;
    }

    public final void setRESOLUCIONIVA(String value) {
        this.RESOLUCIONIVA = value;
    }

    public final String getURLPARAVERIFICARQR() {
        return this.URLPARAVERIFICARQR;
    }

    public final void setURLPARAVERIFICARQR(String value) {
        this.URLPARAVERIFICARQR = value;
    }

    public final short getCORRESPONDESOBRE() {
        return this.CORRESPONDESOBRE;
    }

    public final void setCORRESPONDESOBRE(short value) {
        this.CORRESPONDESOBRE = value;
    }

    public final String ToXML() throws PropertyException, JAXBException, IOException {
        XStream xstream = new XStream(new StaxDriver());
        String[] formats = {"MM-dd-yyyy"};
        xstream.registerConverter(new DateConverter("yyyy-MM-dd", formats));
        //xstream.ignoreUnknownElements();
        xstream.processAnnotations(XMLRESPUESTA.class);
        String xstreamed = xstream.toXML(this).replace("<?xml version='1.0' encoding='UTF-8'?><XMLRESPUESTA>", "<XMLRESPUESTA xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"TAFACE\">");
        xstreamed = xstreamed.replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?><XMLRESPUESTA>", "<XMLRESPUESTA xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"TAFACE\">");
        xstreamed = xstreamed.replace("<?xml version=\"1.0\" ?><XMLRESPUESTA>", "<XMLRESPUESTA xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"TAFACE\">");
        return xstreamed;
    }

    public final XMLRESPUESTA LoadXML(String XML) throws JAXBException {
        XStream xstream = new XStream(new StaxDriver());
        String[] formats = {"MM-dd-yyyy"};
        xstream.registerConverter(new DateConverter("yyyy-MM-dd", formats));
        //xstream.ignoreUnknownElements();
        xstream.processAnnotations(XMLRESPUESTA.class);
        //converting xml to object
        XMLRESPUESTA myObject = (XMLRESPUESTA) xstream.fromXML(XML);

        return myObject;
    }
}