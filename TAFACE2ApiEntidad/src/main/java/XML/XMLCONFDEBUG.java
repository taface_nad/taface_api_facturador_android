package XML;

import TAFACE2ApiEntidad.TAException;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import javax.xml.bind.JAXBException;

/**
 *
 * @author danielcera
 */
@XStreamAlias("CONFIGDEBUG")
public class XMLCONFDEBUG {

    private String RutaArchivoConfigDebug = "TAFEApiDebug.config";
    private short TRAZAENCENDIDA;

    public final short getTRAZAENCENDIDA() {
        return TRAZAENCENDIDA;
    }

    public final void setTRAZAENCENDIDA(short value) {
        TRAZAENCENDIDA = value;
    }

    public final XMLCONFDEBUG CargarConfiguracion(String pRuta) throws FileNotFoundException, IOException, JAXBException {
        try {
            if (!pRuta.trim().equals("")) {
                RutaArchivoConfigDebug = pRuta + "TAFEApiDebug.config";
            }
            if (!((new java.io.File(RutaArchivoConfigDebug)).isFile())) {
                if (!((new java.io.File(pRuta)).isDirectory())) {
                    (new java.io.File(pRuta)).mkdir();
                }
                //Si el config no existe lo creo
                CrearConfiguracion();
            }
            String initialText = "";
            FileReader fr = new FileReader(RutaArchivoConfigDebug);
            BufferedReader br = new BufferedReader(fr);
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                initialText += sCurrentLine;
            }
            return LoadXML(initialText);
        } catch (RuntimeException ex) {
            throw new TAException("Error al cargar configuracion: " + ex.getMessage());
        }
    }

    private boolean CrearConfiguracion() throws FileNotFoundException, IOException, JAXBException {
        try {
            OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(RutaArchivoConfigDebug), Charset.forName("UTF-8").newEncoder());
            PrintWriter writer = new PrintWriter(osw);
            writer.print(this.ToXML());
            osw.close();
            return true;
        } catch (RuntimeException ex) {
            throw new TAException("Error al crear la configuracion: " + ex.getMessage());
        }
    }

    public final String ToXML() throws JAXBException, IOException {
        XStream xstream = new XStream(new StaxDriver());
        //xstream.ignoreUnknownElements();
        xstream.processAnnotations(XMLCONFDEBUG.class);
        String xstreamed = xstream.toXML(this).replace("<?xml version=\"1.0\" ?><CONFIGDEBUG>", "<CONFIGDEBUG xmlns:xsd=\"http://www.w3.org/20	01/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"TAFACE\">");
        return xstreamed;
    }

    public final XMLCONFDEBUG LoadXML(String XML) throws JAXBException {
        XStream xstream = new XStream(new StaxDriver());
        //xstream.ignoreUnknownElements();
        xstream.processAnnotations(XMLCONFDEBUG.class);
        //converting xml to object
        XMLCONFDEBUG myObject = (XMLCONFDEBUG) xstream.fromXML(XML);
        return myObject;
    }
}