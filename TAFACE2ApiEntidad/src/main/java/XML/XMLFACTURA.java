package XML;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.thoughtworks.xstream.converters.basic.DateConverter;
import com.thoughtworks.xstream.converters.reflection.FieldDictionary;
import com.thoughtworks.xstream.converters.reflection.PureJavaReflectionProvider;
import com.thoughtworks.xstream.io.xml.StaxDriver;

import java.lang.annotation.Repeatable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlAccessOrder;
import javax.xml.bind.annotation.XmlAccessorOrder;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlType;

@XMLSequence({
        "DGI",
        "DATOSCONTINGENCIA",
        "DATOSADICIONALES"
})


/**
 *
 * @author danielcera
 */
@XStreamAlias("XMLENTRADA")
public class XMLFACTURA implements java.io.Serializable {

	public XMLFACTURA() {
	}

	public enum enumTipoDeComprobanteCFE {

		eTicket(101),
		eTicketNotaCred(102),
		eTicketNotaDeb(103),
		eFactura(111),
		eFacturaNotaCred(112),
		eFacturaNotaDeb(113),
		eRemito(181),
		eResguardo(182),
		eFacturaExportacion(121),
		eFacturaNotaCredExportacion(122),
		eFacturaNotaDebExportacion(123),
		eRemitoExportacion(124),
		eTicketCuentaAjena(131),
		eTicketNotaCredCuentaAjena(132),
		eTicketNotaDebCuentaAjena(133),
		eFacturaCuentaAjena(141),
		eFacturaNotaCredCuentaAjena(142),
		eFacturaNotaDebCuentaAjena(143),
		eBoleta(151),
		eBoletaNotaCred(152),
		eBoletaNotaDeb(153),
		eTicketContingencia(201),
		eTicketNotaCredContingencia(202),
		eTicketNotaDebContingencia(203),
		eFacturaContingencia(211),
		eFacturaNotaCredContingencia(212),
		eFacturaNotaDebContingencia(213),
		eRemitoContingencia(281),
		eResguardoContingencia(282),
		eFacturaExportacionContingencia(221),
		eFacturaNotaCredExportacionContingencia(222),
		eFacturaNotaDebExportacionContingencia(223),
		eRemitoExportacionContingencia(224),
		eTicketCuentaAjenaContingencia(231),
		eTicketNotaCredCuentaAjenaContingencia(232),
		eTicketNotaDebCuentaAjenaContingencia(233),
		eFacturaCuentaAjenaContingencia(241),
		eFacturaNotaCredCuentaAjenaContingencia(242),
		eFacturaNotaDebCuentaAjenaContingencia(243),
		eBoletaContingencia(251),
		eBoletaNotaCredContingencia(252),
		eBoletaNotaDebContingencia(253);
		private int intValue;
		private static java.util.HashMap<Integer, enumTipoDeComprobanteCFE> mappings;

		private static java.util.HashMap<Integer, enumTipoDeComprobanteCFE> getMappings() {

				if (mappings == null) {
					mappings = new java.util.HashMap<Integer, enumTipoDeComprobanteCFE>();

			}
			return mappings;
		}

		private enumTipoDeComprobanteCFE(int value) {
			intValue = value;
			getMappings().put(value, this);
		}

		public int getValue() {
			return intValue;
		}

		public static enumTipoDeComprobanteCFE forValue(int value) {
			return getMappings().get(value);
		}
	}
	//NODO 1 - DatosAdicionales, NODO 2 - DGI
	//El nodo 1 se carga en el metodo NuevaFactura el cual debera ser invocado enseguida despues del constructor.
	//El nodo 2 se carga en las propiedades correspondientes al cabezal, detalle, etc.
	private clsDGI DGI = new clsDGI();
	private clsDATOSCONTINGENCIA DATOSCONTINGENCIA;
	private clsDATOSADICIONALES DATOSADICIONALES;

	public clsDGI getDGI() {
		return this.DGI;
	}

	public void setDGI(clsDGI value) {
		this.DGI = value;
	}

	public clsDATOSADICIONALES getDATOSADICIONALES() {
		return this.DATOSADICIONALES;
	}

	public void setDATOSADICIONALES(clsDATOSADICIONALES value) {
		this.DATOSADICIONALES = value;
	}

	public clsDATOSCONTINGENCIA getDATOSCONTINGENCIA() {
		return this.DATOSCONTINGENCIA;
	}

	public void setDATOSCONTINGENCIA(clsDATOSCONTINGENCIA value) {
		this.DATOSCONTINGENCIA = value;
	}

	@XStreamAlias("DGI")
    @XMLSequence({"IDDocVersionFormato", "IDDocTipoCFE", "IDDocFchEmis", "IDDocMntBruto", "IDDocFmaPago", "IDDocPeriodoDesde", "IDDocPeriodoHasta", "IDDocClausulaVenta", "IDDocModalidadVenta", "IDDocFchVenc", "IDDocTipoTraslado", "IDDocInfoAdicionalDoc", "IDDocViaTransporte", "IDDocIndPropiedad", "IDDocTipoDocProp", "IDDocCodPaisProp", "IDDocDocProp", "IDDocDocPropExt", "IDDocRznSocProp", "IDDocIVAalDia", "IDDocSecProf", "EMIRUCEmisor", "EMIRznSoc", "EMINomComercial", "EMIGiroEmis", "EmiTelefonos", "EMICorreoEmisor", "EMIEmiSucursal", "EMICdgDGISucur", "EMIDomFiscal", "EMICiudad", "EMIDepartamento", "EMIInfoAdicionalEmisor", "RECTipoDocRecep", "RECCodPaisRecep", "RECDocRecep", "RECRznSocRecep", "RECDirRecep", "RECCiudadRecep", "RECDeptoRecep", "RECPaisRecep", "RECInfoAdicional", "RECLugarDestinoEntrega", "RECNroOrdenCompra", "RECDocRecepExtranjero", "RECCP", "TOTTpoMoneda", "TOTMntNoGrv", "TOTMntExpoyAsim", "TOTMntImpuestoPerc", "TOTMntIVaenSusp", "TOTMntNetoIvaTasaMin", "TOTMntNetoIVATasaBasica", "TOTMntNetoIVAOtra", "TOTIVATasaMin", "TOTIVATasaBasica", "TOTMntIVATasaMin", "TOTMntIVATasaBasica", "TOTMntIVAOtra", "TOTMntTotal", "TOTMntTotRetenido", "TOTMntTotCredFisc", "TOTCantLinDet", "TOTMontoNF", "TOTMntPagar", "TOTTpoCambio", "TOTRetencPerceps", "Detalles", "SubTotInfos", "DscRcgGlobales", "MediosPago", "Referencias", "ADETextoADE", "ADEOtroADE", "CFRUCEmisor", "CFNombreMdte", "CFDocMdte", "CFTipoDocMdte", "CFPais"})
	public class clsDGI implements java.io.Serializable {

		private String IDDocVersionFormato = null;
		private String IDDocTipoCFE;
		private String IDDocFchEmis = null;
		private String IDDocMntBruto;
		private String IDDocFmaPago = null;
		private String IDDocPeriodoDesde = null;
		private String IDDocPeriodoHasta = null;
		private String IDDocClausulaVenta = null;
		private String IDDocModalidadVenta = null;
		private String IDDocFchVenc = null;
		private String IDDocTipoTraslado = null;
		private String IDDocInfoAdicionalDoc = null;
		private String IDDocViaTransporte = null;
		private String IDDocIndPropiedad = null;
		private String IDDocTipoDocProp = null;
		private String IDDocCodPaisProp = null;
		private String IDDocDocProp = null;
		private String IDDocDocPropExt = null;
		private String IDDocRznSocProp = null;
		private String IDDocIVAalDia = null;
		private String IDDocSecProf = null;
		private String EMIRUCEmisor = null;
		private String EMIRznSoc = null;
		private String EMINomComercial = null;
		private String EMIGiroEmis = null;
		@XStreamAlias("EmiTelefonos")
		private EmiTelefonos EmiTelefonos;
		private String EMICorreoEmisor = null;
		private String EMIEmiSucursal = null;
		private String EMICdgDGISucur;
		private String EMIDomFiscal = null;
		private String EMICiudad = null;
		private String EMIDepartamento = null;
		private String EMIInfoAdicionalEmisor = null;
		private String RECTipoDocRecep = null;
		private String RECCodPaisRecep = null;
		private String RECDocRecep = null;
		private String RECRznSocRecep = null;
		private String RECDirRecep = null;
		private String RECCiudadRecep = null;
		private String RECDeptoRecep = null;
		private String RECPaisRecep = null;
		private String RECInfoAdicional;
		private String RECLugarDestinoEntrega;
		private String RECNroOrdenCompra;
		private String RECDocRecepExtranjero;
		@XStreamOmitField
		private String RECCP;
		private String TOTTpoMoneda = null;
		private String TOTMntNoGrv;
		private String TOTMntExpoyAsim;
		private String TOTMntImpuestoPerc;
		private String TOTMntIVaenSusp;
		private String TOTMntNetoIvaTasaMin;
		private String TOTMntNetoIVATasaBasica;
		private String TOTMntNetoIVAOtra;
		private String TOTIVATasaMin;
		private String TOTIVATasaBasica;
		private String TOTMntIVATasaMin;
		private String TOTMntIVATasaBasica;
		private String TOTMntIVAOtra;
		private String TOTMntTotal;
		private String TOTMntTotRetenido;
		private String TOTMntTotCredFisc;
		private String TOTCantLinDet;
		private String TOTMontoNF;
		private String TOTMntPagar;
		private String TOTTpoCambio;
		@XStreamAlias("TOTRetencPerceps")
		private TOTRetencPerceps TOTRetencPerceps;
		@XStreamAlias("Detalles")
		private Detalle Detalles;
		@XStreamAlias("SubTotalInfos")
		private SubTotalInfos SubTotInfos;
		@XStreamAlias("DscRcgGlobales")
		private DscRcgGlobals DscRcgGlobales;
		@XStreamAlias("MediosPago")
		private MedioPagos MediosPago;
		@XStreamAlias("Referencias")
		private Referencias Referencias;
		private String ADETextoADE = null;
		private byte[] ADEOtroADE;
		private String CFRUCEmisor = null;
		private String CFNombreMdte = null;
		private String CFDocMdte = null;
		private String CFTipoDocMdte = null;
		private String CFPais = null;
		@XStreamOmitField
		private boolean iDDocPeriodoDesdeSpecified;
		@XStreamOmitField
		private boolean iDDocPeriodoHastaSpecified;
		@XStreamOmitField
		private boolean iDDocMntBrutoSpecified;
		@XStreamOmitField
		private boolean iDDocFmaPagoSpecified;
		@XStreamOmitField
		private boolean iDDocFchVencSpecified;
		@XStreamOmitField
		private boolean iDDocModalidadVentaSpecified;
		@XStreamOmitField
		private boolean iDDocViaTransporteSpecified;
		@XStreamOmitField
		private boolean iDDocIndPropiedadSpecified;
		@XStreamOmitField
		private boolean iDDocTipoDocPropSpecified;
		@XStreamOmitField
		private boolean iDDocCodPaisPropSpecified;
		@XStreamOmitField
		private boolean iDDocDocPropSpecified;
		@XStreamOmitField
		private boolean iDDocDocPropExtSpecified;
		@XStreamOmitField
		private boolean iDDocRznSocPropSpecified;
		@XStreamOmitField
		private boolean iDDocIVAalDiaSpecified;
		@XStreamOmitField
		private boolean iDDocSecProfSpecified;
		@XStreamOmitField
		private boolean tOTMontoNFSpecified;
		@XStreamOmitField
		private boolean tOTMntPagarSpecified;
		@XStreamOmitField
		private boolean rECTipoDocRecepSpecified;
		@XStreamOmitField
		private boolean iDDocTipoTrasladoSpecified;
		@XStreamOmitField
		private boolean rECCPSpecified;
		@XStreamOmitField
		private boolean tOTTpoCambioSpecified;
		@XStreamOmitField
		private boolean tOTMntNoGrvSpecified;
		@XStreamOmitField
		private boolean tOTMntExpoyAsimSpecified;
		@XStreamOmitField
		private boolean tOTMntImpuestoPercSpecified;
		@XStreamOmitField
		private boolean tOTMntIVaenSuspSpecified;
		@XStreamOmitField
		private boolean tOTMntNetoIvaTasaMinSpecified;
		@XStreamOmitField
		private boolean tOTMntNetoIVATasaBasicaSpecified;
		@XStreamOmitField
		private boolean tOTMntNetoIVAOtraSpecified;
		@XStreamOmitField
		private boolean tOTIVATasaMinSpecified;
		@XStreamOmitField
		private boolean tOTIVATasaBasicaSpecified;
		@XStreamOmitField
		private boolean tOTMntIVATasaMinSpecified;
		@XStreamOmitField
		private boolean tOTMntIVATasaBasicaSpecified;
		@XStreamOmitField
		private boolean tOTMntIVAOtraSpecified;
		@XStreamOmitField
		private boolean tOTMntTotalSpecified;
		@XStreamOmitField
		private boolean tOTMntTotRetenidoSpecified;
		@XStreamOmitField
		private boolean tOTMntTotCredFiscSpecified;
		@XStreamOmitField
		private boolean cFRUCEmisorSpecified;
		@XStreamOmitField
		private boolean cFTipoDocMdteSpecified;
		@XStreamOmitField
		private boolean cFPaisSpecified;
		@XStreamOmitField
		private boolean cFDocMdteSpecified;
		@XStreamOmitField
		private boolean CFNombreMdteSpecified;

		public clsDGI() {
		}

		public short getIDDocTipoCFE() {
			if (this.IDDocTipoCFE == null || this.IDDocTipoCFE.equals("")) {
				return 0;
			}
			return Short.valueOf(this.IDDocTipoCFE);
		}

		public void setIDDocTipoCFE(short value) {
			this.IDDocTipoCFE = String.valueOf(value);
		}

		public String getIdDocVersionFormato() {
			if (this.IDDocVersionFormato == null) {
				return "";
			} else {
				return this.IDDocVersionFormato;
			}
		}

		public void setIdDocVersionFormato(String value) {
			if (value != null && !value.equals("")) {
				this.IDDocVersionFormato = value;
			}
		}

		public java.util.Date getIDDocFchEmis() {
			SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date aux = new java.util.Date(0);
			if (IDDocFchEmis != null) {
				try {
					aux = formato.parse(IDDocFchEmis);
				} catch (ParseException ex) {
					Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
			return aux;
		}

		public void setIDDocFchEmis(java.util.Date value) {
			SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
			IDDocFchEmis = formato.format(value);
		}

		public java.util.Date getIDDocFchVenc() {
			SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date aux = null;
			if (IDDocFchVenc != null) {
				try {
					aux = formato.parse(IDDocFchVenc);
				} catch (ParseException ex) {
					Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
			return aux;
		}

		public void setIDDocFchVenc(java.util.Date value) {
			SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
			IDDocFchVenc = formato.format(value);
		}

		public short getIDDocTipoTraslado() {
			if (this.IDDocTipoTraslado != null) {
				return Short.parseShort(this.IDDocTipoTraslado);
			} else {
				return 0;
			}
		}

		public void setIDDocTipoTraslado(short value) {
			this.IDDocTipoTraslado = String.valueOf(value);
		}

		public boolean getIDDocTipoTrasladoSpecified() {
			return this.iDDocTipoTrasladoSpecified;
		}

		public void setIDDocTipoTrasladoSpecified(boolean value) {
			this.iDDocTipoTrasladoSpecified = value;
		}

		public java.util.Date getIDDocPeriodoDesde() {
			SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date aux = null;
			if (IDDocPeriodoDesde != null) {
				try {
					aux = formato.parse(IDDocPeriodoDesde);
				} catch (ParseException ex) {
					Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
			return aux;
		}

		public void setIDDocPeriodoDesde(java.util.Date value) {
			SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
			IDDocPeriodoDesde = formato.format(value);
		}

		public java.util.Date getIDDocPeriodoHasta() {
			SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date aux = null;
			if (IDDocPeriodoHasta != null) {
				try {
					aux = formato.parse(IDDocPeriodoHasta);
				} catch (ParseException ex) {
					Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
			return aux;
		}

		public void setIDDocPeriodoHasta(java.util.Date value) {
			SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
			IDDocPeriodoHasta = formato.format(value);
		}

		public boolean getIDDocPeriodoDesdeSpecified() {
			return this.iDDocPeriodoDesdeSpecified;
		}

		public void setIDDocPeriodoDesdeSpecified(boolean value) {
			this.iDDocPeriodoDesdeSpecified = value;
		}

		public boolean getIDDocPeriodoHastaSpecified() {
			return this.iDDocPeriodoHastaSpecified;
		}

		public void setIDDocPeriodoHastaSpecified(boolean value) {
			this.iDDocPeriodoHastaSpecified = value;
		}

		public short getIDDocMntBruto() {
			if (this.IDDocMntBruto == null || this.IDDocMntBruto.equals("")) {
				return 0;
			}
			return Short.parseShort(this.IDDocMntBruto);
		}

		public void setIDDocMntBruto(short value) {
			this.IDDocMntBruto = String.valueOf(value);
		}

		public boolean getIDDocMntBrutoSpecified() {
			return this.iDDocMntBrutoSpecified;
		}

		public void setIDDocMntBrutoSpecified(boolean value) {
			this.iDDocMntBrutoSpecified = value;
		}

		public byte getIDDocFmaPago() {
			if (this.IDDocFmaPago == null) {
				return 0;
			}
			return Byte.parseByte(this.IDDocFmaPago);
		}

		public void setIDDocFmaPago(byte value) {
			this.IDDocFmaPago = String.valueOf(value);
		}

		public boolean getIDDocFmaPagoSpecified() {
			return this.iDDocFmaPagoSpecified;
		}

		public void setIDDocFmaPagoSpecified(boolean value) {
			this.iDDocFmaPagoSpecified = value;
		}

		public boolean getIDDocFchVencSpecified() {
			return this.iDDocFchVencSpecified;
		}

		public void setIDDocFchVencSpecified(boolean value) {
			this.iDDocFchVencSpecified = value;
		}

		public String getIDDocClausulaVenta() {
			if (this.IDDocClausulaVenta == null) {
				return "";
			} else {
				return this.IDDocClausulaVenta;
			}
		}

		public void setIDDocClausulaVenta(String value) {
			if (value != null && !value.equals("")) {
				this.IDDocClausulaVenta = value;
			}
		}

		public short getIDDocModalidadVenta() {
			if (this.IDDocModalidadVenta == null || this.IDDocModalidadVenta.equals("")) {
				return 0;
			}
			return Short.parseShort(this.IDDocModalidadVenta);
		}

		public void setIDDocModalidadVenta(short value) {
			this.IDDocModalidadVenta = String.valueOf(value);
		}

		public boolean getIDDocModalidadVentaSpecified() {
			return this.iDDocModalidadVentaSpecified;
		}

		public void setIDDocModalidadVentaSpecified(boolean value) {
			this.iDDocModalidadVentaSpecified = value;
		}

		public short getIDDocViaTransporte() {
			if (this.IDDocViaTransporte == null || this.IDDocViaTransporte.equals("")) {
				return 0;
			} else {
				return Short.parseShort(this.IDDocViaTransporte);
			}
		}

		public void setIDDocViaTransporte(short value) {
			this.IDDocViaTransporte = String.valueOf(value);
		}

		public boolean getIDDocViaTransporteSpecified() {
			return this.iDDocViaTransporteSpecified;
		}

		public void setIDDocViaTransporteSpecified(boolean value) {
			this.iDDocViaTransporteSpecified = value;
		}

		public String getIDDocInfoAdicionalDoc() {
			if (this.IDDocInfoAdicionalDoc == null) {
				return "";
			} else {
				return this.IDDocInfoAdicionalDoc;
			}
		}

		public void setIDDocInfoAdicionalDoc(String value) {
			if (value != null && !value.equals("")) {
				this.IDDocInfoAdicionalDoc = value;
			}
		}

		public short getIDDocIndPropiedad() {
			if (this.IDDocIndPropiedad == null || this.IDDocIndPropiedad.equals("")) {
				return 0;
			} else {
				return Short.parseShort(this.IDDocIndPropiedad);
			}
		}

		public void setIDDocIndPropiedad(short value) {
			this.IDDocIndPropiedad = String.valueOf(value);
		}

		public short getIDDocTipoDocProp() {
			if (this.IDDocTipoDocProp == null || this.IDDocTipoDocProp.equals("")) {
				return 0;
			} else {
				return Short.parseShort(this.IDDocTipoDocProp);
			}
		}

		public void setIDDocTipoDocProp(short value) {
			this.IDDocTipoDocProp = String.valueOf(value);
		}

		public String getIDDocCodPaisProp() {
			if (this.IDDocCodPaisProp == null) {
				return "";
			} else {
				return this.IDDocCodPaisProp;
			}
		}

		public void setIDDocCodPaisProp(String value) {
			this.IDDocCodPaisProp = value;
		}

		public String getIDDocDocProp() {
			if (this.IDDocDocProp == null) {
				return "";
			} else {
				return this.IDDocDocProp;
			}
		}

		public void setIDDocDocProp(String value) {
			this.IDDocDocProp = value;
		}

		public String getIDDocDocPropExt() {
			if (this.IDDocDocPropExt == null) {
				return "";
			} else {
				return this.IDDocDocPropExt;
			}
		}

		public void setIDDocDocPropExt(String value) {
			this.IDDocDocPropExt = value;
		}

		public String getIDDocRznSocProp() {
			if (this.IDDocRznSocProp == null) {
				return "";
			} else {
				return this.IDDocRznSocProp;
			}
		}

		public void setIDDocRznSocProp(String value) {
			this.IDDocRznSocProp = value;
		}

		public short getIDDocIVAalDia() {
			if (this.IDDocIVAalDia == null || this.IDDocIVAalDia.equals("")) {
				return 0;
			} else {
				return Short.parseShort(this.IDDocIVAalDia);
			}
		}

		public void setIDDocIVAalDia(short value) {
			this.IDDocIVAalDia = String.valueOf(value);
		}

		public short getIDDocSecProf() {
			if (this.IDDocSecProf == null || this.IDDocSecProf.equals("")) {
				return 0;
			} else {
				return Short.parseShort(this.IDDocSecProf);
			}
		}

		public void setIDDocSecProf(short value) {
			this.IDDocSecProf = String.valueOf(value);
		}

		public void setIDDocIndPropiedadSpecified(boolean value) {
			this.iDDocIndPropiedadSpecified = value;
		}

		public boolean getIDDocIndPropiedadSpecified() {
			return this.iDDocIndPropiedadSpecified;
		}

		public void setIDDocTipoDocPropSpecified(boolean value) {
			this.iDDocTipoDocPropSpecified = value;
		}

		public boolean getIDDocTipoDocPropSpecified() {
			return this.iDDocTipoDocPropSpecified;
		}

		public void setIDDocCodPaisPropSpecified(boolean value) {
			this.iDDocCodPaisPropSpecified = value;
		}

		public boolean getIDDocCodPaisPropSpecified() {
			return this.iDDocCodPaisPropSpecified;
		}

		public void setIDDocDocPropSpecified(boolean value) {
			this.iDDocDocPropSpecified = value;
		}

		public boolean getIDDocDocPropSpecified() {
			return this.iDDocDocPropSpecified;
		}

		public void setIDDocDocPropExtSpecified(boolean value) {
			this.iDDocDocPropExtSpecified = value;
		}

		public boolean getIDDocDocPropExtSpecified() {
			return this.iDDocDocPropExtSpecified;
		}

		public void setIDDocRznSocPropSpecified(boolean value) {
			this.iDDocRznSocPropSpecified = value;
		}

		public boolean getIDDocRznSocPropSpecified() {
			return this.iDDocRznSocPropSpecified;
		}

		public void setIDDocIVAalDiaSpecified(boolean value) {
			this.iDDocIVAalDiaSpecified = value;
		}

		public boolean getIDDocIVAalDiaSpecified() {
			return this.iDDocIVAalDiaSpecified;
		}

		public void setIDDocSecProfSpecified(boolean value) {
			this.iDDocSecProfSpecified = value;
		}

		public boolean getIDDocSecProfSpecified() {
			return this.iDDocSecProfSpecified;
		}

		public String getEMIRUCEmisor() {
			if (this.EMIRUCEmisor == null) {
				return "";
			} else {
				return this.EMIRUCEmisor;
			}
		}

		public void setEMIRUCEmisor(String value) {
			if (value != null && !value.equals("")) {
				this.EMIRUCEmisor = value;
			}
		}

		public String getEMIRznSoc() {
			if (this.EMIRznSoc == null) {
				return "";
			} else {
				return this.EMIRznSoc;
			}
		}

		public void setEMIRznSoc(String value) {
			if (value != null && !value.equals("")) {
				this.EMIRznSoc = value;
			}
		}

		public String getEMINomComercial() {
			if (this.EMINomComercial == null) {
				return "";
			} else {
				return this.EMINomComercial;
			}
		}

		public void setEMINomComercial(String value) {
			if (value != null && !value.equals("")) {
				this.EMINomComercial = value;
			}
		}

		public String getEMIGiroEmis() {
			if (this.EMIGiroEmis == null) {
				return "";
			} else {
				return this.EMIGiroEmis;
			}
		}

		public void setEMIGiroEmis(String value) {
			if (value != null && !value.equals("")) {
				this.EMIGiroEmis = value;
			}
		}

		public EmiTelefonos getEmiTelefonos() {
			return this.EmiTelefonos;
		}

		public void setEmiTelefonos(EmiTelefonos value) {
			this.EmiTelefonos = value;
		}

		public String getEMICorreoEmisor() {
			if (this.EMICorreoEmisor == null) {
				return "";
			} else {
				return this.EMICorreoEmisor;
			}
		}

		public void setEMICorreoEmisor(String value) {
			if (value != null && !value.equals("")) {
				this.EMICorreoEmisor = value;
			}
		}

		public String getEMIEmiSucursal() {
			if (this.EMIEmiSucursal == null) {
				return "";
			} else {
				return this.EMIEmiSucursal;
			}
		}

		public void setEMIEmiSucursal(String value) {
			if (value != null && !value.equals("")) {
				this.EMIEmiSucursal = value;
			}
		}

		public int getEMICdgDGISucur() {
			if (this.EMICdgDGISucur == null || this.EMICdgDGISucur.equals("")) {
				return 0;
			}
			return Integer.parseInt(this.EMICdgDGISucur);
		}

		public void setEMICdgDGISucur(int value) {
			this.EMICdgDGISucur = String.valueOf(value);
		}

		public String getEMIDomFiscal() {
			if (this.EMIDomFiscal == null) {
				return "";
			} else {
				return this.EMIDomFiscal;
			}
		}

		public void setEMIDomFiscal(String value) {
			if (value != null && !value.equals("")) {
				this.EMIDomFiscal = value;
			}
		}

		public String getEMICiudad() {
			if (this.EMICiudad == null) {
				return "";
			} else {
				return this.EMICiudad;
			}
		}

		public void setEMICiudad(String value) {
			if (value != null && !value.equals("")) {
				this.EMICiudad = value;
			}
		}

		public String getEMIDepartamento() {
			if (this.EMIDepartamento == null) {
				return "";
			} else {
				return this.EMIDepartamento;
			}
		}

		public void setEMIDepartamento(String value) {
			if (value != null && !value.equals("")) {
				this.EMIDepartamento = value;
			}
		}

		public String getEMIInfoAdicionalEmisor() {
			if (this.EMIInfoAdicionalEmisor == null) {
				return "";
			} else {
				return this.EMIInfoAdicionalEmisor;
			}
		}

		public void setEMIInfoAdicionalEmisor(String value) {
			if (value != null && !value.equals("")) {
				this.EMIInfoAdicionalEmisor = value;
			}
		}

		public short getRECTipoDocRecep() {
			if (this.RECTipoDocRecep == null || this.RECTipoDocRecep.equals("")) {
				return 0;
			} else {
				return Short.parseShort(this.RECTipoDocRecep);
			}
		}

		public void setRECTipoDocRecep(short value) {
			this.RECTipoDocRecep = String.valueOf(value);
		}

		public boolean getRECTipoDocRecepSpecified() {
			return this.rECTipoDocRecepSpecified;
		}

		public void setRECTipoDocRecepSpecified(boolean value) {
			this.rECTipoDocRecepSpecified = value;
		}

		public String getRECCodPaisRecep() {
			if (this.RECCodPaisRecep == null) {
				return "";
			} else {
				return this.RECCodPaisRecep;
			}
		}

		public void setRECCodPaisRecep(String value) {
			if (value != null && !value.equals("")) {
				this.RECCodPaisRecep = value;
			}
		}

		public String getRECDocRecep() {
			if (this.RECDocRecep == null) {
				return "";
			} else {
				return this.RECDocRecep;
			}
		}

		public void setRECDocRecep(String value) {
			if (value != null && !value.equals("")) {
				this.RECDocRecep = value;
			}
		}

		public String getRECDocRecepExtranjero() {
			if (this.RECDocRecepExtranjero == null) {
				return "";
			} else {
				return this.RECDocRecepExtranjero;
			}
		}

		public void setRECDocRecepExtranjero(String value) {
			if (value != null && !value.equals("")) {
				this.RECDocRecepExtranjero = value;
			}
		}

		public String getRECRznSocRecep() {
			if (this.RECRznSocRecep == null) {
				return "";
			} else {
				return this.RECRznSocRecep;
			}
		}

		public void setRECRznSocRecep(String value) {
			if (value != null && !value.equals("")) {
				this.RECRznSocRecep = value;
			}
		}

		public String getRECDirRecep() {
			if (this.RECDirRecep == null) {
				return "";
			} else {
				return this.RECDirRecep;
			}
		}

		public void setRECDirRecep(String value) {
			if (value != null && !value.equals("")) {
				this.RECDirRecep = value;
			}
		}

		public String getRECCiudadRecep() {
			if (this.RECCiudadRecep == null) {
				return "";
			} else {
				return this.RECCiudadRecep;
			}
		}

		public void setRECCiudadRecep(String value) {
			if (value != null && !value.equals("")) {
				this.RECCiudadRecep = value;
			}
		}

		public String getRECDeptoRecep() {
			if (this.RECDeptoRecep == null) {
				return "";
			} else {
				return this.RECDeptoRecep;
			}
		}

		public void setRECDeptoRecep(String value) {
			if (value != null && !value.equals("")) {
				this.RECDeptoRecep = value;
			}
		}

		public String getRECPaisRecep() {
			if (this.RECPaisRecep == null) {
				return "";
			} else {
				return this.RECPaisRecep;
			}
		}

		public void setRECPaisRecep(String value) {
			if (value != null && !value.equals("")) {
				this.RECPaisRecep = value;
			}
		}

		public int getRECCP() {
			if (this.RECCP == null || this.RECCP.equals("")) {
				return 0;
			}
			return Integer.parseInt(this.RECCP);
		}

		public void setRECCP(int value) {
			this.RECCP = String.valueOf(value);
		}

		public boolean getRECCPSpecified() {
			return this.rECCPSpecified;
		}

		public void setRECCPSpecified(boolean value) {
			this.rECCPSpecified = value;
		}

		public String getRECInfoAdicional() {
			if (this.RECInfoAdicional == null) {
				return "";
			} else {
				return this.RECInfoAdicional;
			}
		}

		public void setRECInfoAdicional(String value) {
			if (value != null && !value.equals("")) {
				this.RECInfoAdicional = value;
			}
		}

		public String getRECLugarDestinoEntrega() {
			if (this.RECLugarDestinoEntrega == null) {
				return "";
			} else {
				return this.RECLugarDestinoEntrega;
			}
		}

		public void setRECLugarDestinoEntrega(String value) {
			if (value != null && !value.equals("")) {
				this.RECLugarDestinoEntrega = value;
			}
		}

		public String getRECNroOrdenCompra() {
			if (this.RECNroOrdenCompra == null) {
				return "";
			} else {
				return this.RECNroOrdenCompra;
			}
		}

		public void setRECNroOrdenCompra(String value) {
			if (value != null && !value.equals("")) {
				this.RECNroOrdenCompra = value;
			}
		}

		public String getTOTTpoMoneda() {
			if (this.TOTTpoMoneda == null) {
				return "";
			} else {
				return this.TOTTpoMoneda;
			}
		}

		public void setTOTTpoMoneda(String value) {
			if (value != null && !value.equals("")) {
				this.TOTTpoMoneda = value;
			}
		}

		public double getTOTTpoCambio() {
			if (this.TOTTpoCambio == null || this.TOTTpoCambio.equals("")) {
				return 0;
			}
			return Double.parseDouble(this.TOTTpoCambio);
		}

		public void setTOTTpoCambio(double value) {
			this.TOTTpoCambio = String.valueOf(value);
		}

		public boolean getTOTTpoCambioSpecified() {
			return this.tOTTpoCambioSpecified;
		}

		public void setTOTTpoCambioSpecified(boolean value) {
			this.tOTTpoCambioSpecified = value;
		}

		public double getTOTMntNoGrv() {
			if (this.TOTMntNoGrv == null || this.TOTMntNoGrv.equals("")) {
				return 0;
			}
			return Double.parseDouble(this.TOTMntNoGrv);
		}

		public void setTOTMntNoGrv(double value) {
			this.TOTMntNoGrv = String.valueOf(value);
		}

		public boolean getTOTMntNoGrvSpecified() {
			return this.tOTMntNoGrvSpecified;
		}

		public void setTOTMntNoGrvSpecified(boolean value) {
			this.tOTMntNoGrvSpecified = value;
		}

		public double getTOTMntExpoyAsim() {
			if (this.TOTMntExpoyAsim == null || this.TOTMntExpoyAsim.equals("")) {
				return 0;
			}
			return Double.parseDouble(this.TOTMntExpoyAsim);
		}

		public void setTOTMntExpoyAsim(double value) {
			this.TOTMntExpoyAsim = String.valueOf(value);
		}

		public boolean getTOTMntExpoyAsimSpecified() {
			return this.tOTMntExpoyAsimSpecified;
		}

		public void setTOTMntExpoyAsimSpecified(boolean value) {
			this.tOTMntExpoyAsimSpecified = value;
		}

		public double getTOTMntImpuestoPerc() {
			if (this.TOTMntImpuestoPerc == null || this.TOTMntImpuestoPerc.equals("")) {
				return 0;
			}
			return Double.parseDouble(this.TOTMntImpuestoPerc);
		}

		public void setTOTMntImpuestoPerc(double value) {
			this.TOTMntImpuestoPerc = String.valueOf(value);
		}

		public boolean getTOTMntImpuestoPercSpecified() {
			return this.tOTMntImpuestoPercSpecified;
		}

		public void setTOTMntImpuestoPercSpecified(boolean value) {
			this.tOTMntImpuestoPercSpecified = value;
		}

		public double getTOTMntIVaenSusp() {
			if (this.TOTMntIVaenSusp == null || this.TOTMntIVaenSusp.equals("")) {
				return 0;
			}
			return Double.parseDouble(this.TOTMntIVaenSusp);
		}

		public void setTOTMntIVaenSusp(double value) {
			this.TOTMntIVaenSusp = String.valueOf(value);
		}

		public boolean getTOTMntIVaenSuspSpecified() {
			return this.tOTMntIVaenSuspSpecified;
		}

		public void setTOTMntIVaenSuspSpecified(boolean value) {
			this.tOTMntIVaenSuspSpecified = value;
		}

		public double getTOTMntNetoIvaTasaMin() {
			if (this.TOTMntNetoIvaTasaMin == null || this.TOTMntNetoIvaTasaMin.equals("")) {
				return 0;
			}
			return Double.parseDouble(this.TOTMntNetoIvaTasaMin);
		}

		public void setTOTMntNetoIvaTasaMin(double value) {
			this.TOTMntNetoIvaTasaMin = String.valueOf(value);
		}

		public boolean getTOTMntNetoIvaTasaMinSpecified() {
			return this.tOTMntNetoIvaTasaMinSpecified;
		}

		public void setTOTMntNetoIvaTasaMinSpecified(boolean value) {
			this.tOTMntNetoIvaTasaMinSpecified = value;
		}

		public double getTOTMntNetoIVATasaBasica() {
			if (this.TOTMntNetoIVATasaBasica == null || this.TOTMntNetoIVATasaBasica.equals("")) {
				return 0;
			}
			return Double.parseDouble(this.TOTMntNetoIVATasaBasica);
		}

		public void setTOTMntNetoIVATasaBasica(double value) {
			this.TOTMntNetoIVATasaBasica = String.valueOf(value);
		}

		public boolean getTOTMntNetoIVATasaBasicaSpecified() {
			return this.tOTMntNetoIVATasaBasicaSpecified;
		}

		public void setTOTMntNetoIVATasaBasicaSpecified(boolean value) {
			this.tOTMntNetoIVATasaBasicaSpecified = value;
		}

		public double getTOTMntNetoIVAOtra() {
			if (this.TOTMntNetoIVAOtra == null || this.TOTMntNetoIVAOtra.equals("")) {
				return 0;
			}
			return Double.parseDouble(this.TOTMntNetoIVAOtra);
		}

		public void setTOTMntNetoIVAOtra(double value) {
			this.TOTMntNetoIVAOtra = String.valueOf(value);
		}

		public boolean getTOTMntNetoIVAOtraSpecified() {
			return this.tOTMntNetoIVAOtraSpecified;
		}

		public void setTOTMntNetoIVAOtraSpecified(boolean value) {
			this.tOTMntNetoIVAOtraSpecified = value;
		}

		public double getTOTIVATasaMin() {
			if (this.TOTIVATasaMin == null || this.TOTIVATasaMin.equals("")) {
				return 0;
			}
			return Double.parseDouble(this.TOTIVATasaMin);
		}

		public void setTOTIVATasaMin(double value) {
			this.TOTIVATasaMin = String.valueOf(value);
		}

		public boolean getTOTIVATasaMinSpecified() {
			return this.tOTIVATasaMinSpecified;
		}

		public void setTOTIVATasaMinSpecified(boolean value) {
			this.tOTIVATasaMinSpecified = value;
		}

		public double getTOTIVATasaBasica() {
			if (this.TOTIVATasaBasica == null || this.TOTIVATasaBasica.equals("")) {
				return 0;
			}
			return Double.parseDouble(this.TOTIVATasaBasica);
		}

		public void setTOTIVATasaBasica(double value) {
			this.TOTIVATasaBasica = String.valueOf(value);
		}

		public boolean getTOTIVATasaBasicaSpecified() {
			return this.tOTIVATasaBasicaSpecified;
		}

		public void setTOTIVATasaBasicaSpecified(boolean value) {
			this.tOTIVATasaBasicaSpecified = value;
		}

		public double getTOTMntIVATasaMin() {
			if (this.TOTMntIVATasaMin == null || this.TOTMntIVATasaMin.equals("")) {
				return 0;
			}
			return Double.parseDouble(this.TOTMntIVATasaMin);
		}

		public void setTOTMntIVATasaMin(double value) {
			this.TOTMntIVATasaMin = String.valueOf(value);
		}

		public boolean getTOTMntIVATasaMinSpecified() {
			return this.tOTMntIVATasaMinSpecified;
		}

		public void setTOTMntIVATasaMinSpecified(boolean value) {
			this.tOTMntIVATasaMinSpecified = value;
		}

		public double getTOTMntIVATasaBasica() {
			if (this.TOTMntIVATasaBasica == null || this.TOTMntIVATasaBasica.equals("")) {
				return 0;
			}
			return Double.parseDouble(this.TOTMntIVATasaBasica);
		}

		public void setTOTMntIVATasaBasica(double value) {
			this.TOTMntIVATasaBasica = String.valueOf(value);
		}

		public boolean getTOTMntIVATasaBasicaSpecified() {
			return this.tOTMntIVATasaBasicaSpecified;
		}

		public void setTOTMntIVATasaBasicaSpecified(boolean value) {
			this.tOTMntIVATasaBasicaSpecified = value;
		}

		public double getTOTMntIVAOtra() {
			if (this.TOTMntIVAOtra == null || this.TOTMntIVAOtra.equals("")) {
				return 0;
			}
			return Double.parseDouble(this.TOTMntIVAOtra);
		}

		public void setTOTMntIVAOtra(double value) {
			this.TOTMntIVAOtra = String.valueOf(value);
		}

		public boolean getTOTMntIVAOtraSpecified() {
			return this.tOTMntIVAOtraSpecified;
		}

		public void setTOTMntIVAOtraSpecified(boolean value) {
			this.tOTMntIVAOtraSpecified = value;
		}

		public double getTOTMntTotal() {
			if (this.TOTMntTotal == null || this.TOTMntTotal.equals("")) {
				return 0;
			}
			return Double.parseDouble(this.TOTMntTotal);
		}

		public void setTOTMntTotal(double value) {
			this.TOTMntTotal = String.valueOf(value);
		}

		public boolean getTOTMntTotalSpecified() {
			return this.tOTMntTotalSpecified;
		}

		public void setTOTMntTotalSpecified(boolean value) {
			this.tOTMntTotalSpecified = value;
		}

		public double getTOTMntTotRetenido() {
			if (this.TOTMntTotRetenido == null || this.TOTMntTotRetenido.equals("")) {
				return 0;
			}
			return Double.parseDouble(this.TOTMntTotRetenido);
		}

		public void setTOTMntTotRetenido(double value) {
			this.TOTMntTotRetenido = String.valueOf(value);
		}

		public boolean getTOTMntTotRetenidoSpecified() {
			return this.tOTMntTotRetenidoSpecified;
		}

		public void setTOTMntTotRetenidoSpecified(boolean value) {
			this.tOTMntTotRetenidoSpecified = value;
		}

		public double getTOTMntTotCredFisc() {
			if (this.TOTMntTotCredFisc == null || this.TOTMntTotCredFisc.equals("")) {
				return 0;
			}
			return Double.parseDouble(this.TOTMntTotCredFisc);
		}

		public void setTOTMntTotCredFisc(double value) {
			this.TOTMntTotCredFisc = String.valueOf(value);
		}

		public boolean getTOTMntTotCredFiscSpecified() {
			return this.tOTMntTotCredFiscSpecified;
		}

		public void setTOTMntTotCredFiscSpecified(boolean value) {
			this.tOTMntTotCredFiscSpecified = value;
		}

		public short getTOTCantLinDet() {
			if (this.TOTCantLinDet == null || this.TOTCantLinDet.equals("")) {
				return 0;
			}
			return Short.parseShort(this.TOTCantLinDet);
		}

		public void setTOTCantLinDet(short value) {
			this.TOTCantLinDet = String.valueOf(value);
		}

		public TOTRetencPerceps getTOTRetencPerceps() {
			return this.TOTRetencPerceps;
		}

		public void setTOTRetencPerceps(TOTRetencPerceps value) {
			this.TOTRetencPerceps = value;
		}

		public double getTOTMontoNF() {
			if (this.TOTMontoNF == null || this.TOTMontoNF.equals("")) {
				return 0;
			}
			return Double.parseDouble(this.TOTMontoNF);
		}

		public void setTOTMontoNF(double value) {
			this.TOTMontoNF = String.valueOf(value);
		}

		public boolean getTOTMontoNFSpecified() {
			return this.tOTMontoNFSpecified;
		}

		public void setTOTMontoNFSpecified(boolean value) {
			this.tOTMontoNFSpecified = value;
		}

		public double getTOTMntPagar() {
			if (this.TOTMntPagar == null || this.TOTMntPagar.equals("")) {
				return 0;
			}
			return Double.parseDouble(this.TOTMntPagar);
		}

		public void setTOTMntPagar(double value) {
			this.TOTMntPagar = String.valueOf(value);
		}

		public boolean getTOTMntPagarSpecified() {
			return this.tOTMntPagarSpecified;
		}

		public void setTOTMntPagarSpecified(boolean value) {
			this.tOTMntPagarSpecified = value;
		}

		public Detalle getDetalles() {
			return this.Detalles;
		}

		public void setDetalles(Detalle value) {
			this.Detalles = value;
		}

		public SubTotalInfos getSubTotalInfos() {
			return this.SubTotInfos;
		}

		public void setSubTotalInfos(SubTotalInfos value) {
			this.SubTotInfos = value;
		}

		public DscRcgGlobals getDscRcgGlobales() {
			return this.DscRcgGlobales;
		}

		public void setDscRcgGlobales(DscRcgGlobals value) {
			this.DscRcgGlobales = value;
		}

		public MedioPagos getMediosPago() {
			return this.MediosPago;
		}

		public void setMediosPago(MedioPagos value) {
			this.MediosPago = value;
		}

		public Referencias getReferencias() {
			return this.Referencias;
		}

		public void setReferencias(Referencias value) {
			this.Referencias = value;
		}

		public String getADETextoADE() {
			if (this.ADETextoADE == null) {
				return "";
			} else {
				return this.ADETextoADE;
			}
		}

		public void setADETextoADE(String value) {
			if (value != null && !value.equals("")) {
				this.ADETextoADE = value;
			}
		}

		public byte[] getADEOtroADE() {
			return this.ADEOtroADE;
		}

		public void setADEOtroADE(byte[] value) {
			this.ADEOtroADE = value;
		}

		public String getCFRUCEmisor() {
			if (this.CFRUCEmisor == null) {
				return "";
			} else {
				return this.CFRUCEmisor;
			}
		}

		public void setCFRUCEmisor(String value) {
			if (value != null && !value.equals("")) {
				this.CFRUCEmisor = value;
			}
		}

		public boolean getCFRUCEmisorSpecified() {
			return this.cFRUCEmisorSpecified;
		}

		public void setCFRUCEmisorSpecified(boolean value) {
			this.cFRUCEmisorSpecified = value;
		}

		public short getCFTipoDocMdte() {
			if (this.CFTipoDocMdte == null || this.CFTipoDocMdte.equals("")) {
				return 0;
			}
			return Short.parseShort(this.CFTipoDocMdte);

		}

		public void setCFTipoDocMdte(short value) {
			this.CFTipoDocMdte = String.valueOf(value);
		}

		public boolean getCFTipoDocMdteSpecified() {
			return this.cFTipoDocMdteSpecified;
		}

		public void setCFTipoDocMdteSpecified(boolean value) {
			this.cFTipoDocMdteSpecified = value;
		}

		public String getCFPais() {
			if (this.CFPais == null) {
				return "";
			} else {
				return this.CFPais;
			}
		}

		public void setCFPais(String value) {
			if (value != null && !value.equals("")) {
				this.CFPais = value;
			}
		}

		public boolean getCFPaisSpecified() {
			return this.cFPaisSpecified;
		}

		public void setCFPaisSpecified(boolean value) {
			this.cFPaisSpecified = value;
		}

		public String getCFDocMdte() {
			if (this.CFDocMdte == null) {
				return "";
			} else {
				return this.CFDocMdte;
			}
		}

		public void setCFDocMdte(String value) {
			if (value != null && !value.equals("")) {
				this.CFDocMdte = value;
			}
		}

		public boolean getCFDocMdteSpecified() {
			return this.cFDocMdteSpecified;
		}

		public void setCFDocMdteSpecified(boolean value) {
			this.cFDocMdteSpecified = value;
		}

		public String getCFNombreMdte() {
			if (this.CFNombreMdte == null) {
				return "";
			} else {
				return this.CFNombreMdte;
			}
		}

		public void setCFNombreMdte(String value) {
			if (value != null && !value.equals("")) {
				this.CFNombreMdte = value;
			}
		}

		public boolean getCFNombreMdteSpecified() {
			return this.CFNombreMdteSpecified;
		}

		public void setCFNombreMdteSpecified(boolean value) {
			this.CFNombreMdteSpecified = value;
		}

		@XStreamAlias("Detalles")
		public class Detalle implements java.io.Serializable {

			@XStreamImplicit
			private List<DETALLE> Detalle;

			/**
			 * @return the Detalle
			 */
			public List<DETALLE> getDetalle() {
				return Detalle;
			}

			/**
			 * @param Detalle the Detalle to set
			 */
			public void setDetalle(List<DETALLE> Detalle) {
				this.Detalle = Detalle;
			}

			@XStreamAlias(value = "Detalle")
            @XMLSequence({"DETNroLinDet", "DETCodItems", "DETIndFact", "DETIndAgenteResp", "DETNomItem", "DETDscItem", "DETCantidad", "DETUniMed", "DETPrecioUnitario", "DETDescuentoPct", "DETDescuentoMonto", "DETSubDescuentos", "DETRecargoPct",
                    "DETRecargoMnt", "DETSubRecargos", "DETRetencPerceps", "DETMontoItem", "dETCantidadSpecified", "dETPrecioUnitarioSpecified", "dETDescuentoPctSpecified", "dETDescuentoMontoSpecified", "dETRecargoPctSpecified",
            "dETRecargoMntSpecified","dETMontoItemSpecified"})
			public class DETALLE implements java.io.Serializable {

				private short DETNroLinDet;
				@XStreamAlias("DETCodItems")
				private DETCodItems DETCodItems;
				private String DETIndFact = null;
				private String DETIndAgenteResp = null;
				private String DETNomItem = null;
				private String DETDscItem = null;
				private String DETCantidad;
				private String DETUniMed = null;
				private String DETPrecioUnitario = null;
				private String DETDescuentoPct = null;
				private String DETDescuentoMonto = null;
				@XStreamAlias("DETSubDescuentos")
				private DETSubDescuentos DETSubDescuentos;
				private String DETRecargoPct = null;
				private String DETRecargoMnt = null;
				@XStreamAlias("DETSubRecargos")
				private DETSubRecargos DETSubRecargos;
				@XStreamAlias("DETRetencPerceps")
				private DETRetencPerceps DETRetencPerceps;
				private String DETMontoItem = null;
				@XStreamOmitField
				private boolean dETCantidadSpecified;
				@XStreamOmitField
				private boolean dETPrecioUnitarioSpecified;
				@XStreamOmitField
				private boolean dETDescuentoPctSpecified;
				@XStreamOmitField
				private boolean dETDescuentoMontoSpecified;
				@XStreamOmitField
				private boolean dETRecargoPctSpecified;
				@XStreamOmitField
				private boolean dETRecargoMntSpecified;
				@XStreamOmitField
				private boolean dETMontoItemSpecified;

				public short getDETNroLinDet() {
					return this.DETNroLinDet;
				}

				public void setDETNroLinDet(short value) {
					this.DETNroLinDet = value;
				}

				public DETCodItems getDETCodItems() {
					return this.DETCodItems;
				}

				public void setDETCodItems(DETCodItems value) {
					this.DETCodItems = value;
				}

				public byte getDETIndFact() {
					if (this.DETIndFact == null) {
						return 0;
					} else {
						return Byte.valueOf(DETIndFact);
					}
				}

				public void setDETIndFact(byte value) {
					if (value != 0) {
						this.DETIndFact = String.valueOf(value);
					}
				}

				public String getDETIndAgenteResp() {
					if (this.DETIndAgenteResp == null) {
						return "";
					} else {
						return this.DETIndAgenteResp;
					}
				}

				public void setDETIndAgenteResp(String value) {
					if (value != null && !value.equals("")) {
						this.DETIndAgenteResp = value;
					}
				}

				public String getDETNomItem() {
					if (this.DETNomItem == null) {
						return "";
					} else {
						return this.DETNomItem;
					}
				}

				public void setDETNomItem(String value) {
					if (value != null && !value.equals("")) {
						this.DETNomItem = value;
					}
				}

				public String getDETDscItem() {
					if (this.DETDscItem == null) {
						return "";
					} else {
						return this.DETDscItem;
					}
				}

				public void setDETDscItem(String value) {
					if (value != null && !value.equals("")) {
						this.DETDscItem = value;
					}
				}

				public double getDETCantidad() {
					if (this.DETCantidad == null || this.DETCantidad.equals("")) {
						return 0;
					}
					return Double.parseDouble(this.DETCantidad);
				}

				public void setDETCantidad(double value) {
					this.DETCantidad = String.valueOf(value);
				}

				public boolean getDETCantidadSpecified() {
					return this.dETCantidadSpecified;
				}

				public void setDETCantidadSpecified(boolean value) {
					this.dETCantidadSpecified = value;
				}

				public String getDETUniMed() {
					if (this.DETUniMed == null) {
						return "N/A";
					} else {
						return this.DETUniMed;
					}
				}

				public void setDETUniMed(String value) {
					if (value != null && !value.equals("")) {
						this.DETUniMed = value;
					}
				}

				public double getDETPrecioUnitario() {
					if (this.DETPrecioUnitario == null || this.DETPrecioUnitario.equals("")) {
						return 0;
					}
					return Math.round(Double.parseDouble(this.DETPrecioUnitario) * Math.pow(10, 6)) / Math.pow(10, 6);
				}

				public void setDETPrecioUnitario(double value) {
					this.DETPrecioUnitario = String.valueOf(value);
				}

				public boolean getDETPrecioUnitarioSpecified() {
					return this.dETPrecioUnitarioSpecified;
				}

				public void setDETPrecioUnitarioSpecified(boolean value) {
					this.dETPrecioUnitarioSpecified = value;
				}

				public double getDETDescuentoPct() {
					if (this.DETDescuentoPct == null || this.DETDescuentoPct.equals("")) {
						return 0;
					}
					return Double.parseDouble(this.DETDescuentoPct);
				}

				public void setDETDescuentoPct(double value) {
					this.DETDescuentoPct = String.valueOf(value);
				}

				public boolean getDETDescuentoPctSpecified() {
					return this.dETDescuentoPctSpecified;
				}

				public void setDETDescuentoPctSpecified(boolean value) {
					this.dETDescuentoPctSpecified = value;
				}

				public double getDETDescuentoMonto() {
					if (this.DETDescuentoMonto == null || this.DETDescuentoMonto.equals("")) {
						return 0;
					}
					return Double.parseDouble(this.DETDescuentoMonto);
				}

				public void setDETDescuentoMonto(double value) {
					this.DETDescuentoMonto = String.valueOf(value);
				}

				public boolean getDETDescuentoMontoSpecified() {
					return this.dETDescuentoMontoSpecified;
				}

				public void setDETDescuentoMontoSpecified(boolean value) {
					this.dETDescuentoMontoSpecified = value;
				}

				public DETSubDescuentos getDETSubDescuentos() {
					return this.DETSubDescuentos;
				}

				public void setDETSubDescuentos(DETSubDescuentos value) {
					this.DETSubDescuentos = value;
				}

				public double getDETRecargoPct() {
					if (this.DETRecargoPct == null || this.DETRecargoPct.equals("")) {
						return 0;
					}
					return Double.parseDouble(this.DETRecargoPct);
				}

				public void setDETRecargoPct(double value) {
					this.DETRecargoPct = String.valueOf(value);
				}

				public boolean getDETRecargoPctSpecified() {
					return this.dETRecargoPctSpecified;
				}

				public void setDETRecargoPctSpecified(boolean value) {
					this.dETRecargoPctSpecified = value;
				}

				public double getDETRecargoMnt() {
					if (this.DETRecargoMnt == null || this.DETRecargoMnt.equals("")) {
						return 0;
					}
					return Double.parseDouble(this.DETRecargoMnt);
				}

				public void setDETRecargoMnt(double value) {
					this.DETRecargoMnt = String.valueOf(value);
				}

				public boolean getDETRecargoMntSpecified() {
					return this.dETRecargoMntSpecified;
				}

				public void setDETRecargoMntSpecified(boolean value) {
					this.dETRecargoMntSpecified = value;
				}

				public DETSubRecargos getDETSubRecargos() {
					return this.DETSubRecargos;
				}

				public void setDETSubRecargos(DETSubRecargos value) {
					this.DETSubRecargos = value;
				}

				public DETRetencPerceps getDETRetencPerceps() {
					return this.DETRetencPerceps;
				}

				public void setDETRetencPerceps(DETRetencPerceps value) {
					this.DETRetencPerceps = value;
				}

				public double getDETMontoItem() {
					if (this.DETMontoItem == null || this.DETMontoItem.equals("")) {
						return 0;
					}
					return Double.parseDouble(this.DETMontoItem);
				}

				public void setDETMontoItem(double value) {
					this.DETMontoItem = String.valueOf(value);
				}

				public boolean getDETMontoItemSpecified() {
					return this.dETMontoItemSpecified;
				}

				public void setDETMontoItemSpecified(boolean value) {
					this.dETMontoItemSpecified = value;
				}
			}

			@XStreamAlias("DETRetencPerceps")
			public class DETRetencPerceps implements java.io.Serializable {

				@XStreamImplicit(itemFieldName = "DETRetencPercep")
				private List<DETRetencPercep> DETRetencPercep = new ArrayList<DETRetencPercep>();

				public List<DETRetencPercep> getDETRetencPercep() {
					return DETRetencPercep;
				}

				public void setDETRetencPercep(List<DETRetencPercep> DETRetencPercep) {
					this.DETRetencPercep = DETRetencPercep;
				}

				public class DETRetencPercep implements java.io.Serializable {

					private String DETCodRet = "";
					private double DETTasa;
					private double DETMntSujetoaRet;
					private double DETValRetPerc;

					public String getDETCodRet() {
						if (this.DETCodRet == null) {
							return "";
						} else {
							return this.DETCodRet;
						}
					}

					public void setDETCodRet(String value) {
						if (value != null && !value.equals("")) {
							this.DETCodRet = value;
						}
					}

					public double getDETTasa() {
						return this.DETTasa;
					}

					public void setDETTasa(double value) {
						this.DETTasa = value;
					}

					public double getDETMntSujetoaRet() {
						return this.DETMntSujetoaRet;
					}

					public void setDETMntSujetoaRet(double value) {
						this.DETMntSujetoaRet = value;
					}

					public double getDETValRetPerc() {
						return this.DETValRetPerc;
					}

					public void setDETValRetPerc(double value) {
						this.DETValRetPerc = value;
					}
				}
			}

			@XStreamAlias("DETSubRecargos")
			public class DETSubRecargos implements java.io.Serializable {

				@XStreamImplicit(itemFieldName = "DETSubRecargo")
				private List<DETSubRecargo> DETSubRecargo = new ArrayList<DETSubRecargo>();

				public List<DETSubRecargo> getDETSubRecargo() {
					return DETSubRecargo;
				}

				public void setDETSubRecargo(List<DETSubRecargo> DETSubRecargo) {
					this.DETSubRecargo = DETSubRecargo;
				}

				public class DETSubRecargo implements java.io.Serializable {

					private byte DETRecargoTipo;
					private double DETRecargoVal;

					public byte getDETRecargoTipo() {
						return this.DETRecargoTipo;
					}

					public void setDETRecargoTipo(byte value) {
						this.DETRecargoTipo = value;
					}

					public double getDETRecargoVal() {
						return this.DETRecargoVal;
					}

					public void setDETRecargoVal(double value) {
						this.DETRecargoVal = value;
					}
				}
			}

			@XStreamAlias("DETSubDescuentos")
			public class DETSubDescuentos implements java.io.Serializable {

				@XStreamImplicit(itemFieldName = "DETSubDescuento")
				private List<DETSubDescuento> DETSubDescuento = new ArrayList<DETSubDescuento>();

				public List<DETSubDescuento> getDETSubDescuento() {
					return DETSubDescuento;
				}

				public void setDETSubDescuento(List<DETSubDescuento> DETSubDescuento) {
					this.DETSubDescuento = DETSubDescuento;
				}
                @XMLSequence({"DETDescTipo", "DETDescVal"})
				public class DETSubDescuento implements java.io.Serializable {

					private byte DETDescTipo;
					private double DETDescVal;

					public byte getDETDescTipo() {
						return this.DETDescTipo;
					}

					public void setDETDescTipo(byte value) {
						this.DETDescTipo = value;
					}

					public double getDETDescVal() {
						return this.DETDescVal;
					}

					public void setDETDescVal(double value) {
						this.DETDescVal = value;
					}
				}
			}

			@XStreamAlias("DETCodItems")
			public class DETCodItems implements java.io.Serializable {

				@XStreamImplicit(itemFieldName = "DETCodItem")
				private List<DETCodItem> DETCodItem = new ArrayList<DETCodItem>();

				public List<DETCodItem> getDETCodItem() {
					return DETCodItem;
				}

				public void setDETCodItem(List<DETCodItem> DETCodItem) {
					this.DETCodItem = DETCodItem;
				}
                @XMLSequence({"DETCod", "DETTpoCod"})
				public class DETCodItem implements java.io.Serializable {

					private String DETCod = "";
					private String DETTpoCod = "";

					public String getDETCod() {
						if (this.DETCod == null) {
							return "";
						} else {
							return this.DETCod;
						}
					}

					public void setDETCod(String value) {
						if (value != null && !value.equals("")) {
							this.DETCod = value;
						}
					}

					public String getDETTpoCod() {
						if (this.DETTpoCod == null) {
							return "";
						} else {
							return this.DETTpoCod;
						}
					}

					public void setDETTpoCod(String value) {
						if (value != null && !value.equals("")) {
							this.DETTpoCod = value;
						}
					}
				}
			}
		}

		@XStreamAlias("TOTRetencPerceps")
		public class TOTRetencPerceps implements java.io.Serializable {

			@XStreamImplicit
			private List<TOTRetencPercep> TOTRetencPercep = new ArrayList<TOTRetencPercep>();

			public void setTOTRetencPercep(List<TOTRetencPercep> pTOTRetencPercep) {
				this.TOTRetencPercep = pTOTRetencPercep;
			}

			public List<TOTRetencPercep> getOTRetencPercep() {
				return this.TOTRetencPercep;
			}

			@XStreamAlias("TOTRetencPercep")
            @XMLSequence({"TOTCodRet", "TOTValRetPerc", "TOTValRetPercSpecified"})
			public class TOTRetencPercep implements java.io.Serializable {

				private String TOTCodRet = null;
				private double TOTValRetPerc;
				@XStreamOmitField
				private boolean TOTValRetPercSpecified;

				public String getTOTCodRet() {
					if (this.TOTCodRet == null) {
						return "";
					} else {
						return this.TOTCodRet;
					}
				}

				public void setTOTCodRet(String value) {
					if (value != null && !value.equals("")) {
						this.TOTCodRet = value;
					}
				}

				public double getTOTValRetPerc() {
					return this.TOTValRetPerc;
				}

				public void setTOTValRetPerc(double value) {
					this.TOTValRetPerc = value;
				}

				public boolean getTOTValRetPercSpecified() {
					return this.TOTValRetPercSpecified;
				}

				public void setTOTValRetPercSpecified(boolean value) {
					this.TOTValRetPercSpecified = value;
				}
			}
		}

		@XStreamAlias("EmiTelefonos")
        public class EmiTelefonos implements java.io.Serializable {

			@XStreamImplicit(itemFieldName = "EmiTelefono")
			private List<EMITelefono> EmiTelefono;

			public List<EMITelefono> getEMITelefono() {
				return EmiTelefono;
			}

			public void setEMITelefono(List<EMITelefono> EMITelefono) {
				this.EmiTelefono = EMITelefono;
			}

			public EmiTelefonos() {
			}

			public class EMITelefono implements java.io.Serializable {

				@XStreamAlias("EMITelefono")
				private String EMITelefono = "";

				public String getEMITelefono() {
					if (this.EMITelefono == null) {
						return "";
					} else {
						return this.EMITelefono;
					}
				}

				public void setEMITelefono(String value) {
					if (value != null && !value.equals("")) {
						this.EMITelefono = value;
					}
				}
			}
		}

		@XStreamAlias("SubTotalInfos")
		public class SubTotalInfos implements java.io.Serializable {

			@XStreamImplicit
			private List<SubTotalInfo> SubTotalInfo;

			public List<SubTotalInfo> getSubTotalInfo() {
				return this.SubTotalInfo;
			}

			public void setSubTotalInfo(List<SubTotalInfo> pSubTotalInfo) {
				this.SubTotalInfo = pSubTotalInfo;
			}

			@XStreamAlias("SubTotalInfo")
            @XMLSequence({"SUBNroSTI", "SUBGlosaSTI", "SUBOrdenSTI", "SUBValSubtotSTI"})
            public class SubTotalInfo implements java.io.Serializable {

				private byte SUBNroSTI;
				private String SUBGlosaSTI = null;
				private byte SUBOrdenSTI;
				private double SUBValSubtotSTI;

				public byte getSUBNroSTI() {
					return this.SUBNroSTI;
				}

				public void setSUBNroSTI(byte value) {
					this.SUBNroSTI = value;
				}

				public String getSUBGlosaSTI() {
					if (this.SUBGlosaSTI == null) {
						return "";
					} else {
						return this.SUBGlosaSTI;
					}
				}

				public void setSUBGlosaSTI(String value) {
					if (value != null && !value.equals("")) {
						this.SUBGlosaSTI = value;
					}
				}

				public byte getSUBOrdenSTI() {
					return this.SUBOrdenSTI;
				}

				public void setSUBOrdenSTI(byte value) {
					this.SUBOrdenSTI = value;
				}

				public double getSUBValSubtotSTI() {
					return this.SUBValSubtotSTI;
				}

				public void setSUBValSubtotSTI(double value) {
					this.SUBValSubtotSTI = value;
				}
			}
		}

		public class DscRcgGlobals implements java.io.Serializable {

			@XStreamImplicit(itemFieldName = "DscRcgGlobal")
			private List<DscRcgGlobal> DscRcgGlobal;

			public List<DscRcgGlobal> getDscRcgGlobal() {
				return DscRcgGlobal;
			}

			public void setDscRcgGlobal(List<DscRcgGlobal> pDscRcgGlobal) {
				this.DscRcgGlobal = pDscRcgGlobal;
			}

			@XStreamAlias("DscRcgGlobal")
            @XMLSequence({"DRGNroLinDR", "DRGTpoMovDR", "DRGTpoDR", "DRGCodDR", "DRGGlosaDR", "DRGValorDR", "DRGIndFactDR"})
			public class DscRcgGlobal implements java.io.Serializable {

				private byte DRGNroLinDR;
				private String DRGTpoMovDR = null;
				private byte DRGTpoDR;
				private short DRGCodDR;
				private String DRGGlosaDR = null;
				private double DRGValorDR;
				private short DRGIndFactDR;

				public byte getDRGNroLinDR() {
					return this.DRGNroLinDR;
				}

				public void setDRGNroLinDR(byte value) {
					this.DRGNroLinDR = value;
				}

				public String getDRGTpoMovDR() {
					if (this.DRGTpoMovDR == null) {
						return "";
					} else {
						return this.DRGTpoMovDR;
					}
				}

				public void setDRGTpoMovDR(String value) {
					if (value != null && !value.equals("")) {
						this.DRGTpoMovDR = value;
					}
				}

				public byte getDRGTpoDR() {
					return this.DRGTpoDR;
				}

				public void setDRGTpoDR(byte value) {
					this.DRGTpoDR = value;
				}

				public short getDRGCodDR() {
					return this.DRGCodDR;
				}

				public void setDRGCodDR(short value) {
					this.DRGCodDR = value;
				}

				public String getDRGGlosaDR() {
					if (this.DRGGlosaDR == null) {
						return "";
					} else {
						return this.DRGGlosaDR;
					}
				}

				public void setDRGGlosaDR(String value) {
					if (value != null && !value.equals("")) {
						this.DRGGlosaDR = value;
					}
				}

				public double getDRGValorDR() {
					return this.DRGValorDR;
				}

				public void setDRGValorDR(double value) {
					this.DRGValorDR = value;
				}

				public short getDRGIndFactDR() {
					return this.DRGIndFactDR;
				}

				public void setDRGIndFactDR(short value) {
					this.DRGIndFactDR = value;
				}
			}
		}

		public class MedioPagos implements java.io.Serializable {

			@XStreamImplicit(itemFieldName = "MedioPago")
			private List<MedioPago> MedioPago;

			public List<MedioPago> getMedioPago() {
				return this.MedioPago;
			}

			public void setMedioPago(List<MedioPago> pMedioPago) {
				this.MedioPago = pMedioPago;
			}

			@XStreamAlias("MedioPago")
            @XMLSequence({"MPNroLinMP", "MPCodMP", "MPGlosaMP", "MPOrdenMP", "MPValorPago"})
			public class MedioPago implements java.io.Serializable {

				private byte MPNroLinMP;
				private short MPCodMP;
				private String MPGlosaMP = null;
				private byte MPOrdenMP;
				private double MPValorPago;

				public byte getMPNroLinMP() {
					return this.MPNroLinMP;
				}

				public void setMPNroLinMP(byte value) {
					this.MPNroLinMP = value;
				}

				public short getMPCodMP() {
					return this.MPCodMP;
				}

				public void setMPCodMP(short value) {
					this.MPCodMP = value;
				}

				public String getMPGlosaMP() {
					if (this.MPGlosaMP == null) {
						return "";
					} else {
						return this.MPGlosaMP;
					}
				}

				public void setMPGlosaMP(String value) {
					if (value != null && !value.equals("")) {
						this.MPGlosaMP = value;
					}
				}

				public byte getMPOrdenMP() {
					return this.MPOrdenMP;
				}

				public void setMPOrdenMP(byte value) {
					this.MPOrdenMP = value;
				}

				public double getMPValorPago() {
					return this.MPValorPago;
				}

				public void setMPValorPago(double value) {
					this.MPValorPago = value;
				}
			}
		}

		@XStreamAlias("Referencias")
		public class Referencias implements java.io.Serializable {

			@XStreamImplicit
			private List<Referencia> Referencia;

			public List<Referencia> getReferencia() {
				return this.Referencia;
			}

			public void setReferencia(List<Referencia> pReferencia) {
				this.Referencia = pReferencia;
			}

			@XStreamAlias("Referencia")
            @XMLSequence({"REFNroLinRef", "REFIndGlobal", "REFIndGlobalSpecified", "REFTpoDocRef", "REFTpoDocRefSpecified", "REFSerie", "REFNroCFERef", "REFNroCFERefSpecified", "REFRazonRef", "REFFechaCFEref", "REFFechaCFErefSpecified"})
			public class Referencia implements java.io.Serializable {

				private byte REFNroLinRef;
				private String REFIndGlobal = null;
				@XStreamOmitField
				private boolean REFIndGlobalSpecified;
				private short REFTpoDocRef;
				@XStreamOmitField
				private boolean REFTpoDocRefSpecified;
				private String REFSerie = null;
				private String REFNroCFERef;
				@XStreamOmitField
				private boolean REFNroCFERefSpecified;
				private String REFRazonRef = null;
				private String REFFechaCFEref = null;
				@XStreamOmitField
				private boolean REFFechaCFErefSpecified;

				public byte getREFNroLinRef() {
					return this.REFNroLinRef;
				}

				public void setREFNroLinRef(byte value) {
					this.REFNroLinRef = value;
				}

				public String getREFIndGlobal() {
					if (this.REFIndGlobal == null) {
						return "";
					} else {
						return this.REFIndGlobal;
					}
				}

				public void setREFIndGlobal(String value) {
					if (value != null && !value.equals("")) {
						this.REFIndGlobal = value;
					}
				}

				public boolean getREFIndGlobalSpecified() {
					return this.REFIndGlobalSpecified;
				}

				public void setREFIndGlobalSpecified(boolean value) {
					this.REFIndGlobalSpecified = value;
				}

				public short getREFTpoDocRef() {
					return this.REFTpoDocRef;
				}

				public void setREFTpoDocRef(short value) {
					this.REFTpoDocRef = value;
				}

				public boolean getREFTpoDocRefSpecified() {
					return this.REFTpoDocRefSpecified;
				}

				public void setREFTpoDocRefSpecified(boolean value) {
					this.REFTpoDocRefSpecified = value;
				}

				public String getREFSerie() {
					if (this.REFSerie == null) {
						return "";
					} else {
						return this.REFSerie;
					}
				}

				public void setREFSerie(String value) {
					if (value != null && !value.equals("")) {
						this.REFSerie = value;
					}
				}

				public String getREFNroCFERef() {
					if (this.REFNroCFERef == null) {
						return "";
					} else {
						return this.REFNroCFERef;
					}
				}

				public void setREFNroCFERef(String value) {
					if (value != null && !value.equals("")) {
						this.REFNroCFERef = value;
					}
				}

				public boolean getREFNroCFERefSpecified() {
					return this.REFNroCFERefSpecified;
				}

				public void setREFNroCFERefSpecified(boolean value) {
					this.REFNroCFERefSpecified = value;
				}

				public String getREFRazonRef() {
					if (this.REFRazonRef == null) {
						return "";
					} else {
						return this.REFRazonRef;
					}
				}

				public void setREFRazonRef(String value) {
					if (value != null && !value.equals("")) {
						this.REFRazonRef = value;
					}
				}

				public java.util.Date getREFFechaCFEref() {
					SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
					java.util.Date aux = new java.util.Date(0);
					if (REFFechaCFEref != null) {
						try {
							aux = formato.parse(REFFechaCFEref);
						} catch (ParseException ex) {
							Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
						}
					}
					return aux;
				}

				public void setREFFechaCFEref(java.util.Date value) {
					SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
					REFFechaCFEref = formato.format(value);
				}

				public boolean getREFFechaCFErefSpecified() {
					return this.REFFechaCFErefSpecified;
				}

				public void setREFFechaCFErefSpecified(boolean value) {
					this.REFFechaCFErefSpecified = value;
				}
			}
		}
	}


	@XStreamAlias("DATOSCONTINGENCIA")
    @XMLSequence({"TIPOCFE","SERIE", "NRO", "CAENROAUTORIZACION","CAENRODESDE","CAENROHASTA","CAEVENCIMIENTO","CAEFecVencSpecified"})
	public class clsDATOSCONTINGENCIA implements java.io.Serializable {

		private short TIPOCFE = new Short("0");
		private String SERIE = null;
		private int NRO;
		private long CAENROAUTORIZACION;
		private long CAENRODESDE;
		private long CAENROHASTA;
		private String CAEVENCIMIENTO = null;
		@XStreamOmitField
		private boolean CAEFecVencSpecified;

		public clsDATOSCONTINGENCIA() {
		}

		public short getTIPOCFE() {
			return this.TIPOCFE;
		}

		public void setTIPOCFE(short value) {
			this.TIPOCFE = value;
		}

		public String getSERIE() {
			if (this.SERIE == null) {
				return "";
			} else {
				return this.SERIE;
			}
		}

		public void setSERIE(String value) {
			if (value != null && !value.equals("")) {
				this.SERIE = value;
			}
		}

		public int getNRO() {
			return this.NRO;
		}

		public void setNRO(int value) {
			this.NRO = value;
		}

		public long getCAENROAUTORIZACION() {
			return this.CAENROAUTORIZACION;
		}

		public void setCAENROAUTORIZACION(long value) {
			this.CAENROAUTORIZACION = value;
		}

		public long getCAENRODESDE() {
			return this.CAENRODESDE;
		}

		public void setCAENRODESDE(long value) {
			this.CAENRODESDE = value;
		}

		public long getCAENROHASTA() {
			return this.CAENROHASTA;
		}

		public void setCAENROHASTA(long value) {
			this.CAENROHASTA = value;
		}

		public java.util.Date getCAEVENCIMIENTO() {
			SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date aux = null;
			if (CAEVENCIMIENTO != null) {
				try {
					aux = formato.parse(CAEVENCIMIENTO);
				} catch (ParseException ex) {
					Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
			return aux;
		}

		public void setCAEVENCIMIENTO(java.util.Date value) {
			SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
			CAEVENCIMIENTO = formato.format(value);
		}

		public boolean getCAEVENCIMIENTOSpecified() {
			return this.CAEFecVencSpecified;
		}

		public void setCAEVENCIMIENTOSpecified(boolean value) {
			this.CAEFecVencSpecified = value;
		}
	}

	@XStreamAlias("DATOSADICIONALES")
    @XMLSequence({"EMPRESARUC", "SUCURSALNRO", "CAJANRO", "SOFTWAREFACTURADOR", "VERSIONDESOFTWAREFACTURADOR", "TRANSACCIONNRO", "DOCUMENTOORIGINALTIPO", "DOCUMENTOORIGINALSERIE", "DOCUMENTOORIGINALNRO", "CAJERONRO", "CAJERONOMBRE", "CLIENTENRO", "CLIENTENOMBRE", "CLIENTERAZONSOCIAL", "CLIENTEDOCUMENTO", "CLIENTEDIRECCION", "CLIENTEPAISNOM", "VENDEDORNRO", "VENDEDORNOMBRE", "TIPODENEGOCIO", "COTIZACIONUI", "CLIENTETELEFONO", "CLIENTEEMAIL"})
	public class clsDATOSADICIONALES implements java.io.Serializable {

		private String EMPRESARUC = null;
		private int SUCURSALNRO;
		private int CAJANRO;
		private String SOFTWAREFACTURADOR = null;
		private String VERSIONDESOFTWAREFACTURADOR = null;
		private long TRANSACCIONNRO;
		private String DOCUMENTOORIGINALTIPO = null;
		private String DOCUMENTOORIGINALSERIE = null;
		private long DOCUMENTOORIGINALNRO;
		private long CAJERONRO;
		private String CAJERONOMBRE = null;
		private long CLIENTENRO;
		private String CLIENTENOMBRE = null;
		private String CLIENTERAZONSOCIAL = null;
		private String CLIENTEDOCUMENTO = null;
		private String CLIENTEDIRECCION = null;
		private String CLIENTEPAISNOM = null;
		private long VENDEDORNRO;
		private String VENDEDORNOMBRE = null;
		private short TIPODENEGOCIO;
		private double COTIZACIONUI;
		private String CLIENTETELEFONO = null;
		private String CLIENTEEMAIL = null;

		/**
		 * @return the EMPRESARUC
		 */
		public String getEMPRESARUC() {
			if (this.EMPRESARUC == null) {
				return "";
			} else {
				return this.EMPRESARUC;
			}
		}

		/**
		 * @param EMPRESARUC the EMPRESARUC to set
		 */
		public void setEMPRESARUC(String EMPRESARUC) {
			if (EMPRESARUC != null && !EMPRESARUC.equals("")) {
				this.EMPRESARUC = EMPRESARUC;
			}
		}

		/**
		 * @return the SUCURSALNRO
		 */
		public int getSUCURSALNRO() {
			return SUCURSALNRO;
		}

		/**
		 * @param SUCURSALNRO the SUCURSALNRO to set
		 */
		public void setSUCURSALNRO(int SUCURSALNRO) {
			this.SUCURSALNRO = SUCURSALNRO;
		}

		/**
		 * @return the CAJANRO
		 */
		public int getCAJANRO() {
			return CAJANRO;
		}

		/**
		 * @param CAJANRO the CAJANRO to set
		 */
		public void setCAJANRO(int CAJANRO) {
			this.CAJANRO = CAJANRO;
		}

		/**
		 * @return the SOFTWAREFACTURADOR
		 */
		public String getSOFTWAREFACTURADOR() {
			if (this.SOFTWAREFACTURADOR == null) {
				return "";
			} else {
				return this.SOFTWAREFACTURADOR;
			}
		}

		/**
		 * @param SOFTWAREFACTURADOR the SOFTWAREFACTURADOR to set
		 */
		public void setSOFTWAREFACTURADOR(String SOFTWAREFACTURADOR) {
			if (SOFTWAREFACTURADOR != null && !SOFTWAREFACTURADOR.equals("")) {
				this.SOFTWAREFACTURADOR = SOFTWAREFACTURADOR;
			}
		}

		/**
		 * @return the VERSIONDESOFTWAREFACTURADOR
		 */
		public String getVERSIONDESOFTWAREFACTURADOR() {
			if (this.VERSIONDESOFTWAREFACTURADOR == null) {
				return "";
			} else {
				return this.VERSIONDESOFTWAREFACTURADOR;
			}
		}

		/**
		 * @param VERSIONDESOFTWAREFACTURADOR the
		 * VERSIONDESOFTWAREFACTURADOR to set
		 */
		public void setVERSIONDESOFTWAREFACTURADOR(String VERSIONDESOFTWAREFACTURADOR) {
			if (VERSIONDESOFTWAREFACTURADOR != null && !VERSIONDESOFTWAREFACTURADOR.equals("")) {
				this.VERSIONDESOFTWAREFACTURADOR = VERSIONDESOFTWAREFACTURADOR;
			}
		}

		/**
		 * @return the CAJERONRO
		 */
		public long getCAJERONRO() {
			return CAJERONRO;
		}

		/**
		 * @param CAJERONRO the CAJERONRO to set
		 */
		public void setCAJERONRO(long CAJERONRO) {
			this.CAJERONRO = CAJERONRO;
		}

		/**
		 * @return the CAJERONOMBRE
		 */
		public String getCAJERONOMBRE() {
			if (this.CAJERONOMBRE == null) {
				return "";
			} else {
				return this.CAJERONOMBRE;
			}
		}

		/**
		 * @param CAJERONOMBRE the CAJERONOMBRE to set
		 */
		public void setCAJERONOMBRE(String CAJERONOMBRE) {
			if (CAJERONOMBRE != null && !CAJERONOMBRE.equals("")) {
				this.CAJERONOMBRE = CAJERONOMBRE;
			}
		}

		/**
		 * @return the DOCUMENTOORIGINALTIPO
		 */
		public String getDOCUMENTOORIGINALTIPO() {
			if (this.DOCUMENTOORIGINALTIPO == null) {
				return "";
			} else {
				return this.DOCUMENTOORIGINALTIPO;
			}
		}

		/**
		 * @param DOCUMENTOORIGINALTIPO the DOCUMENTOORIGINALTIPO to
		 * set
		 */
		public void setDOCUMENTOORIGINALTIPO(String DOCUMENTOORIGINALTIPO) {
			if (DOCUMENTOORIGINALTIPO != null && !DOCUMENTOORIGINALTIPO.equals("")) {
				this.DOCUMENTOORIGINALTIPO = DOCUMENTOORIGINALTIPO;
			}
		}

		/**
		 * @return the DOCUMENTOORIGINALSERIE
		 */
		public String getDOCUMENTOORIGINALSERIE() {
			if (this.DOCUMENTOORIGINALSERIE == null) {
				return "";
			} else {
				return this.DOCUMENTOORIGINALSERIE;
			}
		}

		/**
		 * @param DOCUMENTOORIGINALSERIE the DOCUMENTOORIGINALSERIE to
		 * set
		 */
		public void setDOCUMENTOORIGINALSERIE(String DOCUMENTOORIGINALSERIE) {
			if (DOCUMENTOORIGINALSERIE != null && !DOCUMENTOORIGINALSERIE.equals("")) {
				this.DOCUMENTOORIGINALSERIE = DOCUMENTOORIGINALSERIE;
			}
		}

		/**
		 * @return the DOCUMENTOORIGINALNRO
		 */
		public long getDOCUMENTOORIGINALNRO() {
			return DOCUMENTOORIGINALNRO;
		}

		/**
		 * @param DOCUMENTOORIGINALNRO the DOCUMENTOORIGINALNRO to set
		 */
		public void setDOCUMENTOORIGINALNRO(long DOCUMENTOORIGINALNRO) {
			this.DOCUMENTOORIGINALNRO = DOCUMENTOORIGINALNRO;
		}

		/**
		 * @return the TRANSACCIONNRO
		 */
		public long getTRANSACCIONNRO() {
			return TRANSACCIONNRO;
		}

		/**
		 * @param TRANSACCIONNRO the TRANSACCIONNRO to set
		 */
		public void setTRANSACCIONNRO(long TRANSACCIONNRO) {
			this.TRANSACCIONNRO = TRANSACCIONNRO;
		}

		/**
		 * @return the CLIENTENRO
		 */
		public long getCLIENTENRO() {
			return CLIENTENRO;
		}

		/**
		 * @param CLIENTENRO the CLIENTENRO to set
		 */
		public void setCLIENTENRO(long CLIENTENRO) {
			this.CLIENTENRO = CLIENTENRO;
		}

		/**
		 * @return the CLIENTERAZONSOCIAL
		 */
		public String getCLIENTERAZONSOCIAL() {
			if (this.CLIENTERAZONSOCIAL == null) {
				return "";
			} else {
				return this.CLIENTERAZONSOCIAL;
			}
		}

		/**
		 * @param CLIENTERAZONSOCIAL the CLIENTERAZONSOCIAL to set
		 */
		public void setCLIENTERAZONSOCIAL(String CLIENTERAZONSOCIAL) {
			if (CLIENTERAZONSOCIAL != null && !CLIENTERAZONSOCIAL.equals("")) {
				this.CLIENTERAZONSOCIAL = CLIENTERAZONSOCIAL;
			}
		}

		/**
		 * @return the CLIENTEDOCUMENTO
		 */
		public String getCLIENTEDOCUMENTO() {
			if (this.CLIENTEDOCUMENTO == null) {
				return "";
			} else {
				return this.CLIENTEDOCUMENTO;
			}
		}

		/**
		 * @param CLIENTEDOCUMENTO the CLIENTEDOCUMENTO to set
		 */
		public void setCLIENTEDOCUMENTO(String CLIENTEDOCUMENTO) {
			if (CLIENTEDOCUMENTO != null && !CLIENTEDOCUMENTO.equals("")) {
				this.CLIENTEDOCUMENTO = CLIENTEDOCUMENTO;
			}
		}

		/**
		 * @return the CLIENTENOMBRE
		 */
		public String getCLIENTENOMBRE() {
			if (this.CLIENTENOMBRE == null) {
				return "";
			} else {
				return this.CLIENTENOMBRE;
			}
		}

		/**
		 * @param CLIENTENOMBRE the CLIENTENOMBRE to set
		 */
		public void setCLIENTENOMBRE(String CLIENTENOMBRE) {
			if (CLIENTENOMBRE != null && !CLIENTENOMBRE.equals("")) {
				this.CLIENTENOMBRE = CLIENTENOMBRE;
			}
		}

		/**
		 * @return the cLIENTEDIRECCION
		 */
		public String getcLIENTEDIRECCION() {
			if (this.CLIENTEDIRECCION == null) {
				return "";
			} else {
				return this.CLIENTEDIRECCION;
			}
		}

		/**
		 * @param cLIENTEDIRECCION the cLIENTEDIRECCION to set
		 */
		public void setcLIENTEDIRECCION(String cLIENTEDIRECCION) {
			if (cLIENTEDIRECCION != null && !cLIENTEDIRECCION.equals("")) {
				this.CLIENTEDIRECCION = cLIENTEDIRECCION;
			}
		}

		/**
		 * @return the cLIENTETELEFONO
		 */
		public String getcLIENTETELEFONO() {
			if (this.CLIENTETELEFONO == null) {
				return "";
			} else {
				return this.CLIENTETELEFONO;
			}
		}

		/**
		 * @param cLIENTETELEFONO the cLIENTETELEFONO to set
		 */
		public void setcLIENTETELEFONO(String cLIENTETELEFONO) {
			if (cLIENTETELEFONO != null && !cLIENTETELEFONO.equals("")) {
				this.CLIENTETELEFONO = cLIENTETELEFONO;
			}
		}

		/**
		 * @return the cLIENTEEMAIL
		 */
		public String getcLIENTEEMAIL() {
			if (this.CLIENTEEMAIL == null) {
				return "";
			} else {
				return this.CLIENTEEMAIL;
			}
		}

		/**
		 * @param cLIENTEEMAIL the cLIENTEEMAIL to set
		 */
		public void setcLIENTEEMAIL(String cLIENTEEMAIL) {
			if (cLIENTEEMAIL != null && !cLIENTEEMAIL.equals("")) {
				this.CLIENTEEMAIL = cLIENTEEMAIL;
			}
		}

		/**
		 * @return the CLIENTEPAISNOM
		 */
		public String getCLIENTEPAISNOM() {
			if (this.CLIENTEPAISNOM == null) {
				return "";
			} else {
				return this.CLIENTEPAISNOM;
			}
		}

		/**
		 * @param CLIENTEPAISNOM the CLIENTEPAISNOM to set
		 */
		public void setCLIENTEPAISNOM(String CLIENTEPAISNOM) {
			if (!CLIENTEPAISNOM.equals("")) {
				this.CLIENTEPAISNOM = CLIENTEPAISNOM;
			}
		}

		/**
		 * @return the VENDEDORNRO
		 */
		public long getVENDEDORNRO() {
			return VENDEDORNRO;
		}

		/**
		 * @param VENDEDORNRO the VENDEDORNRO to set
		 */
		public void setVENDEDORNRO(long VENDEDORNRO) {
			this.VENDEDORNRO = VENDEDORNRO;
		}

		/**
		 * @return the VENDEDORNOMBRE
		 */
		public String getVENDEDORNOMBRE() {
			if (this.VENDEDORNOMBRE == null) {
				return "";
			} else {
				return this.VENDEDORNOMBRE;
			}
		}

		/**
		 * @param VENDEDORNOMBRE the VENDEDORNOMBRE to set
		 */
		public void setVENDEDORNOMBRE(String VENDEDORNOMBRE) {
			if (VENDEDORNOMBRE != null && !VENDEDORNOMBRE.equals("")) {
				this.VENDEDORNOMBRE = VENDEDORNOMBRE;
			}
		}

		/**
		 * @return the TIPODENEGOCIO
		 */
		public short getTIPODENEGOCIO() {
			return TIPODENEGOCIO;
		}

		/**
		 * @param TIPODENEGOCIO the TIPODENEGOCIO to set
		 */
		public void setTIPODENEGOCIO(short TIPODENEGOCIO) {
			this.TIPODENEGOCIO = TIPODENEGOCIO;
		}

		/**
		 * @return the COTIZACIONUI
		 */
		public double getCOTIZACIONUI() {
			return COTIZACIONUI;
		}

		/**
		 * @param COTIZACIONUI the COTIZACIONUI to set
		 */
		public void setCOTIZACIONUI(double COTIZACIONUI) {
			this.COTIZACIONUI = COTIZACIONUI;
		}
	}

	public boolean EsContingencia() {
		if (this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eFacturaContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eFacturaExportacionContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eFacturaNotaCredContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eFacturaNotaDebContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eRemitoContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eRemitoExportacionContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eResguardoContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eTicketContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eTicketNotaCredContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eTicketNotaDebContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eTicketCuentaAjenaContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eTicketNotaCredCuentaAjenaContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eTicketNotaDebCuentaAjenaContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eFacturaCuentaAjenaContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eFacturaNotaCredCuentaAjenaContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eFacturaNotaDebCuentaAjenaContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == (short) enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()) {
			return true;
		} else {
			return false;
		}

	}

	public final boolean EsBoleta() {
		if (this.DGI.getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia.getValue()
				|| this.DGI.getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaCred.getValue()
				|| this.DGI.getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoletaNotaDeb.getValue()
				|| this.DGI.getIDDocTipoCFE() == enumTipoDeComprobanteCFE.eBoleta.getValue()) {
			return true;
		} else {
			return false;
		}
	}

	public String ToXML() {
		XStream xstream = new XStream(new PureJavaReflectionProvider(new FieldDictionary(new SequenceFieldKeySorter())));
		xstream.ignoreUnknownElements();
		xstream.processAnnotations(XMLFACTURA.class);
        xstream.alias("DGI", clsDGI.class);
        xstream.alias("DATOSCONTINGENCIA", clsDATOSCONTINGENCIA.class);
        xstream.alias("DATOSADICIONALES", clsDATOSADICIONALES.class);
		String resultReplaced1 = xstream.toXML(this).replace("\n", "").replace("                                ","");
		resultReplaced1 = resultReplaced1.replace("                  ","").replace("                ","");
		resultReplaced1 = resultReplaced1.replace("              ","").replace("          ","").replace("        ","");
		resultReplaced1 = resultReplaced1.replace("      ","");
		resultReplaced1 = resultReplaced1.replace("    ","").replace("  ","");
		resultReplaced1 = resultReplaced1.replace("<XMLENTRADA>","<XMLENTRADA xmlns=\"TAFACE\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
        return  resultReplaced1;
	}

	public XMLFACTURA LoadXML(String XML) {
		XStream xstream = new XStream(new PureJavaReflectionProvider(new FieldDictionary(new SequenceFieldKeySorter())));
		String[] formats = {"MM-dd-yyyy"};
		xstream.registerConverter(new DateConverter("yyyy-MM-dd", formats));
		//xstream.ignoreUnknownElements();
		xstream.processAnnotations(XMLFACTURA.class);
		xstream.autodetectAnnotations(true);
		//converting xml to object
		XMLFACTURA myObject = (XMLFACTURA) xstream.fromXML(XML);

		return myObject;
	}

}