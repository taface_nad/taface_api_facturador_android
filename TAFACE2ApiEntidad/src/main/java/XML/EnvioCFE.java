package XML;

//------------------------------------------------------------------------------
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.thoughtworks.xstream.converters.basic.DateConverter;
import com.thoughtworks.xstream.converters.reflection.FieldDictionary;
import com.thoughtworks.xstream.converters.reflection.PureJavaReflectionProvider;
import com.thoughtworks.xstream.converters.reflection.Sun14ReflectionProvider;
import com.thoughtworks.xstream.io.xml.Dom4JDriver;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;

@XMLSequence({
        "caratula",
        "cFE",
        "version"
})


public class EnvioCFE implements java.io.Serializable{

    private EnvioCFECaratula caratula;
    @XStreamImplicit(itemFieldName = "CFE")
    private List<CFEDefType> cFE;
    private String version = null;

    public EnvioCFE() {
        super();
        this.version = "1.0";
    }
	
    public EnvioCFECaratula getCaratula() {
        return this.caratula;
    }

    public void setCaratula(EnvioCFECaratula value) {
        this.caratula = value;
    }

    public List<CFEDefType> getCFE() {
        return this.cFE;
    }

    public void setCFE(List<CFEDefType> value) {
        this.cFE = value;
    }

    public String getversion() {
        if (this.version == null) {
            return "";
        } else {
            return this.version;
        }
    }

    public void setversion(String value) {
        if (value != null && !value.equals("")) {
            this.version = value;
        }
    }

    @XMLSequence({"RutReceptor", "RUCEmisor", "Idemisor", "CantCFE", "Fecha", "X509Certificate", "Version"})
    public class EnvioCFECaratula {

        private String RutReceptor = null;
        private String RUCEmisor = null;
        private String Idemisor = null;
        private String CantCFE = null;
        private java.util.Date Fecha = new java.util.Date(0);
        private byte[] X509Certificate;
        private String Version = null;

        public EnvioCFECaratula() {
            super();
            this.Version = "1.0";
        }

        public String getRutReceptor() {
            if (this.RutReceptor == null) {
                return "";
            } else {
                return this.RutReceptor;
            }
        }

        public void setRutReceptor(String value) {
            if (value != null && !value.equals("")) {
                this.RutReceptor = value;
            }
        }

        public String getRUCEmisor() {
            if (this.RUCEmisor == null) {
                return "";
            } else {
                return this.RUCEmisor;
            }
        }

        public void setRUCEmisor(String value) {
            if (value != null && !value.equals("")) {
                this.RUCEmisor = value;
            }
        }

        public String getIdemisor() {
            if (this.Idemisor == null) {
                return "";
            } else {
                return this.Idemisor;
            }
        }

        public void setIdemisor(String value) {
            if (value != null && !value.equals("")) {
                this.Idemisor = value;
            }
        }

        public String getCantCFE() {
            if (this.CantCFE == null) {
                return "";
            } else {
                return this.CantCFE;
            }
        }

        public void setCantCFE(String value) {
            if (value != null && !value.equals("")) {
                this.CantCFE = value;
            }
        }

        public java.util.Date getFecha() {
            return this.Fecha;
        }

        public void setFecha(java.util.Date value) {
            this.Fecha = value;
        }

        public byte[] getX509Certificate() {
            return this.X509Certificate;
        }

        public void setX509Certificate(byte[] value) {
            this.X509Certificate = value;
        }

        public String getversion() {
            if (this.Version == null) {
                return "";
            } else {
                return this.Version;
            }
        }

        public void setversion(String value) {
            if (value != null && !value.equals("")) {
                this.Version = value;
            }
        }
    }

    @XStreamAlias("CFEDefType")
    @XMLSequence({"item", "signature", "version", "typeCFEDefined"})
    public class CFEDefType implements java.io.Serializable{

        @XStreamAsAttribute
        private CFEDefTypesHeritage item;
        private SignatureType signature;
        @XStreamOmitField
        private String version = null;
        @XStreamOmitField
        private String typeCFEDefined;

        public CFEDefType() {
            super();
            this.version = "1.0";
        }

        public CFEDefTypesHeritage getItem() {
            if (item.getClass() == CFEDefTypeETck.class) {
                typeCFEDefined = "eTck";
                return (CFEDefTypeETck) item;
            }
            if (item.getClass() == CFEDefTypeEFact.class) {
                typeCFEDefined = "eFact";
                return (CFEDefTypeEFact) item;
            }
            if (item.getClass() == CFEDefTypeERem.class) {
                typeCFEDefined = "eRem";
                return (CFEDefTypeERem) item;
            }
            if (item.getClass() == CFEDefTypeEResg.class) {
                typeCFEDefined = "eResg";
                return (CFEDefTypeEResg) item;
            }
            if (item.getClass() == CFEDefTypeEFact_Exp.class) {
                typeCFEDefined = "eFact_Exp";
                return (CFEDefTypeEFact_Exp) item;
            }
            if (item.getClass() == CFEDefTypeERem_Exp.class) {
                typeCFEDefined = "eRem_Exp";
                return (CFEDefTypeERem_Exp) item;
            }
            if (item.getClass() == CFEDefTypeEBoleta.class) {
                typeCFEDefined = "eBoleta";
                return (CFEDefTypeEBoleta) item;
            } else {
                return null;
            }
        }

        public void setItem(Object value) {
            this.item = (CFEDefTypesHeritage) value;
        }

        public SignatureType getSignature() {
            return this.signature;
        }

        public void setSignature(SignatureType value) {
            this.signature = value;
        }

        public String getversion() {
            if (this.version == null) {
                return "";
            } else {
                return this.version;
            }
        }

        public void setversion(String value) {
            if (value != null && !value.equals("")) {
                this.version = value;
            }
        }

        public String ToXML() throws JAXBException, IOException {

            //new PureJavaReflectionProvider(new FieldDictionary(new SequenceFieldKeySorter()))
            XStream xstream = new XStream(new PureJavaReflectionProvider(new FieldDictionary(new SequenceFieldKeySorter())));
            xstream.ignoreUnknownElements();
            xstream.processAnnotations(EnvioCFE.class);
            String[] formats = {"MM-dd-yyyy"};
            xstream.registerConverter(new DateConverter("yyyy-MM-dd", formats));
            xstream.autodetectAnnotations(true);
            String xstreamed = xstream.toXML(this);
            String xml = xstreamed.toString();
            xml = xml.substring(0, xml.length() - 14);
            String result = replaceMissedTagsXML(replaceMissedTagsXML(xstreamed).replace("CFEDefType", typeCFEDefined));
            return "<CFE version='" + getversion().trim() + "'>" + result.trim() + "</CFE>";
        }

        public CFEDefType LoadXML(String XML) throws JAXBException {
            XStream xstream = new XStream(new PureJavaReflectionProvider(new FieldDictionary(new SequenceFieldKeySorter())));
            String[] formats = {"MM-dd-yyyy"};
            xstream.registerConverter(new DateConverter("yyyy-MM-dd", formats));
            //xstream.ignoreUnknownElements();
            xstream.processAnnotations(EnvioCFE.class);
            xstream.autodetectAnnotations(true);
            //converting xml to object
            CFEDefType myObject = (CFEDefType) xstream.fromXML(XML);

            return myObject;
        }
    }

    /////////////TIPOS CFE CLASS//////////////////////
    @XMLSequence({"TmstFirma", "Encabezado", "SubTotInfo", "DscRcgGlobal", "MediosPago", "Referencia", "CAEData", "Compl_Fiscal"})
    public class CFEDefTypesHeritage  implements java.io.Serializable{

        @XStreamAlias("TmstFirma")
        private String tmstFirma = null;
        @XStreamAlias("Encabezado")
        private CFEDefTypeEncabezado encabezado;
        @XStreamAlias("SubTotInfo")
        private SubTotInfoSTI_Item subTotInfo;
        @XStreamAlias("DscRcgGlobal")
        private DscRcgGlobalDRG_Item dscRcgGlobal;
        @XStreamAlias("MediosPago")
        private MediosPagoMedioPago mediosPago;
        @XStreamAlias("Referencia")
        private ReferenciaReferencia referencia;
        @XStreamAlias("CAEData")
        private CAEDataType cAEData;
        @XStreamAlias("Compl_Fiscal")
        private Compl_FiscalType compl_Fiscal;

        public String getTmstFirma() {
            return tmstFirma;
        }

        public void setTmstFirma(String value) {
            tmstFirma = value;
        }

        public CFEDefTypeEncabezado getEncabezado() {
            return encabezado;
        }

        public void setEncabezado(CFEDefTypeEncabezado value) {
            encabezado = value;
        }

        public SubTotInfoSTI_Item getSubTotInfo() {
            return subTotInfo;
        }

        public void setSubTotInfo(SubTotInfoSTI_Item value) {
            subTotInfo = value;
        }

        public DscRcgGlobalDRG_Item getDscRcgGlobal() {
            return dscRcgGlobal;
        }

        public void setDscRcgGlobal(DscRcgGlobalDRG_Item value) {
            dscRcgGlobal = value;
        }

        public MediosPagoMedioPago getMediosPago() {
            return mediosPago;
        }

        public void setMediosPago(MediosPagoMedioPago value) {
            mediosPago = value;
        }

        public ReferenciaReferencia getReferencia() {
            return referencia;
        }

        public void setReferencia(ReferenciaReferencia value) {
            referencia = value;
        }

        public CAEDataType getCAEData() {
            return cAEData;
        }

        public void setCAEData(CAEDataType value) {
            cAEData = value;
        }

        public Compl_FiscalType getCompl_Fiscal() {
            return compl_Fiscal;
        }

        public void setCompl_Fiscal(Compl_FiscalType value) {
            compl_Fiscal = value;
        }
    }

    @XMLSequence({"TmstFirma", "Encabezado", "Detalle", "SubTotInfo", "DscRcgGlobal", "MediosPago", "Referencia", "CAEData", "Compl_Fiscal"})
    public class CFEDefTypeEFact extends CFEDefTypesHeritage  implements java.io.Serializable{

        @XStreamAlias("TmstFirma")
        private String tmstFirma = null;
        @XStreamAlias("Encabezado")
        private CFEDefTypeEFactEncabezado encabezado;
        @XStreamAlias("Detalle")
        private Item_Det_Fact detalle;
        @XStreamAlias("SubTotInfo")
        private SubTotInfoSTI_Item subTotInfo;
        @XStreamAlias("DscRcgGlobal")
        private DscRcgGlobalDRG_Item dscRcgGlobal;
        @XStreamAlias("MediosPago")
        private MediosPagoMedioPago mediosPago;
        @XStreamAlias("Referencia")
        private ReferenciaReferencia referencia;
        @XStreamAlias("CAEData")
        private CAEDataType cAEData;
        @XStreamAlias("Compl_Fiscal")
        private Compl_FiscalType compl_Fiscal;

        public CFEDefTypeEFact() {
            super();
        }

        @Override
        public String getTmstFirma() {
            if (this.tmstFirma == null) {
                return "";
            } else {
                return this.tmstFirma;
            }
        }

        @Override
        public void setTmstFirma(String value) {
            if (value != null && !value.equals("")) {
                this.tmstFirma = value;
            }
        }

        @Override
        public CFEDefTypeEFactEncabezado getEncabezado() {
            return this.encabezado;
        }

        public void setEncabezado(CFEDefTypeEFactEncabezado value) {
            this.encabezado = value;
        }

        public Item_Det_Fact getDetalle() {
            return this.detalle;
        }

        public void setDetalle(Item_Det_Fact value) {
            this.detalle = value;
        }

        @Override
        public SubTotInfoSTI_Item getSubTotInfo() {
            return this.subTotInfo;
        }

        @Override
        public void setSubTotInfo(SubTotInfoSTI_Item value) {
            this.subTotInfo = value;
        }

        @Override
        public DscRcgGlobalDRG_Item getDscRcgGlobal() {
            return this.dscRcgGlobal;
        }

        @Override
        public void setDscRcgGlobal(DscRcgGlobalDRG_Item value) {
            this.dscRcgGlobal = value;
        }

        @Override
        public MediosPagoMedioPago getMediosPago() {
            return this.mediosPago;
        }

        @Override
        public void setMediosPago(MediosPagoMedioPago value) {
            this.mediosPago = value;
        }

        @Override
        public ReferenciaReferencia getReferencia() {
            return this.referencia;
        }

        @Override
        public void setReferencia(ReferenciaReferencia value) {
            this.referencia = value;
        }

        @Override
        public CAEDataType getCAEData() {
            return this.cAEData;
        }

        @Override
        public void setCAEData(CAEDataType value) {
            this.cAEData = value;
        }

        @Override
        public Compl_FiscalType getCompl_Fiscal() {
            return this.compl_Fiscal;
        }

        @Override
        public void setCompl_Fiscal(Compl_FiscalType value) {
            this.compl_Fiscal = value;
        }
    }

    @XMLSequence({"TmstFirma", "Encabezado", "Detalle", "SubTotInfo", "DscRcgGlobal", "MediosPago", "Referencia", "CAEData", "Compl_Fiscal"})
    public class CFEDefTypeETck extends CFEDefTypesHeritage  implements java.io.Serializable{

        @XStreamAlias("TmstFirma")
        private String tmstFirma = null;
        @XStreamAlias("Encabezado")
        private CFEDefTypeETckEncabezado encabezado;
        @XStreamAlias("Detalle")
        private Item_Det_Fact detalle;
        @XStreamAlias("SubTotInfo")
        private SubTotInfoSTI_Item subTotInfo;
        @XStreamAlias("DscRcgGlobal")
        private DscRcgGlobalDRG_Item dscRcgGlobal;
        @XStreamAlias("MediosPago")
        private MediosPagoMedioPago mediosPago;
        @XStreamAlias("Referencia")
        private ReferenciaReferencia referencia;
        @XStreamAlias("CAEData")
        private CAEDataType cAEData;
        @XStreamAlias("Compl_Fiscal")
        private Compl_FiscalType compl_Fiscal;

        @Override
        public String getTmstFirma() {
            if (this.tmstFirma == null) {
                return "";
            } else {
                return this.tmstFirma;
            }
        }

        @Override
        public void setTmstFirma(String value) {
            if (value != null && !value.equals("")) {
                this.tmstFirma = value;
            }
        }

        @Override
        public CFEDefTypeETckEncabezado getEncabezado() {
            return this.encabezado;
        }

        public void setEncabezado(CFEDefTypeETckEncabezado value) {
            this.encabezado = value;
        }

        public Item_Det_Fact getDetalle() {
            return this.detalle;
        }

        public void setDetalle(Item_Det_Fact value) {
            this.detalle = value;
        }

        @Override
        public SubTotInfoSTI_Item getSubTotInfo() {
            return this.subTotInfo;
        }

        @Override
        public void setSubTotInfo(SubTotInfoSTI_Item value) {
            this.subTotInfo = value;
        }

        @Override
        public DscRcgGlobalDRG_Item getDscRcgGlobal() {
            return this.dscRcgGlobal;
        }

        @Override
        public void setDscRcgGlobal(DscRcgGlobalDRG_Item value) {
            this.dscRcgGlobal = value;
        }

        @Override
        public MediosPagoMedioPago getMediosPago() {
            return this.mediosPago;
        }

        @Override
        public void setMediosPago(MediosPagoMedioPago value) {
            this.mediosPago = value;
        }

        @Override
        public ReferenciaReferencia getReferencia() {
            return this.referencia;
        }

        @Override
        public void setReferencia(ReferenciaReferencia value) {
            this.referencia = value;
        }

        @Override
        public CAEDataType getCAEData() {
            return this.cAEData;
        }

        @Override
        public void setCAEData(CAEDataType value) {
            this.cAEData = value;
        }

        @Override
        public Compl_FiscalType getCompl_Fiscal() {
            return this.compl_Fiscal;
        }

        @Override
        public void setCompl_Fiscal(Compl_FiscalType value) {
            this.compl_Fiscal = value;
        }
    }

    @XMLSequence({"TmstFirma", "Encabezado", "Detalle", "SubTotInfo", "Referencia", "CAEData"})
    public class CFEDefTypeERem extends CFEDefTypesHeritage  implements java.io.Serializable{

        @XStreamAlias("TmstFirma")
        private String tmstFirma = null;
        @XStreamAlias("Encabezado")
        private CFEDefTypeERemEncabezado encabezado;
        @XStreamAlias("Detalle")
        private Item_Rem detalle;
        @XStreamAlias("SubTotInfo")
        private SubTotInfoSTI_Item subTotInfo;
        @XStreamAlias("Referencia")
        private ReferenciaReferencia referencia;
        @XStreamAlias("CAEData")
        private CAEDataType cAEData;

        @Override
        public String getTmstFirma() {
            if (this.tmstFirma == null) {
                return "";
            } else {
                return this.tmstFirma;
            }
        }

        @Override
        public void setTmstFirma(String value) {
            if (value != null && !value.equals("")) {
                this.tmstFirma = value;
            }
        }

        @Override
        public CFEDefTypeERemEncabezado getEncabezado() {
            return this.encabezado;
        }

        public void setEncabezado(CFEDefTypeERemEncabezado value) {
            this.encabezado = value;
        }

        public Item_Rem getDetalle() {
            return this.detalle;
        }

        public void setDetalle(Item_Rem value) {
            this.detalle = value;
        }

        @Override
        public SubTotInfoSTI_Item getSubTotInfo() {
            return this.subTotInfo;
        }

        @Override
        public void setSubTotInfo(SubTotInfoSTI_Item value) {
            this.subTotInfo = value;
        }

        @Override
        public ReferenciaReferencia getReferencia() {
            return this.referencia;
        }

        @Override
        public void setReferencia(ReferenciaReferencia value) {
            this.referencia = value;
        }

        @Override
        public CAEDataType getCAEData() {
            return this.cAEData;
        }

        @Override
        public void setCAEData(CAEDataType value) {
            this.cAEData = value;
        }
    }

    @XMLSequence({"TmstFirma", "Encabezado", "Detalle", "SubTotInfo", "Referencia", "CAEData"})
    public class CFEDefTypeEResg extends CFEDefTypesHeritage  implements java.io.Serializable{

        @XStreamAlias("TmstFirma")
        private String tmstFirma = null;
        @XStreamAlias("Encabezado")
        private CFEDefTypeEResgEncabezado encabezado;
        @XStreamAlias("Detalle")
        private Item_Resg detalle;
        @XStreamAlias("SubTotInfo")
        private SubTotInfoSTI_Item subTotInfo;
        @XStreamAlias("Referencia")
        private ReferenciaReferencia referencia;
        @XStreamAlias("CAEData")
        private CAEDataType cAEData;

        @Override
        public String getTmstFirma() {
            if (this.tmstFirma == null) {
                return "";
            } else {
                return this.tmstFirma;
            }
        }

        @Override
        public void setTmstFirma(String value) {
            if (value != null && !value.equals("")) {
                this.tmstFirma = value;
            }
        }

        @Override
        public CFEDefTypeEResgEncabezado getEncabezado() {
            return this.encabezado;
        }

        public void setEncabezado(CFEDefTypeEResgEncabezado value) {
            this.encabezado = value;
        }

        public Item_Resg getDetalle() {
            return this.detalle;
        }

        public void setDetalle(Item_Resg value) {
            this.detalle = value;
        }

        @Override
        public SubTotInfoSTI_Item getSubTotInfo() {
            return this.subTotInfo;
        }

        @Override
        public void setSubTotInfo(SubTotInfoSTI_Item value) {
            this.subTotInfo = value;
        }

        @Override
        public ReferenciaReferencia getReferencia() {
            return this.referencia;
        }

        @Override
        public void setReferencia(ReferenciaReferencia value) {
            this.referencia = value;
        }

        @Override
        public CAEDataType getCAEData() {
            return this.cAEData;
        }

        @Override
        public void setCAEData(CAEDataType value) {
            this.cAEData = value;
        }
    }

    @XMLSequence({"TmstFirma", "Encabezado", "Detalle", "SubTotInfo", "Referencia", "MediosPago", "DscRcgGlobal", "CAEData"})
    public class CFEDefTypeEFact_Exp extends CFEDefTypesHeritage  implements java.io.Serializable{

        @XStreamAlias("TmstFirma")
        private String tmstFirma = null;
        @XStreamAlias("Encabezado")
        private CFEDefTypeEFact_ExpEncabezado encabezado;
        @XStreamAlias("Detalle")
        private Item_Det_Fact_Exp detalle;
        @XStreamAlias("SubTotInfo")
        private SubTotInfoSTI_Item subTotInfo;
        @XStreamAlias("Referencia")
        private ReferenciaReferencia referencia;
        @XStreamAlias("MediosPago")
        private MediosPagoMedioPago mediosPago;
        @XStreamAlias("DscRcgGlobal")
        private DscRcgGlobalDRG_Item dscRcgGlobal;
        @XStreamAlias("CAEData")
        private CAEDataType cAEData;

        @Override
        public String getTmstFirma() {
            if (this.tmstFirma == null) {
                return "";
            } else {
                return this.tmstFirma;
            }
        }

        @Override
        public void setTmstFirma(String value) {
            if (value != null && !value.equals("")) {
                this.tmstFirma = value;
            }
        }

        @Override
        public CFEDefTypeEFact_ExpEncabezado getEncabezado() {
            return this.encabezado;
        }

        public void setEncabezado(CFEDefTypeEFact_ExpEncabezado value) {
            this.encabezado = value;
        }

        public Item_Det_Fact_Exp getDetalle() {
            return this.detalle;
        }

        public void setDetalle(Item_Det_Fact_Exp value) {
            this.detalle = value;
        }

        @Override
        public SubTotInfoSTI_Item getSubTotInfo() {
            return this.subTotInfo;
        }

        @Override
        public void setSubTotInfo(SubTotInfoSTI_Item value) {
            this.subTotInfo = value;
        }

        @Override
        public DscRcgGlobalDRG_Item getDscRcgGlobal() {
            return this.dscRcgGlobal;
        }

        @Override
        public void setDscRcgGlobal(DscRcgGlobalDRG_Item value) {
            this.dscRcgGlobal = value;
        }

        @Override
        public MediosPagoMedioPago getMediosPago() {
            return this.mediosPago;
        }

        @Override
        public void setMediosPago(MediosPagoMedioPago value) {
            this.mediosPago = value;
        }

        @Override
        public ReferenciaReferencia getReferencia() {
            return this.referencia;
        }

        @Override
        public void setReferencia(ReferenciaReferencia value) {
            this.referencia = value;
        }

        @Override
        public CAEDataType getCAEData() {
            return this.cAEData;
        }

        @Override
        public void setCAEData(CAEDataType value) {
            this.cAEData = value;
        }
    }

    @XMLSequence({"TmstFirma", "Encabezado", "Detalle", "SubTotInfo", "Referencia", "CAEData"})
    public class CFEDefTypeERem_Exp extends CFEDefTypesHeritage  implements java.io.Serializable{

        @XStreamAlias("TmstFirma")
        private String tmstFirma = null;
        @XStreamAlias("Encabezado")
        private CFEDefTypeERem_ExpEncabezado encabezado;
        @XStreamAlias("Detalle")
        private Item_Rem_Exp detalle;
        @XStreamAlias("SubTotInfo")
        private SubTotInfoSTI_Item subTotInfo;
        @XStreamAlias("Referencia")
        private ReferenciaReferencia referencia;
        @XStreamAlias("CAEData")
        private CAEDataType cAEData;

        @Override
        public String getTmstFirma() {
            if (this.tmstFirma == null) {
                return "";
            } else {
                return this.tmstFirma;
            }
        }

        @Override
        public void setTmstFirma(String value) {
            if (value != null && !value.equals("")) {
                this.tmstFirma = value;
            }
        }

        @Override
        public CFEDefTypeERem_ExpEncabezado getEncabezado() {
            return this.encabezado;
        }

        public void setEncabezado(CFEDefTypeERem_ExpEncabezado value) {
            this.encabezado = value;
        }

        public Item_Rem_Exp getDetalle() {
            return this.detalle;
        }

        public void setDetalle(Item_Rem_Exp value) {
            this.detalle = value;
        }

        @Override
        public SubTotInfoSTI_Item getSubTotInfo() {
            return this.subTotInfo;
        }

        @Override
        public void setSubTotInfo(SubTotInfoSTI_Item value) {
            this.subTotInfo = value;
        }

        @Override
        public ReferenciaReferencia getReferencia() {
            return this.referencia;
        }

        @Override
        public void setReferencia(ReferenciaReferencia value) {
            this.referencia = value;
        }

        @Override
        public CAEDataType getCAEData() {
            return this.cAEData;
        }

        @Override
        public void setCAEData(CAEDataType value) {
            this.cAEData = value;
        }
    }

    @XMLSequence({"TmstFirma", "Encabezado", "Detalle", "SubTotInfo", "DscRcgGlobal", "MediosPago", "Referencia", "CAEData", "Compl_Fiscal"})
    public class CFEDefTypeEBoleta extends CFEDefTypesHeritage  implements java.io.Serializable{

        @XStreamAlias("TmstFirma")
        private String tmstFirma = null;
        @XStreamAlias("Encabezado")
        private CFEDefTypeEBoletaEncabezado encabezado;
        @XStreamAlias("Detalle")
        private Item_Det_Boleta detalle;
        @XStreamAlias("SubTotInfo")
        private SubTotInfoSTI_Item subTotInfo;
        @XStreamAlias("DscRcgGlobal")
        private DscRcgGlobalDRG_Item dscRcgGlobal;
        @XStreamAlias("MediosPago")
        private MediosPagoMedioPago mediosPago;
        @XStreamAlias("Referencia")
        private ReferenciaReferencia referencia;
        @XStreamAlias("CAEData")
        private CAEDataType cAEData;
        @XStreamAlias("Compl_Fiscal")
        private Compl_FiscalType compl_Fiscal;

        @Override
        public String getTmstFirma() {
            if (this.tmstFirma == null) {
                return "";
            } else {
                return this.tmstFirma;
            }
        }

        @Override
        public void setTmstFirma(String value) {
            if (value != null && !value.equals("")) {
                this.tmstFirma = value;
            }
        }

        @Override
        public CFEDefTypeEBoletaEncabezado getEncabezado() {
            return this.encabezado;
        }

        public void setEncabezado(CFEDefTypeEBoletaEncabezado value) {
            this.encabezado = value;
        }

        public Item_Det_Boleta getDetalle() {
            return this.detalle;
        }

        public void setDetalle(Item_Det_Boleta value) {
            this.detalle = value;
        }

        @Override
        public SubTotInfoSTI_Item getSubTotInfo() {
            return this.subTotInfo;
        }

        @Override
        public void setSubTotInfo(SubTotInfoSTI_Item value) {
            this.subTotInfo = value;
        }

        @Override
        public DscRcgGlobalDRG_Item getDscRcgGlobal() {
            return this.dscRcgGlobal;
        }

        @Override
        public void setDscRcgGlobal(DscRcgGlobalDRG_Item value) {
            this.dscRcgGlobal = value;
        }

        @Override
        public MediosPagoMedioPago getMediosPago() {
            return this.mediosPago;
        }

        @Override
        public void setMediosPago(MediosPagoMedioPago value) {
            this.mediosPago = value;
        }

        @Override
        public ReferenciaReferencia getReferencia() {
            return this.referencia;
        }

        @Override
        public void setReferencia(ReferenciaReferencia value) {
            this.referencia = value;
        }

        @Override
        public CAEDataType getCAEData() {
            return this.cAEData;
        }

        @Override
        public void setCAEData(CAEDataType value) {
            this.cAEData = value;
        }

        @Override
        public Compl_FiscalType getCompl_Fiscal() {
            return this.compl_Fiscal;
        }

        @Override
        public void setCompl_Fiscal(Compl_FiscalType value) {
            this.compl_Fiscal = value;
        }
    }

    ////////////////ENCABEZADOS CFE CLASS////////////////////////////////
    @XMLSequence({"IdDoc", "Emisor", "Receptor", "Totales"})
    public class CFEDefTypeEncabezado  implements java.io.Serializable{

        @XStreamAlias("IdDoc")
        private IdDoc idDoc;
        @XStreamAlias("Emisor")
        private Emisor emisor;
        @XStreamAlias("Receptor")
        private Object receptor;
        @XStreamAlias("Totales")
        private Object totales;

        public IdDoc getIdDoc() {
            return this.idDoc;
        }

        public void setIdDoc(IdDoc value) {
            this.idDoc = value;
        }

        public Emisor getEmisor() {
            return this.emisor;
        }

        public void setEmisor(Emisor value) {
            this.emisor = value;
        }

        public Object getReceptor() {
            return this.receptor;
        }

        public void setReceptor(Object value) {
            this.receptor = value;
        }

        public Object getTotales() {
            return this.totales;
        }

        public void setTotales(Object value) {
            this.totales = value;
        }
    }

    @XMLSequence({"IdDoc", "Emisor", "Receptor", "Totales"})
    public class CFEDefTypeETckEncabezado extends CFEDefTypeEncabezado  implements java.io.Serializable{

        @XStreamAlias("IdDoc")
        private IdDoc_Tck idDoc;
        @XStreamAlias("Emisor")
        private Emisor emisor;
        @XStreamAlias("Receptor")
        private Receptor_Tck receptor;
        @XStreamAlias("Totales")
        private Totales totales;

        @Override
        public IdDoc_Tck getIdDoc() {
            return this.idDoc;
        }

        public void setIdDoc(IdDoc_Tck value) {
            this.idDoc = value;
        }

        @Override
        public Emisor getEmisor() {
            return this.emisor;
        }

        @Override
        public void setEmisor(Emisor value) {
            this.emisor = value;
        }

        @Override
        public Receptor_Tck getReceptor() {
            return this.receptor;
        }

        public void setReceptor(Receptor_Tck value) {
            this.receptor = value;
        }

        @Override
        public Totales getTotales() {
            return this.totales;
        }

        public void setTotales(Totales value) {
            this.totales = value;
        }
    }

    @XMLSequence({"IdDoc", "Emisor", "Receptor", "Totales"})
    public class CFEDefTypeEFactEncabezado extends CFEDefTypeEncabezado  implements java.io.Serializable{

        @XStreamAlias("IdDoc")
        private IdDoc_Fact idDoc;
        @XStreamAlias("Emisor")
        private Emisor emisor;
        @XStreamAlias("Receptor")
        private Receptor_Fact receptor;
        @XStreamAlias("Totales")
        private Totales totales;

        @Override
        public IdDoc_Fact getIdDoc() {
            return this.idDoc;
        }

        public void setIdDoc(IdDoc_Fact value) {
            this.idDoc = value;
        }

        @Override
        public Emisor getEmisor() {
            return this.emisor;
        }

        @Override
        public void setEmisor(Emisor value) {
            this.emisor = value;
        }

        @Override
        public Receptor_Fact getReceptor() {
            return this.receptor;
        }

        public void setReceptor(Receptor_Fact value) {
            this.receptor = value;
        }

        @Override
        public Totales getTotales() {
            return this.totales;
        }

        public void setTotales(Totales value) {
            this.totales = value;
        }
    }

    @XMLSequence({"IdDoc", "Emisor", "Receptor", "Totales"})
    public class CFEDefTypeEFact_ExpEncabezado extends CFEDefTypeEncabezado  implements java.io.Serializable{

        @XStreamAlias("IdDoc")
        private IdDoc_Fact_Exp idDoc;
        @XStreamAlias("Emisor")
        private Emisor emisor;
        @XStreamAlias("Receptor")
        private Receptor_Fact_Exp receptor;
        @XStreamAlias("Totales")
        private Totales_Fact_Exp totales;

        @Override
        public IdDoc_Fact_Exp getIdDoc() {
            return this.idDoc;
        }

        public void setIdDoc(IdDoc_Fact_Exp value) {
            this.idDoc = value;
        }

        @Override
        public Emisor getEmisor() {
            return this.emisor;
        }

        @Override
        public void setEmisor(Emisor value) {
            this.emisor = value;
        }

        @Override
        public Receptor_Fact_Exp getReceptor() {
            return this.receptor;
        }

        public void setReceptor(Receptor_Fact_Exp value) {
            this.receptor = value;
        }

        @Override
        public Totales_Fact_Exp getTotales() {
            return this.totales;
        }

        public void setTotales(Totales_Fact_Exp value) {
            this.totales = value;
        }
    }

    @XMLSequence({"IdDoc", "Emisor", "Receptor", "Totales"})
    public class CFEDefTypeERemEncabezado extends CFEDefTypeEncabezado  implements java.io.Serializable{

        @XStreamAlias("IdDoc")
        private IdDoc_Rem idDoc;
        @XStreamAlias("Emisor")
        private Emisor emisor;
        @XStreamAlias("Receptor")
        private Receptor_Rem receptor;
        @XStreamAlias("Totales")
        private CFEDefTypeERemEncabezadoTotales totales;

        @Override
        public IdDoc_Rem getIdDoc() {
            return this.idDoc;
        }

        public void setIdDoc(IdDoc_Rem value) {
            this.idDoc = value;
        }

        @Override
        public Emisor getEmisor() {
            return this.emisor;
        }

        @Override
        public void setEmisor(Emisor value) {
            this.emisor = value;
        }

        @Override
        public Receptor_Rem getReceptor() {
            return this.receptor;
        }

        public void setReceptor(Receptor_Rem value) {
            this.receptor = value;
        }

        @Override
        public CFEDefTypeERemEncabezadoTotales getTotales() {
            return this.totales;
        }

        public void setTotales(CFEDefTypeERemEncabezadoTotales value) {
            this.totales = value;
        }
    }

    @XMLSequence({"IdDoc", "Emisor", "Receptor", "Totales"})
    public class CFEDefTypeERem_ExpEncabezado extends CFEDefTypeEncabezado  implements java.io.Serializable{

        @XStreamAlias("IdDoc")
        private IdDoc_Rem_Exp idDoc;
        @XStreamAlias("Emisor")
        private Emisor emisor;
        @XStreamAlias("Receptor")
        private Receptor_Rem_Exp receptor;
        @XStreamAlias("Totales")
        private Totales_Rem_Exp totales;

        @Override
        public IdDoc_Rem_Exp getIdDoc() {
            return this.idDoc;
        }

        public void setIdDoc(IdDoc_Rem_Exp value) {
            this.idDoc = value;
        }

        @Override
        public Emisor getEmisor() {
            return this.emisor;
        }

        @Override
        public void setEmisor(Emisor value) {
            this.emisor = value;
        }

        @Override
        public Receptor_Rem_Exp getReceptor() {
            return this.receptor;
        }

        public void setReceptor(Receptor_Rem_Exp value) {
            this.receptor = value;
        }

        @Override
        public Totales_Rem_Exp getTotales() {
            return this.totales;
        }

        public void setTotales(Totales_Rem_Exp value) {
            this.totales = value;
        }
    }

    @XMLSequence({"IdDoc", "Emisor", "Receptor", "Totales"})
    public class CFEDefTypeEResgEncabezado extends CFEDefTypeEncabezado  implements java.io.Serializable{

        @XStreamAlias("IdDoc")
        private IdDoc_Resg idDoc;
        @XStreamAlias("Emisor")
        private Emisor emisor;
        @XStreamAlias("Receptor")
        private Receptor_Resg receptor;
        @XStreamAlias("Totales")
        private Totales_Resg totales;

        @Override
        public IdDoc_Resg getIdDoc() {
            return this.idDoc;
        }

        public void setIdDoc(IdDoc_Resg value) {
            this.idDoc = value;
        }

        @Override
        public Emisor getEmisor() {
            return this.emisor;
        }

        @Override
        public void setEmisor(Emisor value) {
            this.emisor = value;
        }

        @Override
        public Receptor_Resg getReceptor() {
            return this.receptor;
        }

        public void setReceptor(Receptor_Resg value) {
            this.receptor = value;
        }

        @Override
        public Totales_Resg getTotales() {
            return this.totales;
        }

        public void setTotales(Totales_Resg value) {
            this.totales = value;
        }
    }

    @XMLSequence({"IdDoc", "Emisor", "Receptor", "Totales"})
    public class CFEDefTypeEBoletaEncabezado extends CFEDefTypeEncabezado  implements java.io.Serializable{

        @XStreamAlias("IdDoc")
        private IdDoc_Boleta idDoc;
        @XStreamAlias("Emisor")
        private Emisor emisor;
        @XStreamAlias("Receptor")
        private Receptor_Tck receptor;
        @XStreamAlias("Totales")
        private Totales_Boleta totales;

        @Override
        public IdDoc_Boleta getIdDoc() {
            return this.idDoc;
        }

        public void setIdDoc(IdDoc_Boleta value) {
            this.idDoc = value;
        }

        @Override
        public Emisor getEmisor() {
            return this.emisor;
        }

        @Override
        public void setEmisor(Emisor value) {
            this.emisor = value;
        }

        @Override
        public Receptor_Tck getReceptor() {
            return this.receptor;
        }

        public void setReceptor(Receptor_Tck value) {
            this.receptor = value;
        }

        @Override
        public Totales_Boleta getTotales() {
            return this.totales;
        }

        public void setTotales(Totales_Boleta value) {
            this.totales = value;
        }
    }

    public class CFEDefTypeERemEncabezadoTotales  implements java.io.Serializable{

        private String CantLinDet;

        public String getCantLinDet() {
            return this.CantLinDet;
        }

        public void setCantLinDet(String value) {
            this.CantLinDet = value;
        }
    }

    /////////DETALLE ITEM CLASS////////////////
	@XStreamAlias("Detalle")
	public class Item_Rem  implements java.io.Serializable{
		@XStreamImplicit(itemFieldName = "Item")
		 private List<Item_Rem.Item> Item = new ArrayList<Item_Rem.Item>();

		public List<Item_Rem.Item> getItem() {
			return Item;
		}

		public void setItem(List<Item_Rem.Item> Item) {
			this.Item = Item;
		}

        @XMLSequence({"NroLinDet", "CodItem", "IndFact", "IndFactSpecified", "NomItem", "DscItem", "Cantidad", "UniMed"})
		public class Item {

			private String NroLinDet = null;
            @XStreamImplicit(itemFieldName = "CodItem")
            private List<Item_RemCodItem> CodItem;
			private String IndFact = null;
			@XStreamOmitField
			private boolean IndFactSpecified;
			private String NomItem = null;
			private String DscItem = null;
			private java.math.BigDecimal Cantidad = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
			private String UniMed = "N/A";

			public String getNroLinDet() {
				if (this.NroLinDet == null) {
					return "";
				} else {
					return this.NroLinDet;
				}
			}

			public void setNroLinDet(String value) {
				if (value != null && !value.equals("")) {
					this.NroLinDet = value;
				}
			}

			public List<Item_RemCodItem> getCodItem() {
				return this.CodItem;
			}

			public void setCodItem(List<Item_RemCodItem> value) {
				this.CodItem = value;
			}

			public Item_RemIndFact getIndFact() {
				return Item_RemIndFact.forValue(Integer.parseInt(this.IndFact));
			}

			public void setIndFact(Item_RemIndFact value) {
				this.IndFact = String.valueOf(value.getValue());
			}

			public boolean getIndFactSpecified() {
				return this.IndFactSpecified;
			}

			public void setIndFactSpecified(boolean value) {
				this.IndFactSpecified = value;
			}

			public String getNomItem() {
				if (this.NomItem == null) {
					return "";
				} else {
					return this.NomItem;
				}
			}

			public void setNomItem(String value) {
				if (value != null && !value.equals("")) {
					this.NomItem = value;
				}
			}

			public String getDscItem() {
				if (this.DscItem == null) {
					return "";
				} else {
					return this.DscItem;
				}
			}

			public void setDscItem(String value) {
				if (value != null && !value.equals("")) {
					this.DscItem = value;
				}
			}

			public java.math.BigDecimal getCantidad() {
				return this.Cantidad.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setCantidad(java.math.BigDecimal value) {
				this.Cantidad = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public String getUniMed() {
				return this.UniMed;
			}

			public void setUniMed(String value) {
				if (value != null && !value.equals("")) {
					this.UniMed = value;
				}
			}
		}
	}

	@XStreamAlias("Detalle")
	public class Item_Det_Fact  implements java.io.Serializable{

		private List<Item_Det_Fact.Item> Item = new ArrayList<Item_Det_Fact.Item>();

		public List<Item_Det_Fact.Item> getItem() {
			return Item;
		}

		public void setItem(List<Item_Det_Fact.Item> Item) {
			this.Item = Item;
		}

        @XMLSequence({"NroLinDet", "CodItem", "IndFact", "IndAgenteResp", "NomItem", "DscItem", "Cantidad", "UniMed",
        "PrecioUnitario", "DescuentoPct", "DescuentoMonto", "SubDescuento", "RecargoPct", "RecargoMnt", "SubRecargo", "RetencPercep", "MontoItem",
                "IndAgenteRespSpecified", "recargoMntSpecified", "RecargoPctSpecified", "descuentoMontoSpecified", "descuentoPctSpecified"})
        public class Item {

			private String NroLinDet = null;
			@XStreamImplicit(itemFieldName = "CodItem")
			private List<Item_Det_FactCodItem> CodItem;
			private String IndFact = null;
			private String IndAgenteResp = null;
			private String NomItem = null;
			private String DscItem = null;
			private java.math.BigDecimal Cantidad = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
			private String UniMed = "N/A";
			private java.math.BigDecimal PrecioUnitario = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
			private java.math.BigDecimal DescuentoPct = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
			private java.math.BigDecimal DescuentoMonto = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
			@XStreamImplicit(itemFieldName = "SubDescuento")
            private List<Item_Det_FactSubDescuento> SubDescuento;
			private java.math.BigDecimal RecargoPct = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
			private java.math.BigDecimal RecargoMnt = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
			@XStreamImplicit(itemFieldName = "SubRecargo")
            private List<Item_Det_FactSubRecargo> SubRecargo;
            @XStreamImplicit(itemFieldName = "RetencPercep")
            private List<RetPerc> RetencPercep;
			private java.math.BigDecimal MontoItem = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
			@XStreamOmitField
			private boolean IndAgenteRespSpecified;
			@XStreamOmitField
			private boolean recargoMntSpecified;
			@XStreamOmitField
			private boolean RecargoPctSpecified;
			@XStreamOmitField
			private boolean descuentoMontoSpecified;
			@XStreamOmitField
			private boolean descuentoPctSpecified;

			public String getNroLinDet() {
				if (this.NroLinDet == null) {
					return "";
				} else {
					return this.NroLinDet;
				}
			}

			public void setNroLinDet(String value) {
				if (value != null && !value.equals("")) {
					this.NroLinDet = value;
				}
			}

			public List<Item_Det_FactCodItem> getCodItem() {
				return this.CodItem;
			}

			public void setCodItem(List<Item_Det_FactCodItem> value) {
				this.CodItem = value;
			}

			public Item_Det_FactIndFact getIndFact() {
				return Item_Det_FactIndFact.forValue(Integer.parseInt(this.IndFact));
			}

			public void setIndFact(Item_Det_FactIndFact value) {
				this.IndFact = String.valueOf(value.getValue());
			}

			public String getIndAgenteResp() {
				if (this.IndAgenteResp == null) {
					return "";
				} else {
					return this.IndAgenteResp;
				}
			}

			public void setIndAgenteResp(String value) {
				if (value != null && !value.equals("")) {
					this.IndAgenteResp = value;
				}
			}

			public boolean getIndAgenteRespSpecified() {
				return this.IndAgenteRespSpecified;
			}

			public void setIndAgenteRespSpecified(boolean value) {
				this.IndAgenteRespSpecified = value;
			}

			public String getNomItem() {
				if (this.NomItem == null) {
					return "";
				} else {
					return this.NomItem;
				}
			}

			public void setNomItem(String value) {
				if (value != null && !value.equals("")) {
					this.NomItem = value;
				}
			}

			public String getDscItem() {
				if (this.DscItem == null) {
					return "";
				} else {
					return this.DscItem;
				}
			}

			public void setDscItem(String value) {
				if (value != null && !value.equals("")) {
					this.DscItem = value;
				}
			}

			public java.math.BigDecimal getCantidad() {
				return this.Cantidad.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setCantidad(java.math.BigDecimal value) {
				this.Cantidad = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public String getUniMed() {
				return this.UniMed;
			}

			public void setUniMed(String value) {
				if (value != null && !value.equals("")) {
					this.UniMed = value;
				}
			}

			public java.math.BigDecimal getPrecioUnitario() {
				return new java.math.BigDecimal(Math.round(this.PrecioUnitario.doubleValue() * Math.pow(10, 6)) / Math.pow(10, 6));
			}

			public void setPrecioUnitario(java.math.BigDecimal value) {
				this.PrecioUnitario = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public java.math.BigDecimal getDescuentoPct() {
				return this.DescuentoPct.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setDescuentoPct(java.math.BigDecimal value) {
				this.DescuentoPct = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public boolean getDescuentoPctSpecified() {
				return this.descuentoPctSpecified;
			}

			public void setDescuentoPctSpecified(boolean value) {
				this.descuentoPctSpecified = value;
			}

			public java.math.BigDecimal getDescuentoMonto() {
				return this.DescuentoMonto.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setDescuentoMonto(java.math.BigDecimal value) {
				this.DescuentoMonto = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public boolean getDescuentoMontoSpecified() {
				return this.descuentoMontoSpecified;
			}

			public void setDescuentoMontoSpecified(boolean value) {
				this.descuentoMontoSpecified = value;
			}

			public List<Item_Det_FactSubDescuento> getSubDescuento() {
				return this.SubDescuento;
			}

			public void setSubDescuento(List<Item_Det_FactSubDescuento> value) {
				this.SubDescuento = value;
			}

			public java.math.BigDecimal getRecargoPct() {
				return this.RecargoPct.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setRecargoPct(java.math.BigDecimal value) {
				this.RecargoPct = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public boolean getRecargoPctSpecified() {
				return this.RecargoPctSpecified;
			}

			public void setRecargoPctSpecified(boolean value) {
				this.RecargoPctSpecified = value;
			}

			public java.math.BigDecimal getRecargoMnt() {
				return this.RecargoMnt.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setRecargoMnt(java.math.BigDecimal value) {
				this.RecargoMnt = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public boolean getRecargoMntSpecified() {
				return this.recargoMntSpecified;
			}

			public void setRecargoMntSpecified(boolean value) {
				this.recargoMntSpecified = value;
			}

			public List<Item_Det_FactSubRecargo> getSubRecargo() {
				return this.SubRecargo;
			}

			public void setSubRecargo(List<Item_Det_FactSubRecargo> value) {
				this.SubRecargo = value;
			}

			public List<RetPerc> getRetencPercep() {
				return this.RetencPercep;
			}

			public void setRetencPercep(List<RetPerc> value) {
				this.RetencPercep = value;
			}

			public java.math.BigDecimal getMontoItem() {
				return this.MontoItem.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setMontoItem(java.math.BigDecimal value) {
				this.MontoItem = value.setScale(2, RoundingMode.HALF_EVEN);
			}
		}
	}

	@XStreamAlias("Detalle")
	public class Item_Rem_Exp  implements java.io.Serializable{

		@XStreamImplicit(itemFieldName = "Item")
        private List<Item_Rem_Exp.Item> Item = new ArrayList<Item_Rem_Exp.Item>();

        public List<Item_Rem_Exp.Item> getItem() {
            return Item;
        }

        public void setItem(List<Item_Rem_Exp.Item> Item) {
            this.Item = Item;
        }

        @XMLSequence({"NroLinDet", "CodItem", "IndFact", "IndFactSpecified", "NomItem", "DscItem", "Cantidad", "UniMed", "PrecioUnitario", "MontoItem"})
        public class Item {

			private String NroLinDet = null;
			@XStreamImplicit(itemFieldName = "CodItem")
            private List<Item_Rem_ExpCodItem> CodItem;
			private String IndFact = null;
			@XStreamOmitField
			private boolean indFactSpecified;
			private String NomItem = null;
			private String DscItem = null;
			private java.math.BigDecimal Cantidad = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
			private String UniMed = "N/A";
			private java.math.BigDecimal PrecioUnitario = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
			private java.math.BigDecimal MontoItem = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);

			public String getNroLinDet() {
				if (this.NroLinDet == null) {
					return "";
				} else {
					return this.NroLinDet;
				}
			}

			public void setNroLinDet(String value) {
				if (value != null && !value.equals("")) {
					this.NroLinDet = value;
				}
			}

			 public List<Item_Rem_ExpCodItem> getCodItem() {
                return this.CodItem;
            }

			public void setCodItem(List<Item_Rem_ExpCodItem> value) {
				this.CodItem = value;
			}

			public Item_Rem_ExpIndFact getIndFact() {
				return Item_Rem_ExpIndFact.forValue(Integer.parseInt(this.IndFact));
			}

			public void setIndFact(Item_Rem_ExpIndFact value) {
				this.IndFact = String.valueOf(value.getValue());
			}

			public boolean getIndFactSpecified() {
				return this.indFactSpecified;
			}

			public void setIndFactSpecified(boolean value) {
				this.indFactSpecified = value;
			}

			public String getNomItem() {
				if (this.NomItem == null) {
					return "";
				} else {
					return this.NomItem;
				}
			}

			public void setNomItem(String value) {
				if (value != null && !value.equals("")) {
					this.NomItem = value;
				}
			}

			public String getDscItem() {
				if (this.DscItem == null) {
					return "";
				} else {
					return this.DscItem;
				}
			}

			public void setDscItem(String value) {
				if (value != null && !value.equals("")) {
					this.DscItem = value;
				}
			}

			public java.math.BigDecimal getCantidad() {
				return this.Cantidad.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setCantidad(java.math.BigDecimal value) {
				this.Cantidad = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public String getUniMed() {
				return this.UniMed;
			}

			public void setUniMed(String value) {
				if (value != null && !value.equals("")) {
					this.UniMed = value;
				}
			}

			public java.math.BigDecimal getPrecioUnitario() {
				return new java.math.BigDecimal(Math.round(this.PrecioUnitario.doubleValue() * Math.pow(10, 6)) / Math.pow(10, 6));
			}

			public void setPrecioUnitario(java.math.BigDecimal value) {
				this.PrecioUnitario = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public java.math.BigDecimal getMontoItem() {
				return this.MontoItem.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setMontoItem(java.math.BigDecimal value) {
				this.MontoItem = value.setScale(2, RoundingMode.HALF_EVEN);
			}
		}
	}

	@XStreamAlias("Detalle")
	public class Item_Resg  implements java.io.Serializable{

		 @XStreamImplicit(itemFieldName = "Item")
        private List<Item_Resg.Item> Item = new ArrayList<Item_Resg.Item>();

        public List<Item_Resg.Item> getItem() {
            return Item;
        }

        public void setItem(List<Item_Resg.Item> Item) {
            this.Item = Item;
        }

        @XMLSequence({"NroLinDet", "IndFact", "RetencPercep"})
        public class Item {

			private String NroLinDet = null;
			private String IndFact = null;
			@XStreamOmitField
			private boolean IndFactSpecified;
			 @XStreamImplicit(itemFieldName = "RetencPercep")
            private List<RetPerc_Resg> RetencPercep;

			public String getNroLinDet() {
				if (this.NroLinDet == null) {
					return "";
				} else {
					return this.NroLinDet;
				}
			}

			public void setNroLinDet(String value) {
				if (value != null && !value.equals("")) {
					this.NroLinDet = value;
				}
			}

			public Item_ResgIndFact getIndFact() {
				return Item_ResgIndFact.forValue(Integer.valueOf(this.IndFact));
			}

			public void setIndFact(Item_ResgIndFact value) {
				this.IndFact = String.valueOf(value.getValue());
			}

			public boolean getIndFactSpecified() {
				return this.IndFactSpecified;
			}

			public void setIndFactSpecified(boolean value) {
				this.IndFactSpecified = value;
			}

			public List<RetPerc_Resg> getRetencPercep() {
				return this.RetencPercep;
			}

			public void setRetencPercep(List<RetPerc_Resg> value) {
				this.RetencPercep = value;
			}
		}
	}

	@XStreamAlias("Detalle")
	public class Item_Det_Fact_Exp  implements java.io.Serializable{

		@XStreamImplicit(itemFieldName = "Item")
        private List<Item_Det_Fact_Exp.Item> Item = new ArrayList<Item_Det_Fact_Exp.Item>();

        public List<Item_Det_Fact_Exp.Item> getItem() {
            return Item;
        }

        public void setItem(List<Item_Det_Fact_Exp.Item> Item) {
            this.Item = Item;
        }

        @XMLSequence({"NroLinDet", "CodItem", "IndFact", "NomItem", "DscItem", "Cantidad", "UniMed",
                "PrecioUnitario", "DescuentoPct", "descuentoPctSpecified", "DescuentoMonto", "descuentoMontoSpecified", "SubDescuento", "RecargoPct",
                "recargoPctSpecified", "RecargoMnt", "recargoMntSpecified", "SubRecargo", "MontoItem"})
        public class Item {

            private String NroLinDet = null;
            @XStreamImplicit(itemFieldName = "CodItem")
            private List<Item_Det_Fact_ExpCodItem> CodItem;
            private String IndFact = null;
            private String NomItem = null;
            private String DscItem = null;
            private java.math.BigDecimal Cantidad = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
            private String UniMed = "N/A";
            private java.math.BigDecimal PrecioUnitario = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
            private java.math.BigDecimal DescuentoPct = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
            @XStreamOmitField
            private boolean descuentoPctSpecified;
            private java.math.BigDecimal DescuentoMonto = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
            @XStreamOmitField
            private boolean descuentoMontoSpecified;
            @XStreamImplicit(itemFieldName = "SubDescuento")
            private List<Item_Det_Fact_ExpSubDescuento> SubDescuento;
            private java.math.BigDecimal RecargoPct = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
            @XStreamOmitField
            private boolean recargoPctSpecified;
            private java.math.BigDecimal RecargoMnt = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
            @XStreamOmitField
            private boolean recargoMntSpecified;
            @XStreamImplicit(itemFieldName = "SubRecargo")
            private List<Item_Det_Fact_ExpSubRecargo> SubRecargo;
            private java.math.BigDecimal MontoItem = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);

            public String getNroLinDet() {
                if (this.NroLinDet == null) {
                    return "";
                } else {
                    return this.NroLinDet;
				}
			}

			public void setNroLinDet(String value) {
				if (value != null && !value.equals("")) {
					this.NroLinDet = value;
				}
			}

			public List<Item_Det_Fact_ExpCodItem> getCodItem() {
				return this.CodItem;
			}

			public void setCodItem(List<Item_Det_Fact_ExpCodItem> value) {
				this.CodItem = value;
			}

			public Item_Det_Fact_ExpIndFact getIndFact() {
				return Item_Det_Fact_ExpIndFact.forValue(Integer.valueOf(this.IndFact));
			}

			public void setIndFact(Item_Det_Fact_ExpIndFact value) {
				this.IndFact = String.valueOf(value.getValue());
			}

			public String getNomItem() {
				if (this.NomItem == null) {
					return "";
				} else {
					return this.NomItem;
				}
			}

			public void setNomItem(String value) {
				if (value != null && !value.equals("")) {
					this.NomItem = value;
				}
			}

			public String getDscItem() {
				if (this.DscItem == null) {
					return "";
				} else {
					return this.DscItem;
				}
			}

			public void setDscItem(String value) {
				if (value != null && !value.equals("")) {
					this.DscItem = value;
				}
			}

			public java.math.BigDecimal getCantidad() {
				return this.Cantidad.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setCantidad(java.math.BigDecimal value) {
				this.Cantidad = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public String getUniMed() {
				return this.UniMed;
			}

			public void setUniMed(String value) {
				this.UniMed = value;
			}

			public java.math.BigDecimal getPrecioUnitario() {
				return new java.math.BigDecimal(Math.round(this.PrecioUnitario.doubleValue() * Math.pow(10, 6)) / Math.pow(10, 6));
			}

			public void setPrecioUnitario(java.math.BigDecimal value) {
				this.PrecioUnitario = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public java.math.BigDecimal getDescuentoPct() {
				return this.DescuentoPct.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setDescuentoPct(java.math.BigDecimal value) {
				this.DescuentoPct = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public boolean getDescuentoPctSpecified() {
				return this.descuentoPctSpecified;
			}

			public void setDescuentoPctSpecified(boolean value) {
				this.descuentoPctSpecified = value;
			}

			public java.math.BigDecimal getDescuentoMonto() {
				return this.DescuentoMonto.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setDescuentoMonto(java.math.BigDecimal value) {
				this.DescuentoMonto = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public boolean getDescuentoMontoSpecified() {
				return this.descuentoMontoSpecified;
			}

			public void setDescuentoMontoSpecified(boolean value) {
				this.descuentoMontoSpecified = value;
			}

			public List<Item_Det_Fact_ExpSubDescuento> getSubDescuento() {
				return this.SubDescuento;
			}

			public void setSubDescuento(List<Item_Det_Fact_ExpSubDescuento> value) {
				this.SubDescuento = value;
			}

			public java.math.BigDecimal getRecargoPct() {
				return this.RecargoPct.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setRecargoPct(java.math.BigDecimal value) {
				this.RecargoPct = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public boolean getRecargoPctSpecified() {
				return this.recargoPctSpecified;
			}

			public void setRecargoPctSpecified(boolean value) {
				this.recargoPctSpecified = value;
			}

			public java.math.BigDecimal getRecargoMnt() {
				return this.RecargoMnt.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setRecargoMnt(java.math.BigDecimal value) {
				this.RecargoMnt = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public boolean getRecargoMntSpecified() {
				return this.recargoMntSpecified;
			}

			public void setRecargoMntSpecified(boolean value) {
				this.recargoMntSpecified = value;
			}

			public List<Item_Det_Fact_ExpSubRecargo> getSubRecargo() {
				return this.SubRecargo;
			}

			public void setSubRecargo(List<Item_Det_Fact_ExpSubRecargo> value) {
				this.SubRecargo = value;
			}

			public java.math.BigDecimal getMontoItem() {
				return this.MontoItem.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setMontoItem(java.math.BigDecimal value) {
				this.MontoItem = value.setScale(2, RoundingMode.HALF_EVEN);
			}
		}
	}

	@XStreamAlias("Detalle")
	public class Item_Det_Boleta  implements java.io.Serializable{

		@XStreamImplicit(itemFieldName = "Item")
		private List<Item_Det_Boleta.Item> Item = new ArrayList<Item_Det_Boleta.Item>();

		public List<Item_Det_Boleta.Item> getItem() {
			return Item;
		}

		public void setItem(List<Item_Det_Boleta.Item> Item) {
			this.Item = Item;
		}

        @XMLSequence({"NroLinDet", "CodItem", "IndFact", "IndAgenteResp", "NomItem", "DscItem", "Cantidad", "UniMed",
                "PrecioUnitario", "DescuentoPct", "DescuentoMonto", "SubDescuento", "RecargoPct", "RecargoMnt", "SubRecargo", "RetencPercep", "MontoItem",
                "IndAgenteRespSpecified", "recargoMntSpecified", "RecargoPctSpecified", "descuentoMontoSpecified", "descuentoPctSpecified"})
		public class Item {

			private String NroLinDet = null;
			@XStreamImplicit(itemFieldName = "CodItem")
			private List<Item_Det_FactCodItem> CodItem;
			private String IndFact = null;
			private String IndAgenteResp = null;
			private String NomItem = null;
			private String DscItem = null;
			private java.math.BigDecimal Cantidad = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
			private String UniMed = "N/A";
			private java.math.BigDecimal PrecioUnitario = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
			private java.math.BigDecimal DescuentoPct = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
			private java.math.BigDecimal DescuentoMonto = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
			@XStreamImplicit(itemFieldName = "SubDescuento")
			private List<Item_Det_FactSubDescuento> SubDescuento;
			private java.math.BigDecimal RecargoPct = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
			private java.math.BigDecimal RecargoMnt = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
			@XStreamImplicit(itemFieldName = "SubRecargo")
			private List<Item_Det_FactSubRecargo> SubRecargo;
			@XStreamImplicit(itemFieldName = "RetencPercep")
			private List<RetPerc> RetencPercep;
			private java.math.BigDecimal MontoItem = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
			@XStreamOmitField
			private boolean IndAgenteRespSpecified;
			@XStreamOmitField
			private boolean recargoMntSpecified;
			@XStreamOmitField
			private boolean RecargoPctSpecified;
			@XStreamOmitField
			private boolean descuentoMontoSpecified;
			@XStreamOmitField
			private boolean descuentoPctSpecified;

			public String getNroLinDet() {
				if (this.NroLinDet == null) {
					return "";
				} else {
					return this.NroLinDet;
				}
			}

			public void setNroLinDet(String value) {
				if (value != null && !value.equals("")) {
					this.NroLinDet = value;
				}
			}

			public List<Item_Det_FactCodItem> getCodItem() {
				return this.CodItem;
			}

			public void setCodItem(List<Item_Det_FactCodItem> value) {
				this.CodItem = value;
			}

			public Item_Det_BoletaIndFact getIndFact() {
				return Item_Det_BoletaIndFact.forValue(Integer.parseInt(this.IndFact));
			}

			public void setIndFact(Item_Det_BoletaIndFact value) {
				this.IndFact = String.valueOf(value.getValue());
			}

			public String getIndAgenteResp() {
				if (this.IndAgenteResp == null) {
					return "";
				} else {
					return this.IndAgenteResp;
				}
			}

			public void setIndAgenteResp(String value) {
				if (value != null && !value.equals("")) {
					this.IndAgenteResp = value;
				}
			}

			public boolean getIndAgenteRespSpecified() {
				return this.IndAgenteRespSpecified;
			}

			public void setIndAgenteRespSpecified(boolean value) {
				this.IndAgenteRespSpecified = value;
			}

			public String getNomItem() {
				if (this.NomItem == null) {
					return "";
				} else {
					return this.NomItem;
				}
			}

			public void setNomItem(String value) {
				if (value != null && !value.equals("")) {
					this.NomItem = value;
				}
			}

			public String getDscItem() {
				if (this.DscItem == null) {
					return "";
				} else {
					return this.DscItem;
				}
			}

			public void setDscItem(String value) {
				if (value != null && !value.equals("")) {
					this.DscItem = value;
				}
			}

			public java.math.BigDecimal getCantidad() {
				return this.Cantidad.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setCantidad(java.math.BigDecimal value) {
				this.Cantidad = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public String getUniMed() {
				return this.UniMed;
			}

			public void setUniMed(String value) {
				if (value != null && !value.equals("")) {
					this.UniMed = value;
				}
			}

			public java.math.BigDecimal getPrecioUnitario() {
				return new java.math.BigDecimal(Math.round(this.PrecioUnitario.doubleValue() * Math.pow(10, 6)) / Math.pow(10, 6));
			}

			public void setPrecioUnitario(java.math.BigDecimal value) {
				this.PrecioUnitario = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public java.math.BigDecimal getDescuentoPct() {
				return this.DescuentoPct.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setDescuentoPct(java.math.BigDecimal value) {
				this.DescuentoPct = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public boolean getDescuentoPctSpecified() {
				return this.descuentoPctSpecified;
			}

			public void setDescuentoPctSpecified(boolean value) {
				this.descuentoPctSpecified = value;
			}

			public java.math.BigDecimal getDescuentoMonto() {
				return this.DescuentoMonto.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setDescuentoMonto(java.math.BigDecimal value) {
				this.DescuentoMonto = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public boolean getDescuentoMontoSpecified() {
				return this.descuentoMontoSpecified;
			}

			public void setDescuentoMontoSpecified(boolean value) {
				this.descuentoMontoSpecified = value;
			}

			public List<Item_Det_FactSubDescuento> getSubDescuento() {
				return this.SubDescuento;
			}

			public void setSubDescuento(List<Item_Det_FactSubDescuento> value) {
				this.SubDescuento = value;
			}

			public java.math.BigDecimal getRecargoPct() {
				return this.RecargoPct.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setRecargoPct(java.math.BigDecimal value) {
				this.RecargoPct = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public boolean getRecargoPctSpecified() {
				return this.RecargoPctSpecified;
			}

			public void setRecargoPctSpecified(boolean value) {
				this.RecargoPctSpecified = value;
			}

			public java.math.BigDecimal getRecargoMnt() {
				return this.RecargoMnt.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setRecargoMnt(java.math.BigDecimal value) {
				this.RecargoMnt = value.setScale(2, RoundingMode.HALF_EVEN);
			}

			public boolean getRecargoMntSpecified() {
				return this.recargoMntSpecified;
			}

			public void setRecargoMntSpecified(boolean value) {
				this.recargoMntSpecified = value;
			}

			public List<Item_Det_FactSubRecargo> getSubRecargo() {
				return this.SubRecargo;
			}

			public void setSubRecargo(List<Item_Det_FactSubRecargo> value) {
				this.SubRecargo = value;
			}

			public List<RetPerc> getRetencPercep() {
				return this.RetencPercep;
			}

			public void setRetencPercep(List<RetPerc> value) {
				this.RetencPercep = value;
			}

			public java.math.BigDecimal getMontoItem() {
				return this.MontoItem.setScale(2, RoundingMode.HALF_EVEN);
			}

			public void setMontoItem(java.math.BigDecimal value) {
				this.MontoItem = value.setScale(2, RoundingMode.HALF_EVEN);
			}
		}
	}

    @XMLSequence({"TpoCod", "Cod"})
	public class Item_Det_Fact_ExpCodItem  implements java.io.Serializable{

		private String TpoCod = null;
		private String Cod = null;

		public String getTpoCod() {
			if (this.TpoCod == null) {
				return "";
			} else {
				return this.TpoCod;
			}
		}

		public void setTpoCod(String value) {
			if (value != null && !value.equals("")) {
				this.TpoCod = value;
			}
		}

		public String getCod() {
			if (this.Cod == null) {
				return "";
			} else {
				return this.Cod;
			}
		}

		public void setCod(String value) {
			if (value != null && !value.equals("")) {
				this.Cod = value;
			}
		}
	}

    @XMLSequence({"TpoCod", "Cod"})
    public class Item_Rem_ExpCodItem  implements java.io.Serializable{

        private String TpoCod = null;
        private String Cod = null;

        public String getTpoCod() {
            if (this.TpoCod == null) {
                return "";
            } else {
                return this.TpoCod;
            }
        }

        public void setTpoCod(String value) {
            if (value != null && !value.equals("")) {
                this.TpoCod = value;
            }
        }

        public String getCod() {
            if (this.Cod == null) {
                return "";
            } else {
                return this.Cod;
            }
        }

        public void setCod(String value) {
            if (value != null && !value.equals("")) {
                this.Cod = value;
            }
        }
    }

    @XMLSequence({"DescTipo", "DescVal"})
    public class Item_Det_Fact_ExpSubDescuento  implements java.io.Serializable{

        private String DescTipo = null;
        private java.math.BigDecimal DescVal = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);

        public String getDescTipo() {
            if (this.DescTipo == null) {
                return "";
            } else {
                return this.DescTipo;
            }
        }

        public void setDescTipo(String value) {
            if (value != null && !value.equals("")) {
                this.DescTipo = value;
            }
        }

        public java.math.BigDecimal getDescVal() {
            return this.DescVal.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setDescVal(java.math.BigDecimal value) {
            this.DescVal = value.setScale(2, RoundingMode.HALF_EVEN);
        }
    }

    @XMLSequence({"RecargoTipo", "RecargoVal"})
    public class Item_Det_Fact_ExpSubRecargo  implements java.io.Serializable{

        private Item_Det_Fact_ExpSubRecargoRecargoTipo RecargoTipo = Item_Det_Fact_ExpSubRecargoRecargoTipo.values()[0];
        private java.math.BigDecimal RecargoVal = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);

        public Item_Det_Fact_ExpSubRecargoRecargoTipo getRecargoTipo() {
            return this.RecargoTipo;
        }

        public void setRecargoTipo(Item_Det_Fact_ExpSubRecargoRecargoTipo value) {
            this.RecargoTipo = value;
        }

        public java.math.BigDecimal getRecargoVal() {
            return this.RecargoVal.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setRecargoVal(java.math.BigDecimal value) {
            this.RecargoVal = value.setScale(2, RoundingMode.HALF_EVEN);
        }
    }

    @XMLSequence({"TpoCod", "Cod"})
    public class Item_Det_FactCodItem  implements java.io.Serializable{

        private String TpoCod = null;
        private String Cod = null;

        public String getTpoCod() {
            if (this.TpoCod == null) {
                return "";
            } else {
                return this.TpoCod;
            }
        }

        public void setTpoCod(String value) {
            if (value != null && !value.equals("")) {
                this.TpoCod = value;
            }
        }

        public String getCod() {
            if (this.Cod == null) {
                return "";
            } else {
                return this.Cod;
            }
        }

        public void setCod(String value) {
            if (value != null && !value.equals("")) {
                this.Cod = value;
            }
        }
    }

    @XMLSequence({"DescTipo", "DescVal"})
    public class Item_Det_FactSubDescuento  implements java.io.Serializable{

        private String DescTipo = null;
        private java.math.BigDecimal DescVal = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);

        public String getDescTipo() {
            if (this.DescTipo == null) {
                return "";
            } else {
                return this.DescTipo;
            }
        }

        public void setDescTipo(String value) {
            if (value != null && !value.equals("")) {
                this.DescTipo = value;
            }
        }

        public java.math.BigDecimal getDescVal() {
            return this.DescVal.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setDescVal(java.math.BigDecimal value) {
            this.DescVal = value.setScale(2, RoundingMode.HALF_EVEN);
        }
    }

    @XMLSequence({"RecargoTipo", "RecargoVal"})
    public class Item_Det_FactSubRecargo  implements java.io.Serializable{

        private Item_Det_FactSubRecargoRecargoTipo RecargoTipo = Item_Det_FactSubRecargoRecargoTipo.values()[0];
        private java.math.BigDecimal RecargoVal = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);

        public Item_Det_FactSubRecargoRecargoTipo getRecargoTipo() {
            return this.RecargoTipo;
        }

        public void setRecargoTipo(Item_Det_FactSubRecargoRecargoTipo value) {
            this.RecargoTipo = value;
        }

        public java.math.BigDecimal getRecargoVal() {
            return this.RecargoVal.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setRecargoVal(java.math.BigDecimal value) {
            this.RecargoVal = value.setScale(2, RoundingMode.HALF_EVEN);
        }
    }

    @XMLSequence({"TpoCod", "Cod"})
    public class Item_RemCodItem  implements java.io.Serializable{

        private String TpoCod = null;
        private String Cod = null;

        public String getTpoCod() {
            if (this.TpoCod == null) {
                return "";
            } else {
                return this.TpoCod;
            }
        }

        public void setTpoCod(String value) {
            if (value != null && !value.equals("")) {
                this.TpoCod = value;
            }
        }

        public String getCod() {
            if (this.Cod == null) {
                return "";
            } else {
                return this.Cod;
            }
        }

        public void setCod(String value) {
            if (value != null && !value.equals("")) {
                this.Cod = value;
            }
        }
    }

    /////////DETALLE ITEM ENUM DECLARATION////////////////
    public enum Item_RemIndFact {

        Item1,
        Item2,
        Item3,
        Item4,
        Item5,
        Item6,
        Item7,
        Item8,
        Item10,
        Item11,
        Item12;

        public int getValue() {
            return this.ordinal();
        }

        public static Item_RemIndFact forValue(int value) {
            return values()[value];
        }
    }

    public enum Item_Det_FactIndAgenteResp {

        R,
        A;

        public int getValue() {
            return this.ordinal();
        }

        public static Item_Det_FactIndAgenteResp forValue(int value) {
            return values()[value];
        }
    }

    public enum Item_Det_FactSubRecargoRecargoTipo {

        Item1(1),
        Item2(2);
        private int intValue;
        private static java.util.HashMap<Integer, Item_Det_FactSubRecargoRecargoTipo> mappings;

        private static java.util.HashMap<Integer, Item_Det_FactSubRecargoRecargoTipo> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, Item_Det_FactSubRecargoRecargoTipo>();
                    }

            return mappings;
        }

        private Item_Det_FactSubRecargoRecargoTipo(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static Item_Det_FactSubRecargoRecargoTipo forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum Item_Det_FactIndFact {

        Item1(1),
        Item2(2),
        Item3(3),
        Item4(4),
        Item5(5),
        Item6(6),
        Item7(7),
        Item10(10),
        Item11(11),
        Item12(12);
        private int intValue;
        private static java.util.HashMap<Integer, Item_Det_FactIndFact> mappings;

        private static java.util.HashMap<Integer, Item_Det_FactIndFact> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, Item_Det_FactIndFact>();
                    }

            return mappings;
        }

        private Item_Det_FactIndFact(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static Item_Det_FactIndFact forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum Item_Det_Fact_ExpSubRecargoRecargoTipo {

        Item1,
        Item2;

        public int getValue() {
            return this.ordinal();
        }

        public static Item_Det_Fact_ExpSubRecargoRecargoTipo forValue(int value) {
            return values()[value];
        }
    }

    public enum Item_Det_Fact_ExpIndFact {

        Item1(1),
        Item2(2),
        Item3(3),
        Item4(4),
        Item5(5),
        Item6(6),
        Item7(7),
        Item10(10),
        Item11(11),
        Item12(12);
        private int intValue;
        private static java.util.HashMap<Integer, Item_Det_Fact_ExpIndFact> mappings;

        private static java.util.HashMap<Integer, Item_Det_Fact_ExpIndFact> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, Item_Det_Fact_ExpIndFact>();
                    }

            return mappings;
        }

        private Item_Det_Fact_ExpIndFact(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static Item_Det_Fact_ExpIndFact forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum Item_Rem_ExpIndFact {

        Item1(1),
        Item2(2),
        Item3(3),
        Item4(4),
        Item5(5),
        Item6(6),
        Item7(7),
        Item8(8),
        Item10(10),
        Item11(11),
        Item12(12);
        private int intValue;
        private static java.util.HashMap<Integer, Item_Rem_ExpIndFact> mappings;

        private static java.util.HashMap<Integer, Item_Rem_ExpIndFact> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, Item_Rem_ExpIndFact>();
                    }

            return mappings;
        }

        private Item_Rem_ExpIndFact(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static Item_Rem_ExpIndFact forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum Item_ResgIndFact {

        Item1(1),
        Item2(2),
        Item3(3),
        Item4(4),
        Item5(5),
        Item6(6),
        Item7(7),
        Item9(9),
        Item10(10),
        Item11(11),
        Item12(12);
        private int intValue;
        private static java.util.HashMap<Integer, Item_ResgIndFact> mappings;

        private static java.util.HashMap<Integer, Item_ResgIndFact> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, Item_ResgIndFact>();
                    }

            return mappings;
        }

        private Item_ResgIndFact(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static Item_ResgIndFact forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum Item_Det_BoletaIndFact {

        Item13(13),
        Item14(14),
        Item15(15);
        private int intValue;
        private static java.util.HashMap<Integer, Item_Det_BoletaIndFact> mappings;

        private static java.util.HashMap<Integer, Item_Det_BoletaIndFact> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, Item_Det_BoletaIndFact>();
                    }
            return mappings;
        }

        private Item_Det_BoletaIndFact(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static Item_Det_BoletaIndFact forValue(int value) {
            return getMappings().get(value);
        }
    }

    /////////ID DOC CLASS////////////////
    @XMLSequence({"TipoCFE", "Serie","Nro", "FchEmis","PeriodoDesde", "periodoDesdeSpecified","PeriodoHasta", "periodoHastaSpecified",
            "MntBruto", "mntBrutoSpecified","FmaPago", "FchaVenc","fchVencSpecified", "InfoAdicionalDoc","IVAalDia", "SecProf"})
    public class IdDoc  implements java.io.Serializable{

        @XStreamAlias("TipoCFE")
        private String tipoCFE;
        @XStreamAlias("Serie")
        private String serie;
        @XStreamAlias("Nro")
        private String nro;
        @XStreamAlias("FchEmis")
        private String fchEmis;
        @XStreamAlias("PeriodoDesde")
        private String periodoDesde = null;
        @XStreamOmitField
        private boolean periodoDesdeSpecified;
        @XStreamAlias("PeriodoHasta")
        private String periodoHasta = null;
        @XStreamOmitField
        private boolean periodoHastaSpecified;
        @XStreamAlias("MntBruto")
        private String mntBruto;
        @XStreamOmitField
        private boolean mntBrutoSpecified;
        @XStreamAlias("FmaPago")
        private String fmaPago = null;
        @XStreamAlias("FchaVenc")
        private String fchVenc = null;
        @XStreamOmitField
        private boolean fchVencSpecified;
        @XStreamAlias("InfoAdicionalDoc")
        private String InfoAdicionalDoc = null;
        @XStreamAlias("IVAalDia")
        private String iVAalDia = null;
        @XStreamAlias("SecProf")
        private String secProf = null;

        public Object getTipoCFE() {
            return this.tipoCFE;
        }

        public void setTipoCFE(Object value) {
        }

        public String getSerie() {
            return this.serie;
        }

        public void setSerie(String value) {
            this.serie = value;
        }

        public String getNro() {
            return this.nro;
        }

        public void setNro(String value) {
            this.nro = value;
        }

        public java.util.Date getFchEmis() {
            return null;
        }

        public void setFchEmis(java.util.Date value) {
        }

        public java.util.Date getPeriodoDesde() {
            return null;
        }

        public void setPeriodoDesde(java.util.Date value) {
        }

        public boolean getPeriodoDesdeSpecified() {
            return this.periodoDesdeSpecified;
        }

        public void setPeriodoDesdeSpecified(boolean value) {
            this.periodoDesdeSpecified = value;
        }

        public java.util.Date getPeriodoHasta() {
            return null;
        }

        public void setPeriodoHasta(java.util.Date value) {
        }

        public boolean getPeriodoHastaSpecified() {
            return this.periodoHastaSpecified;
        }

        public void setPeriodoHastaSpecified(boolean value) {
            this.periodoHastaSpecified = value;
        }

        public Object getMntBruto() {
            return null;
        }

        public void setMntBruto(Object value) {
        }

        public boolean getMntBrutoSpecified() {
            return this.mntBrutoSpecified;
        }

        public void setMntBrutoSpecified(boolean value) {
            this.mntBrutoSpecified = value;
        }

        public Object getFmaPago() {
            return null;
        }

        public void setFmaPago(Object value) {
        }

        public java.util.Date getFchaVenc() {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date aux = null;
            if (fchVenc != null) {
                try {
                    aux = formato.parse(fchVenc);
                } catch (ParseException ex) {
                    Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
                }
                return aux;
            } else {
                return null;
            }
        }

        public void setFchaVenc(java.util.Date value) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            fchVenc = formato.format(value);
        }

        public boolean getFchVencSpecified() {
            return this.fchVencSpecified;
        }

        public void setFchVencSpecified(boolean value) {
            this.fchVencSpecified = value;
        }

        public String getInfoAdicionalDoc() {
            return this.InfoAdicionalDoc;
        }

        public void setInfoAdicionalDoc(String value) {
            this.InfoAdicionalDoc = value;
        }

        public String getIVAalDia() {
            return null;
        }

        public void setIVAalDia(String value) {
            iVAalDia = value;
        }

        public String getSecProf() {
            return null;
        }

        public void setSecProf(String value) {
            secProf = value;
        }
    }

    @XMLSequence({"TipoCFE", "Serie","Nro", "FchEmis","PeriodoDesde", "periodoDesdeSpecified","PeriodoHasta", "periodoHastaSpecified",
            "MntBruto", "mntBrutoSpecified","FmaPago", "FchaVenc","fchVencSpecified", "InfoAdicionalDoc","IVAalDia", "SecProf"})
    public class IdDoc_Tck extends IdDoc  implements java.io.Serializable{

        @XStreamAlias("TipoCFE")
        private int tipoCFE;
        @XStreamAlias("Serie")
        private String serie;
        @XStreamAlias("Nro")
        private String nro;
        @XStreamAlias("FchEmis")
        private java.util.Date fchEmis = new java.util.Date(0);
        @XStreamAlias("PeriodoDesde")
        private String periodoDesde = null;
        @XStreamOmitField
        private boolean periodoDesdeSpecified;
        @XStreamAlias("PeriodoHasta")
        private String periodoHasta = null;
        @XStreamOmitField
        private boolean periodoHastaSpecified;
        @XStreamAlias("MntBruto")
        private String mntBruto;
        @XStreamOmitField
        private boolean mntBrutoSpecified;
        @XStreamAlias("FmaPago")
        private int fmaPago;
        @XStreamAlias("FchaVenc")
        private String fchVenc = null;
        @XStreamOmitField
        private boolean fchVencSpecified;
        private String InfoAdicionalDoc = null;
        @XStreamAlias("IVAalDia")
        private String iVAalDia = null;
        @XStreamAlias("SecProf")
        private String secProf = null;

        @Override
        public IdDoc_TckTipoCFE getTipoCFE() {
            return IdDoc_TckTipoCFE.forValue(this.tipoCFE);
        }

        public void setTipoCFE(IdDoc_TckTipoCFE value) {
            this.tipoCFE = value.getValue();
        }

        @Override
        public String getSerie() {
            return this.serie;
        }

        @Override
        public void setSerie(String value) {
            this.serie = value;
        }

        @Override
        public String getNro() {
            return this.nro;
        }

        @Override
        public void setNro(String value) {
            this.nro = value;
        }

        @Override
        public java.util.Date getFchEmis() {
            return this.fchEmis;
        }

        @Override
        public void setFchEmis(java.util.Date value) {
            this.fchEmis = value;
        }

        @Override
        public java.util.Date getPeriodoDesde() {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date aux = null;
            if (periodoDesde != null) {
                try {
                    aux = formato.parse(periodoDesde);
                } catch (ParseException ex) {
                    Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
                }
                return aux;
            } else {
                return null;
            }
        }

        @Override
        public void setPeriodoDesde(java.util.Date value) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            periodoDesde = formato.format(value);
        }

        @Override
        public boolean getPeriodoDesdeSpecified() {
            return this.periodoDesdeSpecified;
        }

        @Override
        public void setPeriodoDesdeSpecified(boolean value) {
            this.periodoDesdeSpecified = value;
        }

        @Override
        public java.util.Date getPeriodoHasta() {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date aux = null;
            if (periodoHasta != null) {
                try {
                    aux = formato.parse(periodoHasta);
                } catch (ParseException ex) {
                    Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
                }
                return aux;
            } else {
                return null;
            }
        }

        @Override
        public void setPeriodoHasta(java.util.Date value) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            periodoHasta = formato.format(value);
        }

        @Override
        public boolean getPeriodoHastaSpecified() {
            return this.periodoHastaSpecified;
        }

        @Override
        public void setPeriodoHastaSpecified(boolean value) {
            this.periodoHastaSpecified = value;
        }

        @Override
        public IdDoc_TckMntBruto getMntBruto() {
            return IdDoc_TckMntBruto.forValue(Integer.parseInt(this.mntBruto));
        }

        public void setMntBruto(IdDoc_TckMntBruto value) {
            this.mntBruto = String.valueOf(value.getValue());
        }

        @Override
        public boolean getMntBrutoSpecified() {
            return this.mntBrutoSpecified;
        }

        @Override
        public void setMntBrutoSpecified(boolean value) {
            this.mntBrutoSpecified = value;
        }

        @Override
        public IdDoc_TckFmaPago getFmaPago() {
            return IdDoc_TckFmaPago.forValue(this.fmaPago);
        }

        public void setFmaPago(IdDoc_TckFmaPago value) {
            this.fmaPago = value.getValue();
        }

        @Override
        public java.util.Date getFchaVenc() {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date aux = null;
            if (fchVenc != null) {
                try {
                    aux = formato.parse(fchVenc);
                } catch (ParseException ex) {
                    Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
                }
                return aux;
            } else {
                return null;
            }
        }

        @Override
        public void setFchaVenc(java.util.Date value) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            fchVenc = formato.format(value);
        }

        @Override
        public boolean getFchVencSpecified() {
            return this.fchVencSpecified;
        }

        @Override
        public void setFchVencSpecified(boolean value) {
            this.fchVencSpecified = value;
        }

        @Override
        public String getInfoAdicionalDoc() {
            if (this.InfoAdicionalDoc == null) {
                return "";
            } else {
                return this.InfoAdicionalDoc;
            }
        }

        @Override
        public void setInfoAdicionalDoc(String value) {
            if (value != null && !value.equals("")) {
                this.InfoAdicionalDoc = value;
            }
        }

        @Override
        public String getIVAalDia() {
            return iVAalDia;
        }

        @Override
        public void setIVAalDia(String iVAalDia) {
            this.iVAalDia = iVAalDia;
        }


        @Override
        public String getSecProf() {
            return secProf;
        }

        @Override
        public void setSecProf(String secProf) {
            this.secProf = secProf;
        }
    }

    @XMLSequence({"TipoCFE", "Serie", "Nro", "FchEmis", "InfoAdicionalDoc", "SecProf"})
    public class IdDoc_Resg extends IdDoc  implements java.io.Serializable{

        @XStreamAlias("TipoCFE")
        private int TipoCFE;
        @XStreamAlias("Serie")
        private String serie;
        @XStreamAlias("Nro")
        private String nro;
        @XStreamAlias("FchEmis")
        private String fchEmis = null;
        private String InfoAdicionalDoc = null;
        @XStreamAlias("SecProf")
        private String secProf = null;

        @Override
        public IdDoc_ResgTipoCFE getTipoCFE() {
            return IdDoc_ResgTipoCFE.forValue(this.TipoCFE);
        }

        public void setTipoCFE(IdDoc_ResgTipoCFE value) {
            this.TipoCFE = value.getValue();
        }

        @Override
        public String getSerie() {
            return this.serie;
        }

        @Override
        public void setSerie(String value) {
            this.serie = value;
        }

        @Override
        public String getNro() {
            return this.nro;
        }

        @Override
        public void setNro(String value) {
            this.nro = value;
        }

        @Override
        public java.util.Date getFchEmis() {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date aux = null;
            if (fchEmis != null) {
                try {
                    aux = formato.parse(fchEmis);
                } catch (ParseException ex) {
                    Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
                }
                return aux;
            } else {
                return null;
            }
        }

        @Override
        public void setFchEmis(java.util.Date value) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            fchEmis = formato.format(value);
        }

        @Override
        public String getInfoAdicionalDoc() {
            if (this.InfoAdicionalDoc == null) {
                return "";
            } else {
                return this.InfoAdicionalDoc;
            }
        }

        @Override
        public void setInfoAdicionalDoc(String value) {
            if (value != null && !value.equals("")) {
                this.InfoAdicionalDoc = value;
            }
        }

        @Override
        public String getSecProf() {
            return secProf;
        }

        @Override
        public void setSecProf(String secProf) {
            this.secProf = secProf;
        }
    }

    @XMLSequence({"TipoCFE", "Serie","Nro", "FchEmis","TipoTraslado", "IndPropiedad","TipoDocProp", "DocProp",
            "DocPropExt", "CodPaisProp","RznSocProp", "ClauVenta","ModVenta", "ViaTransp","InfoAdicionalDoc", "SecProf"})
    public class IdDoc_Rem_Exp extends IdDoc  implements java.io.Serializable{

        private int TipoCFE;
        private String Serie;
        private String Nro;
        private String FchEmis = null;
        private String TipoTraslado = null;
        private String IndPropiedad = null;
        private String TipoDocProp = null;
        private String DocProp = null;
        private String DocPropExt = null;
        private String CodPaisProp = null;
        private String RznSocProp = null;
        private String ClauVenta = null;
        private String ModVenta = null;
        private String ViaTransp = null;
        private String InfoAdicionalDoc = null;
        private String SecProf = null;

        @Override
        public IdDoc_Rem_ExpTipoCFE getTipoCFE() {
            return IdDoc_Rem_ExpTipoCFE.forValue(this.TipoCFE);
        }

        public void setTipoCFE(IdDoc_Rem_ExpTipoCFE value) {
            this.TipoCFE = value.getValue();
        }

        @Override
        public String getSerie() {
            if (this.Serie == null) {
                return "";
            } else {
                return this.Serie;
            }
        }

        @Override
        public void setSerie(String value) {
            if (value != null && !value.equals("")) {
                this.Serie = value;
            }
        }

        @Override
        public String getNro() {
            if (this.Nro == null) {
                return "";
            } else {
                return this.Nro;
            }
        }

        @Override
        public void setNro(String value) {
            if (value != null && !value.equals("")) {
                this.Nro = value;
            }
        }

        @Override
        public java.util.Date getFchEmis() {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date aux = null;
            try {
                aux = formato.parse(FchEmis);
            } catch (ParseException ex) {
            }
            return aux;
        }

        @Override
        public void setFchEmis(java.util.Date value) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            FchEmis = formato.format(value);
        }

        public IdDoc_Rem_ExpTipoTraslado getTipoTraslado() {
            return IdDoc_Rem_ExpTipoTraslado.forValue(Integer.parseInt(this.TipoTraslado));
        }

        public void setTipoTraslado(IdDoc_Rem_ExpTipoTraslado value) {
            this.TipoTraslado = String.valueOf(value.getValue());
        }

        public String getClauVenta() {
            if (this.ClauVenta == null) {
                return "";
            } else {
                return this.ClauVenta;
            }
        }

        public void setClauVenta(String value) {
            if (value != null && !value.equals("")) {
                this.ClauVenta = value;
            }
        }

        public int getModVenta() {
            if (this.ModVenta != null) {
                return Integer.parseInt(this.ModVenta);
            } else {
                return 0;
            }
        }

        public void setModVenta(int value) {
            this.ModVenta = String.valueOf(value);
        }

        public int getViaTransp() {
            if (this.ViaTransp != null) {
                return Integer.parseInt(this.ViaTransp);
            } else {
                return 0;
            }
        }

        public void setViaTransp(int value) {
            this.ViaTransp = String.valueOf(value);
        }

        @Override
        public String getInfoAdicionalDoc() {
            if (this.InfoAdicionalDoc == null) {
                return "";
            } else {
                return this.InfoAdicionalDoc;
            }
        }

        @Override
        public void setInfoAdicionalDoc(String value) {
            if (value != null && !value.equals("")) {
                this.InfoAdicionalDoc = value;
            }
        }
        
         @Override
        public String getSecProf() {
            return SecProf;
        }

        @Override
        public void setSecProf(String secProf) {
            this.SecProf = secProf;
        }

        public IdDoc_IndPropiedadMercadTransp getIndPropiedad() {
            if (this.IndPropiedad != null) {
                return IdDoc_IndPropiedadMercadTransp.forValue(Integer.parseInt(this.IndPropiedad));
            } else {
                return null;
            }
        }

        public void setIndPropiedad(IdDoc_IndPropiedadMercadTransp value) {
            this.IndPropiedad = String.valueOf(value.getValue());
        }

        public DocType getTipoDocProp() {
            if (this.TipoDocProp != null) {
                return DocType.forValue(Integer.parseInt(this.TipoDocProp));
            } else {
                return null;
            }
        }

        public void setTipoDocProp(DocType value) {
            this.TipoDocProp = String.valueOf(value.getValue());
        }

        public String getDocProp() {
            return DocProp;
        }

        public void setDocProp(String DocProp) {
            this.DocProp = DocProp;
        }

        public String getDocPropExt() {
            return DocPropExt;
        }

        public void setDocPropExt(String DocPropExt) {
            this.DocPropExt = DocPropExt;
        }

        public String getCodPaisProp() {
            return CodPaisProp;
        }

        public void setCodPaisProp(String CodPaisProp) {
            this.CodPaisProp = CodPaisProp;
        }

        public String getRznSocProp() {
            return RznSocProp;
        }

        public void setRznSocProp(String RznSocProp) {
            this.RznSocProp = RznSocProp;
        }
    }

    @XMLSequence({"TipoCFE", "Serie","Nro", "FchEmis","PeriodoDesde", "PeriodoDesdeSpecified", "PeriodoHasta", "PeriodoHastaSpecified",
            "MntBruto", "MntBrutoSpecified","FmaPago", "FchVenc", "FchVencSpecified", "ClauVenta","ModVenta", "ModVentaSpecified",
            "ViaTransp","ViaTranspSpecified","InfoAdicionalDoc","IVAalDia","SecProf"})
    public class IdDoc_Fact_Exp extends IdDoc  implements java.io.Serializable{

        private int TipoCFE;
        private String Serie;
        private String Nro;
        private String FchEmis = null;
        private String PeriodoDesde = null;
        @XStreamOmitField
        private boolean PeriodoDesdeSpecified;
        private String PeriodoHasta = null;
        @XStreamOmitField
        private boolean PeriodoHastaSpecified;
        private String MntBruto = null;
        @XStreamOmitField
        private boolean MntBrutoSpecified;
        private String FmaPago = null;
        private String FchVenc = null;
        @XStreamOmitField
        private boolean FchVencSpecified;
        private String ClauVenta;
        private String ModVenta = null;
        @XStreamOmitField
        private boolean ModVentaSpecified;
        private String ViaTransp = null;
        @XStreamOmitField
        private boolean ViaTranspSpecified;
        private String InfoAdicionalDoc = null;
        @XStreamAlias("IVAalDia")
        private String iVAalDia = null;
        @XStreamAlias("SecProf")
        private String secProf = null;

        @Override
        public IdDoc_Fact_ExpTipoCFE getTipoCFE() {
            return IdDoc_Fact_ExpTipoCFE.forValue(this.TipoCFE);
        }

        public void setTipoCFE(IdDoc_Fact_ExpTipoCFE value) {
            this.TipoCFE = value.getValue();
        }

        @Override
        public String getSerie() {
            return this.Serie;
        }

        @Override
        public void setSerie(String value) {
            this.Serie = value;
        }

        @Override
        public String getNro() {
            return this.Nro;
        }

        @Override
        public void setNro(String value) {
            this.Nro = value;
        }

        @Override
        public java.util.Date getFchEmis() {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date aux = null;
            try {
                aux = formato.parse(FchEmis);
            } catch (ParseException ex) {
            }
            return aux;
        }

        @Override
        public void setFchEmis(java.util.Date value) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            FchEmis = formato.format(value);
        }

        @Override
        public java.util.Date getPeriodoDesde() {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date aux = null;
            if (PeriodoDesde != null) {
                try {
                    aux = formato.parse(PeriodoDesde);
                } catch (ParseException ex) {
                    Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
                }
                return aux;
            } else {
                return null;
            }
        }

        @Override
        public void setPeriodoDesde(java.util.Date value) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            PeriodoDesde = formato.format(value);
        }

        @Override
        public boolean getPeriodoDesdeSpecified() {
            return this.PeriodoDesdeSpecified;
        }

        @Override
        public void setPeriodoDesdeSpecified(boolean value) {
            this.PeriodoDesdeSpecified = value;
        }

        @Override
        public java.util.Date getPeriodoHasta() {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date aux = null;
            if (PeriodoHasta != null) {
                try {
                    aux = formato.parse(PeriodoHasta);
                } catch (ParseException ex) {
                    Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
                }
                return aux;
            } else {
                return null;
            }
        }

        @Override
        public void setPeriodoHasta(java.util.Date value) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            PeriodoHasta = formato.format(value);
        }

        @Override
        public boolean getPeriodoHastaSpecified() {
            return this.PeriodoHastaSpecified;
        }

        @Override
        public void setPeriodoHastaSpecified(boolean value) {
            this.PeriodoHastaSpecified = value;
        }

        @Override
        public IdDoc_Fact_ExpMntBruto getMntBruto() {
            if (this.MntBruto != null) {
                return IdDoc_Fact_ExpMntBruto.forValue(Integer.parseInt(this.MntBruto));
            } else {
                return null;
            }
        }

        public void setMntBruto(IdDoc_Fact_ExpMntBruto value) {
            this.MntBruto = String.valueOf(value.getValue());
        }

        @Override
        public boolean getMntBrutoSpecified() {
            return this.MntBrutoSpecified;
        }

        @Override
        public void setMntBrutoSpecified(boolean value) {
            this.MntBrutoSpecified = value;
        }

        @Override
        public IdDoc_Fact_ExpFmaPago getFmaPago() {
            if (this.FmaPago != null) {
                return IdDoc_Fact_ExpFmaPago.forValue(Integer.parseInt(this.FmaPago));
            } else {
                return null;
            }
        }

        public void setFmaPago(IdDoc_Fact_ExpFmaPago value) {
            this.FmaPago = String.valueOf(value.getValue());
        }

        @Override
        public java.util.Date getFchaVenc() {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date aux = null;
            if (FchVenc != null) {
                try {
                    aux = formato.parse(FchVenc);
                } catch (ParseException ex) {
                    Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
                }
                return aux;
            } else {
                return null;
            }
        }

        @Override
        public void setFchaVenc(java.util.Date value) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            FchVenc = formato.format(value);
        }

        @Override
        public boolean getFchVencSpecified() {
            return this.FchVencSpecified;
        }

        @Override
        public void setFchVencSpecified(boolean value) {
            this.FchVencSpecified = value;
        }

        public String getClauVenta() {
            return this.ClauVenta;
        }

        public void setClauVenta(String value) {
            this.ClauVenta = value;
        }

        public int getModVenta() {
            if (this.ModVenta != null) {
                return Integer.parseInt(this.ModVenta);
            } else {
                return 0;
            }
        }

        public void setModVenta(int value) {
            this.ModVenta = String.valueOf(value);
        }

        public boolean getModVentaSpecified() {
            return this.ModVentaSpecified;
        }

        public void setModVentaSpecified(boolean value) {
            this.ModVentaSpecified = value;
        }

        public int getViaTransp() {
            if (this.ViaTransp != null) {
                return Integer.parseInt(this.ViaTransp);
            } else {
                return 0;
            }
        }

        public void setViaTransp(int value) {
            this.ViaTransp = String.valueOf(value);
        }

        public boolean getViaTranspSpecified() {
            return this.ViaTranspSpecified;
        }

        public void setViaTranspSpecified(boolean value) {
            this.ViaTranspSpecified = value;
        }

        @Override
        public String getInfoAdicionalDoc() {
            if (this.InfoAdicionalDoc == null) {
                return "";
            } else {
                return this.InfoAdicionalDoc;
            }
        }

        @Override
        public void setInfoAdicionalDoc(String value) {
            if (value != null && !value.equals("")) {
                this.InfoAdicionalDoc = value;
            }
        }

        @Override
        public String getIVAalDia() {
            return iVAalDia;
        }

        @Override
        public void setIVAalDia(String iVAalDia) {
            this.iVAalDia = iVAalDia;
        }

        @Override
        public String getSecProf() {
            return secProf;
        }

        @Override
        public void setSecProf(String secProf) {
            this.secProf = secProf;
        }
    }

    @XMLSequence({"TipoCFE", "Serie","Nro", "FchEmis","TipoTraslado", "IndPropiedad", "TipoDocProp", "CodPaisProp", "DocProp", "DocPropExt","RznSocProp","InfoAdicionalDoc", "SecProf"})
    public class IdDoc_Rem extends IdDoc  implements java.io.Serializable{

        private int TipoCFE;
		private String Serie;
		private String Nro;
		private String FchEmis = null;
		private String TipoTraslado;
		private String IndPropiedad = null;
		private String TipoDocProp = null;
		private String CodPaisProp = null;
		private String DocProp = null;
		private String DocPropExt = null;
		private String RznSocProp = null;
		private String InfoAdicionalDoc = null;
		@XStreamAlias("SecProf")
		private String secProf = null;

        @Override
        public IdDoc_RemTipoCFE getTipoCFE() {
            return IdDoc_RemTipoCFE.forValue(this.TipoCFE);
        }

        public void setTipoCFE(IdDoc_RemTipoCFE value) {
            this.TipoCFE = value.getValue();
        }

        @Override
        public String getSerie() {
            return this.Serie;
        }

        @Override
        public void setSerie(String value) {
            this.Serie = value;
        }

        @Override
        public String getNro() {
            return this.Nro;
        }

        @Override
        public void setNro(String value) {
            this.Nro = value;
        }

        @Override
        public java.util.Date getFchEmis() {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date aux = null;
            try {
                aux = formato.parse(FchEmis);
            } catch (ParseException ex) {
            }
            return aux;
        }

        @Override
        public void setFchEmis(java.util.Date value) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            FchEmis = formato.format(value);
        }

        public IdDoc_RemTipoTraslado getTipoTraslado() {
            if (this.TipoTraslado != null) {
                return IdDoc_RemTipoTraslado.forValue(Integer.parseInt(this.TipoTraslado));
            } else {
                return null;
            }
        }

        public void setTipoTraslado(IdDoc_RemTipoTraslado value) {
            this.TipoTraslado = String.valueOf(value.getValue());
        }

        @Override
        public String getInfoAdicionalDoc() {
            if (this.InfoAdicionalDoc == null) {
                return "";
            } else {
                return this.InfoAdicionalDoc;
            }
        }

        @Override
        public void setInfoAdicionalDoc(String value) {
            if (value != null && !value.equals("")) {
                this.InfoAdicionalDoc = value;
            }
        }

        @Override
        public String getSecProf() {
            return secProf;
        }

        @Override
        public void setSecProf(String secProf) {
            this.secProf = secProf;
        }

        public IdDoc_IndPropiedadMercadTransp getIndPropiedad() {
            if (this.IndPropiedad != null) {
                return IdDoc_IndPropiedadMercadTransp.forValue(Integer.parseInt(this.IndPropiedad));
            } else {
                return null;
            }
        }

        public void setIndPropiedad(IdDoc_IndPropiedadMercadTransp value) {
            this.IndPropiedad = String.valueOf(value.getValue());
        }

        public DocType getTipoDocProp() {
            if (this.TipoDocProp != null) {
                return DocType.forValue(Integer.parseInt(this.TipoDocProp));
            } else {
                return null;
            }
        }

        public void setTipoDocProp(DocType value) {
            this.TipoDocProp = String.valueOf(value.getValue());
        }

        public String getDocProp() {
            return DocProp;
        }

        public void setDocProp(String DocProp) {
            this.DocProp = DocProp;
        }

        public String getDocPropExt() {
            return DocPropExt;
        }

        public void setDocPropExt(String DocPropExt) {
            this.DocPropExt = DocPropExt;
        }

        public String getCodPaisProp() {
            return CodPaisProp;
        }

        public void setCodPaisProp(String CodPaisProp) {
            this.CodPaisProp = CodPaisProp;
        }

        public String getRznSocProp() {
            return RznSocProp;
        }

        public void setRznSocProp(String RznSocProp) {
            this.RznSocProp = RznSocProp;
        }
    }

    @XMLSequence({"TipoCFE", "Serie","Nro", "FchEmis","PeriodoDesde", "periodoDesdeSpecified", "PeriodoHasta", "periodoHastaSpecified", "MntBruto",
            "mntBrutoSpecified", "FmaPago", "FchaVenc", "fchVencSpecified", "InfoAdicionalDoc","IVAalDia", "SecProf"})
    public class IdDoc_Fact extends IdDoc  implements java.io.Serializable{

        @XStreamAlias("TipoCFE")
        private int tipoCFE;
        @XStreamAlias("Serie")
        private String serie;
        @XStreamAlias("Nro")
        private String nro;
        @XStreamAlias("FchEmis")
        private String fchEmis = null;
        @XStreamAlias("PeriodoDesde")
        private String periodoDesde = null;
        @XStreamOmitField
        private boolean periodoDesdeSpecified;
        @XStreamAlias("PeriodoHasta")
        private String periodoHasta = null;
        @XStreamOmitField
        private boolean periodoHastaSpecified;
        @XStreamAlias("MntBruto")
        private String mntBruto;
        @XStreamOmitField
        private boolean mntBrutoSpecified;
        @XStreamAlias("FmaPago")
        private String fmaPago;
        @XStreamAlias("FchVenc")
        private String fchVenc = null;
        @XStreamOmitField
        private boolean fchVencSpecified;
        @XStreamAlias("InfoAdicionalDoc")
        private String InfoAdicionalDoc = null;
        @XStreamAlias("IVAalDia")
        private String iVAalDia = null;
        @XStreamAlias("SecProf")
        private String secProf = null;

        @Override
        public IdDoc_FactTipoCFE getTipoCFE() {
            return IdDoc_FactTipoCFE.forValue(this.tipoCFE);
        }

        public void setTipoCFE(IdDoc_FactTipoCFE value) {
            this.tipoCFE = value.getValue();
        }

        @Override
        public String getSerie() {
            return this.serie;
        }

        @Override
        public void setSerie(String value) {
            this.serie = value;
        }

        @Override
        public String getNro() {
            return this.nro;
        }

        @Override
        public void setNro(String value) {
            this.nro = value;
        }

        @Override
        public java.util.Date getFchEmis() {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date aux = null;
            try {
                aux = formato.parse(fchEmis);
            } catch (ParseException ex) {
            }
            return aux;
        }

        @Override
        public void setFchEmis(java.util.Date value) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            fchEmis = formato.format(value);
        }

        @Override
        public java.util.Date getPeriodoDesde() {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date aux = null;
            if (periodoDesde != null) {
                try {
                    aux = formato.parse(periodoDesde);
                } catch (ParseException ex) {
                    Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
                }
                return aux;
            } else {
                return null;
            }
        }

        @Override
        public void setPeriodoDesde(java.util.Date value) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            periodoDesde = formato.format(value);
        }

        @Override
        public boolean getPeriodoDesdeSpecified() {
            return this.periodoDesdeSpecified;
        }

        @Override
        public void setPeriodoDesdeSpecified(boolean value) {
            this.periodoDesdeSpecified = value;
        }

        @Override
        public java.util.Date getPeriodoHasta() {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date aux = null;
            if (periodoHasta != null) {
                try {
                    aux = formato.parse(periodoHasta);
                } catch (ParseException ex) {
                    Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
                }
                return aux;
            } else {
                return null;
            }
        }

        @Override
        public void setPeriodoHasta(java.util.Date value) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            periodoHasta = formato.format(value);
        }

        @Override
        public boolean getPeriodoHastaSpecified() {
            return this.periodoHastaSpecified;
        }

        @Override
        public void setPeriodoHastaSpecified(boolean value) {
            this.periodoHastaSpecified = value;
        }

        @Override
        public IdDoc_FactMntBruto getMntBruto() {
            if (this.mntBruto != null) {
                return IdDoc_FactMntBruto.forValue(Integer.parseInt(this.mntBruto));
            } else {
                return null;
            }
        }

        public void setMntBruto(IdDoc_FactMntBruto value) {
            this.mntBruto = String.valueOf(value.getValue());
        }

        @Override
        public boolean getMntBrutoSpecified() {
            return this.mntBrutoSpecified;
        }

        @Override
        public void setMntBrutoSpecified(boolean value) {
            this.mntBrutoSpecified = value;
        }

        @Override
        public IdDoc_FactFmaPago getFmaPago() {
            if (this.fmaPago != null) {
                return IdDoc_FactFmaPago.forValue(Integer.parseInt(this.fmaPago));
            } else {
                return null;
            }
        }

        public void setFmaPago(IdDoc_FactFmaPago value) {
            this.fmaPago = String.valueOf(value.getValue());
        }

        @Override
        public java.util.Date getFchaVenc() {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date aux = null;
            if (fchVenc != null) {
                try {
                    aux = formato.parse(fchVenc);
                } catch (ParseException ex) {
                    Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
                }
                return aux;
            } else {
                return null;
            }
        }

        @Override
        public void setFchaVenc(java.util.Date value) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            fchVenc = formato.format(value);
        }

        @Override
        public boolean getFchVencSpecified() {
            return this.fchVencSpecified;
        }

        @Override
        public void setFchVencSpecified(boolean value) {
            this.fchVencSpecified = value;
        }

        @Override
        public String getInfoAdicionalDoc() {
            if (this.InfoAdicionalDoc == null) {
                return "";
            } else {
                return this.InfoAdicionalDoc;
            }
        }

        @Override
        public void setInfoAdicionalDoc(String value) {
            if (value != null && !value.equals("")) {
                this.InfoAdicionalDoc = value;
            }
        }

        @Override
        public String getIVAalDia() {
            return iVAalDia;
        }

        @Override
        public void setIVAalDia(String iVAalDia) {
            this.iVAalDia = iVAalDia;
        }

        @Override
        public String getSecProf() {
            return secProf;
        }

        @Override
        public void setSecProf(String secProf) {
            this.secProf = secProf;
        }
    }

    @XMLSequence({"TipoCFE", "Serie","Nro", "FchEmis","PeriodoDesde", "periodoDesdeSpecified", "PeriodoHasta", "periodoHastaSpecified",
            "MntBruto", "mntBrutoSpecified", "FmaPago", "FchaVenc", "fchVencSpecified", "InfoAdicionalDoc"})
    public class IdDoc_Boleta extends IdDoc  implements java.io.Serializable{

        @XStreamAlias("TipoCFE")
        private int tipoCFE;
        @XStreamAlias("Serie")
        private String serie;
        @XStreamAlias("Nro")
        private String nro;
        @XStreamAlias("FchEmis")
        private java.util.Date fchEmis = new java.util.Date(0);
        private String periodoDesde = null;
        @XStreamOmitField
        private boolean periodoDesdeSpecified;
        private String periodoHasta = null;
        @XStreamOmitField
        private boolean periodoHastaSpecified;
        @XStreamAlias("MntBruto")
        private String mntBruto;
        @XStreamOmitField
        private boolean mntBrutoSpecified;
        @XStreamAlias("FmaPago")
        private int fmaPago;
        @XStreamOmitField
        @XStreamAlias("FchaVenc")
        private String fchVenc = null;
        @XStreamOmitField
        private boolean fchVencSpecified;
        @XStreamOmitField
        private String InfoAdicionalDoc = null;

        @Override
        public IdDoc_BoletaTipoCFE getTipoCFE() {
            return IdDoc_BoletaTipoCFE.forValue(this.tipoCFE);
        }

        public void setTipoCFE(IdDoc_BoletaTipoCFE value) {
            this.tipoCFE = value.getValue();
        }

        @Override
        public String getSerie() {
            return this.serie;
        }

        @Override
        public void setSerie(String value) {
            this.serie = value;
        }

        @Override
        public String getNro() {
            return this.nro;
        }

        @Override
        public void setNro(String value) {
            this.nro = value;
        }

        @Override
        public java.util.Date getFchEmis() {
            return this.fchEmis;
        }

        @Override
        public void setFchEmis(java.util.Date value) {
            this.fchEmis = value;
        }

        @Override
        public java.util.Date getPeriodoDesde() {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date aux = null;
            if (periodoDesde != null) {
                try {
                    aux = formato.parse(periodoDesde);
                } catch (ParseException ex) {
                    Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
                }
                return aux;
            } else {
                return null;
            }
        }

        @Override
        public void setPeriodoDesde(java.util.Date value) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            periodoDesde = formato.format(value);
        }

        @Override
        public boolean getPeriodoDesdeSpecified() {
            return this.periodoDesdeSpecified;
        }

        @Override
        public void setPeriodoDesdeSpecified(boolean value) {
            this.periodoDesdeSpecified = value;
        }

        @Override
        public java.util.Date getPeriodoHasta() {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date aux = null;
            if (periodoHasta != null) {
                try {
                    aux = formato.parse(periodoHasta);
                } catch (ParseException ex) {
                    Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
                }
                return aux;
            } else {
                return null;
            }
        }

        @Override
        public void setPeriodoHasta(java.util.Date value) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            periodoHasta = formato.format(value);
        }

        @Override
        public boolean getPeriodoHastaSpecified() {
            return this.periodoHastaSpecified;
        }

        @Override
        public void setPeriodoHastaSpecified(boolean value) {
            this.periodoHastaSpecified = value;
        }

        @Override
        public IdDoc_BoletaMntBruto getMntBruto() {
            return IdDoc_BoletaMntBruto.forValue(Integer.parseInt(this.mntBruto));
        }

        public void setMntBruto(IdDoc_BoletaMntBruto value) {
            this.mntBruto = String.valueOf(value.getValue());
        }

        @Override
        public boolean getMntBrutoSpecified() {
            return this.mntBrutoSpecified;
        }

        @Override
        public void setMntBrutoSpecified(boolean value) {
            this.mntBrutoSpecified = value;
        }

        @Override
        public IdDoc_TckFmaPago getFmaPago() {
            return IdDoc_TckFmaPago.forValue(this.fmaPago);
        }

        public void setFmaPago(IdDoc_TckFmaPago value) {
            this.fmaPago = value.getValue();
        }

        @Override
        public java.util.Date getFchaVenc() {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date aux = null;
            if (fchVenc != null) {
                try {
                    aux = formato.parse(fchVenc);
                } catch (ParseException ex) {
                    Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
                }
                return aux;
            } else {
                return null;
            }
        }

        @Override
        public void setFchaVenc(java.util.Date value) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            fchVenc = formato.format(value);
        }

        @Override
        public boolean getFchVencSpecified() {
            return this.fchVencSpecified;
        }

        @Override
        public void setFchVencSpecified(boolean value) {
            this.fchVencSpecified = value;
        }

        @Override
        public String getInfoAdicionalDoc() {
            if (this.InfoAdicionalDoc == null) {
                return "";
            } else {
                return this.InfoAdicionalDoc;
            }
        }

        @Override
        public void setInfoAdicionalDoc(String value) {
            if (value != null && !value.equals("")) {
                this.InfoAdicionalDoc = value;
            }
        }
    }

    /////////EMISOR///////////////////////
    @XMLSequence({"RUCEmisor", "RznSoc","NomComercial", "GiroEmis","Telefono", "CorreoEmisor", "EmiSucursal", "CdgDGISucur",
            "DomFiscal", "Ciudad", "Departamento", "InfoAdicionalEmisor"})
    public class Emisor  implements java.io.Serializable{

        @XStreamAlias("RUCEmisor")
        private String rUCEmisor = null;
        @XStreamAlias("RznSoc")
        private String rznSoc = null;
        @XStreamAlias("NomComercial")
        private String nomComercial = null;
        @XStreamAlias("GiroEmis")
        private String giroEmis = null;
        @XStreamImplicit(itemFieldName = "Telefono")
        private List<String> telefono;
        @XStreamAlias("CorreoEmisor")
        private String correoEmisor = null;
        @XStreamAlias("EmiSucursal")
        private String emiSucursal = null;
        @XStreamAlias("CdgDGISucur")
        private String cdgDGISucur = null;
        @XStreamAlias("DomFiscal")
        private String domFiscal = null;
        @XStreamAlias("Ciudad")
        private String ciudad = null;
        @XStreamAlias("Departamento")
        private String departamento = null;
        @XStreamAlias("InfoAdicionalEmisor")
        private String InfoAdicionalEmisor = null;

        public String getRUCEmisor() {
            if (this.rUCEmisor == null) {
                return "";
            } else {
                return this.rUCEmisor;
            }
        }

        public void setRUCEmisor(String value) {
            if (value != null && !value.equals("")) {
                this.rUCEmisor = value;
            }
        }

        public String getRznSoc() {
            if (this.rznSoc == null) {
                return "";
            } else {
                return this.rznSoc;
            }
        }

        public void setRznSoc(String value) {
            if (value != null && !value.equals("")) {
                this.rznSoc = value;
            }
        }

        public String getNomComercial() {
            if (this.nomComercial == null) {
                return "";
            } else {
                return this.nomComercial;
            }
        }

        public void setNomComercial(String value) {
            if (value != null && !value.equals("")) {
                this.nomComercial = value;
            }
        }

        public String getGiroEmis() {
            if (this.giroEmis == null) {
                return "";
            } else {
                return this.giroEmis;
            }
        }

        public void setGiroEmis(String value) {
            if (value != null && !value.equals("")) {
                this.giroEmis = value;
            }
        }

        public List<String> getTelefono() {
            return this.telefono;
        }

        public void setTelefono(List<String> value) {
            this.telefono = value;
        }

        public String getCorreoEmisor() {
            if (this.correoEmisor == null) {
                return "";
            } else {
                return this.correoEmisor;
            }
        }

        public void setCorreoEmisor(String value) {
            if (value != null && !value.equals("")) {
                this.correoEmisor = value;
            }
        }

        public String getEmiSucursal() {
            if (this.emiSucursal == null) {
                return "";
            } else {
                return this.emiSucursal;
            }
        }

        public void setEmiSucursal(String value) {
            if (value != null && !value.equals("")) {
                this.emiSucursal = value;
            }
        }

        public String getCdgDGISucur() {
            if (this.cdgDGISucur == null) {
                return "";
            } else {
                return this.cdgDGISucur;
            }
        }

        public void setCdgDGISucur(String value) {
            if (value != null && !value.equals("")) {
                this.cdgDGISucur = value;
            }
        }

        public String getDomFiscal() {
            if (this.domFiscal == null) {
                return "";
            } else {
                return this.domFiscal;
            }
        }

        public void setDomFiscal(String value) {
            if (value != null && !value.equals("")) {
                this.domFiscal = value;
            }
        }

        public String getCiudad() {
            if (this.ciudad == null) {
                return "";
            } else {
                return this.ciudad;
            }
        }

        public void setCiudad(String value) {
            if (value != null && !value.equals("")) {
                this.ciudad = value;
            }
        }

        public String getDepartamento() {
            if (this.departamento == null) {
                return "";
            } else {
                return this.departamento;
            }
        }

        public void setDepartamento(String value) {
            if (value != null && !value.equals("")) {
                this.departamento = value;
            }
        }

        public String getInfoAdicionalEmisor() {
            if (this.InfoAdicionalEmisor == null) {
                return "";
            } else {
                return this.InfoAdicionalEmisor;
            }
        }

        public void setInfoAdicionalEmisor(String value) {
            if (value != null && !value.equals("")) {
                this.InfoAdicionalEmisor = value;
            }
        }
    }

    /////////TOTALES CLASS////////////////
    @XMLSequence({"TpoMoneda", "TpoCambio", "TpoCambioSpecified", "MntNoGrv", "mntNoGrvSpecified", "MntExpoyAsim", "mntExpoyAsimSpecified",
            "MntImpuestoPerc", "mntImpuestoPercSpecified", "MntIVaenSusp", "mntIVaenSuspSpecified", "MntNetoIvaTasaMin", "mntNetoIvaTasaMinSpecified",
            "MntNetoIVATasaBasica", "mntNetoIVATasaBasicaSpecified", "MntNetoIVAOtra", "mntNetoIVAOtraSpecified", "IVATasaMin", "iVATasaMinSpecified",
            "IVATasaBasica", "iVATasaBasicaSpecified", "MntIVATasaMin", "mntIVATasaMinSpecified", "MntIVATasaBasica", "mntIVATasaBasicaSpecified", "MntIVAOtra",
            "mntIVAOtraSpecified", "MntTotal", "MntTotRetenido", "mntTotRetenidoSpecified", "MntTotCredFisc", "MntTotCredFiscSpecified", "CantLinDet",
            "RetencPercep", "MontoNF", "montoNFSpecified", "MntPagar"})
    public class Totales  implements java.io.Serializable{

        private String TpoMoneda = "";
        private String TpoCambio = null;
        @XStreamOmitField
        private boolean TpoCambioSpecified;
        private java.math.BigDecimal MntNoGrv = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        @XStreamOmitField
        private boolean mntNoGrvSpecified;
        private java.math.BigDecimal MntExpoyAsim = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        @XStreamOmitField
        private boolean mntExpoyAsimSpecified;
        private java.math.BigDecimal MntImpuestoPerc = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        @XStreamOmitField
        private boolean mntImpuestoPercSpecified;
        private java.math.BigDecimal MntIVaenSusp = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        @XStreamOmitField
        private boolean mntIVaenSuspSpecified;
        private java.math.BigDecimal MntNetoIvaTasaMin = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        @XStreamOmitField
        private boolean mntNetoIvaTasaMinSpecified;
        private java.math.BigDecimal MntNetoIVATasaBasica = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        @XStreamOmitField
        private boolean mntNetoIVATasaBasicaSpecified;
        private java.math.BigDecimal MntNetoIVAOtra = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        @XStreamOmitField
        private boolean mntNetoIVAOtraSpecified;
        private java.math.BigDecimal IVATasaMin = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        @XStreamOmitField
        private boolean iVATasaMinSpecified;
        private java.math.BigDecimal IVATasaBasica = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        @XStreamOmitField
        private boolean iVATasaBasicaSpecified;
        private java.math.BigDecimal MntIVATasaMin = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        @XStreamOmitField
        private boolean mntIVATasaMinSpecified;
        private java.math.BigDecimal MntIVATasaBasica = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        @XStreamOmitField
        private boolean mntIVATasaBasicaSpecified;
        private java.math.BigDecimal MntIVAOtra = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        @XStreamOmitField
        private boolean mntIVAOtraSpecified;
        private java.math.BigDecimal MntTotal = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        private java.math.BigDecimal MntTotRetenido = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        @XStreamOmitField
        private boolean mntTotRetenidoSpecified;
        private java.math.BigDecimal MntTotCredFisc = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        @XStreamOmitField
        private boolean MntTotCredFiscSpecified;
        private String CantLinDet;
        @XStreamImplicit(itemFieldName = "RetencPercep")
        private List<TotalesRetencPercep> RetencPercep;
        private java.math.BigDecimal MontoNF = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        @XStreamOmitField
        private boolean montoNFSpecified;
        private java.math.BigDecimal MntPagar = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);

        public String getTpoMoneda() {
            return this.TpoMoneda;
        }

        public void setTpoMoneda(String value) {
            this.TpoMoneda = value;
        }

        public java.math.BigDecimal getTpoCambio() {
            if (this.TpoCambio != null) {
                return new java.math.BigDecimal(this.TpoCambio).setScale(2, RoundingMode.HALF_EVEN);
            } else {
                return new java.math.BigDecimal(0);
            }
        }

        public void setTpoCambio(java.math.BigDecimal value) {
            this.TpoCambio = String.valueOf(value.setScale(2, RoundingMode.HALF_EVEN).doubleValue());
        }

        public boolean getTpoCambioSpecified() {
            return this.TpoCambioSpecified;
        }

        public void setTpoCambioSpecified(boolean value) {
            this.TpoCambioSpecified = value;
        }

        public java.math.BigDecimal getMntNoGrv() {
            return this.MntNoGrv.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntNoGrv(java.math.BigDecimal value) {
            this.MntNoGrv = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getMntNoGrvSpecified() {
            return this.mntNoGrvSpecified;
        }

        public void setMntNoGrvSpecified(boolean value) {
            this.mntNoGrvSpecified = value;
        }

        public java.math.BigDecimal getMntExpoyAsim() {
            return this.MntExpoyAsim.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntExpoyAsim(java.math.BigDecimal value) {
            this.MntExpoyAsim = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getMntExpoyAsimSpecified() {
            return this.mntExpoyAsimSpecified;
        }

        public void setMntExpoyAsimSpecified(boolean value) {
            this.mntExpoyAsimSpecified = value;
        }

        public java.math.BigDecimal getMntImpuestoPerc() {
            return this.MntImpuestoPerc.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntImpuestoPerc(java.math.BigDecimal value) {
            this.MntImpuestoPerc = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getMntImpuestoPercSpecified() {
            return this.mntImpuestoPercSpecified;
        }

        public void setMntImpuestoPercSpecified(boolean value) {
            this.mntImpuestoPercSpecified = value;
        }

        public java.math.BigDecimal getMntIVaenSusp() {
            return this.MntIVaenSusp.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntIVaenSusp(java.math.BigDecimal value) {
            this.MntIVaenSusp = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getMntIVaenSuspSpecified() {
            return this.mntIVaenSuspSpecified;
        }

        public void setMntIVaenSuspSpecified(boolean value) {
            this.mntIVaenSuspSpecified = value;
        }

        public java.math.BigDecimal getMntNetoIvaTasaMin() {
            return this.MntNetoIvaTasaMin.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntNetoIvaTasaMin(java.math.BigDecimal value) {
            this.MntNetoIvaTasaMin = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getMntNetoIvaTasaMinSpecified() {
            return this.mntNetoIvaTasaMinSpecified;
        }

        public void setMntNetoIvaTasaMinSpecified(boolean value) {
            this.mntNetoIvaTasaMinSpecified = value;
        }

        public java.math.BigDecimal getMntNetoIVATasaBasica() {
            return this.MntNetoIVATasaBasica.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntNetoIVATasaBasica(java.math.BigDecimal value) {
            this.MntNetoIVATasaBasica = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getMntNetoIVATasaBasicaSpecified() {
            return this.mntNetoIVATasaBasicaSpecified;
        }

        public void setMntNetoIVATasaBasicaSpecified(boolean value) {
            this.mntNetoIVATasaBasicaSpecified = value;
        }

        public java.math.BigDecimal getMntNetoIVAOtra() {
            return this.MntNetoIVAOtra.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntNetoIVAOtra(java.math.BigDecimal value) {
            this.MntNetoIVAOtra = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getMntNetoIVAOtraSpecified() {
            return this.mntNetoIVAOtraSpecified;
        }

        public void setMntNetoIVAOtraSpecified(boolean value) {
            this.mntNetoIVAOtraSpecified = value;
        }

        public java.math.BigDecimal getIVATasaMin() {
            return this.IVATasaMin.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setIVATasaMin(java.math.BigDecimal value) {
            this.IVATasaMin = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getIVATasaMinSpecified() {
            return this.iVATasaMinSpecified;
        }

        public void setIVATasaMinSpecified(boolean value) {
            this.iVATasaMinSpecified = value;
        }

        public java.math.BigDecimal getIVATasaBasica() {
            return this.IVATasaBasica.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setIVATasaBasica(java.math.BigDecimal value) {
            this.IVATasaBasica = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getIVATasaBasicaSpecified() {
            return this.iVATasaBasicaSpecified;
        }

        public void setIVATasaBasicaSpecified(boolean value) {
            this.iVATasaBasicaSpecified = value;
        }

        public java.math.BigDecimal getMntIVATasaMin() {
            return this.MntIVATasaMin.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntIVATasaMin(java.math.BigDecimal value) {
            this.MntIVATasaMin = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getMntIVATasaMinSpecified() {
            return this.mntIVATasaMinSpecified;
        }

        public void setMntIVATasaMinSpecified(boolean value) {
            this.mntIVATasaMinSpecified = value;
        }

        public java.math.BigDecimal getMntIVATasaBasica() {
            return this.MntIVATasaBasica.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntIVATasaBasica(java.math.BigDecimal value) {
            this.MntIVATasaBasica = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getMntIVATasaBasicaSpecified() {
            return this.mntIVATasaBasicaSpecified;
        }

        public void setMntIVATasaBasicaSpecified(boolean value) {
            this.mntIVATasaBasicaSpecified = value;
        }

        public java.math.BigDecimal getMntIVAOtra() {
            return this.MntIVAOtra.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntIVAOtra(java.math.BigDecimal value) {
            this.MntIVAOtra = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getMntIVAOtraSpecified() {
            return this.mntIVAOtraSpecified;
        }

        public void setMntIVAOtraSpecified(boolean value) {
            this.mntIVAOtraSpecified = value;
        }

        public java.math.BigDecimal getMntTotal() {
            return this.MntTotal.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntTotal(java.math.BigDecimal value) {
            this.MntTotal = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public java.math.BigDecimal getMntTotRetenido() {
            return this.MntTotRetenido.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntTotRetenido(java.math.BigDecimal value) {
            this.MntTotRetenido = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getMntTotRetenidoSpecified() {
            return this.mntTotRetenidoSpecified;
        }

        public void setMntTotRetenidoSpecified(boolean value) {
            this.mntTotRetenidoSpecified = value;
        }

        public java.math.BigDecimal getMntTotCredFisc() {
            return this.MntTotCredFisc.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntTotCredFisc(java.math.BigDecimal value) {
            this.MntTotCredFisc = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getMntTotCredFiscSpecified() {
            return this.MntTotCredFiscSpecified;
        }

        public void setMntTotCredFiscSpecified(boolean value) {
            this.MntTotCredFiscSpecified = value;
        }

        public String getCantLinDet() {
            return this.CantLinDet;
        }

        public void setCantLinDet(String value) {
            this.CantLinDet = value;
        }

        public List<TotalesRetencPercep> getRetencPercep() {
            return this.RetencPercep;
        }

        public void setRetencPercep(List<TotalesRetencPercep> value) {
            this.RetencPercep = value;
        }

        public java.math.BigDecimal getMontoNF() {
            return this.MontoNF.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMontoNF(java.math.BigDecimal value) {
            this.MontoNF = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getMontoNFSpecified() {
            return this.montoNFSpecified;
        }

        public void setMontoNFSpecified(boolean value) {
            this.montoNFSpecified = value;
        }

        public java.math.BigDecimal getMntPagar() {
            return this.MntPagar.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntPagar(java.math.BigDecimal value) {
            this.MntPagar = value.setScale(2, RoundingMode.HALF_EVEN);
        }
    }

    public class Totales_ResgRetencPercep  implements java.io.Serializable{

        private String CodRet;
        private java.math.BigDecimal ValRetPerc = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);

        public String getCodRet() {
            return this.CodRet;
        }

        public void setCodRet(String value) {
            this.CodRet = value;
        }

        public java.math.BigDecimal getValRetPerc() {
            return this.ValRetPerc.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setValRetPerc(java.math.BigDecimal value) {
            this.ValRetPerc = value.setScale(2, RoundingMode.HALF_EVEN);
        }
    }

    @XMLSequence({"TpoMoneda", "TpoCambio","TpoCambioSpecified", "MntExpoyAsim","MntTotal", "CantLinDet", "MntPagar"})
    public class Totales_Rem_Exp  implements java.io.Serializable{

        private String TpoMoneda = null;
        private String TpoCambio = null;
        @XStreamOmitField
        private boolean TpoCambioSpecified;
        private java.math.BigDecimal MntExpoyAsim = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        private java.math.BigDecimal MntTotal = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        private String CantLinDet = null;
        private java.math.BigDecimal MntPagar = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);

        public String getTpoMoneda() {
            if (this.TpoMoneda == null) {
                return "";
            } else {
                return this.TpoMoneda;
            }
        }

        public void setTpoMoneda(String value) {
            if (value != null && !value.equals("")) {
                this.TpoMoneda = value;
            }
        }

        public java.math.BigDecimal getTpoCambio() {
            if (this.TpoCambio != null) {
                return new java.math.BigDecimal(this.TpoCambio).setScale(2, RoundingMode.HALF_EVEN);
            } else {
                return new java.math.BigDecimal(0);
            }
        }

        public void setTpoCambio(java.math.BigDecimal value) {
            this.TpoCambio = String.valueOf(value.setScale(2, RoundingMode.HALF_EVEN).doubleValue());
        }

        public boolean getTpoCambioSpecified() {
            return this.TpoCambioSpecified;
        }

        public void setTpoCambioSpecified(boolean value) {
            this.TpoCambioSpecified = value;
        }

        public java.math.BigDecimal getMntExpoyAsim() {
            return this.MntExpoyAsim.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntExpoyAsim(java.math.BigDecimal value) {
            this.MntExpoyAsim = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public java.math.BigDecimal getMntTotal() {
            return this.MntTotal.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntTotal(java.math.BigDecimal value) {
            this.MntTotal = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public String getCantLinDet() {
            return this.CantLinDet;
        }

        public void setCantLinDet(String value) {
            this.CantLinDet = value;
        }

        public java.math.BigDecimal getMntPagar() {
            return this.MntPagar.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntPagar(java.math.BigDecimal value) {
            this.MntPagar = value.setScale(2, RoundingMode.HALF_EVEN);
        }
    }

    @XMLSequence({"TpoMoneda", "TpoCambio","TpoCambioSpecified", "MntExpoyAsim", "MntTotal", "CantLinDet", "MontoNF", "MontoNFSpecified", "MntPagar"})
    public class Totales_Fact_Exp  implements java.io.Serializable{

        private String TpoMoneda = null;
        private String TpoCambio = null;
        @XStreamOmitField
        private boolean TpoCambioSpecified;
        private java.math.BigDecimal MntExpoyAsim = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        private java.math.BigDecimal MntTotal = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        private String CantLinDet;
        private java.math.BigDecimal MontoNF = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        @XStreamOmitField
        private boolean MontoNFSpecified;
        private java.math.BigDecimal MntPagar = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);

        public String getTpoMoneda() {
            if (this.TpoMoneda == null) {
                return "";
            } else {
                return this.TpoMoneda;
            }
        }

        public void setTpoMoneda(String value) {
            if (value != null && !value.equals("")) {
                this.TpoMoneda = value;
            }
        }

        public java.math.BigDecimal getTpoCambio() {
            if (this.TpoCambio != null) {
                return new java.math.BigDecimal(this.TpoCambio).setScale(2, RoundingMode.HALF_EVEN);
            } else {
                return new java.math.BigDecimal(0);
            }
        }

        public void setTpoCambio(java.math.BigDecimal value) {
            this.TpoCambio = String.valueOf(value.setScale(2, RoundingMode.HALF_EVEN).doubleValue());
        }

        public boolean getTpoCambioSpecified() {
            return this.TpoCambioSpecified;
        }

        public void setTpoCambioSpecified(boolean value) {
            this.TpoCambioSpecified = value;
        }

        public java.math.BigDecimal getMntExpoyAsim() {
            return this.MntExpoyAsim.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntExpoyAsim(java.math.BigDecimal value) {
            this.MntExpoyAsim = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public java.math.BigDecimal getMntTotal() {
            return this.MntTotal.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntTotal(java.math.BigDecimal value) {
            this.MntTotal = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public String getCantLinDet() {
            return this.CantLinDet;
        }

        public void setCantLinDet(String value) {
            this.CantLinDet = value;
        }

        public java.math.BigDecimal getMontoNF() {
            return this.MontoNF.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMontoNF(java.math.BigDecimal value) {
            this.MontoNF = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getMontoNFSpecified() {
            return this.MontoNFSpecified;
        }

        public void setMontoNFSpecified(boolean value) {
            this.MontoNFSpecified = value;
        }

        public java.math.BigDecimal getMntPagar() {
            return this.MntPagar.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntPagar(java.math.BigDecimal value) {
            this.MntPagar = value.setScale(2, RoundingMode.HALF_EVEN);
        }
    }

    @XMLSequence({"TpoMoneda", "TpoCambio","tpoCambioSpecified", "MntTotRetenido", "MntTotCredFisc", "MntTotCredFiscSpecified", "CantLinDet", "RetencPercep"})
    public class Totales_Resg  implements java.io.Serializable{

        private String TpoMoneda = null;
        private String TpoCambio = null;
        @XStreamOmitField
        private boolean tpoCambioSpecified;
        private java.math.BigDecimal MntTotRetenido = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        private java.math.BigDecimal MntTotCredFisc = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        @XStreamOmitField
        private boolean MntTotCredFiscSpecified;
        private String CantLinDet = null;
        @XStreamImplicit(itemFieldName = "RetencPercep")
        private List<Totales_ResgRetencPercep> RetencPercep;

        public String getTpoMoneda() {
            if (this.TpoMoneda == null) {
                return "";
            } else {
                return this.TpoMoneda;
            }
        }

        public void setTpoMoneda(String value) {
            if (value != null && !value.equals("")) {
                this.TpoMoneda = value;
            }
        }

        public java.math.BigDecimal getTpoCambio() {
            if (this.TpoCambio != null) {
                return new java.math.BigDecimal(this.TpoCambio).setScale(2, RoundingMode.HALF_EVEN);
            } else {
                return new java.math.BigDecimal(0);
            }
        }

        public void setTpoCambio(java.math.BigDecimal value) {
            this.TpoCambio = String.valueOf(value.setScale(2, RoundingMode.HALF_EVEN).doubleValue());
        }

        public boolean getTpoCambioSpecified() {
            return this.tpoCambioSpecified;
        }

        public void setTpoCambioSpecified(boolean value) {
            this.tpoCambioSpecified = value;
        }

        public java.math.BigDecimal getMntTotRetenido() {
            return this.MntTotRetenido.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntTotRetenido(java.math.BigDecimal value) {
            this.MntTotRetenido = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public java.math.BigDecimal getMntTotCredFisc() {
            return this.MntTotCredFisc.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntTotCredFisc(java.math.BigDecimal value) {
            this.MntTotCredFisc = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getMntTotCredFiscSpecified() {
            return this.MntTotCredFiscSpecified;
        }

        public void setMntTotCredFiscSpecified(boolean value) {
            this.MntTotCredFiscSpecified = value;
        }

        public String getCantLinDet() {
            if (this.CantLinDet == null) {
                return "";
            } else {
                return this.CantLinDet;
            }
        }

        public void setCantLinDet(String value) {
            if (value != null && !value.equals("")) {
                this.CantLinDet = value;
            }
        }

        public List<Totales_ResgRetencPercep> getRetencPercep() {
            return this.RetencPercep;
        }

        public void setRetencPercep(List<Totales_ResgRetencPercep> value) {
            this.RetencPercep = value;
        }
    }

    @XMLSequence({"CodRet", "ValRetPerc"})
    public class TotalesRetencPercep  implements java.io.Serializable{

        private String CodRet;
        private java.math.BigDecimal ValRetPerc = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);

        public String getCodRet() {
            return this.CodRet;
        }

        public void setCodRet(String value) {
            this.CodRet = value;
        }

        public java.math.BigDecimal getValRetPerc() {
            return this.ValRetPerc.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setValRetPerc(java.math.BigDecimal value) {
            this.ValRetPerc = value.setScale(2, RoundingMode.HALF_EVEN);
        }
    }

    @XMLSequence({"TpoMoneda", "TpoCambio","TpoCambioSpecified", "MntNoGrv", "mntNoGrvSpecified", "MntIVaenSusp", "mntIVaenSuspSpecified",
    "MntTotal", "MntTotRetenido", "mntTotRetenidoSpecified", "CantLinDet", "RetencPercep", "MontoNF", "montoNFSpecified", "MntPagar"})
    public class Totales_Boleta  implements java.io.Serializable{

        private String TpoMoneda = "";
		private String TpoCambio = null;
		@XStreamOmitField
		private boolean TpoCambioSpecified;
		private java.math.BigDecimal MntNoGrv = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
		@XStreamOmitField
		private boolean mntNoGrvSpecified;
		private java.math.BigDecimal MntIVaenSusp = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
		@XStreamOmitField
		private boolean mntIVaenSuspSpecified;
		private java.math.BigDecimal MntTotal = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
		private java.math.BigDecimal MntTotRetenido = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
		@XStreamOmitField
		private boolean mntTotRetenidoSpecified;
		private String CantLinDet;
		@XStreamImplicit(itemFieldName = "RetencPercep")
		private List<TotalesRetencPercep> RetencPercep;
		private java.math.BigDecimal MontoNF = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
		@XStreamOmitField
		private boolean montoNFSpecified;
		private java.math.BigDecimal MntPagar = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);

        public String getTpoMoneda() {
            return this.TpoMoneda;
        }

        public void setTpoMoneda(String value) {
            this.TpoMoneda = value;
        }

        public java.math.BigDecimal getTpoCambio() {
            if (this.TpoCambio != null) {
                return new java.math.BigDecimal(this.TpoCambio).setScale(2, RoundingMode.HALF_EVEN);
            } else {
                return new java.math.BigDecimal(0);
            }
        }

        public void setTpoCambio(java.math.BigDecimal value) {
            this.TpoCambio = String.valueOf(value.setScale(2, RoundingMode.HALF_EVEN).doubleValue());
        }

        public boolean getTpoCambioSpecified() {
            return this.TpoCambioSpecified;
        }

        public void setTpoCambioSpecified(boolean value) {
            this.TpoCambioSpecified = value;
        }

        public java.math.BigDecimal getMntNoGrv() {
            return this.MntNoGrv.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntNoGrv(java.math.BigDecimal value) {
            this.MntNoGrv = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getMntNoGrvSpecified() {
            return this.mntNoGrvSpecified;
        }

        public void setMntNoGrvSpecified(boolean value) {
            this.mntNoGrvSpecified = value;
        }

        public java.math.BigDecimal getMntIVaenSusp() {
            return this.MntIVaenSusp.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntIVaenSusp(java.math.BigDecimal value) {
            this.MntIVaenSusp = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getMntIVaenSuspSpecified() {
            return this.mntIVaenSuspSpecified;
        }

        public void setMntIVaenSuspSpecified(boolean value) {
            this.mntIVaenSuspSpecified = value;
        }

        public java.math.BigDecimal getMntTotal() {
            return this.MntTotal.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntTotal(java.math.BigDecimal value) {
            this.MntTotal = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public java.math.BigDecimal getMntTotRetenido() {
            return this.MntTotRetenido.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntTotRetenido(java.math.BigDecimal value) {
            this.MntTotRetenido = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getMntTotRetenidoSpecified() {
            return this.mntTotRetenidoSpecified;
        }

        public void setMntTotRetenidoSpecified(boolean value) {
            this.mntTotRetenidoSpecified = value;
        }

        public String getCantLinDet() {
            return this.CantLinDet;
        }

        public void setCantLinDet(String value) {
            this.CantLinDet = value;
        }

        public List<TotalesRetencPercep> getRetencPercep() {
            return this.RetencPercep;
        }

        public void setRetencPercep(List<TotalesRetencPercep> value) {
            this.RetencPercep = value;
        }

        public java.math.BigDecimal getMontoNF() {
            return this.MontoNF.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMontoNF(java.math.BigDecimal value) {
            this.MontoNF = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getMontoNFSpecified() {
            return this.montoNFSpecified;
        }

        public void setMontoNFSpecified(boolean value) {
            this.montoNFSpecified = value;
        }

        public java.math.BigDecimal getMntPagar() {
            return this.MntPagar.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntPagar(java.math.BigDecimal value) {
            this.MntPagar = value.setScale(2, RoundingMode.HALF_EVEN);
        }
    }

    /////////RECEPTOR CLASS////////////////
    @XMLSequence({"TipoDocRecep", "TipoDocRecepSpecified","CodPaisRecep", "CodPaisRecepSpecified", "DocRecep", "DocRecepExt", "RznSocRecep", "DirRecep", "CiudadRecep", "DeptoRecep", "PaisRecep", "Cp", "InfoAdicional", "LugarDestEnt", "CompraID"})
    public class Receptor_Tck  implements java.io.Serializable{

        private int TipoDocRecep;
        @XStreamOmitField
        private boolean TipoDocRecepSpecified;
        private String CodPaisRecep = null;
        @XStreamOmitField
        private boolean CodPaisRecepSpecified;
        private String DocRecep = null;
        private String DocRecepExt = null;
        private String RznSocRecep = null;
        private String DirRecep = null;
        private String CiudadRecep = null;
        private String DeptoRecep = null;
        private String PaisRecep = null;
        private String Cp = null;
        private String InfoAdicional = null;
        private String LugarDestEnt = null;
        private String CompraID = null;

        public DocType getTipoDocRecep() {
            return DocType.forValue(this.TipoDocRecep);
        }

        public void setTipoDocRecep(DocType value) {
            this.TipoDocRecep = value.getValue();
        }

        public boolean getTipoDocRecepSpecified() {
            return this.TipoDocRecepSpecified;
        }

        public void setTipoDocRecepSpecified(boolean value) {
            this.TipoDocRecepSpecified = value;
        }

        public String getCodPaisRecep() {
            if (this.CodPaisRecep == null) {
                return "";
            } else {
                return this.CodPaisRecep;
            }
        }

        public void setCodPaisRecep(String value) {
            if (value != null && !value.equals("")) {
                this.CodPaisRecep = value;
            }
        }

        public boolean getCodPaisRecepSpecified() {
            return this.CodPaisRecepSpecified;
        }

        public void setCodPaisRecepSpecified(boolean value) {
            this.CodPaisRecepSpecified = value;
        }

        public String getDocRecep() {
            if (this.DocRecep == null) {
                return "";
            } else {
                return this.DocRecep;
            }
        }

        public void setDocRecep(String value) {
            if (value != null && !value.equals("")) {
                this.DocRecep = value;
            }
        }

        public String getDocRecepExt() {
            if (this.DocRecepExt == null) {
                return "";
            } else {
                return this.DocRecepExt;
            }
        }

        public void setDocRecepExt(String value) {
            if (value != null && !value.equals("")) {
                this.DocRecepExt = value;
            }
        }

        public String getRznSocRecep() {
            if (this.RznSocRecep == null) {
                return "";
            } else {
                return this.RznSocRecep;
            }
        }

        public void setRznSocRecep(String value) {
            if (value != null && !value.equals("")) {
                this.RznSocRecep = value;
            }
        }

        public String getDirRecep() {
            if (this.DirRecep == null) {
                return "";
            } else {
                return this.DirRecep;
            }
        }

        public void setDirRecep(String value) {
            if (value != null && !value.equals("")) {
                this.DirRecep = value;
            }
        }

        public String getCiudadRecep() {
            if (this.CiudadRecep == null) {
                return "";
            } else {
                return this.CiudadRecep;
            }
        }

        public void setCiudadRecep(String value) {
            if (value != null && !value.equals("")) {
                this.CiudadRecep = value;
            }
        }

        public String getDeptoRecep() {
            if (this.DeptoRecep == null) {
                return "";
            } else {
                return this.DeptoRecep;
            }
        }

        public void setDeptoRecep(String value) {
            if (value != null && !value.equals("")) {
                this.DeptoRecep = value;
            }
        }

        public String getPaisRecep() {
            if (this.PaisRecep == null) {
                return "";
            } else {
                return this.PaisRecep;
            }
        }

        public void setPaisRecep(String value) {
            if (value != null && !value.equals("")) {
                this.PaisRecep = value;
            }
        }

        public String getCP() {
            if (this.Cp == null) {
                return "";
            } else {
                return this.Cp;
            }
        }

        public void setCP(String value) {
            if (value != null && !value.equals("")) {
                this.PaisRecep = value;
            }
        }

        public String getInfoAdicional() {
            if (this.InfoAdicional == null) {
                return "";
            } else {
                return this.InfoAdicional;
            }
        }

        public void setInfoAdicional(String value) {
            if (value != null && !value.equals("")) {
                this.InfoAdicional = value;
            }
        }

        public String getLugarDestEnt() {
            if (this.LugarDestEnt == null) {
                return "";
            } else {
                return this.LugarDestEnt;
            }
        }

        public void setLugarDestEnt(String value) {
            if (value != null && !value.equals("")) {
                this.LugarDestEnt = value;
            }
        }

        public String getCompraID() {
            if (this.CompraID != null) {
                return this.CompraID;
            } else {
                return null;
            }
        }

        public void setCompraID(String value) {
            if (value != null && !value.equals("")) {
                this.CompraID = value;
            }
        }
    }

    @XMLSequence({"TipoDocRecep", "TipoDocRecepSpecified","CodPaisRecep", "CodPaisRecepSpecified", "DocRecep", "DocRecepExt", "RznSocRecep", "DirRecep", "CiudadRecep", "DeptoRecep", "PaisRecep", "Cp", "InfoAdicional"})
    public class Receptor_Resg  implements java.io.Serializable{

        private int TipoDocRecep;
        @XStreamOmitField
        private boolean TipoDocRecepSpecified;
        private String CodPaisRecep = null;
        @XStreamOmitField
        private boolean CodPaisRecepSpecified;
        private String DocRecep = null;
        private String DocRecepExt = null;
        private String RznSocRecep = null;
        private String DirRecep = null;
        private String CiudadRecep = null;
        private String DeptoRecep = null;
        private String PaisRecep = null;
        private String Cp = null;
        private String InfoAdicional = null;

        public DocType getTipoDocRecep() {
            return DocType.forValue(this.TipoDocRecep);
        }

        public void setTipoDocRecep(DocType value) {
            this.TipoDocRecep = value.getValue();
        }

        public boolean getTipoDocRecepSpecified() {
            return this.TipoDocRecepSpecified;
        }

        public void setTipoDocRecepSpecified(boolean value) {
            this.TipoDocRecepSpecified = value;
        }

        public String getCodPaisRecep() {
            if (this.CodPaisRecep == null) {
                return "";
            } else {
                return this.CodPaisRecep;
            }
        }

        public void setCodPaisRecep(String value) {
            if (value != null && !value.equals("")) {
                this.CodPaisRecep = value;
            }
        }

        public boolean getCodPaisRecepSpecified() {
            return this.CodPaisRecepSpecified;
        }

        public void setCodPaisRecepSpecified(boolean value) {
            this.CodPaisRecepSpecified = value;
        }

        public String getDocRecep() {
            if (this.DocRecep == null) {
                return "";
            } else {
                return this.DocRecep;
            }
        }

        public void setDocRecep(String value) {
            if (value != null && !value.equals("")) {
                this.DocRecep = value;
            }
        }

        public String getDocRecepExt() {
            if (this.DocRecepExt == null) {
                return "";
            } else {
                return this.DocRecepExt;
            }
        }

        public void setDocRecepExt(String value) {
            if (value != null && !value.equals("")) {
                this.DocRecepExt = value;
            }
        }

        public String getRznSocRecep() {
            if (this.RznSocRecep == null) {
                return "";
            } else {
                return this.RznSocRecep;
            }
        }

        public void setRznSocRecep(String value) {
            if (value != null && !value.equals("")) {
                this.RznSocRecep = value;
            }
        }

        public String getDirRecep() {
            if (this.DirRecep == null) {
                return "";
            } else {
                return this.DirRecep;
            }
        }

        public void setDirRecep(String value) {
            if (value != null && !value.equals("")) {
                this.DirRecep = value;
            }
        }

        public String getCiudadRecep() {
            if (this.CiudadRecep == null) {
                return "";
            } else {
                return this.CiudadRecep;
            }
        }

        public void setCiudadRecep(String value) {
            if (value != null && !value.equals("")) {
                this.CiudadRecep = value;
            }
        }

        public String getDeptoRecep() {
            if (this.DeptoRecep == null) {
                return "";
            } else {
                return this.DeptoRecep;
            }
        }

        public void setDeptoRecep(String value) {
            if (value != null && !value.equals("")) {
                this.DeptoRecep = value;
            }
        }

        public String getPaisRecep() {
            if (this.PaisRecep == null) {
                return "";
            } else {
                return this.PaisRecep;
            }
        }

        public void setPaisRecep(String value) {
            if (value != null && !value.equals("")) {
                this.PaisRecep = value;
            }
        }

        public String getCP() {
            if (this.Cp == null) {
                return "";
            } else {
                return this.Cp;
            }
        }

        public void setCP(String value) {
            if (value != null && !value.equals("")) {
                this.PaisRecep = value;
            }
        }

        public String getInfoAdicional() {
            if (this.InfoAdicional == null) {
                return "";
            } else {
                return this.InfoAdicional;
            }
        }

        public void setInfoAdicional(String value) {
            if (value != null && !value.equals("")) {
                this.InfoAdicional = value;
            }
        }
    }

    @XMLSequence({"TipoDocRecep", "TipoDocRecepSpecified","CodPaisRecep", "CodPaisRecepSpecified", "DocRecep", "DocRecepExt", "RznSocRecep", "DirRecep", "CiudadRecep", "DeptoRecep", "PaisRecep", "Cp", "InfoAdicional", "LugarDestEnt", "CompraID"})
    public class Receptor_Rem_Exp  implements java.io.Serializable{

        private int TipoDocRecep;
        @XStreamOmitField
        private boolean TipoDocRecepSpecified;
        private String CodPaisRecep = null;
        @XStreamOmitField
        private boolean CodPaisRecepSpecified;
        private String DocRecep = null;
        private String DocRecepExt = null;
        private String RznSocRecep = null;
        private String DirRecep = null;
        private String CiudadRecep = null;
        private String DeptoRecep = null;
        private String PaisRecep = null;
        private String Cp = null;
        private String InfoAdicional = null;
        private String LugarDestEnt = null;
        private String CompraID = null;

        public DocType getTipoDocRecep() {
            return DocType.forValue(this.TipoDocRecep);
        }

        public void setTipoDocRecep(DocType value) {
            this.TipoDocRecep = value.getValue();
        }

        public boolean getTipoDocRecepSpecified() {
            return this.TipoDocRecepSpecified;
        }

        public void setTipoDocRecepSpecified(boolean value) {
            this.TipoDocRecepSpecified = value;
        }

        public String getCodPaisRecep() {
            if (this.CodPaisRecep == null) {
                return "";
            } else {
                return this.CodPaisRecep;
            }
        }

        public void setCodPaisRecep(String value) {
            if (value != null && !value.equals("")) {
                this.CodPaisRecep = value;
            }
        }

        public boolean getCodPaisRecepSpecified() {
            return this.CodPaisRecepSpecified;
        }

        public void setCodPaisRecepSpecified(boolean value) {
            this.CodPaisRecepSpecified = value;
        }

        public String getDocRecep() {
            if (this.DocRecep == null) {
                return "";
            } else {
                return this.DocRecep;
            }
        }

        public void setDocRecep(String value) {
            if (value != null && !value.equals("")) {
                this.DocRecep = value;
            }
        }

        public String getDocRecepExt() {
            if (this.DocRecepExt == null) {
                return "";
            } else {
                return this.DocRecepExt;
            }
        }

        public void setDocRecepExt(String value) {
            if (value != null && !value.equals("")) {
                this.DocRecepExt = value;
            }
        }

        public String getRznSocRecep() {
            if (this.RznSocRecep == null) {
                return "";
            } else {
                return this.RznSocRecep;
            }
        }

        public void setRznSocRecep(String value) {
            if (value != null && !value.equals("")) {
                this.RznSocRecep = value;
            }
        }

        public String getDirRecep() {
            if (this.DirRecep == null) {
                return "";
            } else {
                return this.DirRecep;
            }
        }

        public void setDirRecep(String value) {
            if (value != null && !value.equals("")) {
                this.DirRecep = value;
            }
        }

        public String getCiudadRecep() {
            if (this.CiudadRecep == null) {
                return "";
            } else {
                return this.CiudadRecep;
            }
        }

        public void setCiudadRecep(String value) {
            if (value != null && !value.equals("")) {
                this.CiudadRecep = value;
            }
        }

        public String getDeptoRecep() {
            if (this.DeptoRecep == null) {
                return "";
            } else {
                return this.DeptoRecep;
            }
        }

        public void setDeptoRecep(String value) {
            if (value != null && !value.equals("")) {
                this.DeptoRecep = value;
            }
        }

        public String getPaisRecep() {
            if (this.PaisRecep == null) {
                return "";
            } else {
                return this.PaisRecep;
            }
        }

        public void setPaisRecep(String value) {
            if (value != null && !value.equals("")) {
                this.PaisRecep = value;
            }
        }

        public String getCP() {
            if (this.Cp == null) {
                return "";
            } else {
                return this.Cp;
            }
        }

        public void setCP(String value) {
            if (value != null && !value.equals("")) {
                this.PaisRecep = value;
            }
        }

        public String getInfoAdicional() {
            if (this.InfoAdicional == null) {
                return "";
            } else {
                return this.InfoAdicional;
            }
        }

        public void setInfoAdicional(String value) {
            if (value != null && !value.equals("")) {
                this.InfoAdicional = value;
            }
        }

        public String getLugarDestEnt() {
            if (this.LugarDestEnt == null) {
                return "";
            } else {
                return this.LugarDestEnt;
            }
        }

        public void setLugarDestEnt(String value) {
            if (value != null && !value.equals("")) {
                this.LugarDestEnt = value;
            }
        }

        public String getCompraID() {
            if (this.CompraID == null) {
                return "";
            } else {
                return this.CompraID;
            }
        }

        public void setCompraID(String value) {
            if (value != null && !value.equals("")) {
                this.CompraID = value;
            }
        }
    }

    @XMLSequence({"TipoDocRecep", "TipoDocRecepSpecified","CodPaisRecep", "CodPaisRecepSpecified", "DocRecep", "DocRecepExt", "RznSocRecep", "DirRecep", "CiudadRecep", "DeptoRecep", "PaisRecep", "Cp", "InfoAdicional", "LugarDestEnt", "CompraID"})
    public class Receptor_Rem  implements java.io.Serializable{

        private int TipoDocRecep;
        @XStreamOmitField
        private boolean TipoDocRecepSpecified;
        private String CodPaisRecep = null;
        @XStreamOmitField
        private boolean CodPaisRecepSpecified;
        private String DocRecep = null;
        private String DocRecepExt = null;
        private String RznSocRecep = null;
        private String DirRecep = null;
        private String CiudadRecep = null;
        private String DeptoRecep = null;
        private String PaisRecep = null;
        private String Cp = null;
        private String InfoAdicional = null;
        private String LugarDestEnt = null;
        private String CompraID = null;

        public DocType getTipoDocRecep() {
            return DocType.forValue(this.TipoDocRecep);
        }

        public void setTipoDocRecep(DocType value) {
            this.TipoDocRecep = value.getValue();
        }

        public boolean getTipoDocRecepSpecified() {
            return this.TipoDocRecepSpecified;
        }

        public void setTipoDocRecepSpecified(boolean value) {
            this.TipoDocRecepSpecified = value;
        }

        public String getCodPaisRecep() {
            if (this.CodPaisRecep == null) {
                return "";
            } else {
                return this.CodPaisRecep;
            }
        }

        public void setCodPaisRecep(String value) {
            if (value != null && !value.equals("")) {
                this.CodPaisRecep = value;
            }
        }

        public boolean getCodPaisRecepSpecified() {
            return this.CodPaisRecepSpecified;
        }

        public void setCodPaisRecepSpecified(boolean value) {
            this.CodPaisRecepSpecified = value;
        }

        public String getDocRecep() {
            if (this.DocRecep == null) {
                return "";
            } else {
                return this.DocRecep;
            }
        }

        public void setDocRecep(String value) {
            if (value != null && !value.equals("")) {
                this.DocRecep = value;
            }
        }

        public String getDocRecepExt() {
            if (this.DocRecepExt == null) {
                return "";
            } else {
                return this.DocRecepExt;
            }
        }

        public void setDocRecepExt(String value) {
            if (value != null && !value.equals("")) {
                this.DocRecepExt = value;
            }
        }

        public String getRznSocRecep() {
            if (this.RznSocRecep == null) {
                return "";
            } else {
                return this.RznSocRecep;
            }
        }

        public void setRznSocRecep(String value) {
            if (value != null && !value.equals("")) {
                this.RznSocRecep = value;
            }
        }

        public String getDirRecep() {
            if (this.DirRecep == null) {
                return "";
            } else {
                return this.DirRecep;
            }
        }

        public void setDirRecep(String value) {
            if (value != null && !value.equals("")) {
                this.DirRecep = value;
            }
        }

        public String getCiudadRecep() {
            if (this.CiudadRecep == null) {
                return "";
            } else {
                return this.CiudadRecep;
            }
        }

        public void setCiudadRecep(String value) {
            if (value != null && !value.equals("")) {
                this.CiudadRecep = value;
            }
        }

        public String getDeptoRecep() {
            if (this.DeptoRecep == null) {
                return "";
            } else {
                return this.DeptoRecep;
            }
        }

        public void setDeptoRecep(String value) {
            if (value != null && !value.equals("")) {
                this.DeptoRecep = value;
            }
        }

        public String getPaisRecep() {
            if (this.PaisRecep == null) {
                return "";
            } else {
                return this.PaisRecep;
            }
        }

        public void setPaisRecep(String value) {
            if (value != null && !value.equals("")) {
                this.PaisRecep = value;
            }
        }

        public String getCP() {
            if (this.Cp == null) {
                return "";
            } else {
                return this.Cp;
            }
        }

        public void setCP(String value) {
            if (value != null && !value.equals("")) {
                this.PaisRecep = value;
            }
        }

        public String getInfoAdicional() {
            if (this.InfoAdicional == null) {
                return "";
            } else {
                return this.InfoAdicional;
            }
        }

        public void setInfoAdicional(String value) {
            if (value != null && !value.equals("")) {
                this.InfoAdicional = value;
            }
        }

        public String getLugarDestEnt() {
            if (this.LugarDestEnt == null) {
                return "";
            } else {
                return this.LugarDestEnt;
            }
        }

        public void setLugarDestEnt(String value) {
            if (value != null && !value.equals("")) {
                this.LugarDestEnt = value;
            }
        }

        public String getCompraID() {
            if (this.CompraID == null) {
                return "";
            } else {
                return this.CompraID;
            }
        }

        public void setCompraID(String value) {
            if (value != null && !value.equals("")) {
                this.CompraID = value;
            }
        }
    }

    @XMLSequence({"TipoDocRecep", "TipoDocRecepSpecified","CodPaisRecep", "CodPaisRecepSpecified", "DocRecep", "DocRecepExt", "RznSocRecep", "DirRecep", "CiudadRecep", "DeptoRecep", "PaisRecep", "Cp", "InfoAdicional", "LugarDestEnt", "CompraID"})
    public class Receptor_Fact_Exp  implements java.io.Serializable{

        private int TipoDocRecep;
        @XStreamOmitField
        private boolean TipoDocRecepSpecified;
        private String CodPaisRecep = null;
        @XStreamOmitField
        private boolean CodPaisRecepSpecified;
        private String DocRecep = null;
        private String DocRecepExt = null;
        private String RznSocRecep = null;
        private String DirRecep = null;
        private String CiudadRecep = null;
        private String DeptoRecep = null;
        private String PaisRecep = null;
        private String Cp = null;
        private String InfoAdicional = null;
        private String LugarDestEnt = null;
        private String CompraID = null;

        public DocType getTipoDocRecep() {
            return DocType.forValue(this.TipoDocRecep);
        }

        public void setTipoDocRecep(DocType value) {
            this.TipoDocRecep = value.getValue();
        }

        public boolean getTipoDocRecepSpecified() {
            return this.TipoDocRecepSpecified;
        }

        public void setTipoDocRecepSpecified(boolean value) {
            this.TipoDocRecepSpecified = value;
        }

        public String getCodPaisRecep() {
            if (this.CodPaisRecep == null) {
                return "";
            } else {
                return this.CodPaisRecep;
            }
        }

        public void setCodPaisRecep(String value) {
            if (value != null && !value.equals("")) {
                this.CodPaisRecep = value;
            }
        }

        public boolean getCodPaisRecepSpecified() {
            return this.CodPaisRecepSpecified;
        }

        public void setCodPaisRecepSpecified(boolean value) {
            this.CodPaisRecepSpecified = value;
        }

        public String getDocRecep() {
            if (this.DocRecep == null) {
                return "";
            } else {
                return this.DocRecep;
            }
        }

        public void setDocRecep(String value) {
            if (value != null && !value.equals("")) {
                this.DocRecep = value;
            }
        }

        public String getDocRecepExt() {
            if (this.DocRecepExt == null) {
                return "";
            } else {
                return this.DocRecepExt;
            }
        }

        public void setDocRecepExt(String value) {
            if (value != null && !value.equals("")) {
                this.DocRecepExt = value;
            }
        }

        public String getRznSocRecep() {
            if (this.RznSocRecep == null) {
                return "";
            } else {
                return this.RznSocRecep;
            }
        }

        public void setRznSocRecep(String value) {
            if (value != null && !value.equals("")) {
                this.RznSocRecep = value;
            }
        }

        public String getDirRecep() {
            if (this.DirRecep == null) {
                return "";
            } else {
                return this.DirRecep;
            }
        }

        public void setDirRecep(String value) {
            if (value != null && !value.equals("")) {
                this.DirRecep = value;
            }
        }

        public String getCiudadRecep() {
            if (this.CiudadRecep == null) {
                return "";
            } else {
                return this.CiudadRecep;
            }
        }

        public void setCiudadRecep(String value) {
            if (value != null && !value.equals("")) {
                this.CiudadRecep = value;
            }
        }

        public String getDeptoRecep() {
            if (this.DeptoRecep == null) {
                return "";
            } else {
                return this.DeptoRecep;
            }
        }

        public void setDeptoRecep(String value) {
            if (value != null && !value.equals("")) {
                this.DeptoRecep = value;
            }
        }

        public String getPaisRecep() {
            if (this.PaisRecep == null) {
                return "";
            } else {
                return this.PaisRecep;
            }
        }

        public void setPaisRecep(String value) {
            if (value != null && !value.equals("")) {
                this.PaisRecep = value;
            }
        }

        public String getCP() {
            if (this.Cp == null) {
                return "";
            } else {
                return this.Cp;
            }
        }

        public void setCP(String value) {
            if (value != null && !value.equals("")) {
                this.PaisRecep = value;
            }
        }

        public String getInfoAdicional() {
            if (this.InfoAdicional == null) {
                return "";
            } else {
                return this.InfoAdicional;
            }
        }

        public void setInfoAdicional(String value) {
            if (value != null && !value.equals("")) {
                this.InfoAdicional = value;
            }
        }

        public String getLugarDestEnt() {
            if (this.LugarDestEnt == null) {
                return "";
            } else {
                return this.LugarDestEnt;
            }
        }

        public void setLugarDestEnt(String value) {
            if (value != null && !value.equals("")) {
                this.LugarDestEnt = value;
            }
        }

        public String getCompraID() {
            if (this.CompraID == null) {
                return "";
            } else {
                return this.CompraID;
            }
        }

        public void setCompraID(String value) {
            if (value != null && !value.equals("")) {
                this.CompraID = value;
            }
        }
    }

    @XMLSequence({"TipoDocRecep", "CodPaisRecep","DocRecep", "RznSocRecep", "DirRecep", "CiudadRecep", "DeptoRecep", "PaisRecep", "Cp", "InfoAdicional", "LugarDestEnt", "CompraID"})
    public class Receptor_Fact  implements java.io.Serializable{

        @XStreamAlias("TipoDocRecep")
        private int tipoDocRecep;
        @XStreamAlias("CodPaisRecep")
        private String codPaisRecep = null;
        @XStreamAlias("DocRecep")
        private String docRecep = null;
        @XStreamAlias("RznSocRecep")
        private String rznSocRecep = null;
        @XStreamAlias("DirRecep")
        private String dirRecep = null;
        @XStreamAlias("CiudadRecep")
        private String ciudadRecep = null;
        @XStreamAlias("DeptoRecep")
        private String deptoRecep = null;
        @XStreamAlias("PaisRecep")
        private String paisRecep = null;
        @XStreamAlias("Cp")
        private String cp = null;
        @XStreamAlias("InfoAdicional")
        private String infoAdicional = null;
        @XStreamAlias("LugarDestEnt")
        private String lugarDestEnt = null;
        @XStreamAlias("CompraID")
        private String compraID = null;

        public Receptor_Fact() {
            super();
            this.tipoDocRecep = DocType.CI.getValue();
            this.codPaisRecep = "UY";
        }

        public DocType getTipoDocRecep() {
            return DocType.forValue(this.tipoDocRecep);
        }

        public void setTipoDocRecep(DocType value) {
            this.tipoDocRecep = value.getValue();
        }

        public String getCodPaisRecep() {
            if (this.codPaisRecep == null) {
                return "";
            } else {
                return this.codPaisRecep;
            }
        }

        public void setCodPaisRecep(String value) {
            if (value != null && !value.equals("")) {
                this.codPaisRecep = value;
            }
        }

        public String getDocRecep() {
            if (this.docRecep == null) {
                return "";
            } else {
                return this.docRecep;
            }
        }

        public void setDocRecep(String value) {
            if (value != null && !value.equals("")) {
                this.docRecep = value;
            }
        }

        public String getRznSocRecep() {
            if (this.rznSocRecep == null) {
                return "";
            } else {
                return this.rznSocRecep;
            }
        }

        public void setRznSocRecep(String value) {
            if (value != null && !value.equals("")) {
                this.rznSocRecep = value;
            }
        }

        public String getDirRecep() {
            if (this.dirRecep == null) {
                return "";
            } else {
                return this.dirRecep;
            }
        }

        public void setDirRecep(String value) {
            if (value != null && !value.equals("")) {
                this.dirRecep = value;
            }
        }

        public String getCiudadRecep() {
            if (this.ciudadRecep == null) {
                return "";
            } else {
                return this.ciudadRecep;
            }
        }

        public void setCiudadRecep(String value) {
            if (value != null && !value.equals("")) {
                this.ciudadRecep = value;
            }
        }

        public String getDeptoRecep() {
            if (this.deptoRecep == null) {
                return "";
            } else {
                return this.deptoRecep;
            }
        }

        public void setDeptoRecep(String value) {
            if (value != null && !value.equals("")) {
                this.deptoRecep = value;
            }
        }

        public String getPaisRecep() {
            if (this.paisRecep == null) {
                return "";
            } else {
                return this.paisRecep;
            }
        }

        public void setPaisRecep(String value) {
            if (value != null && !value.equals("")) {
                this.paisRecep = value;
            }
        }

        public String getCP() {
            if (this.cp == null) {
                return "";
            } else {
                return this.cp;
            }
        }

        public void setCP(String value) {
            if (value != null && !value.equals("")) {
                this.cp = value;
            }
        }

        public String getInfoAdicional() {
            if (this.infoAdicional == null) {
                return "";
            } else {
                return this.infoAdicional;
            }
        }

        public void setInfoAdicional(String value) {
            if (value != null && !value.equals("")) {
                this.infoAdicional = value;
            }
        }

        public String getLugarDestEnt() {
            if (this.lugarDestEnt == null) {
                return "";
            } else {
                return this.lugarDestEnt;
            }
        }

        public void setLugarDestEnt(String value) {
            if (value != null && !value.equals("")) {
                this.lugarDestEnt = value;
            }
        }

        public String getCompraID() {
            if (this.compraID == null) {
                return "";
            } else {
                return this.compraID;
            }
        }

        public void setCompraID(String value) {
            this.compraID = value;
        }
    }

    public class MediosPagoMedioPago  implements java.io.Serializable{

       @XStreamImplicit(itemFieldName = "MedioPago")
        private List<MedioPago_Item> Item = new ArrayList<MedioPago_Item>();

        public List<MedioPago_Item> getItem() {
            return Item;
        }

        public void setItem(List<MedioPago_Item> Item) {
            this.Item = Item;
        }

        @XMLSequence({"NroLinMP", "CodMP", "GlosaMP", "OrdenMP", "ValorPago"})
        public class MedioPago_Item {

            private String NroLinMP = null;
            private String CodMP = null;
            private String GlosaMP = null;
            private String OrdenMP = null;
            private java.math.BigDecimal ValorPago = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);

            public String getNroLinMP() {
                return this.NroLinMP;
            }

            public void setNroLinMP(String value) {
                if (value != null && !value.equals("")) {
                    this.NroLinMP = value;
                }
            }

            public String getCodMP() {
                if (this.CodMP == null) {
                    return "";
                } else {
                    return this.CodMP;
                }
            }

            public void setCodMP(String value) {
                if (value != null && !value.equals("")) {
                    this.CodMP = value;
                }
            }

            public String getGlosaMP() {
                if (this.GlosaMP == null) {
                    return "";
                } else {
                    return this.GlosaMP;
                }
            }

            public void setGlosaMP(String value) {
                if (value != null && !value.equals("")) {
                    this.GlosaMP = value;
                }
            }

            public String getOrdenMP() {
                if (this.OrdenMP == null) {
                    return "";
                } else {
                    return this.OrdenMP;
                }
            }

            public void setOrdenMP(String value) {
                if (value != null && !value.equals("")) {
                    this.OrdenMP = value;
                }
            }

            public java.math.BigDecimal getValorPago() {
                return this.ValorPago.setScale(2, RoundingMode.HALF_EVEN);
            }

            public void setValorPago(java.math.BigDecimal value) {
                this.ValorPago = value.setScale(2, RoundingMode.HALF_EVEN);
            }
        }
    }

    public class Compl_FiscalType  implements java.io.Serializable{

        private Compl_Fiscal_DataType Item;

        public Compl_Fiscal_DataType getItem() {
            return this.Item;
        }

        public void setItem(Compl_Fiscal_DataType value) {
            this.Item = value;
        }
    }

    /////////UTILS CLASS////////////////
    public class SubTotInfoSTI_Item  implements java.io.Serializable{

       @XStreamImplicit(itemFieldName = "STI_Item")
        private List<STI_Item> Item = new ArrayList<STI_Item>();

        public List<STI_Item> getItem() {
            return Item;
        }

        public void setItem(List<STI_Item> Item) {
            this.Item = Item;
        }

        @XMLSequence({"NroSTI", "GlosaSTI", "OrdenSTI", "ValSubtotSTI"})
        public class STI_Item {

            private String NroSTI = null;
            private String GlosaSTI = null;
            private String OrdenSTI = null;
            private java.math.BigDecimal ValSubtotSTI = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);

            public String getNroSTI() {
                if (this.NroSTI == null) {
                    return "";
                } else {
                    return this.NroSTI;
                }
            }

            public void setNroSTI(String value) {
                if (value != null && !value.equals("")) {
                    this.NroSTI = value;
                }
            }

            public String getGlosaSTI() {
                if (this.GlosaSTI == null) {
                    return "";
                } else {
                    return this.GlosaSTI;
                }
            }

            public void setGlosaSTI(String value) {
                if (value != null && !value.equals("")) {
                    this.GlosaSTI = value;
                }
            }

            public String getOrdenSTI() {
                if (this.OrdenSTI == null) {
                    return "";
                } else {
                    return this.OrdenSTI;
                }
            }

            public void setOrdenSTI(String value) {
                if (value != null && !value.equals("")) {
                    this.OrdenSTI = value;
                }
            }

            public java.math.BigDecimal getValSubtotSTI() {
                return this.ValSubtotSTI.setScale(2, RoundingMode.HALF_EVEN);
            }

            public void setValSubtotSTI(java.math.BigDecimal value) {
                this.ValSubtotSTI = value.setScale(2, RoundingMode.HALF_EVEN);
            }
        }
    }

    public class ReferenciaReferencia  implements java.io.Serializable{

         @XStreamImplicit(itemFieldName = "Referencia")
        private List<Referencia_Item> Item = new ArrayList<Referencia_Item>();

        public List<Referencia_Item> getItem() {
            return Item;
        }

        public void setItem(List<Referencia_Item> Item) {
            this.Item = Item;
        }

        @XMLSequence({"NroLinRef", "IndGlobal", "IndGlobalSpecified", "TpoDocRef", "TpoDocRefSpecified", "Serie", "NroCFERef", "RazonRef", "FechaCFEref", "FechaCFErefSpecified"})
        public class Referencia_Item {

            private String NroLinRef = null;
            private String IndGlobal = null;
            @XStreamOmitField
            private boolean IndGlobalSpecified;
            private String TpoDocRef = null;
            @XStreamOmitField
            private boolean TpoDocRefSpecified;
            private String Serie = null;
            private String NroCFERef = null;
            private String RazonRef = null;
            @XStreamOmitField
            private String FechaCFEref = null;
            @XStreamOmitField
            private boolean FechaCFErefSpecified;

            public Referencia_Item() {
                super();
            }

            public String getNroLinRef() {
                if (this.NroLinRef == null) {
                    return "";
                } else {
                    return this.NroLinRef;
                }
            }

            public void setNroLinRef(String value) {
                if (value != null && !value.equals("")) {
                    this.NroLinRef = value;
                }
            }

            public String getIndGlobal() {
                if (this.IndGlobal != null) {
                    return this.IndGlobal;
                } else {
                    return null;
                }
            }

            public void setIndGlobal(int value) {
                this.IndGlobal = String.valueOf(value);
            }

            public boolean getIndGlobalSpecified() {
                return this.IndGlobalSpecified;
            }

            public void setIndGlobalSpecified(boolean value) {
                this.IndGlobalSpecified = value;
            }

            public CFEType getTpoDocRef() {
                return CFEType.forValue(Integer.parseInt(this.TpoDocRef));
            }

            public void setTpoDocRef(CFEType value) {
                this.TpoDocRef = String.valueOf(value.getValue());
            }

            public boolean getTpoDocRefSpecified() {
                return this.TpoDocRefSpecified;
            }

            public void setTpoDocRefSpecified(boolean value) {
                this.TpoDocRefSpecified = value;
            }

            public String getSerie() {
                if (this.Serie == null) {
                    return "";
                } else {
                    return this.Serie;
                }
            }

            public void setSerie(String value) {
                if (value != null && !value.equals("")) {
                    this.Serie = value;
                }
            }

            public String getNroCFERef() {
                if (this.NroCFERef == null) {
                    return "";
                } else {
                    return this.NroCFERef;
                }
            }

            public void setNroCFERef(String value) {
                if (value != null && !value.equals("")) {
                    this.NroCFERef = value;
                }
            }

            public String getRazonRef() {
                if (this.RazonRef == null) {
                    return "";
                } else {
                    return this.RazonRef;
                }
            }

            public void setRazonRef(String value) {
                if (value != null && !value.equals("")) {
                    this.RazonRef = value;
                }
            }

            public java.util.Date getFechaCFEref() {
                SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
                java.util.Date aux = null;
                try {
                    aux = formato.parse(FechaCFEref);
                } catch (ParseException ex) {
                }
                return aux;
            }

            public void setFechaCFEref(java.util.Date value) {
                SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
                FechaCFEref = formato.format(value);
            }

            public boolean getFechaCFErefSpecified() {
                return this.FechaCFErefSpecified;
            }

            public void setFechaCFErefSpecified(boolean value) {
                this.FechaCFErefSpecified = value;
            }
        }
    }

    public class DscRcgGlobalDRG_Item  implements java.io.Serializable{

		@XStreamImplicit(itemFieldName = "DRG_Item")
		private List<DRG_Item> Item = new ArrayList<DRG_Item>();

		public List<DRG_Item> getItem() {
			return Item;
		}

		public void setItem(List<DRG_Item> Item) {
			this.Item = Item;
		}

      @XMLSequence({"NroLinDR", "TpoMovDR","TpoDR", "TpoDRSpecified","CodDR", "GlosaDR", "ValorDR", "IndFactDR", "IndFactDRSpecified"})
		public class DRG_Item {

			private String NroLinDR = null;
			private String TpoMovDR = null;
			private int TpoDR;
			@XStreamOmitField
			private boolean TpoDRSpecified;
			private String CodDR = null;
			private String GlosaDR = null;
			private String ValorDR = null;
			private String IndFactDR = null;
			@XStreamOmitField
			private boolean IndFactDRSpecified;

			public String getNroLinDR() {
				if (this.NroLinDR == null) {
					return "";
				} else {
					return this.NroLinDR;
				}
			}

			public void setNroLinDR(String value) {
				if (value != null && !value.equals("")) {
					this.NroLinDR = value;
				}
			}

			public String getTpoMovDR() {
				if (this.TpoMovDR == null) {
					return "";
				} else {
					return this.TpoMovDR;
				}
			}

			public void setTpoMovDR(String value) {
				if (value != null && !value.equals("")) {
					this.TpoMovDR = value;
				}
			}

			public TipoDRType getTpoDR() {
				return TipoDRType.forValue(this.TpoDR);
			}

			public void setTpoDR(TipoDRType value) {
				this.TpoDR = value.getValue();
			}

			public boolean getTpoDRSpecified() {
				return this.TpoDRSpecified;
			}

			public void setTpoDRSpecified(boolean value) {
				this.TpoDRSpecified = value;
			}

			public String getCodDR() {
				if (this.CodDR == null) {
					return "";
				} else {
					return this.CodDR;
				}
			}

			public void setCodDR(String value) {
				if (value != null && !value.equals("")) {
					this.CodDR = value;
				}
			}

			public String getGlosaDR() {
				if (this.GlosaDR == null) {
					return "";
				} else {
					return this.GlosaDR;
				}
			}

			public void setGlosaDR(String value) {
				if (value != null && !value.equals("")) {
					this.GlosaDR = value;
				}
			}

			public java.math.BigDecimal getValorDR() {
				if (this.ValorDR != null) {
					return new java.math.BigDecimal(this.ValorDR).setScale(2, RoundingMode.HALF_EVEN);
				} else {
					return new java.math.BigDecimal(0);
				}
			}

			public void setValorDR(java.math.BigDecimal value) {
				this.ValorDR = String.valueOf(value.setScale(2, RoundingMode.HALF_EVEN).doubleValue());
			}

			public DscRcgGlobalDRG_ItemIndFactDR getIndFactDR() {
				if (this.IndFactDR != null) {
					return DscRcgGlobalDRG_ItemIndFactDR.forValue(Integer.parseInt(this.IndFactDR));
				}
				return null;
			}

			public void setIndFactDR(DscRcgGlobalDRG_ItemIndFactDR value) {
				this.IndFactDR = String.valueOf(value.getValue());
			}

			public boolean getIndFactDRSpecified() {
				return this.IndFactDRSpecified;
			}

			public void setIndFactDRSpecified(boolean value) {
				this.IndFactDRSpecified = value;
			}
		}
	}

    @XMLSequence({"CAE_ID", "DNro","HNro", "FecVenc","CAEEspecial", "CausalCAEEsp"})
    public class CAEDataType  implements java.io.Serializable{

        private String CAE_ID = null;
        private String DNro = null;
        private String HNro = null;
        private java.util.Date FecVenc = new java.util.Date(0);
        private String CAEEspecial = null;
        private String CausalCAEEsp = null;

        public String getCAE_ID() {
            if (this.CAE_ID == null) {
                return "";
            } else {
                return this.CAE_ID;
            }
        }

        public void setCAE_ID(String value) {
            if (value != null && !value.equals("")) {
                this.CAE_ID = value;
            }
        }

        public String getDNro() {
            if (this.DNro == null) {
                return "";
            } else {
                return this.DNro;
            }
        }

        public void setDNro(String value) {
            if (value != null && !value.equals("")) {
                this.DNro = value;
            }
        }

        public String getHNro() {
            if (this.HNro == null) {
                return "";
            } else {
                return this.HNro;
            }
        }

        public void setHNro(String value) {
            if (value != null && !value.equals("")) {
                this.HNro = value;
            }
        }

        public java.util.Date getFecVenc() {
            return this.FecVenc;
        }

        public void setFecVenc(java.util.Date value) {
            this.FecVenc = value;
        }
        
         public String getCAEEspecial() {
            if (this.CAEEspecial == null) {
                return "";
            } else {
                return this.CAEEspecial;
            }
        }

        public void setCAEEspecial(String value) {
            if (value != null && !value.equals("")) {
                this.CAEEspecial = value;
            }
        }
        
         public String getCausalCAEEsp() {
            if (this.CausalCAEEsp == null) {
                return "";
            } else {
                return this.CausalCAEEsp;
            }
        }

        public void setCausalCAEEsp(String value) {
            if (value != null && !value.equals("")) {
                this.CausalCAEEsp = value;
            }
        }
    }

    @XMLSequence({"CodRet", "Tasa","TasaSpecified", "MntSujetoaRet","MntSujetoaRetSpecified", "ValRetPerc"})
    public class RetPerc  implements java.io.Serializable{

        private String CodRet;
        private java.math.BigDecimal Tasa = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        @XStreamOmitField
        private boolean TasaSpecified;
        private java.math.BigDecimal MntSujetoaRet = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        @XStreamOmitField
        private boolean MntSujetoaRetSpecified;
        private java.math.BigDecimal ValRetPerc = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);

        public String getCodRet() {
            return this.CodRet;
        }

        public void setCodRet(String value) {
            this.CodRet = value;
        }

        public java.math.BigDecimal getTasa() {
            return this.Tasa.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setTasa(java.math.BigDecimal value) {
            this.Tasa = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getTasaSpecified() {
            return this.TasaSpecified;
        }

        public void setTasaSpecified(boolean value) {
            this.TasaSpecified = value;
        }

        public java.math.BigDecimal getMntSujetoaRet() {
            return this.MntSujetoaRet.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntSujetoaRet(java.math.BigDecimal value) {
            this.MntSujetoaRet = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getMntSujetoaRetSpecified() {
            return this.MntSujetoaRetSpecified;
        }

        public void setMntSujetoaRetSpecified(boolean value) {
            this.MntSujetoaRetSpecified = value;
        }

        public java.math.BigDecimal getValRetPerc() {
            return this.ValRetPerc.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setValRetPerc(java.math.BigDecimal value) {
            this.ValRetPerc = value.setScale(2, RoundingMode.HALF_EVEN);
        }
    }
    @XMLSequence({"CodRet", "Tasa","TasaSpecified", "MntSujetoaRet", "ValRetPerc"})

    public class RetPerc_Resg  implements java.io.Serializable{

        private String CodRet;
        private java.math.BigDecimal Tasa = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        @XStreamOmitField
        private boolean TasaSpecified;
        private java.math.BigDecimal MntSujetoaRet = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
        private java.math.BigDecimal ValRetPerc = new java.math.BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);

        public String getCodRet() {
            return this.CodRet;
        }

        public void setCodRet(String value) {
            this.CodRet = value;
        }

        public java.math.BigDecimal getTasa() {
            return this.Tasa.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setTasa(java.math.BigDecimal value) {
            this.Tasa = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public boolean getTasaSpecified() {
            return this.TasaSpecified;
        }

        public void setTasaSpecified(boolean value) {
            this.TasaSpecified = value;
        }

        public java.math.BigDecimal getMntSujetoaRet() {
            return this.MntSujetoaRet.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setMntSujetoaRet(java.math.BigDecimal value) {
            this.MntSujetoaRet = value.setScale(2, RoundingMode.HALF_EVEN);
        }

        public java.math.BigDecimal getValRetPerc() {
            return this.ValRetPerc.setScale(2, RoundingMode.HALF_EVEN);
        }

        public void setValRetPerc(java.math.BigDecimal value) {
            this.ValRetPerc = value.setScale(2, RoundingMode.HALF_EVEN);
        }
    }

    @XMLSequence({"RUCEmisor", "TipoDocMdte","Pais", "DocMdte","NombreMdte"})
    public class Compl_Fiscal_DataType  implements java.io.Serializable{

        private String RUCEmisor = null;
        private int TipoDocMdte;
        private String Pais = null;
        private String DocMdte = null;
        private String NombreMdte = null;

        public String getRUCEmisor() {
            return this.RUCEmisor;
        }

        public void setRUCEmisor(String value) {
            if (value != null && !value.equals("")) {
                this.RUCEmisor = value;
            }
        }

        public DocTypemasNIE getTipoDocMdte() {
            return DocTypemasNIE.forValue(this.TipoDocMdte);
        }

        public void setTipoDocMdte(DocTypemasNIE value) {
            this.TipoDocMdte = value.getValue();
        }

        public String getPais() {
            if (this.Pais == null) {
                return "";
            } else {
                return this.Pais;
            }
        }

        public void setPais(String value) {
            if (value != null && !value.equals("")) {
                this.Pais = value;
            }
        }

        public String getDocMdte() {
            if (this.DocMdte == null) {
                return "";
            } else {
                return this.DocMdte;
            }
        }

        public void setDocMdte(String value) {
            if (value != null && !value.equals("")) {
                this.DocMdte = value;
            }
        }

        public String getNombreMdte() {
            if (this.NombreMdte == null) {
                return "";
            } else {
                return this.NombreMdte;
            }
        }

        public void setNombreMdte(String value) {
            if (value != null && !value.equals("")) {
                this.NombreMdte = value;
            }
        }
    }

    /////////ENUM DECLARAION////////////////
    public enum CFEType {

        Item101(101),
        Item102(102),
        Item103(103),
        Item111(111),
        Item112(112),
        Item113(113),
        Item121(121),
        Item122(122),
        Item123(123),
        Item124(124),
        Item131(131),
        Item132(132),
        Item133(133),
        Item141(141),
        Item142(142),
        Item143(143),
        Item151(151),
        Item152(152),
        Item153(153),
        Item181(181),
        Item182(182),
        Item201(201),
        Item202(202),
        Item203(203),
        Item211(211),
        Item212(212),
        Item213(213),
        Item221(221),
        Item222(222),
        Item223(223),
        Item224(224),
        Item231(231),
        Item232(232),
        Item233(233),
        Item241(241),
        Item242(242),
        Item243(243),
        Item251(251),
        Item252(252),
        Item253(253),
        Item281(281),
        Item282(282);
        private int intValue;
        private static java.util.HashMap<Integer, CFEType> mappings;

        private static java.util.HashMap<Integer, CFEType> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, CFEType>();
                    }

            return mappings;
        }

        private CFEType(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static CFEType forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum IdDoc_TckTipoCFE {

        Item101(101),
        Item102(102),
        Item103(103),
        Item131(131),
        Item132(132),
        Item133(133),
        Item201(201),
        Item202(202),
        Item203(203),
        Item231(231),
        Item232(232),
        Item233(233);
        private int intValue;
        private static java.util.HashMap<Integer, IdDoc_TckTipoCFE> mappings;

        private static java.util.HashMap<Integer, IdDoc_TckTipoCFE> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, IdDoc_TckTipoCFE>();
                    }

            return mappings;
        }

        private IdDoc_TckTipoCFE(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static IdDoc_TckTipoCFE forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum IdDoc_FactTipoCFE {

        Item111(111),
        Item112(112),
        Item113(113),
        Item141(141),
        Item142(142),
        Item143(143),
        Item211(211),
        Item212(212),
        Item213(213),
        Item241(241),
        Item242(242),
        Item243(243);
        private int intValue;
        private static java.util.HashMap<Integer, IdDoc_FactTipoCFE> mappings;

        private static java.util.HashMap<Integer, IdDoc_FactTipoCFE> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, IdDoc_FactTipoCFE>();
                    }

            return mappings;
        }

        private IdDoc_FactTipoCFE(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static IdDoc_FactTipoCFE forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum IdDoc_Fact_ExpTipoCFE {

        Item121(121),
        Item122(122),
        Item123(123),
        Item221(221),
        Item222(222),
        Item223(223);
        private int intValue;
        private static java.util.HashMap<Integer, IdDoc_Fact_ExpTipoCFE> mappings;

        private static java.util.HashMap<Integer, IdDoc_Fact_ExpTipoCFE> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, IdDoc_Fact_ExpTipoCFE>();
                    }

            return mappings;
        }

        private IdDoc_Fact_ExpTipoCFE(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static IdDoc_Fact_ExpTipoCFE forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum IdDoc_RemTipoCFE {

        Item181(181),
        Item281(281);
        private int intValue;
        private static java.util.HashMap<Integer, IdDoc_RemTipoCFE> mappings;

        private static java.util.HashMap<Integer, IdDoc_RemTipoCFE> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, IdDoc_RemTipoCFE>();
                    }

            return mappings;
        }

        private IdDoc_RemTipoCFE(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static IdDoc_RemTipoCFE forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum IdDoc_ResgTipoCFE {

        Item182(182),
        Item282(282);
        private int intValue;
        private static java.util.HashMap<Integer, IdDoc_ResgTipoCFE> mappings;

        private static java.util.HashMap<Integer, IdDoc_ResgTipoCFE> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, IdDoc_ResgTipoCFE>();
                    }

            return mappings;
        }

        private IdDoc_ResgTipoCFE(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static IdDoc_ResgTipoCFE forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum IdDoc_Rem_ExpTipoCFE {

        Item124(124),
        Item224(224);
        private int intValue;
        private static java.util.HashMap<Integer, IdDoc_Rem_ExpTipoCFE> mappings;

        private static java.util.HashMap<Integer, IdDoc_Rem_ExpTipoCFE> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, IdDoc_Rem_ExpTipoCFE>();
                    }

            return mappings;
        }

        private IdDoc_Rem_ExpTipoCFE(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static IdDoc_Rem_ExpTipoCFE forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum IdDoc_BoletaTipoCFE {

        Item151(151),
        Item152(152),
        Item153(153),
        Item251(251),
        Item252(252),
        Item253(253);
        private int intValue;
        private static java.util.HashMap<Integer, IdDoc_BoletaTipoCFE> mappings;

        private static java.util.HashMap<Integer, IdDoc_BoletaTipoCFE> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, IdDoc_BoletaTipoCFE>();
                    }

            return mappings;
        }

        private IdDoc_BoletaTipoCFE(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static IdDoc_BoletaTipoCFE forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum DocType {

        RUC(2),
        CI(3),
        Otros(4),
        Pasaporte(5),
        DNI(6),
        NIFE(7);
        private int intValue;
        private static java.util.HashMap<Integer, DocType> mappings;

        private static java.util.HashMap<Integer, DocType> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, DocType>();
                    }
            return mappings;
        }

        private DocType(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static DocType forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum TipMonType {

        AED,
        AFN,
        ALL,
        AMD,
        ANG,
        AOA,
        ARS,
        AUD,
        AWG,
        AZM,
        BAM,
        BBD,
        BDT,
        BGN,
        BHD,
        BIF,
        BMD,
        BND,
        BOB,
        BOV,
        BRL,
        BSD,
        BTN,
        BWP,
        BYR,
        BZD,
        CAD,
        CDF,
        CHF,
        CLF,
        CLP,
        CNY,
        COP,
        COU,
        CRC,
        CSD,
        CUP,
        CUC,
        CVE,
        CYP,
        CZK,
        DJF,
        DKK,
        DOP,
        DZD,
        EEK,
        EGP,
        ERN,
        ETB,
        EUR,
        FJD,
        FKP,
        GBP,
        GEL,
        GHS,
        GIP,
        GMD,
        GNF,
        GTQ,
        GYD,
        HKD,
        HNL,
        HRK,
        HTG,
        HUF,
        IDR,
        ILS,
        INR,
        IQD,
        IRR,
        ISK,
        JMD,
        JOD,
        JPY,
        KES,
        KGS,
        KHR,
        KMF,
        KPW,
        KRW,
        KWD,
        KYD,
        KZT,
        LAK,
        LBP,
        LKR,
        LRD,
        LSL,
        LTL,
        LVL,
        LYD,
        MAD,
        MDL,
        MGA,
        MKD,
        MMK,
        MNT,
        MOP,
        MRO,
        MTL,
        MUR,
        MVR,
        MWK,
        MXN,
        MXV,
        MYR,
        MZN,
        NAD,
        NGN,
        NIO,
        NOK,
        NPR,
        NZD,
        OMR,
        PAB,
        PEN,
        PGK,
        PHP,
        PKR,
        PLN,
        PYG,
        QAR,
        RON,
        RUB,
        RWF,
        SAR,
        SBD,
        SCR,
        SDG,
        SEK,
        SGD,
        SHP,
        SKK,
        SLL,
        SOS,
        SRD,
        STD,
        SYP,
        SZL,
        THB,
        TJS,
        TMT,
        TND,
        TOP,
        TRY,
        TTD,
        TWD,
        TZS,
        UAH,
        UGX,
        USD,
        USN,
        USS,
        UYU,
        UZS,
        VEF,
        VND,
        VUV,
        WST,
        XAF,
        XAG,
        XAU,
        XBA,
        XBB,
        XBC,
        XBD,
        XCD,
        XDR,
        XFO,
        XFU,
        XOF,
        XPD,
        XPF,
        XPT,
        XTS,
        XXX,
        YER,
        ZAR,
        ZMK,
        ZWL,
        UYI,
        UYR;

        public int getValue() {
            return this.ordinal();
        }

        public static TipMonType forValue(int value) {
            return values()[value];
        }
    }

    public enum TipoDRType {

        Item1(1),
        Item2(2);
        private int intValue;
        private static java.util.HashMap<Integer, TipoDRType> mappings;

        private static java.util.HashMap<Integer, TipoDRType> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, TipoDRType>();
                    }
            return mappings;
        }

        private TipoDRType(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static TipoDRType forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum DocTypemasNIE {

        Item1,
        Item2,
        Item3,
        Item4,
        Item5,
        Item6;

        public int getValue() {
            return this.ordinal();
        }

        public static DocTypemasNIE forValue(int value) {
            return values()[value];
        }
    }

    public enum IdDoc_TckMntBruto {

        Item1(1);
        private int intValue;
        private static java.util.HashMap<Integer, IdDoc_TckMntBruto> mappings;

        private static java.util.HashMap<Integer, IdDoc_TckMntBruto> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, IdDoc_TckMntBruto>();
                    }
            return mappings;
        }

        private IdDoc_TckMntBruto(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static IdDoc_TckMntBruto forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum IdDoc_BoletaMntBruto {

        Item2(2);
        private int intValue;
        private static java.util.HashMap<Integer, IdDoc_BoletaMntBruto> mappings;

        private static java.util.HashMap<Integer, IdDoc_BoletaMntBruto> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, IdDoc_BoletaMntBruto>();
                    }

            return mappings;
        }

        private IdDoc_BoletaMntBruto(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static IdDoc_BoletaMntBruto forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum IdDoc_FactMntBruto {

        Item1(1);
        private int intValue;
        private static java.util.HashMap<Integer, IdDoc_FactMntBruto> mappings;

        private static java.util.HashMap<Integer, IdDoc_FactMntBruto> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, IdDoc_FactMntBruto>();
                    }

            return mappings;
        }

        private IdDoc_FactMntBruto(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static IdDoc_FactMntBruto forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum IdDoc_FactFmaPago {

        Item1(1),
        Item2(2);
        private int intValue;
        private static java.util.HashMap<Integer, IdDoc_FactFmaPago> mappings;

        private static java.util.HashMap<Integer, IdDoc_FactFmaPago> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, IdDoc_FactFmaPago>();
                    }

            return mappings;
        }

        private IdDoc_FactFmaPago(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static IdDoc_FactFmaPago forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum IdDoc_RemTipoTraslado {

        Item1(1),
        Item2(2),
        Item3(3);
        private int intValue;
        private static java.util.HashMap<Integer, IdDoc_RemTipoTraslado> mappings;

        private static java.util.HashMap<Integer, IdDoc_RemTipoTraslado> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, IdDoc_RemTipoTraslado>();
                    }

            return mappings;
        }

        private IdDoc_RemTipoTraslado(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static IdDoc_RemTipoTraslado forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum IdDoc_Fact_ExpMntBruto {

        Item1(1);
        private int intValue;
        private static java.util.HashMap<Integer, IdDoc_Fact_ExpMntBruto> mappings;

        private static java.util.HashMap<Integer, IdDoc_Fact_ExpMntBruto> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, IdDoc_Fact_ExpMntBruto>();
                    }
            return mappings;
        }

        private IdDoc_Fact_ExpMntBruto(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static IdDoc_Fact_ExpMntBruto forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum IdDoc_Fact_ExpFmaPago {

        Item1(1),
        Item2(2);
        private int intValue;
        private static java.util.HashMap<Integer, IdDoc_Fact_ExpFmaPago> mappings;

        private static java.util.HashMap<Integer, IdDoc_Fact_ExpFmaPago> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, IdDoc_Fact_ExpFmaPago>();
                    }

            return mappings;
        }

        private IdDoc_Fact_ExpFmaPago(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static IdDoc_Fact_ExpFmaPago forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum IdDoc_Rem_ExpTipoTraslado {

        Item1(1),
        Item2(2),
        Item3(3);
        private int intValue;
        private static java.util.HashMap<Integer, IdDoc_Rem_ExpTipoTraslado> mappings;

        private static java.util.HashMap<Integer, IdDoc_Rem_ExpTipoTraslado> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, IdDoc_Rem_ExpTipoTraslado>();
                    }

            return mappings;
        }

        private IdDoc_Rem_ExpTipoTraslado(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static IdDoc_Rem_ExpTipoTraslado forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum IdDoc_TckFmaPago {

        Item1(1),
        Item2(2);
        private int intValue;
        private static java.util.HashMap<Integer, IdDoc_TckFmaPago> mappings;

        private static java.util.HashMap<Integer, IdDoc_TckFmaPago> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, IdDoc_TckFmaPago>();
                    }

            return mappings;
        }

        private IdDoc_TckFmaPago(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static IdDoc_TckFmaPago forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum IdDoc_IVAalDia {

        Item0(0),
        Item1(1);
        private int intValue;
        private static java.util.HashMap<Integer, IdDoc_IVAalDia> mappings;

        private static java.util.HashMap<Integer, IdDoc_IVAalDia> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, IdDoc_IVAalDia>();
                    }
            return mappings;
        }

        private IdDoc_IVAalDia(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static IdDoc_IVAalDia forValue(int value) {
            return getMappings().get(value);
        }
    }
    
    public enum IdDoc_IndPropiedadMercadTransp {

        Item1(1);
        private int intValue;
        private static java.util.HashMap<Integer, IdDoc_IndPropiedadMercadTransp> mappings;

        private static java.util.HashMap<Integer, IdDoc_IndPropiedadMercadTransp> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, IdDoc_IndPropiedadMercadTransp>();
                    }

            return mappings;
        }

        private IdDoc_IndPropiedadMercadTransp(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static IdDoc_IndPropiedadMercadTransp forValue(int value) {
            return getMappings().get(value);
        }
    }
    
    public enum IdDoc_SecretoProfesional {

        Item1(1);
        private int intValue;
        private static java.util.HashMap<Integer, IdDoc_SecretoProfesional> mappings;

        private static java.util.HashMap<Integer, IdDoc_SecretoProfesional> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, IdDoc_SecretoProfesional>();
                    }

            return mappings;
        }

        private IdDoc_SecretoProfesional(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static IdDoc_SecretoProfesional forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum DscRcgGlobalDRG_ItemIndFactDR {

        Item1(1),
        Item2(2),
        Item3(3),
        Item4(4),
        Item6(6),
        Item7(7),
        Item10(10),
        Item11(11),
        Item12(12),
        Item13(13),
        Item14(14),
        Item15(15);
        private int intValue;
        private static java.util.HashMap<Integer, DscRcgGlobalDRG_ItemIndFactDR> mappings;

        private static java.util.HashMap<Integer, DscRcgGlobalDRG_ItemIndFactDR> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, DscRcgGlobalDRG_ItemIndFactDR>();
                    }

            return mappings;
        }

        private DscRcgGlobalDRG_ItemIndFactDR(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static DscRcgGlobalDRG_ItemIndFactDR forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum ReferenciaReferenciaIndGlobal {

        Item1(1);
        private int intValue;
        private static java.util.HashMap<Integer, ReferenciaReferenciaIndGlobal> mappings;

        private static java.util.HashMap<Integer, ReferenciaReferenciaIndGlobal> getMappings() {

                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, ReferenciaReferenciaIndGlobal>();
                    }

            return mappings;
        }

        private ReferenciaReferenciaIndGlobal(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static ReferenciaReferenciaIndGlobal forValue(int value) {
            return getMappings().get(value);
        }
    }

    public enum ItemChoiceType {

        DocRecep,
        DocRecepExt;

        public int getValue() {
            return this.ordinal();
        }

        public static ItemChoiceType forValue(int value) {
            return values()[value];
        }
    }

    public enum ItemChoiceType1 {

        DocRecep,
        DocRecepExt;

        public int getValue() {
            return this.ordinal();
        }

        public static ItemChoiceType1 forValue(int value) {
            return values()[value];
        }
    }

    public enum ItemChoiceType2 {

        DocRecep,
        DocRecepExt;

        public int getValue() {
            return this.ordinal();
        }

        public static ItemChoiceType2 forValue(int value) {
            return values()[value];
        }
    }

    public enum ItemChoiceType3 {

        DocRecep,
        DocRecepExt;

        public int getValue() {
            return this.ordinal();
        }

        public static ItemChoiceType3 forValue(int value) {
            return values()[value];
        }
    }

    public enum ItemChoiceType4 {

        DocRecep,
        DocRecepExt;

        public int getValue() {
            return this.ordinal();
        }

        public static ItemChoiceType4 forValue(int value) {
            return values()[value];
        }
    }

    ////////SIGNATURE CLASS//////////////////
    @XMLSequence({"hMACOutputLength", "algorithm"})
    public class SignatureMethodType  implements java.io.Serializable{

        private String hMACOutputLength;
        private String algorithm;

        public String getHMACOutputLength() {
            return this.hMACOutputLength;
        }

        public void setHMACOutputLength(String value) {
            this.hMACOutputLength = value;
        }

        public String getAlgorithm() {
            return this.algorithm;
        }

        public void setAlgorithm(String value) {
            this.algorithm = value;
        }
    }

    public class CanonicalizationMethodType  implements java.io.Serializable{

        private String algorithm;

        public String getAlgorithm() {
            return this.algorithm;
        }

        public void setAlgorithm(String value) {
            this.algorithm = value;
        }
    }

    @XMLSequence({"canonicalizationMethod", "signatureMethod", "reference", "id"})
    public class SignedInfoType  implements java.io.Serializable{

        private CanonicalizationMethodType canonicalizationMethod;
        private SignatureMethodType signatureMethod;
        private ReferenceType[] reference;
        private String id;

        public CanonicalizationMethodType getCanonicalizationMethod() {
            return this.canonicalizationMethod;
        }

        public void setCanonicalizationMethod(CanonicalizationMethodType value) {
            this.canonicalizationMethod = value;
        }

        public SignatureMethodType getSignatureMethod() {
            return this.signatureMethod;
        }

        public void setSignatureMethod(SignatureMethodType value) {
            this.signatureMethod = value;
        }

        public ReferenceType[] getReference() {
            return this.reference;
        }

        public void setReference(ReferenceType[] value) {
            this.reference = value;
        }

        public String getId() {
            return this.id;
        }

        public void setId(String value) {
            this.id = value;
        }
    }

    @XMLSequence({"signedInfo", "signatureValue", "keyInfo", "object", "id"})
    public class SignatureType  implements java.io.Serializable{

        private SignedInfoType signedInfo;
        private SignatureValueType signatureValue;
        private KeyInfoType keyInfo;
        private ObjectType[] object;
        private String id;

        public SignedInfoType getSignedInfo() {
            return this.signedInfo;
        }

        public void setSignedInfo(SignedInfoType value) {
            this.signedInfo = value;
        }

        public SignatureValueType getSignatureValue() {
            return this.signatureValue;
        }

        public void setSignatureValue(SignatureValueType value) {
            this.signatureValue = value;
        }

        public KeyInfoType getKeyInfo() {
            return this.keyInfo;
        }

        public void setKeyInfo(KeyInfoType value) {
            this.keyInfo = value;
        }

        public ObjectType[] getObject() {
            return this.object;
        }

        public void setObject(ObjectType[] value) {
            this.object = value;
        }

        public String getId() {
            return this.id;
        }

        public void setId(String value) {
            this.id = value;
        }
    }

    @XMLSequence({"id", "value"})
    public class SignatureValueType  implements java.io.Serializable{

        private String id;
        private byte[] value;

        public String getId() {
            return this.id;
        }

        public void setId(String value) {
            this.id = value;
        }

        public byte[] getValue() {
            return this.value;
        }

        public void setValue(byte[] value) {
            this.value = value;
        }
    }

    public class DigestMethodType  implements java.io.Serializable{

        private String algorithm;

        public String getAlgorithm() {
            return this.algorithm;
        }

        public void setAlgorithm(String value) {
            this.algorithm = value;
        }
    }

    @XMLSequence({"x509IssuerName", "x509SerialNumber"})
    public class X509IssuerSerialType  implements java.io.Serializable{

        private String x509IssuerName = null;
        private String x509SerialNumber = null;

        public String getX509IssuerName() {
            if (this.x509IssuerName == null) {
                return "";
            } else {
                return this.x509IssuerName;
            }
        }

        public void setX509IssuerName(String value) {
            if (value != null && !value.equals("")) {
                this.x509IssuerName = value;
            }
        }

        public String getX509SerialNumber() {
            if (this.x509SerialNumber == null) {
                return "";
            } else {
                return this.x509SerialNumber;
            }
        }

        public void setX509SerialNumber(String value) {
            if (value != null && !value.equals("")) {
                this.x509SerialNumber = value;
            }
        }
    }

    @XMLSequence({"items", "itemsElementName"})
    public class X509DataType  implements java.io.Serializable{

        private Object[] items;
        private ItemsChoiceType[] itemsElementName;

        public Object[] getItems() {
            return this.items;
        }

        public void setItems(Object[] value) {
            this.items = value;
        }

        public ItemsChoiceType[] getItemsElementName() {
            return this.itemsElementName;
        }

        public void setItemsElementName(ItemsChoiceType[] value) {
            this.itemsElementName = value;
        }
    }

    public class TransformsType  implements java.io.Serializable{

        private TransformType[] transform;

        public TransformType[] getTransform() {
            return this.transform;
        }

        public void setTransform(TransformType[] value) {
            this.transform = value;
        }
    }

    @XMLSequence({"referemce", "id"})
    public class ManifestType  implements java.io.Serializable{

        private ReferenceType[] reference;
        private String id;

        public ReferenceType[] getReference() {
            return this.reference;
        }

        public void setReference(ReferenceType[] value) {
            this.reference = value;
        }

        public String getId() {
            return this.id;
        }

        public void setId(String value) {
            this.id = value;
        }
    }

    @XMLSequence({"signatureProperty", "id"})
    public class SignaturePropertiesType  implements java.io.Serializable{

        private SignaturePropertyType[] signatureProperty;
        private String id;

        public SignaturePropertyType[] getSignatureProperty() {
            return this.signatureProperty;
        }

        public void setSignatureProperty(SignaturePropertyType[] value) {
            this.signatureProperty = value;
        }

        public String getId() {
            return this.id;
        }

        public void setId(String value) {
            this.id = value;
        }
    }

    @XMLSequence({"text", "target", "id"})
    public class SignaturePropertyType  implements java.io.Serializable{

        private String[] text;
        private String target;
        private String id;

        public String[] getText() {
            return this.text;
        }

        public void setText(String[] value) {
            this.text = value;
        }

        public String getTarget() {
            return this.target;
        }

        public void setTarget(String value) {
            this.target = value;
        }

        public String getId() {
            return this.id;
        }

        public void setId(String value) {
            this.id = value;
        }
    }

    @XMLSequence({"id", "mimeType", "encoding"})
    public class ObjectType  implements java.io.Serializable{

        private String id;
        private String mimeType;
        private String encoding;

        public String getId() {
            return this.id;
        }

        public void setId(String value) {
            this.id = value;
        }

        public String getMimeType() {
            return this.mimeType;
        }

        public void setMimeType(String value) {
            this.mimeType = value;
        }

        public String getEncoding() {
            return this.encoding;
        }

        public void setEncoding(String value) {
            this.encoding = value;
        }
    }

    public class SPKIDataType  implements java.io.Serializable{

        private byte[][] sPKISexp;

        public byte[][] getSPKISexp() {
            return this.sPKISexp;
        }

        public void setSPKISexp(byte[][] value) {
            this.sPKISexp = value;
        }
    }

    @XMLSequence({"items", "itemsElementName"})
    public class PGPDataType  implements java.io.Serializable{

        private Object[] items;
        private ItemsChoiceType1[] itemsElementName;

        public Object[] getItems() {
            return this.items;
        }

        public void setItems(Object[] value) {
            this.items = value;
        }

        public ItemsChoiceType1[] getItemsElementName() {
            return this.itemsElementName;
        }

        public void setItemsElementName(ItemsChoiceType1[] value) {
            this.itemsElementName = value;
        }
    }

    @XMLSequence({"transforms", "uRI", "type"})
    public class RetrievalMethodType implements java.io.Serializable{

        private TransformType[] transforms;
        private String uRI;
        private String type;

        public TransformType[] getTransforms() {
            return this.transforms;
        }

        public void setTransforms(TransformType[] value) {
            this.transforms = value;
        }

        public String getURI() {
            return this.uRI;
        }

        public void setURI(String value) {
            this.uRI = value;
        }

        public String getType() {
            return this.type;
        }

        public void setType(String value) {
            this.type = value;
        }
    }

    @XMLSequence({"items", "text", "algorithm"})
    public class TransformType implements java.io.Serializable{

        private Object[] items;
        private String[] text;
        private String algorithm;

        public Object[] getItems() {
            return this.items;
        }

        public void setItems(Object[] value) {
            this.items = value;
        }

        public String[] getText() {
            return this.text;
        }

        public void setText(String[] value) {
            this.text = value;
        }

        public String getAlgorithm() {
            return this.algorithm;
        }

        public void setAlgorithm(String value) {
            this.algorithm = value;
        }
    }

    @XMLSequence({"modulus", "exponent"})
    public class RSAKeyValueType implements java.io.Serializable{

        private byte[] modulus;
        private byte[] exponent;

        public byte[] getModulus() {
            return this.modulus;
        }

        public void setModulus(byte[] value) {
            this.modulus = value;
        }

        public byte[] getExponent() {
            return this.exponent;
        }

        public void setExponent(byte[] value) {
            this.exponent = value;
        }
    }

    @XMLSequence({"p", "q", "g", "y", "j", "seed", "pgenCounter"})
    public class DSAKeyValueType implements java.io.Serializable{

        private byte[] p;
        private byte[] q;
        private byte[] g;
        private byte[] y;
        private byte[] j;
        private byte[] seed;
        private byte[] pgenCounter;

        public byte[] getP() {
            return this.p;
        }

        public void setP(byte[] value) {
            this.p = value;
        }

        public byte[] getQ() {
            return this.q;
        }

        public void setQ(byte[] value) {
            this.q = value;
        }

        public byte[] getG() {
            return this.g;
        }

        public void setG(byte[] value) {
            this.g = value;
        }

        public byte[] getY() {
            return this.y;
        }

        public void setY(byte[] value) {
            this.y = value;
        }

        public byte[] getJ() {
            return this.j;
        }

        public void setJ(byte[] value) {
            this.j = value;
        }

        public byte[] getSeed() {
            return this.seed;
        }

        public void setSeed(byte[] value) {
            this.seed = value;
        }

        public byte[] getPgenCounter() {
            return this.pgenCounter;
        }

        public void setPgenCounter(byte[] value) {
            this.pgenCounter = value;
        }
    }

    @XMLSequence({"item", "text"})
    public class KeyValueType implements java.io.Serializable{

        private Object item;
        private String[] text;

        public Object getItem() {
            return this.item;
        }

        public void setItem(Object value) {
            this.item = value;
        }

        public String[] getText() {
            return this.text;
        }

        public void setText(String[] value) {
            this.text = value;
        }
    }

    @XMLSequence({"items", "itemsElementName", "text", "id"})
    public class KeyInfoType implements java.io.Serializable{

        private Object[] items;
        private ItemsChoiceType2[] itemsElementName;
        private String[] text;
        private String id;

        public Object[] getItems() {
            return this.items;
        }

        public void setItems(Object[] value) {
            this.items = value;
        }

        public ItemsChoiceType2[] getItemsElementName() {
            return this.itemsElementName;
        }

        public void setItemsElementName(ItemsChoiceType2[] value) {
            this.itemsElementName = value;
        }

        public String[] getText() {
            return this.text;
        }

        public void setText(String[] value) {
            this.text = value;
        }

        public String getId() {
            return this.id;
        }

        public void setId(String value) {
            this.id = value;
        }
    }

    @XMLSequence({"transforms", "digestMethod", "digestValue", "id", "uRI", "type"})
    public class ReferenceType implements java.io.Serializable{

        private TransformType[] transforms;
        private DigestMethodType digestMethod;
        private byte[] digestValue;
        private String id;
        private String uRI;
        private String type;

        public TransformType[] getTransforms() {
            return this.transforms;
        }

        public void setTransforms(TransformType[] value) {
            this.transforms = value;
        }

        public DigestMethodType getDigestMethod() {
            return this.digestMethod;
        }

        public void setDigestMethod(DigestMethodType value) {
            this.digestMethod = value;
        }

        public byte[] getDigestValue() {
            return this.digestValue;
        }

        public void setDigestValue(byte[] value) {
            this.digestValue = value;
        }

        public String getId() {
            return this.id;
        }

        public void setId(String value) {
            this.id = value;
        }

        public String getURI() {
            return this.uRI;
        }

        public void setURI(String value) {
            this.uRI = value;
        }

        public String getType() {
            return this.type;
        }

        public void setType(String value) {
            this.type = value;
        }
    }

    public enum ItemsChoiceType {

        Item,
        X509CRL,
        X509Certificate,
        X509IssuerSerial,
        X509SKI,
        X509SubjectName;

        public int getValue() {
            return this.ordinal();
        }

        public static ItemsChoiceType forValue(int value) {
            return values()[value];
        }
    }

    public enum ItemsChoiceType1 {

        Item,
        PGPKeyID,
        PGPKeyPacket;

        public int getValue() {
            return this.ordinal();
        }

        public static ItemsChoiceType1 forValue(int value) {
            return values()[value];
        }
    }

    public enum ItemsChoiceType2 {

        Item,
        KeyName,
        KeyValue,
        MgmtData,
        PGPData,
        RetrievalMethod,
        SPKIData,
        X509Data;

        public int getValue() {
            return this.ordinal();
        }

        public static ItemsChoiceType2 forValue(int value) {
            return values()[value];
        }
    }

    //AUX METHODS
    private String replaceMissedTagsXML(String xstreamed) {

        String resultReplaced1 = xstreamed.replace("\"", "'");
        resultReplaced1 = resultReplaced1.replace(" />", "/>");
        resultReplaced1 = resultReplaced1.replace("<?xml version='1.0' ?>", "").replace("<item class='XML.EnvioCFE$CFEDefTypeETck'>", "").replace("</item>", "");
        resultReplaced1 = resultReplaced1.replace("<?xml version='1.0' encoding='UTF-8'?>", "");

        resultReplaced1 = resultReplaced1.replace("<CFE reference='../../..' />", "");
        resultReplaced1 = resultReplaced1.replace("<CFE reference='../../..'/>", "");
        resultReplaced1 = resultReplaced1.replace("<CFE reference=\\'../../..\\' />", "");
        resultReplaced1 = resultReplaced1.replace("<CFE reference='../../..'></CFE>", "");
        resultReplaced1 = resultReplaced1.replace("<CFE reference='../../..' ></CFE>", "");

        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../../outer-class' />", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class><CFE reference='../../../../..'></CFE><version>1.0</version></outer-class>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class defined-in='XML.EnvioCFE$IdDoc' reference='../../../outer-class'>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../../../../outer-class'>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../../../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../IdDoc/outer-class'></outer-class>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../IdDoc/outer-class'></outer-class>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../../Encabezado/IdDoc/outer-class'></outer-class>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../Encabezado/IdDoc/outer-class'>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../Encabezado/IdDoc/outer-class'></outer-class>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../..'></outer-class>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../../outer-class'></outer-class>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class defined-in='XML.EnvioCFE$CFEDefTypesHeritage'>", "");
        resultReplaced1 = resultReplaced1.replace("</outer-class>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class defined-in='XML.EnvioCFE$CFEDefTypeEncabezado' reference='../../outer-class'></outer-class>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class defined-in='XML.EnvioCFE$IdDoc' reference='../../../outer-class'></outer-class>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../outer-class'></outer-class>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../outer-class'></outer-class>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../item/outer-class'></outer-class>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class defined-in='XML.EnvioCFE$CFEDefTypeEncabezado' reference='../../outer-class'>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../outer-class'>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../item/outer-class'>", "");
        resultReplaced1 = resultReplaced1.replace("</outer-class><outer-class reference='../item/Encabezado/IdDoc/outer-class'></outer-class>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../../../outer-class'>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class defined-in='XML.EnvioCFE$eTckEncabezado' reference='../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class defined-in='XML.EnvioCFE$IdDoc' reference='../../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../..'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../item/outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class defined-in='XML.EnvioCFE$eFactEncabezado' reference='../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class defined-in='XML.EnvioCFE$IdDoc' reference='../../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../../../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../..'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../..'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../item/outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class defined-in='XML.EnvioCFE$eFactEncabezado' reference='../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class defined-in='XML.EnvioCFE$IdDoc' reference='../../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../../../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../..'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../item/outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../../../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../..'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class defined-in='XML.EnvioCFE$eTckEncabezado' reference='../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class defined-in='XML.EnvioCFE$IdDoc' reference='../../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../../../../outer-class'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../..'/>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../..'>", "");
        resultReplaced1 = resultReplaced1.replace("</XML.EnvioCFE_-Item__Det__Fact_-Item>", "");
        resultReplaced1 = resultReplaced1.replace("<XML.EnvioCFE_-Item__Det__Fact_-Item>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class>", "");
        resultReplaced1 = resultReplaced1.replace("<CFE reference='../../../..'/>", "");
        resultReplaced1 = resultReplaced1.replace("<version>1.0</version>", "");

        resultReplaced1 = resultReplaced1.replace("<XML.EnvioCFE_-Item__Det__Fact>", "");
        resultReplaced1 = resultReplaced1.replace("</XML.EnvioCFE_-Item__Det__Fact>", "");
        resultReplaced1 = resultReplaced1.replace("CAE__ID", "CAE_ID");
        resultReplaced1 = resultReplaced1.replace("<FchEmis defined-in='XML.EnvioCFE$IdDoc'>1970-01-01</FchEmis>", "");
        resultReplaced1 = resultReplaced1.replace("<FmaPago defined-in='XML.EnvioCFE$IdDoc'>0</FmaPago>", "");
        resultReplaced1 = resultReplaced1.replace("<MntBruto defined-in='XML.EnvioCFE$IdDoc'>0</MntBruto>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../outer-class'>", "");
        resultReplaced1 = resultReplaced1.replace("<outer-class reference='../../outer-class'>", "");
        resultReplaced1 = resultReplaced1.replace("<XML.EnvioCFE_-Item__Rem>", "").replace("</XML.EnvioCFE_-Item__Rem>", "");
        resultReplaced1 = resultReplaced1.replace("<XML.EnvioCFE_-Item__Rem__Exp>", "").replace("</XML.EnvioCFE_-Item__Rem__Exp>", "");
        resultReplaced1 = resultReplaced1.replace("<XML.EnvioCFE_-Item__Resg>", "").replace("</XML.EnvioCFE_-Item__Resg>", "");
        resultReplaced1 = resultReplaced1.replace("<?xml version='1.0' encoding='UTF-8'?>", "");
        resultReplaced1 = resultReplaced1.replace("<XML.EnvioCFE_-Item__Det__Fact__Exp>", "").replace("</XML.EnvioCFE_-Item__Det__Fact__Exp>", "");
        resultReplaced1 = resultReplaced1.replace("</XML.EnvioCFE_-SubTotInfoSTI_Item>", "");
        resultReplaced1 = resultReplaced1.replace("</XML.EnvioCFE_-ReferenciaReferencia>", "");
        resultReplaced1 = resultReplaced1.replace("STI__Item", "STI_Item");
        resultReplaced1 = resultReplaced1.replace("<item class='XML.EnvioCFE$CFEDefTypeEFact'>", "");
        resultReplaced1 = resultReplaced1.replace("<version>1.0</version>", "");
        resultReplaced1 = resultReplaced1.replace("<item class='XML.EnvioCFE$eRemERem'>", "");
        resultReplaced1 = resultReplaced1.replace("<item class='XML.EnvioCFE$eRem_ExpERem_Exp'>", "");
        resultReplaced1 = resultReplaced1.replace("<item class='XML.EnvioCFE$eResgEResg'>", "");
        resultReplaced1 = resultReplaced1.replace("<item class='XML.EnvioCFE$eFact_ExpEFact_Exp'>", "");
        resultReplaced1 = resultReplaced1.replace("<XML.EnvioCFE_-SubTotInfoSTI_Item>", "");
        resultReplaced1 = resultReplaced1.replace("<XML.EnvioCFE_-ReferenciaReferencia>", "");
        resultReplaced1 = resultReplaced1.replace("<TipoCFE defined-in='XML.EnvioCFE$IdDoc'>0</TipoCFE>", "");
		resultReplaced1 = resultReplaced1.replace("<item class='XML.EnvioCFE$eBoletaEBoleta'>", "");
		resultReplaced1 = resultReplaced1.replace("</XML.EnvioCFE_-Item__Det__Boleta>", "");
		resultReplaced1 = resultReplaced1.replace("<XML.EnvioCFE_-Item__Det__Boleta>", "");
        resultReplaced1 = resultReplaced1.replace("\n", "").replace("                                ","");
        resultReplaced1 = resultReplaced1.replace("                  ","").replace("                ","");
        resultReplaced1 = resultReplaced1.replace("              ","").replace("          ","").replace("        ","");
        resultReplaced1 = resultReplaced1.replace("      ","");
        resultReplaced1 = resultReplaced1.replace("    ","").replace("  ","");
        return resultReplaced1;
    }
}