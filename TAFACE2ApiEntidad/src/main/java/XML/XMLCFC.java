package XML;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.thoughtworks.xstream.converters.basic.DateConverter;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import javax.xml.bind.JAXBException;

/**
 *
 * @author danielcera
 */
@XStreamAlias("XMLCFC")
public class XMLCFC {

    @XStreamOmitField
    private final String sNombreArchivoConfiguracion = "_TAFEApi.CFC";
    @XStreamOmitField
    private String sRutaArchivoConfiguracion = "";
    @XStreamImplicit(itemFieldName = "CFCITEMS")
    private CFCItem[] cFCITEMSField;

    public final CFCItem[] getCFCITEMS() {
        return cFCITEMSField;
    }

    public final void setCFCITEMS(CFCItem[] value) {
        cFCITEMSField = value;
    }

    public final XMLCFC CargarCFC(String CarpetaOperacion, String EmpRUT, int SucId, int CajaId) throws FileNotFoundException, IOException, JAXBException {
        try {
            sRutaArchivoConfiguracion = CarpetaOperacion + "TAFACE" + File.separator + EmpRUT + "_" + SucId + sNombreArchivoConfiguracion;
            if (!((new File(CarpetaOperacion + "TAFACE" + File.separator)).isDirectory())) {
                (new File(CarpetaOperacion + "TAFACE" + File.separator)).mkdir();
            }
            if (!((new File(sRutaArchivoConfiguracion)).isFile())) {
                return null;
            }
            String initialText = "";
            FileReader fr = new FileReader(sRutaArchivoConfiguracion);
            BufferedReader br = new BufferedReader(fr);
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                initialText += sCurrentLine;
            }
            return LoadXML(initialText);

        } catch (RuntimeException ex) {
            return null;
        }
    }

    public final void GuardarCFC(String CarpetaOperacion, String EmpRut, int SucId, int CajaId) throws FileNotFoundException, IOException, JAXBException {
        try {
            if (sRutaArchivoConfiguracion.trim().equals("")) {
                if (CarpetaOperacion.trim().equals("")) {
                    sRutaArchivoConfiguracion = System.getProperty("user.dir") + "TAFACE" + File.separator + EmpRut + "_" + SucId + sNombreArchivoConfiguracion;
                } else {
                    if (!((new File(CarpetaOperacion)).exists())) {
                        {
                            new File(CarpetaOperacion).mkdirs();
                        }
                        sRutaArchivoConfiguracion = CarpetaOperacion + "TAFACE" + File.separator + EmpRut + "_" + SucId + sNombreArchivoConfiguracion;
                    }
                }
                OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(sRutaArchivoConfiguracion), Charset.forName("UTF-8").newEncoder());
                PrintWriter writer = new PrintWriter(osw);
                writer.print(this.ToXML());
                osw.close();
            }
        } catch (RuntimeException ex) {
            throw ex;
        }
    }

    private String ToXML() throws JAXBException, IOException {
        XStream xstream = new XStream(new StaxDriver());
        String[] formats = {"MM-dd-yyyy"};
        xstream.registerConverter(new DateConverter("yyyy-MM-dd", formats));
        //xstream.ignoreUnknownElements();
        xstream.processAnnotations(XMLCFC.class);
        String xstreamed = xstream.toXML(this);
        return xstreamed;
    }

    public final XMLCFC LoadXML(String XML) throws JAXBException {
        XStream xstream = new XStream(new StaxDriver());
        String[] formats = {"MM-dd-yyyy"};
        xstream.registerConverter(new DateConverter("yyyy-MM-dd", formats));
        //xstream.ignoreUnknownElements();
        xstream.processAnnotations(XMLCFC.class);
        //converting xml to object
        XMLCFC myObject = (XMLCFC) xstream.fromXML(XML);

        return myObject;
    }

    @XStreamAlias("CFCItem")
    public class CFCItem {

        private int cFCEmpId;
        private int cFCSucId;
        private java.util.Date cFCFchHora = new java.util.Date(0);
        private long cFCId;
        private long cFCNroAutorizacion;
        private long cFCNroDesde;
        private long cFCNroHasta;
        private String cFCRowGUID = "";
        private String cFCSerie = "";
        private int cFCTipoCFE;
        private java.util.Date cFCVencimiento = new java.util.Date(0);

        public final int getEmpId() {
            return cFCEmpId;
        }

        public final void setEmpId(int value) {
            cFCEmpId = value;
        }

        public final int getSucId() {
            return cFCSucId;
        }

        public final void setSucId(int value) {
            cFCSucId = value;
        }

        public final java.util.Date getCFCFchHora() {
            return cFCFchHora;
        }

        public final void setCFCFchHora(java.util.Date value) {
            cFCFchHora = value;
        }

        public final int getCFCId() {
            return (int) cFCId;
        }

        public final void setCFCId(int value) {
            cFCId = value;
        }

        public final long getCFCNroAutorizacion() {
            return cFCNroAutorizacion;
        }

        public final void setCFCNroAutorizacion(long value) {
            cFCNroAutorizacion = value;
        }

        public final long getCFCNroDesde() {
            return cFCNroDesde;
        }

        public final void setCFCNroDesde(long value) {
            cFCNroDesde = value;
        }

        public final long getCFCNroHasta() {
            return cFCNroHasta;
        }

        public final void setCFCNroHasta(long value) {
            cFCNroHasta = value;
        }

        public final String getCFCSerie() {
            return cFCSerie;
        }

        public final void setCFCSerie(String value) {
            cFCSerie = value;
        }

        public final int getCFCTipoCFE() {
            return cFCTipoCFE;
        }

        public final void setCFCTipoCFE(int value) {
            cFCTipoCFE = value;
        }

        public final java.util.Date getCFCVencimiento() {
            return cFCVencimiento;
        }

        public final void setCFCVencimiento(java.util.Date value) {
            cFCVencimiento = value;
        }

        public final String getCFCRowGUID() {
            return cFCRowGUID;
        }

        public final void setCFCRowGUID(String value) {
            cFCRowGUID = value;
        }
    }
}
