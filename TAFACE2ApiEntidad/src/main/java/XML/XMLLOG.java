package XML;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author danielcera
 */
@XStreamAlias("FacturaElectronica")
public class XMLLOG {

    private int SUCURSALNRO;
    private String SUCURSALNOMBRE = "";
    private int CAJANRO;
    private int CAJERONRO;
    private String CAJERONOMBRE = "";
    private String FECHA = null;
    private String LOG = "";

    public final int getSUCURSALNRO() {
        return this.SUCURSALNRO;
    }

    public final void setSUCURSALNRO(int value) {
        this.SUCURSALNRO = value;
    }

    public final String getSUCURSALNOMBRE() {
        return this.SUCURSALNOMBRE;
    }

    public final void setSUCURSALNOMBRE(String value) {
        this.SUCURSALNOMBRE = value;
    }

    public final int getCAJANRO() {
        return this.CAJANRO;
    }

    public final void setCAJANRO(int value) {
        this.CAJANRO = value;
    }

    public final int getCAJERONRO() {
        return this.CAJERONRO;
    }

    public final void setCAJERONRO(int value) {
        this.CAJERONRO = value;
    }

    public final String getCAJERONOMBRE() {
        return this.CAJERONOMBRE;
    }

    public final void setCAJERONOMBRE(String value) {
        this.CAJERONOMBRE = value;
    }

    public final java.util.Date getFECHA() {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date aux = null;
        if (FECHA != null) {
            try {
                aux = formato.parse(FECHA);
            } catch (ParseException ex) {
                Logger.getLogger(XMLFACTURA.class.getName()).log(Level.SEVERE, null, ex);
            }
            return aux;
        } else {
            return null;
        }
    }

    public final void setFECHA(java.util.Date value) {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        FECHA = formato.format(value);
    }

    public final String getLOG() {
        return this.LOG;
    }

    public final void setLOG(String value) {
        this.LOG = value;
    }
}