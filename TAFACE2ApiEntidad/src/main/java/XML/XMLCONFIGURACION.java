package XML;

import TAFACE2ApiEntidad.TAException;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

@XStreamAlias("PARAMETROSTAFEAPI")
public class XMLCONFIGURACION {

    private static final String sNombreArchivoConfiguracion = "TAFEApi.config";
    @XStreamOmitField
    private String sRutaArchivoConfiguracion = "";
    private short MODOFIRMA;
    private short TIPOPROVEEDORSQL;
    private double TOPEUIPLAZA;
    private double TOPEUIFREESHOP;
    private short VALIDARCFC;
    private short TIPOENCRIPTACION;
    private short LOTESINCRONIZACION;
    private short HORASSINCONEXION;
    private short CANTDIASANTESCAEVENCER;
    private byte CAESPORCAVANCE;
    private short CANTDIASANTESCERTIFICADOVENCER;
    private short MINDATOSCAJASINSINC;
    private String GUID = "";
    private short ENCRIPTARCOMPLEMENTOFISCAL;
    private int MAXIMOTIEMPOTAREAPROGRAMADA;
    private int MINUTOSCADUCIDADRESERVACAE;
    private int LARGOMINIMOTICKET;
    private short ABRIRCAJON;
    private String FECHASISTEMA = null;
    private short NOVALIDARFECHAFIRMA;
    private short TERMINALSERVER;
    private int ELIMINARCTESINCRONIZADODIAS;
    private String SYNC = "1969-12-31"; //parametro para configuracion de la eliminacion de comprobantes sincronizados en la BD
    private int DIASFIRMARRETROACTIVO = 0;

    public XMLCONFIGURACION() {
        CrearConfiguracion();
    }

    public final String getsRutaArchivoConfiguracion() {
        return sRutaArchivoConfiguracion;
    }

    public final void setsRutaArchivoConfiguracion(String value) {
        sRutaArchivoConfiguracion = value;
    }

    public final short getTIPOENCRIPTACION() {
        return TIPOENCRIPTACION;
    }

    public final void setTIPOENCRIPTACION(short value) {
        TIPOENCRIPTACION = value;
    }

    public final short getVALIDARCFC() {
        return VALIDARCFC;
    }

    public final void setVALIDARCFC(short value) {
        VALIDARCFC = value;
    }

    public final short getMODOFIRMA() {
        return MODOFIRMA;
    }

    public final void setMODOFIRMA(short value) {
        MODOFIRMA = value;
    }

    public final short getTIPOPROVEEDORSQL() {
        return TIPOPROVEEDORSQL;
    }

    public final void setTIPOPROVEEDORSQL(short value) {
        TIPOPROVEEDORSQL = value;
    }

    public final double getTOPEUIPLAZA() {
        return TOPEUIPLAZA;
    }

    public final void setTOPEUIPLAZA(double value) {
        TOPEUIPLAZA = value;
    }

    public final double getTOPEUIFREESHOP() {
        return TOPEUIFREESHOP;
    }

    public final void setTOPEUIFREESHOP(double value) {
        TOPEUIFREESHOP = value;
    }

    public final short getLOTESINCRONIZACION() {
        return LOTESINCRONIZACION;
    }

    public final void setLOTESINCRONIZACION(short value) {
        LOTESINCRONIZACION = value;
    }

    public final short getHORASSINCONEXION() {
        return HORASSINCONEXION;
    }

    public final void setHORASSINCONEXION(short value) {
        HORASSINCONEXION = value;
    }

    public final short getCANTDIASANTESCAEVENCER() {
        return CANTDIASANTESCAEVENCER;
    }

    public final void setCANTDIASANTESCAEVENCER(short value) {
        CANTDIASANTESCAEVENCER = value;
    }

    public final byte getCAESPORCAVANCE() {
        return CAESPORCAVANCE;
    }

    public final void setCAESPORCAVANCE(byte value) {
        CAESPORCAVANCE = value;
    }

    public final short getCANTDIASANTESCERTIFICADOVENCER() {
        return CANTDIASANTESCERTIFICADOVENCER;
    }

    public final void setCANTDIASANTESCERTIFICADOVENCER(short value) {
        CANTDIASANTESCERTIFICADOVENCER = value;
    }

    public final short getMINDATOSCAJASINSINC() {
        return MINDATOSCAJASINSINC;
    }

    public final void setMINDATOSCAJASINSINC(short value) {
        MINDATOSCAJASINSINC = value;
    }

    public final String getGUID() {
        return GUID;
    }

    public final void setGUID(String value) {
        GUID = value;
    }

    public final short getENCRIPTARCOMPLEMENTOFISCAL() {
        return ENCRIPTARCOMPLEMENTOFISCAL;
    }

    public final void setENCRIPTARCOMPLEMENTOFISCAL(short value) {
        ENCRIPTARCOMPLEMENTOFISCAL = value;
    }

    public final int getMAXIMOTIEMPOTAREAPROGRAMADA() {
        return MAXIMOTIEMPOTAREAPROGRAMADA;
    }

    public final void setMAXIMOTIEMPOTAREAPROGRAMADA(int value) {
        MAXIMOTIEMPOTAREAPROGRAMADA = value;
    }

    public final int getMINUTOSCADUCIDADRESERVACAE() {
        return MINUTOSCADUCIDADRESERVACAE;
    }

    public final void setMINUTOSCADUCIDADRESERVACAE(int value) {
        MINUTOSCADUCIDADRESERVACAE = value;
    }

    public final int getLARGOMINIMOTICKET() {
        return LARGOMINIMOTICKET;
    }

    public final void setLARGOMINIMOTICKET(int value) {
        LARGOMINIMOTICKET = value;
    }

    public short getABRIRCAJON() {
        return ABRIRCAJON;
    }

    public void setABRIRCAJON(short ABRIRCAJON) {
        this.ABRIRCAJON = ABRIRCAJON;
    }

    public Date getFECHASISTEMA() {
        Date fechaSistemaDate = null;
        try {
            //2017-05-02T12:47:52
            String getted = FECHASISTEMA.replace("T", " ");
            SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            fechaSistemaDate = form.parse(getted);
        } catch (ParseException ex) {
            Logger.getLogger(XMLCONFIGURACION.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fechaSistemaDate;
    }

    public void setFECHASISTEMA(Date FECHASISTEMA) {
        String form = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(FECHASISTEMA);
        this.FECHASISTEMA = form.split(" ")[0] + "T" + form.split(" ")[1];
    }

    public short getNOVALIDARFECHAFIRMA() {
        return NOVALIDARFECHAFIRMA;
    }

    public void setNOVALIDARFECHAFIRMA(short NOVALIDARFECHAFIRMA) {
        this.NOVALIDARFECHAFIRMA = NOVALIDARFECHAFIRMA;
    }

    public short getTERMINALsERVER() {
        return TERMINALSERVER;
    }

    public void setTERMINALsERVER(short TERMINALsERVER) {
        this.TERMINALSERVER = TERMINALsERVER;
    }

    public int getELIMINARCTESINCRONIZADODIAS() {
        return ELIMINARCTESINCRONIZADODIAS;
    }

    public void setELIMINARCTESINCRONIZADODIAS(int value) {
        this.ELIMINARCTESINCRONIZADODIAS = value;
    }

    public Date getSYNC() {
        Date fechaSistemaDate = null;
        try {
            SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd");
            fechaSistemaDate = form.parse(SYNC);
        } catch (ParseException ex) {
            Logger.getLogger(XMLCONFIGURACION.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fechaSistemaDate;
    }

    public void setSYNC(Date value) {
        this.SYNC = new SimpleDateFormat("yyyy-MM-dd").format(value);
    }
    
    public int getDIASFIRMARRETROACTIVO() {
        return DIASFIRMARRETROACTIVO;
    }

    public void setDIASFIRMARRETROACTIVO(int DIASFIRMARRETROACTIVO) {
        this.DIASFIRMARRETROACTIVO = DIASFIRMARRETROACTIVO;
    }

    public final XMLCONFIGURACION CargarConfiguracion(String CarpetaOperacion, String EmpRUT, int SucId) throws FileNotFoundException, IOException {
        try {
            sRutaArchivoConfiguracion = CarpetaOperacion + EmpRUT + "_" + SucId + "_" + sNombreArchivoConfiguracion;
            if (!((new File(CarpetaOperacion)).isDirectory())) {
                (new File(CarpetaOperacion)).mkdir();
            }
            if (!((new File(sRutaArchivoConfiguracion)).isFile())) {
                return null;
            }
            String initialText = "";
            FileReader fr = new FileReader(sRutaArchivoConfiguracion);
            BufferedReader br = new BufferedReader(fr);
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                initialText += sCurrentLine;
            }
            return LoadXML(initialText);
        } catch (RuntimeException ex) {
            throw new TAException("Error al cargar la configuracion: " + ex.getMessage());
        }
    }

    private void CrearConfiguracion() {
        TIPOENCRIPTACION = 1;
        VALIDARCFC = 0;
        MODOFIRMA = 2;
        TIPOPROVEEDORSQL = 3;
        TOPEUIPLAZA = 10000;
        TOPEUIFREESHOP = 0;
        ENCRIPTARCOMPLEMENTOFISCAL = 0;
        MAXIMOTIEMPOTAREAPROGRAMADA = 30;
        MINUTOSCADUCIDADRESERVACAE = 1440;
        ABRIRCAJON = 0;
        Date dateConfig = new Date();
        FECHASISTEMA = convertirFechaXMLSincronizar(new Date(dateConfig.getYear(), dateConfig.getMonth(), dateConfig.getDate(), 0, 0, 0));
        ABRIRCAJON = 0;
        TERMINALSERVER = 0;
        ELIMINARCTESINCRONIZADODIAS = 10;
        SYNC = "1969-12-31";
        DIASFIRMARRETROACTIVO = 0;
        dateConfig = null;
    }

    public final void GuardarConfig(String CarpetaOperacion, String EmpRut, int SucId) throws IOException {
        try {
            if (sRutaArchivoConfiguracion.trim().equals("")) {
                if (CarpetaOperacion.trim().equals("")) {
                    sRutaArchivoConfiguracion = CarpetaOperacion + File.separator + EmpRut + "_" + SucId + "_" + sNombreArchivoConfiguracion;
                }
                if (!((new File(CarpetaOperacion + File.separator)).exists())) {
                    {
                        new File(CarpetaOperacion + File.separator).mkdirs();
                    }
                }
                sRutaArchivoConfiguracion = CarpetaOperacion + File.separator + EmpRut + "_" + SucId + "_" + sNombreArchivoConfiguracion;
            }
            OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(sRutaArchivoConfiguracion), Charset.forName("UTF-8").newEncoder());
            PrintWriter writer = new PrintWriter(osw);
            writer.print(this.ToXML());
            osw.close();
        } catch (RuntimeException ex) {
            throw new TAException("Error al guardar la configuracion: " + ex.getMessage());
        }
    }

    public String ToXML() throws IOException {
        XStream xstream = new XStream(new StaxDriver());
        //xstream.ignoreUnknownElements();
        xstream.processAnnotations(XMLCONFIGURACION.class);
        String xstreamed = xstream.toXML(this).replace("<?xml version=\"1.0\" ?><PARAMETROSTAFEAPI>", "<PARAMETROSTAFEAPI xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"TAFACE\">");
        return xstreamed;
    }

    public XMLCONFIGURACION LoadXML(String XML) {
        XStream xstream = new XStream(new StaxDriver());
        //xstream.ignoreUnknownElements();
        xstream.processAnnotations(XMLCONFIGURACION.class);
        //converting xml to object
        XMLCONFIGURACION myObject = (XMLCONFIGURACION) xstream.fromXML(XML);
        return myObject;
    }

    public String convertirFechaXMLSincronizar(Date date) {
        //2017-05-02T12:47:52.3558396-03:00
        String form = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
        String concat = form.split(" ")[0] + "T" + form.split(" ")[1];
        return concat;
    }
}