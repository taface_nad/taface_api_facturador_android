/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tangible;

import java.util.Date;

/**
 *
 * @author danielcera
 */
public class CurrentDate {

    private int year;
    private int month;
    private int day;
    private int hour;
    private int min;
    private int seconds;
    private String dateTime;

    public CurrentDate() {
        Date date = new Date();
        String time = date.toString(); //Thu Mar 30 11:18:21 UYT 2017
        String[] splitted = time.split(" ");
        day = Integer.parseInt(splitted[2]);
        month = Month(splitted[1]);
        year = Integer.parseInt(splitted[5]);
        String[] timeSplitted = splitted[3].split(":");
        hour = Integer.parseInt(timeSplitted[0]);
        min = Integer.parseInt(timeSplitted[1]);
        seconds = Integer.parseInt(timeSplitted[2]);
        dateTime = String.valueOf(year) + "/" + String.valueOf(month) + "/" + String.valueOf(day) + " " + String.valueOf(hour) + ":" + String.valueOf(min) + ":" + String.valueOf(seconds);


    }

    public CurrentDate(Date data) {
        String time = data.toString(); //Thu Mar 30 11:18:21 UYT 2017
        String[] splitted = time.split(" ");
        day = Integer.parseInt(splitted[2]);
        month = Month(splitted[1]);
        year = Integer.parseInt(splitted[5]);
        String[] timeSplitted = splitted[3].split(":");
        hour = Integer.parseInt(timeSplitted[0]);
        min = Integer.parseInt(timeSplitted[1]);
        seconds = Integer.parseInt(timeSplitted[2]);
        dateTime = String.valueOf(year) + "/" + String.valueOf(month) + "/" + String.valueOf(day) + " " + String.valueOf(hour) + ":" + String.valueOf(min) + ":" + String.valueOf(seconds);
    }

    public String getDate() {
        return dateTime;
    }

    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * @return the month
     */
    public int getMonth() {
        return month;
    }

    /**
     * @param month the month to set
     */
    public void setMonth(int month) {
        this.month = month;
    }

    /**
     * @return the day
     */
    public int getDay() {
        return day;
    }

    /**
     * @param day the day to set
     */
    public void setDay(int day) {
        this.day = day;
    }

    /**
     * @return the hour
     */
    public int getHour() {
        return hour;
    }

    /**
     * @param hour the hour to set
     */
    public void setHour(int hour) {
        this.hour = hour;
    }

    /**
     * @return the min
     */
    public int getMin() {
        return min;
    }

    /**
     * @param min the min to set
     */
    public void setMin(int min) {
        this.min = min;
    }

    /**
     * @return the seconds
     */
    public int getSeconds() {
        return seconds;
    }

    /**
     * @param seconds the seconds to set
     */
    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public int Month(String mes) {
        int mesInt = 0;
        if ("Ene".equals(mes) || "Jan".equals(mes)) {
            mesInt = 1;
        }
        if ("Feb".equals(mes) || "Feb".equals(mes)) {
            mesInt = 2;
        }
        if ("Mar".equals(mes) || "Mar".equals(mes)) {
            mesInt = 3;
        }
        if ("Abr".equals(mes) || "Apr".equals(mes)) {
            mesInt = 4;
        }
        if ("May".equals(mes) || "May".equals(mes)) {
            mesInt = 5;
        }
        if ("Jun".equals(mes) || "Jun".equals(mes)) {
            mesInt = 6;
        }
        if ("Jul".equals(mes) || "Jul".equals(mes)) {
            mesInt = 7;
        }
        if ("Ago".equals(mes) || "Aug".equals(mes)) {
            mesInt = 8;
        }
        if ("Sep".equals(mes) || "Sep".equals(mes)) {
            mesInt = 9;
        }
        if ("Oct".equals(mes) || "Oct".equals(mes)) {
            mesInt = 10;
        }
        if ("Nov".equals(mes) || "Nov".equals(mes)) {
            mesInt = 11;
        }
        if ("Dic".equals(mes) || "Dec".equals(mes)) {
            mesInt = 12;
        }
        return mesInt;
    }

    public long getDateResulttoLong(int days) {
        //long dateResult = 0;
        Date data = new Date();
        data.setDate(data.getDate() - days);
        return data.getTime();
    }

    public boolean checkDayDifference(Date fecha_ult_modif_archivo, int cantDays) {
        boolean diff = false;
        CurrentDate current1 = new CurrentDate(fecha_ult_modif_archivo); //fecha de modificacion del archivo
        CurrentDate current2 = new CurrentDate(); //fecha actual
        if (current1.getDay() == current2.getDay()) {
            diff = false;
        }
        if (current1.getDay() > 5 && current2.getDay() > 5) {
            if (current1.getDay() + cantDays <= current2.getDay()) {
                diff = true;
            }
        }
        if (current2.getDay() <= 5) {
        }
        return diff;
    }
}
