package TAFACE2ApiEntidad;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import tangible.CurrentDate;

public class TALog {

    public enum enumTipoLog {

        LogError,
        LogDebug,
        LogAdvertencia,
        LogOK,
        LogErrorDeConexion;

        public int getValue() {
            return this.ordinal();
        }

        public static enumTipoLog forValue(int value) {
            return values()[value];
        }
    }
    private String Carpeta = "LOG";
    private String EmpresaRUT = "";
    private int SucursalId;
    private int CajaId;
    private String _rutaArchivo;
    private final CurrentDate currentDate;

    //Creo el Log si no existe
    public TALog(String CarpetaOperacion, String EmpRut, int SucId, int CajId) {

        currentDate = new CurrentDate();

        try {
            if (!EmpRut.trim().equals("")) {
                //Si viene cargado el rut es un log de error, de lo contrario es un log del debug
                _rutaArchivo = CarpetaOperacion + Carpeta;
                EmpresaRUT = EmpRut;
                SucursalId = SucId;
                CajaId = CajId;
            } else {
                //La carpeta es TAFACE\Debug
                _rutaArchivo = CarpetaOperacion;
            }
            if (!((new File(_rutaArchivo)).exists())) {
                {
                    new File(_rutaArchivo).mkdirs();
                }
            }
        } catch (Exception e) {
        }
    }

    private String getCarpetaArchivo() {
        return _rutaArchivo + File.separator;
    }

    public final String getNombreArchivo(enumTipoLog TipoLog) {

        if (TipoLog == enumTipoLog.LogDebug) {
            return "ClienteDebug_" + currentDate.getYear() + currentDate.getMonth() + currentDate.getDay() + ".log";
        } else if (TipoLog == enumTipoLog.LogErrorDeConexion) {
            return "CNX_" + EmpresaRUT + "_" + SucursalId + "_" + CajaId + "_" + currentDate.getYear() + currentDate.getMonth() + currentDate.getDay() + ".log";
        } else {
            return EmpresaRUT + "_" + SucursalId + "_" + CajaId + "_" + currentDate.getYear() + currentDate.getMonth() + currentDate.getDay() + ".log";
        }
    }

    public final boolean ObtenerXMLLog(tangible.RefObject<String> XML, tangible.RefObject<String> CarpArchivo, tangible.RefObject<String> NomArchivo, tangible.RefObject<Date> Creacion) throws FileNotFoundException, IOException {
        //Si no hay archivos devuelvo false
        try {
            //Busco logs de error
            String Log = getCarpetaArchivo();
            File dir = new File(Log);
            FilenameFilter filter = new FilenameFilter() {
                @Override
                public boolean accept(File current, String name) {
                    return name.contains(EmpresaRUT + "_" + SucursalId + "_" + CajaId + "_");
                }
            };
            File[] filesList = dir.listFiles();
            for (File file : filesList) {
                if (file.isFile()) {
                    ObtenerCarpetaArchivo(file.getAbsolutePath(), CarpArchivo, NomArchivo);
                    if (!NomArchivo.argValue.substring(0, 1).equals("_")) {
                        //mover archivo
                        File afile = new File(CarpArchivo.argValue + NomArchivo.argValue);
                        Date date = new Date(afile.lastModified());
                        File movedFile = new File(CarpArchivo.argValue + "_" + NomArchivo.argValue);
                        afile.renameTo(movedFile);
                        FileInputStream fis = new FileInputStream(movedFile);
                        byte[] data = new byte[(int) file.length()];
                        fis.read(data);
                        fis.close();
                        XML.argValue = new String(data, "UTF-8");
                        Creacion.argValue = date;
                        return true;
                    }
                }
            }
        } catch (RuntimeException ex) {
            return false;
        }
        return false;
    }

    public final void Eliminar(String RutaArchivo) {
        try {
            (new File(RutaArchivo)).delete();
        } catch (RuntimeException ex) {
            try {
                throw new TAException(ex.getMessage());
            } catch (TAException ex1) {
                Logger.getLogger(TALog.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    private void Escribir(String texto, enumTipoLog TipoLog) throws UnsupportedEncodingException {
        try {
            OutputStreamWriter osw;

            if ((EmpresaRUT.trim() == null || EmpresaRUT.trim().length() == 0) && TipoLog != enumTipoLog.LogDebug) {
                return;
            }

            //primero leo el contenido del log
            String a = getCarpetaArchivo();
            String b = getNombreArchivo(TipoLog);
            File logFile = new File(a + b);
            if (logFile.exists()) {
                FileInputStream fis = new FileInputStream(logFile);
                byte[] data = new byte[(int) logFile.length()];
                fis.read(data);
                fis.close();
                String initialText = new String(data, "UTF-8");
                Format formatter = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                String string = formatter.format(new Date());
                texto = "{0:" + string + "}" + " - " + texto;
                osw = new OutputStreamWriter(new FileOutputStream(getCarpetaArchivo() + getNombreArchivo(TipoLog)), Charset.forName("UTF-8").newEncoder());
                PrintWriter writer = new PrintWriter(osw);
                writer.print(initialText);
                writer.println();
                writer.print(getCarpetaArchivo() + getNombreArchivo(TipoLog));
                writer.print(texto);
                writer.println();
                osw.close();
            } else {
                Format formatter = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                String string = formatter.format(new Date());
                texto = "{0:" + string + "}" + " - " + texto;
                osw = new OutputStreamWriter(new FileOutputStream(getCarpetaArchivo() + getNombreArchivo(TipoLog)), Charset.forName("UTF-8").newEncoder());
                PrintWriter writer = new PrintWriter(osw);
                osw.append(getCarpetaArchivo() + getNombreArchivo(TipoLog));
                writer.print(texto);
                writer.println();
                osw.close();
            }

        } catch (Exception e) {
        }
    }

    public final void EscribirLog(String texto, enumTipoLog TipoLog) throws UnsupportedEncodingException {
        texto = TipoLog.toString() + " - " + texto;
        this.Escribir(texto, TipoLog);
    }

    //borra los logs de 5 dias de antiguedad
    public void LimpiarLogs() {
        try {
            String Log = getCarpetaArchivo();
            File dir = new File(Log);
            File[] filesList = dir.listFiles();
            for (File file : filesList) {
                if (file.isFile()) {
                    Date date = new Date(file.lastModified());
                    Date logDate = new Date();
                    logDate.setDate(logDate.getDate() - 5);
                    if (date.before(logDate)) {
                        file.delete();
                    }
                }
            }
        } catch (RuntimeException ex) {
            throw ex;
        }
    }

    private boolean ObtenerCarpetaArchivo(String RutaArchivo, tangible.RefObject<String> CarpetaArchivo, tangible.RefObject<String> NomArchivo) {
        int PosCarp = 0;
        boolean tempObtenerCarpetaArchivo = false;
        PosCarp = RutaArchivo.lastIndexOf(File.separator);
        CarpetaArchivo.argValue = RutaArchivo.substring(0, PosCarp + 1);
        NomArchivo.argValue = RutaArchivo.substring((PosCarp + 2) - 1);
        tempObtenerCarpetaArchivo = true;

        return tempObtenerCarpetaArchivo;
    }
}