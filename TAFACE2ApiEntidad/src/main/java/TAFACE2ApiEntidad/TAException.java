package TAFACE2ApiEntidad;

/**
 *
 * @author danielcera
 */
import java.io.IOException;

public class TAException extends IOException {

    public int CodError = 8;
    public int CodErrorInterno = 1000;

    public TAException() {
    }

    public TAException(String message) {
        super(message);
    }

    public TAException(String message, int pCodError) {
        super(message);
        CodErrorInterno = pCodError;
        SetCodigoPublico(pCodError);
    }

    public TAException(String message, IOException inner) {
        super(message, inner);
        if (!((inner == null)) && inner.getClass() == TAException.class) {
            CodError = ((TAException) inner).CodError;
        }
    }

    public TAException(String message, IOException inner, int pCodError) {
        super(message, inner);
        CodErrorInterno = pCodError;
        if (!((inner == null)) && inner.getClass() == TAException.class) {
            CodError = ((TAException) inner).CodError;
        } else {
            SetCodigoPublico(pCodError);
        }
    }

    //1 - Firmador de factura electronica no responde - Reintente
    //2 - CAE o Certificado no disponible 
    //3 - Error en configuracion
    //4 - Falta especificar datos obligatorios
    //5 - Error en base de datos del firmador
    //6 - Montos invalidos
    //7 - Error en la integracion
    //8 - Error interno del componente
    //9 - Error iniciando conexion(wakeup)
    //Seteo codigo publico segun el codigo privado
    //!!! Ver codigos disponibles en el archivo "TAException_Codigos_de_Error.xlsx"
    public void SetCodigoPublico(int CodigoInterno) {
        if (1 <= CodigoInterno && CodigoInterno < 1999) {
            //4 - Firmador de factura electronica no responde - Reintente
            CodError = 4;
        } else if (3100 <= CodigoInterno && CodigoInterno < 3200) {
            //CAE o Certificado no disponible 
            CodError = 2;
        } else if (3200 <= CodigoInterno && CodigoInterno < 3300) {
            //Error en configuracion
            CodError = 3;
        } else if (6000 <= CodigoInterno && CodigoInterno < 7000) {
            //Falta especificar datos obligatorios
            CodError = 1;
        } else if (3000 <= CodigoInterno && CodigoInterno < 3100) {
            //Error en base de datos del firmador
            CodError = 5;
        } else if (5000 <= CodigoInterno && CodigoInterno < 6000) {
            //Montos invalidos
            CodError = 6;
        } else if (4000 <= CodigoInterno && CodigoInterno < 5000) {
            //Error en la integracion 
            CodError = 7;
        } else if (3300 <= CodigoInterno && CodigoInterno < 3700) {
            //Error interno del componente
            CodError = 8;
        } else if (CodigoInterno == 9999) {
            //Error iniciando conexion(wakeup)
            CodError = 9;
        } else {
            //No se pudo identificar el codigo de error interno
            CodError = 0;
        }
    }
}