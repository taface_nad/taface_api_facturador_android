package TAFACE2ApiEntidad;

import XML.XMLCONFDEBUG;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.xml.bind.JAXBException;

public class TADebug {

    private String sRutaCarpetaDebug;
    private XMLCONFDEBUG objConfig;

    public final String getRutaCarpetaDebug() {
        return sRutaCarpetaDebug;
    }

    public final void setRutaCarpetaDebug(String value) {
        sRutaCarpetaDebug = value;
    }

    public final void EscribirDebug(String methodName, Object[] parmsNames, Object[] parmsValues) throws UnsupportedEncodingException, FileNotFoundException, IOException, JAXBException {
        String DebugMsg = methodName + " --> ";
        String Directorio = "";
        try {
            objConfig = Configuracion();
            if (objConfig.getTRAZAENCENDIDA() == 1) {
                for (int i = 0; i < parmsNames.length; i++) {
                    if (parmsValues[i] != null) {
                        DebugMsg += parmsNames[i].toString() + " : " + parmsValues[i].toString() + " || ";
                    } else {
                        DebugMsg += parmsNames[i].toString() + " : Nothing || ";
                    }
                }
                Directorio = sRutaCarpetaDebug + File.separator + "Debug";
                if (!new File(Directorio).exists()) {
                    new File(Directorio).mkdirs();
                }
                TALog objLog = new TALog(Directorio, "", 0, 0);
                int threadid = 0;
                try {
                    threadid = (int) Thread.currentThread().getId();
                } catch (Exception e) {
                }
                DebugMsg = "Thread: " + threadid + " | " + DebugMsg;
                objLog.EscribirLog(DebugMsg, TALog.enumTipoLog.LogDebug);
            }
        } catch (RuntimeException ex) {
            String ErrorMsg = ex.getMessage() + " || Error al leer parametros || --> " + DebugMsg;
            TALog objLog = new TALog(Directorio, "", 0, 0);
            objLog.EscribirLog(ErrorMsg, TALog.enumTipoLog.LogError);
        }
    }

    private XMLCONFDEBUG Configuracion() throws FileNotFoundException, IOException, JAXBException {
        if ((objConfig == null)) {
            objConfig = new XMLCONFDEBUG();
            objConfig = objConfig.CargarConfiguracion(sRutaCarpetaDebug);
        }
        return objConfig;
    }
}