package TAFACE2ApiEntidad;

import java.util.Date;

public class TAFormato {

    public enum TIPO_ALINTEXTO {

        cDerecha(1),
        cIzquierda(2),
        cCentrado(3),
        cNoAlinear(4);
        private int intValue;
        private static java.util.HashMap<Integer, TIPO_ALINTEXTO> mappings;

        private static java.util.HashMap<Integer, TIPO_ALINTEXTO> getMappings() {
                    if (mappings == null) {
                        mappings = new java.util.HashMap<Integer, TIPO_ALINTEXTO>();
                    }
            return mappings;
        }

        private TIPO_ALINTEXTO(int value) {
            intValue = value;
            getMappings().put(value, this);
        }

        public int getValue() {
            return intValue;
        }

        public static TIPO_ALINTEXTO forValue(int value) {
            return getMappings().get(value);
        }
    }

    public final String CNullStr(Object Value) {
        String tempCNullStr = null;
        tempCNullStr = "";
        if (!((Value == null))) {
            //If VarType(Value) <> VariantType.Null Then
            if (Value != null) {
                tempCNullStr = String.valueOf(Value);
            }
        }
        return tempCNullStr;
    }

    public final String AjustoTexto(String Cadena, TIPO_ALINTEXTO Alinear, int Desde, int Hasta) {
        String tempAjustoTexto = null;

        tempAjustoTexto = Cadena;
        if (Desde == 0 && Hasta == 0) {
            switch (Alinear) {
                case cIzquierda:
                    tempAjustoTexto = trimStringFromPosition(Cadena, 0, 0, 0, Cadena.length());
                    break;
                case cDerecha:
                    tempAjustoTexto = trimStringFromPosition(Cadena, 1, 1, 0, Cadena.length());
                    break;
                case cCentrado:
                    tempAjustoTexto = Cadena.trim();
                    break;
            }
        } else {
            switch (Alinear) {
                case cIzquierda:
                    tempAjustoTexto = RTrim(Cadena.substring(Desde - 1, Hasta), Hasta - Desde + 1, ' ');
                    break;
                case cDerecha:
                    tempAjustoTexto = LTrim(Cadena.substring(Desde - 1, Hasta), Hasta - Desde + 1, ' ');
                    break;
                case cCentrado:
                    tempAjustoTexto = CTrim(tangible.DotNetToJavaStringHelper.substring(Cadena, Desde - 1, Hasta), Hasta - Desde + 1);
                    break;
                case cNoAlinear:
                    tempAjustoTexto = tangible.DotNetToJavaStringHelper.substring(Cadena, Desde - 1, Hasta);
                    break;
            }
        }
        return tempAjustoTexto;
    }

    public final short CTShort(Object Texto) {
        short tempCTShort = 0;
        if (isNumeric(Texto.toString())) {
            tempCTShort = Short.parseShort(String.valueOf(Texto));
        } else {
            tempCTShort = 0;
        }
        return tempCTShort;
    }

    // public String LTrim(String Cadena, int Largo) { original declaration
    public String LTrim(String Cadena, Integer Largo, char PadCad) {
        //LTrim = Space(Largo - Len(Left(Cadena, Largo))) & Left(Cadena, Largo)
        String Pad;
        int i;
        Pad = "";
        if (Largo > Cadena.length()) {
            for (i = 0; i < (Largo - Cadena.length()); i++) {
                Pad = Pad + PadCad;
            }
            Cadena = Pad + Cadena;
        } else {
            Cadena = Cadena.substring(0, Largo);
        }
        return Cadena;
    }

    public String RTrim(String Cadena, Integer Largo, char PadCad) {
        //LTrim = Space(Largo - Len(Left(Cadena, Largo))) & Left(Cadena, Largo)
        String Pad;
        int i;
        Pad = "";
        if (Largo > Cadena.length()) {
            for (i = 0; i < (Largo - Cadena.length()); i++) {
                Pad = Pad + PadCad;
            }
            Cadena = Cadena + Pad;
        } else {
            Cadena = Cadena.substring(0, Largo);
        }
        return Cadena;
    }

    public final String CTrim(String Cadena, int Largo) {
        int i = 0;
        String CadTxt;
        try {
            CadTxt = Cadena;
            for (i = 0; i < ((Largo - CadTxt.length()) / 2); i++) {
                Cadena = " " + Cadena;
            }
            for (i = 0; i < ((Largo - CadTxt.length()) / 2); i++) {
                Cadena = Cadena + " ";
            }
            if (Cadena.length() < Largo) {
                return Cadena + " ";
            } else if (Cadena.length() < Largo) {
                Cadena = Cadena.substring(0, Largo).trim();
            }
            return Cadena;
        } catch (Exception e) {
            return "";
        }
    }

    //////////////ADDED FUNCTIONS//////////
    private boolean isNumeric(String s) { //equivalente al isNumeric de VB
        return s.matches("[-+]?\\d*\\.?\\d+");
    }

    //trim a cualquier posicion de la cadena
    public static String trimStringFromPosition(String cadena, int left, int rigth, int posD, int posH) {
        String resultTrim = "";
        for (int i = posD; i < posH; i++) {
            if (left == 0 && rigth == 0) {
                if (cadena.charAt(i) != ' ') {
                    resultTrim = cadena.substring(i, posH);
                    i = posH;
                }
            }
            if (left == 0 && rigth == 1) {
                resultTrim = cadena.substring(posD, posH).trim();
                i = posH;
            }
            if (left == 1 && rigth == 1) {
                resultTrim = reverse(trimStringFromPosition(reverse(cadena), 0, 0, posD, posH));
            }
            if (left == 2 && rigth == 2) {
                if (cadena.charAt(i) != ' ') {
                    resultTrim += Character.toString(cadena.charAt(i));
                }
            }
        }
        return resultTrim;
    }

    //reversar un string
    public static String reverse(String input) {
        char[] in = input.toCharArray();
        int begin = 0;
        int end = in.length - 1;
        char temp;
        while (end > begin) {
            temp = in[begin];
            in[begin] = in[end];
            in[end] = temp;
            end--;
            begin++;
        }
        return new String(in);
    }
    
    /**
     * 
     * @param fecha
     * Recibe una cadena en formato 2018-01-31 12:00:00 y retorna el objeto Date generado
     * @return 
     */
    public Date ConvertirStringAFechaHora(String fecha) {
        Date resultDate = null;
        if (fecha.length() > 0) {

            String[] l = fecha.split(" ");
            String[] fechaS = l[0].split("-");
            String[] timeS = l[1].split(":");
            int year, month, day, hour, minute, second = 0;
            year = Integer.parseInt(fechaS[0]);
            month = Integer.parseInt(fechaS[1]);
            day = Integer.parseInt(fechaS[2]);
            hour = Integer.parseInt(timeS[0]);
            minute = Integer.parseInt(timeS[1]);
            second = Integer.parseInt(timeS[2]);
            resultDate = new Date(year - 1900, month - 1, day, hour, minute, second);
        }
        return resultDate;
    }
    
     /**
     * 
     * @param fecha
     * Recibe una cadena en formato 2018-01-31 12:00:00 y retorna el objeto Date generado
     * @return 
     */
    public Date ConvertirStringAFecha(String fecha) {
        Date resultDate = null;
        if (fecha.length() > 0) {
            String[] fechaS = fecha.split("-");
            int year, month, day = 0;
            year = Integer.parseInt(fechaS[0]);
            month = Integer.parseInt(fechaS[1]);
            day = Integer.parseInt(fechaS[2]);
            resultDate = new Date(year - 1900, month - 1, day);
        }
        return resultDate;
    }
}